<?php
namespace frontend\controllers;


use yii\web\Controller;
use Yii;
use yii\db\Query;

use common\models\Store;
use common\models\Deal;
use common\models\Workorderpopup;
use common\models\Workorders;
use common\models\Country;
use frontend\models\UserStore;
use frontend\models\Manufacturer;
use frontend\models\Profiles;
use frontend\models\Storelocator;
use frontend\models\Loyaltypoint;
use common\models\User;
use common\models\Pass;
use common\models\Loyalty;
use common\models\Order;
use common\models\UserLoyalty;
use common\models\Qr;
//use common\models\Loyaltypoint;
use common\models\Loyaltycard;
use common\models\Profile;
use common\models\OrderItems;
use common\models\OrderItem;
use common\models\OrderSubItems;
use frontend\models\UserCategory;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use dosamigos\qrcode\QrCode;

class StoreController extends Controller
{    
       
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }
    	
  /// stores
    public function actionIndex()
    {        
	   //$user_id=Yii::$app->user->id;   
	   
	   
       $model = new Manufacturer();
	   $user_id=Yii::$app->user->id;   
	   
	   $dataProvider =$model->getManufacturer($user_id);
	   
	
	   
	    return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' =>'',
            
        ]);
  
    }
    //Store list
	public function actionView($id)
	{        
		$model=Manufacturer::find()
		->joinWith('profiles')
		->where([Manufacturer::tableName().'.id'=>$id,'user_type'=>Manufacturer::ROLE_LOCATION_OWNER])
		->one();

		 return $this->render('view', [
            'model' => $model,
        
        ]);
		
	}

    /* To add Brand in favourite list */
    public function actionUpdate($id)
	{
		
		$user_id=Yii::$app->user->id;        
		$userStore=UserStore::findOne(['user_id'=>$user_id,'store_id'=>$id]);
		if($userStore)
		{
			$userStore->status=1;
			$userStore->save();
		}
		else
		{
			$userStore=new UserStore();
			$userStore->user_id=$user_id;
			$userStore->store_id=$id;
			$userStore->status=1;
			$userStore->save();
		}
		  return $this->redirect(['view', 'id' => $id]);
	}
	/* To remove an store from favourite list */
    public function actionDelete($id)
    {        
        $user_id=Yii::$app->user->id;
      
		$userStore=UserStore::findOne(['user_id'=>$user_id,'store_id'=>$id,'status'=>1]);
	
		if($userStore)
		{
			$userStore->status=0;
			$userStore->save();
			  return $this->redirect(['view', 'id' => $id]);
		}
		else
			  return $this->redirect(['view', 'id' => $id]);
		
    }
    
	//Address of Store
	public function actionStorelocator($id)
	{	
		
    $model=new Storelocator();   
	$view=Manufacturer::find()
		->joinWith('profiles')
		->where([Manufacturer::tableName().'.id'=>$id,'user_type'=>Manufacturer::ROLE_LOCATION_OWNER])
		->one();
	$model->getlatitue();
	$model->getorgname($id);
	
	 
    if($model->validate())    
    {		
        $lng=$model->lng;
        $lat=$model->lat;
        
        
       
        $countryTable=Country::tableName();
            $query = new Query;
          
            $exp=sprintf('(6371 * acos (cos ( radians(%s) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(%s) ) + sin ( radians(%s) ) * sin( radians( latitude ) ) )) as distance',$lat,$lng,$lat);
            $expression = new Expression($exp);
			
            // compose the query
        $query->select('id,storename,address,street,city,state,owner,name as country,postal_code,latitude,longitude, '.$expression)
            ->from(Store::tableName())
            ->where(['owner'=>$id])
            ->andWhere(['status'=> 1])
            ->join("INNER JOIN", $countryTable,sprintf('%s.id_countries=%s.country',$countryTable,Store::tableName()))
            ->orderBy('distance');
			
        //	$query= Store::find(['owner'=>$id]);//with('countryname',false);
            
             $dataProvider =  new ActiveDataProvider([
                'query' => $query,
            ]);
            
            return $this->render('storelocator', [
            'dataProvider' => $dataProvider,
             'orgName' => $model->orgName,  
             'brandLogo' => $model->brandLogo,          
        ]);
        
    }else{
       return $this->render('view', [
            'model' => $view,
            ]);
        }
     }
        
        //Location of particular Store 
    public function actionStorelocatorview($id)
	{	
		
    $model = new Storelocator(); 
    $model->getlatitue(); 
	 
    if($model->validate())    
    {		
        $lng=$model->lng;
        $lat=$model->lat;
       
        $countryTable=Country::tableName();
            $query = new Query;
          
            $exp=sprintf('(6371 * acos (cos ( radians(%s) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(%s) ) + sin ( radians(%s) ) * sin( radians( latitude ) ) )) as distance',$lat,$lng,$lat);
            $expression = new Expression($exp);

            // compose the query
			$query->select('*,name as country, '.$expression)
            ->from(Store::tableName())
            ->join("INNER JOIN", $countryTable,sprintf('%s.id_countries=%s.country',$countryTable,Store::tableName()))
            ->andWhere(['id'=>$id])->one();
           

        //	$query= Store::find(['owner'=>$id]);//with('countryname',false);
            
           $command = $query->createCommand();
		   $data = $command->queryOne();   
	
            return $this->render('storelocatorview', [
            'model' => $data,
                     
        ]);
        
    }else{
       return $this->render('view', [
            'model' => $model,
            ]);
        }
     }
     
     // Current Deal of particular store
   public function actionCurrentdeals($id)
     {
	   $user_id=Yii::$app->user->id; 
	   $store_id=$id; 
	   $category_ids=implode(",",ArrayHelper::map( UserCategory::find()->select('category_id')->where(['user_id'=>$user_id,'status'=>1])->all(), 'category_id', 'category_id'));
	   $dataProvider = new ActiveDataProvider([ 
            'query' => Deal::find()
       		 ->joinWith('workorderpopup')       
              
       ->joinWith(['workordercategory'=> function ($q) use ($category_ids) {
            if($category_ids=="")
                $category_ids='0';
        $q->where('category_id in ( '.$category_ids.')');
           }
        ],false,'INNER JOIN')        
        
        ->joinWith(['workorders'=> function ($q) use($store_id)  {
            
        $q->where(sprintf('dateFrom <= %s and (dateTo + 86400) > %s and advertiserID=%s ',time(date("Y-m-d")),time(date("Y-m-d")),$store_id));}
        ],false,'INNER JOIN')
		->where(Workorders::tableName().'.status = 1')
		->andWhere([Workorders::tableName().'.workorderType' =>1])
		->andWhere(['IN', Workorders::tableName().'.type', [1,4]])
		->groupBy('id')       
       ->orderBy("id desc")	
        ]);
        return $this->render('currentdeals', [
            'dataProvider' => $dataProvider,            
        ]);
	 }
	 
	public function actionLoyalty($id)
	{ 
	$user_id=Yii::$app->user->id; 	 
    $SLoyaltypoint= new Loyaltypoint();
    $SLoyaltypoint->load(Yii::$app->getRequest()->getBodyParams(), ''); 
    $SLoyaltypoint->user_id= $user_id; 

	if ($SLoyaltypoint->load(Yii::$app->request->post()) && $SLoyaltypoint->validate())
	{	
		$checkUserbday = User::findOne(['id'=>Yii::$app->user->id]); 
		$checkStorebday = User::findOne(['id'=>$id]);
		$loyalty= Loyalty:: find()->select(
				[Loyalty::tableName().'.*'])
				->where([Loyalty::tableName().'.user_id' =>$id,
				'status'=>1])
				->orderBy([Loyalty::tableName().'.id' => SORT_DESC])->asArray()->one();
				if(($checkUserbday->bdayOffer==1) && ($checkStorebday->bdayOffer==1) ){
				$loyaltypoints = $loyalty['pointvalue']* $model->amount*2;
				$SLoyaltypoint->method = 3;
			}else{
				$loyaltypoints = $loyalty['pointvalue']* $SLoyaltypoint->amount;
				$SLoyaltypoint->method = 2;
			}	
			$loyaltypoints = $loyalty['pointvalue']* $SLoyaltypoint->amount;
			$SLoyaltypoint->valueOnDay = $loyalty['dolor']/$loyalty['loyalty'];
			$SLoyaltypoint->rValueOnDay = $loyalty['redeem_pointvalue'];
			$SLoyaltypoint->order_id = 0;
			$SLoyaltypoint->loyaltypoints = $loyaltypoints;
			$SLoyaltypoint->status = 1;
			$SLoyaltypoint->store_id=$id;
			$SLoyaltypoint->save();
			}
		$UserLoyalty=  UserLoyalty:: find()->select(
					[UserLoyalty::tableName().'.loyaltyID',UserLoyalty::tableName().'.user_id',UserLoyalty::tableName().'.store_id','ifnull(value,0) as value', max(['points'])])
					->joinWith("profile")			
					->where([UserLoyalty::tableName().'.user_id' => Yii::$app->user->id])
					->andWhere([UserLoyalty::tableName().'.store_id' => $id])
					->one();
	
		if($UserLoyalty=="")	{
			return $this->render('loyalty', [
            'model' => $UserLoyalty,
            'id'=>$id,
			'SLoyaltypoint'=>$SLoyaltypoint,
        ]);}
        else{
		 return $this->render('loyalty', [
           'model'=>$UserLoyalty,
			'SLoyaltypoint'=>$SLoyaltypoint,
        ]);
			}
		
	}
		 
	
	 public function actionPaywithpoints($id)
	{        
		$user_id=Yii::$app->user->id;   
        $store_id=$id;
	
		$model=new Deal();
		$model->paywithpoint=1;
	 $dataProvider= new ActiveDataProvider([
            'query' => $model::find()
            ->select([Deal::tableName().".*", new Expression("1 as 'paywithpoint'")])
                  
       ->joinWith('workorderpopup')              
     
        ->joinWith(['workorders'=> function ($q) use($store_id)  {
            
        $q->where(sprintf('dateFrom <= %s and (dateTo + 86400) > %s and advertiserID=%s ',time(date("Y-m-d")),time(date("Y-m-d")),$store_id));}
        ],false,'INNER JOIN')
		->where(Workorders::tableName().'.status = 1')
		->andWhere([Workorders::tableName().'.workorderType' =>1])
		->andWhere([Workorders::tableName().'.buyNow' =>1])
		->andWhere(['IN', Workorders::tableName().'.type', [1,4]])
        ->groupBy('id')        
       ->orderBy("id desc"),
      
       
        ]);
		 return $this->render('paywithpoints', [
           'dataProvider'=>$dataProvider,
		   'id'=>$id,
        ]);
		
	}
	
	public function actionOrdernow($id)
	{
		$user_id=Yii::$app->user->id;
		  $dataProvider = new ActiveDataProvider([ 
            'query' => Order::find()
			->where([Order::tableName().'.user_id'=>Yii::$app->user->id])
			->where([Order::tableName().'.id'=>$id])
			
		->joinWith('orderitems')
		->joinWith('orderitems.ordersubitems')
		 ->orderBy("id desc")
       			
        ]);
        //echo'<pre>';print_r($dataProvider);die;
		 return $this->render('ordernow', [
           'model'=> $dataProvider,
        ]);
	}
	 
	public function actionMypurchase($id)
    {	
		//$this->layout='main';
        return $this->render('mypurchase');
    }
    
    public function actionMenu($id)
    {	
		//$this->layout='main';
        return $this->render('menu');
    }
    public function actionPendingorder($id)
    {
		$this->layout='main';
        	$model=Manufacturer::find()
		->joinWith('profiles')
		->where([Manufacturer::tableName().'.id'=>$id,'user_type'=>Manufacturer::ROLE_LOCATION_OWNER])
		->one();
		
		 return $this->render('pendingorder', [
            'model' => $model,
        
        ]);

	}

}
