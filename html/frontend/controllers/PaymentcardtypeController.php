<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use common\models\Paymentcardtype;

class PaymentcardtypeController extends ActiveController
{    
        public $modelClass = 'common\models\Paymentcardtype';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
    public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['delete'],$actions['view']);                    

		return $actions;
	}

  	   /* To get all cards*/
   public function actionView($id)
   {
        $user_id=Yii::$app->user->id;
       return new ActiveDataProvider([
                            'query' => Paymentcardtype::find()
                                        ->with(['myids'=>function ($query) use($user_id){
                                                    $query->where(['user_id'=>$user_id]);
                                        },
                       ])
                       
                       ->where(['id'=>$id])
                        
                            
                       
                        ]);
            
   }
  
   
}
