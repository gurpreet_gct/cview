<?php
namespace frontend\controllers;

use yii\web\Controller;
use Yii;
use yii\db\Query;
use common\models\DealBookmark;
use common\models\Deal;
use frontend\models\UserCategory;
use frontend\models\Manufacturer;
use frontend\models\Storelocator;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class BookmarkdealController extends Controller
{    
  
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }
    
  	public function actionBookmark($id)
    {
				
        $user_id=Yii::$app->user->id; 
         $model= new DealBookmark();
			$model->user_id=$user_id;
            $model->deal_id=$id;
            $model->created_at= time(date("Y-m-d"));
            
         if ($model->save(false)) {
					   return $this->redirect(['index']);
            }else {
		 
		   return $this->redirect(['index']);
		 
		}			
       
    }
    
      public function actionUnbookmark($id)
    {
				
        $user_id=Yii::$app->user->id; 
        
          $model= DealBookmark::find()->where(['user_id'=>$user_id,'deal_id'=>$id])->one();
            if($model)
            {
             $model->delete();   
               return $this->redirect(['index']);
            }else{
				 return $this->redirect(['index']);
			}
          
    }
    
  
    public function actionIndex()
    {   
    
	   $user_id=Yii::$app->user->id;   
       $category_ids=implode(",",ArrayHelper::map( UserCategory::find()->select('category_id')->where(['user_id'=>$user_id,'status'=>1])->all(), 'category_id', 'category_id'));
	  
      
      
	   $dataProvider = new ActiveDataProvider([
            'query' => Deal::find()
                  
       ->joinWith('workorderpopup')
       
       
        ->joinWith(['dealbookmark'=> function ($q) use ($user_id) {
            
           $q->where('user_id='. $user_id);
           }
        ],false,'INNER JOIN')
        
       
       ->joinWith(['workordercategory'=> function ($q) use ($category_ids) {
            if($category_ids=="")
                $category_ids='0';
           $q->where('category_id in ( '. $category_ids.')');
           }
        ],false,'INNER JOIN')
        
        
        ->joinWith(['workorders'=> function ($q)  {
            
        $q->where(sprintf('dateFrom <= %s and dateTo > %s',time(date("Y-m-d")),time(date("Y-m-d"))));}
        ],false,'INNER JOIN')
        ->groupBy('id')        
       ->orderBy("id desc")
        ]);
        
       return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' =>'',
            
        ]);
  
       
		

    }
    
    

    
 
	
}
