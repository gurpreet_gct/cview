<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use api\modules\v1\models\Cart;
use api\modules\v1\models\Customer;
use common\models\PaymentAccount;
use common\models\PaymentMethod;
use common\models\Transaction;
use common\models\Order;
use common\models\OrderItems;
use common\models\Loyaltypoint;
use common\models\Loyalty;
use common\models\OrderSubItems;
use common\models\OrderLoyalty;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class CartController extends ActiveController
{    
        public $modelClass = 'api\modules\v1\models\cart';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
    public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);                    

		return $actions;
	}


  	public function actionCreate(){	 
		
		 $data=Yii::$app->getRequest()->getBodyParams();    
				 
		 $cart=json_decode($data['cart']);
		 $model=new Cart();
		 $model->data=$cart;		 
		 
		 if($model->validate())
		 {			
		
			Yii::$app->db->createCommand()->batchInsert(Cart::tableName(), $model->attributes(), $model->rows)->execute(); 
			$braintree = Yii::$app->braintree;
			$response = $braintree->call('ClientToken', 'generate' ,[]);
			return [
			"message"=> "",
			'session_key'=>$model->session_key,
			'clientToken'=>$response,
			'loyalty'=>$model->loyalty,
			"statusCode"=> 200
			];
		 }
		return $model;
		
	}
    
    /* To validate cart and payment */
    public function actionUpdate($id)
    {
		
		if($id)
		{
						
			try{
				 $data=Yii::$app->getRequest()->getBodyParams();    
				 $cart=json_decode($data['cart']);
				 $model=new Cart();
				 $model->data=$cart;
				 $model->useloyalty=isset($data['useloyalty'])?$data['useloyalty']:0;
				 $model->session_key=$id;		 
				 /* Create instance of payment gateway */
				 $braintree = Yii::$app->braintree;
				 /* validate the data/cart */
				 if($model->validate())
				 {	
					$payment_method=new PaymentMethod();
					if($model->payable >0)
					{
					 /* find the account of customer for braintree */		
					$paymentAccount=PaymentAccount::find()->where(['user_id'=>Yii::$app->user->id,'status'=>1])->one();
					if(!$paymentAccount)
					{
						/* register customer to braintree */
						$user=Customer::findOne(Yii::$app->user->id);		
						$response = $braintree->call('Customer', 'create', [
							'firstName' => $user->firstName,
								'lastName' => $user->lastName,
								//'company' => 'graycell',
								'email' => $user->email,
								'phone' => $user->phone,    
								//'website' => ''   
							]);
							
							/* Register braintree account detail to database */
						    $paymentAccount=new PaymentAccount();
						    $paymentAccount->user_id=Yii::$app->user->id;
						    $paymentAccount->bt_id=$response->customer->id;
						    $paymentAccount->status=1;
						    $paymentAccount->save();
							
					}
					
					/* generate the payment method */
					$response = $braintree->call('PaymentMethod','create',[
						'customerId' => $paymentAccount->bt_id,
						'paymentMethodNonce' => $data['nonce'],
					]);
					
					if(!$response->success)
					{
					
						return [
							"message"=> $response->message,
							'nonce'=>$data['nonce'],
							'cart'=>$data['cart'],
							'session_key'=>$id,
							"statusCode"=> 404
							];
					}
					
				    /* save the payment method detail and token */
					$payment_method=new PaymentMethod();
				//	$payment_method->load($response->paymentMethod->attributes());
					$payment_method->bin=$response->paymentMethod->bin;
					$payment_method->last4=$response->paymentMethod->last4;
					$payment_method->cardType=$response->paymentMethod->cardType;
					$payment_method->expirationMonth=$response->paymentMethod->expirationMonth;
					$payment_method->expirationYear=$response->paymentMethod->expirationYear;
					$payment_method->cardholderName=$response->paymentMethod->cardholderName;
					$payment_method->customerLocation=$response->paymentMethod->customerLocation;
					$payment_method->imageUrl=$response->paymentMethod->imageUrl;
					$payment_method->prepaid=$response->paymentMethod->prepaid;
					$payment_method->healthcare=$response->paymentMethod->healthcare;
					$payment_method->debit=$response->paymentMethod->debit;
					$payment_method->durbinRegulated=$response->paymentMethod->durbinRegulated;
					$payment_method->commercial=$response->paymentMethod->commercial;
					$payment_method->payroll=$response->paymentMethod->payroll;
					$payment_method->issuingBank=$response->paymentMethod->issuingBank;
					$payment_method->countryOfIssuance=$response->paymentMethod->countryOfIssuance;
				//	$payment_method->productId=$response->paymentMethod->productId;
					$payment_method->token=$response->paymentMethod->token;
					$payment_method->uniqueNumberIdentifier=$response->paymentMethod->uniqueNumberIdentifier;
					$payment_method->maskedNumber=$response->paymentMethod->maskedNumber;
					$payment_method->venmoSdk=$response->paymentMethod->venmoSdk;
					$payment_method->expirationDate=$response->paymentMethod->expirationDate;
					$payment_method->save(false);
					
				
						/* Request to prcess the transaction */
						$response = $braintree->call('Transaction', 'sale' ,[
						'amount' => $model->payable,
							'customerId' =>  $paymentAccount->bt_id,
							'paymentMethodToken' => $payment_method->token,
							 'options' => [
								'submitForSettlement' => True
							  ]

						]);
					
						if(!$response->success)
						{
						
							return [
								"message"=> $response->message,
								'nonce'=>$data['nonce'],
								'cart'=>$data['cart'],
								'session_key'=>$id,
								"statusCode"=> 404
								];
						}
					}
					/* Save the order */
					$order =new Order();					    
					//$order->shipping_description=
					//$order->shipper_id=
					$order->is_offline=0;
					$order->user_id=Yii::$app->user->id;
					$order->discount_amount=$model->discount;
					$order->grand_total=$model->total;
					$order->shipping_amount=0;
					$order->shipping_tax_amount=0;
					$order->subtotal=$model->total - $model->discount;
					$order->tax_amount=0;
					$order->total_offline_refunded=0;
					$order->total_online_refunded=0;
					$order->total_paid=0;
					$order->qty_ordered=$model->qty;
					$order->total_refunded=0;
					$order->can_ship_partially=0;
					$order->customer_note_notify=0;
					$order->billing_address_id=0;
					$order->billing_address="";
					$order->shipping_address_id=0;
					$order->shipping_address="";
					$order->forced_shipment_with_invoice=0;
					$order->weight=0;
					$order->email_sent=0;
					$order->edit_increment=0;
					$order->save(false);
					
				
					
				
					$created_at=time();
					$orderLoyalty=[];
					foreach($model->storeTotal as $key=>$value)
					{
						
							array_push($model->loyaltyOrderData,[
					"order_id"=>$order->id,
					"user_id"=>Yii::$app->user->id,
					"store_id"=>$key,
					"payable"=>$value["total"]-$value["discount"]-$value["loyalty"],
					"value"=>$value["loyalty"],
					
					
					]);
						
						
							
					
								$total_price_value = $value["total"]-$value["discount"] - $value["loyalty"];
								$result['user_id'] = Yii::$app->user->id;
								$result['order_id'] = $order->id;
								$result['store_id'] = $key;
								$result['status'] = 1;
								$result['created_at'] = $created_at;
								$result['updated_at'] = $created_at;
								$result['created_by'] = Yii::$app->user->id;
								$result['updated_by'] = Yii::$app->user->id;
								
								
								$loyalty = new Loyalty();			
								$loyaltyid=Loyalty::find()->where(['user_id'=>$key])
													->select('*')
													->orderBy(['id' => SORT_DESC,])
													->asArray()->one();
							
								
								if(count($loyaltyid)){								
								$Totalsloyaltypoints =$total_price_value * $loyaltyid['pointvalue'];
								
								array_push($orderLoyalty,[		
											'loyaltypoints'=>$Totalsloyaltypoints,
											'valueOnDay'=>$loyaltyid['loyalty'],
											'order_id'=>$order->id,
											'user_id'=>Yii::$app->user->id,
											'store_id'=>$key,
											'method'=>1,
											'status'=>1,
											'created_at'=>$created_at,
											'updated_at'=>$created_at,
											'created_by'=>Yii::$app->user->id,
											'updated_by'=>Yii::$app->user->id,
									]);
								}
								
					}
					
					
					if(count($model->loyaltyOrderData))
					{						
						$loyaltyOrderData=new OrderLoyalty();
						$loyaltyOrderDataColumn=$loyaltyOrderData->attributes();					    
						array_shift($loyaltyOrderDataColumn);						
					
					    Yii::$app->db->createCommand()->batchInsert(OrderLoyalty::tableName(), $loyaltyOrderDataColumn, $model->loyaltyOrderData)->execute(); 
					}
					
					if($model->payable>0)
					{
						$payment_method->order_id=$order->id;
						$payment_method->save(false);
					}
					/* Get the orderitems for batch insert */
					$orderItems=array();
					$orderSubItems=array();
					//$orderLoyalty=array();
					//$orderLoyalty=array();
					$result=array();
					
					 foreach($model->data as $key=>$value)
					{			   
						foreach($value as $item)
						{

									array_push($orderItems,[		
									'product_id'=>$item->productID,														
									'deal_id'=>$item->workOrderID,
									'dealCount'=>$item->dealCount,
									'isCombo'=>$item->isCombo,
									'qty'=>$item->quantity,
									'order_id'=>$order->id,
									'user_id'=>Yii::$app->user->id,
									'store_id'=>$item->storeID,
									'store_location_id'=>0,
									'product_price'=>$item->productPrice,
									'product_currency_code'=>0,		
									'product_currency_id'=>0,					
									'payment_currency_code'=>0,		
									'payment_currency_id'=>0,					
									'exchange_rate'=>0,
									'product_exchange_price'=>0,
									'tax'=>0,
									'shipping_charges'=>0,									
									'discount'=>$item->discount,
									'total'=>$item->total,
									'payable'=>$item->total-$item->discount,									
									'status'=>1,
									'created_at'=>$created_at,
									'updated_at'=>$created_at,
									'created_by'=>Yii::$app->user->id,
									'updated_by'=>Yii::$app->user->id,
									]);
									
									if($item->isCombo)
									{									

										array_push($orderSubItems,[		
											'product_id'=>$item->comboData->productID,														
											'deal_id'=>$item->comboData->workOrderID,																						
											'qty'=>$item->comboData->quantity,
											'order_id'=>$order->id,
											'user_id'=>Yii::$app->user->id,
											'store_id'=>$item->comboData->storeID,
											'store_location_id'=>0,
											'product_price'=>$item->comboData->productPrice,
											'product_currency_code'=>0,		
											'product_currency_id'=>0,					
											'payment_currency_code'=>0,																				
											'payment_currency_id'=>0,
											'exchange_rate'=>0,
											'product_exchange_price'=>0,
											'tax'=>0,
											'shipping_charges'=>0,									
											'discount'=>$item->comboData->discount,
											'total'=>$item->comboData->total,
											'status'=>1,
											'created_at'=>$created_at,
											'updated_at'=>$created_at,
											'created_by'=>Yii::$app->user->id,
											'updated_by'=>Yii::$app->user->id,
									]);
									
									}
									
							
						}
				
					}
				
					if(count($orderItems))
					{
						$OrderItems=new OrderItems();
						$orderItemColumn=$OrderItems->attributes();
					    array_shift($orderItemColumn);
						Yii::$app->db->createCommand()->batchInsert(OrderItems::tableName(), $orderItemColumn, $orderItems)->execute(); 
					
					if(count($orderSubItems))
					{
						
						$OrderSubItems=new OrderSubItems();
						$orderSubItemColumn=$OrderSubItems->attributes();					    
						array_shift($orderSubItemColumn);
					    Yii::$app->db->createCommand()->batchInsert(OrderSubItems::tableName(), $orderSubItemColumn, $orderSubItems)->execute(); 
					}
						if(count($orderLoyalty)){
						$Loyaltypoints=new Loyaltypoint();
						$LoyaltypointsColumn=$Loyaltypoints->attributes();
						array_shift($LoyaltypointsColumn);
						Yii::$app->db->createCommand()->batchInsert(Loyaltypoint::tableName(), $LoyaltypointsColumn, $orderLoyalty)->execute();
					}
					
					}
					
					
					/* empty the temp. cart */
					if($id)
					$tmodel=Cart::deleteAll('session_key = :key ', [':key' => $id]);
					
					$transactionID=0;
					if($model->payable>0)
					{
					/* save the transaction details */				
					$transaction=new Transaction();
					$transaction->order_id=$order->id;
					$transaction->tid = $response->transaction->id;
					$transaction->status = $response->transaction->status;
					$transaction->type = $response->transaction->type;
					$transaction->currencyIsoCode = $response->transaction->currencyIsoCode;
					$transaction->amount = $response->transaction->amount;
					$transaction->merchantAccountId = $response->transaction->merchantAccountId;
					$transaction->save(false);
					$transactionID=$transaction->tid;
					}
					
					/*Send confirmation email */
					$subject="SweetSpot:Order Confirmation";
					$user=Customer::find(Yii::$app->user->id)->one();
					
                   // $sitepath = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('user/reset');                        
                    $message=sprintf("Dear %s,<br/>Thanks for your purchase. You should receive your email confirmation soon. If you have further questions or enquiry. please CONTACT US. In the mean time, sit back and enjoy your day. <br/>Below are the order detail:<br/>Order ID: %s<br/>Transaction ID: %s<br/>Thanks<br/>SweetSpot Team.",
                    $user->fullName,$order->id,$transactionID);                    
                    $emailsend =Yii::$app->mail->compose()
                            ->setTo($user->email)
                            ->setFrom(Yii::$app->params["salesEmail"],'No Reply')
                            ->setSubject($subject)
                            ->setHtmlBody($message)
							->send();  
					
					/* email end 
					//$transaction->orderId = $response->transaction->orderId;*/
					return [
					"message"=> "Order successfully placed.",					
					'orderID'=>$order->id,										
					'transactionID'=>$transactionID,										
					"statusCode"=> 200
					];
				}
				 else
				 {
					 return $model;
					Yii::$app->db->createCommand()->batchInsert(Cart::tableName(), $model->attributes(), $model->rows)->execute(); 
					$braintree = Yii::$app->braintree;
					$response = $braintree->call('ClientToken', 'generate' ,[]);
					return [
					"message"=> "",
					'session_key'=>$model->session_key,
					'clientToken'=>$response,
					"statusCode"=> 200
					];
				 }
				return $model;
				
			}
			catch(Exception $e)
			{  	return [
					"message"=> $e->message,					
					'nonce'=>$data['nonce'],
					'cart'=>$data['cart'],
					'session_key'=>$id,
					"statusCode"=> 404
					];
				}
		}	
		
	}
	public function actionDelete($id)
	{
		if($id)
		$model=Cart::deleteAll('session_key = :key ', [':key' => $id]);
		return [
			"message"=> "",			
			"statusCode"=> 204
			];
		
		
	}
    

    

	
}
