<?php
namespace frontend\controllers;


use Yii;
use yii\web\Controller;
use yii\db\Query;
use common\models\Product;
use common\models\Workorderpopup;
use common\models\Profile;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class MyorderController extends Controller
{    
   
   public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }  
	   
    
 
  	    
    public function actionView($id)
    {   
		
	   $qty = Yii::$app->getRequest()->getQueryParam('qtyplus');  	  
	   $qtymin = Yii::$app->getRequest()->getQueryParam('qtymin'); 
	      /* combo product qty plus and minus */
    
      $comboQtyplus = Yii::$app->getRequest()->getQueryParam('comboqtyplus');              
      $comboQtyminus = Yii::$app->getRequest()->getQueryParam('comboqtymin'); 
	   	 	 	
	   $user_id = Yii::$app->user->id; 
	  
	     	     	    
       $query = Workorderpopup::find()
        ->joinWith('product') 		
		->select([
				  Workorderpopup::tablename().'.*',Product::tablename().'.*'
				])					
		->where(Workorderpopup::tablename().'.workorder_id='.$id)		
		->one();
		
		
	
	
	$dealCount=1;
	$productdetail = array();
	$productdetailmin = array();
	$comboProductType = '';
      if(!empty($query->offertype)) {
     $offertypetext = $query->offertext1;
     $offertitle = $query->offerTitle;
     $offertypevalue1 = $query->customOfferSelect1;
     $comboProductType = $query->comboProductType;     
     $offertypevalue2 = $query->customOfferSelect2;
     $offertypevalue3 = $query->customOfferSelect3;
     $offertypevalue4 = $query->customOfferSelect4;
     $offertype = $query->offertype;
     $offerwas = $query->offerwas;
     $offernow = $query->offernow;
     /* Check Combo Product Price */
     $comboActualPrice = '';
     $comboPDeatils = array();
     // combo arrtibute
				$comboQty = '';
				$comboActual = '';
				$combodiscount = '';
				$comboEffected = '';
		// Actual price 
		       
     if($comboProductType!=''){	
		 if($query->comboProductType==2){	
			$comboProduct = Product::findOne(['id'=>$query->product_id_combo]);		
		 }elseif($query->comboProductType==1){
			$comboProduct = Product::findOne(['id'=>$query->product_id]);		
		 }
			$comboActualPrice = $comboProduct->price;			 		
		}
		
	
		foreach ($query['product'] as $queryval ){
			
				$productdetail['id'] = $queryval->id;
				$productdetail['name'] = $queryval->name;
				$productdetail['price'] = $queryval->price;
				$productdetail['sku'] = $queryval->sku;
				$productdetail['image'] = $queryval->image;
				$productdetail['description'] = $queryval->description;
				$productdetail['status'] = $queryval->status;
				$productdetail['is_online'] = $queryval->is_online;
				$productdetail['store_id'] = $queryval->store_id;				
	     }
	     
	   
	    
	     $branddetails = Profile::find()->select(['brandName','brandLogo','brandDescription'])
	     ->where(['user_id'=> $productdetail['store_id']])->one();
	    
	     
	    /* using the add qty */
	     if($offertype==3 && $offerwas==0 && $offernow==0 && $qty!=''){
			 
			if($offertypetext==1){
				$oldqty = $offertypevalue1+$offertypevalue2;
				$newqty = $qty;
				$actualqty = intval($newqty/$oldqty);
				$dealtypeqty = $actualqty+1;
				$totalqty = $dealtypeqty*$oldqty;
				$qty = $totalqty; 				
				$actualprice = $qty*$productdetail['price'];
				$discountprice = $dealtypeqty*$productdetail['price'];
				$effectedtprice = $actualprice-$discountprice;
				$dealCount=$qty/$oldqty;
			}elseif($offertypetext==2){
				$qty = $qty+1;
				$actualprice = $qty*$productdetail['price'];				
				$discountprice =  ($offertypevalue1 / 100)*$actualprice;				
				$effectedtprice = $actualprice-$discountprice;
				$dealCount=$qty;	
				
			}elseif($offertypetext==3){
				$qty = $qty+1;
				$actualprice = $qty*$productdetail['price'];				
				$discountprice =  ($offertypevalue1 / 100)*$actualprice;				
				$effectedtprice = $actualprice-$discountprice;
				$dealCount=$qty;	
				
			}elseif($offertypetext==4){
				$qty = $qty+1;				
				$actualprice = $qty*$productdetail['price'];				 
				$discountprice =  ($offertypevalue2 / 100)*$actualprice;					
				$effectedtprice = $actualprice-$discountprice;
						$dealCount=$qty;	
				
			}elseif($offertypetext==5){
				$oldqty = $offertypevalue1;
				$discountval = $offertypevalue2; 
				$qty=$qty+ $oldqty;
				$newqty = $qty;		
				$actualqty3 = intval($newqty/$oldqty)*$offertypevalue3;
					
			    $actualqty1 = intval($newqty/$oldqty)*$discountval;						
			    $actualqty2 = intval($newqty%$oldqty)*$productdetail['price'];		
			    $productdetail['price'];											
				$actualprice = $qty*$productdetail['price'];
				$effectedtprice = $actualqty1+$actualqty2;
				$discountprice = $actualprice-$effectedtprice;
				
				/* combo qty default */
				$comboQty = $actualqty3; 								 
				$comboActual = $comboQty*$comboActualPrice;
				$comboEffected = $offertypevalue4*$comboQty;				
				$combodiscount = $comboActual-$comboEffected;
				$dealCount=$qty/$oldqty;
			
			}else{
				$qty = '';
				$actualprice = '';
				$discountprice = '';
				$effectedtprice = '';
			}
		
		 }
		 
		
		
		 
		  /* using the minus qty */
	     if($offertype==3 && $offerwas==0 && $offernow==0 && $qtymin!=''){
			 

			 
			if($offertypetext==1){
				$oldqty = $offertypevalue1+$offertypevalue2;
				$newqty = $qtymin-1;
				if($oldqty < $newqty ){
				$actualqty = intval($newqty/$oldqty);				
				$dealtypeqty = $actualqty;
				$totalqty = $dealtypeqty*$oldqty;
				$qty = $totalqty; 	
				$dealCount=$qty/$oldqty;			
				$actualprice = $qty*$productdetail['price'];
				$discountprice = $dealtypeqty*$productdetail['price'];
				$effectedtprice = $actualprice-$discountprice;
				}else{
					$productdetailmin['message']='Not Found';
					$productdetailmin['statusCode']=204;
					return $productdetailmin;
				 
				}
			}elseif($offertypetext==2){
						 				
				$qty = $qtymin-1;
				$actualprice = $qty*$productdetail['price'];				
				$discountprice =  ($offertypevalue1 / 100)*$actualprice;				
				$effectedtprice = $actualprice-$discountprice;
				$dealCount=$qty;	
				
			}elseif($offertypetext==3){				
				$oldqty = $offertypevalue2;		
				$qty = $qtymin-1;
				$actualprice = $qty*$productdetail['price'];
				if($qty < $oldqty){
				$discountprice =  0;	
				$effectedtprice = $qty*$productdetail['price'];
				}else {				 
				$discountprice =  ($offertypevalue1 / 100)*$actualprice;	
				$effectedtprice = $actualprice-$discountprice;
				}				
				$dealCount=$qty;		
									
			}elseif($offertypetext==4){	
				$oldqty = $offertypevalue1;						
				$qty = $qtymin;				
				$actualprice = $qty*$productdetail['price'];
				if($qty < $oldqty){
				$discountprice = 0;
				$effectedtprice = $qty*$productdetail['price'];
				}else {				 
				$discountprice =  ($offertypevalue2 / 100)*$actualprice;
				$effectedtprice = $actualprice-$discountprice;	
				}				
				$dealCount=$qty;	
				
			}elseif($offertypetext==5){
				$oldqty = $offertypevalue1;				
				$discountval = $offertypevalue2; 
				
				$newqty = $qtymin -$oldqty;				
		        $qty = $newqty; 
		        
		        $actualqty3 = intval($newqty/$oldqty)*$offertypevalue3;						
			    $actualqty1 = intval($newqty/$oldqty)*$discountval;				  				
			    $actualqty2 = intval($newqty%$oldqty)*$productdetail['price'];	 	
			    $productdetail['price'];											
				$actualprice = $newqty*$productdetail['price'];				
				$effectedtprice = $actualqty1+$actualqty2;				
				$discountprice = $actualprice-$effectedtprice;
				/* combo qty default */
				$comboQty = $actualqty3; 								 
				$comboActual = $comboQty*$comboActualPrice;
				$comboEffected = $offertypevalue4*$comboQty;				
				$combodiscount = $comboActual-$comboEffected;
				
				$dealCount=$qty/$oldqty;
				
			}else{
				$qty = '';
				$actualprice = '';
				$discountprice = '';
				$effectedtprice = '';
			}
		
		 }
		 
		 
		 
		 /* when qty empty and offer type 3 */
		  if($offertype==3 && $offerwas==0 && $offernow==0 && $qty=='' && $qtymin=='' && $comboQtyminus=='' && $comboQtyplus==''){
			
			
			if($offertypetext==1){
				$qty = $offertypevalue1+$offertypevalue2;
				$actualprice = $qty*$productdetail['price'];				
				$discountprice = $offertypevalue2*$productdetail['price'];						
				$effectedtprice = $offertypevalue1*$productdetail['price'];
				
			}elseif($offertypetext==2){
				$qty = 1;
				$actualprice = $qty*$productdetail['price'];				
				$discountprice =  ($offertypevalue1 / 100)*$productdetail['price'];				
				$effectedtprice = $productdetail['price']-$discountprice;
				
			}elseif($offertypetext==3){
				$qty = $offertypevalue2;
				$actualprice = $qty*$productdetail['price'];				
				$discountprice =  ($offertypevalue1 / 100)*$actualprice;				
				$effectedtprice = $actualprice-$discountprice;
				
			}elseif($offertypetext==4){
				$qty = $offertypevalue1;				
				$actualprice = $qty*$productdetail['price'];				 
				$discountprice =  ($offertypevalue2 / 100)*$actualprice;					
				$effectedtprice = $actualprice-$discountprice;
				
			}elseif($offertypetext==5){
				$qty = $offertypevalue1;
				$comboQty = $offertypevalue3; 				
				$actualprice = $offertypevalue1*$productdetail['price'];						 
				$comboActual = $comboQty*$comboActualPrice;
				$comboEffected = $offertypevalue4;
				$combodiscount = $comboActual-$comboEffected;
				
				$effectedtprice = $offertypevalue2;						
				$discountprice = $actualprice-$effectedtprice;
						
				//$effectedtprice = $actualprice-$discountprice;
				
			
			}else{
				
				$qty = '';
				$actualprice = '';
				$discountprice = '';
				$effectedtprice = '';
				
			}
		
		 }
		 
		 
		 
		 /* when qty empty and offer type 1*/ 
		  if($offertype==1 && $offerwas!='' && $offernow!='' && $qty==''){
			$qty = 1;
			$actualprice = $offerwas;
			$discountprice = $offerwas-$offernow;
			$effectedtprice = $offernow;		  
			$dealCount=$qty;
		  }
		  
		   /* when qty plus not empty and offer type 1*/ 
		  if($offertype==1 && $offerwas!='' && $offernow!='' && $qty!=''){
			$qty = $qty+1;
			$actualprice = $offerwas*$qty;
			$discountprice = ($offerwas*$qty)-($offernow*$qty);
			$effectedtprice = $offernow*$qty;	
			$dealCount=$qty;	  
		  }
		    /* when qty minus not empty and offer type 1*/ 
		  if($offertype==1 && $offerwas!='' && $offernow!='' && $qtymin!=''){
			$qty = $qtymin-1;
			$actualprice = $offerwas*$qty;
			$discountprice = ($offerwas*$qty)-($offernow*$qty);
			$effectedtprice = $offernow*$qty;		  
			$dealCount=$qty;
		  }
		 $productdetail['offertitle'] = $offertitle;	
		 $productdetail['qty'] = $qty;		 
		 $productdetail['dealCount'] = $dealCount;		 
		 $productdetail['checkcombo'] = $comboProductType;		 
		 $productdetail['actualprice'] = $actualprice;
		 $productdetail['discountprice'] = $discountprice;
		 $productdetail['effectedtprice'] = $effectedtprice;
		 $productdetail['brandName'] =   $branddetails->brandName;
		 $productdetail['brandDescription'] = $branddetails->brandDescription;
		 
		 /*  combo product section */
		 
		 if($comboQty!=''){
			 /*combo product value */
				$comboPDeatils['id'] = $comboProduct->id;
				$comboPDeatils['name'] = $comboProduct->name;
				$comboPDeatils['price'] = $comboProduct->price;
				$comboPDeatils['sku'] = $comboProduct->sku;
				$comboPDeatils['image'] = $comboProduct->image;
				$comboPDeatils['description'] = $comboProduct->description;
				$comboPDeatils['status'] = $comboProduct->status;
				$comboPDeatils['is_online'] = $comboProduct->is_online;
				$comboPDeatils['store_id'] = $comboProduct->store_id;
				
				$comboBranddetails = Profile::find()->select(['brandName','brandLogo','brandDescription'])->where(['user_id'=> $comboPDeatils['store_id']])->one();
				$comboPDeatils['brandName'] =  $comboBranddetails->brandName;
				$comboPDeatils['brandDescription'] = $comboBranddetails->brandDescription;
      	 
			 $comboPDeatils['qty'] = $comboQty;
			
			 if($comboActual!=''){
				 $comboPDeatils['actualprice'] = $comboActual;
			 }
			  if($combodiscount!=''){
			 $comboPDeatils['discountprice'] = $combodiscount;
			 }
			  if($comboEffected!=''){
			 $comboPDeatils['effectedtprice'] = $comboEffected;
			}
		 }
		// return $comboPDeatils; die; 
		 
		if(!empty($branddetails->brandLogo)){
		    $productdetail['brandLogo'] = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$branddetails->brandLogo);
	    }else{
			 $productdetail['brandLogo'] = null;
		}
		
		/* combo product barnd details */
		if(!empty($comboBranddetails->brandLogo)){
		    $comboPDeatils['brandLogo'] = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$comboBranddetails->brandLogo);
	    }
		
		$result = array();
		$result = $productdetail;
		$result['comboProduct'] = $comboPDeatils;
		 
		 return $this->render('index', [
            'dataProvider' => $result,            
        ]);
		
		//return $result;
      }else{
		  		  
		   return $this->render('index', [
            'dataProvider' => $productdetail,            
        ]);
		 
		// $productdetail['message'] ='No result found';
		 //return $productdetail;
	
	 }		
    }
    
		
    
     protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
 
	
}
