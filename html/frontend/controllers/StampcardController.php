<?php
namespace frontend\controllers;
use Yii;
use yii\db\Query;
use common\models\Pass;
use common\models\PassSearch;
use common\models\User;
use common\models\Profile;
use common\models\Workorderpopup;
use common\models\Workorders;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\Offertrack;
use common\models\Loyalty;
//use common\models\Offertrack;



class StampcardController extends Controller
{
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }
	    /* To get all cards*/
	    
   public function actionIndex()
   {
		
		 $searchModel = new PassSearch();
		 $dataProvider = $searchModel->stampcardsearch(Yii::$app->request->queryParams);
         
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
            
   }    
       
	/* public function actionView($id)
	{				
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
	}*/
	
   /* find customer */
	protected function findModel($id)
    {
        if (($model = Id::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGetstampcardview(){ 
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$workorderpopup= new Workorderpopup();
	$passvalue =   Pass:: find()
				->joinWith("workorders")
				->joinWith("workorderpopup")
				->joinWith("profile")
				->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_id' => $_POST['workorderid']])
				->one();
			$countImage = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("images/countimage.png");

			$html='<div class="modal-dialog prod_popup_had"><div class="modal-content model-content-stamp-card"><div class="poup_hed_boder"><div class="modal-header "><h4 class="modal-title modal-title-stamp-card"><b>MY STAMP CARD</b><p class="close-stamp-card" data-dismiss="modal">X</p></h4><h1 class="h1-modal-title">'.$passvalue->profile->brandName.'</h1></div><div class="col-md-12"><div class="part_val"><b>'.$passvalue->profile->brandAddress.'</b> </div></div>
				
				</div>
				<div class="prod_popup"><div class="modal-body">
			<div class="col-md-12">
            <div class="row" >
				<div class="pup_img"><img class="pop-up-image" src="'.$passvalue->workorderpopup->photo.'" /></div>
                 <div class="pup_img_logo">
                 <img class="pop-up-logo"  src="'.$passvalue->workorderpopup->logo.'" /></div>
				 </div></div><div class="col-md-12 grey-bottom" >
				<div class="row">
				<div class="pupup_doller" style="text-align: left;  color:#08afb5;"><b>
				<h1 style="color:#08afb5 !important">'.$passvalue->offerText.'</h1></b></div>';
				
				$html .='<div class="read_popup"  style="text-align: right;">
					<img src="'.Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/imgpsh_fullsize1.png").'" style="height: 16px; margin: 0 5px 0 0; width: 16px;" ><b><a  style="color:black;" href="javascript:void(0)"  data-target="#mygetModalread" data-toggle="modal" class="small-box-foote1 read_popupopen" rel="'.$_POST['workorderid'].'">Read Terms </a></b></div>
				</div>
				
			</div>
			 <div class="col-md-12 pop-bt-cl">
			 <div class="pop-up-image_bar_code ">';
			 $checktotalNo = Offertrack::find()->select(['user_id','workorder_id','usedNo','qty'])->where(['workorder_id' => $_POST['workorderid'] ,'user_id' =>Yii::$app->user->id])->orderBy(['id' => SORT_DESC])->one();
			
			
			 $tatalstampcards = $passvalue->workorderpopup->customOfferSelect1 + $passvalue->workorderpopup->customOfferSelect2;
			 for($i=1; $i<=$tatalstampcards; $i++){
			 if($i>$passvalue->workorderpopup->customOfferSelect1){
				 if(isset($checktotalNo->usedNo) && $i<=$checktotalNo->usedNo){
					   $style = "background-color:yellow1;width:45px;height:40px;padding: 9px 0 0;";
					   $html .='<p class="stm-buy img-circle" style='.$style.' rel='.$i.'-'.$_POST['workorderid'].'><img style="height: 40px;width: 45px;" src="'.$countImage.'">';
					    //$html .= $i;
					    $html .='</p>';
				 }else{
				 $style = "background-color:white;width:45px;color:#08afb5;height:40px;padding:9px;";
				   $html .='<p class="stm-buy img-circle" style='.$style.' rel='.$i.'-'.$_POST['workorderid'].'>';
					  $html .=  "Free";
					    $html .='</p>';
			  }
				
			 }else{
				 if(isset($checktotalNo->usedNo) && $i<=$checktotalNo->usedNo){
					   $style = "background-color:yellow1;width:45px;height:40px;padding: 9px 0 0;";
					   $html .='<p class="stm-buy img-circle" style='.$style.' rel='.$i.'-'.$_POST['workorderid'].'><img style="height: 40px;width: 45px;" src="'.$countImage.'">';
					    //$html .= $i;
					    $html .='</p>';
				}else
				{
				 $style = "background-color:#08afb5;width:45px;color:white;height:40px;padding:9px 0 0;";
					   $html .='<p class="stm-buy img-circle" style='.$style.' rel='.$i.'-'.$_POST['workorderid'].'>';
					    $html .= $i;
					    $html .='</p>';
			 }
			  
		} 
             //$html .='<p class="stm-buy img-circle" style='.$style.' rel='.$i.'-'.$_POST['workorderid'].'>';
             //if($i>$passvalue->workorderpopup->customOfferSelect1){
				// $html .=  "free";
				 
			// }else{
				// $html .= $i;
				// $html .= '<img  width="30px" src="'.$countImage.'">';
				//}
             //$html .='</p>';
            
	}
           $html .='</div></div></div></div><div class="modal-footer"></div>
      </div></div>';
  return 	$html;	
		
	}
	
    
    public function actionGetreadmore(){ 
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	
	
	$passvalue =  Pass:: findOne(['user_id' => Yii::$app->user->id,'workorder_id'=>$_POST['workorderid']]);
		$html='<div class="modal-dialog model-dialog-stamp-readmore">
		
		  <!-- Modal content-->
		  <div class="modal-content model-content-readmore">
			<div class="modal-header model-header-readmore">
			 <p class="closereadmore" data-dismiss="modal">X</p>
			  <h4 class="modal-title" style="text-align:center;"> <b> Read More </b>  </h4>
			</div>
			<div class="modal-body overflow-text">'.$passvalue->readTerms.'
				     
			</div>
			<div class="modal-footer">
			  
			</div>
		  </div>
		  
		</div>';
		return 	$html;		
		
	}
	
	 public function actionGetstampcardbuyinapp(){ 
		 $user_id = Yii::$app->user->id; 
		 $model= new Offertrack();
	
		//$model->load(Yii::$app->request->post()); 
		
		$model->created_by=$user_id;
		$model->updated_by=$user_id;
		$model->user_id=$user_id;
		$model->usedNo=Yii::$app->request->post()['usedNo'];
		$model->workorder_id=Yii::$app->request->post()['workorder_id'];
		$model->pin=Yii::$app->request->post()['pin'];
		
		if($model->validate()){
										
				/* check pin is validate form Loyalty model by workorder id  */
				$workorder = new Workorders();
				$loyalty= new Loyalty();
				
				$checkpin= Workorders:: find()->select(
										[Workorders::tableName().'.workorderpartner',Loyalty::tableName().'.user_id',Loyalty::tableName().'.loyalty_pin'])
										->leftJoin(Loyalty::tableName(), Loyalty::tableName().'.user_id = '.Workorders::tableName(). '.workorderpartner')
										->where([Workorders::tableName().'.id' => $model->workorder_id ,Loyalty::tableName().'.loyalty_pin' => $model->pin, Loyalty::tableName().'.status' =>1,Workorders::tableName().'.status' =>1])
										->orderBy([Workorders::tableName().'.id' => SORT_DESC])->asArray()->one();
																					
						/* is pin is valid */				
					if($checkpin){
						/* check total number from workorder popup model */						
						$checktotalNo = Workorderpopup::find()->select(['customOfferSelect1','customOfferSelect2'])->where(['workorder_id' => $model->workorder_id])->one();
						$paidoffer = $checktotalNo->customOfferSelect1; 
						$freeoffer = $checktotalNo->customOfferSelect2;
						 $total = $paidoffer+$freeoffer;  
						/* used number from Offertrack model */
						$usedtotal = Offertrack::find()
									->where(['workorder_id' => $model->workorder_id])->count();						
						
						
							/* if free some numbers */
						
							
						if($total>=$model->usedNo) {
							$checkusedNo = Offertrack::find()->where(['workorder_id' => $model->workorder_id, 'user_id' => $user_id])->andWhere('usedNo >='.$model->usedNo)->orderBy('id desc')->one();
								$getlast = Offertrack::find()->where(['workorder_id' => $model->workorder_id, 'user_id' => $user_id])->orderBy('id desc')->one();
								
								if($checkusedNo){
									
									///return 'you have already used this number, try another number';
									return "you have already used this number, try another number";
									
								}else{
										if($getlast){
											$lastqty  = $getlast->usedNo;
											$model->qty = $model->usedNo-$lastqty ;									  
											$model->save();
											return 2;											
										
										}else{									
										$model->qty = $model->usedNo;									  
										$model->save();
										return 1;
									}
							} 
						}else{
							//return 'you have used your all paid and free offer';
							 return 3;
						}
						
					}else{
					  //return 'invalid pin or deal expire';
					  
					  
					//  return "That PIN does not match your registered 6-digit PIN. Please try again.";
					  return 4;
					 
					}
					
			}else{
				
				//return $error = $model->errors['pin'][0];
				return 5;

				
			
			}
	}
	
	
	/******************stamp card count **************/
	/*public function actionGetstampcardcount(){ 
		
		$checktotalNo = Offertrack::find()->select(['user_id','workorder_id','usedNo','qty'])->where(['workorder_id' => Yii::$app->request->post()['workorder_id'] ,'user_id' =>Yii::$app->user->id])->orderBy(['id' => SORT_DESC])->one();
		 $checktotalNo->usedNo;
	
		 
			 for($i=1; $i<= $checktotalNo->usedNo; $i++){
			 
             $html ='<p class="stm-buy" style="background-color:yellow;">'.$i.'-'.Yii::$app->request->post()['workorder_id'].'</p>';
            
			}
         return  $html;
		
		
	}*/
	
	 public function actionGetstampcardcount(){ 
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$workorderpopup= new Workorderpopup();
	$passvalue =   Pass:: find()
				->joinWith("workorders")
				->joinWith("workorderpopup")
				->joinWith("profile")
				->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_id' => $_POST['workorderid']])
				->one();
		$countImage = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("images/countimage.png");
			$html='<div class="modal-dialog prod_popup_had"><div class="modal-content model-content-stamp-card"><div class="poup_hed_boder"><div class="modal-header "><h4 class="modal-title modal-title-stamp-card"><b>MY STAMP CARD</b><p class="close-stamp-card" data-dismiss="modal">X</p></h4><h1 class="h1-modal-title">'.$passvalue->profile->brandName.'</h1></div><div class="col-md-12"><div class="part_val"><b>'.$passvalue->profile->brandAddress.'</b> </div></div>
				
				</div>
				<div class="prod_popup"><div class="modal-body">
			<div class="col-md-12">
            <div class="row" >
				<div class="pup_img"><img class="pop-up-image" style="width:279px; height:200px;" src="'.$passvalue->workorderpopup->photo.'" /></div>
                 <div class="pup_img_logo">
                 <img class="pop-up-logo" style="width:100px; height:70px;" src="'.$passvalue->workorderpopup->logo.'" /></div>
				 </div></div><div class="col-md-12 grey-bottom" >
				<div class="row">
				<div class="pupup_doller" style="text-align: left; float:left; color:#08afb5;"><b>
				<h1 style="color:#08afb5 !important;width: 100% !important;">'.$passvalue->offerText.'</h1></b></div>';
				
				$html .='<div class="read_popup"  style="text-align: right;">
					<img src="'.Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/imgpsh_fullsize1.png").'" style="height: 16px; margin: 0 5px 0 0; width: 16px;" ><b><a  style="color:black;" href="javascript:void(0)"  data-target="#mygetModalread" data-toggle="modal" class="small-box-foote1 read_popupopen" rel="'.$_POST['workorderid'].'">Read More </a></b></div>
				</div>
				
			</div>
			 <div class="col-md-12">
			 <div class="pop-up-image_bar_code ">';
			 $checktotalNo = Offertrack::find()->select(['user_id','workorder_id','usedNo','qty'])->where(['workorder_id' => $_POST['workorderid'] ,'user_id' =>Yii::$app->user->id])->orderBy(['id' => SORT_DESC])->one();
			
			
			 $tatalstampcards = $passvalue->workorderpopup->customOfferSelect1 + $passvalue->workorderpopup->customOfferSelect2;
			 for($i=1; $i<=$tatalstampcards; $i++){
			 if($i>$passvalue->workorderpopup->customOfferSelect1){
				if(isset($checktotalNo->usedNo) && $i<=$checktotalNo->usedNo){
					$style = "background-color:yellow1;width:45px;height:40px;";
					   $html .='<p class="stm-buy img-circle" style='.$style.' rel='.$i.'-'.$_POST['workorderid'].'><img  style="height: 40px;width: 45px;" src="'.$countImage.'">';
					    //$html .= $i;
					    $html .='</p>';
				}else{
				 $style = "background-color:white;color:#08afb5;width:45px;height:40px;padding-top:9px;";
				   $html .='<p class="stm-buy img-circle" style='.$style.' rel='.$i.'-'.$_POST['workorderid'].'>';
					  $html .=  "Free";
					    $html .='</p>';
			    }
				
			 }else{
				 if(isset($checktotalNo->usedNo) && $i<=$checktotalNo->usedNo){
					  $style = "background-color:yellow1;width:45px;height:40px;";
					   $html .='<p class="stm-buy img-circle" style='.$style.' rel='.$i.'-'.$_POST['workorderid'].'><img  style="height: 40px;width: 45px;" src="'.$countImage.'">';
					    //$html .= $i;
					    $html .='</p>';
				}else {
				 $style = "background-color:#08afb5;width:45px;height:40px;color:white;padding-top:9px;";
					   $html .='<p class="stm-buy img-circle" style='.$style.' rel='.$i.'-'.$_POST['workorderid'].'>';
					    $html .= $i;
					    $html .='</p>';
			 }
				 } 
           //  $html .='<p class="stm-buy" style='.$style.' rel='.$i.'-'.$_POST['workorderid'].'>'.$i.'</p>';
            
			}
           $html .='</div></div></div></div><div class="modal-footer"></div>
      </div></div>';
  return 	$html;	
		
	}
	/********** end here *********************/
}
