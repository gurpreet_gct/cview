<?php
namespace frontend\controllers;
use Yii;
use yii\db\Query;
use common\models\Pass;
use common\models\PassSearch;
use common\models\User;
use common\models\Profile;
use common\models\Workorderpopup;
use common\models\Workorders;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class RedeemedController extends Controller
{
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }
	    /* To get all cards*/
	    
   public function actionIndex()
   {
		
		  $searchModel = new PassSearch();
		 $dataProvider = $searchModel->redeemedsearch(Yii::$app->request->queryParams);
         
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
            
   }    
       
	/* public function actionView($id)
	{				
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
	}*/
	
   /* find customer */
	protected function findModel($id)
    {
        if (($model = Id::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGetredeemedpassview(){ 
	
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$workorderpopup= new Workorderpopup();
	$passvalue =   Pass:: find()
				->joinWith("workorders")
				->joinWith("workorderpopup")
				->joinWith("profile")
				->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_id' => $_POST['workorderid']])
				->one();
		
			$html='<div class="modal-dialog prod_popup_had"><div class="modal-content" style="background-color:#F3F3F3"><div class="poup_hed_boder"><div class="modal-header "><h4 class="modal-title model-title-redeemed-pass"><b>My  REDEEMED PASS</b><p class="close-redeemed-pass" data-dismiss="modal">X</p></h4><h1 class="h1-modal-title">'.$passvalue->offerTitle.'</h1></div><div class="col-md-12"><div class="part_val"><b>'.$passvalue->profile->brandName.'</b> </div></div>
			
				</div>
				<div class="prod_popup"><div class="modal-body">
			<div class="col-md-12">
            <div class="row" >
				<div class="pup_img"><img class="pop-up-image"  src="'.$passvalue->workorderpopup->photo.'" /></div>
                 <div class="pup_img_logo">
                 <img class="pop-up-logo"  src="'.$passvalue->workorderpopup->logo.'" /></div>
				 </div></div><div class="col-md-12 grey-bottom" >
				<div class="row">
				<div class="pupup_doller" style="text-align: left; float:left; color:#08afb5;"><b>';
				if($passvalue->offernow!=0) { 
					$html .='$'.$passvalue->offernow;
				}else{
					$html .='<h1>'.$passvalue->offerText.'</h1>';
					}
				$html .='</b></div>';
				if($passvalue->offernow!=0) {
					$html .='<div class="col-md-1 text_though">$'.$passvalue->offerwas;
					$html .='</div>';
				}
				$html .='<div class="read_popup"  style="text-align: right;">
					<img src="'.Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/imgpsh_fullsize1.png").'" style="height: 16px; margin: 0 5px 0 0; width: 16px;" ><b><a  style="color:black;" href="javascript:void(0)"  data-target="#mygetModalread" data-toggle="modal" class="small-box-foote1 read_popupopen" rel="'.$_POST['workorderid'].'">Read More </a></b></div>
				</div>
				
			</div>
			 <div class="col-md-12 pop-bt-cl">
			 <div class="pop-up-image_bar_code deal-bar-code">
             <p>'.$passvalue->sspMoreinfo.'</p>
          </div>
				<div class="pop-up-image_bar_code bar-code-image">
             <p><img class="pop-up-image" src="'.$passvalue->workorderpopup->passUrl.'" /> </p>
          </div>
          
          </div>
		</div></div><div class="modal-footer"></div>
      </div></div>';
  return 	$html;	
		
	}
	
    
    public function actionGetreadmore(){ 
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	
	
	$passvalue =  Pass:: findOne(['user_id' => Yii::$app->user->id,'workorder_id'=>$_POST['workorderid']]);
		$html='<div class="modal-dialog model-dialog-pass-readmore">
		
		  <!-- Modal content-->
		  <div class="modal-content model-content-pass-readmore">
			<div class="modal-header model-header-pass-readmore">
			   <p class="closereadmore" data-dismiss="modal">X</p>
			  <h4 class="modal-title" style="text-align:center"> <b> Read More </b> </h4>
			</div>
			<div class="modal-body overflow-text">'.$passvalue->readTerms.'
				     
			</div>
			<div class="modal-footer">
			  
			</div>
		  </div>
		  
		</div>';
		return 	$html;		
		
	}
	
}
