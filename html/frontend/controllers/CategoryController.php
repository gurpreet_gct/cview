<?php
namespace frontend\controllers;

use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\UserCategory;
use frontend\models\Category;
use common\models\Categories;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class CategoryController extends Controller
{  
	
	 public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Store models.
     * @return mixed
     */
       
      
	
    public function actionIndex()    
    {	
		
		$selectedcat = $this->getCategoryTree(Yii::$app->user->identity->id);
				if(count($selectedcat>0)){
					$id = Yii::$app->user->identity->id;
				}else{
					$id = 0;	
				}
		
		if(isset(Yii::$app->request->post()['_csrf']))
			{				
				UserCategory::deleteAll('user_id ='.Yii::$app->user->identity->id);	
				
				if(isset(Yii::$app->request->post()['categoryTree'])) {
					$categoryTree=Yii::$app->request->post()['categoryTree'];	
					unset($categoryTree[0]);
					$catTree=[];
					foreach($categoryTree as $key=>$value)
						{
								array_push($catTree,array("category_id"=>$value,"user_id"=>Yii::$app->user->identity->id,"status"=>1,'created_at'=>time(),'updated_at'=>time()));
						}
					   if(count($catTree))
								Yii::$app->db->createCommand()->batchInsert(UserCategory::tableName(),array_keys($catTree[0]), $catTree)->execute();
			
				
			}
			
					return $this->render('index', [
           
				'categoryTree' => $this->getCategoryTree($id),

				]);
						
			 }else {
				 
				
				
				
						  
				return $this->render('index', [
           
				'categoryTree' => $this->getCategoryTree($id),

				]);
		}
    }
    
    
protected function getCategoryTree($user_id)
	//public function actionTree()
	{
		
		$requiredResult=array();
	  
		$query = new Query;
		$query	->select(['tbl_categories.id as id','name as title','tbl_categories.id as value',  '(lvl+2) as level','lft','ifnull(status,0) as checked'])  
				->from('tbl_categories')
				->leftJoin('tbl_userCategory', 'tbl_userCategory.category_id = tbl_categories.id and tbl_userCategory.user_id='.$user_id)
				//->limit(2)
				->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC]); 	
		$command = $query->createCommand();
		$data = $command->queryAll();
			$requiredResult=array();
		$oldLevel=2;
		$levelKeys=array();
	
		foreach($data as $key=>$value)
		{
			//$value['path']="root/";
			$key=$value['id'];
            $value['title']=htmlspecialchars_decode($value['title']);
           $value['has_children']=false;
           $value['children']=[];
		   $value['checked']=$value['checked']==1?true:false;
		   
			if($value['level']==2)
			{
		//	$value['path']=$value['path'].$value['name'];
			$requiredResult[$key]=$value;
				
			}
			else
			{	
				$array_ptr=&$requiredResult;
				
				for($i=2; $i<$value['level']; $i++)
				{
					//$value['path']= $array_ptr[ strval($levelKeys[$i])]['path'];						 
					 $array_ptr[ strval($levelKeys[$i]) ]['has_children']=true;
					 $array_ptr = &$array_ptr[ strval($levelKeys[$i]) ]['children'];
					 
					 
				}
			//	$value['path']=$value['path']."/".$value['name'];
				$array_ptr[$key]=$value;
			//print_r($requiredResult);die;
			}            
    
			$levelKeys[$value['level']]=$value['id'];			
		}	
		
		$this->recursiveRemoval($requiredResult,'children');
      $requiredResult=is_array($requiredResult)?array_values($requiredResult):$requiredResult;
	  	$data=array('id'=>0,'value'=>0,'title'=>'Root','level'=>1,'has_children'=>true,'children'=>$requiredResult);
		return (json_encode($data));

	}
	
	
	  public $i=0;  
protected function recursiveRemoval(&$array, $val)
{
    if(is_array($array))
    {
        foreach($array as $key=>&$arrayElement)
        {
            if(is_array($arrayElement))
            {
                if($key===$val)
                { 
            $this->i++;
            
             if($this->i==2)
             {//    print_r($arrayElement);echo $key;
         }
            $arrayElement=array_values($arrayElement);
/*if($this->i==2)                   
{ 
echo ($key===$val);
   echo $key;
echo $val;

                    print_r($arrayElement);
                    
die;}*/
            }
                $this->recursiveRemoval($arrayElement, $val);
            }
            else
            {
                if($key == $val)
                {
                    //print_r($arrayElement);
                }
            }
        }
    }
}
    
    public function actionUpdate($id)
	{
		
		$user_id=Yii::$app->user->id;
		$userCategory=UserCategory::findOne(['user_id'=>$user_id,'category_id'=>$id]);
		if($userCategory)
		{
			$userCategory->status=1;
			$userCategory->save();
		}
		else
		{
			$userCategory=new UserCategory();
			$userCategory->user_id=$user_id;
			$userCategory->category_id=$id;
			$userCategory->status=1;
			$userCategory->save();
		}
		return ["message"=>"Category added successfully."];
	}
    public function actionDelete($id)
    {
        
        $user_id=Yii::$app->user->id;
		$userCategory=UserCategory::findOne(['user_id'=>$user_id,'category_id'=>$id,'status'=>1]);
		if($userCategory)
		{
			$userCategory->status=0;
			$userCategory->save();
			return ["message"=>"Category removed successfully."];
		}
		else
			return ["message"=>"Category already removed."];
		
    }
    
    public function actionUpdatebatch()
    {
        if(isset(Yii::$app->request->post()['categories']))
        {
        $user_id=Yii::$app->user->id;
        $userCategory=UserCategory::find()->where(['user_id'=>$user_id])->select('category_id as id,status as isSelected')->asArray()->all();
        $rs=(json_decode(Yii::$app->request->post()['categories'],true));      
         $newCategory=ArrayHelper::map($rs, 'id', 'isSelected');
         $oldCategory=ArrayHelper::map($userCategory, 'id', 'isSelected');
         /* All element having value 0 for isSelected*/
         $newZero=array_keys($newCategory,0);
         $newZero=(array_fill_keys(array_values($newZero),0));
         /* All element having value 1 for isSelected*/
         $newOne=array_keys($newCategory,1);
         $newOne=(array_fill_keys(array_values($newOne),1));
         
         /* All element having value 0 for isSelected*/
         $oldZero=array_keys($oldCategory,0);
         $oldZero=(array_fill_keys(array_values($oldZero),0));
         /* All element having value 1 for isSelected*/
         $oldOne=array_keys($oldCategory,1);
         $oldOne=(array_fill_keys(array_values($oldOne),1));
         
         /* All category need to be set zero in db */
         $catCommon=array_intersect_key($oldCategory,$newZero);         
         $catToBeZero=array_diff_key($catCommon,$oldZero);
         
         /* All category need to be insert with 1 in db */
         $catToBeAdd=array_diff_key($newOne,$oldCategory);
         
         /* All category need to be update with 1 in db */
         $catToBeOne=array_intersect_key($newOne,$oldZero);
        
       if(count($catToBeZero)){  
           $updateZero=sprintf("update %s set status=0, updated_at=%s where user_id=%s and category_id in (%s)",UserCategory::getTableSchema()->fullName,time(),$user_id,implode(',',array_keys($catToBeZero)));
            Yii::$app->db->createCommand($updateZero)->execute();
       }
        if(count($catToBeOne)){
            $updateOne=sprintf("update %s set status=1, updated_at=%s where user_id=%s and category_id in (%s)",UserCategory::getTableSchema()->fullName,time(),$user_id,implode(',',array_keys($catToBeOne)));
            Yii::$app->db->createCommand($updateOne)->execute();
        }
        array_walk($catToBeAdd, function(&$key, $value,$user_id) { $key = ['category_id'=>$value,'status'=>1,'user_id'=>$user_id,'created_at'=>time(),'updated_at'=>time()]; },$user_id) ;
        $catToBeAdd=(array_values($catToBeAdd));
        if(count($catToBeAdd))
            Yii::$app->db->createCommand()->batchInsert(UserCategory::tableName(),array_keys($catToBeAdd[0]), $catToBeAdd)->execute();

        return['message'=>'Categories updated successfully.'];
        }
        else
            return['message'=>'categories can not be blank.',['field'=>'categories','message'=>'categories can not be blank.'],'statusCode'=>422];
    }
}
