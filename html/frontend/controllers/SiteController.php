<?php
namespace frontend\controllers;

use Yii;
use frontend\models\Login;
use common\models\User;
use yii\web\Session;

use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
     
    public $layout='login';
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login','facebooklogin', 'error'],
                        'allow' => true,
                    ],
                    [
                       
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
            
             
           
        ];
    }
    
    
    /* facebook */
    public function successCallback($client)
    {
        $attributes = $client->getUserAttributes();
        // user login or signup comes here
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {	
		$this->layout='main';
        return $this->render('index');
    }
    
   
    

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			
             return $this->redirect(['deal/index']);	
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    
    
    /* facebook login start */
    
    
public function actionFacebooklogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
 
        $s = new Session();
        if(!$s->isActive)   {
            $s->open();
        }
 
        $session = null;
        FacebookSession::setDefaultApplication('105087376495856','64f87ec8530e926a459cf308db693c54');
        $helper = new FacebookRedirectLoginHelper(Yii::$app->urlManager->createAbsoluteUrl(['/site/facebooklogin']));
 
        try {
            $session = $helper->getSessionFromRedirect();
        } catch(FacebookRequestException $ex) {
            // When Facebook returns an error
        } catch(\Exception $ex) {
            // When validation fails or other local issues
        }
        if (!empty($session)) {
            $request = new FacebookRequest($session, 'GET', '/me');
            $response = $request->execute();
            $graphObject = $response->getGraphObject()->asArray();
            $fbid = (string)$graphObject['id'];
            $fbname = $graphObject['name'];
 
            $user = UserMaster::find()->where(['fbidentifier' => $fbid])->one();
 
            if(empty($user))    {
                $user = new User();
                $user->fbidentifier = $fbid;
                $user->created_at = time();
            }
            $user->username = $fbname;
            $model = new LoginForm();
            $model->username = (string)$user->fbidentifier;
 
            if(!$user->isNewRecord) {
                $user->updated_at = time();
                $user->auth_key = $user->generateAuthKey();
            }
 
            if($user->save() && $model->login()) {
                return $this->goBack();
            }   else    {
                $this->goHome();
            }
        }   else    {
            $loginUrl = $helper->getLoginUrl();
            $this->redirect($loginUrl);
        }
    }
    
    /* facebook  login end */

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
