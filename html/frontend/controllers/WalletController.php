<?php
namespace frontend\controllers;

use Yii;
use yii\db\Query;
use frontend\models\WalletPin;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class WalletController extends Controller
{  
   
    
    public function actionIndex()
    {
        $user_id=Yii::$app->user->id;   
        $model= new WalletPin();
        $model->userid = $user_id;
     
        if($model->load(Yii::$app->request->post()) && $model->validate())
        {   
             return $this->render('view', [
                'model' => $model,
            ]);
        }
        else
        {
             return $this->render('index', [
                'model' => $model,
            ]);
        }
        
        
        
    }
    
   
    
    
	
}
