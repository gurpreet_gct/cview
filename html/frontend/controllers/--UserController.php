<?php
namespace frontend\controllers;

use Yii;
use yii\db\Query;
use frontend\models\Login;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use common\models\LoginLogs;
use common\models\User;
use common\models\Profile;
use common\models\Order;
use common\models\Store;
use common\models\States;
use common\models\Address;
use common\models\AddressType;
use common\models\OrderItems;
use common\models\OrderItem;
use common\models\PolygonsRegion;
use common\models\OrderSubItems;
use common\models\StoreOrders;
use common\models\Workorderpopup;
use common\models\Workorders;	
use frontend\models\ChangePin;
use frontend\models\Manufacturer;
use frontend\models\UserStore;
use frontend\models\Customer;
use frontend\models\CustomerSearch;
use frontend\models\Profiles;
use frontend\models\ForgotPassword;
use frontend\models\FacebookLogin;
use backend\models\GeoZone;
use backend\models\Notification;
use backend\models\Notificationsetting;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

class UserController extends Controller
{
	  
    public $layout='login';
	
	
	 
	  public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login','reset','forgot', 'register','addpin','facebooklogin', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout','index','view','update','changepassword','help','aboutus','myorder','mysweets','mypurchase','mysetting','myorderview','updatefav','delete','myaddress','createaddress','countrywisestate','viewaddress','updateaddress','addressdelete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }
	    
	public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['changepassword'],$actions['view'], $actions['index'], $actions['update'],$actions['create']);                    

		return $actions;
	}
	
	
	/* help */
	public function actionHelp()
    {	
		$this->layout='main';
        return $this->render('help');
    }
    
    /* About us*/
    public function actionAboutus()
    {	
		$this->layout='main';
        return $this->render('about');
    }
    /* My Sweets */ 
    public function actionMysweets()
    {	
		$this->layout='main';
        return $this->render('mysweets');
    }
    
    /* my order */
    public function actionMyorder()
    {	
		$this->layout='main';
        return $this->render('myorder');
    }
    
     /* my purchase */
    public function actionMypurchase()
    {	
		$this->layout='main';
	 $user_id = Yii::$app->user->id;
	
            $dataProvider = new ActiveDataProvider([ 
            'query' => Order::find()
			->where([Order::tableName().'.user_id'=>Yii::$app->user->id])
			
		->joinWith('orderitems')
		->joinWith('orderitems.ordersubitems')
		 ->orderBy("id desc")
       			
        ]);
					  return $this->render('mypurchase', [
            'dataProvider' => $dataProvider, 
             ]);
             
             
    }
    public function actionMyorderview($id)
    {	
		$this->layout='main';
	 $user_id = Yii::$app->user->id;
		/*$model=OrderItems::find()
		->where([Order::tableName().'.user_id'=>Yii::$app->user->id]);*/
       $model =  OrderItem::find()
			->where([OrderItem::tableName().'.user_id'=>Yii::$app->user->id])
			->where([OrderItem::tableName().'.id'=>$id])
				->orderBy("id desc")->one();
            	
				//echo'<pre>'; print_r($model); die();
					  return $this->render('myorderview', [
             'model'=> $model,
             ]);
    }
    /* To add Brand in favourite list */
    public function actionUpdatefav($id)
	{
		
		$user_id=Yii::$app->user->id;        
		$userStore=UserStore::findOne(['user_id'=>$user_id,'store_id'=>$id]);
		if($userStore)
		{
			$userStore->status=1;
			$userStore->save();
		}
		else
		{
			$userStore=new UserStore();
			$userStore->user_id=$user_id;
			$userStore->store_id=$id;
			$userStore->status=1;
			$userStore->save();
		}
		  return $this->redirect(['myorderview', 'id' => $id]);
	}
    /* To remove an store from favourite list */
    public function actionDelete($id)
    {        
        $user_id=Yii::$app->user->id;
      
		$userStore=UserStore::findOne(['user_id'=>$user_id,'store_id'=>$id,'status'=>1]);
		if($userStore)
		{
			$userStore->status=0;
			$userStore->save();
			  return $this->redirect(['myorderview', 'id' => $id]);
		}
		else
			  return $this->redirect(['myorderview', 'id' => $id]);
		
    }
    
     /* my order */
    public function actionMysetting()
    {	
		$this->layout='main';
		
        $model = Notificationsetting::find()
        ->where(['user_id'=>Yii::$app->user->id])		
        ->one();
        
		//echo'<pre>';print_r($model);die;
        if($model==""){
				 return $this->render('mysetting',[
			'model'=> $model,
			]);
		}
		else
		{
			if($model->load(Yii::$app->request->post()) && $model->validate())
			{
				$model->save();
			}
			return $this->render('mysetting',[
			'model'=> $model,
        ]);
		}
    }
     public function findaddressModel($id)
    {
		if(($model = Address::findOne($id)) != null) {
		 return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionMyaddress()
	{
			$this->layout='main';
			$user_id = Yii::$app->user->id;
    //echo'<pre>';print_r($user_id);die();
		      $model = new Address();
		   //   echo'<pre>';print_r($model);die();
		      $query = $model::find()
		      ->where(['user_id'=>$user_id])->orderBy('id desc');
		      $dataProvider = new ActiveDataProvider(['query' => $query,]);
//echo'<pre>';print_r($dataProvider);die;
		      return $this->render('myaddress',['dataProvider' =>$dataProvider]);

		      
		      
		      
	}
	
	/* Find State with Country Id */
	
		public function actionCountrywisestate()  
	{
		$country = $_POST['id'];  
		if($country) {
			
			 $state =  ArrayHelper::map(States::find()->where(['country_id' =>$country])->all(), 'id', 'name');
			$st = array('data'=> $state);
			echo json_encode($st); exit;
		}
	}
	
public function actionCreateaddress()
{
	$this->layout='main';
	$model = new Address();	
if ($model->load(Yii::$app->request->post()) && $model->validate()){
	$model->user_id=Yii::$app->user->id;
		$model->save();
		//echo '<pre>'; print_r($model); die; 
		  return $this->redirect(['myaddress', 'id' => $model->id]);
		} else{
	return $this->render('createaddress', [
                'model' => $model,
            ]);
		}
}
	
	 
			public function actionViewaddress($id)
			{
			$this->layout='main';
			$model = Address::findOne(['id'=>$id]);
		//	print_r($model);die;
		return $this->render('viewaddress', [
				'model' => $model]);
      
	}

	
		public function actionUpdateaddress($id)
			{
			$this->layout='main';
			$model = $this->findaddressModel($id);
		if ($model->load(Yii::$app->request->post()) && $model->save()){
			return $this->redirect(['myaddress','id'=> $model->id]);
			}
			else {
				return $this->render('updateaddress',['model'=>$model]);
			}
			
	}
	  public function actionAddressdelete($id)
    {
        $this->findaddressModel($id)->delete();

       return $this->redirect(['myaddress']);
    }
    
	

    
	
	/* View Customer Profile */
	 public function actionView()
	{				
        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->user->identity->id),
        ]);
	}
	
	
    

    /* custom Login api to allow user login into the system */
    public function actionLogin()
    {        
    
        $model = new Login();
		$model->isMobile=true;
		$data=array('Login'=>Yii::$app->request->post());

        if ($model->load($data) && $model->login()) { 
            $rs=Customer::findOne(Yii::$app->user->identity->id);
            $rs->latitude=isset($data['Login']['latitude'])?$data['Login']['latitude']:NULL;;
            $rs->longitude=isset($data['Login']['longitude'])?$data['Login']['longitude']:NULL;;     
            $rs->save(false);
			return //Yii::$app->user->identity;
			Customer::findOne(Yii::$app->user->identity->id);
            //  return ['access_token' => Yii::$app->user->identity->getAuthKey()];
        } else {
			
                   $model->validate(null,false);
				   return $model;
        }
        
    }
	
    /* Disable/remove the token of logged in user */
	public function actionLogout()
	{
		$model=new User();        
		if((Yii::$app->request->post()) && ($model=$model->findIdentityByAccessToken(Yii::$app->request->post()['access-token'])))
		{
            
            try{
                $loginlog=LoginLogs::findOne(['sessionid'=>$model->auth_key]);
                if($loginlog){
                    $loginlog->logout_at=time();
                    $loginlog->save();
                }
            }
            catch(Exception $e){}
            
			$model->auth_key="";
			$model->save();
			return "Logout Successfully";
		}
		else
		{
			
			return ["Please provide an validate user token",'statusCode'=>422];
		}
		
	}
	
    /* To create/singup user */
    public function actionRegister()
    {
		$this->layout='login';            
        $model= new Customer();
      
         $model->scenario = 'register';              	  
         	
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {    
		
		  return $this->redirect(['addpin', 'id' => $model->id ]);	
		  
		
		     
      
		}else {
		
		 return $this->render('register', [
                'model' => $model,
            ]);	
			
		
		}
        
    }
    
    // add pin 
    
     public function actionAddpin($id)    
	{
		$this->layout='login';            
        $model= new ChangePin; 
		$customer = Customer::findOne($id); 
		
          
	  if ($model->load(Yii::$app->getRequest()->getBodyParams()) && $model->validate() && $model->newPassword!='' && $customer->password_hash!='') {        	
			
			 $model->newPassword = Yii::$app->security->generatePasswordHash($model->newPassword);
			 $customer->password_hash =  $model->newPassword;
			 $customer->save(false);
			 
			  return $this->redirect(['site/login']);	
			
		}else{
			
			 return $this->render('setpin', [
                'model' => $model,
            ]);	
			
			
		}
		
	}
    
    // Update Customer data 
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {        	  
        	
        	$uniqueimage = $model->username.substr(md5(date("Ymdhis")),0,15);             
 			/* get old file name */
 			if(isset($model->image) && $model->image !='') {
			$oldImageName = explode(".",$model->image); 
			}else { 
				$oldImageName[0] = $uniqueimage;
			}	   
        	  $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
        	  /*save image in user model */ 
        	  
	          if ($model->imageFile) {
	          	$imageName = $oldImageName[0];
	            $model->image=$oldImageName[0].'.'.$model->imageFile->extension;
	            $imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::getAlias('@frontend')."/" .'web/profile'."/".$model->image);
	            
        		$model->imageFile->saveAs($imagePath);      		
	          	
			    }
			    
			    			       
				$model->save();
				
             return $this->redirect(['view']);			  
			
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    
	
	/* Chnage Password */
	public function actionChangepassword($id) {	
			
		$model= new ChangePin; 
		$customer = Customer::findOne($id);  
		

        if ($model->load(Yii::$app->getRequest()->getBodyParams()) && $model->validate() && $model->newPassword!='' && $customer->password_hash!='') {        	
			
			 $model->newPassword = Yii::$app->security->generatePasswordHash($model->newPassword);
			 $customer->password_hash =  $model->newPassword;
			 $customer->save(false);
	   
		 return $this->redirect(['view']);
	
		
		}else{
		 return $this->render('changepassword', [
                'model' => $model,
            ]);	
		
		}	
		 
	}
	
  /* Forget password */
    public function actionForgot()    
	{				
		   $model = new User();
		  
			if(isset(Yii::$app->request->post()['User']['email'])) {
					
				$getEmail=Yii::$app->request->post()['User']['email'];
				$getuserdetails= User::findByEmail($getEmail);
				 
				
			if(!empty($getuserdetails)) {
				
                $validtoken = $model->generatePasswordResetToken();
             
                $getuserdetails->password_reset_token = $validtoken;
                $getuserdetails->save();
                
                $Name= $getuserdetails->fullName;
				$toEmail= $getuserdetails->email;
                $subject="Forgot PIN";
                $sitepath = Url::to(['user/reset'], true);
                                  
                $message="you have successfully Forgot your PIN click bellow links for reset new PIN : <br/>
                    <a href='".$sitepath."?token=".$validtoken."'>Click Here to Reset PIN</a>";
            
          
            
            if($model->isPasswordResetTokenValid($validtoken))
            {
				
			/* if valid token, then email will be send */            
                
		$emailsend =	Yii::$app->mail->compose()
			->setTo($toEmail)
			->setFrom($toEmail,$Name)
			->setSubject($subject)
			->setHtmlBody($message)
			->send();
		 
		if($emailsend){
			
			echo Yii::$app->getSession()->setFlash('success', 'please check your email and reset your PIN');
				 return $this->render('forgot', [
                'model' => $model,
            ]);
			
			}	                  
               
            }else {
				
				 Yii::$app->getSession()->setFlash('error', 'your token has been expired, please try again');
				 return $this->render('forgot', [
                'model' => $model,
            ]);
				}
 
          
           
            
				}else {
			 
				
				 Yii::$app->getSession()->setFlash('error', 'invalid email address, please try again');
				 return $this->render('forgot', [
                'model' => $model,
            ]);	
			
				}		  
	      
	      }else {
			  
			   return $this->render('forgot', [
                'model' => $model,
            ]);
			  
			  }
        
        
	}
    
    /* Fb Login */
    public function actionFacebooklogin()
    {
		  $model=new FacebookLogin();
		  
           if ($model->load(Yii::$app->request->post()) && $model->validate()) {       
          
            $model->load(Yii::$app->request->getBodyParams(),'');           
				if($model->validate()) {
					return $model->getUserObject();
				}
			}
            else
            {
                return $this->render('login', [
                'model' => $model,
            ]);	
            }
            
    }
	
	/* Username availability */
	public function actionValidateuser()
	{
        
        $model= new Customer();        
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');                
        $data=Yii::$app->getRequest()->getBodyParams();
        
		if($model->validate())
		{
            //Yii::$app->getResponse()->setStatusCode(200);
			return $model;
		}
		else
		{
				//Yii::$app->getResponse()->setStatusCode(422);
				return $model;
		}
		
	}
	
	/* find customer */
	protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
		/* Reset password by 345 */
	public function actionReset($token){
		
				
		$model = new User();	
		
		if(isset(Yii::$app->request->post()['User']['password_hash'])) {
					
				$getnewpassword=Yii::$app->request->post()['User']['password_hash'];
				$getuserdetails= User::findByPasswordResetToken($token);
				
				
				
				if(!empty($getuserdetails)) {
					
					$gettoken = $getuserdetails->password_reset_token;
															
					if($gettoken==$token){ 
						
					$newpasswordhash = $model->setPassword($getnewpassword);
					
					$getuserdetails->password_hash= $newpasswordhash;
					
					$getuserdetails->password_reset_token= $model->removePasswordResetToken();
					
					$getuserdetails->save();				
									    
				    echo Yii::$app->getSession()->setFlash('success', 'You have successfully changed your PIN. Please open the app and login using your new PIN');
						return $this->render('reset', [
						'model' => $model,
					]);
				    
					
					
					#return $this->redirect(Url::to(['site/login'], true));
				
										
					}else {
					
					Yii::$app->getSession()->setFlash('error', 'token not match');
			
					return $this->redirect(Url::to(['user/forgot'], true));
					
				}
				
		}else {
			
			Yii::$app->getSession()->setFlash('error', 'invalid token please try again');
			
			return $this->redirect(Url::to(['user/forgot'], true));
		
			}
		
		}else{
	    return $this->render('reset', [
                'model' => $model,
            ]);
		} 
	}
}
