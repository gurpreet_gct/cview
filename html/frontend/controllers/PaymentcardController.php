<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use common\models\Paymentcard;
use common\models\Paymentcardtype;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class PaymentcardController extends ActiveController
{    
        public $modelClass = 'common\models\Paymentcard';       
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['searchkeyword']);                    

		return $actions;
	}

  	
   /* save new Paymentcard information */    
    public function actionCreate()
    {
        $user_id=Yii::$app->user->id;   
        $model= new Paymentcard();
        $model->scenario = 'create';
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');  
        
       $model->user_id=$user_id;
        if($model->validate())
            {
                $find=Paymentcard::findOne(['cardType'=>$model->cardType,'user_id'=>$user_id]);
                if($find)
                {
                   $model=$find; 
                    $model->load(Yii::$app->getRequest()->getBodyParams(), '');  
                }
                                
                $model->save();
                  //$this->setHeader(201);
            }
			
           
        
         return $model;
    }
    
    public function actionUpdate($id)
    {
        $user_id=Yii::$app->user->id;
		$model= Paymentcard::findOne($id);      
		
        $model->load(Yii::$app->getRequest()->getBodyParams(), ''); 
        $model->user_id=$user_id;
          
       if($model->validate())
        {
          	
				if (($model->save() === false) && (!$model->hasErrors())) {
					throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
					}
				else
					{
						return $model;	
					}
       				
			}
		
        
        return $model;	
    }
    
    /* To get all cards*/
   public function actionIndex()
   {
        $user_id=Yii::$app->user->id;
       return new ActiveDataProvider([
                            'query' => Paymentcard::find()
                                        ->with(['cardtype'=>function ($query) {
                                                    //$query->select('catIDs,id');
                                        },
                       ])
                       ->where(['user_id'=>$user_id]),
                       'pagination' => [
						'pageSize' => 50
						],
                        
                            
                       
                        ]);
            
   }
   
  
	
}
