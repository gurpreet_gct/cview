<?php
namespace frontend\controllers;
use Yii;
use yii\db\Query;
use common\models\Pass;
use common\models\PassSearch;
use common\models\User;
use common\models\Profile;
use common\models\Workorderpopup;
use common\models\Workorders;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class TicketController extends Controller
{
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }
	    /* To get all cards*/
	    
   public function actionIndex()
   {
		
		 $searchModel = new PassSearch();
		 $dataProvider = $searchModel->ticketsearch(Yii::$app->request->queryParams);
         
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
            
   }    
       
	/* public function actionView($id)
	{				
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
	}*/
	
   /* find customer */
	protected function findModel($id)
    {
        if (($model = Id::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGetticketview(){ 
	
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$workorderpopup= new Workorderpopup();
	$passvalue =   Pass:: find()
				->joinWith("workorders")
				->joinWith("workorderpopup")
				->joinWith("profile")
				->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_id' => $_POST['workorderid']])
				->one();
		
			$html='<div class="modal-dialog prod_popup_had"><div class="modal-content" style="background-color:#F3F3F3"><div class="poup_hed_boder"><div class="modal-header "><h4 class="modal-title model-title-ticket"><b>MY TICKET</b><p class="close-ticket" data-dismiss="modal">X</p></h4><h1 class="h1-modal-title">'.$passvalue->profile->brandName.'</h1></div><div class="col-md-12"><div class="part_val"><b>'.$passvalue->workorderpopup->eventVenue.'</b> </div></div>
				<div class="col-md-12">
			<div class="bookmark_part">
				<b>'.$passvalue->workorders->dateTo.'</b>
				</div></div>
				</div>
				<div class="prod_popup"><div class="modal-body">
			<div class="col-md-12">
            <div class="row" >
				<div class="pup_img"><img class="pop-up-image" style="width:279px; height:200px;" src="'.$passvalue->workorderpopup->photo.'" /></div>
                 <div class="pup_img_logo">
                 <img class="pop-up-logo" style="width:100px; height:70px;" src="'.$passvalue->workorderpopup->logo.'" /></div>
				 </div></div><div class="col-md-12 grey-bottom" >
				<div class="row">
				<div class="pupup_doller" style="text-align: left;color:#08afb5;"><b>
				<h1>'.$passvalue->workorderpopup->ticketinfo.'</h1></b></div>';
				
				$html .='<div class="read_popup"  style="text-align: right;">
					<img src="'.Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/imgpsh_fullsize1.png").'" style="height: 16px; margin: 0 5px 0 0; width: 16px;" ><b><a  style="color:black;" href="javascript:void(0)"  data-target="#mygetModalread" data-toggle="modal" class="small-box-foote1 read_popupopen" rel="'.$_POST['workorderid'].'">Read More </a></b></div>
				</div>
				
			</div>
			 <div class="col-md-12 pop-bt-cl">
			 <div class="pop-up-image_bar_code ">
             <p>'.$passvalue->sspMoreinfo.'</p>
          </div>
				<div class="pop-up-image_bar_code  ">
             <p><img class="pop-up-image" style="width:250px; height:100px;" src="'.$passvalue->workorderpopup->passUrl.'" /> </p>
          </div>
          
          </div>
		</div></div><div class="modal-footer"></div>
      </div></div>';
  return 	$html;	
		
	}
	
    
    public function actionGetreadmore(){ 
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	
	
	$passvalue =  Pass:: findOne(['user_id' => Yii::$app->user->id,'workorder_id'=>$_POST['workorderid']]);
		$html='<div class="modal-dialog model-dialog-ticket-readmore">
		
		  <!-- Modal content-->
		  <div class="modal-content model-content-ticket-readmore">
			<div class="modal-header model-header-ticket-readmore">
			  <p class="closeticketreadmore" data-dismiss="modal">X</p>
			  <h4 class="modal-title" style="text-align:center"> <b> Read More </b> </h4>
			</div>
			<div class="modal-body overflow-text">'.$passvalue->readTerms.'
				     
			</div>
			<div class="modal-footer">
			
			</div>
		  </div>
		  
		</div>';
		return 	$html;		
		
	}
	
}
