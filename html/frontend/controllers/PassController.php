<?php
namespace frontend\controllers;
use Yii;
use yii\db\Query;

use common\models\Pass;
use common\models\PassSearch;
use common\models\User;
use common\models\Profile;
use common\models\Loyaltypoint;
use common\models\Loyalty;
use common\models\Workorderpopup;
use common\models\Workorders;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class PassController extends Controller
{
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }
	    /* To get all cards*/
	    
   public function actionIndex()
   {
		// $user_id=Yii::$app->user->id;
		 $searchModel = new PassSearch();
		 $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         /*$query = Pass::find()->where(['user_id'=>$user_id]);
						$dataProvider = new ActiveDataProvider([
							'query' => $query,
						]);*/

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
            
   }    
       
	/* public function actionView($id)
	{				
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
	}*/
	
   /* find customer */
	protected function findModel($id)
    {
        if (($model = Id::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGetpassview(){ 
	
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$workorderpopup= new Workorderpopup();
	$passvalue =   Pass:: find()
				->joinWith("workorders")
				->joinWith("workorderpopup")
				->joinWith("profile")
				->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_id' => $_POST['workorderid']])
				->one();
		
				$html='<div class="modal-dialog prod_popup_had"><div class="modal-content" style="background-color:#F3F3F3"><div class="poup_hed_boder"><div class="modal-header "><h4 class="modal-title modal-title-pass" style="text-align:center"><b> My Pass </b><p  class="close-pass" data-dismiss="modal">X</button></h4><h1 class="h1-modal-title">'.$passvalue->offerTitle.'</h1></div><div class="col-md-12"><div class="part_val"><b>'.$passvalue->profile->brandName.'</b> </div></div></div><div class="prod_popup"><div class="modal-body">
			<div class="col-md-12">
            <div class="row" >
				<div class="pup_img"><img class="pop-up-image" style="width:279px; height:200px;" src="'.$passvalue->workorderpopup->photo.'" /></div>
                 <div class="pup_img_logo">
                 <img class="pop-up-logo"  src="'.$passvalue->workorderpopup->logo.'" /></div>
				 </div></div><div class="col-md-12 grey-bottom">
				<div class="row">
				<div class="pupup_doller" style="text-align: left; float:left; color:#08afb5;"><b>';
				if($passvalue->offernow!=0) { 
					$html .='$'.$passvalue->offernow;
				}else{
					$html .='<h1>'.$passvalue->offerText.'</h1>';
					}
				$html .='</b></div>';
				if($passvalue->offernow!=0) {
					$html .='<div class="col-md-1 text_though">$'.$passvalue->offerwas;
					$html .='</div>';
				}
				$html .='<div class="read_popup"  style="text-align: right;">
					<img src="'.Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/imgpsh_fullsize1.png").'" style="height: 16px; margin: 0 5px 0 0; width: 16px;" ><b><a  style="color:black;" href="javascript:void(0)"  data-target="#mygetModalread" data-toggle="modal" class="small-box-foote1 read_popupopen" rel="'.$_POST['workorderid'].'">Read More </a></b></div>
				</div>
				
			</div> 
			 <div class="col-md-12 pop-bt-cl">
				<div class="pop-up-image_bar_code deal-bar-code">
             <p>'.$passvalue->sspMoreinfo.'</p>
          </div>
          <div class="pop-up-image_buton">';
          if($passvalue->workorders->type==1 || $passvalue->workorders->type==2) {
			   $html .=' <button    data-target="#myModalBuyInStore" data-toggle="modal" class="btn btn-info myModalBuyInStore" type="button" rel="'.$_POST['workorderid'].'"  data-id="'.$passvalue->store_id.'">BUY IN Store
          </button>';
			 }
           $html .='
         <input type="hidden" id="passUrl"  class="passUrl" name="passUrl" value="'.$passvalue->workorderpopup->passUrl.'">';
                if($passvalue->workorderpopup->buynow==1){
                 $html .='<button  id="addToMyWallet" data-target="#myModal" data-toggle="modal" class="btn btn-info addToMyWallet" type="button" rel=""  data-id="">BUY IN APP
          </button>';
	      }
          
         $html .='</div></div>
		</div></div><div class="modal-footer"></div>
      </div></div>';
  return 	$html;	
		
	}
	
    
    public function actionGetreadmore(){ 
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	
	
	$passvalue =  Pass:: findOne(['user_id' => Yii::$app->user->id,'workorder_id'=>$_POST['workorderid']]);
		$html='<div class="modal-dialog model-dialog-pass-readmore">
		
		  <!-- Modal content-->
		  <div class="modal-content model-content-pass-readmore">
			<div class="modal-header model-header-pass-readmore">
			  <p class="closereadmore" data-dismiss="modal">X</p>
			  <h4 class="modal-title" style="text-align:center"> <b> Read More </b> </h4>
			</div>
			<div class="modal-body overflow-text">'.$passvalue->readTerms.'
				     
			</div>
			<div class="modal-footer">
			 
			</div>
		  </div>
		  
		</div>';
		return 	$html;		
		
	}
	
	 public function actionGetaddofflineloyalty(){ 
	$user_id=Yii::$app->user->id;   
    $model= new Loyaltypoint();
	$model->load(Yii::$app->getRequest()->getBodyParams(), ''); 
    $model->user_id=$user_id; 
    if($model->validate())
    {
		
		$loyalty= Loyalty:: find()->select(
								[Loyalty::tableName().'.*'])
								->where([Loyalty::tableName().'.user_id' => $model->store_id,
								Loyalty::tableName().'.loyalty_pin' => $model->pin,
								'status'=>1])
								->orderBy([Loyalty::tableName().'.id' => SORT_DESC])->asArray()->one();
			
		
			if($loyalty){					
			$loyaltypoints = $loyalty['pointvalue']* $model->amount;
			$model->valueOnDay = $loyalty['dolor']/$loyalty['loyalty'];
			$model->rValueOnDay = $loyalty['redeem_pointvalue'];
			$model->order_id = 0;
			$model->loyaltypoints = $loyaltypoints;
			$model->method = 1;
			if($model->workorderid)
			{
				$model->transaction_id= $model->transaction_id . "-wid";
				
				/************** get isUsed pass value from pass table ********************/
				$pass_count = Pass:: find()->select([
														Pass::tableName().'.id,user_id,workorder_id,store_id,isUsed,count_passes'
													])
										->where([
													Pass::tableName().'.workorder_id' => $model->workorderid,Pass::tableName().'.user_id' => $user_id
												])
										->orderBy([Pass::tableName().'.id' => SORT_DESC])
										->asArray()->one();
					
					if($pass_count['isUsed']==0){
						$isUsed = 1;
					}else{
						 $isUsed = $pass_count['isUsed'] + 1;
						}
								
				/************** end here **********************************/
					$condition = 'workorder_id="'.$model->workorderid.'" AND user_id="'.Yii::$app->user->id.'"';
					Yii::$app->db->createCommand()->update(Pass::tableName(), 
									['isUsed' => $isUsed], $condition)
									->execute();
			}
			$model->status = 1;
			$model->save();
			//return "Your pass has been redeemed and your loyalty points added.";
			return "2";
			}else{

				 //return "That PIN does not match your registered 6-digit PIN. Please try again.";
				 return "3";
			}
	}else{
		
		 $errors = $model->errors;

     if(isset($errors['amount'][0])){
			return "5";
		}if(isset($errors['pin'][0])){
			return "6";
		}if(isset($errors['transaction_id'][0])){
			//return "7";
			if($errors['transaction_id'][0]=='This transaction ID has already accrued loyalty points.'){
				return "8";
			}else{
				return "7";
			}
		}

		 
		 else{
		 
		if( array_key_exists('transaction_id',$errors))
		 {
			//return $errors['transaction_id'][0];	 
			return "4";	 
		 }
		
		
	   }
		
		}
		}
	
}
