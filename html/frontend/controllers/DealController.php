<?php
namespace frontend\controllers;


use Yii;
use yii\web\Controller;
use yii\db\Query;
use common\models\Store;
use common\models\Deal;
use common\models\User;
use common\models\Profile;
use common\models\Workorderpopup;
use common\models\Workorders;
use common\models\Pass;
use common\models\DealBookmark;
use frontend\models\UserCategory;
use backend\models\Dealstore;
use frontend\models\Manufacturer;
use frontend\models\Storelocator;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;






class DealController extends Controller
{    
      
    
 

  	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }
    
  
    
    
    public function actionIndex()
    {   
    
	   $user_id=Yii::$app->user->id;   
       $category_ids=implode(",",ArrayHelper::map( UserCategory::find()->select('category_id')->where(['user_id'=>$user_id,'status'=>1])->all(), 'category_id', 'category_id'));
	        
	   $dataProvider = new ActiveDataProvider([ 
            'query' => Deal::find()
                  
       ->joinWith('workorderpopup')       
              
       ->joinWith(['workordercategory'=> function ($q) use ($category_ids) {
            if($category_ids=="")
                $category_ids='0';
        $q->where('category_id in ( '.$category_ids.')');
           }
        ],false,'INNER JOIN')        
        
        ->joinWith(['workorders'=> function ($q)  {            

        $q->where(sprintf('dateFrom <= %s and dateTo > %s and noPasses > usedPasses ' ,time(date("Y-m-d")),time(date("Y-m-d"))+86400));}     

        ],false,'INNER JOIN')
		->where(Workorders::tableName().'.status = 1')
		->andWhere([Workorders::tableName().'.workorderType' =>1])
		->andWhere(['IN', Workorders::tableName().'.type', [1,4]])
        ->groupBy('id')       
       ->orderBy("id desc")
       			
        ]);
        
      
         return $this->render('index', [
            'dataProvider' => $dataProvider,            
        ]);
       		

    }  

     public function actionBookmark($id)
    {
				
        $user_id=Yii::$app->user->id; 
         $model= new DealBookmark();
			$model->user_id=$user_id;
            $model->deal_id=$id;
            $model->created_at= time(date("Y-m-d"));
            
         if ($model->save(false)) {
					   return $this->redirect(['index']);
            }else {
		 
		   return $this->redirect(['index']);
		 
		}			
       
    }
    
      public function actionUnbookmark($id)
    {
				
        $user_id=Yii::$app->user->id; 
        
          $model= DealBookmark::find()->where(['user_id'=>$user_id,'deal_id'=>$id])->one();
            if($model)
            {
             $model->delete();   
               return $this->redirect(['index']);
            }else{
				 return $this->redirect(['index']);
			}
          
    }
    
    
    
    /* public function actionView($id)
    {        
	   $user_id=Yii::$app->user->id;   
        $store_id=$id;
	
	   
	   $dataProvider =  new ActiveDataProvider([
            'query' => Deal::find()
       ->with(['workorders'=>function ($query) {
            /* @var $query \yii\db\ActiveQuery */

           /* $query->select('catIDs,id');
     /*   },      
       
       ])
       ->joinWith('workorderpopup')
       ->joinWith(['dealstore'=> function ($q) use ($store_id) {
        $q->where('store_id ='. $store_id);}
        ])
   //    ->joinWith('workordercategory',false)
      // ->andWhere('')
       
        ]);
        
          return $this->render('view', [
            'dataProvider' => $dataProvider,
            
        ]);
   
        
     //  return $deals;
		

    } */
    
    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Deal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function shareUrl() {
		
	return true;
	}
	
	public function actionAddtowallet1(){ 
	$user_id = Yii::$app->user->id;	
	$model= new Pass();
	$Workorders= new Workorders();
	$workorderpopup= new Workorderpopup();
	$created_at=time();

	if(isset($_POST['workorderid']) && $_POST['workorderid']!=''){
		
	$Workorders = Workorders:: find()->select([
													Workorders::tableName().'.advertiserID,name,dateTo,noPasses,usedPasses,type,allowpasstouser',
													Workorderpopup::tableName().'.offerTitle,offerText,offerwas,offernow,readTerms,sspMoreinfo,passUrl,logo,photo',
													Workorderpopup::tableName().'.buynow',
													Profile::tableName().'.brandName,brandLogo,brandAddress'
												])
										->leftJoin(
													Workorderpopup::tableName(), Workorderpopup::tableName().'.workorder_id = '.Workorders::tableName(). '.id'
												 )
										->leftJoin(
													Profile::tableName(), Workorders::tableName().'.advertiserID= '.Profile::tableName().'.user_id'
												 )
										->where(['=',Workorders::tableName().'.id', $_POST['workorderid']
												])
										->asArray()->one();	
  
		/*************** passes count **************************/
		$pass_count = Pass:: find()->select([
												Pass::tableName().'.id,user_id,workorder_id,store_id,count_passes'
											])
										->where([
													Pass::tableName().'.workorder_id' => $_POST['workorderid'],
													Pass::tableName().'.user_id' => $user_id
												])
										->orderBy([Pass::tableName().'.id' => SORT_DESC])->asArray()->one();
		
							
		/************ end here *******************************************************************/
    if($pass_count)
		{
			if($Workorders['type']==1){
					return "You have already added this pass to your wallet. It can be found in My Passes in My Wallet.";
			}else if($Workorders['type']==2){
						return "You have already added this digital stamp card to your wallet. It can be found in My Stamp Cards in My Wallet.";
				}else{
						return "You have already added this pass to your wallet. It can be found in My Passes in My Wallet.";
					}
		}
		else{
			if($Workorders['type']==4 || $Workorders['type']==2){
					$allowpasstouser = 0;
				}else{
					
					if($Workorders['noPasses'] > $Workorders['usedPasses']){
						$allowpasstouserLeft =  $Workorders['noPasses'] - $Workorders['usedPasses'];
						if($allowpasstouserLeft > $Workorders['allowpasstouser']) {
							$allowpasstouser = $Workorders['allowpasstouser'];
							}else{
								 $allowpasstouser = $allowpasstouserLeft;
							}
						}
					
				}
					
				$model->user_id	 = 		$user_id;		
				$model->store_id = 		$Workorders['advertiserID'];		
				$model->workorder_id = 	Yii::$app->request->post()['workorderid'];
				$model->store_id = 		$Workorders['advertiserID'];
				$model->offerTitle = 	$Workorders['offerTitle'];
				$model->offerText = 	$Workorders['offerText'];
				$model->offerwas = 		$Workorders['offerwas'];
				$model->offernow = 		$Workorders['offernow'];
				$model->readTerms = 	$Workorders['readTerms'];
				$model->sspMoreinfo = 	$Workorders['sspMoreinfo'];
				$model->count_passes = 	1;
				$model->save();	
					
				$usedPasses = $Workorders['usedPasses'] + 1;
				Yii::$app->db->createCommand()->update(Workorders::tableName(), 
									['usedPasses' => $usedPasses],  
									['id' => Yii::$app->request->post()['workorderid']])
									->execute();
									
			return "success";				
			}
		}
	
	}
	
	/*********************************/
	 public function actionAddtowallet(){
	$user_id = Yii::$app->user->id;  	
	$model= new Pass();
	$Workorders= new Workorders();
	$workorderpopup= new Workorderpopup();
	$created_at=time();
	if(isset(Yii::$app->request->post()['workorderid']) && Yii::$app->request->post()['workorderid']!='' ){
		
		$Workorders = Workorders:: find()->select([
													Workorders::tableName().'.advertiserID,name,dateTo,noPasses,usedPasses,type,allowpasstouser',
													Workorderpopup::tableName().'.offerTitle,offerText,offerwas,offernow,readTerms,sspMoreinfo,passUrl,logo,photo',
													Workorderpopup::tableName().'.buynow',
													Profile::tableName().'.brandName,brandLogo,brandAddress'
												])
										->leftJoin(
													Workorderpopup::tableName(), Workorderpopup::tableName().'.workorder_id = '.Workorders::tableName(). '.id'
												 )
										->leftJoin(
													Profile::tableName(), Workorders::tableName().'.advertiserID = '.Profile::tableName().'.user_id'
												 )
										->where([
													Workorders::tableName().'.id' => Yii::$app->request->post()['workorderid']
												])
										->asArray()->one();
								
		
		/*************** passes count **************************/
		$pass_count = Pass:: find()->select([
												Pass::tableName().'.id,user_id,workorder_id,store_id,count_passes,isUsed,passesTrack'
											])
										->where([
													Pass::tableName().'.workorder_id' => Yii::$app->request->post()['workorderid'],
													Pass::tableName().'.user_id' => $user_id
												])
										->orderBy([Pass::tableName().'.id' => SORT_DESC])->asArray()->one();
			
										
		/************ end here *******************************************************************/
		if($pass_count)
		{
			if($Workorders['type']==1){
					if($pass_count['isUsed'] >= $Workorders['allowpasstouser']){
							 return "4";
							
					}elseif($pass_count['isUsed']!=$pass_count['passesTrack']){
						return "5";
					}else{
							$model =  Pass::findOne(['workorder_id' => Yii::$app->request->post()['workorderid'],'user_id' => $user_id ]);
							$model->passesTrack = $model->passesTrack + 1;
							$forPlus = $model->passesTrack - $model->isUsed;
							if($forPlus==1){
								$model->save(false);
							}
							return $query = Pass:: find()
											->joinWith("workorders")
											->joinWith("workorderpopup")
											->joinWith("profile")
											->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_id' => Yii::$app->request->post()['workorderid'],Workorders::tableName().'.status' =>1])
											->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))))->one(); 
						}
				}else if($Workorders['type']==2){
						return "6";
				}else{
							if($pass_count['isUsed'] >= $Workorders['allowpasstouser']){
							 return "7";
							}elseif($pass_count['isUsed']!=$pass_count['passesTrack']){
								return "5";
								
							}else{
									$model =  Pass::findOne(['workorder_id' => Yii::$app->request->post()['workorderid'],'user_id' => $user_id ]);
									$model->passesTrack = $model->passesTrack + 1;
									$forPlus = $model->passesTrack - $model->isUsed;
									if($forPlus==1){
									
										$model->save(false);
									}
									return $query = Pass:: find()
												->joinWith("workorders")
												->joinWith("workorderpopup")
												->joinWith("profile")
												->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_id' => Yii::$app->request->post()['workorderid'],Workorders::tableName().'.status' =>1])
												->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))))->one();
							
							}
					}
		}
		else{
				if($Workorders['type']==4 || $Workorders['type']==2){
					$allowpasstouser = 0;
				}else{
					if($Workorders['noPasses'] > $Workorders['usedPasses']){
						$allowpasstouserLeft =  $Workorders['noPasses'] - $Workorders['usedPasses'];
						if($allowpasstouserLeft > $Workorders['allowpasstouser']) {
							$allowpasstouser = $Workorders['allowpasstouser'];
							}else{
								 $allowpasstouser = $allowpasstouserLeft;
							}
						}
					}
				$model->user_id	 = 		$user_id;		
				$model->store_id = 		$Workorders['advertiserID'];		
				$model->workorder_id = 	Yii::$app->request->post()['workorderid'];
				$model->store_id = 		$Workorders['advertiserID'];
				$model->offerTitle = 	$Workorders['offerTitle'];
				$model->offerText = 	$Workorders['offerText'];
				$model->offerwas = 		$Workorders['offerwas'];
				$model->offernow = 		$Workorders['offernow'];
				$model->readTerms = 	$Workorders['readTerms'];
				$model->sspMoreinfo = 	$Workorders['sspMoreinfo'];
				$model->passesTrack   = 1; 
				$model->count_passes = 	1;
				$model->save();	
					
				$usedPasses = $Workorders['usedPasses'] + 1;
				Yii::$app->db->createCommand()->update(Workorders::tableName(), 
									['usedPasses' => $usedPasses],  
									['id' => Yii::$app->request->post()['workorderid']])
									->execute();
				if($model->id){
				 	 $query = Pass:: find()
									->joinWith("workorders")
									->joinWith("workorderpopup")
									->joinWith("profile")
									->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.id' => $model->id,Workorders::tableName().'.status' =>1])
									->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))))->one();
									return "8";
				 }else{
				  return 'data not saved';
				 
				 }
				
			
		}
		
		
	}else{
		return "workorderid cannot be blank.";
		}
  }
	/*******************************/
}
