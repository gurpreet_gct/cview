<?php
namespace frontend\controllers;
use Yii;
use yii\db\Query;
use common\models\Id;
use common\models\IdSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class WalletidController extends Controller
{
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }
	    /* To get all cards*/
	    
   public function actionIndex()
   {
		 $user_id=Yii::$app->user->id;
		   $searchModel = new IdSearch();
         $query = Id::find()->where(['user_id'=>$user_id]);
						$dataProvider = new ActiveDataProvider([
							'query' => $query,
						]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
            
   }    
       
	 public function actionView($id)
	{				
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
	}
	
    	
   /* save new Id information */    
    public function actionCreate()
    {
        $user_id=Yii::$app->user->id;   
        $model= new Id();
        $model->scenario = 'create';  
        $model->user_id=$user_id;
        
        if($model->load(Yii::$app->request->post()) && $model->validate())
            {
               
               $model->imageFile1 = UploadedFile::getInstance($model, 'imageFile1');              
               $frontphoto = md5($model->nameOnCard.rand(10,1000).'front'). ".jpg";              
                
               if ($model->imageFile1) {
  	     	 		$imageName = $frontphoto;
        			$model->front = $imageName;
        			
       				$imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['idsimagepath']."/".$imageName); 
					$model->imageFile1->saveAs($imagePath);
        				
				}
				
			 $model->imageFile2 = UploadedFile::getInstance($model, 'imageFile2');			
			 $backphoto = md5($model->nameOnCard.rand(10,1000).'back'). ".jpg";
			 
			 if($model->imageFile2) {               
				/* save Id model in back photo */	
					$imageName = $backphoto;
        			$model->back =  $imageName;           				
					$imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['idsimagepath']."/".$imageName); 
        			$model->imageFile2->saveAs($imagePath);
        		
				}
				
				$model->save(false);    					           
                             
                return $this->redirect(['view', 'id' => $model->id]);
                 
            }else{
				
				 return $this->render('create', [
                'model' => $model,
            ]);
			
			}
			
      
    }
    
    public function actionUpdate($id)
    {
        $user_id=Yii::$app->user->id;
		$model= Id::findOne($id);		
        $model->user_id=$user_id;
       
        $model->created_at = strtotime($model->created_at);
                 
       if($model->load(Yii::$app->request->post()) && $model->validate())
            {
				
                $model->imageFile1 = UploadedFile::getInstance($model, 'imageFile1');              
                          
                
               if ($model->imageFile1) {
				    if(!empty($model->front)){	
						$oldfront =(explode("Idimages/",$model->front));								
						$frontphoto = $oldfront[1];
						
					}else{
						 $frontphoto = md5($model->nameOnCard.rand(10,1000).'front'). ".jpg"; 
					} 
  	     	 		$imageName = $frontphoto;
        			$model->front = $imageName;
        			
       				$imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['idsimagepath']."/".$imageName); 
					$model->imageFile1->saveAs($imagePath);
        				
				}
				
			 $model->imageFile2 = UploadedFile::getInstance($model, 'imageFile2');			
			
			 
			 if($model->imageFile2) {               
				/* save Id model in back photo */
					 if(!empty($model->back)){
						 $oldback =(explode("Idimages/",$model->back));								
						 $backphoto = $oldback[1]; 
					}else{
						 $backphoto = md5($model->nameOnCard.rand(10,1000).'back'). ".jpg";	 
					} 
				
					$imageName = $backphoto;
        			$model->back =  $imageName;           				
					$imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['idsimagepath']."/".$imageName); 
        			$model->imageFile2->saveAs($imagePath);
        		
				}
				
				
				$model->save(false);	           
                             
                return $this->redirect(['view', 'id' => $model->id]);
                 
            }else{
				
				 return $this->render('update', [
                'model' => $model,
            ]);
			
			}
		
        
        return $model;	
    }
    
	/* Delete  */   
   public function actionDelete($id)
    {
        $this->findModel($id)->delete();

       return $this->redirect(['index']);
    }
	

	
 /* delete via ajax */   
  public function actionDeletebyajax() 
 
  {
		$pid = $_POST['id'];  
		if($pid) {
       
        $this->findModel($pid)->delete();
       
       echo $pid; exit;
		}
	}
	
	/* find customer */
	protected function findModel($id)
    {
        if (($model = Id::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
    
	
}
