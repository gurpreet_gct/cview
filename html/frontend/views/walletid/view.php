<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Cardtype */

 
 
$this->title = Yii::t('app','View Wallet ID: ') . ' ' .ucfirst($model->nameOnCard);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Wallet ID'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
          <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>
				   <?= Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
     		   <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>   
             
			<?= Html::a(Yii::t('app','Cancel'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
             [
				'attribute'=>'cardType',
				'value'=>$model->cardtype->name,
				
			],
        
            'nameOnCard',
            'expiry',
            [
			'attribute'=>'Front Photo',
			'value'=> $model->frontimage,
			'format' => ['image',['width'=>'70','height'=>'70']],
			 ],
			 [
			'attribute'=>'Back Photo',
			'value'=> $model->backimage,
			'format' => ['image',['width'=>'70','height'=>'70']],
			 ],
			 
           
            'created_at',
            'updated_at',
			
             
             
           
        ],
    ]) ?>

</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>

