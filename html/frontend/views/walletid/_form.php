<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Cardtype;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\cardtype */
/* @var $form yii\widgets\ActiveForm */
?>



   <?php
    $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-cardtype-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]);
    ?>
	 <div class="box-body">
      <div class="form-group">
	 <?php echo $form->errorSummary($model); ?>
	 
	<?php   $cardtype =  ArrayHelper::map(Cardtype::find()->all(), 'id', 'name'); ?>
	<?= $form->field($model, 'cardType')->dropDownList($cardtype, array('prompt'=>Yii::t('app','Select'))) ?>
	 
       <?= $form->field($model, 'nameOnCard')->textInput(['maxlength' => true]) ?>
       <?= $form->field($model, 'cardNumber')->textInput(['maxlength' => true]) ?>
       <?= $form->field($model, 'expiry')->widget(
		DatePicker::className(), [			
			'language' => 'en',
			'size' => 'lg',
			'clientOptions' => [
				'autoclose' => true,				
				'format' => 'yyyy-mm-dd',
			]
	]);?>
          

		<?php
		if(!empty($model->frontimage)){
           echo '<img width="150" height="150" src="'.$model->frontimage.'"/><div class="help-block"></div>';
			}
		?>   
	   <?= $form->field($model, 'imageFile1')->fileInput()->label(Yii::t('app','Front Photo')) ?>
	   <?php
		if(!empty($model->backimage)){
           echo '<img width="150" height="150" src="'.$model->backimage.'"/><div class="help-block"></div>';
			}
		?>   
		
	   <?= $form->field($model, 'imageFile2')->fileInput()->label(Yii::t('app','Back Photo')) ?>
    
   
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Users')]) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>
