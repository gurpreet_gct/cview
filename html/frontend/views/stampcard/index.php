<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CardtypeSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','My Wallet');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="cardtype-index">

 <input type="hidden" id="baseurl" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/stampcard/getstampcardview';?>">
<input type="hidden" id="baseurlread" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/stampcard/getreadmore';?>">
 <input type="hidden" id="baseurlstampcard" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/stampcard/getstampcardbuyinapp';?>">
 <input type="hidden" id="baseurlstampcardcount" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/stampcard/getstampcardcount';?>">
<?php
//profile.brandLog
        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns' => [
				[
              'header'=>Yii::t('app','Logo'),
              'value' => function ($data) {
                       return  Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$data->profile->brandLogo)
                        ;
                    },
                  'format' => ['image',['width'=>'70','height'=>'70']] , 
               /*'onclick'=>'actionTypes($(this).attr("workorder_id"))'*/
              ],
		
              [
              'header'=>Yii::t('app','offerTitle'),
               
               'value'=>function ($model) {
                return "<a href='javascript:void(0);' onclick='getData($model->workorder_id)' data-target='mygetModal' data-toggle='modal' class='small-box-footer'> $model->offerTitle</a>";
               },
               'vAlign'=>'middle',
               'format'=>'raw',
               'width'=>'150px',
               'noWrap'=>true
              ],
			
			
			 [
              'header'=>'Exp.date',
               
               'value'=>function ($model) {
                return "<a href='javascript:void(0);' onclick='getData($model->workorder_id)' data-target='mygetModal' data-toggle='modal' class='small-box-footer'>".date('d/m',  strtotime($model->workorders->dateTo))."</a>";
               },
               'vAlign'=>'middle',
               'format'=>'raw',
               'width'=>'150px',
               'noWrap'=>true
              ],
           
				/*[
				'header'=>'Exp.date',
				'value'=>function ($model) {
                        return   date("d/m",  strtotime($model->workorders->dateTo));
                   } 
              ],*/
              
              
    
			
         
            // 'updated_at',

        ],
     'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
       'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                //'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        
        // set export properties
     
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>Yii::t('app','My Stamp Cards'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
 <!--pop up for passes ---->
 <div class="modal fade" id="mygetModal"  role="dialog">
		
  </div>
 
  <!--- end here ---->  
		<!--  Read Terms Modal mygetModalread -->
		<div class="modal fade" id="mygetModalread" role="dialog">
		
		</div>
	<!--  end here -->
	
	<!--  stamp card Modal mygetModalstampcard  -->
		<div class="modal fade" id="mygetModalstampcard" role="dialog">
		<div class="modal-dialog">
		
		  <!-- Modal content-->
		  <div class="modal-content modelstampcontent">
			<div class="modal-header modelstampcardheader" >
			 
			  <h4 class="modal-title" style="text-align:center"> <b><?= Yii::t('app','BUY IN-STORE') ?></b> <p class="by-in-store-cl" data-dismiss="modal"><?= Yii::t('app','X') ?></p> </h4>
			</div>
			<div class="modal-body model-body-stampcard"  style="text-align:center">
			<p><?= Yii::t('app','Please enter Seller 6-digit PIN.') ?></p>	
			<input type="password" id="pin" class="pin" placeholder="Enter 6 Digit PIN">	
			<input type="hidden" id="stampCardValue" class="stampCardValue">		
			 <button type="button" class="submitpin" id="submitpin"><?= Yii::t('app','SUBMIT') ?></button>          
			</div>
			
		  </div>
		  
		</div>
		
		</div>
	<!--  end here -->

</div>
<script>
function getData($id){
	var workorderid = $id;
	//$('#mygetModal').css('display','block');
	//$('#mygetModal').addClass( "in" );
	//$( ".wrapper" ).append( "<div class='modal-backdrop fade in'></div>" );
	$('#mygetModal').modal('show');
	var baseurl = $('#baseurl').val();
	$.ajax({
	url:  baseurl ,
	data: { 
	workorderid:  workorderid
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		$( "div#mygetModal" ).html(data);
		return false;
	},
	
	})
	}
	

$(document.body).on("click", ".close",function() {
	//$('#mygetModal').css('display','none');
		//$('#mygetModal').removeClass( "in" )
		//$( ".modal-backdrop.in" ).remove();
	$('#mygetModal').modal('hide');
	return false;
	});
	
	$(document.body).on("click", ".read_popupopen",function() {
		
	$('#mygetModalread').css('display','block');
	$('#mygetModalread').addClass( "in" );
	//$('#mygetModalread').modal('show');
	var workorderid = $(this).attr('rel');
	
	var baseurl = $('#baseurlread').val();
	
	$.ajax({
	url:  baseurl ,
	data: { 
	workorderid:  workorderid
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		//alert(data);
		$( "div#mygetModalread" ).html(data);
		return false;
	},
	
	})
	
	});
	
	$(document.body).on("click", ".closereadmore",function() {
	$('#mygetModalread').css('display','block');
	$('#mygetModalread').addClass( "in" );
	return false;
	
	});
	$(document.body).on("click", ".close-stamp-card",function() {
		//alert('fff');
	/*$('#mygetModal').css('display','none');
	$('#mygetModal').removeClass( "in" );
	$( ".modal-backdrop.in" ).remove();*/
	$('#mygetModal').modal('hide');
	
	});
$(document.body).on("click", ".stm-buy",function() {
	
	var images = $(this).find('img').attr('src')
	if(images){
		return false;
	}else{
		$('#mygetModalstampcard').modal('show');
	/*$('#mygetModalstampcard').css('display','block');
	$('#mygetModalstampcard').addClass( "in" );*/
	//var stampCardValue = $(this).text();
	var stampCardValue = $(this).attr('rel');
	$('#stampCardValue').val(stampCardValue);
	 
	return false;
	}


	});	
	

	$( ".submitpin" ).click(function() {
	var stampCardValue = $('#stampCardValue').val();
	var arr = stampCardValue.split('-');
	//alert(arr[0]);
	//alert(arr[1]);
	
	var pin = $('#pin').val();
	var baseurl = $('#baseurlstampcard').val();
	
	$.ajax({
	url:  baseurl ,
	data: { 
	usedNo:  arr[0],
	workorder_id:  arr[1],
	pin : pin
	},
	type: 'POST',
	dataType: 'json',					
	success: function(data) {
		//alert(data);
		$('#mygetModalstampcard').css('display','none');
		//check workorder //
		if(data==1 || data ==2){
		var baseurl = $('#baseurlstampcardcount').val();
			$.ajax({
			url:  baseurl ,
			data: { 
			workorderid:  arr[1],
			
			},
			type: 'POST',
			dataType: 'html',					
			success: function(data) {
				$( "div#mygetModal" ).html(data);
				
			},
			
			})
		}
		if(data==3){
			$('#mygetModalstampcard').css('display','block');
			$('#mygetModalstampcard').addClass( "in" );
			alert('you have used your all paid and free offer');
			return false;
		}
		if(data==4){
			$('#mygetModalstampcard').css('display','block');
			$('#mygetModalstampcard').addClass( "in" );
			alert('That PIN does not match your registered 6-digit PIN. Please try again.');
			return false;
		}
		if(data==5){
			$('#mygetModalstampcard').css('display','block');
			$('#mygetModalstampcard').addClass( "in" );
			alert('Pin cannot be blank.');
			
			return false;
		}
		//end here
		
		return false;
	},
	
	})
	});	
		
$(document.body).on("click", ".stm-free",function() {
	var stampCardfree = $(this).text();
	alert(stampCardfree);
	});	
	
	$(document.body).on("click", ".by-in-store-cl",function() {
	/*$('#mygetModalstampcard').css('display','none');*/
	$('#mygetModalstampcard').modal('hide');
	//$('#mygetModalstampcard').removeClass( "in" );
	});	
	
	
	
	
	
</script>

<style>
	.prod_popup_had h1{ color: #000;
    float: left;
    font-size: 16px;
    margin: 0;
    padding: 4px 0;
    width: 57%;}



</style>
