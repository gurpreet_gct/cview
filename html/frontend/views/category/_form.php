<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Country;
use backend\models\Beaconmanager;
use backend\models\Storebeacons;
use common\models\User;
use backend\models\Categories;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Store */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-form">
   
   <?php  $form = ActiveForm::begin([ 
		'id' => 'create-store-form', 
		'options' => [ 
		'enctype' => 'multipart/form-data', 
		], 
		]); 
		?>
     <div class="box-body">
      <div class="form-group">

  
<label class="control-label" for="store-owner"> <?= yii::t('app','Select Categories') ?> </label>
  <div class="chosentree"></div> 
 </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
   
        <?= Html::submitButton(yii::t('app','Add') , ['class' => 'btn btn-success']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
 
 


<?php 
$this->registerJs(

    '
var loadChildren = function(node) {
	 
JSONObject =JSON.parse(\' '.$categoryTree.'\');	 

	 //JSONObject = JSON.parse(\'{"id":"01","title":"Node 01","has_children":true,"checked":true,"level":1,"children":[{"id":"011","title":"Node 011","name":"paras","has_children":true,"level":2,"children":[{"id":"0111","title":"Node 0111","has_children":false,"level":3,"children":[]}]}]}\');

	 //for(i=0;i<JSONObject.length;i++)
	 
		node.children.push(JSONObject);	 
	 console.log(node);
	 
        return node;
      };

$(document).ready(function() {
        $("div.chosentree").chosentree({
            width: 500,
            deepLoad: true,
              showtree: true,
            load: function(node, callback) {
                    setTimeout(function() {
          callback(loadChildren(node));
       }, 1000);
            }
        });
});
		'
);
    
    
$this->registerJsFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.js', ['position' => \yii\web\View::POS_END]);
$this->registerCssFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.css', ['position' => \yii\web\View::POS_END]);
//$this->registerJsFile(Yii::$app->homeUrl.'store-address-suggest.js', ['position' => \yii\web\View::POS_END]);
?>
	
