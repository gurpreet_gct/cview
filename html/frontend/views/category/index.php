<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Store */

$this->title = yii::t('app','All Category');
$this->params['breadcrumbs'][] = ['label' => yii::t('app','All Category'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-plus"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                
                <div class="panel-before" >    
                <div class="pull-right">
        <div role="toolbar" class="btn-toolbar kv-grid-toolbar">
            <div class="btn-group"><a title="<?= yii::t('app','All Category')?>" href="<?=Yii::$app->urlManager->createUrl(['category/index',])?>" class="btn btn-primary"><i class="glyphicon glyphicon-user"></i></a> 
            <a data-pjax="0" title="<?= yii::t('app','Reset Grid') ?>" href="<?=Yii::$app->urlManager->createUrl(['category/index',])?>" class="btn btn-default"><i class="glyphicon glyphicon-repeat"></i></a></div>


        </div>    
    </div>
    
    <div class="clearfix"></div></div>
                
              
                
                <!-- form start -->
                
                 <?= $this->render('_form', array('categoryTree'=>$categoryTree)) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
