<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Workorders;
use common\models\User;

/**
 * @var yii\base\View $this
 */

$this->title = 'My Orders';
$model = $dataProvider;

//echo '<pre>'; print_r($model); die;

?>


	
 <!-- Main content -->
       <?php echo '<section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
			 <div class="col-sm-4 invoice-col">	
			  <h1>'.$model["brandName"].'</h1>			 
			 <img src="'.$model["brandLogo"].'"/>
			
			 <p>'.$model["brandDescription"].'</p>
			
			 </div>
			  
			 <div class="col-sm-8 invoice-col">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">My Order Summary </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th>Order Detail</th>
                      <th>Qty</th>
                      <th>Price</th>                     
                    </tr>
                    <tr>
                      <td>'.$model["offertitle"].'</td>                     
                      <td>'.$model["qty"].'</td>
                      <td>$'.$model["effectedtprice"].'</td>
                    </tr>'; 
                   
                      if(isset($model["comboProduct"]["id"])) {
					  echo '<tr>
							<td>Combo Product</td>
							<td>'.$model["comboProduct"]["qty"].'</td>                      
							<td>'.$model["comboProduct"]["effectedtprice"].'</td>
							</tr>';
						}
					echo '<tr>
							<td><b> Total </b></td>
							<td> </td>
							<td><b>$'.$model["effectedtprice"].'</b>(inc.tax)</td>
							</tr>';	
					 echo '</tbody></table>
							</div><!-- /.box-body -->
							<div class="box-footer clearfix">
							
							
							<a class="btn btn-danger" href="">CANCEL</a>
							
							<a class="btn bg-green" href="">PAY NOW</a>
							
							</div>
							</div><!-- /.box -->
             
            </div>
            
          </div>';
 
 

