<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CardtypeSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','My Wallet');
$this->params['breadcrumbs'][] = $this->title;
$request_data = Yii::$app->request->get();
if(isset($request_data['wid']) && $request_data['wid']!=''){?>
	<script>
		$( document ).ready(function() {
		$id = "<?php echo $request_data['wid']; ?>";
		var workorderid = $id;
		$('#mygetModal').css('display','block');
	$('#mygetModal').addClass( "in" );
	$( ".wrapper" ).append( "<div class='modal-backdrop fade in'></div>" );
	var baseurl = $('#baseurl').val();
	$.ajax({
	url:  baseurl ,
	data: { 
	workorderid:  workorderid
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		$( "div#mygetModal" ).html(data);
	},
	
	})
	
    
});
	
</script>
<?php 	 }
?>
<div class="cardtype-index">

 <input type="hidden" id="baseurl" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/redeemed/getredeemedpassview';?>">
 <input type="hidden" id="baseurlread" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/redeemed/getreadmore';?>">
<?php
//profile.brandLog
        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns' => [
				[
              'header'=>Yii::t('app','Logo'),
              'value' => function ($data) {
                       return  Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$data->profile->brandLogo)
                        ;
                    },
                  'format' => ['image',['width'=>'70','height'=>'70']] , 
               /*'onclick'=>'actionTypes($(this).attr("workorder_id"))'*/
              ],
		
              [
              'header'=>Yii::t('app','offerTitle'),
               
               'value'=>function ($model) {
                return "<a href='javascript:void(0);' onclick='getData($model->workorder_id)' data-target='mygetModal' data-toggle='modal' class='small-box-footer'> $model->offerTitle</a>";
               },
               'vAlign'=>'middle',
               'format'=>'raw',
               'width'=>'150px',
               'noWrap'=>true
              ],
			
			
			 [
              'header'=>Yii::t('app','Exp.date'),
               
               'value'=>function ($model) {
                return "<a href='javascript:void(0);' onclick='getData($model->workorder_id)' data-target='mygetModal' data-toggle='modal' class='small-box-footer'>".date('d/m',  strtotime($model->workorders->dateTo))."</a>";
               },
               'vAlign'=>'middle',
               'format'=>'raw',
               'width'=>'150px',
               'noWrap'=>true
              ],
           
				/*[
				'header'=>'Exp.date',
				'value'=>function ($model) {
                        return   date("d/m",  strtotime($model->workorders->dateTo));
                   } 
              ],*/
              
              
    
			
         
            // 'updated_at',

        ],
     'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
       'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                //'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        
        // set export properties
     
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>Yii::t('app','My Redeemed Pass'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
 <!--pop up for passes ---->
 <div class="modal fade" id="mygetModal"  role="dialog">
	
  </div>
 
  <!--- end here ---->  
		<!--  Read Terms Modal mygetModalread -->
		<div class="modal fade" id="mygetModalread" role="dialog">
		
		</div>
	<!--  end here -->

</div>
<script>
function getData($id){
	var workorderid = $id;
	$('#mygetModal').css('display','block');
	$('#mygetModal').addClass( "in" );

	$( ".wrapper" ).append( "<div class='modal-backdrop fade in'></div>" );
	var baseurl = $('#baseurl').val();
	
	$.ajax({
	url:  baseurl ,
	data: { 
	workorderid:  workorderid
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		$( "div#mygetModal" ).html(data);
	},
	
	})
	}
	

$(document.body).on("click", ".close-redeemed-pass",function() {
	$('#mygetModal').css('display','none');
		$('#mygetModal').removeClass( "in" )
		$( ".modal-backdrop.in" ).remove();
	
	});
	
	$(document.body).on("click", ".read_popupopen",function() {
		
	$('#mygetModalread').css('display','block');
	$('#mygetModalread').addClass( "in" )
	var workorderid = $(this).attr('rel');
	
	var baseurl = $('#baseurlread').val();
	
	$.ajax({
	url:  baseurl ,
	data: { 
	workorderid:  workorderid
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		$( "div#mygetModalread" ).html(data);
	},
	
	})
	
	});
</script>

<style>
	
	@import url(https://fonts.googleapis.com/css?family=Montserrat:400,700);
	
	
.modal-dialog {
  margin: 30px auto;
  width: 300px;
  font-family: 'Montserrat', sans-serif;
}
/*.modal-content{ height:380px; }*/
.icon-ms {
	background-color:#F3F3F3;
	}
	.prod_popup_had h1{ font-family: 'Montserrat', sans-serif; font-weight:400; color: #555 !important; }
.prod_popup_had h1{ color: #000;
    float: left;
    font-size: 16px;
    margin: 0;
    padding: 4px 0;
    width: 100%;}

.prod_popup_had .modal-header { border:none;}

.prod_popup_had  .modal-footer { border:none;}

.prod_popup_had  .poup_hed_boder{   border-bottom: 1px solid #999;
    float: left;
    padding: 0 0 14px;
    width: 100%;}

.prod_popup_had  .modal-content { border: 0 none; border-radius:4px;}

.prod_popup{ width:100%; float:left;}

.prod_popup .modal-body {
    padding:0px;}

.pup_img{  margin: 10px 0; width: 100%; position: relative;}
	
	
.pup_img img{ /*height: auto !important;*/ width: 100% !important;}

.pup_img_logo{ bottom: 0; float: right;  left:auto; position: absolute; right: 0;  top: 10px;  z-index: 999999999;}

.c{ width:100%; padding:0px 0px 0px 0px; margin:0px 0px 0px 0px;}

.bookmark_part{ float: left; text-align: left; width: 100%; padding-top: 11px; }
.bookmark_part strong, .bookmark_part b{ font-weight:400; }
.pop-up-image_bar_code { margin: 0; padding: 0; width: 100%;}
.grey-bottom{ background-color: #dfdfdf; margin-top: -10px;  padding: 7px 15px; }
.part_val{ float: left; text-align: left; width: 100%;}

.pop-up-image_buton{ margin: 0; padding: 0; text-align: center;  width: 100%;}

.pop-up-image_buton .btn-info { background-color:#08afb5; border-color: #00acd6; text-transform: uppercase; 
	border-radius:25px;   font-size: 14px; font-weight: 400; font-family: 'Montserrat', sans-serif;  }


.text_though{ text-decoration: line-through;  color: #999; padding:8px 10px 0; font-size: 13px; font-weight:400; font-family: 'Montserrat', sans-serif;  }


.pupup_doller{max-width: 100%; min-width:40px;  padding:0px 0 0 15px;  font-size:17px;}
    
    
.read_popup{ color: #444; float: right; font-family: 'Montserrat', sans-serif; font-size: 13.4px; line-height:27px; padding: 0px 15px 0 0; width: 40%; }
.read_popup b{ font-weight:400; }
    
 .modal-header .close{ margin-right:-10px;  margin-top: -16px; font-size: 29px; font-weight:400; }
 
 .pupup_doller b span{ font-size:21px; font-family: 'Montserrat', sans-serif;  }
	
@media all and (max-width:480px) {
	
.modal-dialog {
    margin: 30px auto;
    width: auto;
}

.pup_img_logo {
    left:213px;}

	}
	
	.model-title-redeemed-pass {
	float: left;
    width: 100% !important;
    background-color: #08afb5;
    color:white;
    height: 32px;
    text-align:center;
	}
	.modal-header { padding:0px;}
	
  .close-redeemed-pass {
	background-color: #08afb5 !important;
	float: right;
	cursor: pointer;
    padding: 2px 10px;
  }
  .h1-modal-title {
  margin: 0 0 0 13px !important;
}
	.model-content-pass-readmore {
  height: 600px;
}
.model-header-pass-readmore {
	padding:0px;
	background-color:#08afb5;
	color:white;
	height:35px;	
		}
		.model-dialog-pass-readmore {
  width: 317px !important;
}
.closereadmore {
  background-color: #08afb5 !important;
  float: right;
   cursor: pointer;
    padding: 2px 10px;
}
 .pop-bt-cl {
		 background-color:#F3F3F3;
		 margin-bottom: -13px;
		 } 
</style>
