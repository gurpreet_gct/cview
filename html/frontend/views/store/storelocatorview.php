<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;

/* @var $this yii\web\View */
/* @var $model common\models\Cardtype */



 
$this->title =  Yii::t('app','My Stores'); 
$this->params['breadcrumbs'][] = $this->title; 

?>


     <section class="content">
		 <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">  <?php echo $model['storename']; ?> </h3>            
            </div>
            
		        
       
			
            <div class="box-body box-header with-border" style="display: block;">
				<b> <?= Yii::t('app','Address :') ?> </b> </br>
             <?php echo $model['address']; ?> 
            <P></P>
          <b> <a href="<?php echo 'https://www.google.com/maps?q=loc:'.$model["latitude"].','.$model["longitude"].',14z?hl=en-US'; ?> "> <?= Yii::t('app','GET DIRECTIONS :') ?> </a> </b> </br>
         
           <P></P>
				   
		<!-- Show map -->
		<?php  
		$coord = new LatLng(['lat' => $model['latitude'], 'lng' => $model['longitude']]);
		$map = new Map([
			'center' => $coord,
			'zoom' => 14,
		]);

		// Lets configure the polyline that renders the direction
		$polylineOptions = new PolylineOptions([
			'strokeColor' => '#FFAA00',
			'draggable' => true
		]);


		// Lets add a marker now
		$marker = new Marker([
			'position' => $coord,
			'title' => $model['storename'],
		]);

		// Provide a shared InfoWindow to the marker
		$marker->attachInfoWindow(
			new InfoWindow([
				'content' => $model['storename'],
				
			])
		);

		// Add marker to the map
		$map->addOverlay($marker);

		// Lets show the BicyclingLayer :)
		//$bikeLayer = new BicyclingLayer(['map' => $map->getName()]);

		// Append its resulting script
		$map->appendScript($map->getJs());

		// Display the map -finally :)
		
		 echo $map->display();
		 
		 ?>
			<P></P>
			<b><?= Yii::t('app','Mobile :') ?> </b>
            <?php echo $model['phoneNumber']; ?>
            <P></P>
            
            <b><?= Yii::t('app',' Email :') ?> </b>
            <?php echo $model['email']; ?>
            <P></P>
            
             <b><?= Yii::t('app',' Opening Hours :') ?> </b> 
             <?php echo $model['addopeninghours']; ?>
            <P></P>
            
            
          
            </div><!-- /.box-body -->
            
         
         
           </div>
 </section>
 

