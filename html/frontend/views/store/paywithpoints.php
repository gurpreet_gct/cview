<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Workorders;
use common\models\Store;
use common\models\User;

/**
 * @var yii\base\View $this
 */

$this->title = yii::t('app','Pay for Deals with points');
$models = $dataProvider->getModels();
?>


	
 <!-- Main content -->
        <section class="content">
			 <input type="hidden" id="baseurl" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/deal/addtowallet';?>">
			 <input type="hidden" id="baseurlbookmark" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/deal/bookmark';?>">
          <!-- Small boxes (Stat box) -->
          <div class="row">
			
			  
		<?php 
		
		 foreach ($models as $model) {  ?>	  
            <div class="col-md-4">
			<?php /* get Advertiser name */
			 $Wid = Workorders::find()->select('workorderpartner')->where(['id' => $model->workorder_id])->one();
			 $Advname = User::find()->select('orgName')->where([ 'id' =>$Wid->workorderpartner])->one();  
			?>	
              <!-- small box -->
              <div class="col-md-12">
					  <div class="row">
					  <div class="deal-main-img">
						  <!---$model->mainPhotoUpload--->
						  <img class="deal-image" src="<?php echo $model->mainPhotoUpload;?>" />
						  </div>
						  <div class="deal-img-logo">
							  <!---$model->logoUpload--->
								<img class="deal-logo" src="<?php echo $model->logoUpload;?>" /></div>
					  </div>
				</div>
              
           <div class="col-md-12 deal-section">
			  <h4> <?php echo $model->dealTitle; ?></h4>
			   <h5><?php echo $Advname->orgName; ?></h5>
			  
			   
			   <div class="col-md-12 padding-zero" >
				<?php
					if($model->offernow!=0) { ?>
                  <h1><b><?php echo '$'.$model->offernow; ?></b> <sub style="font-size: 16px;  color: #999; text-decoration: line-through;"><?php echo '$'.$model->offerwas; ?></sub></h1>
                 <?php } else { ?>
					   <h1><b><?php echo $model->text;  ?></b> </h1>
					 <?php } ?>
					 <p>
					 <a href="javascript:void(0)" data-target="#myModal-<?php echo $model->id; ?>" data-toggle="modal" class="small-box-footer share-btn"> <?= yii::t('app','Share')?> </a>
                
                <a href="javascript:void(0)" data-target="#mygetModal-<?php echo $model->id; ?>" data-toggle="modal" class="small-box-footer get-btn"><?= yii::t('app','Get')?> </a>
                </p>
                 </div>
			   </div>
			  
			</div>
            
    <!--  Share Modal -->
	<div class="modal fade" id="myModal-<?php echo $model->id; ?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> <b> <?= yii::t('app','Share this') ?> <?php echo $model->text;  ?> </b> </h4>
        </div>
        <div class="modal-body"  style="text-align:center">
       <!-- Twitter -->
		<a href="https://twitter.com/intent/tweet?url=<?= Url::toRoute(['view' , 'id'=>$model->id])?>&text= <?php echo $model->dealTitle; ?>"  title="<?= yii::t('app','Share on Twitter') ?>" target="_blank" class="btn btn-twitter"><i class="fa fa-twitter"></i> <?= yii::t('app','Twitter') ?></a>
		 <!-- Facebook -->
		<a href="https://www.facebook.com/sharer/sharer.php?u=<?= Url::toRoute(['view' , 'id'=>$model->id])?>" title="<?= yii::t('app','Share on Facebook') ?>" target="_blank" class="btn btn-facebook"><i class="fa fa-facebook"></i> <?= yii::t('app','Facebook') ?> </a>
		<!-- Google+ -->
		<a href="https://plus.google.com/share?url=<?= Url::toRoute(['view' , 'id'=>$model->id])?>" title="<?= yii::t('app','Share on Google+') ?>" target="_blank" class="btn btn-googleplus"><i class="fa fa-google-plus"></i> <?= yii::t('app','Google+') ?> </a>

		<!-- LinkedIn --> 
		<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= Url::toRoute(['view' , 'id'=>$model->id])?>&title=<?php echo $model->dealTitle;  ?>&summary=<?php echo $model->dealTitle;  ?>" title="<?= yii::t('app','Share on LinkedIn') ?>" target="_blank" class="btn btn-linkedin"><i class="fa fa-linkedin"></i><?= Yii::t('app','LinkedIn') ?></a>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"> <?= yii::t('app','Close') ?></button>
        </div>
      </div>
      
	</div>
	</div>
	
		
		
	<div class="modal fade" id="mygetModal-<?php echo $model->id; ?>"  role="dialog">
		<div class="modal-dialog prod_popup_had">
			
		<!-- Modal content-->
      <div class="modal-content model-content-deal">
		  <div class="poup_hed_boder">
			<div class="modal-header deal-model-header ">
			<h1><?php if(isset($model->dealTitle)) { echo $model->dealTitle; } ?> </h1>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div> 
		<div class="col-md-12">
			<div class="part_val">
				<b><?php if(isset($Advname->orgName)) { echo $Advname->orgName; } ?></b> 
				</div></div>
				<div class="col-md-12">
			<div class="bookmark_part" >
				<?php if ($model->bookmarkstatus($model->id) == 1) { ?>
				<img src="<?php echo Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("images/bookmark.png");?>" style="height: 16px; margin: 0 5px 0 0; width: 16px;" class="bookmarkdeal bkmark-deal" rel="<?php echo  $model->id;?>" ><b class="bookmarkdeal bkmark-deal" rel="<?php echo  $model->id;?>"><?= yii::t('app','Unbookmark this deal') ?></b>
				<?php } else { ?>
					<img src="<?php echo Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("images/unbookmark.png");?>" style="height: 16px; margin: 0 5px 0 0; width: 16px;" class="bookmarkdeal bkmark-deal" rel="<?php echo  $model->id;?>" ><b class="bookmarkdeal bkmark-deal" rel="<?php echo  $model->id;?>"><?= yii::t('app','Bookmark this deal') ?></b>
					<?php } ?>
				</div></div>		
				</div>
        <div class="prod_popup">
        <div class="modal-body">
		<div class="col-md-12">
            <div class="row" >
				<div class="pup_img"><img class="pop-up-image" src="<?php if(isset($model['workorderpopup']->photo)) { echo $model['workorderpopup']->photo;} ?>" /></div>
                 <div class="pup_img_logo">
                 <img class="pop-up-logo"  src="<?php if(isset($model['workorderpopup']->logo)) { echo $model['workorderpopup']->logo;} ?>" /></div>
				 </div></div>
                 
              <div class="col-md-12 grey-bottom">
            <div class="row">
            
				<div class="pupup_doller" style="text-align: left; float:left; color:#08afb5;"><b><?php 
				/*if($model->type == 1) { 
					if($offermodel->offerText !=''){
					 echo $offermodel->offerText; }else{
						  echo"$".$offermodel->offernow;
						 } 
					}
					else if($model->type == 2) {
						  if($offermodel->offerText !=''){
						echo $offermodel->offerText;	 
					 }else{
						  echo"$".$offermodel->offernow;
						 } 
					 }
					 else {
					 if($model->type == 4 ) {
						
						echo"$".$offermodel->offernow;
					
					 }
				 }*/
				
					 
			?>
			<?php
					
				if($model->offernow!=0) { 
                   echo '$'.$model->offernow; } else { ?>
					   <h1 style="color:#08afb5;"><b><?php echo $model->text;  ?></b> </h1>
					 <?php } ?>
			</b>
                      
                     </div>
                      <?php if($model->offernow!=0) {?>
                      	<div class="col-md-1 text_though">
					<?php echo"$".$model->offerwas;?>
					</div>
					<?php } ?>
                      
                      
                      
				<div class="read_popup"  style="text-align: right;">
					<img src="<?php echo Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/imgpsh_fullsize1.png");?>" style="height: 16px; margin: 0 5px 0 0; width: 16px;" ><b class="deal-normal-weight"><a  style="color:black;" href="javascript:void(0)"  data-target="#mygetModalread-<?php echo $model->id; ?>" data-toggle="modal" class="small-box-foote1"><?= yii::t('app','Read Terms')?> </a></b></div>
				</div>
				
			</div>
			 <div class="col-md-12">
				
                 
                 
             <!--  <div class="pop-up-image_bar_code ">
                 
          <p><img class="pop-up-image" style="width:300px; height:100px;" src="<?php //if(isset($offermodel->passUrl)) { echo $offermodel->passUrl; } ?>" /> </p>
          </div>-->
          
          
             <div class="pop-up-image_buton">
                 <p><button  id="addToMyWallet" data-target="#myModal" data-toggle="modal" class="btn btn-info addToMyWallet" type="button" rel="<?php echo  $model->workorder_id;?>"  data-id="<?php echo $model->id;?>"> <?= yii::t('app','Add to my wallet') ?>
          </button></p>
          
          </div>
          
          </div>

        </div>
       
      
       </div>
       
       
       
       
        <div class="modal-footer">
          
        </div>
      </div>
      
    </div>
  </div>
 
    
		
		
		
		<!--  view Modal -->
	<div class="modal fade" id="mygetModal1-<?php echo $model->id; ?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> </h4>
        </div>
        <div class="modal-body">
			
			
				
				<div class="small-box bg-aqua" style="margin-bottom: 0px;">
                <div  class="inner">
					                  <h4> <?php echo $model->dealTitle; ?> </h4>
					                   <h5> <b> <?php echo $Advname->orgName; ?> </b> </h5>
                                 
									  <?php if ($model->bookmarkstatus($model->id) == 1) { ?>
									   <h4>
									  <a href="<?= Url::toRoute(['unbookmark', 'id' => $model->id] )?>" style="color:#FFFFFF;" class="small-box-footer"> <i class="fa fa-bookmark"></i> <?= yii::t('app','Unbookmark this deal') ?>  </a>  </h4>
									  <?php }else { ?>
										 <h4>
									  <a href="<?= Url::toRoute(['bookmark', 'id' => $model->id] )?>" style="color:#FFFFFF;" class="small-box-footer"> <i class="fa fa-bookmark-o"></i> <?= yii::t('app','Bookmark this deal') ?>   </a>  </h4> 
									  <?php } ?>
                </div>
               
              
              </div>
              
              <!-- small box -->
              <div class="small-box" style="margin-bottom: 0px;">
				  
				  <?php echo '<img height="100%" width="100%" src="'.$model['workorderpopup']->photo.'">'; ?>
				  </div>
          
           <div class="small-box bg-aqua">
                <div class="inner">
					<?php  if($model->offernow!=0) { ?>
                  <h2><?php echo '$'.$model->offernow; ?> <sup style="font-size: 20px; text-decoration: line-through;"><?php echo '$'.$model->offerwas; ?></sup></h2>
                 <?php } else { ?>
					   <h2><?php echo $model->text; ?> </h2>
					 <?php } ?>
					 
					 <p> <a href="javascript:void(0)" style="color:#FFFFFF;" data-target="#mygetModalread-<?php echo $model->id; ?>" data-toggle="modal" class="small-box-footer"> <?= yii::t('app','Read Terms') ?> <i class="fa fa-info-circle"></i></a> </p>
					  <a class="small-box-footer" style="color:#FFFFFF;" target="_blank" href="javascript:void(0)"> <i class="fa fa-arrow-circle-right"></i> <?= yii::t('app','Add to Passbook') ?>   </a>
               
                </div>
              
                
               <!-- <a href="javascript:void(0)" target="_blank" style="width:50%; float:left; background:#00A3CB;" class="small-box-footer">GET OFFLINE <i class="fa fa-arrow-circle-right"></i></a>-->
                
                
                <?php if($model['workorderpopup']->buynow !== 0) {  ?>
                <!--<a href="<?= Url::toRoute(['myorder/view', 'id' => $model->workorder_id] ) ?>" style="width:50%; float:left; background:#00A3CB;" class="small-box-footer"> <?= yii::t('app','BUY NOW') ?>  <i class="fa fa-arrow-circle-right"></i></a> -->
                <button data-target="#myModal" data-toggle="modal" class="btn btn-info" type="button"> <?= yii::t('app','Add to my wallet') ?>
          </button>
                <?php } else { 
				echo '<a href="javascript:void(0)" style="width:50%; float:left; background:#00A3CB;" class="small-box-footer">Click Here <i class="fa fa-arrow-circle-right"></i></a>';	
				 } ?>
              </div>
            </div> 
            
            
      
      
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"> <?= yii::t('app','Close')?></button>
        </div>
      </div>
      
		</div>
		</div>
		
	
	<!--  Read Terms Modal mygetModalread -->
	<div class="modal fade" id="mygetModalread-<?php echo $model->id; ?>" role="dialog">
    <div class="modal-dialog model-dialog-pass-readmore">
    
      <!-- Modal content-->
      <div class="modal-content model-content-pass-readmore">
        <div class="modal-header model-header-pass-readmore">
          <p class="closereadmore" data-dismiss="modal">X<p>
          <h4 class="modal-title" style="text-align:center"> <b> <?= yii::t('app','Read More')?> </b> </h4>
        </div>
        <div class="modal-body overflow-text">
               <?php if($model['workorderpopup']->readTerms != '') { echo $model['workorderpopup']->readTerms;  } ?> 
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      
	</div>
	</div>
            
            <?php } ?>
            
          </div><!-- /.row -->
    <style>
    .bkmark-deal { cursor:pointer;}
    </style>      
          
<script>
	$( document ).ready(function() {
	$(".addToMyWallet").click(function () {
	var id = $(this).attr('rel');
	var popid = $(this).attr("data-id");
	
	var baseurl = $('#baseurl').val();
	
	$.ajax({
	url:  baseurl ,
	data: { 
	workorderid:  id
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		$('#mygetModal-'+popid).css('display','none');
		$('#mygetModal-'+popid).removeClass( "in" )
		$( ".modal-backdrop.in" ).remove();
		$('#mygetModal-'+popid).modal('hide');
		if(data==4){
			alert("You have already redeemed that pass.");
			window.location.href = "<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl();?>"+"/wallet/index?id=redeem&wid="+id;
			
		return false;
		}if(data==5){
			alert("You have already added this pass to your wallet. It can be found in My Passes in My Wallet.");
			window.location.href = "<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl();?>"+"/wallet/index?id=pass&wid="+id;
		return false;
		}
		if(data==6){
			alert("You have already added this digital stamp card to your wallet. It can be found in My Stamp Cards in My Wallet.");
			window.location.href = "<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl();?>"+"/wallet/index?id=digital&wid="+id;
		return false;
		}
		if(data==7){
			alert("You have already redeemed that event ticket.");
			window.location.href = "<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl();?>"+"/wallet/index?id=redeemedticket&wid="+id;
			
		return false;
		}
		if(data==8){
			return false;
		}
		
	},
	
	})
});
   
});
 
 $(document.body).on("click", ".bookmarkdeal",function() {
	var dealid = $(this).attr('rel');
	var baseurl = $('#baseurlbookmark').val();
	$.ajax({
	url:  baseurl ,
	data: { 
	dealid:  dealid
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		if(data==1){
			//var imagepath ='<?php echo Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("images/bookmark.png");?>';
			$('.bookmark_part').html('<img rel="'+dealid+'" class="bookmarkdeal bkmark-deal" style="height: 16px; margin: 0 5px 0 0; width: 16px;" src="<?php echo Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("images/bookmark.png");?>"><b class="bookmarkdeal bkmark-deal" rel="'+dealid+'">Unbookmark this deal</b>');
			return false;
		}
		if(data==0){
			//var imagepath ='<?php echo Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("images/unbookmark.png");?>';
			$('.bookmark_part').html('<img rel="'+dealid+'" class="bookmarkdeal bkmark-deal" style="height: 16px; margin: 0 5px 0 0; width: 16px;" src="<?php echo Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("images/unbookmark.png");?>"><b class="bookmarkdeal bkmark-deal" rel="'+dealid+'">Bookmark this deal</b>');
		}
		
	},
	
	})
	});   
     </script>     
 

