<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Cardtype */

$models = $dataProvider->getModels(); 


 
$this->title =  $orgName; 
$this->params['breadcrumbs'][] = $this->title; 



?>


     <section class="content">
		 <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">  <?php echo count($models); ?><?= Yii::t('app','Stores found near you') ?></h3>            
            </div>
            
	<?php foreach($models as $model ){    ?> 		        
       
			
            <div class="box-body box-header with-border" style="display: block;">
				<b> <?php echo $model['storename']; ?> </b> </br>
             <a href="<?= Url::toRoute(['storelocatorview' , 'id'=>$model['id'], 'lat'=>$model['latitude'], 'lng'=> $model['longitude']])?>"> <?php echo $model['address']; ?> ,<b> <?php echo number_format($model['distance'], 2, '.', '');  ?> <?= Yii::t('app','KM') ?></b> </a>
            <P></P>
          
            </div><!-- /.box-body -->
            
         
          <?php } ?>
           </div>
 </section>
 

