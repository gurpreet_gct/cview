<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = Yii::t('app','Continue to Menu');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app','In Progress') ?> </p>


</div>

