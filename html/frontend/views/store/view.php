<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Cardtype */


 
$this->title = Yii::t('app','My Stores');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Store'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$lat = '';
$lng = '';
?>

<script>


$( document ).ready(function() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }


	function showPosition(position) {
		var lat = +position.coords.latitude; 
		 var longi = + position.coords.longitude;
		var anchorHref = $('#storel').attr('href');
		
		$('#storel').attr('href',anchorHref+'&lat='+lat+'&lng='+longi);
	}

});

</script>

<?php  //echo '<pre>'; print_r($model); die; ?>

         <section class="content">
			<div class="box">
            <div class="box-header with-border">
               <img src="<?php echo $model['profiles']->brandLogo; ?>"/>  </br> <h3 class="box-title"> <?php // echo ucfirst($model->orgName); ?> 
               <p></p><p>
				   <?php if($model->storefavourite($model->id)==1) { ?>
				<a href="<?= Url::toRoute(['delete', 'id' => $model->id] )?>"> <i class="fa fa-star"></i> <?= Yii::t('app','Remove to favourite') ?> </a> 
				<?php }else{ ?>
					<a href="<?= Url::toRoute(['update', 'id' => $model->id] )?>"> <i class="fa fa-star-o"></i><?= Yii::t('app',' Add to favourite') ?> </a> 					
				<?php }?>
				</p> </h3>
              <div class="box-tools pull-right">
                <!--<button title="" data-toggle="tooltip" data-widget="collapse" class="btn btn-box-tool" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
              <!--  <button title="" data-toggle="tooltip" data-widget="remove" class="btn btn-box-tool" data-original-title="Remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body" style="display: block;">
				<b> <?php echo ucfirst($model->orgName); ?> </b> </br>
				
             <?php  echo $model->address->houseNumber.', '.$model->address->city.', '.$model->address->state.', '.$model->address->country; ?>
            <P></P>
          
           <button type="button" class="btn btn-info" style="padding:0px 3px;" data-toggle="modal" data-target="#myModal"><?= Yii::t('app','READ MORE') ?></button>
            </div><!-- /.box-body -->
            <div style="background:#F9F9F9" class="box-footer" style="display: block;">
                 <i class="fa fa-tag"></i> <a href="<?= Url::toRoute(['currentdeals' , 'id'=>$model->id ])?>"><?= Yii::t('app','CURRENT DEALS') ?> </a>
            </div><!-- /.box-footer-->
            <div class="box-footer" style="display: block;">
              <i class="fa fa-map-marker"></i> 
            
               <a id="storel" href="<?= Url::toRoute(['storelocator' , 'id'=>$model->id ])?>"> <?= Yii::t('app','STORE LOCATOR') ?> </a>
               
            </div><!-- /.box-footer-->
            <div style="background:#F9F9F9" class="box-footer" style="display: block;">
              <i class="fa  fa-heart"></i><a href="<?= Url::toRoute(['loyalty','id'=>$model->id]) ?>"> <?= Yii::t('app',' LOYALTY') ?> </a>
            </div><!-- /.box-footer-->
            <div class="box-footer" style="display: block;">
               <i class="fa fa-shopping-cart"></i> <a href="<?= Url::toRoute(['ordernow','id'=>$model->id]) ?>"><?= Yii::t('app','Order NOW') ?> </a>
            </div><!-- /.box-footer-->
          </div>
 </section>


<!-- Read More Model -->
<div class="container">
  
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title"><?php  echo ucfirst($model->orgName); ?> </h3>
        </div>
        <div class="modal-body">
         <?php  echo $model['profiles']->brandDescription; ?> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Close') ?></button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<!-- Read More Model End -->

</body>
</html>

