<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\Qr;
$id=$_GET['id'];
?>
 <?php
 $form = ActiveForm::begin([
                'options' => [                 
                    'enctype' =>'multipart/form-data'
                ]
    ]);
?>
<?php if(!$model){ ?>
<center><label>You have earned Zero points</label></center></br>
<center><img src="<?= Yii::$app->urlManagerApi->createAbsoluteUrl("/v1/barcodes?id=".Yii::$app->user->id."-".$id) ?> "/></center> 

<?php }
else{ ?>
<center><label>You have earned &nbsp </label><?= $model->points ?><label> &nbsp points</label></center><br/>
<center><img src="<?= Yii::$app->urlManagerApi->createAbsoluteUrl("/v1/barcodes?id=".$model->loyaltyID)?>"/ ></center>
<?php } ?>
<br/> 
<center><label>Cant scan barcode?</label></center>
<!-- Trigger the modal with a button -->
  <center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Click here to get your loyalty points</button></center>
 <button type="button" id='show-addloyalty-target' class="btn btn-info btn-lg" style="display:none" data-toggle="modal" data-target="#myModal"><?= Yii::t('app','Open Modal') ?></button>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"align="center">GET LOYALTY POINTS</h4>
        </div>
        <div class="modal-body">
          <p>Collect loyalty points by entering total amount, tranaction ID and seller 6 digit PIN</p>
		  <?= $form->field($SLoyaltypoint, 'amount')->textInput(['maxlength' => true,'placeholder'=>'$0.00'])->label('Amount'); ?>
		  <?= $form->field($SLoyaltypoint, 'pin')->textInput(['maxlength' => true,'placeholder'=>'Seller 6 digit PIN'])->label('Seller 6 digit PIN'	); ?>
		  <?= $form->field($SLoyaltypoint, 'transaction_id')->textInput(['maxlength' => true,'placeholder'=>'Enter tranaction id here'])->label('Enter transaction id'); ?>
		   <input type="hidden" id="storeid" value="<?php $SLoyaltypoint->store_id ?>">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
        </div>
      </div> 
    </div>
	</div>
<br/><center><?= Html::a(yii::t('app','PAY FOR DEAL WITH POINTS'),  ['paywithpoints','id' =>$id], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> yii::t('app','Pay for deal With Points')]) ?></center>
<?php ActiveForm::end(); ?>
	
