 <?php
 
 use yii\helpers\Html;
 use yii\helpers\Url;
 $usertype = Yii::$app->user->identity->user_type;
 $usertypeid = Yii::$app->user->identity->id;
  ?>
  <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">              
               <?php  if(Yii::$app->user->identity->image!='') {  echo Html::img(Yii::$app->params['userUploadPath']."/".Yii::$app->user->identity->image ,array('class'=>'img-circle','alt'=>'User Image')); } else {
				   echo Html::img(Yii::$app->params['userUploadPath']."/".'no-image.jpg',array('class'=>'img-circle','alt'=>'User Image'));
				   
				  } ?>
            </div>
            <div class="pull-left info">
              <p><?=Yii::$app->user->identity->fullName;?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
         <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..." />
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
           <!-- <li class="header">MAIN NAVIGATION</li>
           <!-- <li class="active treeview">
              <a href="<?php // Url::toRoute(['site/index'])?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>              
            </li>-->
          
      
            
            <li class="treeview">
			<a href="<?= Url::toRoute(['deal/index'])?>">
			<i class="fa fa-newspaper-o"></i>
			<span>My Deals</span>			
			</a>			
			</li>   
			
			<li class="treeview">
			<a href="<?= Url::toRoute(['store/index'])?>">
			<i class="fa fa-home"></i>
			<span>My Store</span>			
			</a>
			</li> 
			
			<li class="treeview">
			<a href="<?= Url::toRoute(['wallet/index'])?>">
			<i class="fa fa-credit-card"></i>
			<span>My Wallets</span>			
			</a>
			</li> 
			
			<li class="treeview">
			<a href="<?= Url::toRoute(['user/mysweets'])?>">
			<i class="fa fa-gift"></i>
			<span>Sweet Shop </span>			
			</a>
			</li> 
			
			<li class="treeview">
			<a href="<?= Url::toRoute(['user/help'])?>">
			<i class="fa fa-bell"></i>
			<span>Help </span>			
			</a>
			</li> 
			
			<li class="treeview">
			<a href="<?= Url::toRoute(['user/aboutus'])?>">
			<i class="fa fa-bullseye"></i>
			<span>About </span>			
			</a>
			</li> 
			
			<li class="treeview">
			<a href="<?= Url::toRoute(['bookmarkdeal/index'])?>">
			<i class="fa fa-bookmark-o"></i>
			<span>Bookmarked Deals </span>			
			</a>
			</li> 
			
			<li class="treeview">
			<a href="<?= Url::toRoute(['user/myorder'])?>">
			<i class="fa fa-cart-plus"></i>
			<span>My Orders(3) </span>			
			</a>
			</li> 
			
			      <li class="treeview ">
              <a href="#">
                <i class="fa fa-user"></i>
              
                <span>My Account </span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?= Url::toRoute(['category/index'])?>"><i class="fa fa-empire"></i> My Categories</a></li>
               
                <li><a href="<?= Url::toRoute(['user/mypurchase'])?>"><i class="fa fa-history"></i> My Purchase History</a></li>
                <li><a href="<?= Url::toRoute(['user/view'])?>"><i class="fa fa-user"></i> My Profile</a></li>
                <li><a href="<?= Url::toRoute(['user/mysetting'])?>"><i class="fa fa-list"></i> My Setting</a></li>  
                <li><a href="<?= Url::toRoute(['user/myaddress'])?>"><i class="fa fa-list"></i> My Address</a></li> 
                <li><a href="<?= Url::toRoute(['site/logout'])?>"><i class="fa fa-sign-out"></i> Logout</a></li>   
                          
              </ul>
            </li>
			
			
						
	
            <!--<li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
