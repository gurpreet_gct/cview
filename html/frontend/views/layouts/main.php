<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$usertype = Yii::$app->user->identity->user_type;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    
    <?php $this->head() ?>
      
       
</head>
<body class="sidebar-mini wysihtml5-supported <!--sidebar-collapse--> skin-blue layout-boxed">
    <?php $this->beginBody() ?>
    
    <div class="wrapper">
      <!-- Header -->
      <?= $this->render("header"); ?>
      <!-- Left Menu -->
      <?= $this->render("leftSideBar"); ?>
      
       <!-- Content --> 
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?=$this->title;?>
            <!--<small>Control panel</small>-->
          </h1>
      
        
      <?= Breadcrumbs::widget([
           
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
           
        ]) ?>  
        </section>

        <!--
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>-->
      <section class="content">  
     <?= $content ?>
     </section>     
      </div><!-- /.content-wrapper -->    
      
      
      <?php /*
        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
        </div>
    
    */?>
 
    
       <!-- Right Menu -->
      <?= $this->render("footer"); ?>

    
    <?= $this->render("rightSideBar");?>
    
         <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
      
    </div><!-- ./wrapper -->
    
    
    
	
    <?php /*
	<!-- jQuery 2.1.4 -->
    <script src="http://localhost/GitProject/SweetSpotV2/backend/themes/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    
	<!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script type="text/javascript">
      $.widget.bridge('uibutton', $.ui.button);
    </script>
	
    <!-- Morris.js charts -->	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    
    */ ?>
  
    
    
    
    <?php $this->endBody() ?>
	<!-- Bootstrap WYSIHTML5 -->
	

    <script src="<?php echo Yii::$app->homeUrl; ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script> 
	  <!-- FastClick -->
	  	 
            
    <script src="<?php echo Yii::$app->homeUrl; ?>plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <?php /*
     <!-- InputMask -->
    <script src="<?php echo Yii::$app->homeUrl; ?>plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
    <script src="<?php echo Yii::$app->homeUrl; ?>plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
    <script src="<?php echo Yii::$app->homeUrl; ?>plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
    
    <!-- AdminLTE App -->
	
    <!--<script src="dist/js/app.min.js" type="text/javascript"></script>-->
    
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php echo Yii::$app->homeUrl; ?>dist/js/pages/dashboard.js" type="text/javascript"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo Yii::$app->homeUrl; ?>dist/js/demo.js" type="text/javascript"></script>
   
   
    <script src="<?php echo Yii::$app->homeUrl; ?>/dist/js/jquery.tree.js" type="text/javascript"></script>
<link href="<?php echo Yii::$app->homeUrl; ?>/css/jquery.tree.css" rel="stylesheet" type="text/css">
    <script src="<?php echo Yii::$app->homeUrl; ?>/dist/js/custom.js" type="text/javascript"></script>
    <?php */?>
 <script src="<?php echo Yii::$app->homeUrl; ?>/dist/js/custom.js" type="text/javascript"></script>
</body>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&callback=initAutocomplete" async defer></script>-->
   
    
</html>
<?php $this->endPage() ?>
