    <?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    ?>
    <div class="login-box">
          <div class="login-logo">
	

            <a href="<?= Yii::$app->homeUrl;?>"> <?= Html::img('@web/images/logo.png',array('alt'=>Yii::$app->name,'title'=>Yii::$app->name,'width'=>'250'));?></b></a>
          </div><!-- /.login-logo -->
          <div class="login-box-body">
            <p class="login-box-msg"><?= Yii::t('app','Sign in') ?></p>
            		  <!-- flash message start  -->	
		<?php if(Yii::$app->getSession()->hasFlash('success')):?>
			<div class="alert alert-success">
				<div> <?php echo Yii::$app->getSession()->getFlash('success'); ?></div>
			</div>
   
		<?php endif; ?>

	<?php if(Yii::$app->getSession()->hasFlash('error')):?>
		<div class="alert alert-error">
		<div> <?php echo Yii::$app->getSession()->getFlash('error'); ?></div>
		</div>
   
<?php endif; ?>

<!-- flash message end  -->	
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
               <?php echo $form->errorSummary($model); ?>
              <div class="form-group has-feedback">
                <?= $form->field($model, 'username')->textInput(['class'=>'form-control', 'placeholder' => Yii::t('app','Username or email')]);?>
              </div>
              <div class="form-group has-feedback">
                <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control', 'placeholder' => Yii::t('app','4 Digit PIN'), 'maxlength' =>4, 'size' =>4 ]) ?>
                <?= $form->field($model, 'resolution')->hiddenInput(['class'=>'form-control'])->label(false); ?>
				
              </div>
              <div class="row">
                <div class="col-xs-8">    
                  <div class="checkbox icheck">
                    <label>
                      <?= $form->field($model, 'rememberMe')->checkbox() ?>
                    </label>
                  </div>                        
                </div><!-- /.col -->
                <div class="col-xs-4">
                  <?= Html::submitButton(Yii::t('app','Login'), ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                </div><!-- /.col -->
              </div>
			 
            </form>	
      <div class="social-auth-links text-center">
              <p>- OR -</p>
              <a href="<?= Url::toRoute(['site/facebooklogin'])?>" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Login Via Facebook </a>
            </div><!-- /.social-auth-links -->
   
            <?= Html::a(Yii::t('app','Forgot PIN?'), ["/user/forgot"]) ?>
            
            <?= Html::a(Yii::t('app','Register'), ["/user/register"]) ?> 

          </div><!-- /.login-box-body -->
          <?php ActiveForm::end(); ?>
                <?php if (Yii::$app->get("authClientCollection", true)): ?>
                            <div class="col-lg-offset-2">
                                <?php yii\authclient\widgets\AuthChoice::widget([
                                    'baseAuthUrl' => ['/user/auth/login']
                                ]) ?>
                            </div>
                        <?php endif; ?>
        </div><!-- /.login-box --> 
	
<script language='JavaScript'>
<!--
$(document).ready(function(){
	var width = screen.width 
	var height = screen.height 
	$('#login-resolution').val(width);
	
});
//-->
</script>
