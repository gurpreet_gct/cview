<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = Yii::t('app','About');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseten" aria-expanded="false" aria-controls="collapseThree">
         <?= Yii::t('app','About SweetSpot') ?>
        </a>
      </h4>
    </div>
    <div id="collapseten" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       <b><?= Yii::t('app','About US:') ?> </b>
       <p>
		<?= Yii::t('app','SweetSpot is a unique new eWallet app that sweetens the deal. Using the world’s largest beacon network, SweetSpot sends you special offers (My Sweet Deals™) and information based on your location and interests. And you can pay any way you like using the secure My Sweet Wallet™. Everything you need in one place!') ?>       
       </p>
       <p>
		<?= Yii::t('app','SweetSpot brings you exciting deals and offers from your favourite brands and stores. Never miss a great deal again. Using the world’s largest beacon network, SweetSpot sends you special offers and information based on your location and interests. And we only send you the information you want to receive. No spam! You can also browse through all the deals available according to store or category.') ?>
		</P>
		<P>
		<?= Yii::t('app','You can securely store all your credit cards, loyalty cards and IDs - never lose a card again! And you can pay for deals using the secure SweetSpot wallet.') ?>
       </p>
       
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseeleven" aria-expanded="false" aria-controls="collapseThree">
        <?= Yii::t('app','Terms & Conditions') ?>
        </a>
      </h4>
    </div>
    <div id="collapseeleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       <b><?= Yii::t('app','Terms & Conditions :') ?></b>
       <p>
       <?= Yii::t('app','SWEETSPOT Terms and Conditions of Use.') ?>
       </p>
       <p>
        <?= Yii::t('app','Sweet Spot Digital (Malaysia) Sdn Bhd (Company No. 1114370-X) (“SweetSpot”, “we”, “our” or “us”) owns the website www.mysweetspot.com.my (“Website”) and “SweetSpot” mobile applications (“App”). We provide website features and products and services when you visit or access our Website and App (collectively “Services”).') ?> 
       </p>
       <p>
		<?= Yii::t('app','By using the Services, you are deemed to have agreed to our terms and conditionsset out here and our privacy policy here (provide link to privacy policy).Where applicable, you will be subject to additional terms of use when you use a specific Service provided by us. The terms and services herein, privacy policy and additional terms (if applicable) are collectively referred to as “Terms and Conditions”. ') ?>  
       </p>
       <p>
       <?= Yii::t('app','Your use of our Services will be subject to the Terms and Conditions. Please read them carefully. If you do not agree to theTerms and Conditions, you may not use our Services.') ?>
       </p>
       <p>
       <?= Yii::t('app','We may vary, amend, modify or revise the Terms and Conditions from time to time. If we revise the Terms and Conditions, such changes will be effective when they are posted on our Website or App. Your continued use of our Services following such revision constitutes your acceptance to the revised Terms and Conditions.') ?>
       </p>
       <b><?= Yii::t('app','1.Accounts') ?></b>
       <ul>
       <li><?= Yii::t('app','You must provide true, accurate and current information when you sign up for an account with us or to use the Services.') ?></li>
       <li><?= Yii::t('app','You are responsible to maintain and promptly update your information to keep it true, accurate and current.') ?></li>
       <li><?= Yii::t('app','You are responsible to maintain confidentiality of your account and password, and for restricting access to your computer or devices. You agree to accept responsibility for all activities that occur under your account or password.') ?></li>
       <li><?= Yii::t('app','We may suspend or terminate your account at our discretion if we believe that you have violated the Terms and Conditions.') ?></li>
       <li><?= Yii::t('app','The Services are available for users who are aged 13 18 and above. If you are under the age of 1318, you must not register with us or use the Services.') ?></li>
              
       </ul>
       <b><?= Yii::t('app','2.Services') ?></b>
       <ul>
       <li><?= Yii::t('app','We may at any time limit access to, or modify or cease, the whole or part of the Services.') ?></li>
       <li><?= Yii::t('app','You are responsible for all equipment, devices and data connection at your own costs to use our Services. You may incur data charges when you use the App from your devices.') ?></li>          
       </ul>
       
       <b><?= Yii::t('app','3.Merchant Offers') ?> </b>
       <ul>
       <li><?= Yii::t('app','We will send you products or services offers or other information (“Merchant Offers”) from third party merchants (“Merchants”).') ?></li>
       <li><?= Yii::t('app','The Merchant Offers are selected based on your location, age, gender, interests and other information provided to us (including via hyperlink).') ?></li>
       <li><?= Yii::t('app','Information about Merchant Offers is not a recommendation or statement of opinion by us in relation to a particular product, service or Merchants and is not intended to be investment, financial product, legal, taxation or other advice by us.') ?></li>
       <li><?= Yii::t('app','Merchant Offers are not provided or endorsed by us. Your purchase or use of Merchant Offers will be on terms agreed between you and the Merchants directly. You must read these terms of use, make your own enquiries with the relevant Merchant directly before entering into a transaction in relation to a Merchant Offer.') ?></li>
       <li><?= Yii::t('app','We are not liable or responsible to you for any of the goods or services which are the subject of any Merchant Offer. If you have any complaints, dispute or claims in any way regarding a Merchant Offer, you must direct them to the Merchant concerned.') ?></li>
       </ul>
       <b><?= Yii::t('app','4.Location Information Required') ?></b>
       <ul>
       <li>
       <?= Yii::t('app','In order to provide you with Merchant Offers that are relevant to your location, the App requires that you provide it with information about the location of your device. At present, it does so using the GPS location functionality in your device and in accordance with your proximity to BLE (Bluetooth low energy) beacon devices we place and operate.') ?>
       </li>
       <li>
       <?= Yii::t('app','If you do not agree to the App accessing that location information, or turn off the GPS and/or Bluetooth functionality in your device, we may not be able to provide you with Merchant Offers which are applicable to your location, or may present you with Merchant Offers which are not available at your location.') ?>
       </li>
       </ul>
       <b><?= Yii::t('app','5.Privacy Policy') ?></b>
       <ul>
       <li>
		<?= Yii::t('app',' We collect, store, use and disclose information about you, your interests, location of your device and other personal information you submit to us in accordance to our Privacy Policy here (provide link to privacy policy).') ?>
       </li>
       </ul>
      <b><?= Yii::t('app','6.Intellectual Property Rights') ?></b> 
      <ul>
       <li><?= Yii::t('app','All Intellectual Property Rights in the Contentin or made available through the Website, App and Servicesare owned by us or the respective owners, or licensed to us and are protected by copyright and other laws.') ?></li>
       <li><?= Yii::t('app','Subject to your compliance with these Terms and Conditions, we grant you a limited, non-exclusive, non-transferable and non-sublicensable license to use and access the Services and Content for your personal and non-commercial purposes only, and only in connection with the Services. The licence granted herein terminates if you do not comply with the Terms and Conditions.') ?></li>
       <li><?= Yii::t('app','No part of the Services or Content may be reproduced, duplicated, copied, sold, distributed, downloaded, displayed, modified, transmittedor otherwise exploited for any commercial purpose without our express written consent.') ?></li>
       <li><?= Yii::t('app','“Content” as used in these Terms and Conditions means all content (including Merchant Offers), information, data, materials, design, text, graphics, logos, photos, videos, music, sound, images, digital downloads and data compilations, in and made available through the Website, App and Services.') ?></li>
       <li><?= Yii::t('app','“Intellectual Property Rights” as used in these Terms and Conditions means all patents, trademarks, service marks, logos, trade names, internet domain names, rights in designs (including “look and feel”), copyright (including rights in computer software) and moral rights, database rights, rights in know-how and other intellectual property rights, in each case whether registered or unregistered and including applications for registration, and all rights or forms of protection having equivalent or similar effect anywhere in the world.') ?></li>      
      </ul>
      <b><?= Yii::t('app','7.Your use of our Services') ?></b>
      <ul>
      <li><?= Yii::t('app','You may post reviews, comments, ideas, suggestions and other content (“User Materials”) provided that (i) they are not illegal, obscene, threatening, defamatory, objectionable, infringing of Intellectual Property Rights of others, in breach of confidentiality, injurious to their parties or in breach of any laws;(ii) do not contain software viruses or other malicious codes; or (ii) they are not a form of political campaigning, commercial solicitation or spam.') ?></li>
      <li><?= Yii::t('app','We have no obligations to monitor any User Materials submitted. We reserve the rights (but not the obligations) to edit or remove any User Materials. We take no responsibility and assume no liability for any User Materials posted by you.') ?></li>
      <li><?= Yii::t('app','You may not use false email address or impersonate any person.') ?></li>
      <li><?= Yii::t('app','You may not collect or store personal data of other users.') ?></li>
      <li><?= Yii::t('app','You may not use the Services for fraudulent purposes engage or otherwise engage in unlawful activities conduct which will restrict or inhibit other users from properly using the Service') ?></li>
      <li><?= Yii::t('app','When you post User Materials or other information to us, you grant us a non-exclusive, royalty free, perpetual, irrevocable and full sublicensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute and display the same anywhere in any media, and to use your name in connection with the same. You warrant that all User Materials or information submitted are accurate and do not violate any laws and you will indemnify us from all claims resulting from the User Materials or information you submitted.') ?></li>
      </ul>
      <b><?= Yii::t('app','8.Links to Other Websites') ?></b>
      <ul>
      <li><?= Yii::t('app','Links to third party websites are provided for your convenience and reference. We advise you to review the terms of use and privacy policy of these linked websites as the sites might have their own terms and policy in place. We are not responsible for the content on these linked websites.') ?>  </li>
      </ul>
      <b><?= Yii::t('app','9.Disclaimers') ?></b>
      <ul>
      <li><?= Yii::t('app','All Services and Contentare provided on an “as is” basis and “as available” basis. Unless otherwise stated herein and to the fullest extent permitted by law, in respect of the Services and Content: (i) we make no representation or warranties of any kind, express or implied, including but not limited to, warranties of merchantability and fitness for a particular purpose; (ii) we do not warrant that they will be uninterrupted, timely, secure, error free or free from virus or harmful components. You agree that the use of the Services is at your sole risk.') ?></li>
      <li><?= Yii::t('app','To the fullest extent permitted by laws, we exclude all liabilities and damages of any kindarising from the use of the Services or from any Content made available to you through our Services, including but not limited to, indirect, incidental, special, punitive or consequential damages.') ?></li>
      </ul>
      <b><?= Yii::t('app','10.Governing Laws') ?></b>
      <ul>
      <li>
      <?= Yii::t('app','The Terms and Conditions are governed by laws of Malaysia and parties agree to submit to the exclusive jurisdiction of the courts in Malaysia.') ?>
      </li>
      </ul>
      </div>
    </div>
  </div>
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsetewlve" aria-expanded="false" aria-controls="collapseThree">
         <?= Yii::t('app','Privacy Policy:') ?>
        </a>
      </h4>
    </div>
    <div id="collapsetewlve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       <b><?= Yii::t('app','Privacy Policy:') ?></b>   
       <p>
		<?= Yii::t('app','Your privacy is important to us. This Privacy Policy explains our policy on collecting, using and disclosing your Personal Information. The processing of your Personal Information is also subject to applicable laws. The Personal Information which you provide to us now or from time to time or have provided to us previously will be used and processed, and continued to be used and processed by us, in accordance with this Privacy Policy (as amended, varied or revised from time to time). We may amend, vary or revise this Privacy Policy from time to time. If we revise the Privacy Policy, we will post the changes on our website.') ?>
       </p>  
       <p><?= Yii::t('app','In this Privacy Policy,') ?></p>
       <p>
		<?= Yii::t('app','Berjaya Group" means Berjaya Corporation Berhad (Company No. 554790-X) and it\'s subsidiaries, related and associate companies.') ?>
       </p> 
       <p>
		<?= Yii::t('app','Personal Information" means any information in our possession or control that relates directly or indirectly to you (or any other individual) to the extent that you (or such other individual) are identified or identifiable from that information or from other information in our possession.') ?>
       </p>
       <p>
		<?= Yii::t('app','Products and Services" means any products or services offered by us.') ?>
       </p>
       <p><?= Yii::t('app','"We", "us" or "our" means Sweet Spot Digital (Malaysia) Sdn Bhd (Company No. 1114370-X).') ?></p>
       <p><?= Yii::t('app','1.Personal Information') ?></p>
       <p><?= Yii::t('app','1.1	What we collect from you') ?></p>
       <p><?= Yii::t('app','Personal Information we collect about you, include but is not limited, to the following: -') ?></p>
       <ul>
       <li><?= Yii::t('app','personal details such as name, identity card number, age, gender, nationality, date of birth, address, telephone number, email address and other details') ?> </li>
       <li><?= Yii::t('app','location of your device and information about your device')?></li>
       <li><?= Yii::t('app','information in connection with Products and Services you used, or purchased from us, or have registered with us for information, updates, inquiries, etc') ?></li>
       <li><?= Yii::t('app','information in connection with products and services you have purchased from merchants') ?></li>
       <li><?= Yii::t('app','personal interests or preferences such as language, product or content interests, communicating and marketing preferences') ?></li>
       <li><?= Yii::t('app','other information that are, have been or may be collected by us or merchants, or which you provide to us or merchants, from time to time, in connection with any service, transaction, contest, survey, promotion, questionnaire, forms or communication with us') ?></li>
       <li><?= Yii::t('app','your photo and other image, by way of video surveillance installed in some parts of our premises as part of our security infrastructure') ?></li>
       <li><?= Yii::t('app','username, password and other identifiers when you register or log in at our website or mobile applications') ?></li>
       <li><?= Yii::t('app','username, password and other identifiers when you register or log in at our website or mobile applications') ?></li>
      <li><?= Yii::t('app','when you visit our website or mobile applications, there is automatic collection of some information about your computer or devices such as IP address, browser software, location of your device and other information about your visit.') ?></li>
      <li><?= Yii::t('app','Certain information requested by us are mandatory which you must provide and agree to the processing of such mandatory information by us, failing which, we will not be able to provide you the relevant Products and Services.') ?></li>
       </ul>
       <p><?= Yii::t('app','1.2 How your Personal Information is collected') ?></p>
       <ul>
       <li><?= Yii::t('app','when you sign up to use or purchase the Products and Services or register your interest with us to obtain information, services and updates on the Products and Services</li>
       <li>when you visit our website or social networking sites</li>
       <li>when you use our mobile applications</li>
       <li>when you enter contests or respond to surveys, promotions or questionnaires</li>
       <li>pursuant to  any transaction or inquiry or communication made with or to us') ?></li>
       </ul>
       <p><?= Yii::t('app','1.3 Purpose of collecting and processing your Personal Information</p>
       <ul>
       <li>to communicate with you</li>
       <li>to process your application, inquiry or request</li>
       <li>to provide the Products and Services to you</li>
       <li>for administrative purposes</li>
       <li>to respond to your queries</li>
       <li>to conduct surveys, research and statistical analysis</li>
       <li>to help us monitor and improve our services</li>
       <li>to comply with applicable laws, proceedings or inquiries from regulatory authorities, and enforcement agencies</li>
       <li>any other purposes permitted by applicable laws</li>
       <li>to market and promote other products and services that are are offered by us, our merchants, sponsors or partners from time to time</li>
       <li>to market and promote other products and services that are are offered by us, our merchants, sponsors or partners from time to time</li>
       <li>for direct marketing purposes</li>
       <li>to provide you with information and updates on the Products and Services</li>
       <li>to provide you with merchant offers relevant to your location') ?></li>
       </ul>
       <p><?= Yii::t('app','1.4 Sensitive Personal Data</p>
       <ul>
		   <li>We do not collect Sensitive Personal Data from you unless required by applicable laws. If you do not want your Sensitive Personal Data collected by us, please do not submit it.</li>
		   <li>"Sensitive Personal Data" means any personal data consisting of information as to physical or mental health or condition, political opinions, religious beliefs or other beliefs of a similar nature, the commission or alleged commission of an offence or any personal data prescribed under law as sensitive personal data.</li>
		   <li>If you do choose to submit your Sensitive Personal Data to us, such data shall then be deemed to be and will be treated by us as Personal Information which will be subject to the terms and scope of this Privacy Policy including as to processing, usage and disclosure thereof.</li>
       </ul>
       <p>2. Disclosure</p>
       <p>To facilitate the purposes above and the provision of products and services to you, we may process, use and disclose your Personal Information to the following classes of third parties: -</p>
       <ul>
			<li>companies within Berjaya Group</li>
			<li>our merchants, partners and sponsors </li>
			<li>payment channels, including without limitation, financial institutions for purposes of facilitating payments due to us</li>
			<li>regulatory authorities and enforcement agencies</li>
			<li>any court of law or any relevant party in connection with any claim or legal proceedings</li>
			<li>our contractors, service providers, consultants, auditors and advisors on a need-to-know basis</li>
			<li>any actual or potential assignee, transferee or acquirer of any of Berjaya Groups businesses') ?></li>
			
       </ul>
       <p><?= Yii::t('app','3. Transfer of Personal Information outside Malaysia</p>
       <p>Your Personal Information may be transferred to a country outside Malaysia. You hereby consent to us transferring your Personal Information outside of Malaysia, including to the class of third parties set out above.</p>
       <p>4. Access or Update your Personal Information</p>
       <p>4.1 You are responsible for providing accurate and complete information about yourself and any other person whose personal information you provide us (for e.g., your spouse, children, relatives or third parties), and as such, as and when such information becomes incorrect or outdated you should correct or update such information by contacting us or submitting fresh information.</p>
       <p>4.2 You may, by writing to us at the address set out in Section 6 below: </p>
       <ul>
			<li>request for a copy of the Personal Information we hold about you</li>
			<li>request to rectify  or update your Personal Information</li>
			
       </ul>
       <p>Please provide proof of your identity, address and sufficient information to enable us to identify you when submitting your request to us.</p>
       <p>4.3 On receipt of your written request, we may:</p>
       <ul>
			<li>comply or refuse your request to access or rectify  your Personal Information. If we refuse, we will notify you the reasons for such refusal.</li>
			<li>charge a fee for processing your request for access or rectification</li>
			
       </ul>
       <p>5. Limit the process of your Personal Information</p>
       <p>By giving or making available your Personal Information or using our website or mobile applications, you expressly agree and consent to the use and process of your Personal Information in accordance to this Privacy Policy, including but not limited to, consenting to receiving communications, offers, promotions and marketing materials from us or our merchants from time to time. If you:</p>
       <ul>
			<li>wish to opt out from receiving offers, promotions and marketing materials and information on other products and services from us or our partners</li>
			<li>would like us to cease processing your Personal Information for a specified purpose or in a specified manner</li>
       </ul>
       <p>please notify us in writing to the address set out in Section 6 below. In the absence of such written notification from you, we will deem it that you consent to the processing, usage and dissemination of your Personal Information in the manner, to the extent and within the scope of this Privacy Policy. If you do not consent to the processing of your Personal Information as above, we may not be able to provide you the Products and Services you requested.</p>
       <p>6. How to Contact Us</p>
       <p>Write to us or contact us at:</p>
       <p>SWEETS SPOT DIGITAL (MALAYSIA) SDN BHD</p>
	 <p>Suite 7A, B &amp; E, 7th Floor,</p>
	 <p>Office Block Plaza Berjaya,</p>
	 <p>12 Jalan Imbi, </p>
	 <p>55100 Kuala Lumpur, </p>
	 <p>Malaysia </p>
	 <p>Email: info@mysweetspot.com.my </p>

	 <p>Tel: +603 2142 7288') ?></p>
      </div>
    </div>
  </div>
  
    
</div>
