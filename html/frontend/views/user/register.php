<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use dosamigos\datepicker\DatePicker;
?>
    <div class="login-box">
          <div class="login-logo">
	

            <a href="<?= Yii::$app->homeUrl;?>"> <?= Html::img('@web/images/logo.png',array('alt'=>Yii::$app->name,'title'=>Yii::$app->name));?></b></a>
          </div><!-- /.login-logo -->
          <div class="login-box-body">
            <p class="login-box-msg"><?= Yii::t('app','Create My Account') ?></p>
            		  <!-- flash message start  -->	
		<?php if(Yii::$app->getSession()->hasFlash('success')):?>
			<div class="alert alert-success">
				<div> <?php echo Yii::$app->getSession()->getFlash('success'); ?></div>
			</div>
   
		<?php endif; ?>

	<?php if(Yii::$app->getSession()->hasFlash('error')):?>
		<div class="alert alert-error">
		<div> <?php echo Yii::$app->getSession()->getFlash('error'); ?></div>
		</div>
   
<?php endif; ?>

<!-- flash message end  -->	
            <?php $form = ActiveForm::begin(['id' => 'register-form']); ?>
           <?php echo $form->errorSummary($model); ?>
              <div class="form-group has-feedback">
                <?= $form->field($model, 'firstName')->textInput(['class'=>'form-control', 'placeholder' => Yii::t('app','firstName')]);?>
              </div>
              <div class="form-group has-feedback">
                <?= $form->field($model, 'lastName')->textInput(['class'=>'form-control', 'placeholder' => Yii::t('app','lastName')]);?>
              </div>
              <div class="form-group has-feedback">
                <?= $form->field($model, 'username')->textInput(['class'=>'form-control', 'placeholder' => Yii::t('app','username')]);?>
              </div>
              
              <div class="form-group has-feedback">
                <?= $form->field($model, 'email')->textInput(['class'=>'form-control', 'placeholder' => Yii::t('app','email')]);?>
              </div>
              <div class="form-group has-feedback">
                <?= $form->field($model, 'phone')->textInput(['class'=>'form-control', 'placeholder' => Yii::t('app','mobile')]);?>
              </div>
              <div class="form-group has-feedback">
               <?= $form->field($model, 'dob')->widget(
				DatePicker::className(), [
					
					'language' => 'en',
					'size' => 'lg',
					'clientOptions' => [
						'autoclose' => true,
						'endDate' => date('2005-12-31'),
						'format' => 'yyyy-mm-dd',
					]
			]);?>
              </div>
              <div class="form-group has-feedback">
               <?= $form->field($model, 'sex')->dropDownList(array('' =>Yii::t('app','Select'), '1' => Yii::t('app','Male'), '0' => Yii::t('app','Female') )) ?>
              </div>
             
              <div class="row">
                <div class="col-xs-8">    
                                  
                </div><!-- /.col -->
                <div class="col-xs-4">
                 <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Next') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
      
    </div>
                </div><!-- /.col -->
              </div>
            </form>

   <?php /*   <div class="social-auth-links text-center">
              <p>- OR -</p>
              <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
              <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
            </div><!-- /.social-auth-links -->
           */ ?>

           
            
            <?php // Html::a('Register', ["/user/register"]) ?> 

          </div><!-- /.login-box-body -->
          <?php ActiveForm::end(); ?>
                <?php if (Yii::$app->get("authClientCollection", false)): ?>
                            <div class="col-lg-offset-2">
                                <?= yii\authclient\widgets\AuthChoice::widget([
                                    'baseAuthUrl' => ['/user/auth/login']
                                ]) ?>
                            </div>
                        <?php endif; ?>
        </div><!-- /.login-box --> 
