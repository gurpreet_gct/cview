<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Welcome to the SweetSpot Help Centre';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseten" aria-expanded="false" aria-controls="collapseThree">
         <?= Yii::t('app','CONTACT US') ?>
        </a>
      </h4>
    </div>
    <div id="collapseten" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       <b><?= Yii::t('app','In progress') ?> </b>
       
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseeleven" aria-expanded="false" aria-controls="collapseThree">
         <?= Yii::t('app','Call Us (Mon-Fri 9am-5pm)') ?>
        </a>
      </h4>
    </div>
    <div id="collapseeleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
<a href="tel:+6494461709">61709</a>
      </div>
    </div>
  </div>
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
 <a href="mailto:someone@example.com"> <?= Yii::t('app',' Email us') ?>
        </a>
      </h4>
    </div>
    <div id="collapsetewlve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       <b><?= Yii::t('app','In progress') ?> </b>       
      </div>
    </div>
  </div>
  
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
         <?= Yii::t('app','MY DEALS') ?>
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
       <b><?= Yii::t('app','What is My Deals?') ?> </b>
       <p>
      <?= Yii::t('app','All available and valid deals that suit your profile (based on the categories you have chosen) appear in My Deals. You can filter the deals according to categories, search for deals, share deals with your friends and “GET” the deal.') ?>       
       </p>
        <b><?= Yii::t('app','What does the “GET” button do? </b>
       <p>
      When you “GET” a deal, you will be able to either BUY NOW* or “Add to Wallet” (Apple or Android). If you BUY NOW you will buy the deal through the app, using the secure payment gateway. If you “Add to Wallet” (Apple or Android) the PASS will launch and you will be able to save it in your wallet (Apple or Android) for redemption at the cashier. When you are ready to pay, open the wallet and show the cashier the PASS.
	 *Some deals only have the option of “Add to Wallet” (Apple or Android).     
       </p>
        <b>Where are my bookmarked deals saved? </b>
       <p>
      You can bookmark a deal by clicking on “GET” and then on “Bookmark this deal”. You can find the deals you have bookmarked in the BOOKMARKED DEALS section of the app as well as in My Account, Bookmarked Deals.
	</p>
	<b>Where are My Passes saved? </b>
	<p>
	To find a pass that you have added to your wallet,click on My Wallet, enter you 4-digit PIN and click on MY Passes.if you have already redeemed the pass,you will find the redeemed pass in My Redeemed Passes in My Wallet. ') ?>    
       </p>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          <?= Yii::t('app','MY STORES') ?>
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
       <b><?= Yii::t('app','What is My Stores? </b>
       <p>
		All stores that have deals that are available and valid will appear in the My Stores section. The stores can be added to Favourites, and can be filtered according to category or alphabetically. Once you click on a store, you can see the relevant deals that store has to offer, as well as store locations, store details and the store loyalty program.
		</p>
		<b>Where do I check my loyalty balance and add loyalty points in-store? </b>
		<p>
		open My Stores and enter the store of choice.Click on Loyalty and you will see your loyalty balance as well as your unique loyalty barcode and number for that store.Click below the barcode to enter loyalty points manually.The seller needs to add their unique 6-digit PIN to add your loyalty points.
		</p>
		<b>Where do I redeem deals for points? </b>
		<p>
		Once you have collected enough points you will be able to redeem deals for points.Open My Stores and enter the store of choice.Then click on LOyalty and click on the  REDEEM DEAL FOR POINTS button.') ?>   
      
       </p>
      </div>
    </div>
  </div>
  
  
  
    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFive">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
          <?= Yii::t('app','MY WALLET') ?>
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       <b><?= Yii::t('app','What is My Wallet? </b>
		<p>
		My Wallet is where all credit cards, loyalty cards and IDs are securely stored for ease of use. Never lose a card again! Add new cards and see details of cards already saved.
		</p>
		<b> How are my Digital Stamp Cards saved? </b>
		<p>
		Add a didital stamp card to the wallet by opening My Sores and entering the store of choice.Then click on Loyalty and click on "ADD STAMP CARD" button.The stamp card will be saved for ease of use in the My Wallet section of the app under My Stamp Cards.
		') ?>
       </p>
      </div>
    </div>
  </div>
  
    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingSix">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
         <?= Yii::t('app','SWEETSHOP') ?>
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       <b><?= Yii::t('app','What is the SweetShop?') ?></b>
       <p>
		<?= Yii::t('app','The SweetShop is where SweetSpot Sweets can be redeemed. Sweets are collected by doing actions within and outside of the app. Look through the SweetShop and choose to redeem your Sweets.') ?>
       </p>
      </div>
    </div>
  </div>
  
    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingSeven">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
         <?= Yii::t('app','MY ORDERS') ?>
        </a>
      </h4>
    </div>
    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       <b><?= Yii::t('app','What is My Orders? </b>
       <p>
		My Orders is where all your orders are saved in the app. Simply click on My Orders to complete your purchase.
       </p>
       <b> What if I complete my purchase and don’t get email confirmation? </b>
       <p>
		Email confirmation should be received within 12 hours of purchase. If you have not received your confirmation, please contact us.') ?>
       </p>
      </div>
    </div>
  </div>
  
    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingEight">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseThree">
         <?= Yii::t('app','MY ACCOUNT') ?>
        </a>
      </h4>
    </div>
    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       <b><?= Yii::t('app','What is My Account?</b>
       <p>
       Change your category selection, see bookmarked deals and purchase history, update your profile and settings or logout from My Account.') ?> 
       </p>
      </div>
    </div>
  </div>
  
    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingNine">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseThree">
         <?= Yii::t('app','MORE FAQs') ?>
        </a>
      </h4>
    </div>
    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       <b><?= Yii::t('app','Why should notifications be on?</b>
       <p>
       When notifications are turned on we can notify you of deals that are available in the app.
       </p>
       <b>Why should location services be on?</b>
       <p>
       We need to know where you are in order to send you deals when they are relevant to you. 
       </p>
       <b>What are beacons?</b>
       <p>
      Beacons are small sensors that are placed in stores and communicate with smartphones. They use Bluetooth technology to detect nearby smartphones and send them media such as ads, coupons or supplementary product information.
       </p>
       <b>What about Bluetooth? Won’t my battery be drained?</b>
       <p>
       No. The new Bluetooth that is on phones today will not drain your battery. Leave your Bluetooth on so that the beacon network can communicate with your phone.') ?>
       </p>
      
      </div>
    </div>
  </div>
</div>
