<?php
use yii\helpers\Html;
//use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
$this->title = Yii::t('app','My Setting');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
 $form = ActiveForm::begin([
                'options' => [                 
                    'enctype' =>'multipart/form-data'
                ]
    ]);
?>
<?php //echo'<pre>';print_r($model);die; ?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if($model==""){ ?>
	
 <?php //echo $form->field($model, 'is_on')->checkBox(); 
 //echo $form->textInput($)
 
 ?>
 <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php }else{ ?>
	<?= $form->field($model, 'is_on')->dropDownList(array('' => 'Select', 1 =>'ON', 0 =>'OFF' ))->label('Turn ON/OFF Notifications') ?>
	<?= $form->field($model, 'notification_allow')->textInput(['type'=>'number', 'min'=>'0','step'=>'1'])->label('Set Limit of Daily Deal Notification') ?>
 <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php } ?>
</div>
<?php ActiveForm::end(); ?>
