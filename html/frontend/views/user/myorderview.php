<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Cardtype */


 
$this->title = Yii::t('app','My Order Summary');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','My Order History'), 'url' => ['mypurchase']];
$this->params['breadcrumbs'][] = $this->title;
//print_r($model->id);
?>


<?php  //echo '<pre>'; print_r($model); die; ?>

         <section class="content">
			<div class="box">
            <div class="box-header with-border">
              <img src="<?php echo $model->thumbimage; ?>"/>  </br> <h3 class="box-title"> <?php // echo ucfirst($model->orgName); ?> 
               <p></p><p>
				   <?php if($model->storefavourite($model->id)==1) { ?>
				<a href="<?= Url::toRoute(['delete', 'id' => $model->id] )?>"> <i class="fa fa-star"></i> <?= Yii::t('app','Remove to favourite') ?> </a> 
				<?php }else{ ?>
					<a href="<?= Url::toRoute(['updatefav', 'id' => $model->id] )?>"> <i class="fa fa-star-o"></i><?= Yii::t('app',' Add to favourite') ?> </a> 					
				<?php }?>
				</p> </h3>
              <div class="box-tools pull-right">
                <!--<button title="" data-toggle="tooltip" data-widget="collapse" class="btn btn-box-tool" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
              <!--  <button title="" data-toggle="tooltip" data-widget="remove" class="btn btn-box-tool" data-original-title="Remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body" style="display: block;">
				<b> <?php echo ucfirst($model->brandName); ?> </b> </br>
				
            <P></P>
          
           <button type="button" class="btn btn-info" style="padding:0px 3px;" data-toggle="modal" data-target="#myModal"><?= Yii::t('app','READ MORE') ?></button>
           <div class="container">
  <h2><?=$this->title ?></h2>
  <table class="table">
    <thead>
      <tr>
        <th>Order Detail</th>
        <th>Quantity</th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><?php echo $model->productname; ?></td>
        <td><?php echo $model->qty_ordered; ?></td>
        <td><?php echo $model->total_paid; ?></td>
      </tr>
      <tr>
        <th colspan="2">Total</th>
        <td><?php echo $model->total_paid; ?></td>
      </tr>
      <tr>
        <th colspan="2">Status</th>
        <td><?php echo $model->statustype; ?></td>
      </tr>
    </tbody>
  </table>
</div>
            </div><!-- /.box-body -->
            
          </div>
 </section>


<!-- Read More Model -->
<div class="container">
  
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title"><?php  echo ucfirst($model->brandName); ?> </h3>
        </div>
        <div class="modal-body">
         <?php  echo $model->brandDescription; ?> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Close') ?></button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<!-- Read More Model End -->

</body>
</html>
