 <?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AddressType;
use common\models\Country;
use kartik\select2\Select2;
use common\models\States;
/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

 $addresstype =  AddressType::find()->all();
 $addresstype = ArrayHelper::map($addresstype, 'id', 'type');
    
 $Country =  ArrayHelper::map(Country::find()->all(), 'id_countries', 'name');
 if(isset($model->country_id)){
 $state =  ArrayHelper::map(States::find()->where(['country_id' =>$model->country_id])->all(), 'id', 'name');
 }else {
 $state = array();
 }	
    
?>


  <?php $form = ActiveForm::begin(); ?>
   <div class="box-body">
      <div class="form-group">
 
 <?php echo $form->errorSummary($model); ?>

<?= $form->field($model,'address_type')->dropDownList($addresstype, ['prompt' => 'Select Address Type']); ?>	 
  <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'addressLine1')->textInput(['maxlength' => true]) ?>
   <?= $form->field($model, 'addressLine2')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
      
    <?=  
		
		 $form->field($model, 'country_id')->widget(Select2::classname(), [
		'data' => $Country,
		'language' => 'en',
		'options' => ['placeholder' => Yii::t('app','Select a country ...')],
		'pluginOptions' => [
			'allowClear' => true
		],
	]);
	?>
	
	<?= 
		
		 $form->field($model, 'state')->widget(Select2::classname(), [
		'data' => $state,
		'language' => 'en',
		'options' => ['placeholder' => Yii::t('app','Select a state ...')],
		'pluginOptions' => [
			'allowClear' => true
		],
	]);
	?>
          <?= $form->field($model, 'pincode')->textInput(['maxlength' => true]) ?>
               <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
           
    <?= $form->field($model, 'is_shipping')->checkbox(['label'=>Yii::t('app','Use as Shipping Address')]); ?> 
               
    <?= $form->field($model, 'is_billing')->checkbox(['label'=>Yii::t('app','Use as Billing Address')]); ?> 
 </div><!-- /.box-body -->
     <div class="box-footer
     
     <div class="form-group">
       
     
         <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>
    
    <?php $siteurl = Yii::$app->getUrlManager()->getBaseUrl(); 
$countrywisestate =$siteurl.'/user/countrywisestate'; ?>   
    
<script type="text/javascript">
	
	$("#address-country_id").on("change", function () {	  
		$('#select2-address-state-container').empty();		
		var values = this.value;		
		$.ajax({
			method: "POST",
			url: "<?php echo $countrywisestate; ?>",
			data: { id: values }	
	
	})
	.done(function(result) {	
		data1 = JSON.parse(result);		
		$('#address-state').empty();	
		 var div_data1="<option value=''><?= Yii::t('app','select') ?></option>";         	
		$.each(data1, function (key, data) {   
			$.each(data, function (index, data) {					 
				 var div_data="<option value="+index+">"+data+"</option>"; 
					$(div_data1).appendTo('#address-state'); 
					$(div_data).appendTo('#address-state'); 
				
			})
		});
		
	  });
		
	});
	</script>
		  
