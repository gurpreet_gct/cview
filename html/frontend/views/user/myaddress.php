<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\Country;
use yii\helpers\ArrayHelper;
use common\models\AddressType;
use common\models\States;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

 $addresstype =  AddressType::find()->all();
 $addresstype = ArrayHelper::map($addresstype, 'id', 'type');

$Country =  ArrayHelper::map(Country::find()->all(), 'id_countries', 'name');
if(isset($_GET['AddressSearch']['country_id'])){
$state =  ArrayHelper::map(States::find()->where(['country_id' =>$_GET['AddressSearch']['country_id']])->all(), 'id', 'name');
}else{
$state = array();
}

$this->title = yii::t('app','My Address ');
#$this->params['breadcrumbs'][] = ['label' => yii::t('app','User'), 'url' => ['myaddress']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-address">
<?php



        echo GridView::widget([
        'dataProvider'=>$dataProvider,
       //'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
             'fullname',
			  'city',
            'mobile',
            [
            'attribute' =>'addresstype',
            'value' => 'typevalue',
            'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $addresstype,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => Yii::t('app','Select addresstype')],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
					],
					

				],
            ],
           
            [
            'attribute' =>'country',
            'value' => 'country.name',
                'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $Country,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => Yii::t('app','Select country')],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
						],
					

					],
				],
            
            
            [
            'attribute'=>'state',           			
			'value'=> 'states.name',
           	   'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $state,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => Yii::t('app','Select country')],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

            ],
            ],
			
            
           ['class'=>'kartik\grid\ActionColumn',
           'contentOptions' => ['style' => 'width:260px;'],
        'header'=>'Actions',
        'template' => '{view}{update}{delete}',
        'buttons' => [
			
		
            
           // view button
            'viewaddress' => function ($url, $model) {
               
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'Viewaddress'),
                            'class'=>'', 
                                                             
                ]);
            },
              // update button
            'updateaddress' => function ($url, $model) {
               
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'Updateaddress'),
                            'class'=>'', 
                                                             
                ]);
            },
              // delete button
            'delete' => function ($url, $model) {
               
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                             'class' => '',
				'data' => [
                'confirm' => yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
                                                             
                ]);
            },
             
      ],

        'urlCreator' => function ($action, $model, $key, $index) {
			
			$baseurl = Yii::$app->getUrlManager()->getBaseUrl(); 
			
				
			
			
			if ($action === 'view') {
                $url =$baseurl.'/user/viewaddress?id='.$model->id;
                return $url;
			}
		
			
			if ($action === 'update') {
                $url =$baseurl.'/user/updateaddress?id='.$model->id;
                return $url;
			}	
		 if ($action === 'delete') {                    
               $url =$baseurl.'/user/addressdelete?id='.$model->id;
                return $url;
          }
	
	}

       ],
           
           
        
            ],
            
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
        'pjaxSettings'=>[
        'neverTimeout'=>true,
          'options'=>[
                'id'=>'kv-unique-id-1',
            ]
    ],
  
      //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['createaddress'],[ 'title'=> yii::t('app','ADD ADDRESS'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['myaddress'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> yii::t('app','Reset Grid')])
              
            ],
            
            
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-user"></i>'.yii::t('app','user'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
