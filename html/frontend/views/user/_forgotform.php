    <?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    ?>
    <div class="login-box">
          <div class="login-logo">
	

            <a href="<?= Yii::$app->homeUrl;?>"> <?= Html::img('@web/images/logo.png',array('alt'=>Yii::$app->name,'title'=>Yii::$app->name));?></b></a>
          </div><!-- /.login-logo -->
          <div class="login-box-body">
            <p class="login-box-msg"><?= Yii::t('app','Sign in') ?></p>
            		  <!-- flash message start  -->	
		<?php if(Yii::$app->getSession()->hasFlash('success')):?>
			<div class="alert alert-success">
				<div> <?php echo Yii::$app->getSession()->getFlash('success'); ?></div>
			</div>
   
		<?php endif; ?>

	<?php if(Yii::$app->getSession()->hasFlash('error')):?>
		<div class="alert alert-error">
		<div> <?php echo Yii::$app->getSession()->getFlash('error'); ?></div>
		</div>
   
<?php endif; ?>

<!-- flash message end  -->	
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
              <div class="form-group has-feedback">
                <?= $form->field($model, 'email')->textInput(['class'=>'form-control', 'placeholder' => Yii::t('app','Username')]);?>
              </div>
             
              <div class="row">
                <div class="col-xs-8">    
                                  
                </div><!-- /.col -->
                <div class="col-xs-4">
                  <?= Html::submitButton(Yii::t('app','Login'), ['class' => 'btn btn-primary btn-block btn-flat', 'name' => Yii::t('app','login-button')]) ?>
                </div><!-- /.col -->
              </div>
            </form>

   <?php /*   <div class="social-auth-links text-center">
              <p>- OR -</p>
              <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
              <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
            </div><!-- /.social-auth-links -->
           */ ?>

            <?= Html::a(Yii::t('app','Forgot password'), ["/user/forgot"]) ?>
            
            <?php // Html::a('Register', ["/user/register"]) ?> 

          </div><!-- /.login-box-body -->
          <?php ActiveForm::end(); ?>
                <?php if (Yii::$app->get("authClientCollection", false)): ?>
                            <div class="col-lg-offset-2">
                                <?= yii\authclient\widgets\AuthChoice::widget([
                                    'baseAuthUrl' => ['/user/auth/login']
                                ]) ?>
                            </div>
                        <?php endif; ?>
        </div><!-- /.login-box --> 
