<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

 
 
$this->title = Yii::t('app','My Profile : ').ucfirst($model->fullName);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','My Account'), 'url' => ['site/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
          <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
             <?= Html::a(Yii::t('app','Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
     		   <?= Html::a(Yii::t('app','Change PIN'), ['changepassword', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to change password?'),
                'method' => 'post',
            ],
        ]) ?>
          <?= Html::a(Yii::t('app','Cancel'), ['deal/index'], ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'fullName',
           
            'email:email',
            
            [                     
            'label' => Yii::t('app','Date of Birth'),
            'value' => $model->dob,
             ],
             
             [                     
            'label' => Yii::t('app','Gender'),
            'value' => $model->sextype,
             ],
             
              'phone',
             
			
            [
			'attribute'=>'Image',
			'value'=> $model->imagename,
			'format' => ['image',['width'=>'70','height'=>'70']],
			 ],
            
        ],
    ]) ?>

</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>

