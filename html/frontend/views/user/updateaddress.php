<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = yii::t('app','Update Address: ') . ' ' . ucfirst($model->fullname);
$this->params['breadcrumbs'][] = ['label' => yii::t('app','Address'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => ucfirst($model->fullname), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = yii::t('app','Update');
?>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
               <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                <!-- form start -->
                
                 <?= $this->render('_formaddress', [
        'model' => $model,
    ]) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>


