<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = Yii::t('app','Update User: ' ). ' ' . ucfirst($model->fullName);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','User'), 'url' => ['site/index']];
$this->params['breadcrumbs'][] = ['label' => ucfirst($model->fullName), 'url' => ['view']];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
               <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                <!-- form start -->
                
                 <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
