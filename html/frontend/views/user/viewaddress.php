<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Cardtype */


 
$this->title = Yii::t('app','My Address');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','My Address'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if(Yii::$app->getSession()->hasFlash('success')):?>
			<div class="alert alert-success">
				<div> <?php echo Yii::$app->getSession()->getFlash('success'); ?></div>
			</div>
   
		<?php endif; ?>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
             
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
                  <p>
                  <?php if(isset($model->id)) { ?>
					  <?= Html::a(Yii::t('app','Update'), ['updateaddress', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
					  <?php } ?>
					  <?= Html::a(Yii::t('app','Delete'),['addressdelete', 'id' => $model->id],[
					        'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
                  </p>
                </div><!-- /.box-header -->
                <!-- form start -->
 
    
    
   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fullname',
            'mobile',
             [                     
            'label' => yii::t('app','City'),
            'value' => $model->city,
             ],
			 [                     
            'label' => yii::t('app','Address Type'),
            'value' => $model->addresstype->type,
             ],
              [                     
            'label' => yii::t('app','Country'),
            'value' => $model->country->name,
             ],
              [                     
            'label' => yii::t('app','State'),
            'value' => $model->states->name,
             ],
      
            
			
             
            
        ],
    ]);
  
    ?>



</div>
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>




