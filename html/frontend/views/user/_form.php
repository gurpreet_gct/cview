<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>




    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-user-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]);
    ?>
     <div class="box-body">
      <div class="form-group">
		    <?php echo $form->errorSummary($model); ?>
    <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'lastName')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'dob')->widget(
		DatePicker::className(), [			
			'language' => 'en',
			'size' => 'lg',
			'clientOptions' => [
				'autoclose' => true,
				'endDate' => date('2005-12-31'),
				'format' => 'yyyy-mm-dd',
			]
	]);?>
   
   	
	
	<div class="help-block"></div>
	<?= $form->field($model, 'sex')->dropDownList(array('' =>Yii::t('app','Select'), '1' => Yii::t('app','Male'), '0' => Yii::t('app','Female') )) ?>
   
    <?= $form->field($model, 'phone')->textInput() ?>
    
    
   
    <?= $form->field($model, 'imageFile')->fileInput() ?>
	<?php if($model->image !=''){
		
		echo '<img height="150" width="150" src="'.$model->imagename.'"/>';
		
		}  ?>
    
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['deal/index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Users')]) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>



  
