<?php
use yii\helpers\Html;
#use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;
use common\models\Usertype;
use kartik\select2\Select2;

//$id=$_GET['id'];
/* @var $this yii\web\View */
$this->title = Yii::t('app','My Order History');
$this->params['breadcrumbs'][] = $this->title;

//$result = $dataProvider->getModels();
//echo '<pre>';print_r($result); 
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

</div>
<?php 
		
$usertype = Yii::$app->user->identity->user_type;	



?>


<div class="beacon-group-index">

<?php


        echo GridView::widget([
        'dataProvider'=>$dataProvider,
       // 'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
        [
			'attribute'=>'image',
			'value'=>'thumbimage',
			'format' => ['image',['width'=>'75','height'=>'75']],
		],
          [
            'attribute'=>'Name',           			
			'value'=> 'brandName', 
			],
			[
            'attribute'=>'Price',           			
			'value'=> 'total_paid', 
			],
			[
            'attribute'=>'Created At',
          	'value' => function ($dataProvider) {
            return date("d M Y",$dataProvider["created_at"]);   
       		 },
            ],
			
          [  
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => ['style' => 'width:260px;'],
        'header'=>'Actions',
        'template' => '{view}',
        'buttons' => [
			
		
            
            //view button
            'view' => function ($url, $model) {
				
                return Html::a('<span class="glyphicon glyphicon-eyes-open">'.Yii::t('app','View-Detail').'</span>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'', 
                                                             
                ]);
            },
             
        ],

        'urlCreator' => function ($action, $model, $key, $index) {
			
			$baseurl = Yii::$app->getUrlManager()->getBaseUrl(); 
			
				
			
			
			if ($action === 'view') {
                $url =$baseurl.'/user/myorderview?id='.$model->id;
                return $url;
			}
		
			
				
	
	}

       ],
           
           
        
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [
               
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
           

            '{export}',
            '{toggleData}',
        ],
        // set export properties
         'export'=> false,
       
        // parameters from the demo form
        'bordered'=>true,
		'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-user"></i>'.Yii::t('app','My Order History'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['pdf'=>'pdf','csv'=>'csv' ],
    ]);


?>
<?php $baseurl = Yii::$app->getUrlManager()->getBaseUrl(); 
      $staturl =$baseurl.'/workorders/setstatus';
  
?>
<script type="text/javascript">
	
function setstatus(id){
	
	var values = $('#updatestatus'+id).val();
 
	var updateconfirm = confirm ("Are you sure to update status?");
		
	if (updateconfirm) {		
	$.ajax({
	method: "POST",
    url: "<?php echo $staturl; ?>",
	data: { id: id, value: values }
	
	
	})
	.done(function( result ) {
		
	  //$( "#"+id ).fadeOut( "slow");
	  });

}
 
	
}

</script>
 
</div>

