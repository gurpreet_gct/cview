<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>




    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-user-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]);
    ?>
     <div class="box-body">
      <div class="form-group">
		    <?php //echo $form->errorSummary($model); ?>
    <?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => 4])->label(Yii::t('app','New 4 Digit PIN')) ?>
    <?= $form->field($model, 'newPasswordconfirm')->passwordInput(['maxlength' => 4]) ?>
    
    
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Change PIN'), ['class' => 'btn btn-success' ]) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['deal/index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Users']) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>



  
