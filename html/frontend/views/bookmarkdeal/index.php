<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Bookmarked Deals';

$models = $dataProvider->getModels();

?>

	

 <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
			  
		<?php  foreach ( $models as $model) {  ?>	  
            <div class="col-lg-3 col-xs-6">
				
              <!-- small box -->
              <div class="small-box" style="margin-bottom: 0px;">
				   <?php if ($model->bookmarkstatus($model->id) == 1) { ?>
									  <h4 class="small-box bg-aqua">
									  <a href="<?= Url::toRoute(['unbookmark', 'id' => $model->id] )?>" style="color:#FFFFFF;" class="small-box-footer"> <i class="fa fa-bookmark"></i> Unbookmark this deal </a>  </h4>
									  <?php }else { ?>
									  <h4 class="small-box bg-aqua" >
									  <a href="<?= Url::toRoute(['bookmark', 'id' => $model->id] )?>" style="color:#FFFFFF;" class="small-box-footer"> <i class="fa fa-bookmark-o"></i> Bookmark this deal </a>  </h4> 
									  <?php } ?>
				  
				  <?php echo '<img height="100%" width="100%" src="'.$model->frontPhotoUpload.'">'; ?>
				  </div>
          
           <div class="small-box bg-aqua">
                <div class="inner">
					<?php  if($model->offernow!=0) { ?>
                  <h3><?php echo '$'.$model->offernow; ?> <sup style="font-size: 20px; text-decoration: line-through;"><?php echo '$'.$model->offerwas; ?></sup></h3>
                 <?php } else { ?>
					   <h3><?php $pieces = explode("OFF", $model->text); echo $pieces[0].' OFF'; ?> </h3>
					 <?php } ?>
                  <p><?php echo $model->text;  ?></p>
                </div>
               
              
                <a href="javascript:void(0)" style="width:50%; float:left; background:#00A3CB;" data-target="#myModal-<?php echo $model->id; ?>" data-toggle="modal" class="small-box-footer">Share <i class="fa fa-arrow-circle-right"></i></a>
                
                <a href="javascript:void(0)" style="width:50%; float:left; background:#00A3CB;" data-target="#mygetModal-<?php echo $model->id; ?>" data-toggle="modal" class="small-box-footer">Get <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            
    <!--  Share Modal -->
	<div class="modal fade" id="myModal-<?php echo $model->id; ?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> <b> Share this <?php echo $model->text;  ?> </b> </h4>
        </div>
        <div class="modal-body"  style="text-align:center">
       <!-- Twitter -->
		<a href="https://twitter.com/intent/tweet?url=<?= Url::toRoute(['view' , 'id'=>$model->id])?>&text= <?php echo $model->text; ?>"  title="Share on Twitter" target="_blank" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>
		 <!-- Facebook -->
		<a href="https://www.facebook.com/sharer/sharer.php?u=<?= Url::toRoute(['view' , 'id'=>$model->id])?>" title="Share on Facebook" target="_blank" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
		<!-- Google+ -->
		<a href="https://plus.google.com/share?url=<?= Url::toRoute(['view' , 'id'=>$model->id])?>" title="Share on Google+" target="_blank" class="btn btn-googleplus"><i class="fa fa-google-plus"></i> Google+</a>

		<!-- LinkedIn --> 
		<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= Url::toRoute(['view' , 'id'=>$model->id])?>&title=<?php echo $model->text;  ?>&summary=<?php echo $model->text;  ?>" title="Share on LinkedIn" target="_blank" class="btn btn-linkedin"><i class="fa fa-linkedin"></i> LinkedIn</a>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
		</div>
		</div>
		
		<!--  view Modal -->
	<div class="modal fade" id="mygetModal-<?php echo $model->id; ?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> <b> View <?php echo $model->text;  ?> </b> </h4>
        </div>
        <div class="modal-body">
			
			
				
				<div class="small-box bg-aqua" style="margin-bottom: 0px;">
                <div style="padding:1px;" class="inner">
					                  <h2> <?php echo $model->dealTitle; ?> </h2>
                                 
									  <?php if ($model->bookmarkstatus($model->id) == 1) { ?>
									   <h4>
									  <a href="<?= Url::toRoute(['unbookmark', 'id' => $model->id] )?>" data-bb="confirm" style="color:#FFFFFF;" class="small-box-footer 1"> <i class="fa fa-bookmark"></i> Unbookmark this deal </a>  </h4>
									  <?php }else { ?>
										 <h4>
									  <a href="<?= Url::toRoute(['bookmark', 'id' => $model->id] )?>" style="color:#FFFFFF;" class="small-box-footer"> <i class="fa fa-bookmark-o"></i> Bookmark this deal </a>  </h4> 
									  <?php } ?>
                </div>
               
              
              </div>
              
              <!-- small box -->
              <div class="small-box" style="margin-bottom: 0px;">
				  
				  <?php echo '<img height="100%" width="100%" src="'.$model->frontPhotoUpload.'">'; ?>
				  </div>
          
           <div class="small-box bg-aqua">
                <div class="inner">
					<?php  if($model->offernow!=0) { ?>
                  <h3><?php echo '$'.$model->offernow; ?> <sup style="font-size: 20px; text-decoration: line-through;"><?php echo '$'.$model->offerwas; ?></sup></h3>
                 <?php } else { ?>
					   <h3><?php $pieces = explode("OFF", $model->text); echo $pieces[0].' OFF'; ?> </h3>
					 <?php } ?>
					   <a href="javascript:void(0)" style="color:#FFFFFF;" class="small-box-footer"> <i class="fa fa-info-circle"></i> Read Terms </a>
               
                </div>
              
                
                <a href="<?=  $model['workorderpopup']->passUrl; ?>" target="_blank" style="width:50%; float:left; background:#00A3CB;" class="small-box-footer">Add to Passbook <i class="fa fa-arrow-circle-right"></i></a>
                <a href=javascript:void(0)" style="width:50%; float:left; background:#00A3CB;" class="small-box-footer">BUY NOW  <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            
      
      
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
		</div>
		</div>
		
            
            <?php } ?>
            
          
            
            
          </div><!-- /.row -->
 
 



