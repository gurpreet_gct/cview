<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CardtypeSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','My Wallet');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cardtype-index">
	
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
 <input type="hidden" id="baseurl" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/ticket/getticketview';?>">
 <input type="hidden" id="baseurlread" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/ticket/getreadmore';?>">
<?php
//profile.brandLog
        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns' => [
				[
              'header'=>Yii::t('app','Logo'),
              'value' => function ($data) {
                       return  Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$data->profile->brandLogo)
                        ;
                    },
                  'format' => ['image',['width'=>'70','height'=>'70']] , 
               /*'onclick'=>'actionTypes($(this).attr("workorder_id"))'*/
              ],
		
              [
              'header'=>Yii::t('app','offerTitle'),
               
               'value'=>function ($model) {
                return "<a href='javascript:void(0);' onclick='getData($model->workorder_id)' data-target='mygetModal' data-toggle='modal' class='small-box-footer'> $model->offerTitle</a>";
               },
               'vAlign'=>'middle',
               'format'=>'raw',
               'width'=>'150px',
               'noWrap'=>true
              ],
			
			
			 [
              'header'=>Yii::t('app','Exp.date'),
               
               'value'=>function ($model) {
                return "<a href='javascript:void(0);' onclick='getData($model->workorder_id)' data-target='mygetModal' data-toggle='modal' class='small-box-footer'>".date('d/m',  strtotime($model->workorders->dateTo))."</a>";
               },
               'vAlign'=>'middle',
               'format'=>'raw',
               'width'=>'150px',
               'noWrap'=>true
              ],
           
				/*[
				'header'=>'Exp.date',
				'value'=>function ($model) {
                        return   date("d/m",  strtotime($model->workorders->dateTo));
                   } 
              ],*/
              
              
    
			
         
            // 'updated_at',

        ],
     'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
       'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                //'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        
        // set export properties
     
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>Yii::t('app','My Tickets'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
 <!--pop up for passes ---->
 <div class="modal fade" id="mygetModal"  role="dialog">
	
  </div>
 
  <!--- end here ---->  
		<!--  Read Terms Modal mygetModalread -->
		<div class="modal fade" id="mygetModalread" role="dialog">
		
		</div>
	<!--  end here -->

</div>
<script>
function getData($id){
	var workorderid = $id;
	$('#mygetModal').css('display','block');
	$('#mygetModal').addClass( "in" );

	$( ".wrapper" ).append( "<div class='modal-backdrop fade in'></div>" );
	var baseurl = $('#baseurl').val();
	
	$.ajax({
	url:  baseurl ,
	data: { 
	workorderid:  workorderid
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		$( "div#mygetModal" ).html(data);
	},
	
	})
	}
	

$(document.body).on("click", ".close-ticket",function() {
	$('#mygetModal').css('display','none');
		$('#mygetModal').removeClass( "in" )
		$( ".modal-backdrop.in" ).remove();
	
	});
	
	$(document.body).on("click", ".read_popupopen",function() {
		
	$('#mygetModalread').css('display','block');
	$('#mygetModalread').addClass( "in" )
	var workorderid = $(this).attr('rel');
	
	var baseurl = $('#baseurlread').val();
	
	$.ajax({
	url:  baseurl ,
	data: { 
	workorderid:  workorderid
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		$( "div#mygetModalread" ).html(data);
	},
	
	})
	
	});
</script>

<style>
	
	@import url(https://fonts.googleapis.com/css?family=Montserrat:400,700);

	.prod_popup_had h1{ font-family: 'Montserrat', sans-serif; font-weight:400; color: #555 !important; }
.prod_popup_had h1{ color: #000;
    float: left;
    font-size: 16px;
    margin: 0;
    padding: 4px 0;
    width: 52%;}


	

</style>
