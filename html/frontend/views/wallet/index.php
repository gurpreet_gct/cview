<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
     use yii\helpers\Url;
$this->title = Yii::t('app','My Wallets');
$this->params['breadcrumbs'][] = $this->title;


?> 





<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
           
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title"><b><?= Yii::t('app','Please enter your 4 digit PIN or use touch ID') ?> </b> </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
               
                 
                  <?php $form = ActiveForm::begin(['class' => 'form-horizontal']); ?>
               <?php echo $form->errorSummary($model); ?>
                <div class="box-body">
                    <div class="form-group">
                     
                     
                        <?= $form->field($model, 'walletpin')->passwordInput(['class'=>'form-control', 'placeholder' => Yii::t('app','4 Digit PIN'), 'maxlength' =>4, 'size' =>4 ]) ?>
             
                   
                    </div>
                    <div class="form-group">
                      
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
				
				<?= Html::submitButton(Yii::t('app','LOGIN TO MY WALLET'), ['class' => 'btn btn-info pull-right', 'name' => 'LOGIN TO MY WALLET']) ?>
                   
                   
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
