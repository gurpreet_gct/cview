<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
    use common\models\Id;
$this->title = Yii::t('app','My Wallets');
$this->params['breadcrumbs'][] = $this->title;
$request_data = Yii::$app->request->get();
if(isset($request_data['id']) && $request_data['id'] == 'redeem'){
	$url = $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/redeemed/index?wid='.$request_data['wid'];
	header("Location: ".$url);
	exit();
	 }
	 if(isset($request_data['id']) && $request_data['id'] == 'pass'){
	$url = $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/pass/index?wid='.$request_data['wid'];
	header("Location: ".$url);
	exit();
	 }


?> 
<section class="content">
	<div class="box box-info">
			<div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div>
            <div class="box-footer" style="background:#F9F9F9">
                 <i class="fa fa-credit-card">  </i> <a href="#"><?= Yii::t('app','My Credit Cards') ?> </a>
            </div><!-- /.box-footer-->
            <div style="display: block;" class="box-footer">
              <i class="fa fa-lock"></i> 
				<a href="#"><?= Yii::t('app','My Loyalty') ?> </a>
               </div><!-- /.box-footer-->
            <div class="box-footer" style="background:#F9F9F9">
              <i class="fa   fa-folder-o"></i><a href="<?= Url::toRoute(['walletid/index'])?>"><?= Yii::t('app','My IDs') ?> </a>
            </div><!-- /.box-footer-->
            <div style="display: block;" class="box-footer">
               <i class="fa fa-credit-card">  </i> <a href="<?= Url::toRoute(['pass/index'])?>"><?= Yii::t('app','My Passes') ?></a>
            </div><!-- /.box-footer-->
            <div style="display: block;" class="box-footer">
               <i class="fa fa-credit-card">  </i> <a href="<?= Url::toRoute(['redeemed/index'])?>"><?= Yii::t('app','My Redeemed Passes') ?></a>
            </div><!-- /.box-footer-->
            <div style="display: block;" class="box-footer">
               <i class="fa fa-credit-card">  </i> <a href="<?= Url::toRoute(['ticket/index'])?>"> <?= Yii::t('app','My Ticket') ?></a>
            </div><!-- /.box-footer-->
            <div style="display: block;" class="box-footer">
               <i class="fa fa-credit-card">  </i> <a href="<?= Url::toRoute(['stampcard/index'])?>"><?= Yii::t('app','My Stamp Cards') ?></a>
            </div><!-- /.box-footer-->
            </div>
          </div>
 </section>
