<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CardtypeSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','My Wallet');
$this->params['breadcrumbs'][] = $this->title;
$request_data = Yii::$app->request->get();
if(isset($request_data['wid']) && $request_data['wid']!=''){?>
	<script>
		$( document ).ready(function() {
		$id = "<?php echo $request_data['wid']; ?>";
		var workorderid = $id;
		$('#mygetModal').css('display','block');
	$('#mygetModal').addClass( "in" );
	$( ".wrapper" ).append( "<div class='modal-backdrop fade in'></div>" );
	var baseurl = $('#baseurl').val();
	$.ajax({
	url:  baseurl ,
	data: { 
	workorderid:  workorderid
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		$( "div#mygetModal" ).html(data);
	},
	
	})
	
    
});
	
</script>
<?php 	 }
?>
<div class="cardtype-index">
	
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
 <input type="hidden" id="baseurl" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/pass/getpassview';?>">
 <input type="hidden" id="baseurlread" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/pass/getreadmore';?>">
 <input type="hidden" id="baseurlofflineloyalry" value="<?php echo $baseurl = Yii::$app->getUrlManager()->getBaseUrl().'/pass/getaddofflineloyalty';?>">
<?php
//profile.brandLog
        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns' => [
				[
              'header'=>Yii::t('app','Logo'),
              'value' => function ($data) {
                       return  Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$data->profile->brandLogo)
                        ;
                    },
                  'format' => ['image',['width'=>'70','height'=>'70']] , 
               /*'onclick'=>'actionTypes($(this).attr("workorder_id"))'*/
              ],
		
              [
              'header'=>Yii::t('app','offerTitle'),
               
               'value'=>function ($model) {
                return "<a href='javascript:void(0);' onclick='getData($model->workorder_id)' data-target='mygetModal' data-toggle='modal' class='small-box-footer'> $model->offerTitle</a>";
               },
               'vAlign'=>'middle',
               'format'=>'raw',
               'width'=>'150px',
               'noWrap'=>true
              ],
			
			
			 [
              'header'=>Yii::t('app','Exp.date'),
               
               'value'=>function ($model) {
                return "<a href='javascript:void(0);' onclick='getData($model->workorder_id)' data-target='mygetModal' data-toggle='modal' class='small-box-footer'>".date('d/m',  strtotime($model->workorders->dateTo))."</a>";
               },
               'vAlign'=>'middle',
               'format'=>'raw',
               'width'=>'150px',
               'noWrap'=>true
              ],
           
				/*[
				'header'=>'Exp.date',
				'value'=>function ($model) {
                        return   date("d/m",  strtotime($model->workorders->dateTo));
                   } 
              ],*/
              
              
    
			
         
            // 'updated_at',

        ],
     'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
       'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                //'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        
        // set export properties
     
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>Yii::t('app','My Passes'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
 <!--pop up for passes ---->
 <div class="modal fade" id="mygetModal"  role="dialog">
		
  </div>
 
  <!--- end here ---->  
		<!--  Read Terms Modal mygetModalread -->
		<div class="modal fade" id="mygetModalread" role="dialog">
			</div>
	<!--  end here -->

</div>

<!--- end here ---->  
		<!--  Read Terms Modal mygetModalread -->
		<div class="modal fade" id="myModalBuyInStore" role="dialog">
		<div class="modal-dialog modal-dialog-offline-loyalty">
		
		  <!-- Modal content-->
		  <div class="modal-content modal-content-offline-loyalty">
			<div class="modal-header modal-header-offline-loyalty">
			  
			  <h4 class="modal-title modal-title-offline-loyalty" style="text-align:center"> <b> BUY IN-STORE </b> <p class="closeloyalty" data-dismiss="modal">X</p></h4>
			</div>
			<div class="modal-body" >
			<p>for in-store redemption please scan barcode OR enter amount, transaction ID and seller 6-digit
			PIN</p>	     
			</div>
			
			<div class="passUrlImg"><p class="passUrlImage"><img src="http://localhost/SweetSpotV2/pass/15932f5b53e081378d238051f6bd8688" id="theImg"></p></div>
			<input type ="number" id="amount" class="amount input-cls"  min=".01" step="0.01" placeholder="$0.00"> 
			<input type ="text" id="pin" class="pin input-cls"  placeholder="Enter 6 Digit PIN"> 
			<input type ="text" id="transaction_id " class="transaction_id input-cls"  placeholder="Enter transaction id here"> 
			<input type ="hidden" id="store_id" class="store_id" > 
			   <p><button  id="addOfflineLoyalty" class="btn btn-info addOfflineLoyalty off-store-btn" type="button" >SUBMIT
          </button></p>
			<div class="modal-footer">
			  <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
			</div>
		  </div>
		  
		</div>
		
		</div>
	<!--  end here -->
<script>
function getData($id){
	var workorderid = $id;
	$('#mygetModal').css('display','block');
	$('#mygetModal').addClass( "in" );
	//$( "#mygetModal" ).toggle();
	/* $( "#mygetModal" ).animate({
    
   
    height: "toggle"
  }, 5000, function() {
    // Animation complete.
  });*/
	$( ".wrapper" ).append( "<div class='modal-backdrop fade in'></div>" );
	var baseurl = $('#baseurl').val();
	
	$.ajax({
	url:  baseurl ,
	data: { 
	workorderid:  workorderid
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		$( "div#mygetModal" ).html(data);
	},
	
	})
	}
	

$(document.body).on("click", ".close-pass",function() {
	$('#mygetModal').css('display','none');
		$('#mygetModal').removeClass( "in" );
		$( ".modal-backdrop.in" ).remove();
	
	});
	
	$(document.body).on("click", ".read_popupopen",function() {
		
	$('#mygetModalread').css('display','block');
	$('#mygetModalread').addClass( "in" );
	var workorderid = $(this).attr('rel');
	
	var baseurl = $('#baseurlread').val();
	
	$.ajax({
	url:  baseurl ,
	data: { 
	workorderid:  workorderid
	},
	type: 'POST',
	dataType: 'html',					
	success: function(data) {
		$( "div#mygetModalread" ).html(data);
	},
	
	})
	
	});
	$(document.body).on("click", ".closereadmore",function() {
	$('#mygetModalread').css('display','none');
	$('#mygetModalread').removeClass( "in" );
	
	});
	
	$(document.body).on("click", ".myModalBuyInStore",function() {
		
	$('#myModalBuyInStore').css('display','block');
	$('#myModalBuyInStore').addClass( "in" );
	var workorderid = $(this).attr('rel');
	var store_id = $(this).attr('data-id');
	$('#store_id').val(store_id);
	var passurl = $('.passUrl').val();
	//$('.passUrlImage').append('<img id="theImg" src="'+$passurl+'" />');
	$("#theImg").attr("src",passurl);
	return false;
	});
	
	
	
	$( "#addOfflineLoyalty" ).click(function() {
	var amount = $('#amount').val();
	var pin = $('#pin').val();
	var transaction_id = $('#transaction_id').val();
	var store_id = $('#store_id').val();
	var baseurl = $('#baseurlofflineloyalry').val();
	$.ajax({
	url:  baseurl ,
	data: { 
	amount:  amount,
	pin:  pin,
	transaction_id:  transaction_id,
	store_id : store_id
	},
	type: 'POST',
	dataType: 'json',					
	success: function(data) {
		if(data==2){
			alert('Your pass has been redeemed and your loyalty points added.');
			$('#myModalBuyInStore').css('display','none');
			$('#myModalBuyInStore').removeClass( "in" );
			$('#mygetModal').css('display','none');
			$('#mygetModal').removeClass( "in" );
			$( ".modal-backdrop.in" ).remove();
			}
			if(data==4){
				alert('This transaction ID has already accrued loyalty points.');
				return false;
			}
		if(data==3){
				alert('That PIN does not match your registered 6-digit PIN. Please try again.');
				return false;
			}
			if(data==5){
				alert('Amount cannot be blank.');
				return false;
			}if(data==6){
				alert('Pin cannot be blank.');
				return false;
			}if(data==7){
				alert('Transaction Id cannot be blank.');
				return false;
			}if(data==8){
				alert('This transaction ID has already accrued loyalty points.');
				return false;
			}
		
		
		return false;
	},
	
	})
	
	});
	$(document.body).on("click", ".closeloyalty",function() {
	
		$('#myModalBuyInStore').css('display','none');
			$('#myModalBuyInStore').removeClass( "in" );
	
	});
</script>

<style>
	@import url(https://fonts.googleapis.com/css?family=Montserrat:400,700);
</style>


