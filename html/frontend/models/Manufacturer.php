<?php
namespace frontend\models;
use \yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\models\Profile;
use frontend\models\UserStore;
use frontend\models\Profiles;
use common\models\Foodorders;

use Yii;

/**
 * User Store Model
 *
 * 
 */
class Manufacturer extends User 
{
	  /**
     * @inheritdoc
     */
	public $params=[]; 
	const ROLE_LOCATION_OWNER=7;
    
	public function init()
	{
		 $this->params['user_id']=0;
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
        ];     
    
    }
    
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last Name'),
			'fullName' => Yii::t('app','Full Name'),
			'orgName' => Yii::t('app','Store Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb Identifier'),
			'role_id' => Yii::t('app','Role Id'),
			'user_type' => Yii::t('app','User Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New Password'),
			'password_reset_token' => Yii::t('app','Password Reset Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date Of Birth'),
			'ageGroup_id' => Yii::t('app','Age Group Id'),
			'walletPinCode' => Yii::t('app','Wallet Pin Code'),
			'loyaltyPin' => Yii::t('app','Loyalty Pin'),
			'auth_key' => Yii::t('app','Auth Key'),
			'activationKey' => Yii::t('app','Activation Key'),
			'confirmationKey' => Yii::t('app','Confirmation Key'),
			'access_token' => Yii::t('app','Access Token'),
			'hashKey' => Yii::t('app','Hash Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device Token'),
			'lastLogin' => Yii::t('app','Last Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created At'),
            'updated_at'=>Yii::t('app','Updated At'),
            
        ];
    }
    
    
    
    
    
  public function fields()
	{
		$fields=parent::fields();
		unset($fields['password_hash']);
		unset($fields['password_reset_token']);
		unset($fields['created_at']);
		unset($fields['updated_at']);
		unset($fields['user_type']);
		unset($fields['activationKey']);
		unset($fields['access_token']);
		unset($fields['hashKey']);
		unset($fields['lastLogin']);
		unset($fields['status']);
		unset($fields['role_id']);
		unset($fields['confirmationKey']);
		unset($fields['device']);
		unset($fields['fbidentifier']);
		unset($fields['username']);
		unset($fields['image']);
		unset($fields['sex']);
		unset($fields['walletPinCode']);
		unset($fields['auth_key']);
		
		return $fields;
	}
    
    public function extraFields()
    {
		$fields=['address'];
		unset($fields['id']);
        return $fields;
    }
    
        
	

    
    public function getAddress()
    {
       return $this->hasOne(Profiles::className(), ['user_id' => 'id']);       
    }
	
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			
            if ($this->isNewRecord) {
               $this->created_at=time();
            }
			     $this->updated_at=time();							
            return true;
        }
        return false;
    }
 
    public function getUserStore()
    {
		
        return $this->hasMany(UserStore::className(), ['store_id' => 'id'])
		->andOnCondition(UserStore::tableName().'.`status` = :status', [':status'=>1]);
		//->andOnCondition('`user_id` = :user_id', [':user_id'=>$this->params['user_id']]);
    }
    
       public function getProfile()
    {
		
         return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }
    
     public function getProfiles()
    {
		
         return $this->hasOne(Profiles::className(), ['user_id' => 'id']);
    }
  
    
    
    public function storefavourite($id)
	{
   	
		$model = UserStore::find()
		->where(['store_id' => $id])
		->Andwhere(['user_id' => Yii::$app->user->id])
		->one();
	     if(isset($model->user_id)) {
           if($model->status==1){
			   
					return true;
			    }else {
					 return 0;
			  }
        }
    }
    
    
    
    
  /* To get all Store */
  
     public function getManufacturer($user_id)
     {
		 $this->params['user_id']=$user_id;
	//	 echo'<pre>';print_r($this->params['user_id']);die();
         return $dataProvider = new ActiveDataProvider([
            'query' =>self::find()
					->select(["tbl_profile.brandLogo","tbl_profile.foregroundHexColour","tbl_profile.backgroundHexColour","tbl_users.id","tbl_users.orgName",'ifnull(if(store_id!=0,1,0),0) as isSelected'])
					->joinWith([
				'userStore'=>function($query) use ($user_id) {
																$query->andOnCondition(['=', 'user_id', $user_id]);
															},
				],false,"LEFT JOIN")
				 ->innerJoinWith('profile', false)
			//->innerJoinWith('foodorders_item',false)
					//->leftJoin('userStore', 'store_id = `id` and status=1 and user_id='.$user_id)
					->where(['user_type'=> self::ROLE_LOCATION_OWNER ])
					->andWhere([self::tableName().'.status'=> 10 ])
					->orderBy(['orgName'=>SORT_ASC])
					
        ]);
//echo'<pre>';print_r($dataProvider);die();
     } 
     
     
     public function getThumbimage(){
		/* Find Brand images */ 
		if(isset($this->id)) {
			 $profile = Profile::find()->select('brandLogo')->where(['user_id'=>$this->id])->one();
			 if($profile->brandLogo !=''){
				 return Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$profile->brandLogo); 
				
			}else{
			 return 'no image found';
			}
		}
			 
	 }
     
 
           
	  
       
       
	 
	 public function getManufacturerInfo(){
		 return $model=self::find()    
		->select(["tbl_profile.*","tbl_users.*"])
		 ->where(['tbl_users.id'=>$this->id,'user_type'=>Store::ROLE_LOCATION_OWNER])
		 //->joinWith('profile')
		 ->innerJoinWith('profile', false)
		 ->one();
	 }
}
