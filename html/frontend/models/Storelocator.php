<?php

namespace frontend\models;
use Yii;
use yii\base\Model;

use frontend\models\Manufacturer;
use frontend\models\Profiles;



/**
 * This is the model class for ForgotPassword.
 *
 * @property integer $email
 * @property string $user
 
 */
class Storelocator extends Model
{
    /**
     * @inheritdoc
     */
  public $lat,$lng,$orgName,$brandLogo;
     
    
   public function rules()
    {
        return [
        [['lat','lng'],'required'],        
        [['lat','lng'],'safe'],        
        ];
    }
    
         /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last Name'),
			'fullName' => Yii::t('app','Full Name'),
			'orgName' => Yii::t('app','Org Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb Identifier'),
			'role_id' => Yii::t('app','Role Id'),
			'user_type' => Yii::t('app','User Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New Password'),
			'password_reset_token' => Yii::t('app','Password Reset Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date Of Birth'),
			'ageGroup_id' => Yii::t('app','Age Group Id'),
			'walletPinCode' => Yii::t('app','Wallet Pin Code'),
			'loyaltyPin' => Yii::t('app','Loyalty Pin'),
			'auth_key' => Yii::t('app','Auth Key'),
			'activationKey' => Yii::t('app','Activation Key'),
			'confirmationKey' => Yii::t('app','Confirmation Key'),
			'access_token' => Yii::t('app','Access Token'),
			'hashKey' => Yii::t('app','Hash Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device Token'),
			'lastLogin' => Yii::t('app','Last Login'),
			'lat' => Yii::t('app','Latitude'),
			'lng' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created At'),
            'updated_at'=>Yii::t('app','Updated At'),
        ];
    } 
    
    public function getlatitue()
    {
		if(isset($_GET['lat'])) {
		$this->lat = $_GET['lat'];
		$this->lng = $_GET['lng'];
		
		}else{
		
		$user = Manufacturer::find()->select(['latitude','longitude'])->where(['id' => Yii::$app->user->id])
				->orderBy('id')
				->one();	
				$this->lat  = $user->latitude; 
				$this->lng = $user->longitude; 
				
		}
		
		
	}
	
	public function getorgname($id){
	
		$orgName = Manufacturer::find()->select(['orgName'])->where(['id' => $id])
				->orderBy('orgName')
				->one();		
		$this->orgName = $orgName->orgName; 
		
	} 
	
	public function getbrandLogo($id){
	
		$orgName = Profiles::find()->select(['brandLogo'])->where(['user_id' => $id])->one();		
		$this->brandLogo =$orgName->brandLogo; 
		
		echo $this->brandLogo; die;
		
	} 
	
	
	
    
 
  
}
