<?php

namespace frontend\models;

use Yii;
use common\models\Profile;
#use yii\web\UploadedFile;
/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property integer $status
 * @property string $email
 * @property integer $contact_no
 * @property string $role
 * @property integer $active
 * @property string $tokenhash
 * @property integer $tokenexpired
 * @property string $created
 * @property string $modified
 */
class Profiles extends Profile
{
    /**
     * @inheritdoc
     */
    public $usertype_id=0; 
      
  /*  public static function tableName()
    {
        return 'tbl_profile';
    }
    
    
*/



	 public function attributeLabels()
    {
        return [
             'id' =>Yii::t('app','Id') ,
            'user_id' =>Yii::t('app','User Id') ,
            'companyName' =>Yii::t('app','Company Name') ,
            'companyNumber' => Yii::t('app','Company Registration Number'),
            'ssdRegion' =>Yii::t('app','SweetSpot Region'),
            'houseNumber' => Yii::t('app','Street Number'),
            'street' => Yii::t('app','Street'),
            'city' =>Yii::t('app','City') ,
            'state' => Yii::t('app','State'),
            'country' => Yii::t('app','Country'),
            'postcode' =>Yii::t('app','Postcode') ,
            'officePhone' =>Yii::t('app','Office Phone') ,
            'officeFax' => Yii::t('app','Office Fax'),
			'alternativeContactperson' => Yii::t('app','Alternative Contact Person'),           
            'commericalStatus' => Yii::t('app','Commerical Status'),
            'latitude' => Yii::t('app','Latitude'),
            'longitude' => Yii::t('app','Longitude'),
            'brandName' => Yii::t('app','Store/Brand Name'),
            'brandLogo' => Yii::t('app','Brand Logo'),
            'brandimgLogo' => Yii::t('app','Store/Brand Logo (132px x 132px)'),
            'backgroundImage' => Yii::t('app','Background Image (640px x 360px)'),
            'brandAddress' => Yii::t('app','Store/Brand Address(head office)'),
            'brandDescription' => Yii::t('app','Store/Brand Description'),
            'socialmediaFacebook' => Yii::t('app','Facebook Url'),     
            'socialmediaTwitter' => Yii::t('app','Twitter Url'),     
            'socialmediaGoogle' => Yii::t('app','Google Url'),  
            'socialmediaPinterest' =>Yii::t('app','Pinterest Url') ,  
            'socialmediaInstagram' => Yii::t('app','Instagram Url'),  
            'backgroundHexColour' => Yii::t('app','Background Hex Colour'),  
            'foregroundHexColour' =>Yii::t('app','Label Text Colour (Hex Colour)') ,  
            'addLoyalty' => Yii::t('app','Add "Loyalty"'),  
            'shopNow' => Yii::t('app','Add "Shop Now"'), 
            'socialcheck' => Yii::t('app','Add "Social media buttons and URLs"'), 
            'addcolors' => Yii::t('app','Add "Background and foreground hex colour"'), 
        ];
    }





    public function init()
	{
		$this->usertype_id=3;
		$this->commercialStatus=3;        
	}
    
    	public function fields()
	{
		$fields=parent::fields();
		unset($fields['RelatedBeaconLocationBrokerID']);
		unset($fields['RelatedBeaconLocationBroker']);
		unset($fields['NumberOfBeaconLocations']);
		unset($fields['contractDuration']);
		unset($fields['alternativeContactperson']);
		unset($fields['longitude']);
		unset($fields['latitude']);
		unset($fields['officeFax']);
		unset($fields['DID']);
		unset($fields['officePhone']);
		unset($fields['ssdRegion']);
		unset($fields['companyNumber']);
		unset($fields['companyName']);
		unset($fields['user_id']);
		unset($fields['commercialStatus']);
		
		
		
		
		return $fields;
	}
	
	 public function afterFind()
    {
        parent::afterFind();
		
          
			if(($this->brandLogo!="")&&(!strstr($this->brandLogo,'http')))
			{
				
					$this->brandLogo=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$this->brandLogo); 
					
			}
			
			if(($this->backgroundHexColour!="")&&(!strstr($this->backgroundHexColour,'http'))&&(strstr($this->backgroundHexColour,'.')))
			{
				
					$this->backgroundHexColour=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$this->backgroundHexColour); 
					
			}
			
			

           return true;
       
       
        
    }
    
  /*  public function rules()
    {
        return [];
    }*/
}
