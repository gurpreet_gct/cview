<?php
namespace frontend\models;
use \yii\db\ActiveRecord;
use Yii;

/**
 * User Category Model
 *
 * 
 */
class UserCategory extends ActiveRecord 
{
	  /**
     * @inheritdoc
     */
  
      
    public static function tableName()
    {
         return '{{%userCategory}}';
    }
    
    
	public function init()
	{
		
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
        ];     
    
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'user_id' => Yii::t('app','User Id'),
            'category_id' => Yii::t('app','Category Id'),
            'status' => Yii::t('app','Status'),
            'firstName' => Yii::t('app','First Name'),
            'lastName' => Yii::t('app','Last Name'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }

	
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			
            if ($this->isNewRecord) {
               $this->created_at=time();
            }
			   $this->updated_at=time();
				
            return true;
        }
        return false;
    }
    
}
