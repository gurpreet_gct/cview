<?php
namespace frontend\models;
use \yii\db\ActiveRecord;
use Yii;

/**
 * Category Model
 *
 * 
 */
class Category extends ActiveRecord 
{
	  /**
     * @inheritdoc
     */
  
    public $isSelected=0;  
    public static function tableName()
    {
         return '{{%categories}}';
    }
    
	
	/* Relation with User Category */
	public function getUserCategory()
    {
        return $this->hasMany(UserCategory::className(), ['category_id' => 'id']);
    }
    
	
	public function init()
	{
		
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
        ];     
    
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last Name'),
			'created_at' => Yii::t('app','Created At'),
			'updated_at' => Yii::t('app','Updated At'),
			'root' => Yii::t('app','Root'),
			'lft' => Yii::t('app','Left'),
			'rgt' => Yii::t('app','Right'),
			'lvl' => Yii::t('app','Level'),
			'name' => Yii::t('app','Name'),
			'parentKey' => Yii::t('app','Parent Key'),
			'icon' => Yii::t('app','Icon'),
			'imageurl' => Yii::t('app','Image Url'),
			'icon_type' => Yii::t('app','Icon Type'),
			'active' => Yii::t('app','Active'),
			'selected' => Yii::t('app','Selected'),
			'disabled' => Yii::t('app','Disabled'),
			'readonly' => Yii::t('app','Readonly'),
			'visible' => Yii::t('app','Visible'),
			'collapsed' => Yii::t('app','Collapsed'),
			'moveable_u' => Yii::t('app','Moveable Up'),
			'moveable_d' => Yii::t('app','Moveable Down'),
			'moveable_r' => Yii::t('app','Moveable Right'),
			'moveable_l' => Yii::t('app','Moveable Left'),
			'removeable' => Yii::t('app','Removeable'),
			'removeable_all' => Yii::t('app','Removeable All'),
			'path' => Yii::t('app','Path'),
        ];
    }

/*	public function fields()
	{
		$fields=parent::fields();
		$fields['category_id']='category_id';
		return $fields;
	}
	
	public function extraFields()
	{
		return ['usercategory'];
	}*/
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			$this->fullName= trim($this->firstName . ' ' . $this->lastName);
            if ($this->isNewRecord) {
               
            }
            return true;
        }
        return false;
    }
    
  public $i=0;  
        function recursiveRemoval(&$array, $val)
{
    if(is_array($array))
    {
        foreach($array as $key=>&$arrayElement)
        {
            if(is_array($arrayElement))
            {
                if($key===$val)
                { 
            $this->i++;
            
             if($this->i==2)
             {//    print_r($arrayElement);echo $key;
         }
            $arrayElement=array_values($arrayElement);
/*if($this->i==2)                   
{ 
echo ($key===$val);
   echo $key;
echo $val;

                    print_r($arrayElement);
                    
die;}*/
            }
                $this->recursiveRemoval($arrayElement, $val);
            }
            else
            {
                if($key == $val)
                {
                    //print_r($arrayElement);
                }
            }
        }
    }
}
    
    
}
