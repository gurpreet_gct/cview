<?php
namespace frontend\models;
use Yii;
use common\models\User;
use frontend\models\Profiles;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
/**
 * Customer Model
 *
 * 
 */
class Customer extends User 
{
	  /**
     * @inheritdoc
     */
   public $imageFile;
   
   
   public $when;
   public $number_max;
   public $suggestedUsername; 
   public $usernameChanged=true; 
   
     
    public static function tableName()
    {
         return '{{%users}}';
    }
    
    
	public function init()
	{
		$this->status=10;
		$this->user_type=3;        
        
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status','phone'], 'integer'],                           
            [['firstName', 'lastName','email','sex', 'dob', 'user_type', 'status'], 'required'],
            
            //['username','required','when'=>function($model){return $model->fbidentifier=="";}],
           
			['username','required','on'=>'register'],  
			['username', 'validateRequiredUnique' ,'on'=>'register'],
				
		    	     
            ['email','unique'],
            ['email', 'email'],            
            ['sex', 'number'],            
            ['dob', 'date', 'format' => 'yyyy-M-d'],            
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
              
            [['username'], 'string', 'max' => 50],
            [['fbidentifier','latitude','longitude'], 'safe', ],
            
           ['phone', 'checklength'],
            
         
        ]; 
    
    
    }
    
    
   

 public function checklength($attribute,$params)
   {

      if (strlen($this->$attribute)!=10) {
                 
         $this->addError($attribute, 'Mobile number should be 10 digits');
         
	 }

 }
 
      /**
     * Validates the username.
     * This method serves as the inline validation for username.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateRequiredUnique($attribute, $params)
    {
        
        if (!$this->hasErrors() && (Customer::findOne(['username'=>$this->username])))
            {
				
				if(!$this->usernameChanged)
				{
					return true;
				}
					
                if($this->fbidentifier!="")
                {
                    $this->getSuggestedUser(true);
                    $this->username=$this->suggestedUsername;
                }
                else if($this->google_identifier!="")
                {
                    $this->getSuggestedUser(true);
                    $this->username=$this->suggestedUsername;
                }
                else if($this->linkedin_identifier!="")
                {
                    $this->getSuggestedUser(true);
                    $this->username=$this->suggestedUsername;
                }
                else
                {
                    $this->getSuggestedUser();                    
                    $usernames=implode(',',$this->suggestedUsername);
                    $this->addError($attribute, "Username \"{$this->username}\" already taken. You can use {$usernames}");
                   // $this->addError('suggestedusername', $this->suggestedUsername);
                }
            
          
        }
    }
    
    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			 'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last Name'),
			'fullName' => Yii::t('app','Full Name'),
			'orgName' => Yii::t('app','Org Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb Identifier'),
			'role_id' => Yii::t('app','Role'),
			'user_type' => Yii::t('app','User Type'),
			'username' => Yii::t('app','User Name'),
			'password_hash' => Yii::t('app','Password'),
			'password_reset_token' => Yii::t('app','Password Reset Token'),
			'phone' => Yii::t('app','Mobile'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Gender'),
			'dob' => Yii::t('app','Date Of Birth'),
			'ageGroup_id' => Yii::t('app','Age_Group_Id'),
			'walletPinCode' => Yii::t('app','Wallet Pin Code'),
			'loyaltyPin' => Yii::t('app','Loyalty Pin'),
			'auth_key' => Yii::t('app','Auth Key'),
			'activationKey' => Yii::t('app','Activation Key'),
			'confirmationKey' => Yii::t('app','Confirmation Key'),
			'access_token' => Yii::t('app','Access Token'),
			'hashKey' => Yii::t('app','Hash Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device Token'),
			'lastLogin' => Yii::t('app','Last Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created At'),
            'updated_at'=>Yii::t('app','Updated At'),
        ];
    }

    public static function findcustomer($email)
	{
		return static::find()->where('email = :email',[':email' => $email])
		->orWhere('username = :username',[':username' => $email])
		->andWhere('status =:status', [':status'=> self::STATUS_ACTIVE])
		->andWhere('user_type =:user_type', [':user_type'=> 3])
		->one();
		
	}
	
	public function fields()
	{
		$fields=parent::fields();
		unset($fields['password_hash']);
		unset($fields['password_reset_token']);
		unset($fields['created_at']);
		unset($fields['updated_at']);
		unset($fields['user_type']);
		unset($fields['activationKey']);
		unset($fields['access_token']);
		unset($fields['hashKey']);
		unset($fields['lastLogin']);
		unset($fields['status']);
		unset($fields['role_id']);
		unset($fields['confirmationKey']);
		unset($fields['orgName']);
		return $fields;
	}
    
    public function extraFields()
    {
        return ['profile'];
    }
    
      public function beforeSave($insert)
    {
		
         
			
			
        if (parent::beforeSave($insert)) {
			$this->fullName= trim($this->firstName . ' ' . $this->lastName);
            if(strstr($this->dob,"-"))	{
			   
				$this->dob = strtotime($this->dob);
			}
                         
             if($this->fbidentifier=="" &&  $this->image!="")
             {
				   preg_match('/.*\/(.*?)\.(jpg|jpeg|png)/',$this->image,$match);  
                      
                    if(count($match)) 
                        $this->image=$match[1].'.'.$match[2];
                 
             }
            if ($this->isNewRecord) {
                $this->password_hash=Yii::$app->security->generatePasswordHash($this->password_hash);
                if($this->username=="")
                {
                    $this->getSuggestedUser(true);
                    $this->username=$this->suggestedUsername;
                }
            }
            return true;
        }
        return false;
    }
    
    public function afterFind()
    {
        parent::afterFind();
		
           if(!strstr($this->dob,"-"))
            {               
                $this->dob=date("Y-m-d",$this->dob);
            }
			if(($this->image!="")&&(!strstr($this->image,'http')))
			{
				
					//$this->image=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$this->image); 
					
			}

           return true;
       
       
        
    }
    
     public function getProfile()
    {
       return $this->hasOne(Profiles::className(), ['user_id' => 'id']);       
    }
    
	public function getImageUrl()
	{
		   if(($this->image!="")&&(!strstr($this->image,'http')))
			{
				
					$this->image=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$this->image); 
					
			}
	}
    public function getSuggestedUser($single=false)	
    {
            
             $isUsername = $this->username == $this->email ? true: ($this->username != "" ? true:false);
            if(!$isUsername)
            $this->username=$this->email!=""?$this->email:$this->fullName;
          
            $username=preg_replace('/([^@]*).*/', '$1',$this->username);  
            $username=preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username);
           
           
           $dob=strstr($this->dob,"-")?$this->dob: date("Y-m-d",$this->dob);
           
            list($month, $day, $year) = explode('-', $dob);
            $usernameSuggestion[]=$username;  
            if($isUsername)
            {
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.$this->fullName));               
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.$day));               
            array_push($usernameSuggestion,$username. rand(1,99));  
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.$this->lastName));               
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.".".$this->lastName));               
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."-".$this->lastName));               
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."_".$this->lastName));    
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.".".$day));  
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.$month));  
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.".".$month));  
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.$dob)); 
            
            array_push($usernameSuggestion,$username. rand(100,1000));      
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username."-".$day));              
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username."-".$month));  
            
            
                
            }
            else
            {
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.$this->fullName));               
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.$this->lastName));               
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.".".$this->lastName));               
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."-".$this->lastName));               
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."_".$this->lastName));               
                
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.$day));               
                
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.".".$day));  
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.$month));  
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.".".$month));  
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.$dob)); 
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."-".$day));                  
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."-".$month));  
                array_push($usernameSuggestion,$username. rand(1,99));  
                array_push($usernameSuggestion,$username. rand(100,1000));  
            }
           
            $result=ArrayHelper::map(Customer::find()->where(['username'=>$usernameSuggestion])->select('username')->asArray()->all(),'username','username');       
           
            $result=array_unique(array_diff($usernameSuggestion,$result));         
            if(!$result)
                array_push($result,$username. time());  
            
            $result=array_values($result);
            if($single)
            {
              $this->suggestedUsername=$result[0]  ;
            } 
                else
            $this->suggestedUsername=array_slice($result, 0, 5, true);
        
    }
    
    /* Get Gender of user */
    public function getsextype()
    {
         $sex = $this->sex;
        		 if($sex==1){         	
        				return 'Male';
        		 }else {
        			  	return 'Female';
        		 }
    } 
    
      /* get user status type */
    public function getStatustype()
    {
		
    	  if($this->status==10){
         return 'Active';    	  
    	  }else {
    	  return 'Inactive';
       }
    }
    
    /* get image */
      public function getimagename(){
		  
		 
		 if($this->image!='') {
		   return Yii::$app->params['userUploadPath']."/".$this->image; 
		 }else {
			  return Yii::$app->params['userUploadPath']."/".'no-image.jpg'; 
		 }
     }
    
}
