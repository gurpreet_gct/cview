<?php

namespace frontend\models;
use Yii;
use yii\base\Model;
use common\models\User;
use frontend\models\Customer;
use linslin\yii2\curl;
/**
 * This is the model class for FacebookLogin.
 *
 * @property integer $email
 * @property string $user
 
 */
class FacebookLogin extends Model
{
    /**
     * @inheritdoc
     */
    private $_user=false;
    public $token="";
    public $_tokenError=false;
    public $apiData=false;
    public $device;    
   public function rules()
    {
        return [
        [['token','device'],'required'],
        ['token', 'validateToken'],
       // ['device', 'in','range'=>['android','ios'],'message'=>'Device can be ios or android.'],
        ];
    }
    
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
      			'firstName' => Yii::t('app','First Name'),
      			'lastName' => Yii::t('app','Last Name'),
      			'fullName' => Yii::t('app','Full Name'),
      			'orgName' => Yii::t('app','Org Name'),
      			'email' => Yii::t('app','Email'),
      			'fbidentifier' => Yii::t('app','fb Identifier'),
      			'role_id' => Yii::t('app','Role'),
      			'user_type' => Yii::t('app','User Type'),
      			'username' => Yii::t('app','Username'),
      			'password_hash' => Yii::t('app','Password'),
      			'password_reset_token' => Yii::t('app','Password Reset Token'),
      			'phone' => Yii::t('app','Mobile'),
      			'image' => Yii::t('app','Image'),
      			'sex' => Yii::t('app','Sex'),
      			'dob' => Yii::t('app','Date Of Birth'),
      			'ageGroup_id' => Yii::t('app','Age Group Id'),
      			'walletPinCode' => Yii::t('app','Wallet Pin Code'),
      			'loyaltyPin' => Yii::t('app','Loyalty Pin'),
      			'auth_key' => Yii::t('app','Auth Key'),
      			'activationKey' => Yii::t('app','Activation Key'),
      			'confirmationKey' => Yii::t('app','Confirmation Key'),
      			'access_token' => Yii::t('app','Access Token'),
      			'hashKey' => Yii::t('app','Hash Key'),
      			'status' => Yii::t('app','Status'),
      			'device' => Yii::t('app','Device'),
      			'device_token' => Yii::t('app','Device Token'),
      			'lastLogin' => Yii::t('app','Last Login'),
      			'latitude' => Yii::t('app','Latitude'),
      			'longitude' => Yii::t('app','Longitude'),
      			'created_at'=>Yii::t('app','Created At'),
            'updated_at'=>Yii::t('app','Updated At'),
            
        ];
    }
    
    
    
       /**
     * Validates the token.
     * This method serves as the inline validation for token.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateToken($attribute, $params)
    {
		    if (!$this->hasErrors()) {
            $user = $this->getUser();            
            if (!$user) {
                if ($this->_tokenError) {
                    $this->addError($attribute, 'Invalid token');
                } else {
                    $this->addError("apiData", $this->apiData); 
                }
            }
        }
    }
    
      /**
     * Finds user by [[emali]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
           
            //Init curl
            $curl = new curl\Curl();
            $curl->setOption(CURLOPT_SSL_VERIFYPEER,false);    
            $url=sprintf("https://graph.facebook.com/me?access_token=%s",$this->token);
            $response = $curl->get($url);  

            if($response)
            {
                
                $ob=json_decode($response);
                $this->apiData=$ob;
                $this->_user = Customer::findByIdentifier($ob->id);                
                if($this->_user)
                {
                    $this->_user->firstName=$ob->first_name;
                    $this->_user->lastName=$ob->last_name;
                    $this->_user->sex=$ob->gender=="male"?1:0;
                    $this->_user->fullName=$ob->name;
                    $this->_user->generateAuthKey();
                    $this->_user->device=$this->device;
                    $this->_user->save(false);
                }
                
            }
                else
                    $this->_tokenError= true;
        
       
           
            
        }

        return $this->_user;
    }
    
    public function getUserObject()
    {
         $this->_user->dob=date("Y-m-d", $this->_user->dob);
          return $this->_user;
    }
    
    
}
