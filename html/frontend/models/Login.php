<?php
namespace frontend\models;
use common\models\LoginForm;
use common\models\LoginLogs;
use Yii;
use yii\base\Model;

/**
 * Login 
 */
class Login extends LoginForm
{
    
    private $_user=false;
    
    
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last Name'),
			'fullName' => Yii::t('app','Full Name'),
			'orgName' => Yii::t('app','Org Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb Identifier'),
			'role_id' => Yii::t('app','Role Id'),
			'user_type' => Yii::t('app','User Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New Password'),
			'password_reset_token' => Yii::t('app','Password Reset Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date Of Birth'),
			'ageGroup_id' => Yii::t('app','Age Group Id'),
			'walletPinCode' => Yii::t('app','Wallet Pin Code'),
			'loyaltyPin' => Yii::t('app','Loyalty Pin'),
			'auth_key' => Yii::t('app','Auth Key'),
			'activationKey' => Yii::t('app','Activation Key'),
			'confirmationKey' => Yii::t('app','Confirmation Key'),
			'access_token' => Yii::t('app','Access Token'),
			'hashKey' => Yii::t('app','Hash Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device Token'),
			'lastLogin' => Yii::t('app','Last Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created At'),
            'updated_at'=>Yii::t('app','Updated At'),
            'user_id' => Yii::t('app','User Id'),
			'sessionid' => Yii::t('app','Session Id'),
			'ip' => Yii::t('app','Ip'),
			'device' => Yii::t('app','Device'),
			'login_at' => Yii::t('app','Login At'),
			'logout_at' => Yii::t('app','Logout At'),
			'address' => Yii::t('app','Address'),
			'resolution' => Yii::t('app','Resolution'),
			'my_prefrences' => Yii::t('app','My Prefrences'),
			'my_deals' => Yii::t('app','My Deals'),
			'my_loyalty' => Yii::t('app','My Loyalty'),
			'my_wallet' => Yii::t('app','My Wallet'),
			'my_store' => Yii::t('app','My Store'),
			'help' => Yii::t('app','Help'),
        ];
    }
    
    
    
   
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function login()
    {     
		
			if ($this->validate()) {
            if($this->isMobile)
            {                
                if(User::isUserCustomer($this->username)){
                   $user=$this->getUser();   
                    
                   $user->generateAuthKey();
                
                    $user->device=$this->device;
                    $user->lastLogin=time();
                    $user->save(false);  
                   
                    $loginlog=new LoginLogs();
                    $loginlog->user_id=$user->id;
                    $loginlog->sessionid=$user->auth_key;
                    $loginlog->device=$user->device;
                    $loginlog->login_at=time();
                    $loginlog->ip=$_SERVER['REMOTE_ADDR'];				
                    $loginlog->save();
                    return Yii::$app->user->login($this->getUser());
                }
                else
                {                    
                     $this->addError('password', 'You are not allowed to login the app.');
                    return false;
                }
            }
                else
                {
                    $this->_user->lastLogin=time();
                    $this->_user->device='web';
                    $this->_user->save(false);
                    $identitiyObject=Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
                    $loginlog=new LoginLogs();
                    $loginlog->user_id=$this->_user->id;
                    $loginlog->sessionid=Yii::$app->session->id;
                    $loginlog->device=$this->_user->device;
                    $loginlog->login_at=time();
                    $loginlog->ip=$_SERVER['REMOTE_ADDR'];					
					$loginlog->resolution = $this->resolution ;
                    $loginlog->save();
                    
                    return $identitiyObject;
                }
        } else {
            return false;
        }
    }
    
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Customer::findcustomer($this->username);
        }

        return $this->_user;
    }
    

}
