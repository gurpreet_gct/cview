<?php
namespace frontend\models;
use \yii\db\ActiveRecord;
use common\models\User;
use Yii;

/**
 * User Store Model
 *
 * 
 */
class UserStore extends ActiveRecord 
{
	
	 const STATUS_ACTIVE=1;
	  /**
     * @inheritdoc
     */
	//const ROLE_LOCATION_OWNER=7;
      
    public static function tableName()
    {
         return '{{%userstore}}';
    }
    
    public function status($state = true)
    {
        return $this->andWhere(['status' => $state]);
    }
	public function init()
	{
		
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
        ];     
    
    }
    
     public static function primaryKey(){
       return array('user_id', 'store_id');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [  
			'id' => Yii::t('app','ID'),
			'storename' => Yii::t('app','Store Name'),
			'email' => Yii::t('app','Email'),
			'phoneNumber' => Yii::t('app','Phone Number'),
			'countrycode' => Yii::t('app','Country Code'),
			'mondayopeninghoursAM' => Yii::t('app','Monday Opening Hours AM'),
			'addopeninghours' => Yii::t('app','Add Opening Hours'),
			'mondayopeninghoursPM' => Yii::t('app','Monday Opening Hours PM'),
			'tuesdayopeninghoursAM' => Yii::t('app','Tuesday Opening Hours AM'),
			'tuesdayopeninghoursPM' => Yii::t('app','Tuesday Opening Hours PM'),
			'wednesdayopeninghoursAM' => Yii::t('app','Wednesday Opening Hours AM'),
			'wednesdayopeninghoursPM' => Yii::t('app','Wednesday Opening Hours PM'),
			'thursdayopeninghoursAM' => Yii::t('app','Thursday Opening Hours AM'),
			'thursdayopeninghoursPM' => Yii::t('app','Thursday Opening Hours PM'),
			'fridayopeninghoursAM' => Yii::t('app','Friday Opening Hours AM'),
			'fridayopeninghoursPM' => Yii::t('app','Friday Opening Hours PM'),
			'saturdayopeninghoursAM' => Yii::t('app','Saturday Opening Hours AM'),
			'saturdayopeninghoursPM' => Yii::t('app','Saturday Opening Hours PM'),
			'sundayopeninghoursAM' => Yii::t('app','Sunday Opening Hours AM'),
			'sundayopeninghoursPM' => Yii::t('app','Sunday Opening Hours PM'),
			'sundayFullDayOpen' => Yii::t('app','Sunday Full Day Open'),
			'sundayFullDayClose' => Yii::t('app','Sunday Full Day Close'),
			'mondayFullDayOpen' => Yii::t('app','Monday Full Day Open'),
			'mondayFullDayClose' => Yii::t('app','Monday Full Day Close'),
			'tuesdayFullDayOpen' => Yii::t('app','Tuesday Full Day Open'),
			'tuesdayFullDayClose' => Yii::t('app','Tuesday Full Day Close'),
			'wednesdayFullDayOpen' => Yii::t('app','Wednesday Full Day Open'),
			'wednesdayFullDayClose' => Yii::t('app','Wednesday Full Day Close'),
			'thursdayFullDayOpen' => Yii::t('app','Thursday Full Day Open'),
			'thursdayFullDayClose' => Yii::t('app','Thursday Full Day Close'),
			'fridayFullDayOpen' => Yii::t('app','Friday Full Day Open'),
			'fridayFullDayClose' => Yii::t('app','Friday Full Day Close'),
			'saturdayFullDayOpen' => Yii::t('app','Saturday Full Day Open'),
			'saturdayFullDayClose' => Yii::t('app','Saturday Full Day Close'),
			'address' => Yii::t('app','Address'),
			'street' => Yii::t('app','Street'),
			'city' => Yii::t('app','City'),
			'state' => Yii::t('app','State'),
			'postal_code' => Yii::t('app','Postal Code'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'country' => Yii::t('app','Country'),
			'owner' => Yii::t('app','Owner'),
			'status' => Yii::t('app','Status'),
			'created_at' => Yii::t('app','Created At'),
			'updated_at' => Yii::t('app','Updated At'),
			'created_by' => Yii::t('app','Created By'),
			'updated_by' => Yii::t('app','Updated By'),
        ];
    }
    
     

	
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			
            if ($this->isNewRecord) {
               $this->created_at=time();
            }
			     $this->updated_at=time();							
            return true;
        }
        return false;
    }
 
   
}
