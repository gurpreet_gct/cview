<?php
namespace frontend\models;

use Yii;
use common\models\ChangePassword;
use frontend\models\Customer;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * ChangePassword
 */
class ChangePin extends ChangePassword
{
	
    public $newPassword;
  
    public $newPasswordconfirm;    
	public $auth_key="";
    private $_user = false;    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['newPassword', 'newPasswordconfirm'], 'required'],
            // rememberMe must be a boolean value
			  ['newPasswordconfirm', 'compare','compareAttribute'=>'newPassword','operator'=>'==','message'=>'Your confirm password does not match.'], 
            // password is validated by validatePassword()
          
            
             [['newPassword','newPasswordconfirm'], 'integer'],
             
             
             [['newPassword','newPasswordconfirm'], 'checklength'],
            
        ];
    }
    
     public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last Name'),
			'fullName' => Yii::t('app','Full Name'),
			'orgName' => Yii::t('app','Org Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb Identifier'),
			'role_id' => Yii::t('app','Role Id'),
			'user_type' => Yii::t('app','User Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','Password Hash'),
			'password_reset_token' => Yii::t('app','Password Reset Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date Of Birth'),
			'ageGroup_id' => Yii::t('app','Age Group Id'),
			'walletPinCode' => Yii::t('app','Wallet Pin Code'),
			'loyaltyPin' => Yii::t('app','Loyalty Pin'),
			'auth_key' => Yii::t('app','Auth Key'),
			'activationKey' => Yii::t('app','Activation Key'),
			'confirmationKey' => Yii::t('app','Confirmation Key'),
			'access_token' => Yii::t('app','Access Token'),
			'hashKey' => Yii::t('app','Hash Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device Token'),
			'lastLogin' => Yii::t('app','Last Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created At'),
            'updated_at'=>Yii::t('app','Updated At'),
            'newPassword'=>Yii::t('app','4 Digit PIN'),
            'newPasswordconfirm'=>Yii::t('app','Confirm 4 Digit PIN'),
        ];
    }
    
     public function checklength($attribute,$params)
   {

      if (strlen($this->$attribute)!=4) {
                 
         $this->addError($attribute, 'Pin should be 4 digits');
         
	 }

 }
    

	

}
