<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
      'modules' => [
        'rbac' =>  [
            'class' => 'johnitvn\rbacplus\Module'
    		],
      'gii' => 'yii\gii\Module',
       'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'treemanager' =>  [
        'class' => '\kartik\tree\Module',
        // other module settings, refer detailed documentation
    ]


    ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'frontend\models\Customer',
    'enableAutoLogin' => true,
    'identityCookie' => [
        'name' => '_frontendUser', // unique for frontend
        'path'=>'/frontend/web'  // correct path for the frontend app.
    ]
],
'session' => [
    'name' => '_frontendSessionId', // unique for frontend
    'savePath' => __DIR__ . '/../runtime', // a temporary folder on frontend
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],


    'authClientCollection' => [
        'class' => 'yii\authclient\Collection',
        'clients' => [
            'facebook' => [
                'class' => 'yii\authclient\clients\Facebook',
                'clientId' => '105087376495856',
                'clientSecret' => '64f87ec8530e926a459cf308db693c54',
            ],
        ],
    ]
    ],
    'params' => $params,
	'as beforeRequest' =>
							[  //if guest user access site so, redirect to login page.
							'class' => 'yii\filters\AccessControl',
							'rules' => [
								[
									'actions' => ['login', 'error','forgot'],
									'allow' => true,
								],
								[
									'allow' => true,
									'roles' => ['@'],
								],
							],
						],
];
