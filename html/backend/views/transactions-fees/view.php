<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TransactionsFees */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Transactions Fees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
             <?= Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
     		 <?= Html::a(Yii::t('app','OK'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
             [
				'attribute'=>'charge_type',
				'value'=>$model->charge_type == 1 ? 'Fixed' : 'Percentage',			
            ],
            [
            'attribute'=>'amount',
			'value'=>$model->charge_type == 1 ? '$'.$model->amount : $model->amount.'%',		
            ],
             [
				'attribute'=>'paid_within',
				'value'=>$model->paid_within.'ᵗʰ days',			
            ],
        ],
    ]) ?>

</div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->