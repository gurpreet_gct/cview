<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\User;
use common\models\TransactionsFees;

/* @var $this yii\web\View */
/* @var $model common\models\TransactionsFees */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">


    <?php $form = ActiveForm::begin(); ?>
	 <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'charge_type')->radioList(array(1=>'Fixed',2=>'Percentage')); ?>	
				
   <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

   <p>Deal disbursement will occur after the XX after a user purchases a deal.</p>
	<?php
		foreach (range(1, 28) as $number) {
			$data[$number] = $number.'ᵗʰ day'; 
		}
		echo $form->field($model,'paid_within')->dropDownList($data, ['prompt'=>'Select...','options' => [$model->paid_within => ['selected'=>true]]])->label(false);
	?>
				
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
if($model->charge_type == 1){ ?>
	<script>var wrapper = $("<div class='currency-input' />");
				$('#transactionsfees-amount').wrap(wrapper);
				 $('#transactionsfees-amount').before("<span class='currency-symbol'>$</span>"); </script>
<?php }
?>

<?php 
if($model->charge_type == 2){ ?>
	<script>var wrapper = $("<div class='currency-input' />");
				$('#transactionsfees-amount').wrap(wrapper);
				$('#transactionsfees-amount').before("<span class='percentage-symbol'>%</span>"); </script>
<?php }
?>
<script>
$('#transactionsfees-charge_type').change(function(e){
	var val = $('#transactionsfees-charge_type input[type="radio"]:checked').val();
	if(val == '1'){
		$('.percentage-symbol').remove();
				var wrapper = $("<div class='currency-input' />");
				$('#transactionsfees-amount').wrap(wrapper);
				 $('#transactionsfees-amount').before("<span class='currency-symbol'>$</span>");
				 
			}else if(val == '2'){
				$('.currency-symbol').remove();
				var wrapper = $("<div class='currency-input' />");
				$('#transactionsfees-amount').wrap(wrapper);
				$('#transactionsfees-amount').before("<span class='percentage-symbol'>%</span>");
				//$(this).val(val+'%')
			}
	});
		$('#transactionsfees-amount').change(function(e){
			var val = $(this).val();
			var radio = $('#transactionsfees-charge_type input[type="radio"]:checked').val();
			//alert(radio);
			if(radio == '1'){
				var wrapper = $("<div class='currency-input' />");
				$(this).wrap(wrapper);
				 $(this).before("<span class='currency-symbol'>$</span>");
			}else if(radio == '2'){
				var wrapper = $("<div class='currency-input' />");
				$(this).wrap(wrapper);
				$(this).before("<span class='percentage-symbol'>%</span>");
				
			}
			});
</script>
<style>
.currency-symbol {
  position:absolute;
  padding: 7px 5px;
}
.percentage-symbol {
  position:absolute;
 padding: 7px 5px;
  right : 91.8%;
}
</style>


