 <?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TransactionsFeesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transactions Fees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transactions-fees-index">

  <?php
      echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
			return ['id' => $model['id']];
		},
        'columns'=>  [
			'id',
            'title',
            [
				'attribute'=>'charge_type',
				'value'=>function($model){
					 return $model->charge_type == 1 ? 'Fixed' : 'Percentage';
					},
				'filter' => array(1=>'Fixed',2=>'Percentage'),  		
            ],
            [
            'attribute'=>'amount',
            'value'=>function($model){
				if($model->charge_type == 1)
				return "$".$model->amount;
				else
				return $model->amount."%";
			},
            ],
            [
				'attribute'=>'paid_within',
				'value'=>function($model){
					 return $model->paid_within.'ᵗʰ days';
					},
				'filter' => array(1=>'Fixed',2=>'Percentage'),  		
            ],
           ['class'=>'kartik\grid\ActionColumn','template'=>'{view} {update}'],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [               
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
               Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-list"></i>'. Yii::t('app',' General Transactions Fees'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
</div>
