<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Cardtype;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CardtypeSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Wallet Id');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cardtype-index">


<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
         'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        
        'columns' => [
           

            'id',
            						
			[
            'attribute'=>'cardType',
            'value'=>'cardtype.name',
             'filter' => Html::activeDropDownList($searchModel, 'cardType', ArrayHelper::map(Cardtype::find()->asArray()->where('id'=='cardtype')->all(), 'id', 'name'),['class'=>'form-control','prompt' => 'Select']),
            ],
            
            'nameOnCard',
            'cardNumber',
            'expiry',
           
            // 'updated_at',

            ['class'=>'kartik\grid\ActionColumn',
			'template'=>'{view}'
			
			],
        ],
     'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                /* Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>'Add CardType','data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.*/
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')]) 
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-indent"></i>'.Yii::t('app','Wallet Id '),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
</div>
