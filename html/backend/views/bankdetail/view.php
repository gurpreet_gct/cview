<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Region */

 
 
$this->title = yii::t('app','View Bank Info: ') . ' ' .ucfirst($model->bank_name);
$this->params['breadcrumbs'][] = ['label' => yii::t('app','Bank Info')  , 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="fa fa-bank"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
             <?= Html::a(yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
     		   <?= Html::a(yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a(yii::t('app','OK'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->

 <div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            ['attribute'=>'partner_id',
			 'value'=>$model->partnername,
            ], 
            [   
            'attribute'=>'store_id',
            'value' =>$model->storename,
            ],
			'bank_name',
			'account_name'  ,
			'account_number',
			'routing_number',
            'account_type',
            'holder_type',
            //'disbursement',
            [
            'attribute'=>'disbursement',
		'value'=> $model->disbursement == 1 ? 'Yes':'No',
            ],
            [
            'attribute'=>'status',
		'value'=> $model->status == 1 ? 'Yes':'No',
            ],
          //  'status'
        ],
    ]) ?>
</div>
</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->