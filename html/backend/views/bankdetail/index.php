<?php

use yii\helpers\Html;
#use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Usermanagement;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BankdetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = yii::t('app','Bank details');
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$visiable = 0;
if($userType==1){
$advertiser = ArrayHelper::map(Usermanagement::find() ->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
$visiable = 1;
}else{
		$advertiser='';
	}
?>
<div class="regions-index">
<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
                [
                'attribute'=>'partner_id',
                'visible'=> $visiable,
                'value' => 'partnername',
                'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'data' => $advertiser,
                    'size' => Select2::SMALL,
                    'options' => ['placeholder' => 'Select Partner..'],
                    'pluginOptions' => [
                      'width'=>'150px',
                      'allowClear' => true,
                    ],
                  ],
                ],
            'bank_name',
            'account_name',
           [
            'attribute'=>'created_at',
            'label'=>'Created At',
          	'value' => function ($dataProvider) {
            return date("D, d M Y H:i:s ",$dataProvider["created_at"]);

       		 },
            ],

           ['class'=>'kartik\grid\ActionColumn','template'=>'{view}{update}{delete}'],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']],
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']],
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']],
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>yii::t('app','Add Bank Details'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-bank"></i>'.yii::t('app','Bank Details'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);


?>

</div>
