<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Usermanagement;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Country;
/* @var $this yii\web\View */
/* @var $model app\models\Regions */
/* @var $form yii\widgets\ActiveForm */
$usertype = Yii::$app->user->identity->user_type;
$agency_id = Yii::$app->user->identity->advertister_id;
$id = Yii::$app->user->identity->id;
$partenerId = $usertype == 7 ? $id : $agency_id;
?>
<div class="box-body">
<div class="form-group">
	<?php $form = ActiveForm::begin(); ?>
	 <?php  //echo $form->errorSummary($model); ?>
	<?php if($usertype==1){ ?>
	<?php
		$partners = ArrayHelper::map(Usermanagement::find()
									->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
			echo $form->field($model, 'partner_id')->widget(Select2::classname(), [
				'data' => $partners,
				'options' => ['placeholder' => 'Select a Partner ...'],
				'pluginOptions' => [
				'allowClear' => true
				],
			]);
	?>
	<?php } else { ?>
	<?= $form->field($model, 'partner_id')->hiddenInput(['value' => $partenerId])->label(false) ?>
	<?php	}?>	  	
	<?php  $country_name =  ArrayHelper::map(Country::find()->all(), 'iso_code', 'name'); ?>   
	 <?= $form->field($model, 'country')->dropDownList($country_name, array('prompt'=>yii::t('app','Select'),'options' => ['AUS'=> ['Selected'=>true]])) ?>    
		<?= $form->field($model, 'bank_name',['inputOptions' => ['class' => 'form-control','placeholder'=>$model->getAttributeLabel('bank_name')]])->textInput();?>		
						
		<?= $form->field($model, 'account_name',['inputOptions' => ['class' => 'form-control','placeholder'=>$model->getAttributeLabel('account_name')]])->textInput();?>	
							
		<?= $form->field($model, 'account_number',['inputOptions' => ['class' => 'form-control','placeholder'=>$model->getAttributeLabel('account_number')]])->textInput();?>	
		<?= $form->field($model, 'routing_number',['inputOptions' => ['class' => 'form-control','placeholder'=>$model->getAttributeLabel('routing_number')]])->textInput();?>		
						
		<?= $form->field($model, 'account_type',['inputOptions' => ['class' => 'form-control']])
		->widget(Select2::classname(),[
		"data"=>array("savings"=>"Savings","checking"=>"Check"),
		'options'=> ["class"=>"form-control",'placeholder' =>$model->getAttributeLabel('account_type')]
		]); ?>			
					
		<?= $form->field($model, 'holder_type',['inputOptions' => ['class' => 'form-control']])
		->widget(Select2::classname(),[

		"data"=>array("personal"=>'Personal','business'=>"Business"),
		'options'=> ["class"=>"form-control",'placeholder' =>$model->getAttributeLabel('holder_type')]
		]); ?>	
		
		<?= $form->field($model, 'prefrence',['inputOptions' => ['class' => 'form-control']])
		->widget(Select2::classname(),[
		"data"=>array(1=>"Partner",2=>"Store"),
		'options'=> ["class"=>"form-control"]
		]); ?>	
		<div class="store-list <?php if($model->prefrence==1){ echo 'hidden'; }else{ echo 'hidden'; } ?>">
		<?= $form->field($model, 'store_id')->widget(Select2::classname(), [
				'data' => backend\models\Usermanagement::getStorelist($partenerId),
				'options' => ['placeholder' => 'Select a store ...'],
				'pluginOptions' => [
				'allowClear' => true
				],
			]); 
		?>
		</div>
		<?= $form->field($model, 'disbursement')->dropDownList(array(1=>"Yes",2=>"No"), array('prompt'=>yii::t('app','Select'))) ?>    	
		<?= $form->field($model, 'status')->dropDownList(array(1=>"Yes",2=>"No"), array('prompt'=>yii::t('app','Select'))) ?>    	
								
		<p>
			Your bank account details are stored securely by our payment processor. Back account details are never transmitted to, or stored on, <?= Yii::$app->name; ?> servers.
		</p>
   
    
     <div class="box-footer">
		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? yii::t('app','Create') : yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			<?= Html::a(yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> yii::t('app','Users')]) ?>
		</div>
     </div>
</div>
<?php ActiveForm::end(); ?>
</div>
 <?php $siteurl = Yii::$app->getUrlManager()->getBaseUrl().'/employee/storelist'; ?>
<script type="text/javascript">	
	$('#bankdetail-partner_id').change(function(){
		var partnerId = $(this).val();		
		$.ajax({
			method	:'POST',
			url		: '<?php echo $siteurl; ?>',
			data	: { partnerId : partnerId }
			})
			.done(function(result){
				$('#bankdetail-store_id').empty();	
				data1 = JSON.parse(result);	
				var selected = "<?php echo $model->store_id ?>";
				 var div_data1="<option value=''>select store</option>";   
				 $('#bankdetail-store_id').append(div_data1);      	
				$.each(data1.data, function (key, data) {  
					var param = '';
					if(selected==data.id){
						param = 'selected="selected"';
					}
					var div_data="<option "+param+" value="+data.id+">"+data.storename+"</option>";  
				  $('#bankdetail-store_id').append(div_data); 
				});
				$('#bankdetail-store_id').trigger('change');
			});
	});
	$('#bankdetail-prefrence').change(function(){
		if($(this).val()==2){
			$('.store-list.hidden').removeClass('hidden');
		}else{
			$('.store-list').addClass('hidden');
			$('#bankdetail-store_id').val(['']);
		}
	});
	/*var val = $('#bankdetail-prefrence').val();
	if(val = 2)
	$('.store-list').removeClass('hidden');*/
	$('#bankdetail-partner_id').trigger('change');

</script>

    
