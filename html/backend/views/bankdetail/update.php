<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Region */

$this->title = yii::t('app','Update Bank Info: ') . ' ' . ucfirst($model->bank_name);
$this->params['breadcrumbs'][] = ['label' =>  yii::t('app','Bank Info'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => ucfirst($model->bank_name), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = yii::t('app','Update');
?>
    <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
               <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="fa fa-bank"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                <!-- form start -->
                
                 <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->