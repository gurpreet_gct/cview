<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model common\models\Setting */

$this->title = 'General Setting';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-wrench"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
             <?= Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
     		 <?= Html::a(Yii::t('app','OK'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
             'id',
            'partner_referral',
            'customer_referral',
            [
             'attribute'=>'minimum_cashout',
             'value'=>'$'.$model->minimum_cashout,
            ],
            [
            'attribute'=>'cashout',
            'value'=>$model->cashout ? $model->cashout.'%' : 'not set'
				//return $model->cashout.'%';
            ],
            [
             'attribute'=>'subscription_fees',
             'value'=>'$'.$model->subscription_fees,
            ],
            [
             'attribute'=>'subscription_day',
             'value'=>$model->subscription_day .' ᵗʰ day of each month',
            ],
            [
			 'attribute'=>'created_at',
			 'value'=>date('Y-m-d h:i',$model->created_at),
            ],
           
            [
			 'attribute'=>'updated_at',
			 'value'=>date('Y-m-d h:i',$model->updated_at),
            ],
            [
             'attribute'=>'created_by',
             'value'=>\common\models\User::getUserNameById($model->created_by),
            ],
            [
             'attribute'=>'updated_by',
             'value'=>\common\models\User::getUserNameById($model->updated_by),			 
            ]
        ]
    ]); ?>

</div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->