<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Setting */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Setting',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>


   <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
               <div class=" panel panel-primary">
                <div class="panel-heading">
                    <div class="pull-right">
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                <!-- form start -->

                 <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->