<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Setting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'partner_referral')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_referral')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'minimum_cashout')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'cashout')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subscription_fees')->textInput(['maxlength' => true]) ?>
	<?php
		foreach (range(1, 30) as $number) {
			$data[$number] = $number.'ᵗʰ day'; 
		}
		echo $form->field($model,'subscription_day')->dropDownList($data, ['prompt'=>'Select...','options' => [$model->subscription_day => ['selected'=>true]]]);
	?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
<?php 
if($model->cashout){ ?>
var wrapper = $("<div class='currency-input' />");
$('#setting-cashout').wrap(wrapper);
 $('#setting-cashout').before("<span class='currency-symbol'>%</span>"); </script>

<?php }
?>
<style>
.currency-symbol {
  position:absolute;
  padding: 7px 5px;
    right : 94.1%;
}
.percentage-symbol {
  position:absolute;
 padding: 7px 5px;
  right : 91.8%;
}
</style>
