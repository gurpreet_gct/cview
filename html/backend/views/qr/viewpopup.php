<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Qr */

 
 
$this->title = Yii::t('app','View Qr Code:') . ' ' .ucfirst($model->id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Qr Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
          <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="fa fa-qrcode"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
             <?= Html::a(Yii::t('app','Update'), ['updateframe', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
     		   <?= Html::a(Yii::t('app','Delete'), ['deleteframe', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Preview Url Key</button>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->


    <?php
   
       echo  DetailView::widget([
        'model' => $model,
        'attributes' => [
           
            'data',            
             [
             'attribute'=>'qrkey',
             'value'=>'<span id="qrkey">'.$model->qrkey.'</span>',
              'format' => 'raw',           
             ],
			/*[                     
            'label' => 'Image',
            'value' => $model->imagetype,
            'type' => 'image',
            ], */
                              
            'qrImage:image',
            [                     
            'label' => 'Status',
            'value' => $model->statustype,
             ],
             
            
            
        ],
    ]);
    
    ?>

</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="border-bottom-color:#F1F1F1">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body" style="text-align: center;">
         
			<div class="pop-up-title"> <tr>
				<th> <b>Url Key :- </b> </th>
				<td><?php echo $model->qrkey; ?></td>
				</tr> 				
			</div>
			<div class="pop-up-title"> <tr>
				<th> <b>Note :- </b> </th>
				<td><?= Yii::t('app','copy this url key and paste above key url fields') ?></td>
				</tr> 				
			</div>
        </div>
        <div class="modal-footer" style="text-align: center; border-top-color: #F1F1F1">
          <button type="button" class="btn-info1" data-dismiss="modal" style="padding: 7px 25px;  border-radius: 18px;">OK, thanks!</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
   <script type="text/javascript">
	$(document).ready(function(){
    $("#workorderpopup-passurl", parent.document).val($("#qrkey").text());
});
</script>

