<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Workorders;
use backend\models\Usermanagement;
/* @var $this yii\web\View */
/* @var $model app\models\Regions */
/* @var $form yii\widgets\ActiveForm */

$userType = Yii::$app->user->identity->user_type;

$selPartner = Yii::$app->request->get('id')?[Yii::$app->request->get('id')]:[];
$userId = '';
//print_r($userId);die();
$partner = ArrayHelper::map(Usermanagement::find()->joinwith('workorders',false,'INNER JOIN')->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
$partners  = '';
if(!empty($_GET['partnerId'])){
	$userId =	$_GET['partnerId'];
}
else if(empty($_GET['partnerId']) && $userType == 1){
$partner = ArrayHelper::map(Usermanagement::find()->joinwith('workorders',false,'INNER JOIN')->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
}
else if ($userType == 7){
		$userId = $userType== 7 ? Yii::$app->user->identity->id : Yii::$app->user->identity->advertister_id;
		
}

?>


    <?php $form = ActiveForm::begin(); ?>
	 <div class="box-body">
      <div class="form-group">
	 <?php // echo $form->errorSummary($model); ?>
	 <?php if(empty($_GET['partnerId']) && $userType == 1) {?>
	
	 <?= $form->field($model, 'agency_id')->dropDownList($partner) ?>
		 <?php } else { ?>
		<?= $form->field($model, 'agency_id' )->hiddenInput(['value' => $userId ])->label(false); ?>
		<?php	} ?>	
    <?= $form->field($model, 'data')->textInput(['maxlength' => true]) ?>
     <?= $form->field($model, 'type')->dropDownList(array( 0 =>'Qr Code' ,1 =>'Barcode'), array('prompt' => 'select')) ?>
    <?php // $form->field($model, 'pin')->textInput(['maxlength' => true]) ?>
	
	 <?= $form->field($model, 'status')->dropDownList(array('' => 'Select', 1 =>'Enable', 0 =>'Disable' )) ?>

	
   
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Users']) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>


