 
<?php
use yii\helpers\Html;
#use yii\grid\GridView;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;



/* @var $this yii\web\View */
/* @var $searchModel app\models\RegionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app','Loyalty');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="regions-index">
	
<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
         'rowOptions' => function ($model, $key, $index, $grid) {
		return ['id' => $model['id']];
},
        'columns'=>  [
		'id',
		
		 [
            'attribute'=>'created_at',
            'value'=>'created_at',
            'format'=>'date',
              'filterType'=>GridView::FILTER_DATE_RANGE,
                 'filterWidgetOptions' => [

					'pluginOptions' => [
					'format' => 'YYYY-MM-DD',
					'separator' => ' - ',
					'opens'=>'right',
					] ,

					], 
            ],
             [
            'attribute'=>'order_id',
            'value'=>'order_id',
            ],
            [
            'attribute'=>'user_id',
            'value'=>'customername.username',
            ],
            [
            'attribute'=>'loyaltypoints',
            'value'=>'loyaltypoints',
            ],
            
	
	
            ],
       'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
        'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                // remove this row from export
            ]
        ],
        // set your toolbar
        
        // set export properties
       
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>Yii::t('app','loyalty'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
</div>


