<?php

use yii\helpers\Html;
use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = yii::t('app','Administrators');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">
<?php



        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
            'firstName',
            'lastName',
            
            'username',
            'email:email',
            /*[
            'attribute'=>'user_type',
            'value'=>'usertype.usertype',
            ],*/
            'phone',
							
            [
			'attribute' => 'sex',
			'value' => yii::t('app','sextype'),
			'filter' => array('1' => yii::t('app','Male'), '0' => yii::t('app','Female') ),
			],
			
            
           /*  [
        		'attribute' => 'image',
       		'format' => 'html',    
     			'value' => function ($dataProvider) {
            return Html::img(Yii::getAlias('@web').'/'. $dataProvider['image'],
                ['width' => '70px']);
       		 },
   			 ],*/
            
           /* ['attribute'=>'lastLogin',
            'format'=>'date',
            'xlFormat'=>"mmm\\-dd\\, \\-yyyy",
            
            
            ],
            */
           ['class'=>'kartik\grid\ActionColumn'],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
        'pjaxSettings'=>[
        'neverTimeout'=>true,
          'options'=>[
                'id'=>'kv-unique-id-1',
            ]
    ],
  
      //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=> yii::t('app','Add User'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'responsiveWrap' => false,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-user"></i>'.yii::t('app','Administrators'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 


</div>
