<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
                    'id' => 'search-form'
                ],
    ]); ?>
    
  

    <?= $form->field($model, 'firstName') ?>

    <?= $form->field($model, 'lastName') ?>

    <?= $form->field($model, 'fullName') ?>
    
    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'email') ?>
    
    <?= $form->field($model, 'phone') ?>
    
    <?= $form->field($model, 'sex') ?>
    
    
    
    

    

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'contact_no') ?>

    <?php // echo $form->field($model, 'role') ?>

    <?php // echo $form->field($model, 'active') ?>

    <?php // echo $form->field($model, 'tokenhash') ?>

    <?php // echo $form->field($model, 'tokenexpired') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'modified') ?>

    <div class="form-group">
        <?= Html::submitButton(yii::t('app','Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(yii::t('app','Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
