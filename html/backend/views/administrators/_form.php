<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use paras\cropper\Widget;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>




    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-user-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]);
    ?>
     <div class="box-body">
      <div class="form-group">
      
      
      <?php //echo $form->errorSummary($model); ?>
    <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'lastName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fullName')->textInput(['maxlength' => true]) ?>
      <?php $isDisabled = isset($model->id) ? 'disabled' :'dis' ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true,$isDisabled=>$isDisabled]) ?>
  <?php  if(!isset($model->password_hash)) {?>
    <?= $form->field($model, 'password_hash')->passwordInput() ?>    
   <?= $form->field($model, 'confirmpass')->passwordInput() ?>
    <?php } ?>
   
    <?= $form->field($model, 'status')->dropDownList(array('' => yii::t('app','Select'),10 => yii::t('app','Enable'), 0 => yii::t('app','Disable'))) ?>
 
 
  <!-- <?= $form->field($model, 'user_type')->dropDownList(array(1 => 'Admin')) ?> -->

		
		<?php 		
		echo $form->field($model, 'user_type', [
		    'inputOptions' => [		        
		        'type' =>'hidden',
		         'value' =>1,
		    ],
		])->label(false);
		
		?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>


<?= $form->field($model, 'dob')->widget(
    DatePicker::className(), [
        
		'language' => 'en',
		'size' => 'lg',
        'clientOptions' => [
            'autoclose' => true,
            'endDate' => date('2005-12-31'),
            'format' => 'yyyy-mm-dd',
        ]
]);?>


<div class="help-block"></div>
     <?= $form->field($model, 'phone')->textInput() ?>
                   
    
    <?= $form->field($model, 'sex')->dropDownList(array('' => yii::t('app','Select'), '1' => yii::t('app','Male'), '0' => yii::t('app','Female') )) ?>
    <?php if($model->image !=''){
		$model->imageFile=$model->imagename;
		//echo '<img height="150" width="150" src="'.$model->imagename.'"/>';
		
		} ?>
   <?= $form->field($model, 'imageFile')->widget(Widget::className(), [
	'uploadUrl' => Url::toRoute('administrators/uploadPhoto'),'width'=>150,
	'height'=>150])   ?>
    <?php /* $form->field($model, 'imageFile')->fileInput()*/ ?>
    
    <?php /* if($model->image !=''){
		
		echo '<img height="150" width="150" src="'.$model->imagename.'"/>';
		
		}  */ ?>

    
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? yii::t('app','Create') : yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> yii::t('app','Users')]) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>



  
