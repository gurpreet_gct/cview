<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = yii::t('app','Create Administrator');
$this->params['breadcrumbs'][] = ['label' => yii::t('app','Administrators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-plus"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                
                <div class="panel-before" >    
                <div class="pull-right">
        <div role="toolbar" class="btn-toolbar kv-grid-toolbar">
            <div class="btn-group"><a title="<?= yii::t('app','All User')?>" href="<?=Yii::$app->urlManager->createUrl(['administrators/index',])?>" class="btn btn-primary"><i class="glyphicon glyphicon-user"></i></a> 
            <a data-pjax="0" title="<?= yii::t('app','Reset')?>" href="<?=Yii::$app->urlManager->createUrl(['administrators/create',])?>" class="btn btn-default"><i class="glyphicon glyphicon-repeat"></i></a></div>


        </div>    
    </div>
    
    <div class="clearfix"></div></div>
                
                
                <!-- form start -->
                
                 <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
