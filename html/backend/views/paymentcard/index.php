<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Paymentcardtype;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PaymentcardSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title= Yii::t('app','Wallet cards');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paymentcardtype-index">

<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
         'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},

        'columns' => [


            'id',
			[
            'attribute'=>'user_id',
            'value'=>'username',
             'filter' => Html::activeDropDownList($searchModel, 'user_id', ArrayHelper::map(User::find()->select("id,ifnull(orgName,`fullName`) fullName")->asArray()->where('user_type=7')->all(), 'id', 'fullName'),['class'=>'form-control','prompt' => 'Select']),
            ],

            'cardType',
            'cardholderName',
            [
            'label'=>Yii::t('app','Card Number'),
            'attribute'=>'maskedNumber',
            'value'=>'maskedNumber',
            ],
            //'expirationDate',
            ['attribute'=>'expirationDate',
            'value'=>function($model){
				return $model->expirationMonth.'/'.$model->expirationYear;
			},
            ],

            // 'updated_at',

            ['class'=>'kartik\grid\ActionColumn',
			'template'=>'{view}{delete}'

			],
        ],
     'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']],
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']],
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']],
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>'Add CardType','data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-credit-card"></i>  '.Yii::t('app','Wallet Cards '),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);


?>

</div>
