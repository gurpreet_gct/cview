<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\SubscriptionFees */
/* @var $form yii\widgets\ActiveForm */
$usertype = Yii::$app->user->identity->user_type;
$userId = $usertype== 7 ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
?>
<div class="box-body">

  <?php $form = ActiveForm::begin(); ?>
  <?php //$form->errorSummary($model); ?>
  <?php if(\Yii::$app->session->hasFlash('error')) { ?>
                <div class="alert alert-error">	<?= Yii::$app->session->getFlash('error'); ?></div>
                <?php } ?>
<div class="form-group">
<?php if(Yii::$app->user->identity->user_type==1){ ?>
  <?php
     echo $form->field($model, 'user_id')->widget(Select2::classname(),
              [
              'data' =>common\models\Profile::getAdvertiser(),
              'options' => ['placeholder' => Yii::t('app','Select Partner ...'),'multiple' => false],
              'pluginOptions' => [
              'allowClear' => true
              ],
      ]); ?>

<?php } else { ?>

<?= $form->field($model, 'user_id')->hiddenInput(['value' => $userId])->label(false) ?>
<?php	}?>

  <?php echo $form->field($model, 'maskedNumber',['inputOptions' => ['class' => 'form-control','maxlength'=>16,'placeholder'=>$model->getAttributeLabel('maskedNumber')]])->textInput()?>

  <?php echo $form->field($model, 'cardholderName',['inputOptions' => ['class' => 'form-control','placeholder'=>$model->getAttributeLabel('cardholderName')]])->textInput();?>

  <?php echo $form->field($model, 'expirationMonth',['inputOptions' => ['class' => 'form-control']])
    ->widget(Select2::classname(),[
    "data"=>array_combine(range(1,12),range(1,12)),
    'options'=> ["class"=>"form-control",'placeholder' =>$model->getAttributeLabel('expirationMonth'),]
    ]);?>

  <?php echo $form->field($model, 'expirationYear',['inputOptions' => ['class' => 'form-control']])
    ->widget(Select2::classname(),[
    "data"=>array_combine(range(date("Y"),date("Y")+20,1),range(date("Y"),date("Y")+20,1)),
    'options'=> ["class"=>"form-control",'placeholder' =>$model->getAttributeLabel('expirationYear'),]
    ]);?>

  <?php echo $form->field($model, 'bin',['inputOptions' => ['class' => 'form-control','maxlength'=>3,'placeholder'=>$model->getAttributeLabel('bin')]])->passwordInput();?>

    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
