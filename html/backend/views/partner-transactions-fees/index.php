 <?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Usermanagement;
use common\models\Store;
use common\models\User;
use kartik\daterange\DateRangePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TransactionsFeesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transactions Fee charges';
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$userId = $userType == User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
$visiable = 0;
if($userType==1){
		$partners = ArrayHelper::map(Usermanagement::find()
					->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
		$stores = ArrayHelper::map(Store::find()
					->all(), 'id', 'storename');
		$visiable = 1;
	}else{
			$partners='';
			$stores = ArrayHelper::map(Store::find()->where('owner='.$userId)
					->all(), 'id', 'storename');
	}

?>
<div class="transactions-fees-index">

  <?php
      echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
			return ['id' => $model['id']];
		},
        'columns'=>  [
				 [
					'attribute'=>Yii::t('app','partner_id'),
					'value'=> 'partnernameval',
					'visible'=> $visiable,
					'filterType'=>GridView::FILTER_SELECT2,
					'filterWidgetOptions' => [
					'data' => $partners,
					'size' => Select2::SMALL,
					'options' => ['placeholder' => Yii::t('app','Select partner')],
					'pluginOptions' => [
						'width'=>'150px',
						'allowClear' => true,
					],


					],
				],
				 [
					'attribute'=>Yii::t('app','store_id'),
					'value'=> 'storename',
					'filterType'=>GridView::FILTER_SELECT2,
					'filterWidgetOptions' => [
					'data' => $stores,
					'size' => Select2::SMALL,
					'options' => ['placeholder' => Yii::t('app','Select store')],
					'pluginOptions' => [
						'width'=>'150px',
						'allowClear' => true,
					],


					],
				],
				[
				 'attribute'=>'workorder_id',
				 'value'=>'workordername'
				],
				[
				'attribute'=>'offer_type',
				'value'=>function($model){
					$data = \common\models\PartnerTransactionsFees::getOfferTypes();
					return $data[$model->offer_type];
				},
				 'filter'=>\common\models\PartnerTransactionsFees::getOfferTypes(),
				],
				'orderAmount',
				'transactionFees',
				[
				 'attribute'=>'is_deducted',
				 'value'=>function($model)
					{
						return $model->is_deducted ==0 ? 'Due' : 'Paid';
					},
				'filter' => array(0=>'Due',1=>'Paid'),
				],
				  [
					'attribute'=>'created_at',
					//'value'=> 'created_at',
					'filterType'=>GridView::FILTER_DATE_RANGE,
					'filterWidgetOptions' => [
					'convertFormat'=>true,
					'pluginOptions'=>[
					'locale'=>[
					'format'=>'Y-m-d',
					'separator'=>' - ',
					],
					'opens'=>'left'
					],

					],
				],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true,
        'beforeHeader'=>[
            [
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
               Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-list"></i>'. Yii::t('app',' Transactions Fee charges'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);


?>
</div>
