<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Cardtype */

 
 
$this->title = Yii::t('app','View Cardtype: ') . ' ' .ucfirst($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Cardtypes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="fa fa-indent"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
             <?= Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
     		   <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a(Yii::t('app','Cancel'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->

 <div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        
            [
			'attribute'=>'icon',
			'value'=> $model->icon,
			'format' => ['image',['width'=>'100','height'=>'70']],
			],
		
            [                     
            'label' => Yii::t('app','Status'),
            'value' => $model->statustype,
             ],
             
            
             [                     
            'label' => Yii::t('app','Created At'),
            'value' => date("D, d M Y H:i:s",$model->created_at),
             ],
             [                     
            'label' => Yii::t('app','Updated at'),
            'value' => date("D, d M Y H:i:s",$model->updated_at),
             ],
             
             
           
        ],
    ]) ?>
</div>
</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->