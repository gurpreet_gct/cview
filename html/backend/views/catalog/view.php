<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

 
 
$this->title = Yii::t('app','View Product: ') . ' ' .ucfirst($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$visible = 0;
if($userType==1){
$visible = 1;
}
else{
	$visiable = 0;
}

?>
    <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
             <?= Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
     		   <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a(Yii::t('app','OK'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->

 <div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            /*['attribute' => 'Store Name',
				'visible'=>$visible,
			   'value' => $model->partnernameval,          
            ],*/
            ['attribute' => 'Partner Name',
				'visible'=>$visible,
			   'value' => $model->partnernameval,          
            ],
            'name',
            'sku',            
            'description',
            'urlkey',
			        
			 ['attribute' => 'Price',
			   'value' => $model->priceval             
             ],          
            
             ['attribute' => 'Is online',
			   'value' => $model->onlinestatus             
             ],
           
			 ['attribute' => 'Status',
              'value' => $model->statustype            
             ],
              [                     
            'label' => Yii::t('app','Categories'),
            'value' => implode(",",$categoryTree),
             ],
            
             [
				'attribute'=>'Product image',
				'value'=>   $model->thumbimage,
				'format' => ['image',['width'=>'100','height'=>'100']],
			 ],

        ],
    ]) ?>
    </div>
	<?php if($model->has_option==1 && isset($optionVal)) { ?>
		<div class="box box-success">
			<div class="box-header">
					  <i class="fa fa-check"></i>

					  <h4 class="box-title"><?= Yii::t('app','Ingredient/Options') ?></h4>
			</div>
		   <table class="table table-striped table-bordered detail-view">
			<tbody>
			<tr><th>Title</th><th><?= Yii::t('app','Price') ?></th></tr>
			<?php foreach($optionVal as $_optionVal){ 	?>
				<tr>
					<td><?php echo $_optionVal['title']; ?></td>
					<td><?php echo $_optionVal['price']; ?>	</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		</div>
	<?php } //if($model->has_option==1) ?>
</div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->