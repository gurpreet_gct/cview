<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Profile;
use common\models\User;
use common\models\Product;
use kartik\select2\Select2;
use paras\cropper\Widget;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Regions */
/* @var $form yii\widgets\ActiveForm */

if(Yii::$app->user->identity->user_type==1){
$storelist = ArrayHelper::map(Profile::find()->joinWith('user', false, 'INNER JOIN')
	     ->select(sprintf("%s.id ,%s.user_type ,%s.user_id, %s.companyName",User::tableName(),User::tableName(),Profile::tableName(),Profile::tableName()))
	     ->where(['user_type' => 7,'status' => 10])
	     ->all(), 'id', 'companyName'); 
	     
}else{
$storelist = ArrayHelper::map(Profile::find()->joinWith('user', true, 'INNER JOIN')
	     ->select(sprintf("%s.id ,%s.user_type ,%s.user_id, %s.companyName",User::tableName(),User::tableName(),Profile::tableName(),Profile::tableName()))
	     ->where(['user_type' => 7,'status' => 10,Profile::tableName().'.user_id' =>Yii::$app->user->identity->id])
	     ->all(), 'id', 'companyName');    
}
	$pid = isset($model->p_type)?$model->p_type:1;
	$_GET['pid'] = isset($_GET['pid'])?$_GET['pid']:$pid;
	$userType = Yii::$app->user->identity->user_type;
	$advertizerId = $storelist!=''?key($storelist):0;
?>
  
	
	


    <?php  $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-product-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]); ?>
	<div class="box-body">
		<div class="form-group">		
		<?php if(isset($_GET['id'])) { ?>
			<?php //echo $form->errorSummary($model); ?>
			<!-- set product types  -->				
				<?= $form->field($model, 'p_type')->hiddenInput(['value' =>$_GET['pid']])->label(false) ?>
			 <?php if($userType==1) { ?>
			 <?= $form->field($model, 'store_id')->dropDownList($storelist, array('prompt'=>Yii::t('app','Select'))) ?>
			 <?php }elseif($userType==2 || $userType==7){ 
			 
				echo $form->field($model, 'store_id')->hiddenInput(['value' =>Yii::$app->user->identity->id])->label(false);
				
			  }else {  ?>			
			 <?= $form->field($model, 'store_id')->hiddenInput(['value' =>$advertizerId])->label(false) ?>
			 <?php  }  ?>		
			<!-- set product types end -->
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'sku',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Enter the item SKU - the unique code for this product or a service. Include specific details about the product such as product size variation.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->textInput(['maxlength' => true]) ?>
			<?php 
			if($model->image){ 
				$model->pimage=$model->thumbimage;
				//print_r($model->pimage);die;
				//echo '<img src="'.$model->thumbimage.'"/>'; 
				}
			?>
	
			<?= $form->field($model, 'pimage')->widget(Widget::className(), [
			'uploadUrl' => Url::toRoute('catalog/uploadPhoto'),'width'=>150,
			'height'=>150])   ?>   
			<?= $form->field($model, 'urlkey',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Enter a URL key consisting of lowercase characters with hyphens to separate words. Include the product name and key words to improve the way it is indexed by search engines. For example: /lamp-white-nordic">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->textInput() ?>
			<?= $form->field($model, 'price')->textInput() ?>
			<?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>
			<?= $form->field($model, 'status')->dropDownList(array('' => Yii::t('app','Select'), 1 =>Yii::t('app','Enable'), 0 =>Yii::t('app','Disable') )) ?>		
			<?= $form->field($model, 'is_online')->dropDownList(array('' => Yii::t('app','Select'), 1 =>Yii::t('app','Yes'), 0 =>Yii::t('app','No') )) ?>				
		<?php if($_GET['pid'] == 3 || $_GET['pid'] == 2 || $model->p_type==3|| $model->p_type==2){ ?>	
		<?= $form->field($model, 'qty')->hiddenInput(['value' =>1])->label(false) ?>
		<?= $form->field($model, 'has_option')->checkbox(); ?>  
		<div class="field-product-options" style="display:none;">
			<div class="form-inline input_fields_wrap">	
					<?php if(isset($optionVal)){
						foreach($optionVal as $_optionVal) {
							echo '<div class="form-inline"><br />'	;
							echo $form->field($model, 'title[]')->textInput(['value' =>$_optionVal['title']]);
							echo $form->field($model, 'oprice[]')->textInput(['value' =>$_optionVal['price']]);							
							if($_optionVal['is_included']==0){
							echo '<div class="form-group field-product-is_included"><label for="product-is_included" class="control-label">Is Included</label><select value="0" name="Product[is_included][]" class="form-control" id="product-is_included"><option value="1">Yes</option><option value="0"selected>NO</option></select><div class="help-block"></div></div>';	
							}	else{
								echo '<div class="form-group field-product-is_included"><label for="product-is_included" class="control-label">Is Included</label><select value="0" name="Product[is_included][]" class="form-control" id="product-is_included"><option value="1" selected>Yes</option><option value="0">NO</option></select><div class="help-block"></div></div>';		
							}						
							echo '<button type="submit" class="btn btn-default remove_field">x</button></div>';
						}
					} else { ?>
					<?php echo $form->field($model, 'title[]')->textInput();?>			
					<?php echo $form->field($model, 'oprice[]')->textInput(['value' =>0.00]);?>									
					<?php echo $form->field($model, 'is_included[]')->dropDownList(['1' => 'Yes', '0' => 'NO']); ?>
					<?php } ?>
				
			</div>			
			<br/>	
			<div class="text-center">
				<button class="btn btn-primary add_field_button"><?= Yii::t('app','Add More Fields') ?></button>
			</div>
		</div>
		<div style="display:block;" id="chosentree-wrap">
			<div class="help-block"></div>
				<!-- Show categories -->	
			  <label class="control-label" for="store-owner"><?= Yii::t('app','Select Categories') ?> </label>
			<div class="chosentree"></div> 
			<div class="help-block"></div>
		</div>
			<!-- Show categories -->	
		 			<?php } ?>
		<?php } else { ?>
		<?= $form->field($model, 'p_type')->dropDownList(array('' => Yii::t('app','Select Product Type'),'1' => Yii::t('app','Catalogue Item'), '2' => Yii::t('app','Food Menu Item (simple)'),'3' => Yii::t('app','Food Menu Item (group)') )) ?>
		<?php } ?>
				<!-- Food Order fields -->
		<?php	/* show product list for group products */
			if($_GET['pid'] == 3 || $model->p_type==3){
				$pList =  ArrayHelper::map(Product::find()->select('id,name')
							->where(['status'=>1,'p_type'=>2])->all(), 'id', 'name');
							echo $form->field($model, 'related_product')->widget(Select2::classname(), 
							[
							'data' => $pList,
							'options' => ['placeholder' => Yii::t('app','Select product ...'),'multiple' => true],
							'pluginOptions' => [
							'allowClear' => true
							],
			]); 
			
			}
			?>
		</div><!-- /.box-body -->
	</div><!-- /.box-body -->
<?php if(isset($_GET['id'])) { ?>
<div class="box-footer">
	<div class="form-group">
	<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','App Version')]) ?>
	</div>
</div>
<?php } else { ?>
<div class="box-footer">
	<div class="form-group">
	<?= Html::a('<span class="btn-label">Continue</span>', ['create', 'id' =>0], ['class' => 'btn btn-primary continue']) ?>
	<?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','App Version')]) ?>
	</div>
</div>
<?php } ?>
<?php ActiveForm::end(); ?>
<?php 

$this->registerJs(

    '
var loadChildren = function(node) {
	 
JSONObject =JSON.parse(\' '.$categoryTree.'\');	 

	 //JSONObject = JSON.parse(\'{"id":"01","title":"Node 01","has_children":true,"checked":true,"level":1,"children":[{"id":"011","title":"Node 011","name":"paras","has_children":true,"level":2,"children":[{"id":"0111","title":"Node 0111","has_children":false,"level":3,"children":[]}]}]}\');

	 //for(i=0;i<JSONObject.length;i++)
	 
	node.children.push(JSONObject);	 
	// console.log(node);
	 
        return node;
      };

$(document).ready(function() {
        $("div.chosentree").chosentree({
            width: 500,
			input_placeholder: "Select Category",
            deepLoad: true,
            showtree: true,
            load: function(node, callback) {
                    setTimeout(function() {
          callback(loadChildren(node));
       }, 1000);
            }
        });
});
		'
);
    
    
$this->registerJsFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect-catalog.js', ['position' => \yii\web\View::POS_END]);
$this->registerCssFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.css', ['position' => \yii\web\View::POS_END]);

$siteurl = Yii::$app->getUrlManager()->getBaseUrl(); 
$foodcat =$siteurl.'/catalog/foodcat';
?>
<script type="text/javascript">
$(document).ready(function() {
    var max_fields      = 50; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class=""><div class="form-group field-product-title"><label for="product-title" class="control-label">Title</label><input type="text" name="Product[title][]" class="form-control"><div class="help-block"></div></div><div class="form-group field-product-oprice"><label for="product-oprice" class="control-label">Price</label><input type="text" value="0.00" name="Product[oprice][]" class="form-control"><div class="help-block"></div></div><div class="form-group field-product-is_included"><label for="product-is_included" class="control-label">Is Included</label><select name="Product[is_included][]" class="form-control" id="product-is_included"><option value="1">Yes</option><option value="0">NO</option></select><div class="help-block"></div></div><button type="submit" class="btn btn-default remove_field">x</button></div>'); //add input box
        }
    });
			
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })

});
$('#product-store_id').change(function(){
	$('#chosentree-wrap').show();
	var storeID = $(this).val();
	$.ajax({
			method: "POST",
			url: "<?php echo $foodcat; ?>",
			data: { id:storeID }	
	
	})
	.done(function(result) {	
		 $('div.chosentree').empty();
		JSONObject = JSON.parse(result);
      $('div.chosentree').chosentree({
          width: 500,
          deepLoad: true,
          load: function(node, callback) {
                  callback(JSONObject);
				  setTimeout(function() {
		$('.level-1').attr('disabled',true);
		$('.level-2').attr('disabled',true);
	
		$('input:checkbox:checked').each(function() {
			var ids = this.id;
		   if(parseInt(ids.length)>1);
		   {
			   $('#'+ids).click();	 
				$('#'+ids).click();	 
		   }
		});
	      $('.level-3').change(function(){
			  var id = $(this).attr('id');	
			  if($( ".chzntree-choices.chosentree-choices li" ).size()>2)
			  {
				   $('.chzntree-choices.chosentree-choices a').click();		
					$('input#'+id).click();				
			  }else{							
				$('input#'+id).attr('checked',true);	
			  }				
		});
       }, 500);
          }
      });
	  });
		
});

$('#product-p_type').change(function(){
	var id = $('select[id=product-p_type]').val();
	var aUrl = '<?php echo \Yii::$app->request->BaseUrl;?>/catalog/create?id='+id+'&pid='+id;
	$('.btn.btn-primary.continue').attr('href',aUrl);
});
$('.btn.btn-primary.continue').click(function(e){
	e.preventDefault();
	var id = $('select[id=product-p_type]').val();
	if(id>0){
	window.location=$('.btn.btn-primary.continue').attr('href');
	}
	else{
		alert('Please select product type');
	}
});

$(document).ready(function() {	
	setTimeout(function() {
		$('.level-1').attr('disabled',true);
		$('.level-2').attr('disabled',true);
		$('input:checkbox:checked').each(function() {
			var ids = this.id;
		   if(parseInt(ids.length)>1);
		   {
			   $('#'+ids).click();	 
				$('#'+ids).click();	 
		   }
		});
	      $('.level-3').change(function(){
			  var id = $(this).attr('id');	
			  if($( ".chzntree-choices.chosentree-choices li" ).size()>2)
			  {
				   $('.chzntree-choices.chosentree-choices a').click();		
					$('input#'+id).click();				
			  }else{							
				$('input#'+id).attr('checked',true);	
			  }				
		});
       }, 1500);
$('#product-has_option').change(function(){
	$('.field-product-options').toggle();
	var checkedval = $(this).is(":checked");
	if(checkedval==true){
		$(this).val(1);
	}else{
		$(this).val(0);
	}
});
$(document).ready(function(){
var checkedval = $('#product-has_option').is(":checked");
	if(checkedval==true){
		$('.field-product-options').show();
	}
}); 
}); 

</script>
<style>
.form-.group.field-product-title >input{  width: 100%; }
.btn.btn-default.remove_field {  margin: -9px 2px 1px 27px;}
.form-group.field-product-title {  padding-right: 10px;}.form-group.field-product-oprice {
  padding-right: 9px;}
.form-inline.input_fields_wrap label {  margin: 1px 7px; }
.information {  background-color: #006dd5;  border-radius: 9px;  color: #fff;
  font-size: 12px;  padding:1px;}
h4.bg-info {font-size:14px; line-height: 1.4; padding: 1px 5px; text-align: justify;}

</style>

