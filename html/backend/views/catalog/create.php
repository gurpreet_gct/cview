<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Region */

$this->title = Yii::t('app','Create Catalogue Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Product'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-plus"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
			<div class="panel-before" >  
				  
				<div class="row">
				<div class="col-sm-10 col-md-10 col-lg-10">
					<h4 class="bg-info">Add products to the catalogue to enable them to be sold in-app. Catalogue Items you add will be sold through deals. Menu items you add will be sold through the FOOD MENU button on the store page.
				  </h4>
				</div>
				<div class="col-sm-2" style="margin-top:12px;">
					 <div role="toolbar" class="btn-toolbar kv-grid-toolbar">
						<div class="btn-group"><a title="<?= Yii::t('app','All Products') ?>" href="<?=Yii::$app->urlManager->createUrl(['catalog/index',])?>" class="btn btn-primary"><i class="glyphicon glyphicon-list"></i></a> 
						<a data-pjax="0" title="<?= Yii::t('app','Reset Grid') ?>" href="<?=Yii::$app->urlManager->createUrl(['catalog/create',])?>" class="btn btn-default"><i class="glyphicon glyphicon-repeat"></i></a></div>
					</div>    
				</div>
				
				<div class="clearfix"></div>
				
			</div>
			</div>

                
                
                <!-- form start -->
                
                 <?= $this->render('_form', [
			'model' => $model,
			'categoryTree' =>$categoryTree,
    ]) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
