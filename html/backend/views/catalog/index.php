<?php

use yii\helpers\Html;
#use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Usermanagement;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RegionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Catalogue');
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$visiable = 0;
if($userType==1){
$partners = ArrayHelper::map(Usermanagement::find()->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
$visiable = 1;
}else{
		$partners='';
	}
?>
<div class="catalog-index">


<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
        
            'name',
            'sku',            
           
            [
            'attribute'=>'price',
            'value'=>'priceval',
            ],
            [
            'attribute'=>'image',
            'value'=>'thumbimage',
            'format' => ['image',['width'=>'75','height'=>'75']],
            ],
            
            [
            'attribute'=>'status',
            'value'=>'statustype',
             'filter' => array('1' => Yii::t('app','Enable'), '0' => Yii::t('app','Disable') ),
            ],
			 [
				'attribute'=>'p_type',
				'value'=>'ptype',
				'filter' => array('1' => Yii::t('app','Simple'), '2' => Yii::t('app','FoodOrder-Simple'),'3'=>Yii::t('app','FoodOrder-Group') ),
            ],

          [
				'label' => Yii::t('app','Advertiser'),
				'attribute'=>Yii::t('app','store_id'),           			
				'value'=> 'partnernameval',    
				'visible'=> $visiable,
                'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $partners,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => Yii::t('app','Select partner')],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

            ],
            ],
            
           ['class'=>'kartik\grid\ActionColumn'],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>Yii::t('app','Create Catalogue Item '),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-list"></i>'. Yii::t('app','  Catalogue'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
</div>
