<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use paras\cropper\Widget;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
$usertype = Yii::$app->user->identity->user_type;
?>




    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-user-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]);
    ?>
     <div class="box-body">
      <div class="form-group">
	<?php //echo $form->errorSummary($model); ?>
    <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'lastName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fullName')->textInput(['maxlength' => true]) ?>
      
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?php if(!isset($model->password_hash)) { ?>
    <?= $form->field($model, 'password_hash')->passwordInput(['class'=>'form-control', 'placeholder' => Yii::t('app','4 Digit PIN'),'required'=>'required','pattern'=>'[0-9]{4,4}','title'=>'4 digits only','maxlength' =>4, 'size' =>4 ])->label(Yii::t('app','Password &nbsp;"Should be 4 Digit PIN"')) ?>
     <?= $form->field($model, 'confirmpass')->passwordInput(['class'=>'form-control', 'placeholder' => Yii::t('app','4 Digit PIN'),'required'=>'required','pattern'=>'[0-9]{4,4}','title'=>'4 digits only', 'maxlength' =>4, 'size' =>4 ]) ?>
   <?php  } ?>
    <?= $form->field($model, 'status')->dropDownList(array('' => Yii::t('app','Select'),10 => Yii::t('app','Enable'), 0 => Yii::t('app','Disable'))) ?>
	
	<?php 		
		echo $form->field($model, 'user_type', [
		    'inputOptions' => [
		         'type' =>'hidden',
		         'value' =>3,
		    ],
		])->label(false);
		
		?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
		
		<?= $form->field($model, 'dob')->widget(
		DatePicker::className(), [			
			'language' => 'en',
			'size' => 'lg',
			'clientOptions' => [
				'autoclose' => true,
				'endDate' => date('2005-12-31'),
				'format' => 'yyyy-mm-dd',
			]
	]);?>
	
<div class="help-block"></div>
    <?= $form->field($model, 'phone')->textInput() ?>
    
    <?= $form->field($model, 'sex')->dropDownList(array('' =>Yii::t('app','Select'), '1' => Yii::t('app','Male'), '0' => Yii::t('app','Female') )) ?>
   <?php if($model->image !=''){
		$model->imageFile=$model->imagename;
		//echo '<img height="150" width="150" src="'.$model->imagename.'"/>';
		
		} ?>
   <?php
		if($usertype==1){
			echo $form->field($model, 'referral_percentage')->textInput(); 
		}

	?>
    <?= $form->field($model, 'imageFile')->widget(Widget::className(), [
    'uploadUrl' => Url::toRoute('customers/uploadPhoto'),'width'=>150,
		'height'=>150]) ?>
	
    
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel',  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Users')]) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>
