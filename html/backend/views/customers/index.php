<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Customers');
$this->params['breadcrumbs'][] = $this->title;


 
?>     
<div class="users-index">
<?php
Pjax::begin();
        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
		return ['id' => $model['id']];
	},
        'columns'=>  [
             'firstName',
            'lastName',
            
            'username',
            'email:email',
          
            'phone',
            [
			'attribute' => 'sex',
			'value' => 'sextype',
			'filter' => array('1' => Yii::t('app','Male'), '0' => Yii::t('app','Female') ),
			],
           
            
         /*   ['attribute'=>'lastLogin',
            'format'=>'date',
            'xlFormat'=>"mmm\\-dd\\, \\-yyyy",
            
            
            ], */
           ['class'=>'kartik\grid\ActionColumn'],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
            'pjaxSettings'=>[
        //    'options'=>['id'=>'w0-pajax'],  
 'options' => [
        'enablePushState' => false,
    ],        
        'neverTimeout'=>true,
    ///    'beforeGrid'=>'My fancy content before.',
     //   'afterGrid'=>'My fancy content after.',
    ],
      //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>yii::t('app','Add Customer'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-user"></i>'.Yii::t('app','Users'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    Pjax::end();

?>
</div>
