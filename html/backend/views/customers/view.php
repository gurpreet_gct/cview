<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

 
 
$this->title = Yii::t('app','View : ').ucfirst($model->username);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Customers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if(Yii::$app->getSession()->hasFlash('success')):?>
			<div class="alert alert-success">
				<div> <?php echo Yii::$app->getSession()->getFlash('success'); ?></div>
			</div>
   
		<?php endif; ?>
          <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
             <?= Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
     		 <?= Html::a(Yii::t('app','Change Password'), ['changepassword', 'id' => $model->id], [
            'class' => 'btn btn-warning',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to change password?'),
                'method' => 'post',
            ],
        ]) ?>
     		 <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
          <?= Html::a(Yii::t('app','Cancel'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->

   
 <div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'firstName',
            'lastName',
            'fullName',
            'username',
            'email:email',
            [                     
            'label' => Yii::t('app','User Type'),
            'value' => $model->usertype->usertype,
             ],
            'phone',
           [                     
            'label' => Yii::t('app','Sex'),
            'value' => $model->sextype,
             ],
            [                     
            'label' => Yii::t('app','Date of Birth'),
            'value' => $model->dob,
             ],
			[                     
            'label' => Yii::t('app','Status'),
            'value' => $model->statustype,
             ],
             'referral_percentage',
            [
			'attribute'=>'image',
			'value'=> $model->imagename,
			'format' => ['image',['width'=>'70','height'=>'70']],
			 ],
            
        ],
    ]) ?>
</div>
</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->


