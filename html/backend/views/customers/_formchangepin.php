<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>




    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-user-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]);
    $userType = Yii::$app->user->identity->user_type;
    ?>
     <div class="box-body">
      <div class="form-group">
		    <?php //echo $form->errorSummary($model); ?>
		    <?php if($userType != 1 && $userType!= 7 ) { ?>
	<?= $form->field($model, 'oldPassword')->passwordInput()->label(Yii::t('app','Old PIN')) ?>
	<?php } ?>
    <?= $form->field($model, 'newPassword')->passwordInput()->label(Yii::t('app','New PIN')) ?>
   
    <?= $form->field($model, 'newPasswordconfirm')->passwordInput()->label(yii::t('app','Confirm PIN') )?>
    
    
    </div><!-- /.box-body -->
     <div class="box-footer"> 

    <div class="form-group">
        <?= Html::submitButton(yii::t('app','Change PIN'), ['class' => 'btn btn-success' ]) ?>
        <?= Html::a(yii::t('app','Cancel'),  ['customers/view','id'=>Yii::$app->request->get('id')], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> yii::t('app','Change PIN')]) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>



  
