<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = Yii::t('app','Update Partners: ') . ' ' . ucfirst($model->username);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Partners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
               <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                <!-- form start -->
                
                 <?= $this->render('_form',array('model'=>$model,'userdetails'=>$userdetails,'categoryTree'=>$categoryTree,'foodCategoryTree' =>$foodCategoryTree,'loyalty'=>$loyalty,'transList' => $transList,'transModel' => $transModel,'setting' =>$setting)) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
<style>
#location-owner { display: none; }
.field-userextrafield-commercialstatus { display : none; }
#cmpy-information { display : none; }
#lal-long { display : none; }
</style>
