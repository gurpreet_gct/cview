<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Usertype;
use common\models\User;
use common\models\Country;
use backend\models\Regions;
use dosamigos\datepicker\DatePicker;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
use paras\cropper\Widget;
/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
$usertype = Yii::$app->user->identity->user_type;
$usertypeid = Yii::$app->user->identity->id;
?>
 <!--script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script-->
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-user-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]);
    ?>
     <div class="box-body">
      <div class="form-group">
	<?php  //	echo $form->errorSummary($model); // uncomment for show error
			//echo $form->errorSummary($userdetails);
			//echo $form->errorSummary($transModel);
	?>
	<?php
		echo $form->field($model, 'user_type', [
		    'inputOptions' => [
		        'type' =>'hidden',
		         'value' =>7,
		    ],
		])->label(false);
		?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?php if(!isset($model->id)) { ?>
    <?= $form->field($model, 'password_hash')->passwordInput() ?>
	 <?= $form->field($model, 'confirmpass')->passwordInput(); } ?>
   	<?= $form->field($userdetails, 'companyName')->textInput() ?>
 	<?= $form->field($userdetails, 'companyNumber')->textInput() ?>
    <?php   $ssdRegion =  ArrayHelper::map(Regions::find()->where(['status' => 1])->all(), 'id', 'region'); ?>
    <?= $form->field($userdetails, 'ssdRegion')->dropDownList($ssdRegion, array('prompt'=>Yii::t('app','Select'))) ?>
   <div class="box box-success">
    <h3> Head Office Address</h3>
		<?php //echo $form->field($userdetails, 'houseNumber')->textInput() ?>
		<?= $form->field($userdetails, 'street')->textInput() ?>
		<?= $form->field($userdetails, 'city')->textInput() ?>
 		<?= $form->field($userdetails, 'state')->textInput() ?>
 		<?php   $countryNames =  ArrayHelper::map(Country::find()->all(), 'name', 'name'); ?>
 		<?= $form->field($userdetails, 'country')->dropDownList($countryNames, ['prompt'=>Yii::t('app','Select'),'options' => ['Australia'=> ['Selected'=>true]]]) ?>
		<?= $form->field($userdetails, 'postcode')->textInput() ?>
		<?= $form->field($userdetails, 'officePhone')->textInput(['placeholder'=>'+ ( __ )']) ?>
		<?= $form->field($userdetails, 'officeFax')->textInput(['placeholder'=>'+ ( __ )']) ?>
	</div>
	 <div class="box box-success">
	 <h3> Contact Person : </h3>
    <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'lastName')->textInput(['maxlength' => true])?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone')->textInput(['placeholder'=>'+ ( __ )'])->label(Yii::t('app','Mobile Number')) ?>
	<?= $form->field($userdetails, 'alternativeContactperson')->textInput() ?>
	</div>
       <div id="lal-long">
		<?= $form->field($userdetails, 'latitude')->textInput() ?>
		<?= $form->field($userdetails, 'longitude')->textInput() ?>
		</div>
	<div class="box box-success">
	 <h3> Store/Brand Details</h3>
    <?= $form->field($userdetails, 'brandName')->textInput(['maxlength' => true]) ?>
    <?php
      if(!empty($userdetails->brandLogo)) {
			$userdetails->brandimgLogo=Yii::$app->params['userUploadPath']."/".$userdetails->brandLogo;
		  }
	?>
     <?= $form->field($userdetails, 'brandimgLogo')->widget(Widget::className(),
		['uploadUrl' => Url::toRoute('partners/uploadPhoto'),
		'width'=>132,
		'height'=>132]) ?>
    <?= $form->field($userdetails, 'brandAddress',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="This address displays in the My Stores section when user clicks on READ MORE ">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->textInput(['maxlength' => true])?>
    <?= $form->field($userdetails, 'brandDescription',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="This information displays in the My Stores section when user clicks on READ MORE">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->widget(CKEditor::className(), [
				'options' => ['rows' => 3],
				'preset' => 'basic'
			]) ?>
	 <?php
     /* Check Background image */
     if(!empty($userdetails->backgroundHexColour)) {
		$userdetails->backgroundImage=Yii::$app->params['userUploadPath']."/".$userdetails->backgroundHexColour;
		  }?>
		<?= $form->field($userdetails, 'backgroundImage',[
		'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="This image displays when user opens the store in the My Stores section">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->widget(Widget::className(), [
		'uploadUrl' => Url::toRoute('partners/uploadPhoto'),'width'=>640,
		'height'=>360]) ?>
		<?= $form->field($userdetails, 'foregroundHexColour',[
		'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="This is the colour of the text that appears on the background image when user opens the store in My Stores">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->textInput() ?>
		<?= $form->field($userdetails, 'addLoyalty',[
		'template' => '<div class="row"><div class="col-sm-12">{input}{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Tick here to set loyalty dollors values when earning and redeeming loyalty dollors in your store.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true" ></i>&nbsp;&nbsp;</span>{error}{hint}</div></div>'])->checkbox(); ?>
		<?= $form->field($userdetails, 'minimumSpend')->textInput(); ?>
		<?= $form->field($model, 'bdayOffer')->checkbox(); ?>
		<!-- show food category -------------------------------------------------->
		<?= $form->field($userdetails, 'foodEstablishment',[
		'template' => '<div class="row"><div class="col-sm-12">{input}{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Tick here if your store is a food establishment.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true" ></i>&nbsp;&nbsp;</span>{error}{hint}</div></div>'])->checkbox(); ?>
		<div id="foodcat-wrap" style="display:none;">
		<?= $form->field($userdetails, 'shopNow',[
		'template' => '<div class="row"><div class="col-sm-12">{input}{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Tick this to add the FOOD MENU button to the store in the My Stores section. This button allows users to open your menu and order food.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true" ></i>&nbsp;&nbsp;</span>{error}{hint}</div></div>'])->checkbox(); ?>
		<?= $form->field($userdetails, 'geo_radius',[
		'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Add a radius (in km) which allows the app to notify the user that it&rsquo;s time to inform the kitchen to start cooking their ordered meal.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true" ></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->textInput() ?>
		<div class="help-block"></div>
		<!-- Show categories -->
		<label class="control-label" for="store-owner">Select Food Categories </label>
		<span class="information" data-toggle="tooltip" data-placement="right" title="Select the categories that will appear in your menu when users order in the app.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span>
		<div class="foodchosentree"></div>
		</div>
		<br />
		<!-- show food category -------------------------------------------------->
	    <?= $form->field($userdetails, 'socialcheck',[
		'template' => '<div class="row"><div class="col-sm-12">{input}{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Enter the store&rsquo;s social media URLS here to help drive traffic to your socials.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true" ></i>&nbsp;&nbsp;</span>{error}{hint}</div></div>'])->checkbox(['label'=>Yii::t('app','Add "Social media buttons and URLs"')]); ?>
    <button type="button" id='show-addloyalty-target' class="btn btn-info btn-lg" style="display:none" data-toggle="modal" data-target="#addloyaltymodel"><?= Yii::t('app','Open Modal') ?></button>
  <!-- transaction rule start from here -->
	<?php if($usertype==1) { 
			echo $form->field($userdetails, 'transactionrule')
			->checkbox(['label'=>Yii::t('app','Add "Transaction Rule"')]); 
		}
	 ?>
     <button type="button" id='transactionrule-target' class="btn btn-info btn-lg hidden"  data-toggle="modal" data-target="#transactionrule"></button> <!-- Modal -->
<div class="modal modal-fullscreen fade" id="transactionrule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title"><?= Yii::t('app','Transaction Rule') ?></h4>
        </div>
      <div class="modal-body">
       <table class="table table-bordered">
						<tr class="info"><th>Choose</th><th>Amount deduct Type</th><th>Amount</th><th>Paid Within</th><th>Use general Rule</th></tr>
						<?php $i=1; foreach($transList as $_val) {  ?>
							<tr>
								<td>
								<label>
								<?php $trans = 'trans_id_'.$_val['id'];?>
								<?= $form->field($transModel,$trans)->dropDownList(array('0'=>'No',1=>'Yes'), ['prompt'=>'Select...','options' => [$transModel->$trans => ['selected'=>true]],'idsval'=>$i])->label($_val['title']); ?>
									</label>
								</td>
								<td>
									<?= $form->field($transModel,'charge_type_'.$_val['id'])->radioList(array('1'=>'Fixed',2=>'%'))->label(false); ?>
								</td>
								<td>
								<?= $form->field($transModel,'amount_'.$_val['id'])->textInput(['size'=>5])->label(false); ?>
								</td>
								<td>
								<?php
							    	$filedname = 'paid_day_'.$_val['id'];
									foreach (range(1, 28) as $number) {
										$data[$number] = $number.'ᵗʰ day';
									}
									echo $form->field($transModel,$filedname)->dropDownList($data, ['prompt'=>'Select...','options' => [$transModel->$filedname => ['selected'=>true]]])->label(false);
								?>
								</td>
								<td>
								<?php $general = 'general_'.$_val['id'];?>
								<input id="partnerfees-general_<?= $_val['id'] ?>" value="<?= $transModel->$general ?>" type="checkbox"  onclick="fillme(<?= $_val['id']; ?>);" <?php if($transModel->$general==1) { echo 'checked="checked"'; } ?>  name="PartnerFees[general_<?= $_val['id']; ?>]">
								</td>
							</tr>
						<?php $i++;  } ?>
					</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- transaction rule end  here -->
<!-- subscription fees  here -->
<?php
	if($usertype==1){ 
		$subscriptionfee = $setting->subscription_fees;
		$subscriptionday = $setting->subscription_day;
		//$userdetails->subscriptionCheck = isset($userdetails->subscriptionFees) ? 1 :0;
		echo $form->field($userdetails, 'subscriptionCheck')
			->checkbox(['label'=>Yii::t('app','Add "Subscription fees"')]);
			
	echo '<div class="subscriptionCheck-wrap hidden">';	
	if(empty($userdetails->subscriptionFees))
	 echo '<input type="hidden" id="fees" value="'.$subscriptionfee.'">';
	 if(empty($userdetails->subscriptionDueDate))
		 echo '<input type="hidden" id="days" value="'.$subscriptionday.'">';	
	echo $form->field($userdetails, 'subscriptionFees')->textInput(['maxlength' => true]); 
	 foreach (range(1, 30) as $number) {
				$dayofmonth[$number] = $number.'ᵗʰ day';
		}
		
		
	 echo $form->field($userdetails,'subscriptionDueDate')->dropDownList($dayofmonth, ['prompt'=>'Select...','options' => [$userdetails->subscriptionDueDate => ['selected'=>true]]]);
	 $paysubscription = array('2'=>'Credit Card','1'=>'Bank Account'); 
	 echo $form->field($userdetails,'subscriptionPay')->dropDownList($paysubscription, ['prompt'=>'Select...','options' => [$userdetails->subscriptionDueDate => ['selected'=>true]]]);
	 echo '</div>';
	}
 ?>
   <button type="button" id='show-social-target' class="btn btn-info btn-lg" style="display:none" data-toggle="modal" data-target="#socialmodel"><?= Yii::t('app','Open Modal') ?></button>
   <label class="control-label" for="store-owner"><?= Yii::t('app','Select Categories') ?> </label> <span class="information" data-toggle="tooltip" data-placement="right" title="Choose the categories that your store belongs in. This will help app users find you easily.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true" >&nbsp;&nbsp;</i></span>
	<div class="chosentree"></div>
	</div>
	 <!-- Modal -->
  <div class="modal fade" id="socialmodel" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Add social media buttons and URLs') ?></h4>
        </div>
        <div class="modal-body">
         <?= $form->field($userdetails, 'socialmediaFacebook')->textInput() ?>
		<?= $form->field($userdetails, 'socialmediaTwitter')->textInput() ?>
		<?= $form->field($userdetails, 'socialmediaGoogle')->textInput() ?>
		<?= $form->field($userdetails, 'socialmediaPinterest')->textInput() ?>
		<?= $form->field($userdetails, 'socialmediaInstagram')->textInput() ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Save and Close') ?></button>
        </div>
      </div>
    </div>
  </div>
 <div class="modal fade" id="addloyaltymodel" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Loyalty') ?></h4>
        </div>
        <div class="modal-body">
			<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
				<?= $form->field($loyalty, 'loyalty')->textInput(['type'=>'number', 'min'=>'1','step'=>'0.01']); ?>
				</div>
				<div class="col-md-6">
				<?= $form->field($loyalty, 'dolor')->textInput(['type'=>'number', 'min'=>'0.1','step'=>'0.01']); ?>
				<?= $form->field($loyalty, 'pointvalue')->hiddenInput()->label(false); ?>
				</div>
				</div>
				<div class="col-md-12">
				<div class="modal-title"><?= Yii::t('app','1 Dollar  =') ?> <span id="total_loyalty"><b><?php if(isset($loyalty->pointvalue)){ echo $loyalty->pointvalue;}else{ echo "X";}?></b> </span><?= Yii::t('app','Loyalty dollors') ?></div>
				</div>
				<div class="col-md-12">
				<div class="modal-header">
					<h4 class="modal-title"><?= Yii::t('app','Redeem') ?></h4>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-6">
				 <?= $form->field($loyalty, 'redeem_loyalty')->textInput(['type'=>'number', 'min'=>'1','step'=>'0.01']); ?>
				</div>
				<div class="col-md-6">
				 <?= $form->field($loyalty, 'redeem_dolor')->textInput(['type'=>'number', 'min'=>'0.1','step'=>'0.01']); ?>
				 <?= $form->field($loyalty, 'redeem_pointvalue')->hiddenInput()->label(false); ?>
				</div>
				</div>
				<div class="col-md-12">
				  <div class="modal-title"><?= Yii::t('app','1 loyalty dollors  =') ?><span id="total_loyalty_point"><b><?php if(isset($loyalty->redeem_pointvalue)){ echo $loyalty->redeem_pointvalue;}else{ echo "X";}?></b> </span><?= Yii::t('app',' Dollar') ?></div>
				</div>
			<div class="col-md-12">
				<div class="modal-header">
				</div>
				<div class="col-md-6">
				<?php # $form->field($loyalty, 'loyalty_pin')->textInput(['type'=>'password']); ?>
				</div>
			</div>
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Save and Close') ?></button>
        </div>
      </div>
    </div>
  </div>
</div>
    </div><!-- /.box-body -->
     <div class="box-footer">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
      <?php  if($usertype ==1): 
		echo Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Users']);
       else:
			echo Html::a(Yii::t('app','Cancel'),  ['view', 'id' => $model->id], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Users']);
		endif;
      ?>
    </div>
     </div>
    <?php ActiveForm::end(); ?>
<!-- show food category -------------------------------------------------->
 <?php
$this->registerJs(
    '
var loadChildren2 = function(node) {
JSONObject =JSON.parse(\' '.$foodCategoryTree.'\');
	 //JSONObject = JSON.parse(\'{"id":"01","title":"Node 01","has_children":true,"checked":true,"level":1,"children":[{"id":"011","title":"Node 011","name":"paras","has_children":true,"level":2,"children":[{"id":"0111","title":"Node 0111","has_children":false,"level":3,"children":[]}]}]}\');
	 //for(i=0;i<JSONObject.length;i++)
	node.children.push(JSONObject);
	// console.log(node);
        return node;
      };
$(document).ready(function() {
        $("div.foodchosentree").chosentree({
            width: 500,
			inputName: "foodcatname",
			input_placeholder: "Select Food Category",
            deepLoad: true,
            showtree: true,
            load: function(node, callback) {
                    setTimeout(function() {
          callback(loadChildren2(node));
       }, 1000);
            }
        });
});
		'
);
?>
<!-- show food category -------------------------------------------------->
<?php
$this->registerJs(
    '
var loadChildren = function(node) {
JSONObject =JSON.parse(\' '.$categoryTree.'\');
	 //JSONObject = JSON.parse(\'{"id":"01","title":"Node 01","has_children":true,"checked":true,"level":1,"children":[{"id":"011","title":"Node 011","name":"paras","has_children":true,"level":2,"children":[{"id":"0111","title":"Node 0111","has_children":false,"level":3,"children":[]}]}]}\');
	 //for(i=0;i<JSONObject.length;i++)
		node.children.push(JSONObject);
		//console.log(node);
        return node;
      };
$(document).ready(function() {
        $("div.chosentree").chosentree({
            width: 500,
            deepLoad: true,
              showtree: true,
            load: function(node, callback) {
                    setTimeout(function() {
          callback(loadChildren(node));
       }, 1000);
            }
        });
});
		'
);
$this->registerJsFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.js', ['position' => \yii\web\View::POS_END]);
$this->registerCssFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.css', ['position' => \yii\web\View::POS_END]);
//$this->registerJsFile(Yii::$app->homeUrl.'store-address-suggest.js', ['position' => \yii\web\View::POS_END]);

?>
<script type="text/javascript">
	$('#profile-subscriptioncheck').change(function(){
		if($(this).is(':checked')){
			$('#profile-subscriptioncheck').val(1);
			$("input[name='Profile[subscriptionCheck]']").val(1);
			var fee = $('#fees').val();
			
			var day = $('#days').val();
			$('#profile-subscriptionfees').val(fee);
			$('#profile-subscriptionduedate option:contains(' + day + ')').attr('selected', 'selected');
			$('.subscriptionCheck-wrap').removeClass('hidden');
		} else {
			$('#profile-subscriptioncheck').val(0);
			$("input[name='Profile[subscriptionCheck]']").val(0);
			$('.subscriptionCheck-wrap').addClass('hidden');
		}
	});
$('#profile-subscriptioncheck').trigger('change');
/* auto file full name */
$('#profile-addloyalty').click(function(){
		if (this.checked) {
			$('#show-addloyalty-target').click();
		}
	})
	$('#profile-socialcheck').click(function(){
		if (this.checked) {
			$('#show-social-target').click();
		}
	})
	$('#profile-addcolors').click(function(){
		if (this.checked) {
			$('#show-color-model').click();
		}
	})
$('#profile-city').keypress(function() {
var city =  $(this).val();
});
$('#loyalty-loyalty').change(function() {
	var loyalty =  parseFloat($('#loyalty-loyalty').val());
	var dolor =  parseFloat($('#loyalty-dolor').val());
	var mul = parseFloat((loyalty/dolor));
  if(isNaN(mul)){
    return;
  }
	$('#total_loyalty').html('<b>'+mul.toFixed(4)+'</b>');
	$('#loyalty-pointvalue').val(mul.toFixed(4));
	return false;
});
$('#loyalty-loyalty').keyup(function() {
	var loyalty =  parseFloat($('#loyalty-loyalty').val());
	var dolor =  parseFloat($('#loyalty-dolor').val());
	var mul = parseFloat((loyalty/dolor));
  if(isNaN(mul)){
    return;
  }
	$('#total_loyalty').html('<b>'+mul.toFixed(4)+'</b>');
	$('#loyalty-pointvalue').val(mul.toFixed(4));
	return false;
});
$('#loyalty-dolor').change(function() {
	var loyalty =  parseFloat($('#loyalty-loyalty').val());
	var dolor =  parseFloat($('#loyalty-dolor').val());
	var mul = parseFloat((loyalty/dolor));
  if(isNaN(mul)){
    return;
  }
	$('#total_loyalty').html('<b>'+mul.toFixed(4)+'</b>');
	$('#loyalty-pointvalue').val(mul.toFixed(4));
	return false;
});
$('#loyalty-dolor').keyup(function() {
	var loyalty =  parseFloat($('#loyalty-loyalty').val());
	var dolor =  parseFloat($('#loyalty-dolor').val());
	var mul = parseFloat((loyalty/dolor));
  if(isNaN(mul)){
    return;
  }
	$('#total_loyalty').html('<b>'+mul.toFixed(4)+'</b>');
	$('#loyalty-pointvalue').val(mul.toFixed(4));
	return false;
});
$('#loyalty-redeem_loyalty').change(function() {
	var redeemloyalty =  parseFloat($('#loyalty-redeem_loyalty').val());
	var redeemdolor =  parseFloat($('#loyalty-redeem_dolor').val());
	var mul = parseFloat((redeemdolor/redeemloyalty));
  if(isNaN(mul)){
    return;
  }
	$('#total_loyalty_point').html('<b>'+mul.toFixed(4)+'</b>');
	$('#loyalty-redeem_pointvalue').val(mul.toFixed(4));
	return false;
});
$('#loyalty-redeem_loyalty').keyup(function() {
	var redeemloyalty =  parseFloat($('#loyalty-redeem_loyalty').val());
	var redeemdolor =  parseFloat($('#loyalty-redeem_dolor').val());
	var mul = parseFloat((redeemdolor/redeemloyalty));
  if(isNaN(mul)){
    return;
  }
	$('#total_loyalty_point').html('<b>'+mul.toFixed(4)+'</b>');
	$('#loyalty-redeem_pointvalue').val(mul.toFixed(4));
	return false;
});
$('#loyalty-redeem_dolor').change(function() {
	var redeemloyalty =  parseFloat($('#loyalty-redeem_loyalty').val());
	var redeemdolor =  parseFloat($('#loyalty-redeem_dolor').val());
	var mul = parseFloat((redeemdolor/redeemloyalty));
  if(isNaN(mul)){
    return;
  }
	$('#total_loyalty_point').html('<b>'+mul.toFixed(4)+'</b>');
	$('#loyalty-redeem_pointvalue').val(mul.toFixed(4));
	return false;
});
$('#loyalty-redeem_dolor').keyup(function() {
	var redeemloyalty =  parseFloat($('#loyalty-redeem_loyalty').val());
	var redeemdolor =  parseFloat($('#loyalty-redeem_dolor').val());
	var mul = parseFloat((redeemdolor/redeemloyalty));
  if(isNaN(mul)){
    return;
  }
	$('#total_loyalty_point').html('<b>'+mul.toFixed(4)+'</b>');
	$('#loyalty-redeem_pointvalue').val(mul.toFixed(4));
	return false;
});
$('#profile-state').keypress(function() {
 var state = $(this).val();
 var city =  $('#profile-city').val();
});
$("#profile-country").on("change", function () {
	var country = this.value;
	var city =  $('#profile-city').val();
	var state =  $('#profile-state').val();
 var address =  city+' '+state+' '+country;
var geocoder = new google.maps.Geocoder();
geocoder.geocode( { 'address': address}, function(results, status) {
  var location = results[0].geometry.location;
  var lat = location.lat();
  var lng = location.lng();
  if(lat) {
  		$('#profile-latitude').val(lat);
	}
   if(lng){
	   $('#profile-longitude').val(lng);
   }
});
});
$('#profile-foodestablishment').change(function(){
	$('#foodcat-wrap').toggle();
});
$(document).ready(function(){
var checkedval = $('#profile-foodestablishment').is(":checked");
	if(checkedval==true){
		$('#foodcat-wrap').show();
	}
});
$('#profile-transactionrule').click(function(){
		if (this.checked) {
			$('#transactionrule-target').click();
			var workid = $('#profile-tempid').val();
			var partnerid = "<?php echo $model->id;?>";
			var url = $('#base_url').val();
			var ifrmaieid = $('#ifrmaieid').attr('src',url+'/partners/transaction?workid='+workid+'&partnerid='+partnerid);
		}
})
</script>
<?php $url = Url::home(true).'partners/general-fees'; ?>
<script>
$('tr td:first-child select').change(function(){
var id = $(this).attr('idsval');
	if($(this).val()==0 || $(this).val()==''){
		$('input[name="PartnerFees[general_'+id+']"]').prop('disabled',true).prop('checked',false);
		$('#partnerfees-amount_'+id).val('').prop('disabled',true);
		$('#partnerfees-paid_day_'+id).val(['']).prop('disabled',true);
		$('input[name="PartnerFees[charge_type_'+id+']"]').val(['']).prop('disabled',true);
	}else{
		$('input[name="PartnerFees[general_'+id+']"]').prop('disabled',false);
		$('#partnerfees-amount_'+id).prop('disabled',false);
		$('#partnerfees-paid_day_'+id).prop('disabled',false);
		$('input[name="PartnerFees[charge_type_'+id+']"]').prop('disabled',false);
	}
});
function fillme(id){
	if($('#partnerfees-general_'+id).is(':checked')==true) {
		$.ajax({
			'method':'post',
			'url':"<?php echo $url; ?>",
			'data':{id:id},
		})
		.done(function(result){
			if(result!=false){
				var data = $.parseJSON(result);
				$('#PartnerFees-trans_id_'+id).prop('checked',true);
				$('input[name="PartnerFees[charge_type_'+id+']"]').val([data.charge_type]);
				$('#partnerfees-amount_'+id).val(data.amount);
				$('#partnerfees-paid_day_'+id).val(data.paid_within);
				$('#partnerfees-trans_id_'+id).val(1);
				$('#partnerfees-general_'+id).val(1);
			}
		});
	}else{
		$('input[name="PartnerFees[charge_type_'+id+']"]').val(['']);
		$('#partnerfees-trans_id_'+id).val(['']);
		$('#partnerfees-amount_'+id).val('');
		$('#partnerfees-paid_day_'+id).val('');
		$('input[name="PartnerFees[general_'+id+']"]').removeAttr('checked');
		$('input[name="PartnerFees[general_'+id+']"]').val(['']);
	}
}
$('#profile-street,#profile-city,#profile-state,#profile-country,#profile-postcode').change(function(){
	 setTimeout(function(){
		 var address = $('#profile-street').val()+' '+$('#profile-city').val()+' '+$('#profile-state').val()+' '+$('#profile-country').val()+' '+$('#profile-postcode').val();
		 $('#profile-brandaddress').val(address);
	}, 3000);
});
</script>
<style>
	tr td:nth-child(2) div,tr td:nth-child(3) div,tr td:nth-child(4) div,tr td:nth-child(5) input {
  margin-top: 15px;
}
.box.box-success .information {  background-color: #006dd5;  border-radius: 9px;  color: #fff;
  font-size: 12px;  padding:1px;}
</style>
