<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Usermanagement;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Regions */
/* @var $form yii\widgets\ActiveForm */
$partenerId = Yii::$app->request->get('partnerid');
?>
<div class="box-body">
<div class="form-group">
	<?php $form = ActiveForm::begin(); ?>
	
		<?= $form->field($model, 'partner_id')->hiddenInput(['value' =>$partenerId])->label(false) ?>

		<?= $form->field($model, 'bank_name',['inputOptions' => ['class' => 'form-control','placeholder'=>$model->getAttributeLabel('bank_name')]])->textInput();?>		
						
		<?= $form->field($model, 'account_name',['inputOptions' => ['class' => 'form-control','placeholder'=>$model->getAttributeLabel('account_name')]])->textInput();?>	
							
		<?= $form->field($model, 'routing_number',['inputOptions' => ['class' => 'form-control','placeholder'=>$model->getAttributeLabel('routing_number')]])->textInput();?>	
							
		<?= $form->field($model, 'account_number',['inputOptions' => ['class' => 'form-control','placeholder'=>$model->getAttributeLabel('account_number')]])->textInput();?>		
						
		<?= $form->field($model, 'account_type',['inputOptions' => ['class' => 'form-control']])
		->widget(Select2::classname(),[
		"data"=>array("savings"=>"Savings","check"=>"Check"),
		'options'=> ["class"=>"form-control",'placeholder' =>$model->getAttributeLabel('account_type')]
		]); ?>			
					
		<?= $form->field($model, 'holder_type',['inputOptions' => ['class' => 'form-control']])
		->widget(Select2::classname(),[

		"data"=>array("personal"=>'Personal','business'=>"Business"),
		'options'=> ["class"=>"form-control",'placeholder' =>$model->getAttributeLabel('holder_type')]
		]); ?>		
		
		<?= $form->field($model, 'prefrence')->hiddenInput(['value' =>1])->label(false) ?>
								
		<p>
			Your bank account details are stored securely by our payment processor. Back account details are never transmitted to, or stored on, TutorsPlus servers.
		</p>
   
    
     <div class="box-footer">
		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? yii::t('app','Create') : yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			
		</div>
     </div>
</div>
<?php ActiveForm::end(); ?>
</div>
 <?php $siteurl = Yii::$app->getUrlManager()->getBaseUrl().'/employee/storelist'; ?>
<script type="text/javascript">
	$(document).ready(function(){
	var partnerId = $('#bankdetail-partner_id').val();		
		$.ajax({
			method	:'POST',
			url		: '<?php echo $siteurl; ?>',
			data	: { partnerId : partnerId }
			})
			.done(function(result){
				$('#bankdetail-store_id').empty();	
				data1 = JSON.parse(result);	
				var selected = "<?php echo $model->store_id ?>";
				 var div_data1="<option value=''>select store</option>";   
				 $('#bankdetail-store_id').append(div_data1);      	
				$.each(data1.data, function (key, data) {  
					var param = '';
					if(selected==data.id){
						param = 'selected="selected"';
					}
					var div_data="<option "+param+" value="+data.id+">"+data.storename+"</option>";  
				  $('#bankdetail-store_id').append(div_data); 
				});
				$('#bankdetail-store_id').trigger('change');
			});
	});
	
</script>

    
