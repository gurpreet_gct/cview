<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

 //echo"<pre>";
 //print_r($loyaltyData[0]);

$this->title = Yii::t('app','View : ').ucfirst($model->username);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Partners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$usertype = Yii::$app->user->identity->user_type;
$usertypeid = Yii::$app->user->identity->id;
?>
<?php if(Yii::$app->getSession()->hasFlash('success')):?>
			<div class="alert alert-success">
				<div> <?php echo Yii::$app->getSession()->getFlash('success'); ?></div>
			</div>

<?php endif; ?>
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <div class=" panel panel-primary">
                <div class="panel-heading">
                    <div class="pull-right">
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>

              <!-- general form elements -->

                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>

             <?= Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
         	<!--button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-bank">Bank account</button-->

			<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Transaction Rule</button>

     		 <?php if($usertype==1){ ?>

         	   <?= Html::a(Yii::t('app','Change Password'), ['changepassword', 'id' => $model->id], [
            'class' => 'btn btn-warning',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to change password?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this user?'),
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a(Yii::t('app','OK'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
         <?php }else { ?>
			  <?= Html::a(Yii::t('app','OK'), ['site/index'], ['class' => 'btn bg-green']) ?>
		<?php } ?>
           </p>

                <!-- form start -->
			<div class="box box-success"></div>
				<h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i> <?= Yii::t('app','Partner Details') ?>  </h3>
                </div><!-- /.box-header -->


                <!-- form start -->


 <div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'firstName',
            'lastName',
            'fullName',
            'username',
            'email:email',

            [
            'label' => 'User Type',
            'value' => $model->usertype->usertype,
             ],
            'phone',
            [
            'label' => 'Referral code',
			'attribute' => 'user_code',
             ],
           
        ],
    ]) ?>
    </div>
<?php 	if(isset($profile[0]) && isset($profile[0]->subscriptionFees)){	 ?>
	<div class="box box-success"></div>
	<h3 class="panel-title" style="padding-left:10px; padding-bottom:10px;">
	<i class="glyphicon glyphicon-usd"></i><?= Yii::t('app',' Subscription Fee Detail') ?>  </h3>
	
		<?= DetailView::widget([
		'model' => $profile[0],
		'attributes' => [
			[
			'attribute'=>'subscriptionFees',
			'value'=>'$'.$profile[0]->subscriptionFees,
			],
				[
				'attribute'=>'subscriptionDueDate',
				'value'=>$profile[0]->subscriptionDueDate.' ᵗʰ day of each month',
				],
				[
				'attribute'=>'	subscriptionPay',
				'value'=>$profile[0]->cardtype,
				]
			],
		]); 	
	} 
?>
    <div class="box box-success"></div>
				<h3 class="panel-title" style="padding-left:10px; padding-bottom:10px;">
                        <i class="glyphicon glyphicon-user"></i><?= Yii::t('app',' Address') ?>  </h3>
<?php if(isset($profile[0])){				 ?>
	 <div class="table-responsive">
      <?= DetailView::widget([
        'model' => $profile[0],
        'attributes' => [
            'companyName',
            'companyNumber',

            [
            'label' => Yii::t('app','Region'),
            'value' => $profile[0]->regionname->region,
             ],
            'houseNumber',
            'street',
            'city',
            'state',
            'country',
            'postcode',
				'officePhone',
				'officeFax',

				'alternativeContactperson',


        ],
]); 
?>
</div>
<?php } ?>

    <div class="box box-success"></div>
				<h3 class="panel-title" style="padding-left:10px; padding-bottom:10px;">
                        <i class="glyphicon glyphicon-user"></i> <?= Yii::t('app','Brand Details')  ?></h3>
      <?php
      //echo"<pre>";
     // print_r($loyaltyData);
     // die();
      if(isset($loyaltyData[0]['pointvalue'])){
		  $point_value =  number_format($loyaltyData[0]['pointvalue'],2);
		  }else{
			   $point_value ='';
		}
		if(isset($loyaltyData[0]['loyalty_pin'])){
		  $loyalty_pin =  $loyaltyData[0]['loyalty_pin'];
		  }else{
			   $loyalty_pin ='';
		}
     ?>
	 <?php if(isset($profile[0])){				 ?>
		  <div class="table-responsive">
    <?= DetailView::widget([
        'model' => $profile[0],
        'attributes' => [
            'brandName',
            [
			'attribute'=>'brandLogo',
			'value'=>   $profile[0]->imagename,
			'format' => ['image',['width'=>'132','height'=>'132']],
			 ],

			 [
			'attribute'=>'addLoyalty',
			'value'=>   $profile[0]->addLoyalty==1?'Yes':'No',
			 ],
			 [
			'attribute'=>'Loyalty Dollors',
			'value'=> $point_value,
			 ],
			 [
			'attribute'=>'Loyalty Pin',
			'value'=> $loyalty_pin,
			 ],
			 'minimumSpend',			  
			 [
			'attribute'=>'shopNow',
			'value'=>   $profile[0]->shopNow==1?'Yes':'No',
			 ],
			[
			'attribute'=>'foodEstablishment',
			'value'=>  $profile[0]->foodEstablishment==1?'Yes':'No',
			 ],
			 [
			'attribute'=>'geo_radius',
			'visible'=> $profile[0]->foodEstablishment==1?1:0,
			'value'=>  $profile[0]->geo_radius.' KM.',
			 ],
			[
            'label' => Yii::t('app','Food Categories'),
            'value' => !empty($foodCategoryTree)?implode(",",array_filter($foodCategoryTree)):'N/A',
            // 'value' => implode(",",$foodCategoryTree),
			],
            'brandAddress',
             [
			'attribute'=>'Store/Brand Description',
			'value'=>  strip_tags(html_entity_decode($profile[0]->brandDescription)),
			 ],

            [
            'label' => Yii::t('app','Categories'),
            'value' => implode(",",$categoryTree),
             ],
            'socialmediaFacebook',
            'socialmediaTwitter',
            'socialmediaGoogle',
            'socialmediaPinterest',
            'socialmediaInstagram',
             [
			'attribute'=>'Background Image',
			'value'=>   $profile[0]->backimage,
			'format' => ['image',['width'=>'320','height'=>'180']],
			 ],

            'foregroundHexColour',

        ],
	 ]); 
	 ?>
	 </div>
	 <?php } ?>



</div><!-- /.box -->
 </div>
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->

<!-- Transactions Modal -->
<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
					<h4 class="modal-title" id="myLargeModalLabel">Transactions Rule</h4>
				</div>
				<div class="modal-body">
				<?php
					$iframeUrl = Yii::$app->getUrlManager()->getBaseUrl().'/partners/transaction?partnerid='.$model->id;
				?>
				<iframe  id="ifrmaieid" width="100%" height="650"  src="<?= $iframeUrl; ?>">
				</iframe>

			 </div>
			 <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	var background =  $('table:last-child tbody tr:nth-last-child(2)>td').text();

	if(background == '#000000') {
	$('table:last-child tbody tr:nth-last-child(2)>td').css('background',background);
	$('table:last-child tbody tr:nth-last-child(2)>td').css('color','#ffffff');
	}else{
		$('table:last-child tbody tr:nth-last-child(2)>td').css('background',background);
		$('table:last-child tbody tr:nth-last-child(2)>td').css('color','#000000');
	}


	var fontColour =  $('table:last-child tbody tr:nth-last-child(1)>td').text();

	if(fontColour == '#ffffff' || fontColour == '#FFFFFF') {
	$('table:last-child tbody tr:nth-last-child(1)>td').css('background',fontColour);
	$('table:last-child tbody tr:nth-last-child(1)>td').css('color','#000000');
	}else{
		$('table:last-child tbody tr:nth-last-child(1)>td').css('background',fontColour);
		$('table:last-child tbody tr:nth-last-child(1)>td').css('color','#ffffff');
	}

});
</script>
