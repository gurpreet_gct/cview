<?php

use yii\helpers\Html;
use yii\helpers\Arrayhelper;
use kartik\grid\GridView;
use common\models\Usertype;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Partners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">
<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
              'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
          
           
            'fullName',
            'username',
            'email:email',
            'orgName',
 			
            
            [
            'attribute'=>'user_type',
            'value'=>'usertype.usertype',
            'filter' => Html::activeDropDownList($searchModel, 'user_type', ArrayHelper::map(Usertype::find()->asArray()->where(['id'=>7])->all(), 'id', 'usertype'),['class'=>'form-control','prompt' => Yii::t('app','Select')]),
            ],
            
            
          
            [
            'attribute'=>'ssdRegion',
            'label'=>'Region',
            'value'=>'ssdRegionval',
            ],
            
            [
            'attribute'=>'status',
            'label'=>'Status',
			'format' => 'raw',
			'value'=>function ($model, $key, $index, $widget) {    
            return \yii\helpers\Html::dropDownList("status", $model->status, [10=>Yii::t('app','Enable'), 0=>Yii::t('app','Disable')],['prompt'=>Yii::t('app','Select'),'id'=>'updatestatus'.$model->id,'onchange'=>'setstatus('.$model->id.')']);       
			},
			
			],
            
           
           
           /*  [
        		'attribute' => 'image',
       		'format' => 'html',    
     			'value' => function ($dataProvider) {
            return Html::img(Yii::getAlias('@web').'/'. $dataProvider['image'],
                ['width' => '70px']);
       		 },
   			 ], */
            
           /* ['attribute'=>'lastLogin',
            'format'=>'date',
            'xlFormat'=>"mmm\\-dd\\, \\-yyyy",
            
            
            ], */
           ['class'=>'kartik\grid\ActionColumn'],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>Yii::t('app','Add Partner'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-user"></i>'.Yii::t('app','Partners'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
</div>

<?php $baseurl = Yii::$app->getUrlManager()->getBaseUrl(); 
      $staturl =$baseurl.'/partners/setstatus';
  
?>
<script type="text/javascript">

function setstatus(id){
	
	var values = $('#updatestatus'+id).val();
 
	var updateconfirm = confirm ("Are you sure to update status?");
		
	if (updateconfirm) {		
	$.ajax({
	method: "POST",
    url: "<?php echo $staturl; ?>",
	data: { id: id, value: values }
	
	
	})
	.done(function( result ) {
		
	  //$( "#"+id ).fadeOut( "slow");
	  });

}
 
	
}

</script>
