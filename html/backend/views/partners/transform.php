<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
	$usertype = Yii::$app->user->identity->user_type;
	$readonly =true;
	if($usertype==1){
		$readonly =false; 
	}
?>
<?php $form = ActiveForm::begin(); ?>
<div class="box-body">
      <div class="form-group existing-rule">		
		    <?php if(\Yii::$app->session->hasFlash('success')) {  ?>
	  	<div class="alert alert-success">	<?= Yii::$app->session->getFlash('success'); ?></div>
	  	<?php } else if(\Yii::$app->session->hasFlash('error')) { ?>
			<div class="alert alert-error">	<?= Yii::$app->session->getFlash('error'); ?></div>
	  	<?php } ?>
	<?php   	if($usertype==1){  ?>
				<table class="table table-bordered">
						<tr class="info"><th>Choose</th><th>Amount deduct Type</th><th>Amount</th><th>Paid Within</th><th>Use general Rule</th></tr>
						<?php $i=1; foreach($transList as $_val) {  ?>							
							<tr>											
								<td>
								<label>								
								<?php $trans = 'trans_id_'.$_val['id'];?>	
								<?= $form->field($model,$trans)->dropDownList(array('0'=>'No',1=>'Yes'), ['prompt'=>'Select...','options' => [$model->$trans => ['selected'=>true]],'idsval'=>$i])->label($_val['title']); ?>
									</label>
									
									
								</td>
								<td>
									<?= $form->field($model,'charge_type_'.$_val['id'])->radioList(array('1'=>'Fixed',2=>'%'))->label(false); ?>
								</td>	
								<td>
								<?= $form->field($model,'amount_'.$_val['id'])->textInput(['size'=>5])->label(false); ?>
								</td>	
								<td>
								<?php
							    	$filedname = 'paid_day_'.$_val['id'];
									foreach (range(1, 28) as $number) {
										$data[$number] = $number.'ᵗʰ day'; 
									}
									echo $form->field($model,$filedname)->dropDownList($data, ['prompt'=>'Select...','options' => [$model->$filedname => ['selected'=>true]]])->label(false);
								?>
								</td>
								<td>	
								<?php $general = 'general_'.$_val['id'];?>		
								<input id="partnerfees-general_<?= $_val['id'] ?>" value="<?= $model->$general ?>" type="checkbox"  onclick="fillme(<?= $_val['id']; ?>);" <?php if($model->$general==1) { echo 'checked="checked"'; } ?>  name="PartnerFees[general_<?= $_val['id']; ?>]">
								</td>	
													
							</tr>
						<?php $i++;  } ?>						
					</table>		
		<?php } else {  ?>
			
			<table class="table table-bordered">
				<tr class="info">
						<th>Transaction title</th>
						<th>Amount deduct Type</th>
						<th>Amount</th>
						<th>Paid Within</th>
				</tr>
				<?php $i=1; foreach($transList as $_val) {
						$filedamt = 'amount_'.$_val['id'];
						if($model->$filedamt!=''){
					?>							
					<tr>								
						<td>				
							<?= $_val['title']; ?>						
							
						</td>
						<td>
						<?php 	
							$filed = 'charge_type_'.$_val['id'];
							echo $model->$filed ==1 ? 'Fixed' : 'Percentage';
						
						 ?>
						
						</td>	
						<td>
						<?php
							$filed = 'amount_'.$_val['id'];
							echo $model->$filed; 
						?>
						</td>	
						<td>
						<?php
							$filed = 'paid_day_'.$_val['id'];
							echo $model->$filed.'ᵗʰ days'; ;
						 ?>
						</td>										
					</tr>
				<?php  } $i++;  } ?>						
			</table>
			
		<?php }   ?>
	</div><!-- /.box -->
	<div class="form-group">
		<?php if($readonly==false){
				echo Html::submitButton('Submit', ['class' =>'btn btn-primary']);
			}
		 ?>
			
	</div>
</div><!-- /.box -->
<?php ActiveForm::end(); ?>
<?php $url = Url::home(true).'partners/general-fees'; ?>
<script>
$('tr td:first-child select').change(function(){
var id = $(this).attr('idsval');
	if($(this).val()==0 || $(this).val()==''){
		$('input[name="PartnerFees[general_'+id+']"]').prop('disabled',true).prop('checked',false);		
		$('#partnerfees-amount_'+id).val('').prop('disabled',true);
		$('#partnerfees-paid_day_'+id).val(['']).prop('disabled',true);
		$('input[name="PartnerFees[charge_type_'+id+']"]').val(['']).prop('disabled',true);
	}else{
		$('input[name="PartnerFees[general_'+id+']"]').prop('disabled',false);		
		$('#partnerfees-amount_'+id).prop('disabled',false);
		$('#partnerfees-paid_day_'+id).prop('disabled',false);
		$('input[name="PartnerFees[charge_type_'+id+']"]').prop('disabled',false);
	}
});
/*
function validateme(id){	
	var ischeck = $('#PartnerFees-trans_id_'+id).val();
	
	if(ischeck==''){
		var a = $('#PartnerFees-trans_id_'+id).attr('idval');
		$('#PartnerFees-trans_id_'+id).val(a);
	}
	else{
		$('input[name="PartnerFees[charge_type_'+id+']"]').prop('checked',false);
		$('input[name="PartnerFees[general_'+id+']"]').prop('checked',false);
		$('#PartnerFees-trans_id_'+id).val('');
		$('#partnerfees-amount_'+id).val('');
		$('#partnerfees-paid_day_'+id).val('');
	}
} */
function fillme(id){

	if($('#partnerfees-general_'+id).is(':checked')==true) {	
		$.ajax({
			'method':'post',
			'url':"<?php echo $url; ?>",
			'data':{id:id},
		})
		.done(function(result){
			if(result!=false){
				var data = $.parseJSON(result);
				$('#PartnerFees-trans_id_'+id).prop('checked',true);				
				$('input[name="PartnerFees[charge_type_'+id+']"]').val([data.charge_type]);	
				$('#partnerfees-amount_'+id).val(data.amount);
				$('#partnerfees-paid_day_'+id).val(data.paid_within);
				$('#partnerfees-trans_id_'+id).val(1);
				$('#partnerfees-general_'+id).val(1);
			}
		});
	}else{
		$('input[name="PartnerFees[charge_type_'+id+']"]').val(['']);	
		$('#partnerfees-trans_id_'+id).val(['']);
		$('#partnerfees-amount_'+id).val('');
		$('#partnerfees-paid_day_'+id).val('');	
		$('input[name="PartnerFees[general_'+id+']"]').removeAttr('checked');
		$('input[name="PartnerFees[general_'+id+']"]').val(['']);
		
	}
	
}


</script>
<style>
	form {  height: auto;  width: auto;}	
	tr td:nth-child(2) div,tr td:nth-child(3) div,tr td:nth-child(4) div,tr td:nth-child(5) input {
  margin-top: 15px;
}
</style>

