<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Taxclass;
use common\models\Tax;
use common\models\TaxruleItem;
use common\models\Country;
use common\models\States;

/* @var $this yii\web\View */
/* @var $model app\models\Taxs */
/* @var $form yii\widgets\ActiveForm */

/* update time ( check if product class )*/
if(isset($model->productclass[1])) {
	
	 $productclass = explode(', ',$model->productclass);	 
		foreach($productclass as $key=>$value) {			
		    $productClass[$value] = array('Selected' => 'selected');
		}			  
		
	}else{
		$productClass = array();
}
 
 /* get tax classes */
$Taxclass =  ArrayHelper::map(Taxclass::find()->all(), 'taxClass', 'taxClass');
 
 /* get tax with country wise */
$CountryName =  ArrayHelper::map(Country::find()->all(), 'id_countries', 'name');
if(isset($model->country_id)){
		$taxrate =  ArrayHelper::map(Tax::find()->where(['country_id' =>$model->country_id])->all(), 'id', 'id');
	}else {
		$taxrate = array();
 }	
 
 /* get taxRate selected value form Taxrule Item Model */ 
 if(isset($model->id)){
	 $taxrulItemModel =  TaxruleItem::find()->select('tax_id')->where(['rule_id' => $model->id])->all();
	 foreach($taxrulItemModel as $key=>$value) {	
			
		    $selectedTaxrate[$value->tax_id] = array('Selected' => Yii::t('app','selected'));
		}	
	
 }else{
	$selectedTaxrate = array();
 }
?>



    <?php $form = ActiveForm::begin(); ?>
	 <div class="box-body">
      <div class="form-group">
	 <?php //echo $form->errorSummary($model); ?>
	 
	    <?= $form->field($model, 'name')->textInput()->label(Yii::t('app','Name (like: TaxruleClass-Taxrate)') ) ?>
			
		<?= $form->field($model, 'productclass')->dropDownList($Taxclass,['options' => $productClass,  'multiple'=>true]) ?>
		
		<?=	 $form->field($model, 'country_id')->widget(Select2::classname(), [
				'data' => $CountryName,
				'options' => ['placeholder' => Yii::t('app','select country ...'),'multiple' => false],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>
		
		<?= $form->field($model, 'taxrate')->dropDownList($taxrate,['options' => $selectedTaxrate,  'multiple'=>true]) ?>			
		
	   
	    <?= $form->field($model, 'priority_id')->textInput()->label() ?>
	    <?= $form->field($model, 'sort_id')->textInput()->label() ?>
	   
	  
		
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Taxs')]) ?>
    </div>
    </div>
     

    <?php ActiveForm::end(); ?>


<?php $siteurl = Yii::$app->getUrlManager()->getBaseUrl(); 
$countrywiseTaxRate =$siteurl.'/taxrule/countrywisetaxtate'; ?>   
    
<script type="text/javascript">
	
	$("#taxrule-country_id").on("change", function () {	  
		$('#select2-taxrule-taxrate-container').empty();		
		var values = this.value;
		$.ajax({
			method: "POST",
			url: "<?php echo $countrywiseTaxRate; ?>",
			data: { id: values }	
	
	})
	.done(function(result) {	
		data1 = JSON.parse(result);		
		$('#taxrule-taxrate').empty();	
		      	
		$.each(data1, function (key, data) {   
			$.each(data, function (index, data) {					 
				 var div_data="<option value="+index+">"+data+"</option>"; 					
					$(div_data).appendTo('#taxrule-taxrate'); 
				
			})
		});
		
	  });
		
	});
	
	/* Check post code range */
	$('#tax-postcoderange').click(function(){
		if (this.checked) {
		  $('#postcodeTohide').show();
		}else{
		  $('#postcodeTohide').hide();
		}
	});
	
	/* check if checked */
	var postcode = $('#tax-postcoderange').is(':checked');                         
					if(postcode ==1) {                           
						  $('#postcodeTohide').show();
					 }



</script>

