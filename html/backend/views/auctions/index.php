<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\CommonModel;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CardtypeSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auction List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cardtype-index">
<?php //echo $form->field($model, 'auction_currency_id')->dropDownList(CommonModel::getCurrency(),['class'=>'form-control', 'prompt'=>'Choose Option']); ?>
<?php
    echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
        return ['id' => $model['id']];
        },
        'columns' => [
            'id',
            'listing_id',
            /*[
                'attribute' => 'auction_type',
                'value'=> 'auction_type',
               // 'format' => 'raw', 
                'filter' => CommonModel::getAuctionType(),
            ],*/            
            [
                'attribute'=>'auction_bidding_status',
                'value'=>'auction_bidding_status',
                'format' => 'raw', 
                'filter' => CommonModel::getBiddingStatus(),
            ],            
            'starting_price',
           // 'auction_bidding_status',
           // ['attribute'=>'owner_id', 'filter'=>CommonModel::getOwnerList($userModel,['status'=>1, 'role'=>[11,12]],['_id', 'name', 'firstname', 'lastname'])],            
//            ['attribute' => 'auction_currency_id', 'value' => CommonModel::getCurrencyData(6), 'format' => 'raw', 'filter' => CommonModel::getCurrency()],
            //['attribute' => 'auction_currency_id', 'value' => CommonModel::getCurrencyData(6), 'format' => 'raw', 'filter' => CommonModel::getCurrency()],
            //'name',
            /*[
                'attribute'=>'listing_id',
                'value'=>'listing_id',
                'format' => 'raw', 
                'filter' => array('1' => Yii::t('app','Enable'), '0' => Yii::t('app','Disable') ),
            ],*/
            // 'updated_at',
            ['class'=>'kartik\grid\ActionColumn'],
        ],
        'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class'=>'kartik-sheet-style'],
        'filterRowOptions' => ['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
        //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>Yii::t('app','Add Auction'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
        'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
        'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-indent"></i> '.Yii::t('app','Auctions List'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);?>
</div>