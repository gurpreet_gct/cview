<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Property;
use common\models\CommonModel;
//use kartik\daterange\DateRangePicker;
use kartik\datetime\DateTimePicker;

/*use yii\bootstrap\Modal;
//use kartik\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;*/
/* @var $this yii\web\View */
/* @var $model common\models\cardtype */
/* @var $form yii\widgets\ActiveForm */
?>
    <?php
    $form = ActiveForm::begin([
        'options' => [
            'id' => 'create-cardtype-form',
            'enctype' =>'multipart/form-data',
        ]
    ]);
    ?>
    <?php // echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?php // echo $form->field($model, 'desc')->textArea(['maxlength' => 500, 'rows' => 6, 'cols' => 50]) ?>
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <?php echo $form->field($model, 'listing_id')->dropDownList(Property::getActiveProperties(),['class'=>'form-control', 'prompt'=>'Choose Option']); ?>
            </div>
            <div class="col-md-6">
                <?php //echo $form->field($model, 'auction_type')->dropDownList(CommonModel::getAuctionType(),['class'=>'form-control', 'prompt'=>'Choose Option']); ?>
                <?php echo $form->field($model, 'estate_agent_id')->dropDownList(CommonModel::getActiveAgents(),['class'=>'form-control', 'prompt'=>'Choose Option']); ?>                
            </div>
        </div>        
        <div class="row">
            <div class="col-md-6">
                <?php echo $form->field($model, 'auction_currency_id')->dropDownList(CommonModel::getCurrency(),['class'=>'form-control', 'prompt'=>'Choose Option']); ?>
            </div>
            <div class="col-md-6">
                <?php echo $form->field($model, 'starting_price'); ?>
            </div>
        </div>        
        <div class="row">
            <div class="col-md-6">
                <?php //echo $form->field($model, 'datetime_start'); ?>
                <?php 
                    echo $form->field($model, 'datetime_start')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Enter Start Date & Time'],
                    'readonly'  => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]);?>                
            </div>
            <div class="col-md-6">
                <?php 
                    echo $form->field($model, 'datetime_expire')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Enter Start Date & Time'],
                    'readonly'  => true,
                    'pluginOptions' => [
                        'autoclose' => true,
                       // 'startDate'=> date('d-m-Y')
                    ]
                ]);?>                 
                <?php //echo $form->field($model, 'datetime_expire'); ?>
            </div>
        </div>        
        <div class="row">
            <div class="col-md-6">
                <?php echo $form->field($model, 'duration')->textInput(['maxlength' => 2, 'class' => 'numericInput form-control' ])->label('Auction duration (in Minutes) Numeric'); ?>
            </div>
            <div class="col-md-6">
                <?php echo $form->field($model, 'auction_bidding_status')->dropDownList(CommonModel::getBiddingStatus(),['class'=>'form-control', 'prompt'=>'Choose Option']); ?>                
                <?php //echo $form->field($model, 'auction_bidding_status')->dropDownList(['1' => 'No Online Bidding', '2' => 'Online Bidding', '3' => 'Offsite Bidding'],['prompt'=>'Choose Option']); ?>
            </div>
        </div>        
        <div class="box-footer">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Users')]) ?>
            </div>
        </div>
    </div>

    <!-- /.box-body -->
<?php ActiveForm::end(); ?>