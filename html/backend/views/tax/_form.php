<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Country;
use common\models\States;

/* @var $this yii\web\View */
/* @var $model app\models\Taxs */
/* @var $form yii\widgets\ActiveForm */
 $CountryName =  ArrayHelper::map(Country::find()->all(), 'id_countries', 'name');
 if(isset($model->country_id)){
 $stateName =  ArrayHelper::map(States::find()->where(['country_id' =>$model->country_id])->all(), 'id', 'name');
 }else {
 $stateName = array();
 }	
?>



    <?php $form = ActiveForm::begin(); ?>
	 <div class="box-body">
      <div class="form-group">
	 <?php //echo $form->errorSummary($model); ?>
	 
	    <?= $form->field($model, 'id')->textInput()->label(Yii::t('app','Tax Identifier (like : country-state-postcode)')) ?>
		<?=  
		
		 $form->field($model, 'country_id')->widget(Select2::classname(), [
		'data' => $CountryName,
		'language' => 'en',
		'options' => ['placeholder' => Yii::t('app','Select a country ...')],
		'pluginOptions' => [
			'allowClear' => true
		],
	]);
	?>
	<?=  
		
		 $form->field($model, 'state_id')->widget(Select2::classname(), [
		'data' => $stateName,
		'language' => 'en',
		'options' => ['placeholder' => Yii::t('app','Select a state ...')],
		'pluginOptions' => [
			'allowClear' => true
		],
	]);
	?>
	
	
    
	<?= $form->field($model, 'postcoderange')->checkbox(); ?>    
    <?= $form->field($model, 'postcode')->textInput(['maxlength' => true]) ?>
   <div id="postcodeTohide" style="display:none">  <?= $form->field($model, 'postcodeTo')->textInput(['maxlength' => true]) ?> </div>
    <?= $form->field($model, 'rate')->textInput(['type'=>'number', 'min'=>'1','step'=>'0.01']); ?>
   
    
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Taxs')]) ?>
    </div>
    </div>
     

    <?php ActiveForm::end(); ?>

<?php $siteurl = Yii::$app->getUrlManager()->getBaseUrl(); 
$countrywisestate =$siteurl.'/tax/countrywisestate'; ?>   
    
<script type="text/javascript">
	
	$("#tax-country_id").on("change", function () {	  
		$('#select2-tax-state_id-container').empty();		
		var values = this.value;
		$.ajax({
			method: "POST",
			url: "<?php echo $countrywisestate; ?>",
			data: { id: values }	
	
	})
	.done(function(result) {	
		data1 = JSON.parse(result);		
		$('#tax-state_id').empty();	
		 var div_data1="<option value=''><?= Yii::t('app','select') ?></option>";         	
		$.each(data1, function (key, data) {   
			$.each(data, function (index, data) {					 
				 var div_data="<option value="+index+">"+data+"</option>"; 
					$(div_data1).appendTo('#tax-state_id'); 
					$(div_data).appendTo('#tax-state_id'); 
				
			})
		});
		
	  });
		
	});
	
	/* Check post code range */
	$('#tax-postcoderange').click(function(){
		if (this.checked) {
		  $('#postcodeTohide').show();
		}else{
		  $('#postcodeTohide').hide();
		}
	});
	
	/* check if checked */
	var postcode = $('#tax-postcoderange').is(':checked');                         
					if(postcode ==1) {                           
						  $('#postcodeTohide').show();
					 }



</script>

