<?php

use yii\helpers\Html;
#use yii\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Country;
use common\models\States;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaxsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$CountryName =  ArrayHelper::map(Country::find()->all(), 'id_countries', 'name');
if(isset($_GET['TaxSearch']['country_id'])){
$stateName =  ArrayHelper::map(States::find()->where(['country_id' =>$_GET['TaxSearch']['country_id']])->all(), 'id', 'name');
}else{
$stateName = array();
}
$this->title = 'Taxs';
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="Taxs-index">


<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
            'id',
            [
            'attribute'=>'country_id',           			
			'value'=> 'country.name',                   
                  'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $CountryName,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => Yii::t('app','Select country')],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

            ],
            ],
             
            [
            'attribute'=>'state_id',           			
			'value'=> 'states.name',                   
                  'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $stateName,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => Yii::t('app','Select state')],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

            ],
            ],
        
                
            
            'postcode',
            'rate',
             
            
           [
            'attribute'=>'created_at',
          	'value' => function ($dataProvider) {
            return date("D, d M Y",$dataProvider["created_at"]);
               
       		 },
            ],
            
           ['class'=>'kartik\grid\ActionColumn'],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>'Add Tax','data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-calculator"></i>'.Yii::t('app','Taxs'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
</div>

<?php $siteurl = Yii::$app->getUrlManager()->getBaseUrl(); 
$countrywisestate =$siteurl.'/tax/countrywisestate'; ?>   
    
<script type="text/javascript"> 
	
	$("#taxsearch-country_id").on("change", function () {	 
		 
		$('#select2-taxsearch-state_id-container').empty();		
		var values = this.value;
		
		$.ajax({
			method: "POST",
			url: "<?php echo $countrywisestate; ?>",
			data: { id: values }	
	
	})
	.done(function(result) {	
		data1 = JSON.parse(result);		
		$('#taxsearch-state_id').empty();		     	
		$.each(data1, function (key, data) {   
			$.each(data, function (index, data) {							 
				    var div_data="<option value="+index+">"+data+"</option>"; 				
					$(div_data).appendTo('#taxsearch-state_id'); 
				
			})
		});
		
	  });
		
	});

</script>
