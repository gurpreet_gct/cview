    <?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    ?>
    <div class="login-box">
		


          <div class="login-logo">
            <a href="<?= Yii::$app->homeUrl;?>"> <?= Html::img('@web/images/logo.png',array('alt'=>Yii::$app->name,'title'=>Yii::$app->name));?></b></a>
          </div><!-- /.login-logo -->
          <div class="login-box-body">
            <p class="login-box-msg"><?= Yii::t('app','Reset PIN') ?></p>
            		<!-- flash message start  -->	
<?php if(Yii::$app->getSession()->hasFlash(Yii::t('app','success'))):?>
     <div class="alert alert-success">
       <div> <?php echo Yii::$app->getSession()->getFlash(Yii::t('app','success')); ?></div>
    </div>
   
<?php endif; ?>

<?php if(Yii::$app->getSession()->hasFlash(Yii::t('app','error'))):?>
     <div class="alert alert-error">
       <div> <?php echo Yii::$app->getSession()->getFlash(Yii::t('app','error')); ?></div>
    </div>
   
<?php endif; ?>

<!-- flash message end  -->	
            <?php $form = ActiveForm::begin(['id' => 'forgot-form']); ?>
              <div class="form-group has-feedback">
                <?= $form->field($model,                 
                'password_hash')->passwordInput(['class'=>'form-control', 'value' => '', 'placeholder' => Yii::t('app','New PIN')])->label('New PIN');?>
                
             
                
                
                 
              </div>

                <?php if (isset($app_id)) { ?>
                    <input type="hidden" name="app_id" value="<?php echo $app_id; ?>">
                <?php } ?>
             
                <div class="row">
                 
                  <div class="col-xs-4">
                    <?= Html::submitButton(Yii::t('app','Submit'), ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                  </div><!-- /.col -->
                </div>
            </form>

   <?php /*   <div class="social-auth-links text-center">
              <p>- OR -</p>
              <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
              <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
            </div><!-- /.social-auth-links -->
           */ ?>

				

          </div><!-- /.login-box-body -->
          <?php /* ActiveForm::end(); */ ?>
              <?php if (Yii::$app->get("authClientCollection", false)): ?>
                  <div class="col-lg-offset-2 hidden">
                      <?= yii\authclient\widgets\AuthChoice::widget([
                          'baseAuthUrl' => ['/user/auth/login']
                      ]) ?>
                  </div>
              <?php /* endif; */ ?>
        </div><!-- /.login-box --> 
