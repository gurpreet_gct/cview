<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SubscriptionCharges */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Subscription Charges',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Subscription Charges'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="subscription-charges-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
