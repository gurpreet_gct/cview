<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SubscriptionCharges */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Subscription Charges'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if(\Yii::$app->session->hasFlash('success')) {  ?>
	  	<div class="alert alert-success">	<?= Yii::$app->session->getFlash('success'); ?></div>
	  	<?php } ?>
    <div class="row">
            <!-- left column -->

             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">
                    <div class="pull-right">
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="fa fa-credit-card"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->

                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>
                <?= Html::a(Yii::t('app','OK'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
                <?php if($model->payment_status==0){ ?>
               
                <?= Html::a(Yii::t('app','Pay now'), ['paynow', 'id' => $model->id], ['class' => 'btn bg-primary']) ?>               
               
                <?php } ?>
                
           </p>

                </div><!-- /.box-header -->
                <!-- form start -->


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          [
            'attribute'=>'partner_id',
            'value'=>$model->partnername,
          ],
          [
            'attribute'=>'amount',
            'value'=>'$'.$model->amount,
          ],
          [
            'attribute'=>'payment_status',
            'value'=>$model->payment_status==1 ? 'Paid' : 'Pending',
          ],
          [
            'attribute'=>'account_id',
            'value'=>$model->accountname,
          ],
		   'transaction_id',
		   [
            'attribute'=>'due_date',
            'value'=>date('Y-m-d',$model->due_date),
          ],
          [
            'attribute'=>'created_at',
            'value'=>date('Y-m-d h:i A',$model->created_at),
          ],
          [
            'attribute'=>'updated_at',
            'value'=>date('Y-m-d h:i A',$model->updated_at),
          ],
        ],
    ]) ?>

</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->