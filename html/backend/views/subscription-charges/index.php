<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Usermanagement;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\SubscriptionChargesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Subscription Charges');
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$visiable = 0;

if($userType==1){
		$advertiser = ArrayHelper::map(Usermanagement::find() ->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
		$visiable = 1;
		$actionBtn = ' {view}';
}else{
		$advertiser='';
		$actionBtn = '{pay_now} {view}';
	}
?>
<div class="transactions-fees-index">
	<?php
     echo GridView::widget([
       'dataProvider'=>$dataProvider,
       'filterModel'=>$searchModel,
               'rowOptions' => function ($model, $key, $index, $grid) {
     return ['id' => $model['id']];
   },
       'columns'=>  [
         [
         'attribute'=>'partner_id',
         'visible'=> $visiable,
         'value' => 'partnername',
         'filterType'=>GridView::FILTER_SELECT2,
         'filterWidgetOptions' => [
             'data' => $advertiser,
             'size' => Select2::SMALL,
             'options' => ['placeholder' => 'Select Partner..'],
             'pluginOptions' => [
               'width'=>'150px',
               'allowClear' => true,
             ],
           ],
         ],
          ['attribute'=>'amount',
          'value' => function($model){
			  return "$".$model->amount;}
          ],
          //'amount',
			  [
			   'attribute'=>'payment_status',
			   'filter'=>array(0=>'Pending',1=>'Paid'),
			   'value' => function ($dataProvider) {
					return  $dataProvider["payment_status"] ==0 ? 'Pending' : 'Paid';

			  },
		  ],
         [
					'attribute'=>'due_date',
					'value'=> function ($dataProvider) {
					return  date('Y-m-d',$dataProvider->due_date);

			  },
					'filterType'=>GridView::FILTER_DATE_RANGE,
					'filterWidgetOptions' => [
					'convertFormat'=>true,
					'pluginOptions'=>[
					'locale'=>[
					'format'=>'Y-m-d',
					'separator'=>' - ',
					],
					'opens'=>'left'
					],

					],
				],
			['class'=>'kartik\grid\ActionColumn','template'=>$actionBtn,
			'buttons' => [
				'pay_now' => function ($url, $model, $key) {
						if($model->payment_status==1){
							return '';
						}
						return Html::a('<i class="fa fa-usd" aria-hidden="true"></i>', ['paynow', 'id'=>$model->id],['title'=>'Pay Now']);
					},
				]
			],
		],
       'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
       'headerRowOptions'=>['class'=>'kartik-sheet-style'],
       'filterRowOptions'=>['class'=>'kartik-sheet-style'],
       'pjax'=>true, // pjax is set to always true for this demo
     //   'export'=>false,
       'beforeHeader'=>[
           [
               'options'=>['class'=>'skip-export'] // remove this row from export
           ]
       ],
       // set your toolbar
       'toolbar'=> [
           ['content'=>
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
           ],
           '{export}',
           '{toggleData}',
       ],
       // set export properties
      'export'=>[
           'fontAwesome'=>true
       ],
       // parameters from the demo form
       'bordered'=>true,
      'striped'=>false,
       'condensed'=>true,
       'responsive'=>true,
       'hover'=>true,
       'showPageSummary'=>true,
       'responsiveWrap' => false,
       'panel'=>[
           'type'=>GridView::TYPE_PRIMARY,
           'heading'=>'<i class="glyphicon glyphicon-list"></i>'. Yii::t('app',' Subscription Charges '),
       ],
       'persistResize'=>false,
       'exportConfig'=>['csv'=>'csv'],
   ]);


?>
</div>
