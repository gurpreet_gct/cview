<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\SubscriptionFees */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Pay Subscription Fees');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pay Subscription Fees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class=" panel panel-primary">
                <div class="panel-heading">
                    <div class="pull-right">
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-plus"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                <?php if($acc==false): ?>
                    <div class="box-body" style="min-height:350px;">
                      <h3><p class="bg-danger text-center" style="padding:10px;">You haven't added any payment method, Please add it to make payment.</p></h3>
                        <div class="color-swatches">
                          <div class="color-swatch brand-primary">
                                <a href="<?= Yii::$app->urlManager->createUrl(['bankdetail/create']); ?>?payid=<?= $_GET['id'];?>">
                                <h3>Add Bank Account</h3>
                              </a>
                          </div>
                          <div class="color-swatch brand-success">
                              <a href="<?= Yii::$app->urlManager->createUrl(['paymentcard/create']); ?>?payid=<?= $_GET['id'];?>"><h3>Add Credit Card</h3></a>
                          </div>
                        </div>
                    </div>
                <?php else: ?>
                <div class="box-body" style="min-height:350px;">
                   <?php $form = ActiveForm::begin(); ?>
                <?php if(\Yii::$app->session->hasFlash('error')) { ?>
                <div class="alert alert-error">	<?= Yii::$app->session->getFlash('error'); ?></div>
                <?php } ?>
                  <br>
                      <?= $form->field($model, 'amount')->textInput(['readOnly' => true]) ?>
                      <?= $form->field($model, 'account')->dropDownList($acc,['prompt'=>'Select...'])->label('Please select account') ?>
                      <?= $form->field($model, 'acc_type')->hiddenInput()->label(false); ?>
				  <?php if($acc!=false): ?>
                    <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Pay Now') , ['class' =>'btn btn-primary']) ?>
                    </div>
                    <?php endif; ?>
                      <?php ActiveForm::end(); ?>
                  </div>
                <?php endif; ?>
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
<script>
	$('#dynamicmodel-account').change(function(){
		var selectedVal = $('#dynamicmodel-account option:selected').text();
		if(selectedVal==''){ return false;}
		if(selectedVal.match('Bank')){
			$('#dynamicmodel-acc_type').val(1);
		}else {
			$('#dynamicmodel-acc_type').val(2);
		}
		
	});
	$('#dynamicmodel-account').trigger('change');
</script>
<style media="screen">
.color-swatches .brand-primary {  background-color: #337ab7; }
.color-swatch {  height: 100px;  width: 100px;0px }
.color-swatches .brand-success {
  background-color: #5cb85c;
}
.color-swatch {  border-radius: 3px;      float: left;      height: 81px;
      margin: 65px 5px;      width:48%;
}
.color-swatch h3{color: #fff; margin-left: 19%; margin-top: 8%;}
</style>
