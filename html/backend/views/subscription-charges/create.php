<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SubscriptionCharges */

$this->title = Yii::t('app', 'Create Subscription Charges');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Subscription Charges'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-charges-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
