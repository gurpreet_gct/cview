<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Store */

$this->title = Yii::t('app','Update Store:') . ' ' . ucfirst($model->storename);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Stores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => ucfirst($model->storename), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
 <?php if(\Yii::$app->session->hasFlash('success')) {  ?>
			<div class="alert alert-success" role="alert">	<?= Yii::$app->session->getFlash('success'); ?></div>
<?php } ?>

          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
               <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                <!-- form start -->
                
                               
                 <?= $this->render('_form', ['model'=>$model]) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->

