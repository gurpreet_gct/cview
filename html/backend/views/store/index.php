<?php

use yii\helpers\Html;
#use yii\grid\GridView;
use kartik\grid\GridView;
use backend\models\Usermanagement;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RegionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Stores');
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$visiable = 0;
$advertiser='';

if($userType==1){
$advertiser = ArrayHelper::map(Usermanagement::find() ->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
$visiable = 1;
}

?>


<div class="beacon-group-index">

<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
		return ['id' => $model['id']];
	},

        'columns'=>  [
            'storename',
            'address',
            'city',
            'state',
            
            [
            'attribute'=>'country',
            'value'=>'countryname.name',
            ],
            
            /*[
            'attribute'=>'owner',
            'value'=>'ownernameval',
            ],*/
            [
            //'label' => 'Advertiser',
            'attribute'=>Yii::t('app','owner'),
            'value'=> 'ownernameval',
            'visible'=> $visiable,
             'filterType'=>GridView::FILTER_SELECT2,
             'filterWidgetOptions' => [
             'data' => $advertiser,
             'size' => Select2::SMALL,
             'options' => ['placeholder' => 'Select advertiser'],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
             ],  
            ],
            
         /*   [
            'attribute'=>'status',
            'value'=>'statustype',
            'filter' => array('1' => 'Enable', '0' => 'Disable' ),
            ],*/
            
                      
           [
           'attribute'=>Yii::t('app','status'),
           'label'=>Yii::t('app','Status'),
			'format' => 'raw',
			'value'=>function ($model, $key, $index, $widget) {				
    
            return \yii\helpers\Html::dropDownList("status", $model->status, [1=>Yii::t('app','Enable'), 0=>Yii::t('app','Disable')],['prompt'=>Yii::t('app','Select'),'id'=>'updatestatus'.$model->id,'onchange'=>'setstatus('.$model->id.')']);
          
			},
			
			],

           [
            'attribute'=>'created_at',
          	'value' => function ($dataProvider) {
            return date("D, d M Y H:i:s ",$dataProvider["created_at"]);
               
       		 },
            ],
            
           ['class'=>'kartik\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:260px;'],
        'header'=>'Actions',
        'template' => '{update} {view} {delete}',
        'buttons' => [
        
           //delete button
            'delete' => function ($url, $model) {
				
               return '<a class="custom-del">	<span class="glyphicon glyphicon-trash" OnClick="DeleteAction('.$model->id.')" >  </span>
								</a> <input type="hidden" name="url" value="'.$url.'" id="url"'; 
            },
             
        ],
      
		            
		'urlCreator' => function ($action, $model, $key, $index) {
			
			$baseurl = Yii::$app->getUrlManager()->getBaseUrl(); 
			
		
					$redirectUrl=['update', 'id' => $model->id];
					$redirectUrlview=['view', 'id' => $model->id];	
					
					if((Yii::$app->request->getQueryParam("pid")))
					{
						$redirectUrl['pid']=Yii::$app->request->getQueryParam("pid");
						$redirectUrlview['pid']=Yii::$app->request->getQueryParam("pid");
						
						
					}
					
		     
				
			if ($action === 'update') {
                $url =$redirectUrl;
                return $url;
			}
			
			if ($action === 'view') {
                $url = $redirectUrlview;
                return $url;
			}
		
			if ($action === 'delete') {					
                $url =$baseurl.'/store/delete';
                return $url;
			}
		

           
	   }
           ],
           
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>Yii::t('app','Add Stores'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=>Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-user"></i>'.Yii::t('app','Stores'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>

<?php $baseurl = Yii::$app->getUrlManager()->getBaseUrl(); 
      $staturl =$baseurl.'/store/setstatus';
  
?>

 <script type="text/javascript">

function setstatus(id){
	
	var values = $('#updatestatus'+id).val();
	
	if(values!=''){
	var updateconfirm = confirm ("<?= Yii::t('app','Are you sure to update status?') ?>");
		
	if (updateconfirm) {		
	$.ajax({
	method: "POST",
    url: "<?php echo $staturl; ?>",
	data: { id: id, value: values }
	
	
	})
	.done(function( result ) {
		
	  
    alert("Store status has been updated successfully.");
    
	  });

	}
	}else{
		alert('Select any from Enable or Disable');
	}
	
}

</script>
</div>
