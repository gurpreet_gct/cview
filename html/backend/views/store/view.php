<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Store */

 
 
$this->title = ucfirst($model->storename);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Store'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$visible = 0;
if($userType==1){
$visible = 1;
}
else{
	$visiable = 0;
}
?>
 <?php if(\Yii::$app->session->hasFlash('success')) {  ?>
			<div class="alert alert-success" role="alert">	<?= Yii::$app->session->getFlash('success'); ?></div>
<?php } ?>

          <div class="row">
            <!-- left column -->  
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-user"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
				  <?php 
					$redirectUrl=['update', 'id' => $model->id];	
					$redirectDeleteUrl=['delete', 'id' => $model->id];	
					$redirectIndexUrl=['index'];	
					if((Yii::$app->request->getQueryParam("pid")))
					{
						$redirectUrl['pid']=Yii::$app->request->getQueryParam("pid");
						$redirectDeleteUrl['pid']=Yii::$app->request->getQueryParam("pid");
						$redirectIndexUrl['pid']=Yii::$app->request->getQueryParam("pid");
						
					}
					
		            ?>
             <?= Html::a(Yii::t('app','Update'), $redirectUrl, ['class' => 'btn btn-primary']) ?>
     		   <?= Html::a(Yii::t('app','Delete'), $redirectDeleteUrl, [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
          <?= Html::a(Yii::t('app','OK'), $redirectIndexUrl, ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->

 <div class="table-responsive">
    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'storename',
            'email',
            
             [                     
            'label' => Yii::t('app','Phone Number'),
            'format'=>'raw',
            'value'=> $model->mobileno,
          
             ], 
           
            
              [                     
            'label' => Yii::t('app','Opening Hours (24 hours)'),
            'value' => $model->openinghours,
             ], 
            
            
            'address',
            'street',
            'city',
            'state',
            
            [                     
            'label' => Yii::t('app','Country'),
            'value' => $model->countryname->name,
             ], 
             
            [                     
            'label' => Yii::t('app','Location owner'),
            'visible'=>$visible,
            'value' => $model->ownernameval,
             ], 
                      
            [                     
            'label' => Yii::t('app','Status'),
            'value' => $model->statustype,
             ], 
                        
             [                     
            'label' => Yii::t('app','Created At'),
            'value' => date("D, d M Y H:i:s",$model->created_at),
             ],
             [                     
            'label' => Yii::t('app','Updated at'),
            'value' => date("D, d M Y H:i:s",$model->updated_at),
             ],
             
             [                     
            'label' => Yii::t('app','Created By'),
            'value' => $model->createduser,
             ],
             
           	[                     
            'label' => Yii::t('app','Updated By'),
            'value' => $model->updateduser,
            ],
             
          
             
             
           
        ],
    ]) ?>
</div>
</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->

<link rel="stylesheet" href="<?php echo Yii::$app->homeUrl; ?>/dist/js/build/css/intlTelInput.css">
    <script src="<?php echo Yii::$app->homeUrl; ?>/dist/js/build/intlTelInput.js" type="text/javascript"></script>
    
<style>
.iti-flag {
    float: left;
    margin-right: 5px;
    margin-top: 5px;
}

.mobile-no {
    float: left;
   
}
</style>
