<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Store */

$this->title = Yii::t('app','Create Store Location');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Stores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

   <?php if(\Yii::$app->session->hasFlash('success')) {  ?>
			<div class="alert alert-success" role="alert">	<?= Yii::$app->session->getFlash('success'); ?></div>
	  	<?php } ?>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-plus"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                
<div class="panel-before" >  
	  
	<div class="row">
	<div class="col-sm-10 col-md-10 col-lg-10">
		<h4 class="bg-info">Please add in the location information of each of the stores you operate. This information will appear in the STORE LOCATOR section of the app. Click on "Save and Add" below to add a further location.
	  </h4>
	</div>
	<div class="col-sm-2" style="margin-top:12px;">
		<div role="toolbar" class="btn-toolbar kv-grid-toolbar">
		<div class="btn-group"><a title="<?= Yii::t('app','All Stores') ?>" href="<?=Yii::$app->urlManager->createUrl(['store/index',])?>" class="btn btn-primary"><i class="glyphicon glyphicon-user"></i></a> 

		<?php

		$redirectUrl=['store/create'];
		if((Yii::$app->request->getQueryParam("pid")))
		{
		$redirectUrl['pid']=Yii::$app->request->getQueryParam("pid");

		} ?>
		<a data-pjax="0" title="Reset Grid" href="<?=Yii::$app->urlManager->createUrl($redirectUrl)?>" class="btn btn-default"><i class="glyphicon glyphicon-repeat"></i></a></div>


		</div>    
	</div>
    
    <div class="clearfix"></div>
    
</div>
</div>
                
              
                
                <!-- form start -->
                
                 <?= $this->render('_form',['model'=>$model]) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
