<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Country;
use common\models\Profile;
use common\models\Beaconmanager;
use backend\models\Storebeacons;
use common\models\User;
use common\models\Categories;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

$usertype = Yii::$app->user->identity->user_type;
/* @var $this yii\web\View */
/* @var $model app\models\Store */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
		$geoVal = 0;
	if($usertype ==7){
		$uid = Yii::$app->user->identity->id;
		$geoRadious = Profile::find()->select('geo_radius')->where('user_id='.$uid)->one();
		$geoVal = $geoRadious->geo_radius;
	}

?>


<div class="store-form">

   <?php  $form = ActiveForm::begin([
		'id' => 'create-store-form',
		'options' => [
		'enctype' => 'multipart/form-data',
		],
		]);

		?>
     <div class="box-body">
      <div class="form-group">
	<?php

		if($usertype==1){
			$ownerNames =  ArrayHelper::map(User::find()
			->where(['user_type' => 7,'status' => 10])->all(), 'id', 'orgName');
		}else{
			$ownerNames =  ArrayHelper::map(User::find()
			->where(['user_type' => 7,'status' => 10,'id' => Yii::$app->user->identity->id])->all(), 'id', 'orgName');

		}
	?>
	<?php if(isset($_GET['pid'])) {
		  $model->owner = $_GET['pid'];
		   echo $form->field($model, 'owner',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Choose the partner that this store location belongs to">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->dropDownList($ownerNames, array('prompt'=>'Select','disabled' =>true));
		  echo $form->field($model, 'owner')->hiddeninput(['value' => $_GET['pid']])->label(false);

		}elseif($usertype==2 || $usertype==7){
		 echo $form->field($model, 'owner')->hiddeninput(['value' => Yii::$app->user->identity->id])->label(false);
		}else {
		   echo $form->field($model, 'owner',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Choose the partner that this store location belongs to">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->dropDownList($ownerNames, array('prompt'=>'Select'));
		} ?>

    <?= $form->field($model, 'storename',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Enter the Store Location Name to differentiate your location. For example: Jay&rsquo;s Tyres Camberwell, Kmart Chadstone etc.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phoneNumber')->textInput(['id' => 'phone']) ?>
    <?= $form->field($model, 'countrycode')->hiddenInput()->label(false); ?>


     <?= $form->field($model, 'addopeninghours')->checkbox(); ?>
      <button type="button" id='store-hours-popup' class="btn btn-info btn-lg" style="display:none" data-toggle="modal" data-target="#show-hours-model">Open Modal</button>

	<div class="help-block"></div>

	 <div class="modal fade" id="show-hours-model" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Select store opening hours (24 hours)') ?></h4>
        </div>
        <div class="modal-body">
           <?php

	   $storetimes= ['12:00AM'=>'12:00AM', '12:30AM' => '12:30AM','1:00AM' => '1:00AM','1:30AM' => '1:30AM','2:00AM' => '2:00AM','2:30AM' => '2:30AM','3:00AM' => '3:00AM','3:30AM' => '3:30AM','4:00AM' => '4:00AM','4:30AM' => '4:30AM','5:00AM' => '5:00AM','5:30AM' => '5:30AM','6:00AM' => '6:00AM','6:30AM' => '6:30AM','7:00AM' => '7:00AM','7:30AM' => '7:30AM','8:00AM' => '8:00AM','8:30AM' => '8:30AM','9:00AM' => '9:00AM','9:30AM' => '9:30AM','10:00AM' => '10:00AM','10:30AM' => '10:30AM','11:00AM' => '11:00AM','11:30AM' => '11:30AM','12:00PM'=>'12:00PM', '12:30PM' => '12:30PM','1:00PM' => '1:00PM','1:30PM' => '1:30PM','2:00PM' => '2:00PM','2:30PM' => '2:30PM','3:00PM' => '3:00PM','3:30PM' => '3:30PM','4:00PM' => '4:00PM','4:30PM' => '4:30PM','5:00PM' => '5:00PM','5:30PM' => '5:30PM','6:00PM' => '6:00PM','6:30PM' => '6:30PM','7:00PM' => '7:00PM','7:30PM' => '7:30PM','8:00PM' => '8:00PM','8:30PM' => '8:30PM','9:00PM' => '9:00PM','9:30PM' => '9:30PM','10:00PM' => '10:00PM','10:30PM' => '10:30PM','11:00PM' => '11:00PM','11:30PM' => '11:30PM'];

		?>
	<label for="customoffer-id" class="control-label"><?= Yii::t('app','Monday Open/Close Hours (Choose 1 of 3 options)') ?> </label>
	<div class="row">
			<div class="col-md-2 col-lg-2"> <?= $form->field($model, 'mondayFullDayOpen')->checkbox(['label'=>Yii::t('app','24 Hours')]); ?>  </div>
			<div class="col-md-2 col-lg-2">   <?= $form->field($model, 'mondayFullDayClose')->checkbox(['label'=>Yii::t('app','Closed')]); ?> </div>
			<div class="col-md-4 col-lg-4">
				<?=  $form->field($model, 'mondayopeninghoursAM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true,
				],
				])->label(false);
				?>

			</div>
			<div class="col-md-4 col-lg-4">
				<?=  $form->field($model, 'mondayopeninghoursPM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>


			</div>
		</div>
		<label for="customoffer-id" class="control-label"><?= Yii::t('app','Tuesday Open/Close Hours (Choose 1 of 3 options)') ?></label>
			<div class="row">
				<div class="col-md-2 col-lg-2"> <?= $form->field($model, 'tuesdayFullDayOpen')->checkbox(['label'=>Yii::t('app','24 Hours')]); ?>  </div>
				<div class="col-md-2 col-lg-2">   <?= $form->field($model, 'tuesdayFullDayClose')->checkbox(['label'=>Yii::t('app','Closed')]); ?> </div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'tuesdayopeninghoursAM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>

				</div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'tuesdayopeninghoursPM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>

				</div>
			</div>
		<label for="customoffer-id" class="control-label"><?= Yii::t('app','Wednesday Open/Close Hours (Choose 1 of 3 options)') ?> </label>
			<div class="row">
				<div class="col-md-2 col-lg-2"> <?= $form->field($model, 'wednesdayFullDayOpen')->checkbox(['label'=>Yii::t('app','24 Hours')]); ?>  </div>
				<div class="col-md-2 col-lg-2">   <?= $form->field($model, 'wednesdayFullDayClose')->checkbox(['label'=>Yii::t('app','Closed')]); ?> </div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'wednesdayopeninghoursAM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>


				</div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'wednesdayopeninghoursPM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>


			</div>
		</div>
		<label for="customoffer-id" class="control-label"><?= Yii::t('app','Thursday Open/Close Hours (Choose 1 of 3 options)') ?></label>
			<div class="row">
				<div class="col-md-2 col-lg-2"> <?= $form->field($model, 'thursdayFullDayOpen')->checkbox(['label'=>Yii::t('app','24 Hours')]); ?>  </div>
				<div class="col-md-2 col-lg-2">   <?= $form->field($model, 'thursdayFullDayClose')->checkbox(['label'=>Yii::t('app','Closed')]); ?> </div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'thursdayopeninghoursAM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>


				</div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'thursdayopeninghoursPM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>


				</div>
			</div>
		<label for="customoffer-id" class="control-label"><?= Yii::t('app','Friday Open/Close Hours (Choose 1 of 3 options)') ?></label>
			<div class="row">
				<div class="col-md-2 col-lg-2"> <?= $form->field($model, 'fridayFullDayOpen')->checkbox(['label'=>Yii::t('app','24 Hours')]); ?>  </div>
				<div class="col-md-2 col-lg-2">   <?= $form->field($model, 'fridayFullDayClose')->checkbox(['label'=>Yii::t('app','Closed')]); ?> </div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'fridayopeninghoursAM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>


				</div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'fridayopeninghoursPM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>


				</div>
			</div>
		<label for="customoffer-id" class="control-label"><?= Yii::t('app','Saturday Open/Close Hours (Choose 1 of 3 options)') ?></label>
			<div class="row">
				<div class="col-md-2 col-lg-2"> <?= $form->field($model, 'saturdayFullDayOpen')->checkbox(['label'=>Yii::t('app','24 Hours')]); ?>  </div>
				<div class="col-md-2 col-lg-2">   <?= $form->field($model, 'saturdayFullDayClose')->checkbox(['label'=>Yii::t('app','Closed')]); ?> </div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'saturdayopeninghoursAM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>


				</div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'saturdayopeninghoursPM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>


				</div>
			</div>
		<label for="customoffer-id" class="control-label"><?= Yii::t('app','Sunday Open/Close Hours (Choose 1 of 3 options)') ?> </label>
			<div class="row">
				<div class="col-md-2 col-lg-2"> <?= $form->field($model, 'sundayFullDayOpen')->checkbox(['label'=>'24 Hours']); ?>  </div>
				<div class="col-md-2 col-lg-2">   <?= $form->field($model, 'sundayFullDayClose')->checkbox(['label'=>'Closed']); ?> </div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'sundayopeninghoursAM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>


				</div>
				<div class="col-md-4 col-lg-4">
					<?=  $form->field($model, 'sundayopeninghoursPM')->widget(Select2::classname(), [
					'data' => $storetimes,
					'language' => 'en',
					'options' => ['placeholder' => Yii::t('app','Select time')],
					'pluginOptions' => [
						'allowClear' => true
				],
				])->label(false); ?>
				</div>
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal"><?= Yii::t('app','Submit') ?></button>
        </div>
      </div>

    </div>
  </div>

</div>

    <?= $form->field($model, 'address',['template' => "{label}\n{input}\t<input type='button' onclick='resetAddress()' value='Reset Address'/></i>\n{hint}\n{error}"])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
       <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
 <?php   $countryNames =  ArrayHelper::map(Country::find()->all(), 'id_countries', 'name'); ?>

    <?= $form->field($model, 'country')->dropDownList($countryNames, ['prompt'=>Yii::t('app','Select'),'options' => [13=> ['Selected'=>true]]]); ?>

    <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>
   <div class="georadies hidden">
  <?php   if($usertype == 7){
	  echo $form->field($model, 'geo_radius')->textInput(['value' => $geoVal]);

		} else { ?>
    <?= $form->field($model, 'geo_radius')->textInput() ?>
  <?php } ?>
  </div>
	<?php  $allCategories = Categories::find()->orderBy(['root' => SORT_ASC, 'lft' => SORT_ASC,])->all(); ?>


 </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">

         <?= Html::a(Yii::t('app','Save and exit'),  ['create'], ['data-pjax'=>0, 'class'=>'btn btn-success saveandnew', 'title'=> 'Stores','id'=>2]) ?>

        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' =>'btn btn-success hidden submitme' ]) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Stores']) ?>
		 <?= Html::a(Yii::t('app','Save and add another store location'),  ['create'], ['data-pjax'=>0, 'class'=>'btn btn-primary saveandnew', 'title'=> 'Stores','id'=>1]) ?>
    </div>
    </div>
		<input type="hidden" id="savenew" name="savenew" value="" />
    <?php ActiveForm::end(); ?>

</div>
<?php $baeUrl =  \Yii::$app->request->BaseUrl; ?>
<script type="text/javascript">
  $('.saveandnew').click(function(e){
	  e.preventDefault();
	  $('#savenew').val($(this).attr('id'));
	  $('.submitme').click();
});
$(document).ready(function() {
	$("#create-store-form").on('beforeSubmit', function (e) {
	var geocoder = new google.maps.Geocoder();


		if(($("#store-address").val()=="")&&(($("#store-latitude").val()=="" )||(($("#store-longitude").val()==""))))
		{

			var address =["store-street","store-city","store-state","store-country"];
			var faddress="";
			var sep="";
				for(var i=0;i<address.length;i++)
				{
					if(document.getElementById(address[i]).value!="")
					{
						faddress=faddress + sep + document.getElementById(address[i]).value;
						sep=", ";
					}
				}

			  geocoder.geocode({'address': faddress}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {

				 $("#store-address").val(results[0].formatted_address);
				  $("#store-latitude").val(results[0].geometry.location.lat());
				  $("#store-longitude").val(results[0].geometry.location.lng());
					var ret=true;
					if($("store-latitude").val()=="")
					{
						$(".field-store-latitude .help-block").innerHtml("Latitude can not be blank.");
						ret=false;
					}
					if($("store-longitue").val()=="")
					{
						$(".field-store-longitue .help-block").innerHtml("Longitude can not be blank.");
						ret=false;
					}
					alert($("#store-address").val());
					return false;
					if(ret)
					$("#create-store-form").submit();
				} else {
				  alert('Geocode was not successful for the following reason: ' + status);
				}
			  });


		}
		else if($("#store-address").val()=="")
		{

			  var latlng = {lat: parseFloat($("#store-latitude").val()), lng: parseFloat($("#store-longitude").val())};
			  geocoder.geocode({'location': latlng}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
				  if (results[1]) {
					$("#store-address").val(results[1].formatted_address);

				  } else {
					alert('No address match with given geo location.');
				  }
				} else {
				  alert('No address match with given geo location.');
				}
			  });

		}
		else
			return true;

		return false;
	});

	});

// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm ={
    "street_number": {
        "field": "store-street",
        "value": "short_name",
		"joint":false,
		"dropdown":false
    },
    "route": {
        "field": "store-street",
        "value": "short_name",
		"joint":true,
		"dropdown":false
    },
    "sublocality_level_1": {
        "field": "store-street",
        "value": "short_name",
		"joint":true,
		"dropdown":false
    },
    "locality": {
        "field": "store-city",
        "value": "long_name",
		"joint": false,
		"dropdown":false
    },
    "administrative_area_level_2": {
        "field": "store-city",
        "value": "long_name",
		"joint": false,
		"dropdown":false
    },
    "administrative_area_level_1": {
        "field": "store-state",
        "value": "long_name",
		"joint": false,
		"dropdown":false
    },
    "country": {
        "field": "store-country",
        "value": "long_name",
		"joint": false,
		"dropdown":true
    },
    "postal_code": {
        "field": "store-postal_code",
        "value": "long_name",
		"joint": false
    }
};

/* {
  "street": {field:"store-street",value:'short_name'},
 // route: 'long_name',
  "locality": {field:"storadministrative_area_level_1e-city",value:'long_name'},
  //: 'short_name',
  "country": {field:"store-country",value:'long_name'}
  //postal_code: 'short_name'
};*/

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('store-address')),
      {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function resetAddress()
{

  for (var component in componentForm) {

    document.getElementById(componentForm[component].field).value = '';
    document.getElementById(componentForm[component].field).readOnly = false;
  }
    document.getElementById('store-address').value='';
    document.getElementById('store-latitude').value='';
    document.getElementById('store-longitude').value='';
}
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {

    document.getElementById(componentForm[component].field).value = '';
    document.getElementById(componentForm[component].field).readOnly = true;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
	var searchAddressComponents = place.address_components,
		searchPostalCode="";

	$.each(searchAddressComponents, function(){
		if(this.types[i]=="postal_code"){
			$('#store-postal_code').val(this.short_name);

		}
	});

    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType].value];

		if(componentForm[addressType].joint)
		{
			var sep="";
			if(document.getElementById(componentForm[addressType].field).value!="")
			sep=" ";
		document.getElementById(componentForm[addressType].field).value =document.getElementById(componentForm[addressType].field).value +sep+ val;
		}
		else if(componentForm[addressType].dropdown)
		{

				var dd = document.getElementById(componentForm[addressType].field);
				for (var i = 0; i < dd.options.length; i++) {
				if (dd.options[i].text === val) {
					dd.selectedIndex = i;
					break;
				}
			}

		}
		else
		document.getElementById(componentForm[addressType].field).value = val;


	}
  }

  if(place.geometry.location)
  {
	  document.getElementById("store-latitude").value=place.geometry.location.lat();
	  document.getElementById("store-longitude").value=place.geometry.location.lng();
  }
  if($('#store-postal_code').val()==''){
	  document.getElementById('store-postal_code').readOnly = false;
  }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}





$( document ).ready(function() {



function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}
var dt = new Date();
var m = dt.getMonth();
var z = m + 1;
user = dt.getFullYear()+"-"+z+"-"+dt.getDate()+" "+dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
document.cookie="username="+user;
var xx= getCookie('username') ;


});

$('#store-addopeninghours').click(function(){

		if (this.checked) {
			$('#store-hours-popup').click();
		}
	})

/* check opening hours fields */

mondayopen = $('#store-mondayfulldayopen:checked').val();
mondayclose = $('#store-mondayfulldayclose:checked').val();
	if(mondayopen==1){
			$("#store-mondayfulldayclose").attr("disabled", true);
			$('#select2-store-mondayopeninghoursam-container').empty();
			$('#select2-store-mondayopeninghourspm-container').empty();
			$("#store-mondayopeninghoursam").val(null);
			$("#store-mondayopeninghourspm").val(null);
			$('#store-mondayopeninghoursam').prop('disabled', 'disabled');
			$('#store-mondayopeninghourspm').prop('disabled', 'disabled');

	}
	if(mondayclose==1){
			$("#store-mondayfulldayopen").attr("disabled", true);
			$('#select2-store-mondayopeninghoursam-container').empty();
			$('#select2-store-mondayopeninghourspm-container').empty();
			$("#store-mondayopeninghoursam").val(null);
			$("#store-mondayopeninghourspm").val(null);
			$('#store-mondayopeninghoursam').prop('disabled', 'disabled');
			$('#store-mondayopeninghourspm').prop('disabled', 'disabled');

	}
tuesdayopen = $('#store-tuesdayfulldayopen:checked').val();
tuesdayclose = $('#store-tuesdayfulldayclose:checked').val();
	if(tuesdayopen==1){
			$("#store-tuesdayfulldayclose").attr("disabled", true);
			$('#select2-store-tuesdayopeninghoursam-container').empty();
			$('#select2-store-tuesdayopeninghourspm-container').empty();
			$("#store-tuesdayopeninghoursam").val(null);
			$("#store-tuesdayopeninghourspm").val(null);
			$('#store-tuesdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-tuesdayopeninghourspm').prop('disabled', 'disabled');

	}
	if(tuesdayclose==1){
			$("#store-tuesdayfulldayopen").attr("disabled", true);
			$('#select2-store-tuesdayopeninghoursam-container').empty();
			$('#select2-store-tuesdayopeninghourspm-container').empty();
			$("#store-tuesdayopeninghoursam").val(null);
			$("#store-tuesdayopeninghourspm").val(null);
			$('#store-tuesdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-tuesdayopeninghourspm').prop('disabled', 'disabled');

	}
wednesdayopen = $('#store-wednesdayfulldayopen:checked').val();
wednesdayclose = $('#store-wednesdayfulldayclose:checked').val();
	if(wednesdayopen==1){
			$("#store-wednesdayfulldayclose").attr("disabled", true);
			$('#select2-store-wednesdayopeninghoursam-container').empty();
			$('#select2-store-wednesdayopeninghourspm-container').empty();
			$("#store-wednesdayopeninghoursam").val(null);
			$("#store-wednesdayopeninghourspm").val(null);
			$('#store-wednesdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-wednesdayopeninghourspm').prop('disabled', 'disabled');

	}
	if(wednesdayclose==1){
			$("#store-wednesdayfulldayopen").attr("disabled", true);
			$('#select2-store-wednesdayopeninghoursam-container').empty();
			$('#select2-store-wednesdayopeninghourspm-container').empty();
			$("#store-wednesdayopeninghoursam").val(null);
			$("#store-wednesdayopeninghourspm").val(null);
			$('#store-wednesdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-wednesdayopeninghourspm').prop('disabled', 'disabled');

	}
thursdayopen = $('#store-thursdayfulldayopen:checked').val();
thursdayclose = $('#store-thursdayfulldayclose:checked').val();
	if(thursdayopen==1){
			$("#store-thursdayfulldayclose").attr("disabled", true);
			$('#select2-store-thursdayopeninghoursam-container').empty();
			$('#select2-store-thursdayopeninghourspm-container').empty();
			$("#store-thursdayopeninghoursam").val(null);
			$("#store-thursdayopeninghourspm").val(null);
			$('#store-thursdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-thursdayopeninghourspm').prop('disabled', 'disabled');

	}
	if(thursdayclose==1){
			$("#store-thursdayfulldayopen").attr("disabled", true);
			$('#select2-store-thursdayopeninghoursam-container').empty();
			$('#select2-store-thursdayopeninghourspm-container').empty();
			$("#store-thursdayopeninghoursam").val(null);
			$("#store-thursdayopeninghourspm").val(null);
			$('#store-thursdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-thursdayopeninghourspm').prop('disabled', 'disabled');

	}
fridayopen = $('#store-fridayfulldayopen:checked').val();
fridayclose = $('#store-fridayfulldayclose:checked').val();
	if(fridayopen==1){
			$("#store-fridayfulldayclose").attr("disabled", true);
			$('#select2-store-fridayopeninghoursam-container').empty();
			$('#select2-store-fridayopeninghourspm-container').empty();
			$("#store-fridayopeninghoursam").val(null);
			$("#store-fridayopeninghourspm").val(null);
			$('#store-fridayopeninghoursam').prop('disabled', 'disabled');
			$('#store-fridayopeninghourspm').prop('disabled', 'disabled');

	}
	if(fridayclose==1){
			$("#store-fridayfulldayopen").attr("disabled", true);
			$('#select2-store-fridayopeninghoursam-container').empty();
			$('#select2-store-fridayopeninghourspm-container').empty();
			$("#store-fridayopeninghoursam").val(null);
			$("#store-fridayopeninghourspm").val(null);
			$('#store-fridayopeninghoursam').prop('disabled', 'disabled');
			$('#store-fridayopeninghourspm').prop('disabled', 'disabled');

	}
saturdayopen = $('#store-saturdayfulldayopen:checked').val();
saturdayclose = $('#store-saturdayfulldayclose:checked').val();
	if(saturdayopen==1){
			$("#store-saturdayfulldayclose").attr("disabled", true);
			$('#select2-store-saturdayopeninghoursam-container').empty();
			$('#select2-store-saturdayopeninghourspm-container').empty();
			$("#store-saturdayopeninghoursam").val(null);
			$("#store-saturdayopeninghourspm").val(null);
			$('#store-saturdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-saturdayopeninghourspm').prop('disabled', 'disabled');

	}
	if(saturdayclose==1){
			$("#store-saturdayfulldayopen").attr("disabled", true);
			$('#select2-store-saturdayopeninghoursam-container').empty();
			$('#select2-store-saturdayopeninghourspm-container').empty();
			$("#store-saturdayopeninghoursam").val(null);
			$("#store-saturdayopeninghourspm").val(null);
			$('#store-saturdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-saturdayopeninghourspm').prop('disabled', 'disabled');

	}
sundayopen = $('#store-sundayfulldayopen:checked').val();
sundayclose = $('#store-sundayfulldayclose:checked').val();
	if(sundayopen==1){
			$("#store-sundayfulldayclose").attr("disabled", true);
			$('#select2-store-sundayopeninghoursam-container').empty();
			$('#select2-store-sundayopeninghourspm-container').empty();
			$("#store-sundayopeninghoursam").val(null);
			$("#store-sundayopeninghourspm").val(null);
			$('#store-sundayopeninghoursam').prop('disabled', 'disabled');
			$('#store-sundayopeninghourspm').prop('disabled', 'disabled');

	}
	if(sundayclose==1){
			$("#store-sundayfulldayopen").attr("disabled", true);
			$('#select2-store-sundayopeninghoursam-container').empty();
			$('#select2-store-sundayopeninghourspm-container').empty();
			$("#store-sundayopeninghoursam").val(null);
			$("#store-sundayopeninghourspm").val(null);
			$('#store-sundayopeninghoursam').prop('disabled', 'disabled');
			$('#store-sundayopeninghourspm').prop('disabled', 'disabled');

	}

/* with check box */

$('#store-mondayfulldayopen').click(function(){
		if (this.checked) {

			$("#store-mondayfulldayclose").attr("disabled", true);
			$('#select2-store-mondayopeninghoursam-container').empty();
			$('#select2-store-mondayopeninghourspm-container').empty();
			$("#store-mondayopeninghoursam").val(null);
			$("#store-mondayopeninghourspm").val(null);
			$('#store-mondayopeninghoursam').prop('disabled', 'disabled');
			$('#store-mondayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-mondayfulldayclose").attr("disabled", false);
			$("#store-mondayopeninghoursam").attr("disabled", false);
			$("#store-mondayopeninghourspm").attr("disabled", false);
		}
	});

$('#store-mondayfulldayclose').click(function(){
		if (this.checked) {
			$("#store-mondayfulldayopen").attr("disabled", true);
			$('#select2-store-mondayopeninghoursam-container').empty();
			$('#select2-store-mondayopeninghourspm-container').empty();
			$("#store-mondayopeninghoursam").val(null);
			$("#store-mondayopeninghourspm").val(null);
			$('#store-mondayopeninghoursam').prop('disabled', 'disabled');
			$('#store-mondayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-mondayfulldayopen").attr("disabled", false);
			$("#store-mondayopeninghoursam").attr("disabled", false);
			$("#store-mondayopeninghourspm").attr("disabled", false);
		}
	});
$('#store-tuesdayfulldayopen').click(function(){
		if (this.checked) {

			$("#store-tuesdayfulldayclose").attr("disabled", true);
			$('#select2-store-tuesdayopeninghoursam-container').empty();
			$('#select2-store-tuesdayopeninghourspm-container').empty();
			$("#store-tuesdayopeninghoursam").val(null);
			$("#store-tuesdayopeninghourspm").val(null);
			$('#store-tuesdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-tuesdayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-tuesdayfulldayclose").attr("disabled", false);
			$("#store-tuesdayopeninghoursam").attr("disabled", false);
			$("#store-tuesdayopeninghourspm").attr("disabled", false);
		}
	});

$('#store-tuesdayfulldayclose').click(function(){
		if (this.checked) {
			$("#store-tuesdayfulldayopen").attr("disabled", true);
			$('#select2-store-tuesdayopeninghoursam-container').empty();
			$('#select2-store-tuesdayopeninghourspm-container').empty();
			$("#store-tuesdayopeninghoursam").val(null);
			$("#store-tuesdayopeninghourspm").val(null);
			$('#store-tuesdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-tuesdayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-tuesdayfulldayopen").attr("disabled", false);
			$("#store-tuesdayopeninghoursam").attr("disabled", false);
			$("#store-tuesdayopeninghourspm").attr("disabled", false);
		}
	});
	$('#store-wednesdayfulldayopen').click(function(){
		if (this.checked) {

			$("#store-wednesdayfulldayclose").attr("disabled", true);
			$('#select2-store-wednesdayopeninghoursam-container').empty();
			$('#select2-store-wednesdayopeninghourspm-container').empty();
			$("#store-wednesdayopeninghoursam").val(null);
			$("#store-wednesdayopeninghourspm").val(null);
			$('#store-wednesdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-wednesdayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-wednesdayfulldayclose").attr("disabled", false);
			$("#store-wednesdayopeninghoursam").attr("disabled", false);
			$("#store-wednesdayopeninghourspm").attr("disabled", false);
		}
	});

$('#store-wednesdayfulldayclose').click(function(){
		if (this.checked) {
			$("#store-wednesdayfulldayopen").attr("disabled", true);
			$('#select2-store-wednesdayopeninghoursam-container').empty();
			$('#select2-store-wednesdayopeninghourspm-container').empty();
			$("#store-wednesdayopeninghoursam").val(null);
			$("#store-wednesdayopeninghourspm").val(null);
			$('#store-wednesdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-wednesdayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-wednesdayfulldayopen").attr("disabled", false);
			$("#store-wednesdayopeninghoursam").attr("disabled", false);
			$("#store-wednesdayopeninghourspm").attr("disabled", false);
		}
	});
$('#store-thursdayfulldayopen').click(function(){
		if (this.checked) {

			$("#store-thursdayfulldayclose").attr("disabled", true);
			$('#select2-store-thursdayopeninghoursam-container').empty();
			$('#select2-store-thursdayopeninghourspm-container').empty();
			$("#store-thursdayopeninghoursam").val(null);
			$("#store-thursdayopeninghourspm").val(null);
			$('#store-thursdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-thursdayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-thursdayfulldayclose").attr("disabled", false);
			$("#store-thursdayopeninghoursam").attr("disabled", false);
			$("#store-thursdayopeninghourspm").attr("disabled", false);
		}
	});

$('#store-thursdayfulldayclose').click(function(){
		if (this.checked) {
			$("#store-thursdayfulldayopen").attr("disabled", true);
			$('#select2-store-thursdayopeninghoursam-container').empty();
			$('#select2-store-thursdayopeninghourspm-container').empty();
			$("#store-thursdayopeninghoursam").val(null);
			$("#store-thursdayopeninghourspm").val(null);
			$('#store-thursdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-thursdayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-thursdayfulldayopen").attr("disabled", false);
			$("#store-thursdayopeninghoursam").attr("disabled", false);
			$("#store-thursdayopeninghourspm").attr("disabled", false);
		}
	});
	$('#store-fridayfulldayopen').click(function(){
		if (this.checked) {

			$("#store-fridayfulldayclose").attr("disabled", true);
			$('#select2-store-fridayopeninghoursam-container').empty();
			$('#select2-store-fridayopeninghourspm-container').empty();
			$("#store-fridayopeninghoursam").val(null);
			$("#store-fridayopeninghourspm").val(null);
			$('#store-fridayopeninghoursam').prop('disabled', 'disabled');
			$('#store-fridayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-fridayfulldayclose").attr("disabled", false);
			$("#store-fridayopeninghoursam").attr("disabled", false);
			$("#store-fridayopeninghourspm").attr("disabled", false);
		}
	});

$('#store-fridayfulldayclose').click(function(){
		if (this.checked) {
			$("#store-fridayfulldayopen").attr("disabled", true);
			$('#select2-store-fridayopeninghoursam-container').empty();
			$('#select2-store-fridayopeninghourspm-container').empty();
			$("#store-fridayopeninghoursam").val(null);
			$("#store-fridayopeninghourspm").val(null);
			$('#store-fridayopeninghoursam').prop('disabled', 'disabled');
			$('#store-fridayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-fridayfulldayopen").attr("disabled", false);
			$("#store-fridayopeninghoursam").attr("disabled", false);
			$("#store-fridayopeninghourspm").attr("disabled", false);
		}
	});
		$('#store-saturdayfulldayopen').click(function(){
		if (this.checked) {

			$("#store-saturdayfulldayclose").attr("disabled", true);
			$('#select2-store-saturdayopeninghoursam-container').empty();
			$('#select2-store-saturdayopeninghourspm-container').empty();
			$("#store-saturdayopeninghoursam").val(null);
			$("#store-saturdayopeninghourspm").val(null);
			$('#store-saturdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-saturdayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-saturdayfulldayclose").attr("disabled", false);
			$("#store-saturdayopeninghoursam").attr("disabled", false);
			$("#store-saturdayopeninghourspm").attr("disabled", false);
		}
	});

$('#store-saturdayfulldayclose').click(function(){
		if (this.checked) {
			$("#store-saturdayfulldayopen").attr("disabled", true);
			$('#select2-store-saturdayopeninghoursam-container').empty();
			$('#select2-store-saturdayopeninghourspm-container').empty();
			$("#store-saturdayopeninghoursam").val(null);
			$("#store-saturdayopeninghourspm").val(null);
			$('#store-saturdayopeninghoursam').prop('disabled', 'disabled');
			$('#store-saturdayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-saturdayfulldayopen").attr("disabled", false);
			$("#store-saturdayopeninghoursam").attr("disabled", false);
			$("#store-saturdayopeninghourspm").attr("disabled", false);
		}
	});
		$('#store-sundayfulldayopen').click(function(){
		if (this.checked) {

			$("#store-sundayfulldayclose").attr("disabled", true);
			$('#select2-store-sundayopeninghoursam-container').empty();
			$('#select2-store-sundayopeninghourspm-container').empty();
			$("#store-sundayopeninghoursam").val(null);
			$("#store-sundayopeninghourspm").val(null);
			$('#store-sundayopeninghoursam').prop('disabled', 'disabled');
			$('#store-sundayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-sundayfulldayclose").attr("disabled", false);
			$("#store-sundayopeninghoursam").attr("disabled", false);
			$("#store-sundayopeninghourspm").attr("disabled", false);
		}
	});

$('#store-sundayfulldayclose').click(function(){
		if (this.checked) {
			$("#store-sundayfulldayopen").attr("disabled", true);
			$('#select2-store-sundayopeninghoursam-container').empty();
			$('#select2-store-sundayopeninghourspm-container').empty();
			$("#store-sundayopeninghoursam").val(null);
			$("#store-sundayopeninghourspm").val(null);
			$('#store-sundayopeninghoursam').prop('disabled', 'disabled');
			$('#store-sundayopeninghourspm').prop('disabled', 'disabled');
		}else{
			$("#store-sundayfulldayopen").attr("disabled", false);
			$("#store-sundayopeninghoursam").attr("disabled", false);
			$("#store-sundayopeninghourspm").attr("disabled", false);
		}
	});

	$( "#store-mondayopeninghoursam" ).change(function() {
	  $( ".select2-search__field" ).focus();
	});


	$.fn.modal.Constructor.prototype.enforceFocus = function() {}
</script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCJAWURcRMIgLBQ6Wt0X7WHrUWk68E5N4g&libraries=places&callback=initAutocomplete"
        async defer></script>
 <link rel="stylesheet" href="<?php echo Yii::$app->homeUrl; ?>/dist/js/build/css/intlTelInput.css">
    <script src="<?php echo Yii::$app->homeUrl; ?>/dist/js/build/intlTelInput.js" type="text/javascript"></script>
        <script>
      $("#phone").intlTelInput({
        //allowExtensions: true,
        //autoFormat: false,
        //autoHideDialCode: false,
        //autoPlaceholder: false,
        defaultCountry: "au",
        //ipinfoToken: "yolo",
         nationalMode: true,
          numberType: "MOBILE",
        //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        //preferredCountries: ['cn', 'jp'],
        utilsScript: "<?php echo Yii::$app->homeUrl; ?>/dist/js/build/utils.js"
      });

       var dial = document.getElementById('phone').value;
          $("#phone").intlTelInput("setNumber", dial);

		$('.intl-tel-input').change(function(){
			//$("#box1 option:selected").text();
			var Countryval  =$('.iti-mobile-select option:selected').val();
			$('#store-countrycode').val(Countryval);

			});

         $('.iti-mobile-select').change(function() {
				var Countryval  =$('option:selected', this).val();

				$('#store-countrycode').val(Countryval);

		 });

         var countrycodeid = $('#store-countrycode').val();
          if(countrycodeid != '')
         {
			 $("#phone").intlTelInput("selectCountry", countrycodeid);

		}

$('#store-owner').change(function(){
		var partnerId = $(this).val();
		$('#store-geo_radius').val(0);
		$.ajax({
			url:'<?php echo $baeUrl;?>'+'/store/georadious',
			type:'GET',
			data:{partnerId:partnerId}

		})
		.done(function(response){
			var res = JSON.parse(response)
			if(res.isFood==1){
				$('.georadies').removeClass('hidden');
				$('#store-geo_radius').val(res.geo_radius);
			}else{
				$('.georadies').addClass('hidden');
				$('#store-geo_radius').val('');
			}

		});

	});
$('#store-owner').trigger('change');

</script>
<style>
	h4.bg-info { font-size:14px;line-height: 1.4; padding: 1px 5px; text-align: justify;}
.information {  background-color: #006dd5;  border-radius: 9px;  color: #fff;
  font-size: 12px;  padding:1px;}
</style>
