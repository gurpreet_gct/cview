<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\StampcardQr;
use dosamigos\qrcode\QrCode;

/* @var $this yii\web\View */
/* @var $model backend\models\Workorders */


$this->title =  ucfirst($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Workorders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
          <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
					  <?= Html::encode($this->title) ?> 
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <?php // Html::encode($this->title) ?> </h3>
              <p>   
             
     		   <h3> QR Images </h3>
          

           </p>
           <div class="box box-success"></div>
           <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
             'id',
            'name',
            
			 [                    
            'label' => Yii::t('app','Campaign Type'),
            'value' => $model->campaigntypename,
             ],
             [                    
            'label' => Yii::t('app','Partner Name'),
            'value' => $model->partnername->orgName,
             ],
             [                    
            'label' => Yii::t('app','Country Name'),
            'value' => $model->countryname->name,
             ],
             
          
            'mediaContractNumber',
            'noPasses',
           
            'duration',
            [                    
            'label' => Yii::t('app','Date From'),
            'value' => $model->dateFrom,
             ],
             [                     
            'label' => Yii::t('app','Date To'),
            'value' => $model->dateTo,
             ],
             
            
			[                     
            'label' => Yii::t('app','User Type'),
            'value' => $model->usertype,
             ],         
            
            [                     
            'label' => Yii::t('app','Age Group'),
            'value' => $model->agegroupname,
             ],          
           
             [                     
            'label' => Yii::t('app','Beacon Group'),
            'value' => $model->beacongroupname,
             ], 
            

        ],
    ]) ?>
          
         <?php
         
          $qrdata = StampcardQr::find()->where(['workorder_id' => $model->id ])->all();
          
          foreach($qrdata as $qrvalue){
			  
		
		echo '<img style="width: 99px; height: 99px; " src= "'.Yii::$app->urlManagerBackEnd->createAbsoluteUrl('qrcodes/'.$qrvalue->qr_code.'.png').'"/></br>';
		 
		  }
    
        
           
        
         
         
          ?>
        
                </div><!-- /.box-header -->
                <!-- form start -->
                



   
    

</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
<style>
.modal-dialog {
  margin: 30px auto;
  width: 430px;
}
.icon-ms {
	background-color:#F3F3F3;
	}


</style>
