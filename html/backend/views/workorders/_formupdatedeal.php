<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use common\models\Workorderpopup;
use common\models\Workorders;
use common\models\Profile;
use backend\models\Beacongroup;
use common\models\Store;
use common\models\User;
use common\models\Country;
use backend\models\Beaconlocation;
use backend\models\Usermanagement;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;
use kartik\select2\Select2;
use paras\cropper\Widget;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\Workorders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="workorders-form">

    <?php

    $form = ActiveForm::begin([
                'options' => [
                    'enctype' =>'multipart/form-data'
                ]
    ]);
    ?>

    <div class="box-body">
      <div class="form-group">
	 <?php  //echo $form->errorSummary($dealmodel); ?>

	 <?php
		$BeaconPop = Workorderpopup::find()->where(['workorder_id' => $_GET['id']])->one();


		$popupphoto = explode("workorder/",$BeaconPop->photo);
		$popuplogo = explode("workorder/",$BeaconPop->logo);

	 ?>


	 <?php

	$getcmpType = workorders::find()->select(['type','workorderpartner'])->where(['id' => $_GET['id']])->one();
	if(isset($getcmpType->workorderpartner)) {
	$partnerinfo = Profile::find()->select(['companyName','brandAddress'])->where(['user_id' => $getcmpType->workorderpartner])->one();
		/* GET partner info from profile model */
		if(isset($partnerinfo->companyName)){
			$partnerName = $partnerinfo->companyName;
			}else{
			$partnerName = Yii::t('app','Not Selected');
			}

		if(isset($partnerinfo->brandAddress)){
			$partnerAddress = $partnerinfo->brandAddress;
			}else{
			$partnerAddress = Yii::t('app','Not Selected');
			}

		}

 ?>
	 <?= $form->field($dealmodel, 'campaignID' )->hiddenInput(['value' => $_GET['id']])->label(false); ?>
    <!--  workorder type in popup form  -->
     <?= $form->field($dealmodel, 'wtype_pop' )->hiddenInput(['value' => $getcmpType->type])->label(false); ?>



		<div class="form-group field-workorderpopup-imagefile" style="margin-top:10px; margin-bottom:2px;">
		<!--<label class="control-label" for="deal-imagefile2">Logo Photo ( Logo size should be 132px x 132px dimension )</label>
		 </div>
		<?php
		if($BeaconPop->logo!=''){
		 //echo '<img width="132" height="132" src="'.$BeaconPop->logo.'"/>';
		 } ?>


		<?= $form->field($dealmodel, 'dealTitle')->hiddenInput(['value' => $BeaconPop->offerTitle])->label(false); ?>			

		<?= $form->field($dealmodel, 'text')->hiddenInput(['value' => $BeaconPop->offerText])->label(false); ?>

		<?= $form->field($dealmodel, 'logoUpload')->hiddenInput(['value' => isset($popuplogo[1])?$popuplogo[1]:''])->label(false); ?>


		<?= $form->field($dealmodel, 'offerwas')->hiddenInput()->label(false); ?>
		<?= $form->field($dealmodel, 'offernow')->hiddenInput()->label(false); ?>



<!-- end deal store form edit  -->


   	<?php echo $form->field($dealmodel, 'dealuploadTypes')->radioList(['1' => Yii::t('app','Add image'), '2' => Yii::t('app','Add Video')]); ?>
		<?php
			echo '<div id="deal-image-block" style="display:none;">';

				if($dealmodel->mainPhotoUpload!=''){
					$dealmodel->imageFile3 = $dealmodel->mainPhotoUpload;
				 //echo '<img  id="imagedealmainPhotoUpload" width="300" height="120" src="'.$dealmodel->mainPhotoUpload.'"/>';
				 } ?>
				<!-- preview image of image -->
				<div id="previewImage"></div>

				<!-- preview image of image -->
			<?php echo $form->field($dealmodel, 'imageFile3')->widget(Widget::className(), [
    'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
    	'width'=>750,
		'height'=>360]) ;
			echo '</div>';

			echo '<div id="deal-video-block-types" style="display:none;">';
			echo $form->field($dealmodel, 'dealvideoTypes')->radioList(['1' => Yii::t('app','Add Video Other/Video Url'), '2' => Yii::t('app','Add Video  Youtube/vimeo')]);
			echo $form->field($dealmodel, 'dealbrowsercheck')->radioList(['1' => Yii::t('app','Yes'), '0' => Yii::t('app','No')])->label(Yii::t('app','Open Video In Browser'));
			echo '</div>';
			echo'<div id="deal-video-block-types-youtube" style="display:none;">';
			echo $form->field($dealmodel, 'dealvideokey')->textInput()->label(Yii::t('app','Deal Video'));
			echo '</div>';
			echo'<div id="deal-video-block-types-system" style="display:none;">';
			?>
			<?php
			 if(($dealmodel->dealvideoTypes==1) && $dealmodel->dealvideokey!='' ){
				$dealmodel->dealvideoWorkorderName = $dealmodel->dealvideokey;
				$videoext = explode('.',$dealmodel->dealvideokey);
				if(isset($videoext[1]) && $videoext[1]=='mp4') {
					?>

			<video width="400" controls>
		  <source src="<?php echo $dealmodel->dealvideoWorkorderName;?>" type="video/<?php if(isset($videoext[1])) { echo $videoext[1]; } ?>">

		  </video>
		<?php  }
			 }
			?>
			<?php echo $form->field($dealmodel, 'dealvideoWorkorderName')->fileInput()->label(Yii::t('app','Video Upload')); ?>
			<?= Yii::t('app','OR') ?>
			<?php
			$dealmodel->dealvideoWorkorderUrl = $dealmodel->dealvideokey;

			echo $form->field($dealmodel, 'dealvideoWorkorderUrl')->textInput()->label(Yii::t('app','Video Url')); ?>
			<?php

			if(($dealmodel->dealvideoTypes==1) && $dealmodel->dealvideoImage!=''){
			$dealmodel->dealvideoWorkorderImage = $dealmodel->dealvideoImage;
			echo '<img id="imagedealvideoImage" src="'.$dealmodel->dealvideoImage.'"/>';
		 } ?>
			<!-- preview image -->
			<div id="previewDealImage"></div>

			<!-- preview image -->
			<?php echo $form->field($dealmodel, 'dealvideoWorkorderImage')->widget(Widget::className(), [
    'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
    	'width'=>132,
		'height'=>132])->label(Yii::t('app','Deal Video Image'));
			 echo '</div>';
			?>



     </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
       <?= Html::a('BACK',['updateoffer','id'=>$_GET['id'],'step'=>2],['class'=>'btn btn-default','id'=>1]) ?>
		<?= Html::a('SAVE AND EXIT',['create'],['class'=>'btn btn-primary progressbtn','id'=>1]) ?>
		<?= Html::a('NEXT',['updateoffer','id'=>$_GET['id'],'step'=>5],['class'=>'btn btn-success progressbtn','id'=>2]) ?>
    </div>
    </div>
<input type="hidden" id="savenew" name="savenew" value="" />
    <?php ActiveForm::end(); ?>

</div>


<script>
$('.progressbtn').click(function(e){
		e.preventDefault();
		$('#savenew').val($(this).attr('id'));
		$('#w0').submit();
});
/* preview image on upload with info */

var _URL = window.URL || window.webkitURL;



function previewImage(input,boxId,imgW,imgH) {


    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {

			$("#"+boxId).html('<img id="upload_preview" src="'+e.target.result+'" style="width:'+imgW+'px;height:'+imgH+'px" />');


        }

        reader.readAsDataURL(input.files[0]);


    }
}

$("#deal-imagefile3").change(function(){

	var file, img;
	var imgObj = this;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function() {
				previewImage(imgObj,'previewImage',650,360);

				var width 	=	this.width;
				var height 	=	this.height;

				if(width!=750 && height!=360){

					alert('Please note an image may become distorted if it is not the correct shape.\nThis image size is '+width+'px X '+height+'px.');
				}
			};
			img.onerror = function() {
				alert( "not a valid file: " + file.type);

			};
			img.src = _URL.createObjectURL(file);
	}


});

$("#deal-dealvideoworkorderimage").change(function(){
	var file, img;
	var imgObj = this;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function() {
				previewImage(imgObj,'previewDealImage',640,300);

				var width 	=	this.width;
				var height 	=	this.height;

				if(width!=640 && height!=300){

					alert('Please note an image may become distorted if it is not the correct shape.\nThis image size is '+width+'px X '+height+'px.');
				}
			};
			img.onerror = function() {
				alert( "not a valid file: " + file.type);

			};
			img.src = _URL.createObjectURL(file);
	}


});

/* preview image on upload with info */
</script>
<style>
	h4.bg-info {font-size:14px; line-height: 1.4; padding: 1px 5px; text-align: justify;}
	.box-footer a {    margin: 1px 5px;}
</style>
