<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use backend\models\Beacongroup;
use common\models\Country;
use backend\models\Beaconlocation;
use backend\models\Usermanagement;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;
use paras\cropper\Widget;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\Beaconpopup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="workorders-form">

      <?php
    $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-offerpopup-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]);
    ?>
    <div class="box-body">
		<div class="form-group">
		<?php //echo $form->errorSummary($model); ?>
	 
	 
	  
	<?= $form->field($model, 'psaoffertype')->dropDownList(['1' =>Yii::t('app','PSA (info)'), '2' =>Yii::t('app','PSA (deal)')], 
						['prompt'=>Yii::t('app','..Select..')])->label(Yii::t('app','PSA Type')); ?>
	 
	
	
    <?= $form->field($model, 'offerTitle')->textarea()->label(Yii::t('app','Text title (50 characters max)')); ?>
  
    <?= $form->field($model, 'workorder_id' )->hiddenInput(['value' => $_GET['id']])->label(false); ?>
    <?= $form->field($model, 'offertype' )->hiddenInput(['value' => 10])->label(false); ?>
    
    <?= $form->field($model, 'offerText')->textarea()->label(Yii::t('app','Text (220 characters max)'));; ?>
		

	<?php // $form->field($model, 'offerText')->textarea(); ?>
	<?= $form->field($model, 'iphoneNotificationText')->textarea(); ?>
		
	<?= $form->field($model, 'imageFile')->widget(Widget::className(), [
    'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
    	'width'=>132,
		'height'=>132])->label(Yii::t('app','Offer photo (Image size should be 640px x 300px dimension)')); ?>
	
     </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
