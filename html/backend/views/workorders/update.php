<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Workorders */

$this->title = Yii::t('app','STEP 1: CREATE CAMPAIGN');
?>
    <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
				<div class=" panel panel-primary">
					<div class="panel-heading">    
						<div class="pull-right"></div>
						<!-- Header Title-->
						<h3 class="panel-title">
							STEP 1: CREATE CAMPAIGN
						</h3>
						<!-- Header Title end -->
						<div class="clearfix"></div>
					</div>
					<!-- form start -->
                
					<?= $this->render('_form', [
								'model' => $model,
								'categoryTree'=>$categoryTree,
						]) ?>
              
				</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->