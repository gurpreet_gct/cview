<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Categories;
use backend\models\Beacongroup;
use common\models\Store;
use common\models\Workorderpopup;
use common\models\Workorders;
use common\models\Qr;
use common\models\Country;
use common\models\Product;
use common\models\Profile;
use backend\models\Beaconlocation;
use backend\models\Usermanagement;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use paras\cropper\Widget;

/* @var $this yii\web\View */
/* @var $offermodel backend\models\Workorders */
/* @var $form yii\widgets\ActiveForm */
$baseurl = Yii::$app->getUrlManager()->getBaseUrl(); 

?>

<div class="workorders-form">

    <?php
  
    $form = ActiveForm::begin([
                'options' => [                 
                    'enctype' =>'multipart/form-data'
                ]
    ]);
    ?>
    
    <div class="box-body">
      <div class="form-group">
	 <?php //echo $form->errorSummary($offermodel);  ?>
	 
	 	<!-- start offer form edit -->
	
	<?php 
	
	//echo "<pre>"; print_r($offermodel);?>
	
		
	<?php
	$getcmpType = workorders::find()->select(['type','workorderpartner'])->where(['id' => $_GET['id']])->one();
	
	if(isset($getcmpType->workorderpartner)) {
		
	$partnerinfo = Profile::find()->select(['companyName','brandAddress'])->where(['user_id' => $getcmpType->workorderpartner])->one();
	
		/* GET partner info from profile model */
		
		if(isset($partnerinfo->companyName)){
			$partnerName = $partnerinfo->companyName;
			}else{
			$partnerName = Yii::t('app','Not Selected');
			}
		
		if(isset($partnerinfo->brandAddress)){
			$partnerAddress = $partnerinfo->brandAddress;
			}else{
			$partnerAddress = Yii::t('app','Not Selected');
			}
	
		}
	?>
	
	
		<!-- Change offer title as per workorder type  start -->
		 <?php
		 /* if stamp card  */
		  if($getcmpType->type == 2){
			  echo $form->field($offermodel, 'offerTitle' )->textInput(['value' => $partnerName,'readonly' => true])->label(Yii::t('app','Advertiser name'));
			  echo $form->field($offermodel, 'partnerAddress' )->textInput(['value' => $partnerAddress,'readonly' => true])->label(Yii::t('app','Advertiser address'));
			 
		  }elseif($getcmpType->type == 4){
		  /* if event ticket */
				echo $form->field($offermodel, 'offerTitle' )->textInput()->label(Yii::t('app','Ticket event name:'));
				echo $form->field($offermodel, 'eventVenue' )->textInput()->label(Yii::t('app','Event venue:'));
				
		  }else{
		 /* if voucher */
			  echo $form->field($offermodel, 'offerTitle')->textarea();
		  }
		  ?>
		<!-- Change offer title as per workorder type  end -->
		<?= $form->field($offermodel, 'campaignID' )->hiddenInput(['value' => $_GET['id']])->label(false); ?>
		<!--  workorder type in popup form  -->
		<?= $form->field($offermodel, 'wtype_pop' )->hiddenInput(['value' => $getcmpType->type])->label(false); ?>
	
		<?php
		$checkbuy = Workorders::find()->select(['buynow','workorderpartner'])->where(['id' => $_GET['id']])->one();   	
		echo $form->field($offermodel, 'buynow' )->hiddenInput(['value' => $checkbuy->buynow])->label(false); 
		/* select product section start "if workorder type voucher" */  
        if($getcmpType->type == 1) {
		if($checkbuy->buynow==1) {  	 
		// select product
		
			echo $form->field($offermodel, 'product_id')->widget(Select2::classname(), [

				'data' => $partnerProducts,
				'options' => ['placeholder' => Yii::t('app','Select product ...')],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); 
			 // select price 
			echo $form->field($offermodel, 'productprice')->textInput(['disabled'=>true, 'value' => '' ,'id'=>'workorderpopup-productprice'])->label(Yii::t('app','Selected product price'));
			echo $form->field($offermodel, 'buynow' )->hiddenInput(['value' => 1])->label(false); 	
		 }else { 
			echo $form->field($offermodel, 'buynow' )->hiddenInput(['value' => 0])->label(false); 	
		 }
	  }
		 /* close product section */
		 ?>
	
	
    
     <?php 
		  $tempstart = [];
		  for ($x = 1; $x <= 1000; $x++) {
			  $xf = $x;			 
			$tempstart[$x] = $xf;
		   } 
			  
		?>
		
		<!-- Make offer type as per workorder type start -->		
		<!-- if workorder type as event ticket -->
		<?php 
			if($getcmpType->type == 4){ 
			 
			printf('<label class="control-label" for="customoffer-id">%s</label><div class="row">
			<div class="col-md-6 col-lg-6">',Yii::t('app','Select Offer Was/Now (Numeric Only in $)') );		
				echo $form->field($offermodel, 'offerwas')->textInput(['type'=>'number', 'min'=>'0','step'=>'0.01']); 
			echo '</div> <div class="col-md-6 col-lg-6">';
				echo $form->field($offermodel, 'offernow')->textInput(['type'=>'number', 'min'=>'0','step'=>'0.01']); 
		    echo '</div>';
			}else {
				
				if($getcmpType->type == 2){ ?>
					<!--<php /* $form->field($offermodel, 'offertype')->radioList(array('3'=>Yii::t('app','Offer Text (Free, OFF etc.)')),['id' => 'customoffer-id']); */ ?>
			div id="row1" class="row selectrow">
				<div class="col-md-1 col-lg-1" style="width: 9.333%;">
				<phph $form->field($offermodel, 'offertext1')->radioList(['1'=>Yii::t('app','Buy')])->label(false); ?>
				</div>
				
				<div class="col-md-3 col-lg-3">			  
				<phph echo $form->field($offermodel, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				 <div class="col-md-1 col-lg-1">
					<phph Yii::t('app','get') ?>
				</div>
				 <div class="col-md-3 col-lg-3">			  
				<phph echo $form->field($offermodel, 'customOfferSelect2')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				 <div class="col-md-2 col-lg-2">			  
				<phph Yii::t('app','FREE') ?></div>
			</div-->
					
				<?php	}else{ ?>
						<?= $form->field($offermodel, 'offertype')->radioList(array('1'=>Yii::t('app','Offer Was/Now (Numeric Only in $)  OR '),'3'=>Yii::t('app','Offer Text (Free, OFF etc.)')),['id' => 'customoffer-id']); ?>
					
		
		<div id="offer-mumaric">						
			<label class="control-label" for="customoffer-id"><?php  Yii::t('app','Select Offer Was/Now (Numeric Only in $)') ?> </label>
				
		
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<?= $form->field($offermodel, 'offerwas')->textInput(['type'=>'number', 'min'=>'0','step'=>'0.01']); ?>
		   </div>
		   <div class="col-md-6 col-lg-6">
			
				<?= $form->field($offermodel, 'offernow')->textInput(['type'=>'number', 'min'=>'0','step'=>'0.01']); ?>	
		    </div>
		</div>
		
		</div>
			<?php } ?>
			
		<div class="custom-error"> 
			<?php if (strlen(Html::error($offermodel,'customOfferSelect1'))>0) { ?>
				<?php echo Html::error($offermodel,'customOfferSelect1', ['class' => 'help-block2']); ?>
				<br />
			<?php } ?>
		</div>	
		<div id="custom-offer-text" >
					 

			<label class="control-label" for="customoffer-id"><?= Yii::t('app','Choose 1 custom offer:') ?></label>
	    
			<div id="row1" class="row selectrow">
				<div class="col-md-1 col-lg-1" style="width: 9.333%;">
				<?= $form->field($offermodel, 'offertext1')->radioList(['1'=>Yii::t('app','Buy')])->label(false); ?>
				</div>
				 <div class="col-md-3 col-lg-3">			  
				<?php echo $form->field($offermodel, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				 <div class="col-md-1 col-lg-1">
					 <?= Yii::t('app','get') ?>
				</div>
				 <div class="col-md-3 col-lg-3">			  
				<?php echo $form->field($offermodel, 'customOfferSelect2')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				 <div class="col-md-2 col-lg-2">			  
				<?= Yii::t('app','FREE') ?></div>
			</div>
			<?php if($getcmpType->type != 2) { ?>
			<div id="row2" class="row selectrow">
				<div class="col-md-1 col-lg-1" style="width: 9.333%;">
				<?= $form->field($offermodel, 'offertext1')->radioList(['2'=>Yii::t('app','Get')])->label(false); ?>
				</div>
				 <div class="col-md-3 col-lg-3">			  
				<?php echo $form->field($offermodel, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				
				 <div class="col-md-2 col-lg-2">			  
				<?= Yii::t('app','% OFF') ?></div>
			</div>
			<div id="row3" class="row selectrow">
				<div class="col-md-1 col-lg-1" style="width: 9.333%;">
				<?= $form->field($offermodel, 'offertext1')->radioList(['3'=>Yii::t('app','Enjoy')])->label(false); ?>
				</div>
				 <div class="col-md-3 col-lg-3">			  
				<?php echo $form->field($offermodel, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				 <div class="col-md-1 col-lg-1"  style="padding-left:10px;">
						<?= Yii::t('app','% Off on') ?>
				</div>
				 <div class="col-md-3 col-lg-3">			  
				<?php echo $form->field($offermodel, 'customOfferSelect2')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				
			</div>
			<div id="row4" class="row selectrow">
				<div class="col-md-1 col-lg-1" style="width: 9.333%;">
				<?= $form->field($offermodel, 'offertext1')->radioList(['4'=>Yii::t('app','Buy')])->label(false); ?>
				</div>
				 <div class="col-md-3 col-lg-3">			  
				<?php echo $form->field($offermodel, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				 <div class="col-md-1 col-lg-1">
					<?= Yii::t('app','get') ?>
				</div>
				 <div class="col-md-3 col-lg-3">			  
				<?php echo $form->field($offermodel, 'customOfferSelect2')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				 <div class="col-md-2 col-lg-2">			  
				<?= Yii::t('app','% off') ?></div>
			</div>
			<!-- combo product offer -->
			
			<div id="row5" class="row selectrow">
				<div class="col-md-1 col-lg-1" style="width: 9.333%;">
				<?= $form->field($offermodel, 'offertext1')->radioList(['5'=>Yii::t('app','Buy')])->label(false); ?>
				</div>
				 <div class="col-md-3 col-lg-3">			  
				<?php echo $form->field($offermodel, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				 <div class="col-md-1 col-lg-1">
					<?= Yii::t('app','for $') ?>
				</div>
				 <div class="col-md-3 col-lg-3">			  
				<?php echo $form->field($offermodel, 'customOfferSelect2')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				
			</div>
			<?php if($checkbuy->buynow==1) {  ?>
			<div id="row5" class="row selectrow seltcombo">
				<div class="col-md-1 col-lg-1" style="width: 9.333%;">
				</div>
				<div id="select-combo-type" style="display:none;" class="col-md-3 col-lg-3">
					<?php echo $form->field($offermodel, 'comboProductType')->dropDownList(['1' => Yii::t('app','use above product'),'2' => Yii::t('app','Add another')], array('multiple'=>false,  'prompt' => Yii::t('app','select'))); ?>	
				</div>
	  
			<div id="Add-combo-product" style="display:none;">
				
				<div class="col-md-3 col-lg-3">
					
					<?php echo $form->field($offermodel, 'product_id_combo')->dropDownList($partnerProducts, array('multiple'=>false,  'prompt' => Yii::t('app','Select product ...'))); ?>	
					
				
				 </div>
				
				<div class="col-md-3 col-lg-3">
					<?= $form->field($offermodel, 'productprice')->textInput(['disabled'=>true, 'value' => '' ,'id'=>'workorderpopup-comboproductprice'])->label(Yii::t('app','Selected product price')) ?>
				</div>
			</div>
			
			</div>
			 <?php } ?>
			<div id="row5" class="row selectrow">
				<div id ="combo-get" class="col-md-1 col-lg-1">			  
						  
				<?= Yii::t('app','Get') ?> </div>
				 <div class="col-md-3 col-lg-3">			  
				<?php echo $form->field($offermodel, 'customOfferSelect3')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				<div class="col-md-1 col-lg-1">	
				<?= Yii::t('app','for $	') ?>
				</div>
				 <div class="col-md-3 col-lg-3">			  
				<?php echo $form->field($offermodel, 'customOfferSelect4')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>	 
				</div>
				</div>
				
				<?php  /* end workorder type for stamp card */ } 
				/* end rest workorder type for event ticket  */ } ?>
			
			
		</div>
				
	<?= $form->field($offermodel, 'iphoneNotificationText')->textarea(); ?>
	<?= $form->field($offermodel, 'readTerms')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'])->label(Yii::t('app','Read More')) ?>
		
	<?php
	 
	if($offermodel->logo!=''){
		
		$offermodel->imageFilelogo = $offermodel->logo;
		
		echo '<img src="'.$offermodel->logo.'"/>';
	} 
	
	?>
	<!-- preview image of logo -->
	<div id="previewImageLogo"></div>
	
	<!-- preview image of logo -->
		 	
		<?= $form->field($offermodel, 'imageFilelogo')->widget(Widget::className(), [
    'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
    	'width'=>132,
		'height'=>132]) ?>
	 
	<?php if($getcmpType->type==1 || $getcmpType->type==4) { ?>
	
	<?php echo $form->field($offermodel, 'uploadTypes')->radioList(['1' => Yii::t('app','Add image'), '2' => Yii::t('app','Add Video')]); ?>
				
	<?php
			echo '<div id="image-block" style="display:none;">';
				if($offermodel->photo!=''){
				  
					$offermodel->imageFile = $offermodel->photo;
					echo '<img src="'.$offermodel->photo.'"/>';
				 
				} 
	?>
	<!-- preview image of photo -->
	<div id="previewPhoto"></div>
	
	<!-- preview image of photo -->
	<?php
					echo $form->field($offermodel, 'imageFile')->widget(Widget::className(),[
					'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
					'width'=>640,
		'height'=>300]) ;
				
					echo '</div>';
	
			/*echo '<div id="video-block" style="display:none;">';
			echo $form->field($offermodel, 'videokey')->textInput();
			echo '</div>';*/
			
			echo '<div id="video-block-types" style="display:none;">';
			
			echo $form->field($offermodel, 'videoTypes')->radioList(['1' => Yii::t('app','Add Video Other/Video Url'), '2' => Yii::t('app','Add Video  Youtube/vimeo')]);
			echo $form->field($offermodel, 'browsercheck')->radioList(['1' => Yii::t('app','Yes'), '0' => Yii::t('app','No')]); 
			echo '</div>';
			echo'<div id="video-block-types-youtube" style="display:none;">';?>
			
			<?php echo $form->field($offermodel, 'videokey')->textInput();
			echo '</div>';
			echo'<div id="video-block-types-system" style="display:none;">'; ?>
			
			<?php
	 
			if(($offermodel->videoTypes==1) && $offermodel->videokey!='' && $offermodel->browsercheck==0 ){
		  
				$offermodel->videoWorkorderName = $offermodel->videokey; 
				$videoext = explode('.',$offermodel->videokey);
				if(isset($videoext[1]) && $videoext[1]=='mp4'){ ?>
		
				<video width="400" controls>
					<source src="<?php echo $offermodel->videoWorkorderName;?>" type="video/<?php if(isset($videoext[1])) { echo $videoext[1]; } ?>">
			 
				</video>
			
			<?php  
					}
				}
			?>
			<?php echo $form->field($offermodel, 'videoWorkorderName')->fileInput(); ?>
			
				OR
				
			<?php 
			
			$offermodel->videoWorkorderUrl = $offermodel->videokey;
			
			echo $form->field($offermodel, 'videoWorkorderUrl')->textInput()->label(Yii::t('app','Video Url'));?> 
			
			<?php
	 
				if(($offermodel->videoTypes==1) && $offermodel->videoImage!=''){
				
					$offermodel->videoWorkorderImage = $offermodel->videoImage;
					echo '<img src="'.$offermodel->videoImage.'"/>';
				} 
			?>
			<!-- preview image of videoImage -->
			<div id="previewVideoImage"></div>
			
			<!-- preview image of videoImage -->
					<?php	
				echo $form->field($offermodel, 'videoWorkorderImage')->widget(Widget::className(),[
					'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
					'width'=>640,
		'height'=>300]) ;
				echo '</div>';
			?>
	  
			<?php 
			
			}elseif($getcmpType->type==2 || $getcmpType->type==5){
				
				// Digital Stamp Card and Go to Store
				$offermodel->uploadTypes = '1';	
		        echo "<div style='display:none'>". $form->field($offermodel, 'uploadTypes')->radioList(['1' => Yii::t('app','Add image'), '2' => Yii::t('app','Add Video')])."</div>";
				
				
				
				echo '<div id="image-blocks">';
				
					if($offermodel->photo!=''){
				  
					$offermodel->imageFile = $offermodel->photo;
					echo '<img src="'.$offermodel->photo.'"/>';
				 
				} 
		 	
					echo $form->field($offermodel, 'imageFile')->widget(Widget::className(),[
					'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
					'width'=>640,
		'height'=>300]) ;
					echo '</div>';
			
			} 
		?>
	</div>
	

<!-- end deal store form edit  -->

   <?= $form->field($offermodel, 'passUrl')->textInput() ?>
	
	<input type="button" class="btn btn-primary" id="createurl" value="<?= Yii::t('app','Create SweetSpot passes') ?>"><br><br>	
	 
	<div id="ss_passes" style="display:none;">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-tasks"></i>  <?= Yii::t('app','Create SweetSpot passes') ?>  </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
     <div class="box-body">
		
		<?php $qrid = Qr::findOne(['qrkey' => $offermodel->passUrl]); ?>
		<?php if($qrid) { 
			printf('<label class="control-label" for="workorderpopup-eventdate">%s</label>',Yii::t('app','Create QR / Barcode'));
		$iframeSrc = sprintf('%s/qr/updateframe?id=%s&partnerId=%s',$baseurl,$qrid->id,$getcmpType->workorderpartner);
		echo '<iframe  width="100%" height="500"  src="'.$iframeSrc.'"></iframe>';
		}
		?>
		<?php 
		if($getcmpType->type == 4) {
		echo $form->field($offermodel, 'eventDate')->widget(
			DatePicker::className(), [
				// inline too, not bad
				 'inline' => false, 				 				
				'clientOptions' => [
					'autoclose' => true,
					 'startDate' => date('yyyy-mm'),
					'format' => 'yyyy-mm-dd'
				]
		]);
		
	echo $form->field($offermodel, 'ticketinfo' )->textInput()->label(Yii::t('app','Ticket info (entry / door number / seat number)'));
	
	
    }
    if($getcmpType->type == 2){
    
		 echo $form->field($offermodel, 'sspMoreinfo' )->textInput()->label(Yii::t('app','How many stamps on each card (excluding FREE):'));
				
		
	}else {
		echo $form->field($offermodel, 'sspMoreinfo')->widget(CKEditor::className(), [
			'options' => ['rows' => 6],
			'preset' => 'basic',
        
		]); 
     
		
      }
     ?>
	 </div>
		</div>
			</div>
			</div><!-- iframe panel end -->
   
     </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($offermodel->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['id'=>'update-offer-data','class' => $offermodel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

		
		$staturl =$baseurl.'/workorders/productprice';
		$staturlcombo =$baseurl.'/workorders/productpricecombo';
 
?>

<script type="text/javascript">
	
$(document).ready(function(){
	
	$("#workorderpopup-product_id").on("change", function () {	  
		var values = this.value;
		var id = $('#workorderpopup-campaignid').val();
		
			$.ajax({
			method: "POST",
			url: "<?php echo $staturl; ?>",
			data: { id: id, value: values }	
	
	})
	.done(function( result ) {			
		$('#workorderpopup-productprice').val(result);
		$("#workorderpopup-comboproducttype").val('');		
		$("#workorderpopup-product_id_combo").val('');		
		$("#workorderpopup-comboproductprice").val('');
		$("#workorderpopup-comboproducttype").trigger('change');		
	  });
		
});
/* get combo product price */
	$("#workorderpopup-product_id_combo").on("change", function(){	  
		var values = this.value;
		var id = $('#workorderpopup-campaignid').val();
		
			$.ajax({
			method: "POST",
			url: "<?php echo $staturlcombo; ?>",
			data: { id: id, value: values }	
	
	})
	.done(function( result ) {
			
	   $('#workorderpopup-comboproductprice').val(result);
	  });
		
		
	
	});
$("#workorderpopup-product_id ").trigger("change");		
$("#workorderpopup-product_id_combo ").trigger("change");		

});

$("#workorderpopup-comboproducttype").on("change", function () {	
	var values = this.value;
	var pval = document.getElementById("workorderpopup-product_id");	
	var pvalue = pval.options[pval.selectedIndex].value;
	//$("#workorderpopup-product_id_combo option[value="+pvalue+"]").remove();
	
});

$("#workorderpopup-product_id_combo").on("change", function () {	
	var values = this.value;	
	var pval = document.getElementById("workorderpopup-product_id");	
	var pvalue = pval.options[pval.selectedIndex].value;
	//$("#workorderpopup-product_id_combo option[value="+pvalue+"]").remove();
	if(values!=='' && pvalue!='' && values==pvalue){
		$(this).val('');		
		alert("You have already added this product above.");
	}

});


var combofferproduct = $( "#workorderpopup-comboproducttype option:selected" ).val();

	if(combofferproduct==2){
		document.getElementById("Add-combo-product").style.display = "block";	
	}else{
		document.getElementById("Add-combo-product").style.display = "none";	
	}

$("#workorderpopup-comboproducttype").on("change", function () {	  
		var values = this.value;
		if(values==2){
		document.getElementById("Add-combo-product").style.display = "block";		
		}else{
			document.getElementById("Add-combo-product").style.display = "none";		
		}
});

var getcombotype = $('input:radio[name="Workorderpopup[offertext1]"]:checked').val();
	
	if(getcombotype==5){
		document.getElementById("select-combo-type").style.display = "block";
				
	}else{
		document.getElementById("select-combo-type").style.display = "none";
		document.getElementById("Add-combo-product").style.display = "none";
			
	}
	

$("input:radio[name='Workorderpopup[offertext1]']").on("change", function () {	
	var values = this.value;
	if(values==5){
		document.getElementById("select-combo-type").style.display = "block";		
	}else{
		document.getElementById("select-combo-type").style.display = "none";
		document.getElementById("Add-combo-product").style.display = "none";		
	}


}); 


/* preview image on upload with info */

var _URL = window.URL || window.webkitURL;



function previewImage(input,boxId,imgW,imgH) {
	 
	 
    if (input.files && input.files[0]) {
		
        var reader = new FileReader();

        reader.onload = function (e) {
			
			$("#"+boxId).html('<img id="upload_preview" src="'+e.target.result+'" style="width:'+imgW+'px;height:'+imgH+'px" />');
			
           
        }

        reader.readAsDataURL(input.files[0]);
		
		
    }
}

$("#workorderpopup-imagefilelogo").change(function(){
	
	var file, img;
	var imgObj = this;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function() {
				previewImage(imgObj,'previewImageLogo',132,132);
				
				var width 	=	this.width;
				var height 	=	this.height;
				
				if(width!=132 && height!=132){
					
					alert('Please note an image may become distorted if it is not the correct shape.\nThis image size is '+width+'px X '+height+'px.');
				}
			};
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
				
			};
			img.src = _URL.createObjectURL(file);
	}
    
	
});

$("#workorderpopup-imagefile").change(function(){
	
	var file, img;
	var imgObj = this;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function() {
				previewImage(imgObj,'previewPhoto',640,300);
				
				var width 	=	this.width;
				var height 	=	this.height;
				
				if(width!=640 && height!=300){
					
					alert('Please note an image may become distorted if it is not the correct shape.\nThis image size is '+width+'px X '+height+'px.');
				}
			};
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
				
			};
			img.src = _URL.createObjectURL(file);
	}
    
	
});


$("#workorderpopup-videoworkorderimage").change(function(){
	
	var file, img;
	var imgObj = this;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function() {
				previewImage(imgObj,'previewVideoImage',640,300);
				
				var width 	=	this.width;
				var height 	=	this.height;
				
				if(width!=640 && height!=300){
					
					alert('Please note an image may become distorted if it is not the correct shape.\nThis image size is '+width+'px X '+height+'px.');
				}
			};
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
				
			};
			img.src = _URL.createObjectURL(file);
	}
	
});

/* preview image on upload with info */
</script>

<style>
	.box-footer a {    margin: 1px 5px;}
#combo-get {   font-weight: bold;    margin-left: 9px;    padding-left: 25px;    padding-right: 0;}
.error-summary {    border: 1px solid #cc0000;    color: #cc0000;    padding: 4px;}
#custom-offer-text .help-block {  display: none !important;}
.form-group.has-error .form-control {  border-color:#d2d6de !important;  box-shadow: none;}
.custom-error{color: #a94442;font-size: 17px;};
</style>

