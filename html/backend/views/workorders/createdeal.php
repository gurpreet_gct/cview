<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Deal */

$this->title = Yii::t('app','STEP 4: CREATE GET REQUEST    ');
?>
<section class="content">
  <div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class=" panel panel-primary">
		<div class="panel-heading">    
			<div class="pull-right">                       
			</div>
			<!-- Header Title-->
			<h3 class="panel-title">
			STEP 4: CREATE GET REQUEST           
			</h3>
			<!-- Header Title end -->
			<div class="clearfix"></div>
		</div>
		<div class="panel-before" >  
<div class="row">
<div class="col-sm-10 col-md-10 col-lg-10">
<h4 class="bg-info">Complete the fields below to create a GET request that will appear when user clicks on GET on a deal.</h4>
</div>
<div class="col-sm-2" style="margin-top:12px;">
  <div role="toolbar" class="btn-toolbar kv-grid-toolbar">
            <div class="btn-group"><a title="<?= Yii::t('app','All Workorders') ?>" href="<?=Yii::$app->urlManager->createUrl(['workorders/index',])?>" class="btn btn-primary"><i class="glyphicon glyphicon-tasks"></i></a> 
            <a data-pjax="0" title="<?= Yii::t('app','Reset Grid') ?>" href="<?=Yii::$app->urlManager->createUrl(['workorders/createdeal?id='.$_GET['id'],])?>" class="btn btn-default"><i class="glyphicon glyphicon-repeat"></i></a></div>


        </div>   
</div>

<div class="clearfix"></div>
</div>
</div>
		<!-- form start -->
		<?= $this->render('_formupdatedeal', [
		'dealmodel' => $model,
		]) 
		?>
		</div><!-- /.box -->
	</div><!--/.col (right) -->
  </div>   <!-- /.row -->
</section>
        
 


	
	
     
