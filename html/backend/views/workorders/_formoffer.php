<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Categories;
use backend\models\Beacongroup;
use common\models\Store;
use common\models\Workorderpopup;
use common\models\Workorders;
use common\models\Qr;
use common\models\Country;
use common\models\Product;
use common\models\Profile;
use backend\models\Beaconlocation;
use backend\models\Usermanagement;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use paras\cropper\Widget;
/* @var $this yii\web\View */
/* @var $model backend\models\Workorders */
/* @var $form yii\widgets\ActiveForm */
$baseurl = Yii::$app->getUrlManager()->getBaseUrl();
$getcmpType = workorders::find()->select(['type','workorderpartner'])->where(['id' => $_GET['id']])->one();
if(isset($getcmpType->workorderpartner)) {
	$partnerinfo = Profile::find()->select(['companyName','brandAddress'])
								->where(['user_id' => $getcmpType->workorderpartner])->one();
	/* GET partner info from profile model */
	if(isset($partnerinfo->companyName)){
		$partnerName = $partnerinfo->companyName;
	}else{
		$partnerName = Yii::t('app','Not Selected');
	}
	if(isset($partnerinfo->brandAddress)){
		$partnerAddress = $partnerinfo->brandAddress;
	}else{
		$partnerAddress = Yii::t('app','Not Selected');
	}
}
?>
<div class="workorders-form">
	<?php
		$form = ActiveForm::begin(['options' => ['enctype' =>'multipart/form-data']]);
	?>
	<div class="box-body">
	<div class="form-group">
	<?php echo $form->errorSummary($model);  ?>
	<!-- ######### Step 1 ###########-->
	<?php if($_GET['step']==2) { ?>
		<?= $form->field($model, 'iphoneNotificationText')->textarea(); ?>
	<?php }  else if($_GET['step']==3) { ?>
	<!--####################### Step 2   #################-->
	<!-- Change offer title as per workorder type  start -->
	<?php
	/* if stamp card  */
	if($getcmpType->type == 2){
		echo $form->field($model, 'offerTitle' )->textInput(['value' => $partnerName,'readonly' => true])->label(Yii::t('app','Advertiser name'));
		echo $form->field($model, 'partnerAddress' )->textInput(['value' => $partnerAddress,'readonly' => true])->label(Yii::t('app','Advertiser address'));
	} elseif($getcmpType->type == 4){
	/* if event ticket */
		echo $form->field($model, 'offerTitle' )->textInput()->label(Yii::t('app','Ticket event name:'));
		echo $form->field($model, 'eventVenue' )->textInput()->label(Yii::t('app','Event venue:'));
	}else{
	/* if voucher */
		echo $form->field($model, 'offerTitle')->textarea();
	}
	?>
<?php 
	if($getcmpType->type != 2){
		echo $form->field($workorderModel, 'buynow',[
		'template' => '<div class="row"><div class="col-sm-12">{input}{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Tick if you have added the item to the catalogue and want the deal to be available for purchase in-app. It is by default always available for purchase in-store.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span>{error}{hint}</div></div>'])->checkbox();
	}
?>
		
	<!-- Change offer title as per workorder type  end -->
		<?= $form->field($model, 'campaignID' )->hiddenInput(['value' => $_GET['id']])->label(false); ?>
	<!--  workorder type in popup form  -->
	<?= $form->field($model, 'wtype_pop' )->hiddenInput(['value' => $getcmpType->type])->label(false); ?>
	<?php	$checkbuy 		= Workorders::find()->select(['buynow','workorderpartner'])->where(['id' => $_GET['id']])->one();
	if($getcmpType->type==1) {
		// show product
		echo $form->field($model, 'product_id')->widget(Select2::classname(), [
		'data' => $partnerProducts,
		'options' => ['placeholder' => Yii::t('app','Select product ...')],
		'pluginOptions' => [
		'allowClear' => true
		],
		]);
	?>
	<?= $form->field($model, 'productprice')->textInput(['disabled'=>true, 'value' => '' ,'id'=>'workorderpopup-productprice'])->label(Yii::t('app','Selected product price')) ?>
	<?php }
	if($checkbuy->buynow==1) {
		echo $form->field($model, 'buynow' )->hiddenInput(['value' => 1])->label(false);
	}else {
		echo $form->field($model, 'buynow' )->hiddenInput(['value' => 0])->label(false);
	}
	?>
	<?php
	$tempstart = [];
	for ($x = 1; $x <= 1000; $x++) {
		$xf = $x;
		$tempstart[$x] = $xf;
	}
	?>
	<!-- Make offer type as per workorder type start -->
	<!-- if workorder type as event ticket -->
	<?php
	if($getcmpType->type == 4){
		$model->offertype = 4;
		echo $form->field($model, 'offertype')->radioList(array('4'=>Yii::t('app','Deal Text (Free, OFF etc.)')),['class' => 'customoffer-id']);
		echo '<label class="control-label" for="customoffer-id" style="display:none;">Deal Was/Now (Numeric Only in $) </label><div class="row">
	<div class="col-md-6 col-lg-6">';
		echo $form->field($model, 'offerwas')->textInput(['type'=>'number', 'min'=>'0','step'=>'0.01']);
		echo '</div> <div class="col-md-6 col-lg-6">';
		echo $form->field($model, 'offernow')->textInput(['type'=>'number', 'min'=>'0','step'=>'0.01']);
		echo '</div>';
	}else {
	/* rest for workorder type */
	if($getcmpType->type == 2){ ?>
	<div id="custom-offer-text" style="display:block;">
	<label class="control-label" for="customoffer-id"><?= Yii::t('app','Choose 1 custom offer:') ?></label>
	<div id="row1" class="row selectrow">
	<div class="col-md-1 col-lg-1" style="width: 9.333%;">
	<?= $form->field($model, 'offertext1')->radioList(['1'=>Yii::t('app','Buy')])->label(false); ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	<div class="col-md-1 col-lg-1">
	<?= Yii::t('app','get') ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect2')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	<div class="col-md-2 col-lg-2">
	<?= Yii::t('app','FREE') ?></div>
	</div>
	<?php	}else{ ?>	
	<?= $form->field($model, 'offertype')->radioList(array('1'=>Yii::t('app','Deal Was/Now (Numeric Only in $)  OR ') ,'3'=>Yii::t('app','Deal Text (Free, OFF etc.)')),['class' => 'customoffer-id',]); ?>
	<div id="offer-mumaric" style="display:none;">
	<div class="row">
	<div class="col-md-6 col-lg-6">
	<?= $form->field($model, 'offerwas')->textInput(['type'=>'number', 'min'=>'0','step'=>'0.01']); ?>
	</div>
	<div class="col-md-6 col-lg-6">
	<?= $form->field($model, 'offernow')->textInput(['type'=>'number', 'min'=>'0','step'=>'0.01']); ?>
	</div>
	</div>
	</div>
	<div class="custom-error">
	<?php if (strlen(Html::error($model,'customOfferSelect1'))>0) { ?>
	<?php echo Html::error($model,'customOfferSelect1', ['class' => 'help-block2']); ?>
	<br />
	<?php } ?>
	</div>
	<div id="custom-offer-text" style="display:none;">
	<label class="control-label" for="customoffer-id"><?= Yii::t('app','Choose 1 custom offer:') ?></label>
	<div id="row1" class="row selectrow">
	<div class="col-md-1 col-lg-1" style="width: 9.333%;">
	<?= $form->field($model, 'offertext1')->radioList(['1'=>Yii::t('app','Buy')])->label(false); ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	<div class="col-md-1 col-lg-1">
	<?= Yii::t('app','get') ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect2')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	<div class="col-md-2 col-lg-2">
	<?= Yii::t('app','FREE') ?></div>
	</div>
	<?php } /* rest for workorder type */ ?>
	<?php if($getcmpType->type != 2) { ?>
	<div id="row2" class="row selectrow">
	<div class="col-md-1 col-lg-1" style="width: 9.333%;">
	<?= $form->field($model, 'offertext1')->radioList(['2'=>Yii::t('app','Get')])->label(false); ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	<div class="col-md-2 col-lg-2">
	<?= Yii::t('app','% OFF') ?></div>
	</div>
	<div id="row3" class="row selectrow">
	<div class="col-md-1 col-lg-1" style="width: 9.333%;">
	<?= $form->field($model, 'offertext1')->radioList(['3'=>Yii::t('app','Enjoy')])->label(false); ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	<div class="col-md-1 col-lg-1"  style="padding-left:10px;">
	<?= yii::t('app','% Off on  ') ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect2')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	</div>
	<div id="row4" class="row selectrow">
	<div class="col-md-1 col-lg-1" style="width: 9.333%;">
	<?= $form->field($model, 'offertext1')->radioList(['4'=>Yii::t('app','Buy')])->label(false); ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	<div class="col-md-1 col-lg-1">
	<?= Yii::t('app',' get') ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect2')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	<div class="col-md-2 col-lg-2">
	<?= Yii::t('app','% off') ?></div>
	</div>
	<!-- combo product offer -->
	<div id="row5" class="row selectrow">
	<div class="col-md-1 col-lg-1" style="width: 9.333%;">
	<?= $form->field($model, 'offertext1')->radioList(['5'=>Yii::t('app','Buy')])->label(false); ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect1')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	<div class="col-md-1 col-lg-1">
	<?= Yii::t('app','for $') ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect2')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	</div>
	<?php if($checkbuy->buynow==1) {  ?>
	<div id="row5" class="row selectrow seltcombo">
	<div class="col-md-1 col-lg-1" style="width: 9.333%;">
	</div>
	<div id="select-combo-type" style="display:none;" class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'comboProductType')->dropDownList(['1' => Yii::t('app','use above product'),'2' => Yii::t('app','Add another')], array('multiple'=>false,  'prompt' => Yii::t('app','select'))); ?>
	</div>
	<div id="Add-combo-product" style="display:none;">
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'product_id_combo')->dropDownList($partnerProducts, array('multiple'=>false,  'prompt' => Yii::t('app','Select product ...'))); ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?= $form->field($model, 'productprice')->textInput(['disabled'=>true, 'value' => '' ,'id'=>'workorderpopup-comboproductprice'])->label(Yii::t('app','Selected product price')) ?>
	</div>
	</div>
	</div>
	<?php } ?>
	<div id="row5" class="row selectrow">
	<div id ="combo-get" class="col-md-1 col-lg-1">
	<?= Yii::t('app','Get') ?> </div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect3')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	<div class="col-md-1 col-lg-1">
	<?= Yii::t('app','for $') ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?php echo $form->field($model, 'customOfferSelect4')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false); ?>
	</div>
	</div>
	<?php  /* end workorder type for stamp card */ }
	/* end rest workorder type for event ticket  */
	}
	?>
	</div>
	<!-- ################# step 3 ###################################### -->
	<?php if ($getcmpType->type == 1 || $getcmpType->type == 4 || $getcmpType->type == 2) { ?>
		<div class="deal logo-img">
				<?php
				if($model->logo!=''){
				$model->imageFilelogo = $model->logo;
				echo '<img src="'.$model->logo.'"/>';
				}
				?>
				<!-- preview image of logo -->
				<div id="previewImageLogo"></div>
				<!-- preview image of logo -->
				<?= $form->field($model, 'imageFilelogo')->widget(Widget::className(),
				['uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
				'width'=>132,
				'height'=>132]) ?>
			</div>
			<?php } ?>
		<?php if ($getcmpType->type == 1 || $getcmpType->type == 4) { ?>	
	<div class="dealModelImages">
			<?php echo $form->field($dealmodel, 'dealuploadTypes')->radioList(['1' => Yii::t('app','Add  image'), '2' => Yii::t('app','Add Video')]); ?>
		 <?php
			 echo '<div id="deal-image-block" style="display:none;">';
				 if($dealmodel->mainPhotoUpload!=''){
					 $dealmodel->imageFile3 = $dealmodel->mainPhotoUpload;
					//echo '<img  id="imagedealmainPhotoUpload" width="300" height="120" src="'.$dealmodel->mainPhotoUpload.'"/>';
					} ?>
				 <!-- preview image of image -->
				 <div id="previewImage"></div>
				 <!-- preview image of image -->
			 <?php echo $form->field($dealmodel, 'imageFile3')->widget(Widget::className(), [
		 'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
			 'width'=>750,
		 'height'=>360]) ;
			 echo '</div>';
			 echo '<div id="deal-video-block-types" style="display:none;">';
			 echo $form->field($dealmodel, 'dealvideoTypes')->radioList(['1' => Yii::t('app','Add Video File/Video URL'), '2' => Yii::t('app','Add YouTube Video/Vimeo video')]);
			 echo $form->field($dealmodel, 'dealbrowsercheck')->radioList(['1' => Yii::t('app','Yes'), '0' => Yii::t('app','No')])->label(Yii::t('app','Open Video In Outside Browser'));
			 echo '</div>';
			 echo'<div id="deal-video-block-types-youtube" style="display:none;">';
			 echo $form->field($dealmodel, 'dealvideokey')->textInput()->label(Yii::t('app','Deal Video'));
			 echo '</div>';
			 echo'<div id="deal-video-block-types-system" style="display:none;">';
			 ?>
			 <?php
				if(($dealmodel->dealvideoTypes==1) && $dealmodel->dealvideokey!='' ){
				 $dealmodel->dealvideoWorkorderName = $dealmodel->dealvideokey;
				 $videoext = explode('.',$dealmodel->dealvideokey);
				 if(isset($videoext[1]) && $videoext[1]=='mp4') {
					 ?>
			 <video width="400" controls>
			 <source src="<?php echo $dealmodel->dealvideoWorkorderName;?>" type="video/<?php if(isset($videoext[1])) { echo $videoext[1]; } ?>">
			 </video>
		 <?php  }
				}
			 ?>
			 <?php echo $form->field($dealmodel, 'dealvideoWorkorderName')->fileInput()->label(Yii::t('app','Video Upload')); ?>
			 <?= Yii::t('app','OR') ?>
			 <?php
			 $dealmodel->dealvideoWorkorderUrl = $dealmodel->dealvideokey;
			 echo $form->field($dealmodel, 'dealvideoWorkorderUrl')->textInput()->label(Yii::t('app','Video Url')); ?>
			 <?php
			 if(($dealmodel->dealvideoTypes==1) && $dealmodel->dealvideoImage!=''){
			 $dealmodel->dealvideoWorkorderImage = $dealmodel->dealvideoImage;
			 echo '<img id="imagedealvideoImage" src="'.$dealmodel->dealvideoImage.'"/>';
			} ?>
			 <!-- preview image -->
			 <div id="previewDealImage"></div>
			 <!-- preview image -->
			 <?php echo $form->field($dealmodel, 'dealvideoWorkorderImage')->widget(Widget::className(), [
		 'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
			 'width'=>132,
		 'height'=>132])->label(Yii::t('app','Deal Video Image'));
				echo '</div>';
			 ?>
	</div>
	<?php } ?>
	<!-- ############## deal image end here ################## -->
	<!-- end deal store form edit  -->
	<?php
		if($getcmpType->type == 4) {
			echo $form->field($model, 'eventDate')->widget(
			DatePicker::className(), [
									'inline' => false,
										'clientOptions' => [
										'autoclose' => true,
										'startDate' => date('yyyy-mm'),
										'format' => 'yyyy-mm-dd'
										]
								]);
			echo $form->field($model, 'ticketinfo' )->textInput()->label(Yii::t('app','Ticket info (entry / door number / seat number)'));
		}
	?>
<!--####################### Step 4  #################-->
<?php } else if($_GET['step']==4) { ?>
<div class="step-4-wrapper">			
			<?php if($getcmpType->type == 1 || $getcmpType->type == 4) { ?>
				<?php echo $form->field($model, 'uploadTypes')->radioList([1 => Yii::t('app','Add image'), '2' => Yii::t('app','Add Video')]); ?>
				<?php
					echo '<div id="image-block" style="display:none;">';
				if($model->photo!=''){
					$model->imageFile = $model->photo;
					echo '<img src="'.$model->photo.'"/>';
				}
				?>
			
				
				<!-- preview image of photo -->
				<div id="previewPhoto"></div>
				<!-- preview image of photo -->
				<?php
					echo $form->field($model, 'imageFile')->widget(Widget::className(),
					['uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
					'width'=>640,
					'height'=>300]);
					echo '</div>';
					echo '<div id="video-block-types" style="display:none;">';
					echo $form->field($model, 'videoTypes')->radioList(['1' => Yii::t('app','Add Video Other/Video Url'), '2' => Yii::t('app','Add Video  Youtube/vimeo')]);
					$model->browsercheck = '0';
					echo $form->field($model, 'browsercheck')->radioList(['1' => Yii::t('app','Yes'), '0' => Yii::t('app','No')]);
					echo '</div>';
					echo'<div id="video-block-types-youtube" style="display:none;">';
					echo $form->field($model, 'videokey')->textInput();
					echo '</div>';
					echo'<div id="video-block-types-system" style="display:none;">';
					if($model->videoName!=''){
						echo  $model->videoWorkorderName = $model->videoName;
					}
				?>
				<?php
					echo $form->field($model, 'videoWorkorderName')->fileInput();
						echo 'OR';
					 echo $form->field($model, 'videoWorkorderUrl')->textInput()->label(Yii::t('app','Video Url'));
					if($model->videoImage!=''){
						$model->videoWorkorderImage = $model->videoImage;
						echo '<img src="'.$offermodel->videoImage.'"/>';
					}
				?>
				<!-- preview image of videoImage -->
				<div id="previewVideoImage"></div>
				<!-- preview image of videoImage -->
				<?php
						echo $form->field($model, 'videoWorkorderImage')->widget(Widget::className(),
						['uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
						'width'=>640,
						'height'=>300]);
						echo '</div>';
				?>
			<?php
				} elseif($getcmpType->type == 2 || $getcmpType->type == 5){
				$model->uploadTypes=1;
				echo "<div style='display:none'>". $form->field($model, 'uploadTypes')->radioList(['1' => Yii::t('app','Add image'), '2' => Yii::t('app','Add Video')])."</div>";
				echo '<div id="image-block" style="display:block;">';
				if($model->photo!=''){
					$model->imageFile = $model->photo;
					echo '<img src="'.$model->photo.'"/>';
				}
					echo $form->field($model, 'imageFile')->widget(Widget::className(),
					['uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
					'width'=>640,
					'height'=>300]);
					echo '</div>';
			}
		?>
		<?= $form->field($model, 'readTerms')->widget(CKEditor::className(), [
		'options' => ['rows' => 6],
		'preset' => 'basic',
		])->label(Yii::t('app','Read More')) ?>
</div>
	<!--####################### Step 5   #################-->
	<?php } else if($_GET['step']==5) {
			if($getcmpType->type == 2){
				echo $form->field($model, 'sspMoreinfo' )->textInput()->label(Yii::t('app','How many stamps on each card (excluding FREE):'));
			}else {
					echo $form->field($model, 'sspMoreinfo')->widget(CKEditor::className(), [
				'options' => ['rows' => 6],
				'preset' => 'basic',
				]);
			}
	?>
	<?= $form->field($model, 'passUrl')->textInput() ?>
	<input type="button" class="btn btn-primary" id="createurl" value="<?= Yii::t('app','Create QR Code / Barcode') ?>">
	<br /><br />
	<div id="ss_passes" style="display:none;">
	<div class="col-md-12">
	<div class="panel panel-success">
	<div class="panel-heading">
	<div class="pull-right">
	</div>
	<!-- Header Title-->
	<h3 class="panel-title">
	<i class="glyphicon glyphicon-tasks"></i><?= Yii::t('app','Create QR Code / Barcode') ?></h3>
	<!-- Header Title end -->
	<div class="clearfix"></div>
	</div>
	<div class="box-body">
	<?php
	echo '<label class="control-label" for="workorderpopup-eventdate">Create QR / Barcode</label>';
	echo '<iframe  width="100%" height="500"  src="'.$baseurl.'/qr/createframe?partnerId='.$getcmpType->workorderpartner.'"></iframe>';
	?>
	</div>
	</div>
	</div>
	</div><!-- iframe panel end -->
	<?php } ?>
	</div><!-- /.box-body -->
	<div class="box-footer">
	<div class="form-group">
	<?php
	$woType = $getcmpType->type;
	if($_GET['step']==2) {
	echo Html::a('BACK',['update','id'=>$_GET['id']],['class'=>'btn btn-default','id'=>1]);
} else if($_GET['step']==3) {
	echo Html::a('BACK',['updateoffer','id'=>$_GET['id'],'step'=>2],['class'=>'btn btn-default','id'=>1]);
	}
	else if($_GET['step']==4) {
		echo Html::a('BACK',['updateoffer','id'=>$_GET['id'],'step'=>3],['class'=>'btn btn-default','id'=>1]);
	}	 else if($_GET['step']==5 && ($woType==4 || $woType==1)) {
		echo Html::a('BACK',['updateoffer','id'=>$_GET['id'],'step'=>4],['class'=>'btn btn-default','id'=>1]);
	} else if($_GET['step']==5 && $woType==2) {
	echo Html::a('BACK',['updateoffer','id'=>$_GET['id'],'step'=>4],['class'=>'btn btn-default','id'=>1]);
	}
	echo Html::a('SAVE AND EXIT',['create'],['class'=>'btn btn-primary progressbtn','id'=>1]) ;
	if($_GET['step']!=5) {
		$val = 2;//$_GET['step'] ==2 ? 4 : 2;
		echo Html::a('NEXT',['create'],['class'=>'btn btn-success progressbtn','id'=>$val]);
	}
	?>
	<div class="hidden">
	<?= Html::submitButton('submit', ['class' =>'submitme']) ?>
	</div>
	</div>
	</div>
	<input type="hidden" id="savenew" name="savenew" value="" />
	<?php ActiveForm::end(); ?>
	</div>
	</div>
</div>
<?php
$staturl =$baseurl.'/workorders/productprice';
$staturlcombo =$baseurl.'/workorders/productpricecombo';
?>
<script type="text/javascript">
$('.progressbtn').click(function(e){
e.preventDefault();
$('#savenew').val($(this).attr('id'));
var step = "<?= $_GET['step'] ; ?>";
if(step==3){
var offertext1 = $('input:radio[name="Workorderpopup[offertext1]"]:checked').val();
$("input:hidden[name='Workorderpopup[offertext1]']").val(offertext1);
}
//$('#w0').submit();
$('.submitme').click();
});
$(document).ready(function(){
$("#workorderpopup-product_id").on("change", function () {
var values = this.value;
var id = $('#workorderpopup-campaignid').val();
$.ajax({
method: "POST",
url: "<?php echo $staturl; ?>",
data: { id: id, value: values }
})
.done(function( result ) {
$('#workorderpopup-productprice').val(result);
$("#workorderpopup-comboproducttype").val('');
$("#workorderpopup-product_id_combo").val('');
$("#workorderpopup-comboproductprice").val('');
$("#workorderpopup-comboproducttype").trigger('change');
});
});
/* get combo product price */
$("#workorderpopup-product_id_combo").on("change", function () {
var values = this.value;
var id = $('#workorderpopup-campaignid').val();
$.ajax({
method: "POST",
url: "<?php echo $staturlcombo; ?>",
data: { id: id, value: values }
})
.done(function( result ) {
$('#workorderpopup-comboproductprice').val(result);
});
});
$("#workorderpopup-product_id ").trigger("change");
$("#workorderpopup-product_id_combo ").trigger("change");
});
$("#workorderpopup-comboproducttype").on("change", function () {
var values = this.value;
var pval = document.getElementById("workorderpopup-product_id");
var pvalue = pval.options[pval.selectedIndex].value;
});
$("#workorderpopup-product_id_combo").on("change", function () {
var values = this.value;
var pval = document.getElementById("workorderpopup-product_id");
var pvalue = pval.options[pval.selectedIndex].value;
if(values!=='' && pvalue!='' && values==pvalue){
$(this).val('');
alert("You have already added this product above.");
}
});
var combofferproduct = $( "#workorderpopup-comboproducttype option:selected" ).val();
if(combofferproduct==2){
$("#Add-combo-product").css({'display':"block"});
}else{
$("#Add-combo-product").css({'display':"none"});
}
$("#workorderpopup-comboproducttype").on("change", function () {
var values = this.value;
if(values==2){
$("#Add-combo-product").css({'display':"block"});
}else{
$("#Add-combo-product").css({'display':"none"});
}
});
var getcombotype = $('input:radio[name="Workorderpopup[offertext1]"]:checked').val();
if(getcombotype==5){
$("#select-combo-type").css({'display':"block"});
}else{
$("#select-combo-type").css({'display':"none"});
$("#Add-combo-product").css({'display':"none"});
}
$("input:radio[name='Workorderpopup[offertext1]']").on("change", function () {
var values = this.value;
if(values==5){
$("#select-combo-type").css({'display':"block"});
}else{
$("#select-combo-type").css({'display':"none"});
$("#Add-combo-product").css({'display':"none"});
}
});
/* preview image on upload with info */
var _URL = window.URL || window.webkitURL;
function previewImage(input,boxId,imgW,imgH) {
if (input.files && input.files[0]) {
var reader = new FileReader();
reader.onload = function (e) {
$("#"+boxId).html('<img id="upload_preview" src="'+e.target.result+'" style="width:'+imgW+'px;height:'+imgH+'px" />');
}
reader.readAsDataURL(input.files[0]);
}
}
$("#workorderpopup-imagefilelogo").change(function(){
var file, img;
var imgObj = this;
if ((file = this.files[0])) {
img = new Image();
img.onload = function() {
previewImage(imgObj,'previewImageLogo',132,132);
var width 	=	this.width;
var height 	=	this.height;
if(width!=132 && height!=132){
alert('Please note an image may become distorted if it is not the correct shape.\nThis image size is '+width+'px X '+height+'px.');
}
};
img.onerror = function() {
alert( "not a valid file: " + file.type);
};
img.src = _URL.createObjectURL(file);
}
});
$("#workorderpopup-imagefile").change(function(){
var file, img;
var imgObj = this;
if ((file = this.files[0])) {
img = new Image();
img.onload = function() {
previewImage(imgObj,'previewPhoto',640,300);
var width 	=	this.width;
var height 	=	this.height;
if(width!=640 && height!=300){
alert('Please note an image may become distorted if it is not the correct shape.\nThis image size is '+width+'px X '+height+'px.');
}
};
img.onerror = function() {
alert( "not a valid file: " + file.type);
};
img.src = _URL.createObjectURL(file);
}
});
$("#workorderpopup-videoworkorderimage").change(function(){
var file, img;
var imgObj = this;
if ((file = this.files[0])) {
img = new Image();
img.onload = function() {
previewImage(imgObj,'previewVideoImage',640,300);
var width 	=	this.width;
var height 	=	this.height;
if(width!=640 && height!=300){
alert('Please note an image may become distorted if it is not the correct shape.\nThis image size is '+width+'px X '+height+'px.');
}
};
img.onerror = function() {
alert( "not a valid file: " + file.type);
};
img.src = _URL.createObjectURL(file);
}
});
/* preview image on upload with info */
</script>
<style>
h4.bg-info {font-size:14px; line-height: 1.4; padding: 1px 5px; text-align: justify;}
.information {  background-color: #006dd5;  border-radius: 9px;  color: #fff;
font-size: 12px;  padding:1px;}
.box-footer a {    margin: 1px 5px;}
.box-footer a {    margin: 1px 5px;}
#combo-get {    font-weight: bold;    margin-left: 9px;    padding-left: 25px;    padding-right: 0;
}
.error-summary {    border: 1px solid #cc0000;    color: #cc0000;    padding: 4px;
}
#custom-offer-text .help-block {  display: none !important;}
.form-group.has-error .form-control {  border-color:#d2d6de !important;  box-shadow: none;}
.custom-error{color: #a94442;font-size: 17px;};
</style>
