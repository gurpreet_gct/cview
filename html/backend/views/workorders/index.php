<?php
 
use yii\helpers\Html;
#use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;
use common\models\Usertype;
use kartik\select2\Select2;
use kartik\daterange\DateRangePicker;
use backend\models\Usermanagement;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkordersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
 
$this->title = Yii::t('app','Workorders');
$this->params['breadcrumbs'][] = $this->title;
 
/* get login user id */
$usertype = Yii::$app->user->identity->user_type;    
 
$campaigntype = array('1' =>Yii::t('app','Voucher'), '2' =>Yii::t('app','Digital Stamp Card'), '4' =>Yii::t('app','Event ticket'), '5' =>Yii::t('app','Go to Store')); 

$partners = ArrayHelper::map(Usermanagement::find() ->joinwith('workorders',false,'INNER JOIN')->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
 
?>
 
 
<div class="workorder-index">
 
<?php

if(isset($_GET['WorkordersSearch']['dateFrom'])){
 
$selected = $_GET['WorkordersSearch']['dateFrom'];
}else {
$selected = 'Jan 01, 2016 to Feb 20, 2016';    
 
}
 

if(isset($_GET['WorkordersSearch']['dateFrom'])){

$selected = $_GET['WorkordersSearch']['dateFrom'];
}else {
$selected = 'Jan 01, 2016 to Feb 20, 2016';	

}
        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
                       return ['id' => $model['id']];
                       },
        'columns'=>  [        
            [           
            'attribute'=>Yii::t('app','offerTitle'),
            'value'=>'offertitlename',
            ],
            
            'name',
           
             [
             'label' => 'Partner',
			 'visible'=>$usertype==1 ? 1: 0,
				'attribute'=>Yii::t('app','workorderpartner'),           			
				'value'=> 'partnernameval',                   
                'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $partners,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => 'Select partner'],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

            ],
            ],
            [
            'attribute'=>Yii::t('app','type'),           			
			'value'=> 'campaigntypename',                   
                'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $campaigntype,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => 'Select campaign type'],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

            ],
            ],
            
            'duration',
                      
            [
            'attribute'=>'dateFrom',           			
			 'value'=> 'dateFrom',                   
                  'filterType'=>GridView::FILTER_DATE_RANGE,
                 'filterWidgetOptions' => [
				'convertFormat'=>true,
				//'useWithAddon'=>true,
				'pluginOptions'=>[
					'locale'=>[
						  'format'=>'Y-m-d',
						   'separator'=>' - ',
						   
					],
					 'opens'=>'left'
					],

            ],
            ],
            
            [
            'attribute'=>'dateTo',           			
			 'value'=> 'dateTo',                   
                  'filterType'=>GridView::FILTER_DATE_RANGE,
                 'filterWidgetOptions' => [
				'convertFormat'=>true,
				//'useWithAddon'=>true,
				'pluginOptions'=>[
					'locale'=>[
						  'format'=>'Y-m-d',
						   'separator'=>' - ',
						  
					],
					 'opens'=>'left'
					],

            ],
            ],        
            
            [
            'attribute'=>'status',           			
		//	'value'=> 'statustype',                   
			'filter'=> array('1' => Yii::t('app','Active'), '2' => Yii::t('app','Submitted'),'3'=>Yii::t('app','Approved'),'4'=>Yii::t('app','Completed'),'0'=>Yii::t('app','Rejected') ),
			'format' => 'raw',
             'value'=>function ($model, $key, $index, $widget) {
			if($model->offertitlename!='N/A' && $model->offertitlename!='' && $model->type!=1 && $model->type!=4){
				return \yii\helpers\Html::dropDownList("status", $model->status, [1=>Yii::t('app','Active'), 2=>Yii::t('app','Submitted'),3=>Yii::t('app','Approved'),4=>Yii::t('app','Completed'),],['prompt'=>Yii::t('app','Select'),'id'=>'updatestatus'.$model->id,'onchange'=>'setstatus('.$model->id.')']);
			}
			else if($model->offertitlename!='N/A' && $model->offertitlename!='' && $model->dealid==true){
				return \yii\helpers\Html::dropDownList("status", $model->status, [1=>Yii::t('app','Active'), 2=>Yii::t('app','Submitted'),3=>Yii::t('app','Approved'),4=>Yii::t('app','Completed'),],['prompt'=>Yii::t('app','Select'),'id'=>'updatestatus'.$model->id,'onchange'=>'setstatus('.$model->id.')']);
			}
			else if($model->offertitlename!='N/A' && $model->offertitlename!='' && $model->type==1 &&  $model->dealid==true){
					return \yii\helpers\Html::dropDownList("status", $model->status, [1=>Yii::t('app','Active'), 2=>Yii::t('app','Submitted'),3=>Yii::t('app','Approved'),4=>Yii::t('app','Completed'),],['prompt'=>Yii::t('app','Select'),'id'=>'updatestatus'.$model->id,'onchange'=>'setstatus('.$model->id.')']);
			}
			else{
				return 'Draft';
			}
          
           },   
            ],
                    
            
          [  
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => ['style' => 'width:260px;'],
        'header'=>'Actions',
        'template' => '{export} {copy} {view}  {delete}',
        'buttons' => [
			
			 //copy botton
            'export' => function ($url, $model) {
               if($model->type ==2){     
         
			}
            },
           
         
            'copy' => function ($url, $model) {
                
                 return '<button onclick="copyworkorderdata(this)" title="Copy" url="'.$url.'" class="glyphicon-export-copy" id="'.$model->id.'">
                <span class="glyphicon glyphicon-copy"></span></button> <button type="button" id="show-popup-model" class="btn btn-info btn-lg" style="display:none" data-toggle="modal" data-target="#myModal">Open Modal</button>';
                
            },
            
            //view button
            'view' => function ($url, $model) {
               
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'',
                           
                                                             
                ]);
            },
            
              //delete button
            'delete' => function ($url, $model) {
               
               return '<a class="custom-del">    <span class="glyphicon glyphicon-trash" OnClick="DeleteAction('.$model->id.')" >  </span>
                               </a> <input type="hidden" name="url" value="'.$url.'" id="url"'; 
            },
             
        ],
 
        'urlCreator' => function ($action, $model, $key, $index) {
           
           $baseurl = Yii::$app->getUrlManager()->getBaseUrl(); 
           if((isset($model->workorderType)) && $model->workorderType==2) {
               
           if ($action === 'copy') {
                $url =$baseurl.'/workorders/copypsa';
                return $url;
           }
           
           if ($action === 'view') {
                $url =$baseurl.'/workorders/viewpsa?id='.$model->id;
                return $url;
           }
       
           
           if ($action === 'delete') {                    
                $url =$baseurl.'/workorders/delete';
                return $url;
           }
               
          }else{
			  
            if($model->type == 2){
			if ($action === 'export') {
                $url =$baseurl.'/workorders/exportqr?id='.$model->id;
                return $url;
           }    
			
			}  
             
           if ($action === 'copy') {
                $url =$baseurl.'/workorders/copy';
                return $url;
           }    
             
            if ($action === 'view') {
                $url =$baseurl.'/workorders/view?id='.$model->id;
                return $url;
           }
                
                           
           if ($action === 'delete') {                    
                $url =$baseurl.'/workorders/delete';
                return $url;
           }
       }
   }
 
       ],
           
           
        
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [
               
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
           
             ['content'=>
               Html::a('<i class="glyphicon glyphicon-plus">Geolocation</i>', ['createzone'],[ 'title'=>Yii::t('app','Geolocation'),'data-pjax'=>1, 'class'=>'btn btn-primary', ]) . ' '.
			   Html::a('<i class="glyphicon glyphicon-plus">PSA</i>', ['createpsa'],[ 'title'=>Yii::t('app','Add PSA'),'data-pjax'=>1, 'class'=>'btn btn-warning', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-plus">WorkOrder</i>', ['create'],[ 'title'=>Yii::t('app','Add Workorder'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
		'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-tasks"></i>'.Yii::t('app',' Workorders'),
            'sort_order' => true,
        ],
        'persistResize'=>false,
        'exportConfig'=>['pdf'=>'pdf','csv'=>'csv' ],
    ]);

 
?>

<?php $baseurl = Yii::$app->getUrlManager()->getBaseUrl(); 
      $staturl =$baseurl.'/workorders/setstatus';
      $copydata = $baseurl.'/workorders/copy';
  
?>

 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Add New Campaign Name') ?></h4>
        </div>
        <div class="modal-body">
         <form id="fromcamp" enctype="multipart/form-data" method="get" action="#">
            
            <div class="form-group field-workorderpopup-passurl required">
           <label class="control-label" for="workorderpopup-passurl"><?= Yii::t('app','Campaign Name') ?></label>
           <input  class="form-control" type="text" value="" name="camname" required>
           <div class="help-block"></div>
           <input type="hidden" name="id" id="workid" value=""/>
           </div>
          
          <input class="btn btn-default" type="submit" name="submit" value="Submit" />
          </form> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Close') ?></button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
 

<script type="text/javascript">
   $('.skip-export.kv-align-center.kv-align-middle >').click(function(){
	   
	  
      
       
});

function copyworkorderdata(obj){
	
        $('#show-popup-model').click();
        
     var id = obj.getAttribute("id");
       $('#workid').val(id);
        var href = obj.getAttribute("url");
       
       $('#fromcamp').attr('action',href);

}
function setstatus(id){
   
   var values = $('#updatestatus'+id).val();
 
   var updateconfirm = confirm ("Are you sure to update status?");
       
   if (updateconfirm) {        
   $.ajax({
   method: "POST",
    url: "<?php echo $staturl; ?>",
   data: { id: id, value: values }
   
   
   })
   .done(function( result ) {
       
     //$( "#"+id ).fadeOut( "slow");
     });
 
}
 
   
}

 function exportqrAction(id){	

	 var win = window.open($('#'+id).attr('url'), '_blank');	
		win.focus();	
 
} 



$(document.body).on("click", "#w0",function() {
	
 //$('#w0').daterangepicker();
 return true;

});
</script>

<style>
	
	.glyphicon-export-copy {
	border: medium none;
    color: #3c8dbc;
    padding: 0;
}
</style>

 

