<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use backend\models\Beacongroup;
use common\models\Store;
use common\models\Country;
use backend\models\Beaconlocation;
use backend\models\Usermanagement;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;
use kartik\select2\Select2;
use kartik\time\TimePicker;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model backend\models\Workorders */
/* @var $form yii\widgets\ActiveForm */

$baseurl 		= 	Yii::$app->getUrlManager()->getBaseUrl(); 
$createzoneUrl 	=	$baseurl.'/workorders/createzone';
$display 		= 	($model->geoProximity==3 || $model->geoProximity==1)?'block':'none';

?>
 
<div class="workorders-form">
 
    <?php
 
    $form = ActiveForm::begin([
                'options' => [                 
                    'enctype' =>'multipart/form-data'
                ]
    ]);
    ?>
    
    <div class="box-body">
      <div class="form-group">
    <?php //echo $form->errorSummary($model); ?>
    
    <?php   $country_name =  ArrayHelper::map(Country::find()->all(), 'id_countries', 'name'); ?>
    
   
    <?= $form->field($model, 'countryID')->dropDownList($country_name, array('prompt'=>Yii::t('app','Select'))) ?>
    
  <?php  
    
     if(Yii::$app->user->identity->user_type==1){
           $partner =  ArrayHelper::map(Usermanagement::find()->where(['user_type' => 7])->andWhere(['status' => 10])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');?>
           <?= $form->field($model, 'workorderpartner')->dropDownList($partner, array('prompt'=>Yii::t('app','Select Partner'))); ?>
   <?php }else{
         $usertype = Yii::$app->user->identity->user_type;
			$userId = $usertype==7?Yii::$app->user->id : Yii::$app->user->identity->advertister_id; ?>
			<?= $form->field($model, 'workorderpartner')->hiddenInput(['value' =>$userId])->label(false) ?>
       <?php }
    ?>     
       
    
    
   <?= $form->field($model, 'buynow')->checkbox(); ?>
   
   <?= $form->field($model, 'mediaContractNumber')->textInput(['maxlength' => true]) ?> 
   
 
     
    <?=  $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
 
	<?= $form->field($model, 'type')->dropDownList(array('1' =>Yii::t('app','Voucher'), '2' =>Yii::t('app','Digital Stamp Card'), '4' =>Yii::t('app','Event ticket'), '5' =>Yii::t('app','Go to Store')),[ 'prompt' => ' -- Select --']) ?>
   
     <?= $form->field($model, 'geoProximity')->dropDownList(array('1' =>Yii::t('app','Both'), '2' =>Yii::t('app','Proximity'), '3' =>Yii::t('app','Geolocation')),[ 'prompt' => Yii::t('app',' -- Select --')]) ?> 
	 
	<!-- added for geozone start -->

	<a id='show-geopopup-model' class="btn btn-info btn-small" style="width:15%;display:<?php echo $display;?>;" href="<?php echo $createzoneUrl.'?id='.$model->workorderpartner.'&did='.$model->id;?>" target="_blank"><?= Yii::t('app','Geolocation') ?></a>
   
	<?= $form->field($model, 'noPasses')->textInput(['maxlength' => true]) ?>
	<?php 
		$arr = array();
	   for ($x = 1; $x <= 50; $x++) {
		 $arr[$x] = $x;
	   }
   ?> 
	<?= $form->field($model, 'allowpasstouser')->dropDownList($arr,[ 'prompt' => Yii::t('app',' -- Select --')]) ?>
	<?= $form->field($model, 'qtyPerPass')->textInput(['maxlength' => true]) ?>
 
  
   
 
	<?= $form->field($model, 'dateFrom')->textInput(['label'=> '' ,'id'=>'start-date'])->widget(DateRangePicker::className(), [
		'attributeTo' => 'dateTo', 
		'form' => $form, // best for correct client validation
		'language' => 'en',
		
		'clientOptions' => [
			'autoclose' => true,
			'startDate' => $model->dateFrom,
			'format' => 'yyyy-mm-dd'
		]
	]); ?>
 
	<div class="help-block"></div>
 <?= $form->field($model, 'duration')->textInput(['readonly'=> true]) ?>
 <?= $form->field($model, 'returnWithin')->textInput() ?> 
 
    <?php
   /* Day parts values */     
      $daypartends= ['01' => Yii::t('app','01 AM'),'02' => Yii::t('app','02 AM'),'03' => Yii::t('app','03 AM'),'04' => Yii::t('app','04 AM'),'05' => Yii::t('app','05 AM'),'06' => Yii::t('app','06 AM'),'07' => Yii::t('app','07 AM'),'08' => Yii::t('app','08 AM'),'09' => Yii::t('app','09 AM'),'10' => Yii::t('app','10 AM'),'11' => Yii::t('app','11 AM'),'12' => Yii::t('app','12 AM'),'13' => Yii::t('app','01 PM'),'14' => Yii::t('app','02 PM'),'15' => Yii::t('app','03 PM'),'16' => Yii::t('app','04 PM'),'17' => Yii::t('app','05 PM'),'18' => Yii::t('app','06 PM'),'19' => Yii::t('app','07 PM'), '20' =>Yii::t('app','08 PM'), '21' => Yii::t('app','09 PM'), '22' => Yii::t('app','10 PM'), '23' => Yii::t('app','11 PM'), '24' => Yii::t('app','12 PM')];
       $daypartstart= ['01' => Yii::t('app','01 AM'),'02' => Yii::t('app','02 AM'),'03' => Yii::t('app','03 AM'),'04' => Yii::t('app','04 AM'),'05' => Yii::t('app','05 AM'),'06' => Yii::t('app','06 AM'),'07' => Yii::t('app','07 AM'),'08' => Yii::t('app','08 AM'),'09' => Yii::t('app','09 AM'),'10' => Yii::t('app','10 AM'),'11' => Yii::t('app','11 AM'),'12' => Yii::t('app','12 AM'),'13' => Yii::t('app','01 PM'),'14' => Yii::t('app','02 PM'),'15' => Yii::t('app','03 PM'),'16' => Yii::t('app','04 PM'),'17' => Yii::t('app','05 PM'),'18' => Yii::t('app','06 PM'),'19' => Yii::t('app','07 PM'), '20' =>Yii::t('app','08 PM'), '21' => Yii::t('app','09 PM'), '22' => Yii::t('app','10 PM'), '23' => Yii::t('app','11 PM'), '24' => Yii::t('app','12 PM')];
     
      ?>
 
       
   
    <?php 
       $userdata = array();
       
       if(is_array($model->userType)) {            
           $model->userType =  implode(', ',$model->userType);
           foreach (explode(", ",$model->userType) as $usert ) {
         
                  
               $userdata[$usert] = array('Selected' => Yii::t('app','selected'));
           } 
           
       }else {
           foreach (explode(", ",$model->userType) as $usert ) {
         
                  
               $userdata[$usert] = array('Selected' => Yii::t('app','selected'));
           } 
       }
         $userDatas = $userdata;  
        ?>
        
         <button type="button" style="padding:0px 5px" onclick="alluserstype()" class="bbtn btn-success">Select all users type</button>
       <?= $form->field($model, 'userType')->dropDownList(array('1' =>Yii::t('app','Men'), '0' =>Yii::t('app','Women') ),['options' => $userDatas, 'id'=>'allselectuser', 'multiple'=>true, 'prompt' => Yii::t('app',' -- Select --')]) ?>
 
 
 
    <?php 
    
		$agedata = array();       
           if(is_array($model->agedGroup)) {            
			   $model->agedGroup =  implode(', ',$model->agedGroup);
			   foreach (explode(", ",$model->agedGroup) as $age ) {    					  
				   $agedata[$age] = array('Selected' => Yii::t('app','selected'));
				   }
           }else{         
			   foreach (explode(", ",$model->agedGroup) as $age ) {								  
				   $agedata[$age] = array('Selected' => Yii::t('app','selected'));
			   } 
			}
				$ageGroup = $agedata;  
							
			echo '<button type="button" style="padding:0px 5px" onclick="allAgegroup()" class="bbtn btn-success">Select all age groups</button>';	
			
			echo $form->field($model, 'agedGroup')->dropDownList(array( '1' =>Yii::t('app','Between 15-18 years'), '2' =>Yii::t('app','Between 19-25 years'), '3' =>Yii::t('app','Between 26-35 years'), '4' =>Yii::t('app','Between 36-45 years'), '5' =>Yii::t('app','Between 46-60 years'), '6' =>Yii::t('app','More than 61 years')), ['options' => $ageGroup, 'id'=>'allselectage', 'multiple'=>true, 'prompt' => Yii::t('app',' -- Select --')]);
			
	 ?>
  
    
       
	<?php   
			if(!empty($model->beaconGroup)){
				$beaconGroup=Beacongroup::find()->where('id IN('.$model->beaconGroup.')')->orderBy('group_name')->asArray()->all();
			}else{
				$beaconGroup = array();
			}     
				$model->beaconGroup =  array_column($beaconGroup,"id");

				$cityDesc= array_column($beaconGroup,"group_name");
              
				$url = \yii\helpers\Url::to(['beacon-groupname']);
      
           echo $form->field($model, 'beaconGroup')->widget(Select2::classname(), [
           'initValueText' => $cityDesc, // set the initial display text
           'options' => ['placeholder' => 'Search for beacon group ...','multiple' => true],
           'pluginOptions' => [
               'allowClear' => true,
               'minimumInputLength' => 3,
               'language' => [
                   'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
               ],
               'ajax' => [
                       'url' => $url,
                       'method'=>'POST',
                       'dataType' => 'json',                    
                       'data' => new JsExpression('function(params) { var selectedVal=($("#workorders-beacongroup").val()); return {q:params.term,sq:selectedVal}; }')
                   
               ],
               'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
               'templateResult' => new JsExpression('function(city) { return city.text; }'),
               'templateSelection' => new JsExpression('function (city) { return city.text; }'),
           ],
       ]);
   ?>
 
 
   <?= $form->field($model, 'rulecondition' )->checkbox(array('1'=>Yii::t('app','Rules/Conditions'))); ?>
   
   <button type="button" id='show-popup-model' class="btn btn-info btn-lg" style="display:none" data-toggle="modal" data-target="#myModal"><?= Yii::t('app','Open Modal') ?></button>
 
   <!-- Trigger the modal with a button -->
   <!-- Modal -->
   <div id="myModal" class="modal fade" role="dialog">
       <div class="modal-dialog">
       <!-- Modal content-->
           <div class="modal-content">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                   <h4 class="modal-title"><?= Yii::t('app','Rules/Conditions') ?></h4>
                   </div>
 
                   <div class="modal-body"> 
                       
                   <?= $form->field($model, 'dayparting')->radioList(array('1'=>Yii::t('app','24 Hours -OR-'),'3'=>Yii::t('app','Custom')),['id' => 'dayparting-id']); ?>    
 
                     <div id="show-dayparting " style="display:none;">
                           <div class="row">
                               <div class="col-md-6 col-lg-6">
                                   <?= $form->field($model, 'daypartingcustom1')->dropDownList($daypartstart, array( 'multiple'=>false,  'prompt' => Yii::t('app',' --option one start--')))->label(false) ?>
                               </div>
                                <div class="col-md-6 col-lg-6">
                                   <?=  $form->field($model, 'daypartingcustom1end')->dropDownList($daypartends, array('multiple'=>false,  'prompt' => Yii::t('app',' --option one end--')))->label(false); ?>
                               </div> 
                               <div class="col-md-6 col-lg-6">
                                   <?= $form->field($model, 'daypartingcustom2')->dropDownList($daypartstart, array('multiple'=>false,  'prompt' => Yii::t('app',' --option two start--')))->label(false) ?>
                               </div> 
                               <div class="col-md-6 col-lg-6">
                                   <?=  $form->field($model, 'daypartingcustom2end')->dropDownList($daypartends, array('multiple'=>false,  'prompt' => Yii::t('app',' --option two end--')))->label(false); ?>
                               </div> 
                               <div class="col-md-6 col-lg-6">
                                   <?= $form->field($model, 'daypartingcustom3')->dropDownList($daypartstart, array('multiple'=>false,  'prompt' => Yii::t('app',' --option three start--')))->label(false) ?>
                               </div> 
                               <div class="col-md-6 col-lg-6">
                                   <?=  $form->field($model, 'daypartingcustom3end')->dropDownList($daypartends, array('multiple'=>false,  'prompt' => Yii::t('app',' --option three end--')))->label(false); ?>
                               </div> 
                               <div class="clear"></div>
                           </div>
                    </div>
                    
                    <?php 
         $tempstart = [];
         for ($x = 1; $x <= 100; $x++) {
             $xf = $x.html_entity_decode("&nbsp;&#8451;");             
           $tempstart[$x] = $xf;
          }            
          $tempend = [];
         for ($h = 1; $h <= 100; $h++) {
             $hf = $h.html_entity_decode("&nbsp;&#8451;");             
           $tempend[$h] = $hf;
          } 
    
       ?>
       <?= $form->field($model, 'temperature')->radioList(array('1'=>Yii::t('app','ALL'),'2'=>Yii::t('app','Custom')),['id' => 'temp-id']); ?>
    
       <div id="show-temp">
           <div class="row">
               <div class="col-md-6 col-lg-6">
                     <?= $form->field($model, 'temperaturestart')->dropDownList($tempstart, array('multiple'=>false,  'prompt' =>Yii::t('app',' --temperature start --')))->label(false) ?>
                      </div>
                      <div class="col-md-6 col-lg-6">
                      <?= $form->field($model, 'temperaturend')->dropDownList($tempend, array('multiple'=>false,  'prompt' => Yii::t('app',' --temperature end --')))->label(false); ?>
                    </div>
           </div>       
      </div>
   
   <?php
     $visit = [];
         for ($h = 1; $h <= 100; $h++) {
             $hf = $h;             
           $visit[$h] = $hf;
          } 
   ?>
                <div class="row">
                   <div class="col-md-12 col-lg-12"> 
                       <label class="control-label" for="temp-id"><?= Yii::t('app','Behaviour visit') ?></label>
                   </div>
                   </div>
                   
                   <div class="row">            
                       <div class="col-md-4 col-lg-4">
                           <?= Yii::t('app','User has visited beacon location minimum of ') ?>
                       </div>
                       <div class="col-md-2 col-lg-2">
                           <?= $form->field($model, 'behaviourvisit1')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
                       </div>
                       <div class="col-md-1 col-lg-1">
                           <?= Yii::t('app','times in') ?>
                       </div>
                       <div class="col-md-2 col-lg-2"> 
                           <?= $form->field($model, 'behaviourvisit2')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
                       </div>
                       <div class="col-md-3 col-lg-3"> 
 
                           <?= $form->field($model, 'behaviourvisit3')->dropDownList([1=>Yii::t('app','days'),2=>Yii::t('app','weeks'),3=>Yii::t('app','months')], array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
 
                       </div>
                       
                  </div>
                  
                  <div class="row">
                   <div class="col-md-12 col-lg-12"> 
                       <label class="control-label" for="temp-id"><?= Yii::t('app','Behaviour dwell') ?></label>
                   </div>
                   </div>      
                   <div class="row">            
                       <div class="col-md-6 col-lg-6" style="padding-right:0px;">
                           <?= Yii::t('app','User has dwelt in beacon location minimum of ') ?>
                       </div>
                       <div class="col-md-3 col-lg-3">
                           <?= $form->field($model, 'behaviourDwell')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
                       </div>
                       <div class="col-md-3 col-lg-3">
                           <?= $form->field($model, 'behaviourDwellMinutes')->dropDownList([1 => Yii::t('app','minutes'), 2 => Yii::t('app','seconds')], array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
                       
                   </div>
                       
                   </div>
                 
                    <div class="row">
                   <div class="col-md-12 col-lg-12"> 
                       <label class="control-label" for="temp-id"><?= Yii::t('app','On Entry') ?></label>
                   </div>
                   </div>      
                   <?= $form->field($model, 'userenter')->checkbox(array('label'=>Yii::t('app','User has entered the store'))) ?>
                   
                    <div class="row">
                   <div class="col-md-12 col-lg-12"> 
                       <label class="control-label" for="temp-id"><?= Yii::t('app','On Exit') ?></label>
                   </div>
                   </div>      
                   <?= $form->field($model, 'usereturned')->checkbox(array('label'=>Yii::t('app','User has exited the store'))) ?>
                
                  <!-- Purchase history start -->     
                   <div class="row">
                   <div class="col-md-12 col-lg-12"> 
                       <label class="control-label" for="temp-id"><?= Yii::t('app','Purchase history') ?></label>
                   </div>
                   </div>      
                   <div class="row">            
                       <div class="col-md-2 col-lg-2">
                           <?= Yii::t('app','User has spent minimum of ') ?>
                       </div>
                       <div class="col-md-1 col-lg-1" style="font-weight: bold;
               padding: 7px 0 0; width:8px;">
                           <?= Yii::t('app','$') ?>
                           </div>
                       <div class="col-md-2 col-lg-2" style="padding-left: 4px;">                
                           <?= $form->field($model, 'purchaseHistory1')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
                       </div>
                       <div class="col-md-2 col-lg-2">
                       in the past
                       </div>
                       <div class="col-md-2 col-lg-2">
                           <?= $form->field($model, 'purchaseHistory2')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
                       </div>
                       <div class="col-md-3 col-lg-3">
 
                           <?= $form->field($model, 'purchaseHistory3')->dropDownList([1=>Yii::t('app','days'),2=>Yii::t('app','weeks'),3=>Yii::t('app','months'),4=>Yii::t('app','year')], array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
 
                       </div>            
                       
                       
                   </div>
                   
                   <!-- Loyalty start -->     
                   <div class="row">
                   <div class="col-md-12 col-lg-12"> 
                       <label class="control-label" for="temp-id"><?= Yii::t('app','Loyalty') ?></label>
                   </div>
                   </div>      
                   <div class="row">            
                       <div class="col-md-2 col-lg-2">
                           <?= Yii::t('app','User has scanned their loyalty ID') ?>
                       </div>
              
                       <div class="col-md-2 col-lg-2">
                           <?= $form->field($model, 'Loyalty1')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
                       </div>
                       <div class="col-md-2 col-lg-2">
                       times     in the past
                       </div>
                       <div class="col-md-2 col-lg-2">
                           <?= $form->field($model, 'Loyalty2')->dropDownList($visit, array('multiple'=>false,  'prompt' => 'select'))->label(false) ?>
                       </div>
                       <div class="col-md-3 col-lg-3">
 
                           <?= $form->field($model, 'Loyalty3')->dropDownList([1=>Yii::t('app','days'),2=>Yii::t('app','weeks'),3=>Yii::t('app','months'),4=>Yii::t('app','year')], array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
 
                       </div>            
                        
                   </div>
 
               </div>
 
               <div class="modal-footer">
                   <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Save and Close') ?></button>
               </div>
         </div>
   </div>
</div>   
   
   <!---end Departing for Update--->  
     
   
   <div class="help-block"></div>
    <label class="control-label" for="store-owner"><?= Yii::t('app','Select Categories') ?></label>
           <div class="chosentree"></div> 
 
   
   
   
     </div><!-- /.box-body -->
     <div class="box-footer">
 
    <div class="form-group">
		<?= Html::a('SAVE AND EXIT',['create'],['class'=>'btn btn-primary progressbtn','id'=>1]) ?>
		<?= Html::a('NEXT',['create'],['class'=>'btn btn-success progressbtn','id'=>2]) ?>	
    </div>
    </div>
	<input type="hidden" id="savenew" name="savenew" value="" />
    <?php ActiveForm::end(); ?>
 
</div>
 
 
 
 
<?php 
 
$this->registerJs(
 
    '
var loadChildren = function(node) {
    
JSONObject =JSON.parse(\' '.$categoryTree.'\');   
       node.children.push(JSONObject);  
        return node;
      };
 
$(document).ready(function() {
        $("div.chosentree").chosentree({
            width: 500,
            deepLoad: true,
              showtree: true,
            load: function(node, callback) {
                    setTimeout(function() {
          callback(loadChildren(node));
       }, 1000);
            }
        });
});
       '
);
    
    
$this->registerJsFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.js', ['position' => \yii\web\View::POS_END]);
$this->registerCssFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.css', ['position' => \yii\web\View::POS_END]);
//$this->registerJsFile(Yii::$app->homeUrl.'store-address-suggest.js', ['position' => \yii\web\View::POS_END]);
$baseurl = Yii::$app->getUrlManager()->getBaseUrl(); 
$shopnowget =$baseurl.'/workorders/partnergetshopnow';
$workordersCat =$baseurl.'/workorders/workcat';
?>
<script type="text/javascript">
	$('.progressbtn').click(function(e){
		e.preventDefault();
		$('#savenew').val($(this).attr('id'));
		$('#w0').submit();
	});
$('#workorders-workorderpartner').change(function(){
	$('#chosentree-wrap').show();
	var storeID = $(this).val();
	$('.loading-image.text-center').show();
	$.ajax({
			method: "POST",
			url: "<?php echo $workordersCat; ?>",
			data: { id:storeID }	
	
	})
	.done(function(result) {	
	$('.loading-image.text-center').hide();
		 $('div.chosentree').empty();
		JSONObject = JSON.parse(result);
      $('div.chosentree').chosentree({
          width: 500,
          deepLoad: true,
          load: function(node, callback) {
                  callback(JSONObject);
          }
      });
	  });
		
});
$(document).ready(function() {
       //var text = $("label[for=start-date]").text();
       $("label[for=start-date]").append( "<label class='control-label' id='end-date'>End Date</label>" );
       $('#end-date').css('margin-left','191px');
       var workorderstype = $( "#workorders-type option:selected" ).val();
       if(workorderstype==1 || workorderstype==4){
       $( ".field-workorders-allowpasstouser" ).css('display','block');
        $( ".field-workorders-qtyperpass" ).css('display','block');
       }else{
       $( ".field-workorders-allowpasstouser" ).css('display','none');
       $( ".field-workorders-qtyperpass" ).css('display','none');
       }
});
   
 
 
   $('#workorders-rulecondition').click(function(){
    if (this.checked) {
       $('#show-popup-model').click();
    }
});
 
 
 $( "#workorders-type" ).change(function() {
  var workorders_type = $(this).val();
   if(workorders_type==4){
  $( "#workorders-buynow" ).prop( "checked", true );
  
}
});
$( "#workorders-type" ).change(function() {
  var workorders_type = $(this).val();
   if(workorders_type==1 || workorders_type==4){
  $( ".field-workorders-allowpasstouser" ).css('display','block');
  $( ".field-workorders-qtyperpass" ).css('display','block');
 }else{
   $( ".field-workorders-allowpasstouser" ).css('display','none');
   $( ".field-workorders-qtyperpass" ).css('display','none');
   }
});

$("#workorders-geoproximity").change(function() {
	
  var geoproximity_type = $(this).val();

	if(geoproximity_type==1 || geoproximity_type==2){
		
		$( ".field-workorders-beacongroup" ).css('display','block');
		
	}else{
		
		$(".field-workorders-beacongroup").css('display','none');
		
	}
	
	if(geoproximity_type==1 || geoproximity_type==3){
		
		$( "#show-geopopup-model" ).css('display','block');
		
	}else{
		
		$("#show-geopopup-model").css('display','none');
		
	}
	
	
});
 
</script>
<style>
	.box-footer a {    margin: 1px 5px;}
.field-workorders-allowpasstouser { display:none;}
.field-workorders-qtyperpass { display:none;}
.loading,.loading:before{position:fixed;top:0;left:0}.field-workorders-allowpasstouser,.field-workorders-qtyperpass{display:none}.loading:before,.loading:not(:required):after{content:'';display:block}.loading{z-index:999;height:2em;width:2em;overflow:show;margin:auto;bottom:0;right:0}.loading:before{width:100%;height:100%;background-color:rgba(0,0,0,.3)}.loading:not(:required){font:0/0 a;color:transparent;text-shadow:none;background-color:transparent;border:0}.loading:not(:required):after{font-size:10px;width:1em;height:1em;margin-top:-.5em;-webkit-animation:spinner 1.5s infinite linear;-moz-animation:spinner 1.5s infinite linear;-ms-animation:spinner 1.5s infinite linear;-o-animation:spinner 1.5s infinite linear;animation:spinner 1.5s infinite linear;border-radius:.5em;-webkit-box-shadow:rgba(0,0,0,.75) 1.5em 0 0 0,rgba(0,0,0,.75) 1.1em 1.1em 0 0,rgba(0,0,0,.75) 0 1.5em 0 0,rgba(0,0,0,.75) -1.1em 1.1em 0 0,rgba(0,0,0,.5) -1.5em 0 0 0,rgba(0,0,0,.5) -1.1em -1.1em 0 0,rgba(0,0,0,.75) 0 -1.5em 0 0,rgba(0,0,0,.75) 1.1em -1.1em 0 0;box-shadow:rgba(0,0,0,.75) 1.5em 0 0 0,rgba(0,0,0,.75) 1.1em 1.1em 0 0,rgba(0,0,0,.75) 0 1.5em 0 0,rgba(0,0,0,.75) -1.1em 1.1em 0 0,rgba(0,0,0,.75) -1.5em 0 0 0,rgba(0,0,0,.75) -1.1em -1.1em 0 0,rgba(0,0,0,.75) 0 -1.5em 0 0,rgba(0,0,0,.75) 1.1em -1.1em 0 0}@-webkit-keyframes spinner{0%{-webkit-transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-o-transform:rotate(360deg);transform:rotate(360deg)}}@-moz-keyframes spinner{0%{-webkit-transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-o-transform:rotate(360deg);transform:rotate(360deg)}}@-o-keyframes spinner{0%{-webkit-transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-o-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes spinner{0%{-webkit-transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-o-transform:rotate(360deg);transform:rotate(360deg)}}
</style>
