<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\WorkordersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="workorders-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'workorderID') ?>

    <?= $form->field($model, 'campaign_name') ?>

    <?= $form->field($model, 'campaign_type') ?>

    <?= $form->field($model, 'country_name') ?>

    <?= $form->field($model, 'media_contract_number') ?>

    <?php // echo $form->field($model, 'number_of_pass') ?>

    <?php // echo $form->field($model, 'total_amount') ?>

    <?php // echo $form->field($model, 'campain_duration') ?>

    <?php // echo $form->field($model, 'date_from') ?>

    <?php // echo $form->field($model, 'date_to') ?>

    <?php // echo $form->field($model, 'campaign_expires') ?>

    <?php // echo $form->field($model, 'users_type') ?>

    <?php // echo $form->field($model, 'aged_group') ?>

    <?php // echo $form->field($model, 'interested_in') ?>

    <?php // echo $form->field($model, 'locations') ?>

    <?php // echo $form->field($model, 'beacon_group') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
