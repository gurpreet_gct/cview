<?php

use yii\helpers\Html;
use common\models\Workorders;

/* @var $this yii\web\View */
/* @var $model app\models\Workorders */
	$wo = workorders::find()->select(['type'])->where(['id' => $_GET['id']])->one();
	$title = [2=>'STEP 2: CREATE NOTIFICATION',3=>'STEP 3: CREATE DEAL',4=>'STEP 4: CREATE GET REQUEST',5=>'STEP 5: CREATE PASS'];
	if($wo->type==2 && $_GET['step']==5){
		$this->title = Yii::t('app','STEP 4: CREATE PASS');
	} else {
		$this->title = Yii::t('app',$title[$_GET['step']]);
	}
	$information = array(2=>'Complete the field below to create the notification that will appear on the phone when the deal is triggered.',3=>'Complete the fields below to create the DEAL that will appear on the My Deals screen.',4=>'Complete the fields below to create a GET request that will appear when user clicks on GET on a deal.',5=>'Complete the fields below to create a PASS that will be saved in the wallet when the user clicks on SAVE TO WALLET.');
?>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
               <div class=" panel panel-primary">
                <div class="panel-heading">
                    <div class="pull-right">
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <?php
						if($wo->type==2 && $_GET['step']==5){
							echo Yii::t('app','STEP 4: CREATE PASS');
						} else {
							echo $title[$_GET['step']];
						} ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
<div class="panel-before" >
<div class="row">
<div class="col-sm-10 col-md-10 col-lg-10">
<h4 class="bg-info"><?= $information[$_GET['step']]; ?></h4>
</div>
<div class="col-sm-2" style="margin-top:12px;">
<div role="toolbar" class="btn-toolbar kv-grid-toolbar">
<div class="btn-group"><a title="<?= Yii::t('app','All Workorders') ?>" href="<?=Yii::$app->urlManager->createUrl(['workorders/index',])?>" class="btn btn-primary"><i class="glyphicon glyphicon-tasks"></i></a>
<a data-pjax="0" title="<?= Yii::t('app','Reset Grid') ?>" href="<?=Yii::$app->urlManager->createUrl(['workorders/createoffer?id='.$_GET['id'].'&step='.$_GET['step'],])?>" class="btn btn-default"><i class="glyphicon glyphicon-repeat"></i></a></div>


</div>
</div>

<div class="clearfix"></div>
</div>
</div>
                <!-- form start -->

     <?= $this->render('_formoffer', [

					'model' => $offermodel,
					'workorderModel' => $workorderModel,
					'dealmodel'=>$dealmodel,
					'partnerProducts' => $partnerProducts,

    ]) ?>

              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
