<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Store;
use common\models\Workorders;
use common\models\Profile;
use paras\cropper\Widget;
use yii\helpers\Url;
use common\models\Workorderpopup;
use common\models\User;


/* @var $this yii\web\View */
/* @var $model backend\models\Deal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="Deal-form">

     <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>


		<div class="box-body">
		<div class="form-group">
	 <?php

		$BeaconPop = Workorderpopup::find()->where(['workorder_id' => $_GET['id']])->one();
	 ?>

	 <?php

	$getcmpType = workorders::find()->select(['type','workorderpartner'])->where(['id' => $_GET['id']])->one();
	if(isset($getcmpType->workorderpartner)) {
	$partnerinfo = Profile::find()->select(['companyName','brandAddress'])->where(['user_id' => $getcmpType->workorderpartner])->one();
		/* GET partner info from profile model */
		if(isset($partnerinfo->companyName)){
			$partnerName = $partnerinfo->companyName;
			}else{
			$partnerName = Yii::t('app','Not Selected');
			}

		if(isset($partnerinfo->brandAddress)){
			$partnerAddress = $partnerinfo->brandAddress;
			}else{
				$partnerAddress = Yii::t('app','Not Selected');
			}

		}

 ?>
	  <?= $form->field($model, 'campaignID' )->hiddenInput(['value' => $_GET['id']])->label(false); ?>
    <!--  workorder type in popup form  -->
     <?= $form->field($model, 'wtype_pop' )->hiddenInput(['value' => $getcmpType->type])->label(false); ?>



		<?= $form->field($model, 'dealTitle')->hiddenInput(['value' => $BeaconPop->offerTitle])->label(false);?>

		<?= $form->field($model, 'text')->hiddenInput(['value' => $BeaconPop->offerText])->label(false); ?>
		<?= $form->field($model, 'logoUpload')->hiddenInput(['value' => ''])->label(false); ?>

		<?= $form->field($model, 'offerwas')->hiddenInput(['value' => $BeaconPop->offerwas])->label(false); ?>
		<?= $form->field($model, 'offernow')->hiddenInput(['value' => $BeaconPop->offernow])->label(false); ?>

		<?php echo $form->field($model, 'dealuploadTypes')->radioList(['1' => Yii::t('app','Add image'), '2' => Yii::t('app','Add Video')]); ?>

		<!-- preview image of image -->
		<div id="previewImage"></div>

		<!-- preview image of image -->
		<?php
			echo '<div id="deal-image-block" style="display:none;">';

			echo $form->field($model, 'imageFile3')->widget(Widget::className(), [
    'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
    	'width'=>750,
		'height'=>360]) ;
			echo '</div>';

			echo '<div id="deal-video-block-types" style="display:none;">';
			echo $form->field($model, 'dealvideoTypes')->radioList(['1' => Yii::t('app','Add Video Other/Video Url'), '2' => Yii::t('app','Add Video  Youtube/vimeo')]);
			echo $form->field($model, 'dealbrowsercheck')->radioList(['1' => Yii::t('app','Yes'), '0' => Yii::t('app','No')])->label(Yii::t('app','Open Video In Browser'));
			echo '</div>';
			echo'<div id="deal-video-block-types-youtube" style="display:none;">';
			echo $form->field($model, 'dealvideokey')->textInput()->label(Yii::t('app','Deal Video'));
			echo '</div>';
			echo'<div id="deal-video-block-types-system" style="display:none;">';
			?>
			<?php echo $form->field($model, 'dealvideoWorkorderName')->fileInput()->label(Yii::t('app','Video Upload')); ?>
			<?= Yii::t('app','OR') ?>
			<?php echo $form->field($model, 'dealvideoWorkorderUrl')->textInput()->label(Yii::t('app','Video Url'));
			?>

			<!-- preview image -->
			<div id="previewDealImage"></div>

			<!-- preview image -->
			<?php
			echo $form->field($model, 'dealvideoWorkorderImage')->widget(Widget::className(), [
    'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
    	'width'=>640,
		'height'=>300]) ;
			 echo '</div>';
			?>

     </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
		<?= Html::a('BACK',['updateoffer','id'=>$_GET['id'],'step'=>2],['class'=>'btn btn-default','id'=>1]) ?>
		<?= Html::a('SAVE AND EXIT',['create'],['class'=>'btn btn-primary progressbtn','id'=>1]) ?>
		<?= Html::a('NEXT',['updateoffer','id'=>$_GET['id'],'step'=>5],['class'=>'btn btn-success progressbtn','id'=>2]) ?>
    </div>
    </div>
<input type="hidden" id="savenew" name="savenew" value="" />
    <?php ActiveForm::end(); ?>



</div>

<script>
$('.progressbtn').click(function(e){
		e.preventDefault();
		$('#savenew').val($(this).attr('id'));
		$('#w0').submit();
});

/* preview image on upload with info */

var _URL = window.URL || window.webkitURL;



function previewImage(input,boxId,imgW,imgH) {


    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {

			$("#"+boxId).html('<img id="upload_preview" src="'+e.target.result+'" style="width:'+imgW+'px;height:'+imgH+'px" />');


        }

        reader.readAsDataURL(input.files[0]);


    }
}

$("#deal-imagefile3").change(function(){

	var file, img;
	var imgObj = this;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function() {
				previewImage(imgObj,'previewImage',650,360);

				var width 	=	this.width;
				var height 	=	this.height;

				if(width!=750 && height!=360){

					alert('Please note an image may become distorted if it is not the correct shape.\nThis image size is '+width+'px X '+height+'px.');
				}
			};
			img.onerror = function() {
				alert( "not a valid file: " + file.type);

			};
			img.src = _URL.createObjectURL(file);
	}


});

$("#deal-dealvideoworkorderimage").change(function(){
	var file, img;
	var imgObj = this;
	if ((file = this.files[0])) {
			img = new Image();
			img.onload = function() {
				previewImage(imgObj,'previewDealImage',640,300);

				var width 	=	this.width;
				var height 	=	this.height;

				if(width!=640 && height!=300){

					alert('Please note an image may become distorted if it is not the correct shape.\nThis image size is '+width+'px X '+height+'px.');
				}
			};
			img.onerror = function() {
				alert( "not a valid file: " + file.type);

			};
			img.src = _URL.createObjectURL(file);
	}


});

/* preview image on upload with info */
</script>
<style>
	h4.bg-info {font-size:14px; line-height: 1.4; padding: 1px 5px; text-align: justify;}
	.box-footer a {    margin: 1px 5px;}
</style>
