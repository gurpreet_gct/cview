<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Workorders */

$this->title = Yii::t('app','Update Workorders(PSA): ') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Workorders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => ucfirst($model->name), 'url' => ['viewpsa', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
               <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-tasks"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                <!-- form start -->
                
                 <?= $this->render('_formupdatepsa', [
			'model' => $model,
			'offermodel' => $offermodel,
			'categoryTree'=>$categoryTree,
		  ]) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
