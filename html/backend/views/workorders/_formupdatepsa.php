<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use backend\models\Beacongroup;
use common\models\Country;
use backend\models\Beaconlocation;
use backend\models\Usermanagement;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;
use kartik\select2\Select2;
use kartik\time\TimePicker;
use yii\web\JsExpression;
use paras\cropper\Widget;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\Workorders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="workorders-form">

    <?php
  
    $form = ActiveForm::begin([
                'options' => [                 
                    'enctype' =>'multipart/form-data'
                ]
    ]);
    ?>
    
    <div class="box-body">
      <div class="form-group">
	 <?php echo $form->errorSummary($model);
		//echo $form->errorSummary($offermodel); ?>
	 
	  <?php  $country_name =  ArrayHelper::map(Country::find()->all(), 'id_countries', 'name'); ?>
	 <?= $form->field($model, 'countryID')->dropDownList($country_name, array('prompt'=>Yii::t('app','Select'))) ?>
    
 
 
      <?php   $partner =  ArrayHelper::map(Usermanagement::find()->where(['user_type' => 7])->andWhere(['status' => 10])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName'); ?>
       
    
    <?= $form->field($model, 'workorderpartner')->dropDownList($partner, array('prompt'=>Yii::t('app','Select Partner'))); ?>
    

	<?= $form->field($model, 'mediaContractNumber')->textInput(['maxlength' => true]) ?> 	  
	  
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

     <?= $form->field($model, 'type')->dropDownList(array('1' =>'PSA info', '2' =>'PSA deal' ),[ 'prompt' => Yii::t('app',' -- Select --')]) ?>
    
     <?= $form->field($model, 'workorderType')->hiddenInput(['value' => 2])->label(false); ?>
     
	  
    <?= $form->field($model, 'noPasses')->textInput(['maxlength' => true]) ?>
   
	<?= $form->field($model, 'dateFrom')->widget(DateRangePicker::className(), [
		'attributeTo' => 'dateTo', 
		'form' => $form, // best for correct client validation
		'language' => 'en',
		'size' => 'lg',
		'clientOptions' => [
			'autoclose' => true,
			'startDate' => date("Y-m-d"),
			'format' => 'yyyy-mm-dd'
		]
	]);?>
	<?= $form->field($model, 'duration')->textInput(['readonly'=> true]) ?>

<div class="help-block"></div>

		 <?php 
	
	   $daypartends= ['1 AM' => Yii::t('app','1 AM'),'2 AM' => Yii::t('app','2 AM'),'3 AM' => Yii::t('app','3 AM'),'4 AM' => Yii::t('app','4 AM'),'5 AM' => Yii::t('app','5 AM'),'6 AM' => Yii::t('app','6 AM'),'7 AM' => Yii::t('app','7 AM'),'8 AM' => Yii::t('app','8 AM'),'9 AM' => Yii::t('app','9 AM'),'10 AM' => Yii::t('app','10 AM'),'11 AM' => Yii::t('app','11 AM'),'12 AM' => Yii::t('app','12 AM'),'1 PM' => Yii::t('app','1 PM'),'2 PM' => Yii::t('app','2 PM'),'3 PM' => Yii::t('app','3 PM'),'4 PM' => Yii::t('app','4 PM'),'5 PM' => Yii::t('app','5 PM'),'6 PM' => Yii::t('app','6 PM'),'7 PM' => Yii::t('app','7 PM'), '8 PM' =>Yii::t('app','8 PM'), '9 PM' => Yii::t('app','9 PM'), '10 PM' => Yii::t('app','10 PM'), '11 PM' => Yii::t('app','11 PM'), '12 PM' => Yii::t('app','12 PM')];
	   
	  $daypartstart = ['1 AM' => Yii::t('app','1 AM'),'2 AM' => Yii::t('app','2 AM'),'3 AM' => Yii::t('app','3 AM'),'4 AM' => Yii::t('app','4 AM'),'5 AM' => Yii::t('app','5 AM'),'6 AM' => Yii::t('app','6 AM'),'7 AM' => Yii::t('app','7 AM'),'8 AM' => Yii::t('app','8 AM'),'9 AM' => Yii::t('app','9 AM'),'10 AM' => Yii::t('app','10 AM'),'11 AM' => Yii::t('app','11 AM'),'12 AM' => Yii::t('app','12 AM'),'1 PM' => Yii::t('app','1 PM'),'2 PM' => Yii::t('app','2 PM'),'3 PM' => Yii::t('app','3 PM'),'4 PM' => Yii::t('app','4 PM'),'5 PM' => Yii::t('app','5 PM'),'6 PM' => Yii::t('app','6 PM'),'7 PM' => Yii::t('app','7 PM'), '8 PM' =>Yii::t('app','8 PM'), '9 PM' => Yii::t('app','9 PM'), '10 PM' => Yii::t('app','10 PM'), '11 PM' => Yii::t('app','11 PM'), '12 PM' => Yii::t('app','12 PM')];
	  
	 
    
   ?>
		
		 
		
       
    <?php 
    		$userdata = array();
		
		if(is_array($model->userType)) {			
			$model->userType =  implode(', ',$model->userType);
			foreach (explode(", ",$model->userType) as $usert ) {
		  
		 		  
				$userdata[$usert] = array('Selected' => 'selected');
			} 
			
		}else {
			foreach (explode(", ",$model->userType) as $usert ) {
		  
		 		  
				$userdata[$usert] = array('Selected' => 'selected');
			} 
		}
		  $userDatas = $userdata;  
		 ?>
		 
		  <button type="button" style="padding:0px 5px" onclick="alluserstype()" class="btn btn-success"><?= Yii::t('app','Select All Users Type') ?></button>
		<?= $form->field($model, 'userType')->dropDownList(array('1' =>'Men', '0' =>'Women'),['options' => $userDatas, 'multiple'=>true, 'id'=>'allselectuser', 'prompt' => Yii::t('app',' -- Select --')]) ?>


	 <?php 
    
		$agedata = array();       
           if(is_array($model->agedGroup)) {            
			   $model->agedGroup =  implode(', ',$model->agedGroup);
			   foreach (explode(", ",$model->agedGroup) as $age ) {    					  
				   $agedata[$age] = array('Selected' => Yii::t('app','selected'));
				   }
           }else{         
			   foreach (explode(", ",$model->agedGroup) as $age ) {								  
				   $agedata[$age] = array('Selected' => Yii::t('app','selected'));
			   } 
			}
				$ageGroup = $agedata;  
							
			echo '<button type="button" style="padding:0px 5px" onclick="allAgegroup()" class="bbtn btn-success">Select all age groups</button>';	
			
			echo $form->field($model, 'agedGroup')->dropDownList(array( '1' =>Yii::t('app','Between 15-18 years'), '2' =>Yii::t('app','Between 19-25 years'), '3' =>Yii::t('app','Between 26-35 years'), '4' =>Yii::t('app','Between 36-45 years'), '5' =>Yii::t('app','Between 46-60 years'), '6' =>Yii::t('app','More than 61 years')), ['options' => $ageGroup, 'id'=>'allselectage', 'multiple'=>true, 'prompt' => Yii::t('app',' -- Select --')]);
			
	 ?>
  
	<?php   
			$beaconGroup=Beacongroup::find()->where('id IN("$model->beaconGroup")')->orderBy('group_name')->asArray()->all();
     
				$model->beaconGroup =  array_column($beaconGroup,"id");

				$cityDesc= array_column($beaconGroup,"group_name");
              
				$url = \yii\helpers\Url::to(['beacon-groupname']);
      
           echo $form->field($model, 'beaconGroup')->widget(Select2::classname(), [
           'initValueText' => $cityDesc, // set the initial display text
           'options' => ['placeholder' => 'Search for beacon group ...','multiple' => true],
           'pluginOptions' => [
               'allowClear' => true,
               'minimumInputLength' => 3,
               'language' => [
                   'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
               ],
               'ajax' => [
                       'url' => $url,
                       'method'=>'POST',
                       'dataType' => 'json',                    
                       'data' => new JsExpression('function(params) { var selectedVal=($("#workorders-beacongroup").val()); return {q:params.term,sq:selectedVal}; }')
                   
               ],
               'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
               'templateResult' => new JsExpression('function(city) { return city.text; }'),
               'templateSelection' => new JsExpression('function (city) { return city.text; }'),
           ],
       ]);
   ?>
 
<!-- start rule and condition  -->



<?= $form->field($model, 'rulecondition' )->checkbox(array('1'=>Yii::t('app','Rules/Conditions'))); ?>

<button type="button" id='show-popup-model' class="btn btn-info btn-lg" style="display:none" data-toggle="modal" data-target="#myModal"><?= Yii::t('app','Open Modal') ?></button>
		
<!-- Trigger the modal with a button -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= Yii::t('app','Rules/Conditions') ?></h4>
      </div>
           <div class="modal-body">
          
		
			<?= $form->field($model, 'dayparting')->radioList(array('1'=>Yii::t('app','24 Hours -OR-'),'3'=>Yii::t('app','Custom')),['id' => 'dayparting-id']); ?>
      
      <div id="show-dayparting " style="display:none;">
			<div class="row">
				<div class="col-md-6 col-lg-6">
				   <?= $form->field($model, 'daypartingcustom1')->dropDownList($daypartstart, array( 'multiple'=>false,  'prompt' => Yii::t('app',' --option one start--')))->label(false) ?>
				  </div> <div class="col-md-6 col-lg-6">
				   <?=  $form->field($model, 'daypartingcustom1end')->dropDownList($daypartends, array('multiple'=>false,  'prompt' => Yii::t('app',' --option one end--')))->label(false); ?>
					</div> <div class="col-md-6 col-lg-6">
					<?= $form->field($model, 'daypartingcustom2')->dropDownList($daypartstart, array('multiple'=>false,  'prompt' => Yii::t('app',' --option two start--')))->label(false) ?>
				   </div> <div class="col-md-6 col-lg-6">
				   <?=  $form->field($model, 'daypartingcustom2end')->dropDownList($daypartends, array('multiple'=>false,  'prompt' => Yii::t('app',' --option two end--')))->label(false); ?>
					</div> <div class="col-md-6 col-lg-6">
					<?= $form->field($model, 'daypartingcustom3')->dropDownList($daypartstart, array('multiple'=>false,  'prompt' => Yii::t('app',' --option three start--')))->label(false) ?>
				   </div> <div class="col-md-6 col-lg-6">
				   <?=  $form->field($model, 'daypartingcustom3end')->dropDownList($daypartends, array('multiple'=>false,  'prompt' => Yii::t('app',' --option three end--')))->label(false); ?>
				</div> 
				<div class="clear"></div>
		   </div>
	   </div>
	    <?php 
		  $tempstart = [];
		  for ($x = 1; $x <= 100; $x++) {
			  $xf = $x.html_entity_decode("&nbsp;&#8451;");			 
			$tempstart[$x] = $xf;
		   } 		   
		   $tempend = [];
		  for ($h = 1; $h <= 100; $h++) {
			  $hf = $h.html_entity_decode("&nbsp;&#8451;");			 
			$tempend[$h] = $hf;
		   } 
    
		?>
		<?= $form->field($model, 'temperature')->radioList(array('1'=>Yii::t('app','ALL'),'2'=>Yii::t('app','Custom')),['id' => 'temp-id']); ?>
	 
	    <div id="show-temp">
			<div class="row">
				<div class="col-md-6 col-lg-6">
					  <?= $form->field($model, 'temperaturestart')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app',' --temperature start --')))->label(false) ?>
					   </div>
					   <div class="col-md-6 col-lg-6">
					   <?= $form->field($model, 'temperaturend')->dropDownList($tempend, array('multiple'=>false,  'prompt' => Yii::t('app',' --temperature end --')))->label(false); ?>
				     </div>
			</div>	   
	   </div>
	
	<?php
	  $visit = [];
		  for ($h = 1; $h <= 100; $h++) {
			  $hf = $h;			 
			$visit[$h] = $hf;
		   } 
	?>
	 <div class="row">
		<div class="col-md-12 col-lg-12"> 
			<label class="control-label" for="temp-id"><?= Yii::t('app','Behaviour visit') ?></label>
		</div>
		</div>
		
		<div class="row">			
			<div class="col-md-4 col-lg-4">
				<?= Yii::t('app','User has visited beacon location minimum of ') ?>
			</div>
			<div class="col-md-2 col-lg-2">
				<?= $form->field($model, 'behaviourvisit1')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
			</div>
			<div class="col-md-1 col-lg-1">
				<?= Yii::t('app','times in') ?>
			</div>
			<div class="col-md-2 col-lg-2"> 
				<?= $form->field($model, 'behaviourvisit2')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
			</div>
			<div class="col-md-3 col-lg-3"> 
				<?= $form->field($model, 'behaviourvisit3')->dropDownList([1=>Yii::t('app','days'),2=>Yii::t('app','weeks'),3=>Yii::t('app','months')], array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
			</div>
			
       </div>
       
       <div class="row">
		<div class="col-md-12 col-lg-12"> 
			<label class="control-label" for="temp-id"><?= Yii::t('app','Behaviour dwell') ?></label>
		</div>
		</div>      
		<div class="row">			
			<div class="col-md-6 col-lg-6" style="padding-right:0px;">
				<?= Yii::t('app','User has dwelt in beacon location minimum of ') ?>
			</div>
			<div class="col-md-3 col-lg-3">
				<?= $form->field($model, 'behaviourDwell')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
			</div>
			<div class="col-md-3 col-lg-3">
							<?= $form->field($model, 'behaviourDwellMinutes')->dropDownList([1 => Yii::t('app','minutes'), 2 => Yii::t('app','seconds')], array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
						
					</div>
			
			</div>
			

         <div class="row">
		<div class="col-md-12 col-lg-12"> 
			<label class="control-label" for="temp-id"><?= Yii::t('app','On Entry') ?></label>
		</div>
		</div>      
		<?= $form->field($model, 'userenter')->checkbox(array('label'=>Yii::t('app','User has entered the store'))) ?>
		
		 <div class="row">
		<div class="col-md-12 col-lg-12"> 
			<label class="control-label" for="temp-id"><?= Yii::t('app','On Exit') ?></label>
		</div>
		</div>      
		<?= $form->field($model, 'usereturned')->checkbox(array('label'=>Yii::t('app','User has exited the store'))) ?>
	 
       <!-- Purchase history start -->	 
		<div class="row">
		<div class="col-md-12 col-lg-12"> 
			<label class="control-label" for="temp-id"><?= Yii::t('app','Purchase history') ?></label>
		</div>
		</div>      
		<div class="row">			
			<div class="col-md-2 col-lg-2">
				<?= Yii::t('app','User has spent minimum of ') ?>
			</div>
			<div class="col-md-1 col-lg-1" style="font-weight: bold;
    padding: 7px 0 0; width:8px;">
				<?= Yii::t('app','$') ?>
				</div>
			<div class="col-md-2 col-lg-2" style="padding-left: 4px;">				
				<?= $form->field($model, 'purchaseHistory1')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
			</div>
			<div class="col-md-2 col-lg-2">
			<?= Yii::t('app','in the past') ?>
			</div>
			<div class="col-md-2 col-lg-2">
				<?= $form->field($model, 'purchaseHistory2')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
			</div>
			<div class="col-md-3 col-lg-3">
				<?= $form->field($model, 'purchaseHistory3')->dropDownList([1=>Yii::t('app','days'),2=>Yii::t('app','weeks'),3=>Yii::t('app','months'),4=>Yii::t('app','year')], array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
			</div>			
			
			
		</div>
		
		<!-- Loyalty start -->	 
		<div class="row">
		<div class="col-md-12 col-lg-12"> 
			<label class="control-label" for="temp-id"><?= Yii::t('app','Loyalty') ?></label>
		</div>
		</div>      
		<div class="row">			
			<div class="col-md-2 col-lg-2">
				<?= Yii::t('app','User has scanned their loyalty ID') ?>
			</div>
   
			<div class="col-md-2 col-lg-2">
				<?= $form->field($model, 'Loyalty1')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
			</div>
			<div class="col-md-2 col-lg-2">
			<?= Yii::t('app','times 	in the past') ?>
			</div>
			<div class="col-md-2 col-lg-2">
				<?= $form->field($model, 'Loyalty2')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
			</div>
			<div class="col-md-3 col-lg-3">
				<?= $form->field($model, 'Loyalty3')->dropDownList([1=>Yii::t('app','days'),2=>Yii::t('app','weeks'),3=>Yii::t('app','months'),4=>Yii::t('app','year')], array('multiple'=>false,  'prompt' => 'select'))->label(false) ?>
			</div>			
			
			
		</div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Save and Close') ?></button>
      </div>
    </div>

  </div>
</div>


<!-- End rule and condition -->

			<!-- Show categories -->
			<label class="control-label" for="store-owner"><?= Yii::t('app','Select Categories') ?> </label>
			<div class="chosentree"></div> 
			<div class="help-block"></div>
<!-- start offer form edit -->
<?php if(isset($offermodel)) {  ?>
<div class="box box-success">
<h3><?= Yii::t('app',' Update Offer:-') ?> </h3>

	
	<?= $form->field($offermodel, 'psaoffertype')->dropDownList(['1' =>Yii::t('app','PSA (info)'), '2' =>Yii::t('app','PSA (deal)')], 
						['prompt'=>Yii::t('app','..Select..')])->label(Yii::t('app','PSA Type')); ?>
						
    <?= $form->field($offermodel, 'offerTitle')->textarea(); ?>
  
    <?= $form->field($offermodel, 'campaignID' )->hiddenInput(['value' => $_GET['id']])->label(false); ?>
    
	<?= $form->field($offermodel, 'offerText')->textarea(); ?>
	
	<?= $form->field($offermodel, 'iphoneNotificationText')->textarea(); ?>
		
	<?php
	 
	  if($offermodel->photo!=''){
		 $offermodel->imageFile = $offermodel->photo;
		 echo '<img src="'.$offermodel->photo.'"/>';
		 } ?>
		 	
	 <?= $form->field($offermodel, 'imageFile')->widget(Widget::className(), [
    'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
    	'width'=>132,
		'height'=>132]) ?>
	 </div>
	 <?php } ?>
<!-- end offer form edit -->
   
     </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 

$this->registerJs(

    '
var loadChildren = function(node) {
	 
JSONObject =JSON.parse(\' '.$categoryTree.'\');	 

	 //JSONObject = JSON.parse(\'{"id":"01","title":"Node 01","has_children":true,"checked":true,"level":1,"children":[{"id":"011","title":"Node 011","name":"paras","has_children":true,"level":2,"children":[{"id":"0111","title":"Node 0111","has_children":false,"level":3,"children":[]}]}]}\');

	 //for(i=0;i<JSONObject.length;i++)
	 
		node.children.push(JSONObject);	 
	 console.log(node);
	 
        return node;
      };

$(document).ready(function() {
        $("div.chosentree").chosentree({
            width: 500,
            deepLoad: true,
              showtree: true,
            load: function(node, callback) {
                    setTimeout(function() {
          callback(loadChildren(node));
       }, 1000);
            }
        });
});
		'
);
    
    
$this->registerJsFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.js', ['position' => \yii\web\View::POS_END]);
$this->registerCssFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.css', ['position' => \yii\web\View::POS_END]);
//$this->registerJsFile(Yii::$app->homeUrl.'store-address-suggest.js', ['position' => \yii\web\View::POS_END]);
?>
<script type="text/javascript">
	$('#workorders-rulecondition').click(function(){
    if (this.checked) {
       $('#show-popup-model').click();
    }
}) 
</script>
