<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model backend\models\Workorders */

$this->title = Yii::t('app','Create QR Image');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Workorders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-plus"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                
                <div class="panel-before" >    
                <div class="pull-right">
        <div role="toolbar" class="btn-toolbar kv-grid-toolbar">
            <div class="btn-group"><a title="<?= Yii::t('app','All Workorders') ?>" href="<?=Yii::$app->urlManager->createUrl(['workorders/index',])?>" class="btn btn-primary"><i class="glyphicon glyphicon-tasks"></i></a> 
            <a data-pjax="0" title="<?= Yii::t('app','Reset Grid') ?>" href="<?=Yii::$app->urlManager->createUrl(['workorders/create',])?>" class="btn btn-default"><i class="glyphicon glyphicon-repeat"></i></a></div>


        </div>    
    </div>
    
    <div class="clearfix"></div></div>
                
              

<div class="workorders-form">

    <?php
  
    $form = ActiveForm::begin([
                'options' => [                 
                    'enctype' =>'multipart/form-data'
                ]
    ]);
    ?>
    
    <div class="box-body">
      <div class="form-group">
		  
	 <?php //echo $form->errorSummary($model); ?>
	 
	 
	
	<?= $form->field($model, 'qrtitle' )->textInput(); ?>
	    
	
     </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>


</div>



              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
        
 


	
	
     
