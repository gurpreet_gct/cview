<div class="kv-container">
            <div class="container">	
	<div class="panel panel-default panel-body">
		<h2>PRIVACY STATEMENT - krajee.com</h2>
		<hr>
		<p>Your privacy is important to krajee.com. This privacy statement provides information
		about the personal information that krajee.com collects across its sites, and the ways in 
        which krajee.com uses that personal information.</p>

		<h3>Personal information collection</h3>

		<p>krajee.com may collect and use the following kinds of personal information from people that visit our website, blog, or app across our sub domains:</p>

		<ul>
			<li>information that you provide for the purpose of subscribing to the website
			services including announcements, blog posts, and comments;</li>

			<li>any other information that you send to krajee.com through the contact form;</li>
		</ul>

		<h3>Using personal information</h3>
	
		<p>krajee.com may use your personal information to:</p>

		<ul>
			<li>administer this website;</li>

			<li>personalize the website for you;</li>

			<li>contact you for providing technical/other support related to this website;</li>
			
		</ul>
        
        <h3>Protecting visitor information</h3>
        <p>krajee.com does not use vulnerability scanning and/or scanning to PCI standards. krajee.com does not use an SSL certificate.
       krajee.com only provides articles and information, and we never ask for personal or private information. krajee.com does not also need 
       or use sensitive financial data (like credit cards) nor does krajee.com process or share this.</p>

       <p>In addition to the disclosures reasonably necessary for the purposes identified
		elsewhere above, krajee.com may disclose your personal information to the extent that it is
		required to do so by law, in connection with any legal proceedings or prospective legal
		proceedings, and in order to establish, exercise or defend its legal rights.</p>

		<h3>Cookies</h3>

		<p>Cookies are used by krajee.com where required as part of its website design. Cookies are small files that a site or its service provider transfers to your 
        computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser 
        and capture and remember certain information. They are also used to help us understand your preferences based on previous or current site 
        activity, which enables us to provide you with improved services. krajee.com may also use cookies to help compile aggregate data about site traffic and 
        site interaction so that it can offer better site experiences and tools in the future.</p>
        
        <h3>Usage of cookies</h3>
        <p>Cookies are used by krajee.com for:</p>
        <ul>
            <li>Keeping track of advertisements.</li>
            <li>Compiling aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. krajee.com may also use trusted third party services that track this information on its behalf.</li>
        </ul>
        A visitor can choose to have his / her computer warn them each time a cookie is being sent, or he / she can choose to turn off all cookies. He / she can do this through his / her browser (like Internet Explorer) settings. Each browser is a little different, so each visitor needs to 
        look at his / her browser's Help menu to learn the correct way to modify his / her cookies.

		<h3>Disabling cookies</h3>

		<p>If a user disable cookies off, some features on this website will be disabled. It will turn off some of the features that make the site 
          experience more efficient and some of krajee.com demos and services will not function properly. Some of the Yii Extension demos and 
          JQuery plugins on krajee.com need cookies to be enabled to run properly.</p>

		
		<h3>Third Party Disclosure</h3>
        <p>krajee.com does not sell, trade, or otherwise transfer to outside parties your personally identifiable information.</p>
        
		<h3>Third party links</h3>
        <p>Occasionally, at discretion of krajee.com, third party links, products or services may be included on its websites. These third party sites have 
           separate and independent privacy policies. krajee.com therefore has no responsibility or liability for the content and activities of these 
           linked sites. Nonetheless, krajee.com seeks to protect the integrity of its site and welcome any feedback about these sites.</p>

        <h3>Google</h3>
        <p>Google's advertising requirements can be summed up by <a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">Google's Advertising Principles</a>. They are put in place to provide a positive experience for users. 
        krajee.com uses Google AdSense Advertising on its website.

        Google, as a third party vendor, uses cookies to serve ads on the sites of krajee.com. Google's use of the DART cookie enables it to serve ads to krajee.com users 
        based on their visit to krajee.com sites and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network 
        privacy policy.</p>

        <p>krajee.com along with third-party vendors, such as Google use first-party cookies (such as the Google Analytics cookies) and third-party 
        cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions, 
        and other ad service functions as they relate to krajee.com website.

        <h4>Opting out:</h4>
        <p>Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, one can opt out by visiting 
        the Network Advertising initiative opt out page or permanently using the Google Analytics Opt Out Browser add on.</p>
        
        <h3>California Online Privacy Protection Act</h3>
		<p>CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's 
        reach stretches well beyond California to require a person or company in the United States (and conceivably the world) that operates websites 
        collecting personally identifiable information from California consumers to post a conspicuous privacy policy on its website stating exactly 
        the information being collected and those individuals with whom it is being shared, and to comply with this policy. - 
        See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf</p>
        
        <h4>According to CalOPPA, krajee.com agrees to the following:</h4>
        <ul>
        <li>Users can visit our site anonymously.</li>
        <li>Once this privacy policy is created, krajee.com will add a link to it on its home page and on most significant pages in its footer.</li>
        <li>The Privacy Policy link for krajee.com includes the word 'Privacy', and can be easily be found on the page specified above.</li>
        <li>Users will be notified of any privacy policy changes on krajee.com Privacy Policy Page</li>
        <li>Users are able to change their personal information wherever collected by sending krajee.com a feedback on the contact form.</li>
        </ul>

		<h3>Do not track signals</h3>
        <p>krajee.com honors do not track signals and does not track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.</p>

		<h3>Third party behavioral tracking</h3>
        <p>It is also important to note that krajee.com allows third party behavioral tracking</p>


		<h3>COPPA (Children Online Privacy Protection Act)</h3>
        <p>When it comes to the collection of personal information from children under 13, the Children's Online Privacy Protection Act (COPPA) puts parents 
        in control. The Federal Trade Commission, the nation's consumer protection agency, enforces the COPPA Rule, which spells out what operators of 
        websites and online services must do to protect children's privacy and safety online.</p>
        <p>krajee.com does not specifically market to children under 13.</p>


		<h3>Fair Information Practices</h3>
        <p>The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles 
        and how they should be implemented is critical to comply with the various privacy laws that protect personal information.</p>
        <p><strong>In order to be in line with Fair Information Practices, krajee.com will take the following responsive action, should a data breach occur:</strong></p>
        <ul>
            <li>krajee.com will notify the users via in site notification within 7 business days</li>
            <li>krajee.com also agrees to the individual redress principle, which requires that individuals have a right to pursue 
            legally enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not 
            only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or a government 
            agency to investigate and/or prosecute non-compliance by data processors.</li>
        </ul>

		<h3>Contacting krajee.com</h3>
        <p>If you have any questions about this privacy policy or krajee.com's treatment of your
		personal information, please write by email to feedback@krajee.com</p>

        <h3>Updating this statement</h3>
		<p>krajee.com may update this privacy policy by posting a new version on this website.</p>
		<p>You should check this page occasionally to ensure you are familiar with any changes.</p>
        
        <em>Last Edited On: 2014-11-21</em>
	</div>
</div>        </div>
