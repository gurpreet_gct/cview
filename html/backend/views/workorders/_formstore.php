<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use backend\models\Beacongroup;
use common\models\Store;
use common\models\Workorderpopup;
use common\models\Workorders;
use common\models\Qr;
use common\models\Country;
use common\models\Product;
use common\models\Profile;
use backend\models\Beaconlocation;
use backend\models\Usermanagement;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use paras\cropper\Widget;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\Workorders */
/* @var $form yii\widgets\ActiveForm */
$baseurl = Yii::$app->getUrlManager()->getBaseUrl();
?>

<div class="workorders-form">
    <?php

		$form = ActiveForm::begin([
					'options' => [
						'enctype' =>'multipart/form-data'
					]
		]);

    ?>
    <div class="box-body">
		<div class="form-group">
			<?php //echo $form->errorSummary($model);  ?>

	 	<!-- start offer form edit -->
	<?= $form->field($model, 'iphoneNotificationText')->textarea(); ?>
	<?php

	$getcmpType = workorders::find()->select(['type','workorderpartner'])->where(['id' => $_GET['id']])->one();

	if(isset($getcmpType->workorderpartner)) {

		$partnerinfo = Profile::find()->select(['companyName','brandAddress'])->where(['user_id' => $getcmpType->workorderpartner])->one();

		/* GET partner info from profile model */

		if($partnerinfo->companyName){

			$partnerName = $partnerinfo->companyName;

		}else{

			$partnerName = Yii::t('app','Not Selected');

		}

		if($partnerinfo->brandAddress){

				$partnerAddress = $partnerinfo->brandAddress;
		}else{

			$partnerAddress = Yii::t('app','Not Selected');

		}

	}

 ?>


	<!-- Change offer title as per workorder type  start -->
	<?php

	/* if stamp card  */

	  echo $form->field($model, 'offerTitle' )->textInput(['value' => $partnerName,'readonly' => true])->label(Yii::t('app','Advertiser name'));
	  echo $form->field($model, 'partnerAddress' )->textInput(['value' => $partnerAddress,'readonly' => true])->label(Yii::t('app','Advertiser address'));


	?>
	<?= $form->field($model, 'readTerms')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic',

    ])->label(Yii::t('app','Read More')) ?>

	<?php

	  if($model->logo!=''){
		 $model->imageFilelogo = $model->logo;
		 //echo '<img src="'.$model->logo.'"/>';
		 } ?>

	 <?= $form->field($model, 'imageFilelogo')->widget(Widget::className(), [
    'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
    	'width'=>132,
		'height'=>132]) ?>

	<?php if($getcmpType->type == 1 || $getcmpType->type == 4) { ?>

	<?php echo $form->field($model, 'uploadTypes')->radioList(['1' => Yii::t('app','Add image'), '2' => Yii::t('app','Add Video')]); ?>

	<?php
			echo '<div id="image-block" style="display:none;">';
			  if($model->photo!=''){
				 $model->imageFile = $model->photo;
				 //echo '<img src="'.$model->photo.'"/>';
				}

			echo $form->field($model, 'imageFile')->widget(Widget::className(), [
    'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
    	'width'=>132,
		'height'=>132]);
			echo '</div>';

			echo '<div id="video-block" style="display:none;">';
			echo $form->field($model, 'videokey')->textInput();
			echo '</div>';
	   ?>
	 <?php }elseif($getcmpType->type == 2 || $getcmpType->type == 5){

				echo '<div id="image-blocks">';

				$model->uploadTypes=1;
				 // escape validation

				//echo $form->field($model, 'offertype')->radioList(array('4'=>'Offer Text (Free, OFF etc.)'),['id' => 'customoffer-id','class' => 'customoffer-id']);

				echo "<div style='display:none'>". $form->field($model, 'uploadTypes')->radioList(['1' => Yii::t('app','Add image'), '2' => Yii::t('app','Add Video')])."</div>";

				if($model->photo!=''){

					$model->imageFile = $model->photo;
					//echo '<img src="'.$model->photo.'"/>';

				}

				echo $form->field($model, 'imageFile')->widget(Widget::className(), [
    'uploadUrl' => Url::toRoute('workorders/uploadPhoto'),
    	'width'=>132,
		'height'=>132])->label(Yii::t('app','Photo'));
				echo '</div>';

				// $model->offertype = 4;


		}

	 ?>


	 </div>








     </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
		<?= Html::a('BACK',['update','id'=>$_GET['id']],['class'=>'btn btn-default','id'=>1]); ?>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','SAVE AND EXIT'),['id'=>'update-offer-data','class' =>'btn btn-success']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php


		$staturl =$baseurl.'/workorders/productprice';
		$staturlcombo =$baseurl.'/workorders/productpricecombo';

?>

<script type="text/javascript">



$(document).ready(function(){
	var product = document.getElementById("workorderpopup-product_id");
	if(product) {

				var values = product.options[product.selectedIndex].value;

				var id = $('#workorderpopup-campaignid').val();

			$.ajax({
			method: "POST",
			url: "<?php echo $staturl; ?>",
			data: { id: id, value: values }
	    	})
		    .done(function(result) {
			   $('#workorderpopup-productprice').val(result);
		});
	}

	$("#workorderpopup-product_id").on("change", function () {
		var values = this.value;
		var id = $('#workorderpopup-campaignid').val();

			$.ajax({
			method: "POST",
			url: "<?php echo $staturl; ?>",
			data: { id: id, value: values }

	})
	.done(function( result ) {

	   $('#workorderpopup-productprice').val(result);
	  });



});

/* get combo product price */

	var comboproduct = document.getElementById("workorderpopup-product_id_combo");
	if(comboproduct) {

				var values = comboproduct.options[comboproduct.selectedIndex].value;

				var id = $('#workorderpopup-campaignid').val();

			$.ajax({
			method: "POST",
			url: "<?php echo $staturlcombo; ?>",
			data: { id: id, value: values }
	    	})
		    .done(function(result) {
			   $('#workorderpopup-comboproductprice').val(result);
		});
	}

	$("#workorderpopup-product_id_combo").on("change", function () {
		var values = this.value;
		var id = $('#workorderpopup-campaignid').val();

			$.ajax({
			method: "POST",
			url: "<?php echo $staturlcombo; ?>",
			data: { id: id, value: values }

	})
	.done(function( result ) {

	   $('#workorderpopup-comboproductprice').val(result);
	  });



	});

	/*$('#update-offer-data').on('click',function(){
			var inputval = $("#offerwas4").val();
				if(inputval.length <=0){
				$('.form-group.field-workorderpopup-offerwas').addClass(' has-error');
				$("#offerwas4").prop('required',true);
			}
		});*/
});


var combofferproduct = $( "#workorderpopup-comboproducttype option:selected" ).val();

	if(combofferproduct==2){
		document.getElementById("Add-combo-product").style.display = "block";
	}else{
		document.getElementById("Add-combo-product").style.display = "none";
	}

$("#workorderpopup-comboproducttype").on("change", function () {
		var values = this.value;
		if(values==2){
		document.getElementById("Add-combo-product").style.display = "block";
		}else{
			document.getElementById("Add-combo-product").style.display = "none";
		}
});

var getcombotype = $('input:radio[name="Workorderpopup[offertext1]"]:checked').val();

	if(getcombotype==5){
		document.getElementById("select-combo-type").style.display = "block";

	}else{
		document.getElementById("select-combo-type").style.display = "none";
		document.getElementById("Add-combo-product").style.display = "none";

	}


$("input:radio[name='Workorderpopup[offertext1]']").on("change", function () {
	var values = this.value;
	if(values==5){
		document.getElementById("select-combo-type").style.display = "block";
	}else{
		document.getElementById("select-combo-type").style.display = "none";
		document.getElementById("Add-combo-product").style.display = "none";
	}


});

$("#workorderpopup-comboproducttype").on("change", function () {
	var values = this.value;

	var pval = document.getElementById("workorderpopup-product_id");
	var pvalue = pval.options[pval.selectedIndex].value;
	$("#workorderpopup-product_id_combo option[value="+pvalue+"]").remove();

});

$("#workorderpopup-product_id_combo").on("change", function () {
	var values = this.value;

	var pval = document.getElementById("workorderpopup-product_id");
	var pvalue = pval.options[pval.selectedIndex].value;
	$("#workorderpopup-product_id_combo option[value="+pvalue+"]").remove();

});

</script>

<style>
#combo-get {

    font-weight: bold;
    margin-left: 9px;
    padding-left: 25px;
    padding-right: 0;
}
.error-summary {
    border: 1px solid #cc0000;
    color: #cc0000;
    padding: 4px;
}

</style>
