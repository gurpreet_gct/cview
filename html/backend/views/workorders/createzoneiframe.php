<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\GeoZone;
use common\models\Workorders;
use common\models\Deal;


$this->title =  Yii::t('app','').ucfirst('Geolocation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Workorders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$dealIds 		= Deal::find()
				->select('workorder_id')
				->asArray()
				->all();
				
if(!empty($dealIds)){
	
	foreach($dealIds as $wid){
		$workIds[] =$wid['workorder_id']; 
	}
	
	
	$condWhere = ['id' => $workIds];
	
}else{
	
	$condWhere = [];
}

// andWhere(['status'=>1])->

$zone_name 	=  ArrayHelper::map(Workorders::find()->where($condWhere)->andWhere(['>=','dateTo',strtotime(date('Y-m-d H:i:s'))])->all(), 'id', 'name'); 

$selPartner = Yii::$app->request->get('id')?[Yii::$app->request->get('id')]:[];
$selDeal	= Yii::$app->request->get('did')?[Yii::$app->request->get('did')]:[];
$dealId	= Yii::$app->request->get('did')?Yii::$app->request->get('did'):'';

$area = array();

$area = ArrayHelper::map(GeoZone::find()->groupBy('area_name','deal_id')->all(),'id', 'area_name');
$geoZone = Html::dropDownList("area_name",$selDeal,$area,['id'=>'area_name4','class' => 'form-control','prompt' => Yii::t('app',' All ')]);


$dealSelect =  Html::dropDownList("deal_id",$selDeal,$zone_name,['id'=>'workOrderId','class' => 'form-control','prompt' => Yii::t('app',' All ')]);

$partners 	=  Html::dropDownList("partner_id",$selPartner, $partner,['id'=>'partner_id','class' => 'form-control','prompt' => Yii::t('app',' All ')]);
$partNerId = Yii::$app->request->get('partnerid');
$tempwororderid = Yii::$app->request->get('tempwororderid'); // temp workorder id
$latandlang = GeoZone::find()->where('deal_id='.$tempwororderid)
				->orderBy('id desc')->asArray()->one();
			
?>
	<div class="row">
		<!-- left column -->
        <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-tasks"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
				<!-- general form elements -->
				
				<!-- start offer model  -->
				<div class="box box-success">
					<input type="hidden" id="gotourl" value="<?php echo '/workorders/createzoneiframe?partnerid='.$partNerId.'&tempwororderid='.$tempwororderid;?>">
				
					<input type="hidden" id="partner_id" name="partner_id" value="<?php echo $partNerId;?>">
					<input type="hidden" id="workId" name="workId" value="<?php echo $tempwororderid ;?>">
					<input type="hidden" id="workOrderId" name="deal_id" value="<?php echo $tempwororderid;?>">
					<input type="hidden" id="notification_limit2" name="notification_limit" value="1">
				<div class="form-group field-role-rulename margin" >
						
						<label for="role-rulename" class="control-label">Area Name</label>
						 <?php echo $geoZone; ?>
						
					</div>
				<b>OR</b>
					<div class="form-group field-role-rulename margin">
						
						<label for="role-rulename" class="control-label">Set map by</label>
						<br />
						<input type="radio" name="addr_type" value="address" checked="checked" class="check_addr_type"/>Address
						<input type="radio" id="addr_type" name="addr_type" value="latlng" class="check_addr_type" /> Latitude/Longitude
					</div>
					<div class="form-group field-role-rulename margin" id="txt_addr">
						
						<label for="role-rulename" class="control-label">Enter address for map </label>
						<input type="text" id="map_address" name="map_address" class="form-control"/>
						
					</div>
					<div class="form-group field-role-rulename margin" id="latilong" style="display:none;">
						
						<label for="role-rulename" class="control-label">Enter Latitude/Longitude </label>
						
						<input type="hidden" id="latitude1" value="<?php echo $latandlang['latitude'];?>"/>
						<input type="hidden" id="longitude1" value="<?php echo $latandlang['longitude'];?>"/>
						<input type="hidden" id="latitude" name="latitude" class="form-control" placeholder="Enter Latitude"/>
						<br />
						<input type="hidden" id="longitude" name="longitude" class="form-control" placeholder="Enter Longitude"/>
					</div>
					<div class="form-group field-role-rulename margin">
						<button id="go_to" name="go_to" class="btn btn-small btn-primary" >Go to Map</button>
						<button id="geo_id2" name="geo_id2" class="btn btn-small btn-primary" style="display:none;">Save this area</button>
					</div>  
					<div class="panel-heading">    
						<div id="map-canvas" style="height:768px;"></div>
					</div>
				<!------------end here --------------------------->
				
				<input type="hidden" id="base_url" name="base_url" value="<?php echo \Yii::$app->request->BaseUrl;?>" />
				</div><!-- /.box -->
			</div><!--/.col (right) -->
				<div class="loading-image text-center">
					<?= Html::img('@web/images/loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
				</div>
				<input type="hidden" id="prev_zoom" name="prev_zoom" value="12" />
				<!-- <input type="hidden" id="map_center_lat" name="map_center_lat" value="-37.814107" />
				<input type="hidden" id="map_center_lng" name="map_center_lng" value="144.963280" /> -->
		</div>
	</div>   <!-- /.row -->

<?php $baeUrl =  \Yii::$app->request->BaseUrl;?>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCJAWURcRMIgLBQ6Wt0X7WHrUWk68E5N4g&libraries=places,drawing"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>zone_map.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var longitude 	= $('#longitude1').val();
		var latitude 	= $('#latitude1').val();
		if(longitude && latitude){
			setTimeout(function(){	
			$('#addr_type').click();
			$('#longitude').val(longitude);
			$('#latitude').val(latitude);			
			$('#go_to').click();
			}, 2000);
		}
	});
	
	 $('#area_name4').change(function(){
		 	var Id = $(this).val();
		if(Id=='' || Id == null){
			alert('Please select a deal');
			return false;
		} 
		$('#geo_id2').show();	
		$.ajax({
			url:'<?php  echo $baeUrl;?>/workorders/namelist',
			type:'POST',
			data:{Id:Id}
		})
		.done(function(response){
			var result = JSON.parse(response);
			$('#latitude').val(result.latitude);
			$('#longitude').val(result.longitude);
			$('#go_to').click();
		});
 });
		 
		 $('#geo_id2').click(function(){
				var Id = $('#area_name4').val();
				var workOrderId = $('#workOrderId').val();
				var partner_id = $('#partner_id').val();
				
				$.ajax({
					url:'<?php  echo $baeUrl;?>/workorders/savelist',
					type:'POST',
					data:{Id:Id,workOrderId: workOrderId,partner_id:partner_id}
					})
				.done(function(response){
					var result = JSON.parse(response);
					alert(result.message);
				});
		 });
	

	
	
</script>
<style>
	.loading-image.text-center {    left: 40%;    position: absolute;    top: 40%;
}
</style>
