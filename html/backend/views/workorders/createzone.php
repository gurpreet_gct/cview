<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\GeoZone;
use common\models\Workorders;
use common\models\Deal;
/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

$usertype = Yii::$app->user->identity->user_type;
$agency_id = Yii::$app->user->identity->advertister_id;
$id = Yii::$app->user->identity->id;
$partenerId = $usertype == 7 ? $id : $agency_id;



$this->title =  Yii::t('app','').ucfirst('Geolocation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Workorders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$dealIds 		= Deal::find()
				->select('workorder_id')
				->asArray()
				->all();
				
if(!empty($dealIds)){
	
	foreach($dealIds as $wid){
		$workIds[] =$wid['workorder_id']; 
	}
	
	
	$condWhere = ['id' => $workIds];
	
}else{
	
	$condWhere = [];
}

// andWhere(['status'=>1])->

$zone_name 	=  ArrayHelper::map(Workorders::find()->where($condWhere)->andWhere(['>=','dateTo',strtotime(date('Y-m-d H:i:s'))])->all(), 'id', 'name'); 

// var_dump(Yii::$app->request->get('id'));
// die;

$selPartner = Yii::$app->request->get('id')?[Yii::$app->request->get('id')]:[];

$selDeal	= Yii::$app->request->get('did')?[Yii::$app->request->get('did')]:[];
$dealId	= Yii::$app->request->get('did')?Yii::$app->request->get('did'):'';


$dealSelect =  Html::dropDownList("deal_id",$selDeal,$zone_name,['id'=>'workOrderId','class' => 'form-control','prompt' => Yii::t('app',' All ')]);

$data = array();
if(!empty($dealId)){
$data =	ArrayHelper::map(GeoZone::find()->where('deal_id='.$dealId)->all(),'id', 'area_name');
	}
$geoZone = Html::dropDownList("area_name2",$selDeal,$data,['id'=>'area_name2','class' => 'form-control','prompt' => Yii::t('app',' All ')]);

$area = array();
$area = ArrayHelper::map(GeoZone::find()->groupBy('area_name','deal_id')->all(),'id', 'area_name');
	
 if(Yii::$app->user->identity->user_type==1){
$geoZone1 = Html::dropDownList("area_name",$selDeal,$area,['id'=>'area_name3','class' => 'form-control','prompt' => Yii::t('app',' All ')]);
}else{
$area = ArrayHelper::map(GeoZone::find()->where(['agency_id' => $id])->groupBy('area_name','deal_id')->all(),'id', 'area_name');
//print_r($area);die();
$geoZone1 = Html::dropDownList("area_name",$selDeal,$area,['id'=>'area_name3','class' => 'form-control','prompt' => Yii::t('app',' All ')]);
}

	
	

  if(Yii::$app->user->identity->user_type==1){
$partners 	=  Html::dropDownList("partner_id",$selPartner, $partner,['id'=>'partner_id','class' => 'form-control','prompt' => Yii::t('app',' All ')]);
 }else{ ?>
	<input type="hidden" id="partner_id" name="partner_id" value="<?php echo $partenerId;?>">
	<?php	}?>	 
	


	
	



   


	<div class="row">
		<!-- left column -->
        <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-tasks"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
				<!-- general form elements -->
				
				<!-- start offer model  -->
				<div class="box-body box-success">
					<div class="form-group field-role-rulename">
						<?php if(Yii::$app->user->identity->user_type==1){?>
						<label for="role-rulename" class="control-label">Select Partner</label>
						
						 <?php echo $partners; ?>
						 <?php }else{ ?>
						 <?php // echo $partners; ?>
							<?php	}?>	
					</div>
					
					<div class="form-group field-role-rulename extra-input">
						<label for="role-rulename" class="control-label">Select Deal</label>
						<?php echo $dealSelect;?>
						<input type="hidden" id="workId" name="workId" value="<?php echo $dealId;?>">
					</div>
					<div class="form-group field-role-rulename extra-input">
						<label for="role-rulename" class="control-label">Select Area</label>
						<?php echo $geoZone;?>
						<input type="hidden" id="workId" name="workId" value="<?php echo $dealId;?>">
					</div>
					<div class="form-group field-role-rulename extra-input limit-update">
						
						<label for="role-rulename" class="control-label">Notification limit in a day </label>
						<input type="text" id="notification_limit" name="notification_limit" class="form-control" />
					</div>
					<div class="form-group field-role-rulename extra-input limit-update">
						<button id="save_limit" name="save_limit" class="btn btn-small btn-primary" >Save Notification</button>
					</div>
						<div class="form-group field-role-rulename">
						
						<label for="role-rulename" class="control-label">Assign existing area </label>
						 <?php echo $geoZone1; ?>
						
					</div>
					<div class="form-group field-role-rulename savebtnid" style="display:none;">
						<button id="area_id" name="area_id" class="btn btn-small btn-primary" >Save this area on selected deal</button>
					</div> 
				<b>OR</b>
					
					<div class="form-group field-role-rulename">
						
						<label for="role-rulename" class="control-label">Set map by</label>
						<br />
						<input type="radio" name="addr_type" value="address" checked="checked" class="check_addr_type"/>Address
						<input type="radio" id="addr_type" name="addr_type" value="latlng" class="check_addr_type" /> Latitude/Longitude
					</div>
					<div class="form-group field-role-rulename" id="txt_addr">
						
						<label for="role-rulename" class="control-label">Enter address for map </label>
						<input type="text" id="map_address" name="map_address" class="form-control"/>
						
					</div>
					<div class="form-group field-role-rulename" id="latilong" style="display:none;">
						
						<label for="role-rulename" class="control-label">Enter Latitude/Longitude </label>
						
						<input type="hidden" id="latitude" name="latitude" class="form-control" placeholder="Enter Latitude"/>
						<input type="hidden" id="redirect" name="redirect" class="form-control" />
						<br />
						<input type="hidden" id="longitude" name="longitude" class="form-control" placeholder="Enter Longitude"/>
						<input type="hidden" id="geolocation_id" name="geolocation_id" value="<?php echo $model->id;?>" />
					</div>
					<div class="form-group field-role-rulename">
						<button id="go_to" name="go_to" class="btn btn-small btn-primary" >Go to Map</button>
							<button id="geo_id" name="geo_id" class="btn btn-small btn-primary" style="display:none;" >Save and Close</button>
						
						
					</div>
					<div class="panel-heading">    
						<div id="map-canvas" style="height:768px;"></div>
					</div>
				<!------------end here --------------------------->
				
				<input type="hidden" id="base_url" name="base_url" value="<?php echo \Yii::$app->request->BaseUrl;?>" />
				</div><!-- /.box -->
			</div><!--/.col (right) -->
				<div class="loading-image text-center">
					<?= Html::img('@web/images/loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
				</div>
				<input type="hidden" id="prev_zoom" name="prev_zoom" value="17" />
				<!-- <input type="hidden" id="map_center_lat" name="map_center_lat" value="-37.814107" />
				<input type="hidden" id="map_center_lng" name="map_center_lng" value="144.963280" /> -->
		</div>
	</div>   <!-- /.row -->

<?php $baeUrl =  \Yii::$app->request->BaseUrl;


?>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCJAWURcRMIgLBQ6Wt0X7WHrUWk68E5N4g&libraries=places,drawing"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>zone_map.js"></script>
<script type="text/javascript">
	var partnerId  = '';
	$('#partner_id').change(function(){
		partnerId = $(this).val();

	});
	$('#workOrderId').change(function(){
		var dealId = $(this).val();
		if(dealId==null || dealId==''){
			 dealId = '';
		}
		if(partnerId==null || partnerId==''){
			partnerId = ''
		}
		var url = '<?php echo $baeUrl; ?>/workorders/createzone?id='+partnerId+'&did='+dealId;
		$('#redirect').val(url);
		// show locations
	 $('#area_name2').empty(); 
		if(dealId){		
			$.ajax({
				url:'<?php echo $baeUrl; ?>/workorders/arealist',
				type:'POST',
				data:{dealId:dealId}
			})
			.done(function(respose){
				 var div_data1="<option value=''>select</option>";
				 $(div_data1).appendTo('#area_name2'); 
				var resutl = JSON.parse(respose);
				$.each(resutl.data,function(key,value){				
				 var div_data="<option value="+value.id+">"+value.area_name+"</option>"; 
				 $(div_data).appendTo('#area_name2'); 	
				});
			});
			
		}
	});
	
	$('#area_name2').change(function(){
		var Id = $(this).val();
		$.ajax({
			url:'<?php  echo $baeUrl;?>/workorders/maplist',
			type:'POST',
			data:{Id:Id}
		})
		.done(function(response){
			var result = JSON.parse(response);
			$('#latitude').val(result.latitude);
			$('#longitude').val(result.longitude);
			$('#go_to').click();
		});
		
	});
	
	
	 $('#geo_id').click(function(){
		window.location.href = '<?php  echo $baeUrl;?>/geo/index';
	});
	
			 $('#area_id').click(function(){
				var Id = $('#area_name3').val();
				var workOrderId = $('#workOrderId').val();
				var partner_id = $('#partner_id').val();
				if(partner_id=='' || partner_id == null){
					alert('Please select a partner');
						return false;
				}
				if(workOrderId=='' || workOrderId==null){				
					alert('Please select a deal');
						return false;
				}
				else{						
					$.ajax({
					url:'<?php  echo $baeUrl;?>/workorders/savelist',
					type:'POST',
					data:{Id:Id,workOrderId: workOrderId,partner_id:partner_id}
					})
					.done(function(response){
					var result = JSON.parse(response);
						alert(result.message);
					});
				}
		 });
	
		$('#area_name3').change(function(){
			var setval = $(this).val();
			if(setval!='' || setval!=null){			
			$('.margin.savebtnid').show();
			}
		});
</script>
<style>
.loading-image.text-center {    left: 40%;    position: absolute;    top: 40%;}
#geo_id {  margin-left: 56px; }
</style>


