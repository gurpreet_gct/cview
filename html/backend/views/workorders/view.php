<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model backend\models\Workorders */
$this->title =  Yii::t('app','View : ').ucfirst($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Workorders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$visible = 0;
if($userType==1){
$visible = 1;
}
else{
	$visiable = 0;
}
?>
  <div class="row">
		<!-- left column -->
        <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">
                    <div class="pull-right">
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-tasks"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
           <div class="box-header with-border">
               <h3 class="box-title"> <?php // Html::encode($this->title) ?> </h3>
               <p>
				<div class="row">
                    <div class="col-xs-12 col-sm-7">
               <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
                   'class' => 'btn btn-danger',
                   'data' => [
                       'confirm' => Yii::t('app','Are you sure you want to delete this item?') ,
                       'method' => 'post',
                   ],
               ]) ?>
               <?= Html::a(Yii::t('app','Cancel'), ['index', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
               <?= Html::a(Yii::t('app','OK'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
               <?php if($model->type != 5) { ?>
               <?= Html::a(Yii::t('app','Preview offer'), 'JavaScript:void(0);', ['class' => 'btn btn-info', 'data-toggle'=>'modal', 'data-target' =>'#myModal']) ?>
               <?= Html::a(Yii::t('app','Preview pass'), 'JavaScript:void(0);', ['class' => 'btn btn-info', 'data-toggle'=>'modal', 'data-target' =>'#myModalpass']) ?>
               <?php # Html::a(Yii::t('app','PDF export'), ['pdfdetailexport', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
               <?php }?>
               </div>
               <div class="col-xs-12 col-sm-5">
                   <?php if($model->type != 5) { ?>
               <div class="dropdown text-right xs-text-left">
                    <button class="btn bg-green dropdown-toggle" type="button" data-toggle="dropdown">PDF export
                       <span class="caret"></span></button>
                           <ul class="dropdown-menu">
                             <li><?= Html::a(Yii::t('app','Content'), ['pdfdetailexport', 'id' => $model->id], ['class' => '']) ?></li>
                             <li><?= Html::a(Yii::t('app','Offer'), ['pdfpopupexport', 'id' => $model->id], ['class' => '']) ?></li>
                             <li><?= Html::a(Yii::t('app','Pass'), ['pdfpassexport', 'id' => $model->id], ['class' => '']) ?></li>
                           </ul>
                         </div>
                   <?php } ?>
                   </div>
               </div>
                   </p>
               <div class="box box-success"></div>
               <h3 class="panel-title">
                   <i class="glyphicon glyphicon-hand-right"></i>  Campaign Detail
               </h3>
             <p style="text-align:right" ><?= Html::a(Yii::t('app','Update/View Geolocation'), ['createzone', 'id' => $model->workorderpartner,'did'=>$model->id], ['class' => 'btn btn-primary']) ?>
               <?= Html::a(Yii::t('app','Update Campaign'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?></p>
           </div><!-- /.box-header -->
                <!-- form start -->
 <div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
             // 'id',
            'name',
            [
            'label' => Yii::t('app','Campaign Type'),
            'visible' => $model->campaigntypename!='' ? true : false,
            'value' => $model->campaigntypename,
             ],
             [
            'label' => Yii::t('app','Partner Name'),
            'visible'=>$visible,
            'value' => $model->partnernameval,
             ],
             [
            'attribute' => Yii::t('app','geoProximity'),
             'visible' => $model->geoproximityname!='' ? true : false,
            'value' => $model->geoproximityname,
             ],
             [
            'label' => Yii::t('app','Country Name'),
            'visible' => $model->countryname->name!='' ? true : false,
            'value' => $model->countryname->name,
             ],
             [
            'visible' => $model->mediaContractNumber!='' ? true : false,
			'attribute' => 'mediaContractNumber',
             ],
             [
            'visible' => $model->noPasses!='' ? true : false,
			'attribute' => 'noPasses',
             ],
             'duration',
             'returnWithin',
            [
            'label' => Yii::t('app','Date From'),
            'value' => $model->dateFrom,
             ],
             [
            'label' => Yii::t('app','Date To'),
            'value' => $model->dateTo,
             ],
           [
            'label' => Yii::t('app','Categories'),
            'value' => implode(",",$categoryTree),
             ],
           [
            'label' => Yii::t('app','User Type'),
            'value' => $model->usertype,
             ],
            [
            'label' => Yii::t('app','Age Group'),
            'value' => $model->agegroupname,
             ],
             [
            'label' => Yii::t('app','Beacon Group'),
            'value' => $model->beacongroupname,
             ],
             [
            'label' => Yii::t('app','Rules/Conditions'),
            'visible' => $model->rulesandconditions!='' ? true : false,
            'value' => $model->rulesandconditions,
             ],
            [
            'label' => Yii::t('app','Dayparting'),
            'visible' => $model->daypartingvalue!='' ? true : false,
            'value' => $model->daypartingvalue,
             ],
             [
            'label' => Yii::t('app','Temprature'),
            'visible' => $model->temparturevalue!='' ? true : false,
            'value' => $model->temparturevalue,
             ],
             [
            'label' => Yii::t('app','Behaviour visit  '),
            'visible' => $model->behaviourvalue!='' ? true : false,
            'value' => $model->behaviourvalue,
             ],
             [
            'label' => Yii::t('app','Behaviour-dwell  '),
            'visible' => $model->behaviourdwellvalue!='' ? true : false,
            'value' => $model->behaviourdwellvalue,
             ],
             [
            'label' => Yii::t('app','On Entry '),
            'visible' => $model->entryvalue!='' ? true : false,
            'value' => $model->entryvalue,
             ],
             [
            'label' => Yii::t('app','On Exit'),
            'visible' => $model->exitvalue!='' ? true : false,
            'value' => $model->exitvalue,
             ],
             [
            'label' => Yii::t('app','Purchase history'),
            'visible' => $model->Purchasevalue!='' ? true : false,
            'value' => $model->Purchasevalue,
             ],
             [
            'label' => Yii::t('app','Loyalty'),
            'visible' => $model->loyaltyvalue!='' ? true : false,
            'value' => $model->loyaltyvalue,
             ],
        ],
    ]) ?>
    </div>
    <!-- start offer model  -->
<div class="box box-success">
	<div class="panel-heading">
		<div class="pull-right"></div>
		<!-- Header Title-->
		<h3 class="panel-title">
			<i class="glyphicon glyphicon-hand-right"> </i> <?= Yii::t('app','Offer Detail') ?>
		</h3>
		<!-- Header Title end -->
		<div class="clearfix"></div>
		<br />
		<p style="text-align:right">
		<?php
			if($model->type == 5 && !isset($offermodel->id))
			{
				echo  Html::a(Yii::t('app','Create Store'), ['createstore', 'id' => $model->id], ['class' => 'btn btn-primary']);
			}
			else if($model->type == 5 && isset($offermodel->id)) {
				echo  Html::a(Yii::t('app','Update Store'), ['updatestore', 'id' => $model->id], ['class' => 'btn btn-primary']);
			}
			else if(!isset($offermodel->id)){
				echo  Html::a(Yii::t('app','Create Offer'), ['createoffer', 'id' => $model->id,'step'=>2], ['class' => 'btn btn-primary']);
			}
			else
			{
				echo  Html::a(Yii::t('app','Update Offer'), ['updateoffer', 'id' => $model->id,'step'=>2], ['class' => 'btn btn-primary']);
			}
		?>
		</p>
	</div>
<?php
	$url_key  = '';
 if($model->type == 4 && isset($offermodel->id))
 {
	if(isset($offermodel->passUrl)){
	$link = $offermodel->passUrl;
	$link_array = explode('/',$link);
	$url_key = end($link_array);
	}
	if($offermodel->browsercheck==1){
		$browsercheckvalue = 'Yes';
	}else{
		$browsercheckvalue = 'No';
	}?>
	<div class="table-responsive">
	<?php echo DetailView::widget([
		'model' => $offermodel,
		'attributes' => [
		[
		'label' => Yii::t('app','Notification Text'),
		'visible' => $offermodel->iphoneNotificationText!='' ? true : false,
		'value' => $offermodel->iphoneNotificationText,
		],
		[
		'label' => Yii::t('app','Ticket event name'),
		'visible' => $offermodel->offerTitle!='' ? true : false,
		'value' =>  $offermodel->offerTitle,
		],
		[
		'label' => Yii::t('app','Event venue'),
		'visible' => $offermodel->eventVenue!='' ? true : false,
		'value' =>  $offermodel->eventVenue,
		],
		[
		'label' => Yii::t('app','Offer was'),
		'value' =>  '$'.$offermodel->offerwas,
		'visible' => $offermodel->offerwas> 0 ? true : false
		],
		[
		'label' => Yii::t('app','Offer now'),
		'visible' => $offermodel->offernow!='' ? true : false,
		'value' =>  '$'.$offermodel->offernow,
		],
		'readTerms:html',
		[
		'attribute'=>'photo',
		'value'=>   $offermodel['photo'] != '' ? $offermodel['photo'] : 'N/A',
		'format' =>[$offermodel['photo'] != '' ?'image':'text' ,$offermodel['photo'] != '' ?['width'=>'100','height'=>'70']:'N/A']
		],
		[
		'attribute'=>'logo',
		'value'=>   $offermodel['logo'] != '' ? $offermodel['logo'] : 'N/A',
		'format' =>[$offermodel['logo'] != '' ?'image':'text' ,$offermodel['logo'] != '' ?['width'=>'70','height'=>'70']:'N/A']
		],
		[
		'attribute'=>'Video Url',
		'visible' => $offermodel->videokey!='' ? true : false,
		'value'=>   $offermodel->videokey,
		],
		[
		'attribute'=>'Video Image',
		'visible' => $offermodel['videoImage']!='' ? true : false,
		'value'=>   $offermodel['videoImage'] != '' ? $offermodel['videoImage'] : 'N/A',
		'format' =>[$offermodel['videoImage'] != '' ?'image':'text' ,$offermodel['videoImage'] != '' ?['width'=>'100','height'=>'70']:'N/A']
		],
		[
		'attribute'=>'Open Video In Browser',
		'visible' => $browsercheckvalue!='' ? true : false,
		'value'=>   $browsercheckvalue,
		],
		[
		'label' => Yii::t('app','URL Key'),
		'visible' => $url_key!='' ? true : false,
		'value' =>  $url_key,
		],
		[
		'label' => Yii::t('app','Event Date'),
		'visible' => $offermodel->eventDate!='' ? true : false,
		'value' =>  $offermodel->eventDate,
		],
		[
		'label' => Yii::t('app','Ticket info'),
		'visible' => $offermodel->ticketinfo!='' ? true : false,
		'value' =>  $offermodel->ticketinfo,
		],
		],
	]);
	?>
	</div>
	<?php
 } // if($model->type == 4 && isset($offermodel->id))
else if($model->type == 2 && isset($offermodel->id))
{
	if(isset($offermodel->passUrl)){
	$link = $offermodel->passUrl;
	$link_array = explode('/',$link);
	$url_key = end($link_array);
	}
	if($offermodel->browsercheck==1){
	$browsercheckvalue = 'Yes';
	}else{
	$browsercheckvalue = 'No';
	} ?>
	<div class="table-responsive">
	<?php 
	echo DetailView::widget([
           'model' => $offermodel,
           'attributes' => [
				[
               'label' => Yii::t('app','Notification Text'),
               'visible' => $offermodel->iphoneNotificationText!='' ? true : false,
               'value' => $offermodel->iphoneNotificationText,
               ],
               [
               'label' => Yii::t('app','Advertiser name'),
               'visible' => $offermodel->advname!='' ? true : false,
               'value' => $offermodel->advname,
               ],
               [
               'label' => Yii::t('app','Advertiser address'),
                'visible' => $offermodel->advaddress!='' ? true : false,
               'value' => $offermodel->advaddress,
               ],
                [
               'label' => Yii::t('app','Offer Text'),
               'value' =>  $offermodel->offerText,
               'visible' => $offermodel->offerText != '' ? true : false
               ],
               [
               'label' => Yii::t('app','Buy Now'),
               'visible' => $offermodel->buynowval!='' ? true : false,
               'value' =>  $offermodel->buynowval
               ],
               'readTerms:html',
               [
               'attribute'=>'photo',
               'visible' =>$offermodel['photo']!='' ? true : false,
               'value'=>   $offermodel['photo'] != '' ? $offermodel['photo'] : 'N/A',
               'format' =>[$offermodel['photo'] != '' ?'image':'text' ,$offermodel['photo'] != '' ?['width'=>'100','height'=>'70']:'N/A']
                         ],
                 [
               'attribute'=>'logo',
               'visible' => $offermodel['logo']!='' ? true : false,
               'value'=>   $offermodel['logo'] != '' ? $offermodel['logo'] : 'N/A',
               'format' =>[$offermodel['logo'] != '' ?'image':'text' ,$offermodel['logo'] != '' ?['width'=>'70','height'=>'70']:'N/A']
                ],
                [
               'label' => Yii::t('app','URL Key'),
               'visible' => $url_key!='' ? true : false,
               'value' =>  $url_key,
               ],
               [
               'label' => Yii::t('app','How many stamps on each card (excluding FREE)'),
                'visible' => strip_tags($offermodel->sspMoreinfo)!='' ? true : false,
               'value' => strip_tags($offermodel->sspMoreinfo),
               ],
           ],
       ]);?>
       </div>
       <?php
 }	// else if($model->type == 2 && isset($offermodel->id))
else if($model->type == 1 && isset($offermodel->id)) {
                   $link = $offermodel->passUrl;
                   $link_array = explode('/',$link);
                   $url_key = end($link_array);
                   if($offermodel->browsercheck==1){
                       $browsercheckvalue = 'Yes';
                   }else{
                       $browsercheckvalue = 'No';
                   }?>
                   <div class="table-responsive">
                   <?php
        echo DetailView::widget([
           'model' => $offermodel,
           'attributes' => [
            [
               'label' => Yii::t('app','Notification Text'),
                'visible' => $offermodel->iphoneNotificationText!='' ? true : false,
               'value' => $offermodel->iphoneNotificationText,
               ],
			[
               'label' => Yii::t('app','Offer Title'),
                'visible' => $offermodel->offerTitle!='' ? true : false,
               'value' =>  $offermodel->offerTitle,
               ],
               [
               'label' => Yii::t('app','Offer was'),
               'value' =>  '$'.$offermodel->offerwas,
               'visible' => $offermodel->offerwas> 0 ? true : false
               ],
               [
               'label' => Yii::t('app','Offer now'),
               'value' =>  '$'.$offermodel->offernow,
               'visible' => $offermodel->offernow> 0 ? true : false
               ],
               [
               'label' => Yii::t('app','Offer Text'),
               'value' =>  $offermodel->offerText,
               'visible' => $offermodel->offerText != '' ? true : false
               ],
               'readTerms:html',
               [
               'attribute'=>'photo',
                'visible' => $offermodel['photo']!='' ? true : false,
               'value'=>   $offermodel['photo'] != '' ? $offermodel['photo'] : 'N/A',
               'format' =>[$offermodel['photo'] != '' ?'image':'text' ,$offermodel['photo'] != '' ?['width'=>'100','height'=>'70']:'N/A']
                ],
                 [
               'attribute'=>'logo',
                'visible' => $offermodel['logo']!='' ? true : false,
               'value'=>   $offermodel['logo'] != '' ? $offermodel['logo'] : 'N/A',
               'format' =>[$offermodel['logo'] != '' ?'image':'text' ,$offermodel['logo'] != '' ?['width'=>'100','height'=>'70']:'N/A']
                  ],
                  [
               'attribute'=>'Video Url',
               'visible' => $offermodel->videokey!='' ? true : false,
               'value'=>   $offermodel->videokey,
                ],
                [
               'attribute'=>'Video Image',
                'visible' => $offermodel['videoImage']!='' ? true : false,
               'value'=>   $offermodel['videoImage'] != '' ? $offermodel['videoImage'] : 'N/A',
               'format' =>[$offermodel['videoImage'] != '' ?'image':'text' ,$offermodel['videoImage'] != '' ?['width'=>'100','height'=>'70']:'N/A']
                ],
                 [
               'attribute'=>'Open Video In Browser',
               'visible' => $browsercheckvalue!='' ? true : false,
               'value'=>   $browsercheckvalue,
                ],
                [
               'label' => Yii::t('app','URL Key'),
               'visible' => $url_key!='' ? true : false,
               'value' => $url_key,
               ],
               'sspMoreinfo:html',
           ],
       ]);?>
       <div class="table-responsive">
       <?php
} //else if($model->type == 1 && isset($offermodel->id))
else if($model->type == 5 && isset($offermodel->id)) { ?>
	<div class="table-responsive">
		<?php
          echo DetailView::widget([
                   'model' => $offermodel,
                   'attributes' => [
                       [
                       'label' => Yii::t('app','Advertiser name'),
                       'visible' => $offermodel->advname!='' ? true : false,
                       'value' => $offermodel->advname,
                       ],
                       [
                       'label' => Yii::t('app','Advertiser address'),
                       'visible' => $url_key!='' ? true : false,
                       'value' => $offermodel->advaddress,
                       ],
                       [
                       'label' => Yii::t('app','Notification Text'),
                       'value' => $offermodel->iphoneNotificationText,
                        'visible' => $offermodel->iphoneNotificationText!='' ? true : false,
                       ],
                       'readTerms:html',
                       [
                       'attribute'=>'photo',
                        'visible' => $offermodel['photo']!='' ? true : false,
                       'value'=>   $offermodel['photo'] != '' ? $offermodel['photo'] : 'N/A',
                       'format' =>[$offermodel['photo'] != '' ?'image':'text' ,$offermodel['photo'] != '' ?['width'=>'100','height'=>'70']:'N/A']
						],
                       [
                       'attribute'=>'logo',
                        'visible' => $offermodel['logo']!='' ? true : false,
                       'value'=>   $offermodel['logo'] != '' ? $offermodel['logo'] : 'N/A',
                       'format' =>[$offermodel['logo'] != '' ?'image':'text' ,$offermodel['logo'] != '' ?['width'=>'70','height'=>'70']:'N/A']
                       ],
                   ],
               ]);?>
               </div>
               <?php
 } // else if($model->type == 5 && isset($offermodel->id))
?>
<?php if(($model->type == 1 || $model->type == 4) && isset($offermodel->id)) { ?>
<!--  start deal model -->
<div class="box box-success">
	<div class="panel-heading">
		<div class="pull-right">
		</div>
		<!-- Header Title-->
		<h3 class="panel-title">
		<i class="glyphicon glyphicon-hand-right"></i><?= Yii::t('app','Deal Detail') ?> </h3>
		<!-- Header Title end -->
		</br>
		<p style="text-align:right">
			<?php    if(isset($dealmodel->id)) {
			echo Html::a(Yii::t('app','Update Deal'), ['updateoffer', 'id' => $model->id,'step'=>3], ['class' => 'btn btn-primary']);
			}else{
			echo Html::a(Yii::t('app','Update Deal'), ['updateoffer', 'id' => $model->id,'step'=>3], ['class' => 'btn btn-primary']);
			} ?>
		</p>
		<div class="clearfix"></div>
	</div>
	<!-- content go here  -->
<?php
 if($model->type == 1 || $model->type == 4 && isset($dealmodel->id))
{
	?>
	<div class="table-responsive">
	<?php
                   echo DetailView::widget([
                           'model' => $dealmodel,
                           'attributes' => [
                           [
                               'attribute'=>Yii::t('app','My Deal image'),
                               'visible' => isset($dealmodel['mainPhotoUpload']) ? true : false,
                               'value'=>   isset($dealmodel['mainPhotoUpload']) ? $dealmodel['mainPhotoUpload'] : 'N/A',
                               'format' =>[isset($dealmodel['mainPhotoUpload']) ?'image':'text' ,isset($dealmodel['mainPhotoUpload']) ?['width'=>'100','height'=>'70']:'N/A']
                           ],
                           [
                               'attribute'=>Yii::t('app','My Deal Video Url'),
                               'visible' => isset($dealmodel['dealvideokey']) ? true : false,
                               'value'=>   isset($dealmodel['dealvideokey']) ?$dealmodel['dealvideokey']:'N/A',
                           ],
                           [
                               'attribute'=>Yii::t('app','My Deal Video Image'),
                               'visible' => isset($dealmodel['dealvideoImage']) ? true : false,
                               'value'=>isset($dealmodel['dealvideoImage']) ?$dealmodel['dealvideoImage']:'N/A',
                               'format' => [isset($dealmodel['dealvideoImage']) ?'image':'text' ,isset($dealmodel['dealvideoImage']) ?['width'=>'100','height'=>'70']:'N/A']
                           ]
                       ],
                       ]);?>
                       </div
                       <?php
}
?>
</div>
<?php } //if($model->type == 1 && $model->type == 4) ?>
 <!-- Modal content-->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog prod_popup_had">
		<div class="modal-content">
		<div class="poup_hed_boder">
		<div class="modal-header ">
		<h1><?php if(isset($offermodel->offerTitle)) {echo $offermodel->offerTitle; } ?> </h1>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title"></h4>
		</div>
		<div class="col-md-12">
		<div class="part_val">
		<b><?php if(isset($model->partnername->orgName)) { echo $model->partnername->orgName; } ?></b>
		</div></div>
		<div class="col-md-12">
		<div class="bookmark_part">
		<img src="<?php echo Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/imgpsh_fullsize.png");?>" style="height: 16px; margin: 0 5px 0 0; width: 16px;" ><b><?= Yii::t('app','Bookmark this deal') ?></b>
		</div>
		</div>
		</div>
		<div class="prod_popup">
		<div class="modal-body">
		<div class="col-md-12">
		<div class="row" >
		<div class="pup_img"><img class="pop-up-image" style="width:279px; height:100px;" src="<?php if(isset($offermodel->uploadTypes) &&$offermodel->uploadTypes==1) { echo $offermodel->photo;} else if(isset($offermodel->videoImage)) {echo $offermodel->videoImage;} ?>" />
		</div>
		<div class="pup_img_logo">
		<img class="pop-up-logo" style="width:60px; height:60px;" src="<?php if(isset($offermodel->logo)) { echo $offermodel->logo;} ?>" /></div>
		</div></div>
		<div class="col-md-12 grey-bottom">
		<div class="row">
		<div class="pupup_doller" style="text-align: left; float:left; color:#08afb5;"><b><?php
		if($model->type == 1)
		{
			if(isset($offermodel->offerText))
			{
				echo $offermodel->offerText;
			}
			else if(isset($offermodel->offernow))
			{
				echo"<span>$</span>".$offermodel->offernow;
			}
		}
		else if($model->type == 2)
		{
			if(isset($offermodel->offerText))
			{
				echo $offermodel->offerText;
			}
			else if(isset($offermodel->offernow))
			{
				echo"<span>$</span>".$offermodel->offernow;
			}
		}
		else {
			if($model->type == 4 && isset($offermodel->offernow))
			{
				echo"<span>$</span>".$offermodel->offernow;
			}
		}
		?></b>
		</div>
		<?php
		if(!empty($offermodel->offernow) && !empty($offermodel->offerwas)) {?>
			<div class="col-md-1 text_though">
			<?php echo"$".$offermodel->offerwas;?>
			</div>
		<?php } ?>
		<div class="read_popup"  style="text-align: right;">
		<img src="<?php echo Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/imgpsh_fullsize1.png");?>" style="height: 16px; margin: 0 5px 0 0; width: 16px;" ><b>Read Terms</b>
		</div>
		</div>
		</div>
		<div class="col-md-12">
		<div class="pop-up-image_buton">
			<p><button data-target="#myModal" data-toggle="modal" class="btn btn-info" type="button"><?= Yii::t('app','Add to my wallet') ?>
			</button>
			</p>
		</div>
		</div>
		</div>
		</div>
		<div class="modal-footer">
		</div>
		</div>
	</div>
</div>
<!-----------preview pass------------------------->
<div class="modal fade" id="myModalpass" role="dialog">
	<div class="modal-dialog prod_popup_had">
	<!-- Modal content-->
	<div class="modal-content">
	<div class="poup_hed_boder">
	<div class="modal-header ">
	<h1><?php /*if(isset($offermodel->offerTitle)) {echo $offermodel->offerTitle; }*/
	if(isset($offermodel->brandnameval)) { echo $offermodel->brandnameval;}
	?> </h1>
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"></h4>
	</div>
	<div class="col-md-12">
	<div class="part_val">
	<b><?php /*if(isset($model->partnername->orgName)) { echo $model->partnername->orgName; } */
	if(isset($offermodel->eventVenue)) { echo $offermodel->eventVenue; }
	?></b>
	</div></div>
	<div class="col-md-12">
	<div class="bookmark_part">
	<b><?php echo $model->dateTo;?></php></b>
	</div></div>
	</div>
	<div class="prod_popup">
	<div class="modal-body">
	<div class="col-md-12">
		<div class="row" >
		<div class="pup_img"><img class="pop-up-image" style="width:279px; height:100px;" src="<?php if(isset($offermodel->uploadTypes) && $offermodel->uploadTypes==1) { echo $offermodel->photo;} else if(isset($offermodel->videoImage)){ echo $offermodel->videoImage;}  ?>" /></div>
		<div class="pup_img_logo">
		<img class="pop-up-logo" style="width:60px; height:60px;" src="<?php if(isset($offermodel->logo)) { echo $offermodel->logo;} ?>" /></div>
		</div>
	</div>
	<div class="col-md-12 grey-bottom">
	<div class="row">
	<div class="pupup_doller" style="text-align: left; float:left; color:#08afb5;">
	<b>
		<?php
			if($model->type == 1)
			{
				if(isset($offermodel->offerText))
				{
					echo $offermodel->offerText;
				}
				else if(isset($offermodel->offernow))
				{
						echo"<span>$</span>".$offermodel->offernow;
				}
			}
			else if($model->type == 2)
			{
				if(isset($offermodel->offerText))
				{
					echo $offermodel->offerText;
				}
				else if(isset($offermodel->offernow))
				{
					echo"<span>$</span>".$offermodel->offernow;
				}
			}
			else
			{
				if($model->type == 4 && isset($offermodel->ticketinfo))
				{
					echo $offermodel->ticketinfo;
				}
			}
		?>
	</b>
	</div>
	<?php if(isset($offermodel->offerText)) {?>
		<div class="col-md-1 text_though">
		<?php //echo"$".$offermodel->offerwas;?>
		</div>
	<?php } ?>
	<div class="read_popup"  style="text-align: right;">
	<img src="<?php echo Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/imgpsh_fullsize1.png");?>" style="height: 16px; margin: 0 5px 0 0; width: 16px;" ><b>Read Terms</b></div>
	</div>
	</div>
	<div class="col-md-12">
	<div class="pop-up-image_bar_code1 text-center">
	<?php
			$customOfferSelect1 = isset($offermodel->customOfferSelect1)  ? $offermodel->customOfferSelect1 : 0;
			$customOfferSelect2 = isset($offermodel->customOfferSelect2)  ? $offermodel->customOfferSelect2 : 0;
			$totalcount = intval($customOfferSelect1)+intval($customOfferSelect2);
	?>
		<p> <?php if($model->type==2 && $totalcount<8){
		 for($i=1;$i<=$customOfferSelect1;$i++) {			?>
		<img class="img-circle" style="width:30px; height:30px;" src="<?php if(isset($offermodel->logo)) { echo $offermodel->logo;} ?>" />
	<?php } //  for($i=1;$i<=$offermodel->customOfferSelect1;$i++) {
	 for($i=1;$i<=$customOfferSelect2;$i++) {
		echo '<span class="glyphicon glyphicon-one-fine-blue-dot"></span>';
	 }
	} //if($model->type==2){
		else if(isset($offermodel->sspMoreinfo)){
			echo strip_tags($offermodel->sspMoreinfo);
		}
		?></p>
	</div>
	<p> <?php if(isset($offermodel->passUrl) && $model->type!=2) { ?>
	<div class="pop-up-image_bar_code ">
	<p>
		<img class="pop-up-image" style="width:260px; height:100px;" src="<?php if(isset($offermodel->passUrl)) { echo $offermodel->passUrl; } ?>" />
	</p>
	</div>
	</p> <?php } ?>
	</div>
	</div>
	</div>
	<div class="modal-footer">
	</div>
	</div>
	</div>
</div>
<!------------pop preview end here --------------------------->
</div><!--/.col (right) end offer and deal wrapper-->
   </div>   <!-- /.row -->
<style>
  @import url(https://fonts.googleapis.com/css?family=Montserrat:400,700);.bookmark_part b,.bookmark_part strong,.modal-header .close,.prod_popup_had h1,.read_popup b,.text_though{font-weight:400}.modal-dialog,.pop-up-image_buton .btn-info,.prod_popup_had h1,.pupup_doller b span,.read_popup,.text_though{font-family:Montserrat,sans-serif}.modal-dialog{margin:30px auto;width:300px}.icon-ms{background-color:#F3F3F3}.prod_popup_had h1{color:#555!important;float:left;font-size:16px;margin:0;padding:4px 0;width:50%}.prod_popup_had .modal-footer,.prod_popup_had .modal-header{border:none}.prod_popup_had .poup_hed_boder{border-bottom:1px solid #999;float:left;padding:0 0 14px;width:100%}.c,.prod_popup .modal-body{padding:0}.prod_popup_had .modal-content{border:0;border-radius:4px}.prod_popup{width:100%;float:left}.pup_img{margin:10px 0;width:100%;position:relative}.pup_img img{width:100%!important}.pup_img_logo{bottom:0;float:right;left:auto;position:absolute;right:0;top:10px;z-index:999999999}.c{width:100%;margin:0}.bookmark_part{float:left;text-align:left;width:100%;padding-top:11px}.pop-up-image_bar_code{margin:0;padding:0;text-align:center;width:100%}.grey-bottom{background-color:#f5f5f5;margin-bottom:25px;margin-top:-10px;padding:7px 15px}.part_val{float:left;text-align:left;width:100%}.pop-up-image_buton{margin:0;padding:0;text-align:center;width:100%}.pop-up-image_buton .btn-info{background-color:#08afb5;border-color:#00acd6;text-transform:uppercase;border-radius:25px;font-size:14px;font-weight:400}.text_though{text-decoration:line-through;color:#999;padding:8px 10px 0;font-size:13px}.pupup_doller{max-width:100%;min-width:40px;padding:0 0 0 15px;font-size:17px}.read_popup{color:#444;float:right;font-size:13.4px;line-height:27px;padding:0 15px 0 0;width:40%}.modal-header .close{margin-right:-10px;margin-top:-16px;font-size:29px}.pupup_doller b span{font-size:21px}@media all and (max-width:480px){.modal-dialog{margin:30px auto;width:auto}.pup_img_logo{left:213px}}.glyphicon-one-fine-blue-dot:before{content:"\25cf";font-size:3em;color:#08afb5}.glyphicon.glyphicon-one-fine-blue-dot{top:8px}
</style>
