<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Workorders */

 

$this->title =  Yii::t('app','View (PSA) : ').ucfirst($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Workorders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
          <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-tasks"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
			 <?php if(isset($offermodel->id)) { ?>
             <?= Html::a(Yii::t('app','Update'), ['updatepsa', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
			 <?php } else{  ?>
			  <?= Html::a(Yii::t('app','Update'), ['createofferpsa', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
			 <?php } ?>
     		   <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a(Yii::t('app','OK'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Preview PSA</button>

           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
             'id',
            'name',
           
              [                    
            'label' => Yii::t('app','Partner Name'),
            'value' => is_object($model->partnername)?$model->partnername->orgName:'N/A',
             ],
             [                    
            'label' => Yii::t('app','Country Name'),
            'value' => is_object($model->countryname)?$model->countryname->name:'N/A',
             ],
             
          
            'mediaContractNumber',          
                    
            'noPasses',
          
            'duration',
            [                    
            'label' => Yii::t('app','Date From'),
            'value' => $model->dateFrom,
             ],
             [                     
            'label' => Yii::t('app','Date To'),
            'value' => $model->dateTo,
             ],
             
            
             
            [                     
            'label' => Yii::t('app','Categories'),
            'value' => implode(",",$categoryTree),
             ],
             
          
            [                     
            'label' => Yii::t('app','userType'),
           'value' => $model->usertype,
             ],
			[                     
            'label' => Yii::t('app','Age Group'),
            'value' => $model->agegroupname,
             ], 
             [                     
            'label' => Yii::t('app','Beacon Group'),
            'value' => $model->beacongroupname,
             ], 
             [                     
            'label' => Yii::t('app','Rules/Conditions'),
            'value' => $model->rulesandconditions,
             ], 
             
             [                     
            'label' => Yii::t('app','Dayparting'),
            'value' => $model->daypartingvalue,
             ],  
             
             [                     
            'label' => Yii::t('app','Temprature'),
            'value' => $model->temparturevalue,
             ],             
             [                     
            'label' => Yii::t('app','Behaviour visit  '),
            'value' => $model->behaviourvalue,
             ], 
             
             [                     
            'label' => Yii::t('app','Behaviour-dwell  '),
            'value' => $model->behaviourdwellvalue,
             ], 
             
             [                     
            'label' => Yii::t('app','On Entry '),
            'value' => $model->entryvalue,
             ], 
             
             [                     
            'label' => Yii::t('app','On Exit'),
            'value' => $model->exitvalue,
             ], 
             
             [                     
            'label' => Yii::t('app','Purchase history'),
            'value' => $model->Purchasevalue,
             ], 
             
              [                     
            'label' => Yii::t('app','Loyalty'),
            'value' => $model->loyaltyvalue,
             ], 
             
             
             
               
           
           

        ],
    ]) ?>
    
    <!-- start offer model  -->
      <div class="box box-success">
		  <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-tasks"></i> <?= Yii::t('app','Offer Detail') ?></h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
    <?php if(is_object( $offermodel)){       ?>     
      <?= DetailView::widget([
        'model' => $offermodel,
        'attributes' => [
        
			[                     
            'label' => Yii::t('app','PSA Type'),
            'value' =>  $offermodel->psatype,
            ], 
               
			[                     
            'label' => Yii::t('app','Text title'),
            'value' =>  $offermodel->offerTitle,
            ], 
           
            [                     
            'label' => Yii::t('app','Text'),
            'value' => $offermodel->offerText,
            ],
            
            [                     
            'label' => Yii::t('app','Notification text'),
            'value' => $offermodel->iphoneNotificationText,
            ],
            
			[
		'attribute'=>'photo',
		'value'=>$offermodel->photo,
		'format' => ['image',['width'=>'100','height'=>'70']],
		 ],
     
        ],
    ]) ?>
	<?php  } ?>	
    </div>
    
   
    

</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
   
        <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="border-bottom-color:#F1F1F1">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body" style="text-align: center;">
          <p><img class="pop-up-image" src="<?php echo isset($offermodel->photo)?$offermodel->photo:''; ?>" /> </p>
          <div class="pop-up-title"> <?php echo isset($offermodel->offerTitle)?$offermodel->offerTitle:''; ?> </div>
          <div class="pop-up-text"> <?php echo isset($offermodel->offerText)?$offermodel->offerText:''; ?> </div>
        </div>
        <div class="modal-footer" style="text-align: center; border-top-color: #F1F1F1">
          <button type="button" class="btn-info1" data-dismiss="modal" style="padding: 7px 25px;  border-radius: 18px;"><?= Yii::t('app','OK, thanks!') ?></button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>


<style>
.modal-dialog {
	width:300px;
	height:380px;
} 
.modal-content{
border-radius: 4px;
background:#F1F1F1;

}
.pop-up-image{
	width:300px;
	height:100px;
 }

.modal-body {
	padding: 0px;
	color: #000000;
	font-size: 13px;
	font-family:Montserrat-Regular;
}

.btn-info1 {
    border-color: #53B3B8;
     background-color: #53B3B8;
     color: #fff;
     border:none;
      font-size: 15px;
    font-weight: 600;
}

.pop-up-title {
    font-weight: bold;
    margin-left: 20px;
    margin-right: 20px;
}

.pop-up-text {
    margin: 10px 20px;
}

.modal-footer {  
    padding: 15px 15px 25px;   
}

.modal-header { 
	 padding: 15px 15px 23px;
}

</style>
