<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Categories;
use backend\models\Beacongroup;
use common\models\Store;
use common\models\Country;
use backend\models\Beaconlocation;
use backend\models\Usermanagement;
#use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model backend\models\Workorders */
/* @var $form yii\widgets\ActiveForm */
$country_name =  ArrayHelper::map(Country::find()->all(), 'id_countries', 'name');
?>
<div class="loading-image text-center" style="display:none;">
	<div class="loading">Loading&#8230;</div>
</div>
<div class="workorders-form">
<?php
$form = ActiveForm::begin([
'options' => [
'enctype' =>'multipart/form-data'
]
]);
	if($model->countryID==''){
		$model->countryID = 13;
	}
?>
<div class="box-body">
	<div class="form-group">
	 <?php //echo $form->errorSummary($model);  ?>
	<?= $form->field($model, 'savetype')->hiddenInput(['value' =>0])->label(false) ?>
	<?= $form->field($model, 'workorderType' )->hiddenInput(['value' =>1])->label(false); ?>
	<?= $form->field($model, 'status' )->hiddenInput(['value' =>2])->label(false); ?>
	<?php
    echo $form->field($model, 'countryID',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Select the country in which your deal will be available.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->widget(Select2::classname(),
							[
							'data' => $country_name,
							'options' => ['placeholder' => Yii::t('app','Select Country ...'),'multiple' => false],
							'pluginOptions' => [
							'allowClear' => true
							],
			]); ?>
	<!-- Show only Location Owner -->
	<?php
	if(Yii::$app->user->identity->user_type==1){
		$partner =  ArrayHelper::map(Usermanagement::find()
		->where(['user_type' => 7])->andWhere(['status' => 10])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName'); ?>
 <?php
    echo $form->field($model, 'workorderpartner',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Select the partner that is connected to your deal.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->widget(Select2::classname(),
							[
							'data' => $partner,
							'options' => ['placeholder' => Yii::t('app','Select Partner ...'),'multiple' => false],
							'pluginOptions' => [
							'allowClear' => true
							],
			]); ?>
	<?php } else {
		$usertype = Yii::$app->user->identity->user_type;
		$userId = $usertype==7?Yii::$app->user->id : Yii::$app->user->identity->advertister_id; ?>
	<?= $form->field($model, 'workorderpartner')->hiddenInput(['value' =>$userId])->label(false) ?>
	<?php }	?>
	<?= $form->field($model, 'mediaContractNumber')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'type')->dropDownList(array('1' =>Yii::t('app','Voucher'), '2' =>Yii::t('app','Digital Stamp Card'), '4' =>Yii::t('app','Event ticket'), '5' =>Yii::t('app','Go to Store') ),[ 'prompt' => Yii::t('app',' -- Select --')]) ?>
	<?= $form->field($model, 'geoProximity',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Choose whether the deal will be triggered by proximity, geolocation, both or none.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->dropDownList(array('0' =>Yii::t('app','None'), '1' =>Yii::t('app','Both'), '2' =>Yii::t('app','Proximity'), '3' =>Yii::t('app','Geolocation')),[ 'prompt' => Yii::t('app',' -- Select --')]) ?>
	<!-- added for geozone start -->
	<button type="button" id='show-geopopup-model' class="btn btn-info btn-small" style="display:none;margin-bottom:5px;" data-toggle="modal" data-target="#gmyModal"><?= Yii::t('app','Add Geolocation') ?></button>
	<!-- Trigger the modal with a button -->
	<!-- start beacon-groupname  -->
	<?php
			if(!empty($model->beaconGroup)){
					$beaconGroup=Beacongroup::find()
					->where(['IN','id',$model->beaconGroup])
					->orderBy('group_name')->asArray()->all();
			}else{
				$beaconGroup = array();
			}
				$model->beaconGroup =  array_column($beaconGroup,"id");
				$cityDesc= array_column($beaconGroup,"group_name");
				$url = \yii\helpers\Url::to(['beacon-groupname']);
           echo $form->field($model, 'beaconGroup',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Select which beacon groups will trigger this deal.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->widget(Select2::classname(), [
           'initValueText' => $cityDesc, // set the initial display text
           'options' => ['placeholder' => 'Search for beacon group ...','multiple' => true],
           'pluginOptions' => [
               'allowClear' => true,
               'minimumInputLength' => 3,
               'language' => [
                   'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
               ],
               'ajax' => [
                       'url' => $url,
                       'method'=>'POST',
                       'dataType' => 'json',
                       'data' => new JsExpression('function(params) { var selectedVal=($("#workorders-beacongroup").val()); return {q:params.term,sq:selectedVal}; }')
               ],
               'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
               'templateResult' => new JsExpression('function(city) { return city.text; }'),
               'templateSelection' => new JsExpression('function (city) { return city.text; }'),
           ],
       ]);
   ?>
	<?= $form->field($model, 'noPasses',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Set the total number of passes that will be available for this particular deal.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->textInput(['maxlength' => true]) ?>
	<?php
		$arr = array();
		for ($x = 1; $x <= 50; $x++) {
			$arr[$x] = $x;
		}
	?>
	<?= $form->field($model, 'allowpasstouser',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="For example: Select 1 if you want the pass to become invalid after each redemption. Select more than 1 if the user can use the same pass within the validity period more than 1 time.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->dropDownList($arr,[ 'prompt' => Yii::t('app',' -- Select --')]) ?>
	<?= $form->field($model, 'qtyPerPass',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Set the maximum of items that can be purchased per pass.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->textInput(['maxlength' => true]) ?>
    <label class="control-label" for="start-date">
		<?= $model->getAttributeLabel('dateFrom'); ?>
		<label class="control-label" id="end-date" style="margin-left: 128px;">
			<?= $model->getAttributeLabel('dateTo'); ?>
		</label>
    </label>
	<?= $form->field($model, 'dateFrom')->textInput(['label'=> ''])->widget(DateRangePicker::className(), [
							'attributeTo' => 'dateTo',
							'form' => $form, 
							'language' => 'en',
							'size' => 'lg',
							'clientOptions' => [
							'autoclose' => true,
							'startDate' => date("Y-m-d"),
							'format' => 'yyyy-mm-dd',
							]])->label(false);
	?>
	<?php
	/* Day parts values */
		$daypartends=$daypartstart= ['01' => Yii::t('app','01 AM'),'02' => Yii::t('app','02 AM'),'03' => Yii::t('app','03 AM'),'04' => Yii::t('app','04 AM'),'05' => Yii::t('app','05 AM'),'06' => Yii::t('app','06 AM'),'07' => Yii::t('app','07 AM'),'08' => Yii::t('app','08 AM'),'09' => Yii::t('app','09 AM'),'10' => Yii::t('app','10 AM'),'11' => Yii::t('app','11 AM'),'12' => Yii::t('app','12 AM'),'13' => Yii::t('app','01 PM'),'14' => Yii::t('app','02 PM'),'15' => Yii::t('app','03 PM'),'16' => Yii::t('app','04 PM'),'17' => Yii::t('app','05 PM'),'18' => Yii::t('app','06 PM'),'19' => Yii::t('app','07 PM'), '20' =>Yii::t('app','08 PM'), '21' => Yii::t('app','09 PM'), '22' => Yii::t('app','10 PM'), '23' => Yii::t('app','11 PM'), '24' => Yii::t('app','12 PM')];
	?>
	<?= $form->field($model, 'duration')->textInput(['readonly'=> true]) ?>
	<?= $form->field($model, 'returnWithin',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Add in the return policy in days for this deal. Only after the return period ends the loyalty points will be administered. If there is no return policy enter 0.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->textInput() ?>
	<div class="help-block"></div>
	<!-- added for geolocatio -->
	<!-- Modal fullscreen -->
	<div class="modal modal-fullscreen fade" id="gmyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		</div>
		<div class="modal-body">
		<input type="hidden" id="base_url" name="base_url" value="<?php echo \Yii::$app->request->BaseUrl;?>" />
		<input type="hidden" id="tempwork" name="tempworid" value="<?php echo mt_rand();?>" />
		<?php
		$baseurl = Yii::$app->getUrlManager()->getBaseUrl();
		echo '<iframe  id="ifrmaieid" width="100%" height="850"  src=""></iframe>';
		?>
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
		</div>
		</div>
	</div>
	<!-- added for geolocatio -->
	<button type="button" id='show-popup-model' class="btn btn-info btn-lg" style="display:none" data-toggle="modal" data-target="#myModal"><?= Yii::t('app','Open Modal') ?></button>
	<!-- Trigger the modal with a button -->
	<!--   Rule and condition Modal popup start  -->
	<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
	<!-- Modal content-->
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?= Yii::t('app','Rules/Conditions') ?></h4>
	</div>
	<!---- MOdal Window Pp Up Content Start Here-->
	<div class="modal-body">
	<?= $form->field($model, 'dayparting')->radioList(array('1'=>Yii::t('app','24 Hours -OR-'),'3'=>Yii::t('app','Custom')),['id' => 'dayparting-id']); ?>
	<div id="show-dayparting " style="display:none;">
	<div class="col-md-6 col-lg-6">
	<?= $form->field($model, 'daypartingcustom1')->dropDownList($daypartstart, array( 'multiple'=>false,  'prompt' => Yii::t('app',' --option one start--')))->label(false) ?>
	</div> <div class="col-md-6 col-lg-6">
	<?=  $form->field($model, 'daypartingcustom1end')->dropDownList($daypartends, array('multiple'=>false,  'prompt' => Yii::t('app',' --option one end--')))->label(false); ?>
	</div> <div class="col-md-6 col-lg-6">
	<?= $form->field($model, 'daypartingcustom2')->dropDownList($daypartstart, array('multiple'=>false,  'prompt' => Yii::t('app',' --option two start--')))->label(false) ?>
	</div> <div class="col-md-6 col-lg-6">
	<?=  $form->field($model, 'daypartingcustom2end')->dropDownList($daypartends, array('multiple'=>false,  'prompt' => Yii::t('app',' --option two end--')))->label(false); ?>
	</div> <div class="col-md-6 col-lg-6">
	<?= $form->field($model, 'daypartingcustom3')->dropDownList($daypartstart, array('multiple'=>false,  'prompt' => Yii::t('app',' --option three start--')))->label(false) ?>
	</div> <div class="col-md-6 col-lg-6">
	<?=  $form->field($model, 'daypartingcustom3end')->dropDownList($daypartends, array('multiple'=>false,  'prompt' => Yii::t('app',' --option three end--')))->label(false); ?>
	</div>
	<div class="clear"></div>
	</div>
	<?php
	$tempstart = [];
	for ($x = 1; $x <= 100; $x++) {
	$xf = $x.html_entity_decode("&nbsp;&#8451;");
	$tempstart[$x] = $xf;
	}
	$tempend = [];
	for ($h = 1; $h <= 100; $h++) {
	$hf = $h.html_entity_decode("&nbsp;&#8451;");
	$tempend[$h] = $hf;
	}
	?>
	<?= $form->field($model, 'temperature')->radioList(array('1'=>Yii::t('app','ALL'),'2'=>Yii::t('app','Custom')),['id' => 'temp-id']); ?>
	<div id="show-temp">
	<div class="col-md-6 col-lg-6">
	<?= $form->field($model, 'temperaturestart')->dropDownList($tempstart, array('multiple'=>false,  'prompt' => Yii::t('app',' --temperature start --')))->label(false) ?>
	</div>
	<div class="col-md-6 col-lg-6">
	<?= $form->field($model, 'temperaturend')->dropDownList($tempend, array('multiple'=>false,  'prompt' => Yii::t('app',' --temperature end --')))->label(false); ?>
	</div>
	</div>
	<?php
	$visit = [];
	for ($h = 1; $h <= 100; $h++)
	{
	$visit[$h] = $h;
	}
	$minute = [];
	for ($h = 1; $h <= 60; $h++)
	{
	$minute[$h] = $h;
	}
	?>
	<!------Behaviour visit Content Start------->
	<div class="row">
	<div class="col-md-12 col-lg-12">
	<label class="control-label" for="temp-id"><?= Yii::t('app','Behaviour visit') ?></label>
	</div>
	</div>
	<div class="row">
	<div class="col-md-4 col-lg-4">
	<?= Yii::t('app','User has visited beacon location minimum of ') ?>
	</div>
	<div class="col-md-2 col-lg-2">
	<?= $form->field($model, 'behaviourvisit1')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
	</div>
	<div class="col-md-1 col-lg-1">
	<?= Yii::t('app','times in') ?>
	</div>
	<div class="col-md-2 col-lg-2">
	<?= $form->field($model, 'behaviourvisit2')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?= $form->field($model, 'behaviourvisit3')->dropDownList([1=>Yii::t('app','days'),2=>Yii::t('app','weeks'),3=>Yii::t('app','months')], array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
	</div>
	</div>
	<!------Behaviour visit Content End------->
	<!------Behaviour dwell Content Start------->
	<div class="row">
	<div class="col-md-12 col-lg-12">
	<label class="control-label" for="temp-id"><?= Yii::t('app','Behaviour dwell') ?></label>
	</div>
	</div>
	<div class="row">
	<div class="col-md-6 col-lg-6" style="padding-right:0px;">
	<?= Yii::t('app','User has dwelt in beacon location minimum of') ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?= $form->field($model, 'behaviourDwell')->dropDownList($minute, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?= $form->field($model, 'behaviourDwellMinutes')->dropDownList([1 => Yii::t('app','minutes'), 2 => Yii::t('app','seconds')], array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
	</div>
	</div>
	<!------Behaviour dwell Content End------->
	<!------On Entry Content Start------->
	<div class="row">
	<div class="col-md-12 col-lg-12">
	<label class="control-label" for="temp-id"><?= Yii::t('app','On Entry') ?></label>
	</div>
	</div>
	<?= $form->field($model, 'userenter')->checkbox(array('label'=>Yii::t('app','User has entered the store'))) ?>
	<div class="row">
	<div class="col-md-12 col-lg-12">
	<label class="control-label" for="temp-id"><?= Yii::t('app','On Exit') ?></label>
	</div>
	</div>
	<?= $form->field($model, 'usereturned')->checkbox(array('label'=>Yii::t('app','User has exited the store'))) ?>
	<!------On Entry Content End------->
	<!-- Purchase history start -->
	<div class="row">
	<div class="col-md-12 col-lg-12">
	<label class="control-label" for="temp-id"><?= Yii::t('app','Purchase history') ?></label>
	</div>
	</div>
	<div class="row">
	<div class="col-md-2 col-lg-2">
	<?= Yii::t('app','User has spent minimum of ') ?>
	</div>
	<div class="col-md-1 col-lg-1" style="font-weight: bold;
	padding: 7px 0 0; width:8px;">
	<?= Yii::t('app','$') ?>
	</div>
	<div class="col-md-2 col-lg-2" style="padding-left: 4px;">
	<?= $form->field($model, 'purchaseHistory1')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
	</div>
	<div class="col-md-2 col-lg-2">
	<?= Yii::t('app','in the past') ?>
	</div>
	<div class="col-md-2 col-lg-2">
	<?= $form->field($model, 'purchaseHistory2')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?= $form->field($model, 'purchaseHistory3')->dropDownList([1=>Yii::t('app','days'),2=>Yii::t('app','weeks'),3=>Yii::t('app','months'),4=>Yii::t('app','year')], array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
	</div>
	</div>
	<!-- Purchase history End -->
	<!-- Loyalty start -->
	<div class="row">
	<div class="col-md-12 col-lg-12">
	<label class="control-label" for="temp-id"><?= Yii::t('app','Loyalty') ?></label>
	</div>
	</div>
	<div class="row">
	<div class="col-md-2 col-lg-2">
	<?= Yii::t('app','User has scanned their loyalty ID') ?>
	</div>
	<div class="col-md-2 col-lg-2">
	<?= $form->field($model, 'Loyalty1')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
	</div>
	<div class="col-md-2 col-lg-2">
	<?= Yii::t('app','times in the past') ?>
	</div>
	<div class="col-md-2 col-lg-2">
	<?= $form->field($model, 'Loyalty2')->dropDownList($visit, array('multiple'=>false,  'prompt' => Yii::t('app','select')))->label(false) ?>
	</div>
	<div class="col-md-3 col-lg-3">
	<?= $form->field($model, 'Loyalty3')->dropDownList([1=>Yii::t('app','days'),2=>Yii::t('app','weeks'),3=>Yii::t('app','months'),4=>Yii::t('app','year')], array('multiple'=>false,  'prompt' => 'select'))->label(false) ?>
	</div>
	</div>
	<!-- Loyalty End -->
	</div>
	<!---- Modal Window Pp Up Content End Here-->
	<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Save and Close') ?></button>
	</div>
	</div>
	</div>
	</div>
	<?php
		$userdata = array();
		if(is_array($model->userType)) {
			$model->userType =  implode(', ',$model->userType);
			foreach (explode(", ",$model->userType) as $usert ) {
				$userdata[$usert] = array('Selected' => Yii::t('app','selected'));
			}
		} else {
		foreach (explode(", ",$model->userType) as $usert ) {
			$userdata[$usert] = array('Selected' => Yii::t('app','selected'));
			}
		}
		$userDatas = $userdata;
	?>
	<?= $form->field($model, 'userType',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Select if your deal is targeted at Men or Women or Both.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><br><button type="button" style="padding:2px 5px;margin: 8px 1px;" onclick="alluserstype()" class="bbtn btn-success">Select all user types</button><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->dropDownList(array('1' =>Yii::t('app','Men'), '0' =>Yii::t('app','Women') ),['options' => $userDatas, 'id'=>'allselectuser', 'multiple'=>true, 'prompt' => Yii::t('app',' -- Select User Type --')]) ?>
	<?php
		$agedata = array();
		if(is_array($model->agedGroup)) {
			$model->agedGroup =  implode(', ',$model->agedGroup);
			foreach (explode(", ",$model->agedGroup) as $age ) {
				$agedata[$age] = array('Selected' => Yii::t('app','selected'));
		}
		}else{
			foreach (explode(", ",$model->agedGroup) as $age ) {
				$agedata[$age] = array('Selected' => Yii::t('app','selected'));
			}
		}
		$ageGroup = $agedata;
	echo $form->field($model, 'agedGroup',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="Select what age group your deal is targeted at or select all.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><br><button type="button" style="padding:2px 5px;margin: 8px 1px;" onclick="allAgegroup()" class="bbtn btn-success">Select all age groups</button><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->dropDownList(array( '1' =>Yii::t('app','Between 15-18 years'), '2' =>Yii::t('app','Between 19-25 years'), '3' =>Yii::t('app','Between 26-35 years'), '4' =>Yii::t('app','Between 36-45 years'), '5' =>Yii::t('app','Between 46-60 years'), '6' =>Yii::t('app','More than 61 years')), ['options' => $ageGroup, 'id'=>'allselectage', 'multiple'=>true, 'prompt' => Yii::t('app',' -- Select Age Group --')]);
	?>
	<!-- start rule and condition  -->
	<?= $form->field($model, 'rulecondition' )->checkbox(); ?>
	<div class="help-block"></div>
	<!-- Show categories -->
	<label class="control-label" for="store-owner"><?= Yii::t('app','Select Categories') ?></label> <span class="information" data-toggle="tooltip" data-placement="right" title="Select the category that the deal belongs to so to ensure the right audience is targeted.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span>
	<?= $form->field($model, 'catIDs')->hiddenInput()->label(false); ?>
	<div class="chosentree"></div>
	</div><!-- /.box-body -->
	<div class="box-footer">
	<div class="form-group">
	<?= Html::a('SAVE AND EXIT',['create'],['class'=>'btn btn-primary progressbtn','id'=>1]) ?>
	<?= Html::a('NEXT',['create'],['class'=>'btn btn-success progressbtn','id'=>2]) ?>
	</div>
	</div>
	<input type="hidden" id="savenew" name="savenew" value="" />
	<?php ActiveForm::end(); ?>
</div>
<?php
$this->registerJs(
    '
var loadChildren = function(node) {
JSONObject =JSON.parse(\' '.$categoryTree.'\');
	 //JSONObject = JSON.parse(\'{"id":"01","title":"Node 01","has_children":true,"checked":true,"level":1,"children":[{"id":"011","title":"Node 011","name":"paras","has_children":true,"level":2,"children":[{"id":"0111","title":"Node 0111","has_children":false,"level":3,"children":[]}]}]}\');
	 //for(i=0;i<JSONObject.length;i++)
		node.children.push(JSONObject);
	// console.log(node);
        return node;
      };
$(document).ready(function() {
        $("div.chosentree").chosentree({
            width: 500,
            deepLoad: true,
              showtree: true,
            load: function(node, callback) {
                    setTimeout(function() {
          callback(loadChildren(node));
       }, 1000);
            }
        });
});
		'
);
$this->registerJsFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.js', ['position' => \yii\web\View::POS_END]);
$this->registerCssFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.css', ['position' => \yii\web\View::POS_END]);
//$this->registerJsFile(Yii::$app->homeUrl.'store-address-suggest.js', ['position' => \yii\web\View::POS_END]);
$baseurl = Yii::$app->getUrlManager()->getBaseUrl();
$shopnowget =$baseurl.'/workorders/partnergetshopnow';
$beacongroup =$baseurl.'/workorders/beacongroup';
$workordersCat =$baseurl.'/workorders/workcat';
$geoZoneUrl =$baseurl.'/workorders/loadmapzone';
?>
<script type="text/javascript">
	$('.progressbtn').click(function(e){
		e.preventDefault();
		$('#savenew').val($(this).attr('id'));
		$('#w0').submit();
	});
	$('#workorders-workorderpartner').change(function(){
	$('#chosentree-wrap').show();
	var storeID = $(this).val();
	if(storeID==''){ return false; }
	$('.loading-image.text-center').show();
	$.ajax({
			method: "POST",
			url: "<?php echo $workordersCat; ?>",
			data: { id:storeID }
	})
	.done(function(result) {
	$('.loading-image.text-center').hide();
		 $('div.chosentree').empty();
		JSONObject = JSON.parse(result);
      $('div.chosentree').chosentree({
          width: 500,
          deepLoad: true,
          load: function(node, callback) {
                  callback(JSONObject);
          }
      });
	  });
});
	$('#workorders-rulecondition').click(function(){
    if (this.checked) {
       $('#show-popup-model').click();
    }
   });
$( "#workorders-type" ).change(function() {
  var workorders_type = $(this).val();
	if(workorders_type==4){
  $( "#workorders-buynow" ).prop( "checked", true );
}
});
var val = $("#workorders-type").val();
if(val == 1 || val == 4){
	$( ".field-workorders-allowpasstouser" ).css('display','block');
  $( ".field-workorders-qtyperpass" ).css('display','block');
}
$( "#workorders-type" ).change(function() {
  var workorders_type = $(this).val();
	if(workorders_type==1 || workorders_type==4){
  $( ".field-workorders-allowpasstouser" ).css('display','block');
  $( ".field-workorders-qtyperpass" ).css('display','block');
	}else{
	$( ".field-workorders-allowpasstouser" ).css('display','none');
	$( ".field-workorders-qtyperpass" ).css('display','none');
	}
});
$("#workorders-geoproximity").change(function() {
  var geoproximity_type = $(this).val();
	if(geoproximity_type==1 || geoproximity_type==2){
		$(".field-workorders-beacongroup" ).show();
		$(".select2-search__field").removeAttr('style');
	}else{
		$(".field-workorders-beacongroup" ).hide();
	}
	if(geoproximity_type==1 || geoproximity_type==3){
		$( "#show-geopopup-model" ).css('display','block');
	}else{
		$("#show-geopopup-model").css('display','none');
	}
});
$("#show-geopopup-model").click(function(){
	var pid = $('#workorders-workorderpartner').val();
	if(pid==''){
			alert('Please Select Partner');
			return false;
		}
	else{
		var workid = $('#tempwork').val();
		var url = $('#base_url').val();
		var ifrmaieid = $('#ifrmaieid').attr('src',url+'/workorders/createzoneiframe?partnerid='+pid+'&tempwororderid='+workid);
	}
	//setTimeout(initialize, 500);
});
// .modal-backdrop classes
$(".modal-fullscreen").on('show.bs.modal', function () {
  setTimeout( function() {
    $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
  }, 0);
});
$(".modal-fullscreen").on('hidden.bs.modal', function () {
  $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
});
</script>
<style>
.box-footer a { margin: 1px 5px;}
.loading,.loading:before{position:fixed;top:0;left:0}.field-workorders-allowpasstouser,.field-workorders-qtyperpass{display:none}.loading:before,.loading:not(:required):after{content:'';display:block}.loading{z-index:999;height:2em;width:2em;overflow:show;margin:auto;bottom:0;right:0}.loading:before{width:100%;height:100%;background-color:rgba(0,0,0,.3)}.loading:not(:required){font:0/0 a;color:transparent;text-shadow:none;background-color:transparent;border:0}.loading:not(:required):after{font-size:10px;width:1em;height:1em;margin-top:-.5em;-webkit-animation:spinner 1.5s infinite linear;-moz-animation:spinner 1.5s infinite linear;-ms-animation:spinner 1.5s infinite linear;-o-animation:spinner 1.5s infinite linear;animation:spinner 1.5s infinite linear;border-radius:.5em;-webkit-box-shadow:rgba(0,0,0,.75) 1.5em 0 0 0,rgba(0,0,0,.75) 1.1em 1.1em 0 0,rgba(0,0,0,.75) 0 1.5em 0 0,rgba(0,0,0,.75) -1.1em 1.1em 0 0,rgba(0,0,0,.5) -1.5em 0 0 0,rgba(0,0,0,.5) -1.1em -1.1em 0 0,rgba(0,0,0,.75) 0 -1.5em 0 0,rgba(0,0,0,.75) 1.1em -1.1em 0 0;box-shadow:rgba(0,0,0,.75) 1.5em 0 0 0,rgba(0,0,0,.75) 1.1em 1.1em 0 0,rgba(0,0,0,.75) 0 1.5em 0 0,rgba(0,0,0,.75) -1.1em 1.1em 0 0,rgba(0,0,0,.75) -1.5em 0 0 0,rgba(0,0,0,.75) -1.1em -1.1em 0 0,rgba(0,0,0,.75) 0 -1.5em 0 0,rgba(0,0,0,.75) 1.1em -1.1em 0 0}@-webkit-keyframes spinner{0%{-webkit-transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-o-transform:rotate(360deg);transform:rotate(360deg)}}@-moz-keyframes spinner{0%{-webkit-transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-o-transform:rotate(360deg);transform:rotate(360deg)}}@-o-keyframes spinner{0%{-webkit-transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-o-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes spinner{0%{-webkit-transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-o-transform:rotate(360deg);transform:rotate(360deg)}}
/* .modal-fullscreen */
.modal-fullscreen{background:0 0}.modal-fullscreen .modal-content{background:0 0;border:0;-webkit-box-shadow:none;box-shadow:none}.modal-backdrop.modal-backdrop-fullscreen{background:#fff}.modal-backdrop.modal-backdrop-fullscreen.in{opacity:.97;filter:alpha(opacity=97)}.modal-fullscreen .modal-dialog{margin:0 auto;width:100%}@media (min-width:768px){.modal-fullscreen .modal-dialog{width:750px}}@media (min-width:992px){.modal-fullscreen .modal-dialog{width:970px}}@media (min-width:1200px){.modal-fullscreen .modal-dialog{width:1170px}}
.information {  background-color: #006dd5;  border-radius: 9px;  color: #fff;
  font-size: 12px;  padding:1px;}
.field-workorders-beacongroup{ display:none;width:100%;}
h4.bg-info {font-size:14px; line-height: 1.4;padding: 1px 5px;  text-align: justify;}
</style>
