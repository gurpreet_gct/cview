<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Country;
use common\models\States;

/* @var $this yii\web\View */
/* @var $model app\models\Taxs */
/* @var $form yii\widgets\ActiveForm */

?>



    <?php $form = ActiveForm::begin(); ?>
	 <div class="box-body">
      <div class="form-group">
	 <?php //echo $form->errorSummary($model); ?>
	 
	    <?= $form->field($model, 'taxClass')->textInput()->label() ?>
		
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Taxs')]) ?>
    </div>
    </div>
     

    <?php ActiveForm::end(); ?>

