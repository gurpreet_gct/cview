<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use paras\cropper\Widget;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
$usertype = Yii::$app->user->identity->user_type;
$agency_id = Yii::$app->user->identity->advertister_id;
$id = Yii::$app->user->identity->id;
$partenerId = $usertype == 7 ? $id : $agency_id;
?>
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-user-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]);
    ?>
     <div class="box-body">
      <div class="form-group">
		    <?php //echo $form->errorSummary($model); ?>
	  <?php if(Yii::$app->user->identity->user_type==1){ ?>
     <?= $form->field($model, 'advertister_id')->dropDownList(common\models\Profile::getAdvertiser(), array('prompt'=>'Select')) ?>
	<?php }else{ ?>
	<?= $form->field($model, 'advertister_id')->hiddenInput(['value' => $partenerId])->label(false) ?>
	<?php	}?>
	<?= $form->field($model, 'storeid')->dropDownList(backend\models\Usermanagement::getStorelist($partenerId),array('prompt'=>'Select Store Location')) ?>
    <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'lastName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fullName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?php if(!isset($model->id)) { ?>
    <?= $form->field($model, 'password_hash')->passwordInput(['class'=>'form-control'])->label(Yii::t('app','Portal password')) ?>
     <?= $form->field($model, 'confirmpass')->passwordInput(['class'=>'form-control'])->label(Yii::t('app','Confirm Portal Password')) ?>
   <?php  } ?>
    <?= $form->field($model, 'status')->dropDownList(array('' => Yii::t('app','Select'),10 => Yii::t('app','Enable'), 0 => Yii::t('app','Disable'))) ?>
     <?= $form->field($model, 'loyaltyPin',[
    'template' => '{label} <span class="information" data-toggle="tooltip" data-placement="right" title="This PIN enables an employee to redeem a pass in-store, add loyalty dollors in-store, confirm pick up of goods, stamp a digital stamp card.">&nbsp;&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;&nbsp;</span><div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>'])->textInput(['maxlength' => true])->label(Yii::t('app','6-Digit Loyalty PIN')) ?>
	<?php
		echo $form->field($model, 'user_type', [
		    'inputOptions' => [
		         'type' =>'hidden',
		         'value' =>9,
		    ],
		])->label(false);
		?>
		<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'dob')->widget(
		DatePicker::className(), [
			'language' => 'en',
			'clientOptions' => [
				'autoclose' => true,
				'endDate' => date('2005-12-31'),
				'format' => 'yyyy-mm-dd',
			]
	]);?>
<div class="help-block"></div>
    <?= $form->field($model, 'phone')->textInput() ?>
    <?= $form->field($model, 'sex')->dropDownList(array('' =>Yii::t('app','Select'), '1' => Yii::t('app','Male'), '0' => Yii::t('app','Female') )) ?>
   <?php if($model->image !=''){
		$model->imageFile=$model->imagename;
		//echo '<img height="150" width="150" src="'.$model->imagename.'"/>';
		} ?>
    <?= $form->field($model, 'imageFile')->widget(Widget::className(), [
	'uploadUrl' => Url::toRoute('employee/uploadPhoto'),'width'=>150,
	'height'=>150])   ?>
    </div><!-- /.box-body -->
     <div class="box-footer">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Users')]) ?>
    </div>
     </div>
    <?php $siteurl = Yii::$app->getUrlManager()->getBaseUrl().'/employee/storelist';
  ?>
    <?php ActiveForm::end(); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#usermanagement-advertister_id').trigger('change');
	});
	$('#usermanagement-advertister_id').change(function(){
		var partnerId = $(this).val();
		$.ajax({
			method	:'POST',
			url		: '<?php echo $siteurl; ?>',
			data	: { partnerId : partnerId }
			})
			.done(function(result){
				$('#usermanagement-storeid').empty();
				data1 = JSON.parse(result);
				var selected = "<?php echo $model->storeid ?>";
				 var div_data1="<option value=''>select store</option>";
				 $('#usermanagement-storeid').append(div_data1);
				$.each(data1.data, function (key, data) {
					var param = '';
					if(selected==data.id){
						param = 'selected="selected"';
					}
					var div_data="<option "+param+" value="+data.id+">"+data.storename+"</option>";
				  $('#usermanagement-storeid').append(div_data);
				});
				$('#usermanagement-storeid').trigger('change');
			});
	});
</script>
<style>
.information {  background-color: #006dd5;  border-radius: 9px;  color: #fff;
  font-size: 12px;  padding: 1px;}
</style>
