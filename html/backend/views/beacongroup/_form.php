<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\models\Beacongroup;
use common\models\States;


/* @var $this yii\web\View */
/* @var $model app\models\Regions */
/* @var $form yii\widgets\ActiveForm */

 $CountryName =  ArrayHelper::map(Beacongroup::find()->Where(['type' =>1])->all(), 'id', 'group_name');
 if(isset($model->grandparent_id)){
 $stateName =  ArrayHelper::map(Beacongroup::find()->Where(['type' =>2])->all(), 'id', 'group_name');
 }else {
 $stateName = array();
 }

 $level=array("0"=>"General","1"=>"Country","2"=>"State","3"=>"City","4"=>"Food Order Checkout");
?>



    <?php $form = ActiveForm::begin(); ?>
	 <div class="box-body">
      <div class="form-group">
	 <?php //echo $form->errorSummary($model); ?>

	<?= $form->field($model, 'type')->widget(Select2::classname(), [
		'data' => $level,
		'options' => ['placeholder' => 'Select level ...','multiple' => false],
		'pluginOptions' => [
			'allowClear' => true,

		],
		]);
	?>

    <?= $form->field($model, 'group_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'grandparent_id')->widget(Select2::classname(), [
		'data' => $CountryName,
		'options' => ['placeholder' => 'Select country ...','multiple' => false],
		'pluginOptions' => [
			'allowClear' => true
		],
		]);
	?>

	<?= $form->field($model, 'parent_id')->widget(Select2::classname(), [
		'data' => $stateName,
		'options' => ['placeholder' => 'Select state ...','multiple' => false],
		'pluginOptions' => [
			'allowClear' => true
		],
		]);
	?>

	<?php $form->field($model, 'city')->textInput(['maxlength' => true]) ?>


	<?= $form->field($model, 'status')->dropDownList(array('' => Yii::t('app','Select'), 1 =>Yii::t('app','Enable'), 0 =>Yii::t('app','Disable') )) ?>


    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Users')]) ?>
    </div>
     </div>


    <?php ActiveForm::end(); ?>

<?php $siteurl = Yii::$app->getUrlManager()->getBaseUrl();
$countrywisestate =$siteurl.'/beacongroup/countrywisestate'; ?>

<style>

.form-group.field-beacongroup-grandparent_id{ display:none;}
.form-group.field-beacongroup-parent_id{ display:none;}
.form-group field-beacongroup-city{ display:none;}

</style>

<script type="text/javascript">

	$("#beacongroup-grandparent_id").on("change", function () {
		$('#select2-beacongroup-parent_id-container').empty();
		var values = this.value;
		$.ajax({
			method: "POST",
			url: "<?php echo $countrywisestate; ?>",
			data: { id: values }

	})
	.done(function(result) {
		data1 = JSON.parse(result);
		$('#beacongroup-parent_id').empty();
		 var div_data1="<option value=''><?= Yii::t('app','select') ?></option>";
		$.each(data1, function (key, data) {
			$.each(data, function (index, data) {
				 var div_data="<option value="+index+">"+data+"</option>";
					$(div_data1).appendTo('#beacongroup-parent_id');
					$(div_data).appendTo('#beacongroup-parent_id');

			})
		});

	  });

	});

	$("#beacongroup-parent_id").on("change", function () {
		$('#beacongroup-city').val("");
		;

	});

	 var values= $('#beacongroup-type').val();
		if(values==1)	{
			$('.form-group.field-beacongroup-group_name').show();
			$('.form-group.field-beacongroup-grandparent_id').hide();
			$('.form-group.field-beacongroup-parent_id').hide();
		}else if(values==2)	{
			$('.form-group.field-beacongroup-group_name').show();
			$('.form-group.field-beacongroup-grandparent_id').show();
			$('.form-group.field-beacongroup-parent_id').hide();
		}else if(values==3)	{
			$('.form-group.field-beacongroup-group_name').show();
			$('.form-group.field-beacongroup-grandparent_id').show();
			$('.form-group.field-beacongroup-parent_id').show();
		}else if(values==0 || values==4)	{
			$('.form-group.field-beacongroup-group_name').show();
			$('.form-group.field-beacongroup-grandparent_id').hide();
			$('.form-group.field-beacongroup-parent_id').hide();
		}

	$("#beacongroup-type").on("change", function () {
		var values = this.value;
		if(values==1)	{
			$('.form-group.field-beacongroup-group_name').show();
			$('.form-group.field-beacongroup-grandparent_id').hide();
			$('.form-group.field-beacongroup-parent_id').hide();
		}else if(values==2)	{
			$('.form-group.field-beacongroup-group_name').show();
			$('.form-group.field-beacongroup-grandparent_id').show();
			$('.form-group.field-beacongroup-parent_id').hide();
		}else if(values==3)	{
			$('.form-group.field-beacongroup-group_name').show();
			$('.form-group.field-beacongroup-grandparent_id').show();
			$('.form-group.field-beacongroup-parent_id').show();
		}else if(values==0 || values==4)	{
			$('.form-group.field-beacongroup-group_name').show();
			$('.form-group.field-beacongroup-grandparent_id').hide();
			$('.form-group.field-beacongroup-parent_id').hide();
		}


	});

	</script>
