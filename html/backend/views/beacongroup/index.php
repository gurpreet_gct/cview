<?php

use yii\helpers\Html;
#use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Beacongroup;
use common\models\States;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RegionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Beacon Group');
$this->params['breadcrumbs'][] = $this->title;

 $CountryName =  ArrayHelper::map(Beacongroup::find()->Where(['type' =>1])->all(), 'id', 'group_name');
 if(isset($model->grandparent_id)){
 $stateName =  ArrayHelper::map(Beacongroup::find()->Where(['type' =>2])->all(), 'id', 'group_name');
 }else {
 $stateName = array();
 }
$level=array("0"=>"General","1"=>"Country","2"=>"State","3"=>"City","4"=>"Food Order Checkout");
?>


<div class="beacon-group-index">

<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
            'group_name',
            [
            'attribute'=>'type',           			
			'value'=> 'levelNameval',                   
                  'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $level,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => Yii::t('app','Select level')],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

            ],
            ],
            
         /* [
            'attribute'=>'grandparent_id',           			
			'value'=> 'CountryNameval',                   
                  'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $CountryName,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => Yii::t('app','Select country')],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

            ],
            ],
             
            [
            'attribute'=>'parent_id',           			
			'value'=> 'StateNameval',                   
                  'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $stateName,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => Yii::t('app','Select state')],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

            ],
            ],
             
            'city',  */
                      
            [
            'attribute'=>'status',
            'value'=>'statustype',
            'filter' => array('1' => Yii::t('app','Enable'), '0' => Yii::t('app','Disable') ),
            ],

           [
            'attribute'=>'Created At',
          	'value' => function ($dataProvider) {
            return date("D, d M Y H:i:s ",$dataProvider["created_at"]);
               
       		 },
            ],
            
           ['class'=>'kartik\grid\ActionColumn'],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>Yii::t('app','Add Beacon Group'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-gears"></i>'.Yii::t('app','Beacon Group'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
</div>


    <?php $siteurl = Yii::$app->getUrlManager()->getBaseUrl(); 
$countrywisestate =$siteurl.'/beacongroup/countrywisestate'; ?>   
    
<script type="text/javascript">
	
	$("#beacongroupsearch-grandparent_id").on("change", function () {			
		$('#select2-beacongroup-parent_id-container').empty();			
		var values = this.value;		
		$.ajax({
			method: "POST",
			url: "<?php echo $countrywisestate; ?>",
			data: { id: values }	
	
	})
	.done(function(result) {	
		data1 = JSON.parse(result);		
		$('#beacongroup-parent_id').empty();	
		 var div_data1="<option value=''><?= Yii::t('app','select') ?></option>";         	
		$.each(data1, function (key, data) {   
			$.each(data, function (index, data) {					 
				 var div_data="<option value="+index+">"+data+"</option>"; 
					$(div_data1).appendTo('#beacongroup-parent_id'); 
					$(div_data).appendTo('#beacongroup-parent_id'); 
				
			})
		});
		
	  });
		
	});
	
	
</script>
