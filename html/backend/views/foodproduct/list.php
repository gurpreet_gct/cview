<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchFoodProduct */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Products List');
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="well-well-lg">
<div class="row line-border">
<?php
echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
			 'name',
			  [
				'attribute'=>'image',
				'value'=>'thumbimage',
				'format' => ['image',['width'=>'75','height'=>'75']],
            ],
            'sku',
            'price',			
			['class'=>'kartik\grid\ActionColumn'],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], 
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true,       
        'beforeHeader'=>[
            [           
                'options'=>['class'=>'skip-export']
            ]
        ],
          'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>Yii::t('app','Add User'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
          'export'=>[
            'fontAwesome'=>true
        ],       
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-cutlery"></i>'.Yii::t('app','Products list'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
?>
</div>
</section>
<style>
.row.line-border {
  margin-left: 4px;
  margin-top: 21px;
}
</style>




