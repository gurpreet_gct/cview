<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\FoodProductsType;
use common\models\FoodProduct;
use common\models\User;
/* @var $this yii\web\View */
/* @var $model common\models\FoodProduct */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$form = ActiveForm::begin([
	'options' => [
	'id' => 'create-food-product-form',	
	'enctype' =>'multipart/form-data',		
	]
]);
?>
<div class="box-body">
	<div class="form-group">
		<?php //echo $form->errorSummary($model); ?>
		<!-- Simple Product type form start from here -->
		<?php if(isset($_GET['id'])) { ?>
		<?php if($model->isNewRecord){ ?>
			<?= $form->field($model, 'p_type')->hiddenInput(['value' =>$_GET['pid']])->label(false) ?>
		<?php } else { ?>
			<?= $form->field($model, 'p_type')->hiddenInput(['value' =>$_GET['id']])->label(false) ?>
		<?php } ?>
		<div class="product-forms">
		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'description')->textArea(['rows' => '6']) ?> 
			<?= $form->field($model, 'urlkey')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'price')->textInput() ?>
			<?= $form->field($model, 'status')->dropDownList(array('' =>Yii::t('app','Select'), '1' => Yii::t('app','Enable'), '0' => Yii::t('app','Disable') )) ?>
			<?= $form->field($model, 'is_online')->dropDownList(array('' =>Yii::t('app','Select'), '1' => Yii::t('app','Yes'), '0' => Yii::t('app','No') )) ?>					
			<?php 	$storeList =  ArrayHelper::map(User::find()->select('id,username')
							->where('user_type=7')->all(), 'id', 'username');
				echo $form->field($model, 'store_id')->widget(Select2::classname(), 
							[
							'data' => $storeList,
							'options' => ['placeholder' => Yii::t('app','Select store ...'),'multiple' => false],
							'pluginOptions' => [
							'allowClear' => true
							],
				]); 
			?>
			<?= $form->field($model, 'imageFile')->fileInput() ?>
			<?php if($model->image !=''){		
				echo '<img height="150" width="150" src="'.$model->thumbimage.'"/>';		
			} 
			/* show product list for group products */
			if($_GET['id'] ==2 && $model->isNewRecord){
				$pList =  ArrayHelper::map(FoodProduct::find()->select('id,sku')
							->where(['status'=>1,'p_type'=>1])->all(), 'id', 'sku');
				echo $form->field($model, 'related_product')->widget(Select2::classname(), 
							[
							'data' => $pList,
							'options' => ['placeholder' => Yii::t('app','Select product ...'),'multiple' => true],
							'pluginOptions' => [
							'allowClear' => true
							],
				]); 
			}
			?>
			<div class="help-block"></div>
			<!-- Show categories -->	
		  <label class="control-label" for="store-owner"><?= Yii::t('app','Select Categories') ?> </label>
			<div class="chosentree"></div> 
			
		</div>
		<!-- Simple Product type form end from here -->
		<!-- Select Product type form start from here -->
		<?php } else {  ?> 
		<div class="product-type">
			<?php 
			$FoodProductsType =  ArrayHelper::map(FoodProductsType::find()
								->select('id,name')->all(), 'id', 'name');
				echo $form->field($model, 'p_type')->widget(Select2::classname(), 
					[
						'data' => $FoodProductsType,
						'options' => ['placeholder' => Yii::t('app','Select Product Type ...'),'multiple' => false],
						'pluginOptions' => [
						'allowClear' => true
						],
					]); 
			?>
		</div>
		<?php } ?>
		<!-- Select Product type form end from here --> 
	</div><!-- /.box-body -->
</div><!-- /.box-body -->
<?php if(isset($_GET['id'])) { ?>
<div class="box-footer">
	<div class="form-group">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::a('Cancel',  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','App Version')]) ?>
	</div>
</div>
<?php } else { ?>
<div class="box-footer">
	<div class="form-group">
	<?= Html::a('<span class="btn-label">Continue</span>', ['create', 'id' =>0], ['class' => 'btn btn-primary continue']) ?>
	<?= Html::a('Cancel',  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app0','App Version')]) ?>
	</div>
</div>
<?php } ?>
<?php ActiveForm::end(); ?>
<?php 

$this->registerJs(

    '
var loadChildren = function(node) {
	 
JSONObject =JSON.parse(\' '.$categoryTree.'\');	 

	 //JSONObject = JSON.parse(\'{"id":"01","title":"Node 01","has_children":true,"checked":true,"level":1,"children":[{"id":"011","title":"Node 011","name":"paras","has_children":true,"level":2,"children":[{"id":"0111","title":"Node 0111","has_children":false,"level":3,"children":[]}]}]}\');

	 //for(i=0;i<JSONObject.length;i++)
	 
	node.children.push(JSONObject);	 
	// console.log(node);
	 
        return node;
      };

$(document).ready(function() {
        $("div.chosentree").chosentree({
            width: 500,
			input_placeholder: "Select Category",
            deepLoad: true,
            showtree: true,
            load: function(node, callback) {
                    setTimeout(function() {
          callback(loadChildren(node));
       }, 1000);
            }
        });
});
		'
);
    
    
$this->registerJsFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.js', ['position' => \yii\web\View::POS_END]);
$this->registerCssFile(Yii::$app->homeUrl.'treeselect/bin/jquery.treeselect.css', ['position' => \yii\web\View::POS_END]);
?>
<script type="text/javascript">
$('#foodproduct-p_type').change(function(){
	var id = $('select[id=foodproduct-p_type]').val();
	var aUrl = '<?php echo \Yii::$app->request->BaseUrl;?>/foodproduct/create?id='+id+'&pid='+id;
	$('.btn.btn-primary.continue').attr('href',aUrl);
});
$('.btn.btn-primary.continue').click(function(e){
	e.preventDefault();
	var id = $('select[id=foodproduct-p_type]').val();
	if(id>0){
	window.location=$('.btn.btn-primary.continue').attr('href');
	}
	else{
		alert('Please select product type');
	}
});
$(document).ready(function() {	
	setTimeout(function() {
		$('.level-1').attr('disabled',true);
		$('.level-2').attr('disabled',true);
		$('input:checkbox:checked').each(function() {
			var ids = this.id;
		   if(parseInt(ids.length)>1);
		   {
			   $('#'+ids).click();	 
				$('#'+ids).click();	 
		   }
		});
	      $('.level-3').change(function(){
			  var id = $(this).attr('id');	
			  if($( ".chzntree-choices.chosentree-choices li" ).size()>2)
			  {
				   $('.chzntree-choices.chosentree-choices a').click();		
					$('input#'+id).click();				
			  }else{							
				$('input#'+id).attr('checked',true);	
			  }				
		});
       }, 1500);
});
</script>

