<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FoodProduct */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => yii::t('app','Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
          <div class="row">
            <!-- left column -->
             <div class="col-md-12">
			 <div id="message">
					<?= Yii::$app->session->getFlash('sucess');?>
			</div>
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="fa fa-cutlery"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
             <?= Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>     		
          <?= Html::a(Yii::t('app','OK'), ['index'], ['class' => 'btn bg-green']) ?>
		   </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'sku',
            ['label'=>Yii::t('app','Category'),'value'=>$model->catname,],
            'description',
           // 'short_description',
            'urlkey',
            'price',
            [
				'label'=>Yii::t('app','Status'),
				'value'=>$model->status = $model->status==1?'Active':'Disable'
			],
			[
				'label'=>Yii::t('app','is online'),
				'value'=>$model->is_online = $model->is_online==1?'Yes':'No'
			],
            [
				'label'=>Yii::t('app','Store'),
				'value'=>$model->storename,
			],
			 [
				'attribute'=>'image',				
				'format' => ['image',['width'=>'70','height'=>'70']],
			 ],
            [
				'label'=>Yii::t('app','Associated Product'),
				'value'=>$model->associatedpro,
			],
           [
			'label'=>Yii::t('app','Create on'),
			'value'=>date('Y-m-d H:s',$model->created_at),
		   ], 
		   [
			'label'=>Yii::t('app','Updated On'),
			'value'=>date('Y-m-d H:s',$model->updated_at),
		   ],
		   [
			'label'=>Yii::t('app','Created By'),
			'value'=>$model->username,],
		   [
			'label'=>Yii::t('app','Updated By'),
			'value'=>$model->username,
		   ],          
        ],
    ]) ?>

</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>		

