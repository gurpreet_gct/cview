<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FoodProduct */

$this->title = Yii::t('app','Create Food Product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Food Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-plus"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
                
                <div class="panel-before" >    
                <div class="pull-right">
        <div role="toolbar" class="btn-toolbar kv-grid-toolbar">
            <div class="btn-group"><a title="<?= Yii::t('app','App Vesrion') ?>" href="<?=Yii::$app->urlManager->createUrl(['foodproduct/index',])?>" class="btn btn-primary"><i class="fa fa-cutlery"></i></a> 
            <a data-pjax="0" title="<?= Yii::t('app','Reset Grid') ?>" href="<?=Yii::$app->urlManager->createUrl(['foodproduct/create',])?>" class="btn btn-default"><i class="glyphicon glyphicon-repeat"></i></a></div>
        </div>    
    </div>
    
    <div class="clearfix"></div></div>
                
                
                <!-- form start -->
                
                 <?= $this->render('_form', [
				'model' => $model,
				'categoryTree' =>$categoryTree,
				]) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
