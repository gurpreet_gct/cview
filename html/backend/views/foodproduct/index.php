<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchFoodProduct */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Products List');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="users-index">
<?php
echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
		  [
				'attribute'=>'image',
				'value'=>'thumbimage',
				'format' => ['image',['width'=>'75','height'=>'75']],
            ],
			 'name',			
            'sku',
            'price',
			[
				'attribute'=>'p_type',
				'value'=>'ptype',
				'filter' => array('2' => yii::t('app','FoodOrder-Simple'), '3' => yii::t('app','FoodOrder-Group')),
				
			],
			
			['class'=>'kartik\grid\ActionColumn',
			
			   'contentOptions' => ['style' => 'width:260px;'],
        'header'=>'Actions',
        'template' => '{update} {view} {delete}',
        'buttons' => [
        
           //delete button
            'delete' => function ($url, $model) {
				
               return '<a class="custom-del">	<span class="glyphicon glyphicon-trash" OnClick="DeleteAction('.$model->id.')" >  </span>
								</a> <input type="hidden" name="url" value="'.$url.'" id="url"'; 
            },
             
        ],
      
		            
		'urlCreator' => function ($action, $model, $key, $index) {
			
			$baseurl = Yii::$app->getUrlManager()->getBaseUrl(); 
			
		
					$redirectUrl=['catalog/update', 'id' => $model->id];
					$redirectUrlview=['catalog/view', 'id' => $model->id];	
					
					if((Yii::$app->request->getQueryParam("pid")))
					{
						$redirectUrl['pid']=Yii::$app->request->getQueryParam("pid");
						$redirectUrlview['pid']=Yii::$app->request->getQueryParam("pid");
						
						
					}
					
		     
				
			if ($action === 'update') {
                $url =$redirectUrl;
                return $url;
			}
			
			if ($action === 'view') {
                $url = $redirectUrlview;
                return $url;
			}
		
			if ($action === 'delete') {					
                $url =$baseurl.'/catalog/delete';
                return $url;
			}
		

           
	   }
			],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], 
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true,       
        'beforeHeader'=>[
            [           
                'options'=>['class'=>'skip-export']
            ]
        ],
          'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['catalog/create'],[ 'title'=>Yii::t('app','Add Product'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
          'export'=>[
            'fontAwesome'=>true
        ],       
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-cutlery"></i>'.Yii::t('app','Products list'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
?>
</div>

<style>
.row.line-border {
  margin-left: 4px;
  margin-top: 21px;
}
</style>




