<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\GeoZone */

$this->title = 'Create Geo Zone';
$this->params['breadcrumbs'][] = ['label' => 'Geo Zones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-zone-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
