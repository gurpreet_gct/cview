<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\GeoZone */

$this->title = 'Update Geo Zone: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Geo Zones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="geo-zone-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
