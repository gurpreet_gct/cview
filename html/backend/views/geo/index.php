<?php
 
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\GeoZone;
use common\models\Workorders;
use backend\models\Usermanagement;
 
/* @var $this yii\web\View */
/* @var $searchModel backend\models\GeoZoneSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Geo Zones';
$this->params['breadcrumbs'][] = $this->title;
 
 	$userType = Yii::$app->user->identity->user_type;
		if ($userType == 7){
		$userId = $userType== 7 ? Yii::$app->user->identity->id : Yii::$app->user->identity->advertister_id;
		$dealIds  = ArrayHelper::map(Workorders::find()->where('workorderpartner='.$userId)
               ->asArray()
               ->all(),'id','name');
   }else{
	   $dealIds  = ArrayHelper::map(Workorders::find()
               ->asArray()
               ->all(),'id','name');
	   }
  
/*$adverIds  = ArrayHelper::map(Usermanagement::find()
               ->asArray()
               ->all(),'id','orgName');*/
//echo '<pre>';print_r($adverIds);die();


$userType=Yii::$app->user->identity->user_type;
$visiable = 0;
$advertiser='';
$partners= array();
if($userType==1){
$partners = ArrayHelper::map(Usermanagement::find() ->where(['user_type' => 7])->joinwith('workorders',false,'INNER JOIN')->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
$visiable = 1;
}
//echo '<pre>';print_r($partners);die();
?>
<div class="geo-zone-index">

<?php
 
 
 
        echo GridView::widget([
        'dataProvider'=>$dataProvider,
       'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
                'area_name',
            'latitude',
            'longitude',
                   [
            'attribute' =>'deal_id',
           'value' => 'dealtype',
           'format' => 'raw',
            'filter' => $dealIds,
			'filterType'=>GridView::FILTER_SELECT2,
               'filterWidgetOptions' => [            
         //     'data' => $dealIds,
              'size' => Select2::SMALL,
           'options' => ['placeholder' => Yii::t('app','Select deal')],
               'pluginOptions' => [                    
                   'width'=>'150px',
                   'allowClear' => true,
                   ],
                   
 
               ],
            ],
            
		 [
            'attribute' =>'agency_id',
           'value' => 'advertiser',
           'format' => 'raw',
            'visible'=> $visiable,
           'filter' => $partners,
          'filterType'=>GridView::FILTER_SELECT2,
               'filterWidgetOptions' => [            
         // 'data' => $Advertiser,
              'size' => Select2::SMALL,
           'options' => ['placeholder' => Yii::t('app','Select Partner')],
               'pluginOptions' => [                    
                   'width'=>'150px',
                   'allowClear' => true,
                   ],
                   
 
               ],
            ],
           
                        
           ['class'=>'kartik\grid\ActionColumn',
           'contentOptions' => ['style' => 'width:260px;'],
        'header'=>'Actions',
        'template' => '{view}{update}{delete}',
 
     
 
        'urlCreator' => function ($action, $model, $key, $index) {
           
           $baseurl = Yii::$app->getUrlManager()->getBaseUrl();             
           $advertiserID = $model->advertiserId($model->deal_id);
           if ($action === 'view') {
                $url =$baseurl.'/workorders/createzone?id='.$advertiserID.'&did='.$model->deal_id;
                return $url;
           }
       
           
           if ($action === 'update') {
                $url =$baseurl.'/workorders/createzone?id='.$advertiserID.'&did='.$model->deal_id;              return $url;
           }    
        if ($action === 'delete') {                    
               $url =$baseurl.'/geo/delete?id='.$model->id;      
                return $url;
          }
   
   }
 
       ],
        
           
        
            ],
            
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
        'pjaxSettings'=>[
        'neverTimeout'=>true,
          'options'=>[
                'id'=>'kv-unique-id-1',
            ]
    ],
  
      //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
             Html::a('<i class="glyphicon glyphicon-plus">Geolocation</i>', ['/workorders/createzone'],[ 'title'=>Yii::t('app','Geolocation'),'data-pjax'=>1, 'class'=>'btn btn-primary', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> yii::t('app','Reset Grid')])
              
            ],
            
            
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-geo"></i>'.yii::t('app','Geolocation'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    
 
?>
