 
<?php
use yii\helpers\Html;
#use yii\grid\GridView;

use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use backend\models\OrderStatus;
//use dosamigos\datepicker\DatePicker;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RegionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app','Cancelled Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="regions-index">
	<?php   $OrderStatusId =  ArrayHelper::map(OrderStatus::find()->all(), 'id', 'status'); ?>
<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
         'rowOptions' => function ($model, $key, $index, $grid) {
		return ['id' => $model['id']];
},
        'columns'=>  [
        [
            'attribute'=>'id',
            'value'=>'id',
            ],
           
               [
			   'attribute' => 'created_at',
			  
                   'value'=>'created_at',
                   'format'=>'date', 
                    'filterType'=>GridView::FILTER_DATE_RANGE,
                 'filterWidgetOptions' => [

					'convertFormat'=>true,
					'pluginOptions'=>[
					'locale'=>[
						'format'=>'Y-m-d',
						'separator'=>' - ',
					],],

					],
			],
          [
            'attribute'=>'user_id',
            'value'=>'customername.username',
            ],
            [
            'attribute'=>'grand_total',
            'value'=>function ($data) {
                        return '$'.$data->grand_total;
                    },
            ],
          [
            'attribute'=>'status',
            'value'=>'OrderStatus.status',
            'filter' =>  ArrayHelper::map(OrderStatus::find()->all(), 'id', 'status'),
            ],
            
          ['class'=>'kartik\grid\ActionColumn','template' => '{view} {delete}',],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [            
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>            
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-shopping-cart"></i>'.Yii::t('app','Cancelled Orders'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
</div>
<script>

	//$(document.body).on("click", "#daterangepicker",function() {
	//alert('rr');
	//location.reload();
	/*$daterangepicker_start = $('#daterangepicker_start').val();
	$daterangepicker_end = $('#daterangepicker_end').val();
	alert($daterangepicker_start);
	alert($daterangepicker_end);
	//2016-01-07 to 2016-01-15
	$datavalue = $daterangepicker_start+' to '+$daterangepicker_end;
	$('#created_at').val($datavalue);
	//$( "#created_at" ).trigger( "keyup" );*/
	//$('#daterangepicker').daterangepicker();
	//return false;
//});
	 
/*$(document).ready(function(){ 
$("#created_at" ).click(function() {
 var loc = window.location;
    var pathName = loc.pathname;
alert(pathName);
return false;
});
});*/
/*$(document.body).on("click", "#daterangepicker",function() {
	alert('rr');
	$('#daterangepicker').daterangepicker();
	return false;
});*/

/*$(document.body).on('pjax:complete', function() {
	//alert('ffff');
$('#daterangepicker').daterangepicker();
return false;
});*/
</script>

