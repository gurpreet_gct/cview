<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\widgets\LinkPager;
use backend\models\OrderStatus;
use common\models\OrderComment;
use common\models\Usertype;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

//$dataProvidermodels = $dataProvider->getModels();
//echo"<pre>";
//print_r($model);
//die();
$usertypeid = Yii::$app->user->identity->id;
$this->title = Yii::t('app','View Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row">
            <!-- left column -->
          <?php //echo"<pre>"; print_r($model);?>
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="fa fa-shopping-cart"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
				<div class="col-md-12">
				<div class=" panel panel-primary">
								<div class="panel-heading">    
								<div class="pull-right"></div>
									<!-- Header Title-->
									<h3 class="panel-title"><?= Yii::t('app','Order#') ?><?php if(isset($model[0]['id'])) { echo $model[0]['id'];}?></h3>
									<!-- Header Title end -->
									<div class="clearfix"></div>
								</div>
								<div class="box-header with-border">
									<div class="col-md-6"><?= Yii::t('app','Order Date') ?></div>
									<div class="col-md-6"><b><?php 
									if(isset($model[0]['created_at'])) {
									echo $datetime =  date('M d Y h:i:s A', $model[0]['created_at']); } ?></b></div>
									<div class="col-md-6"><?= Yii::t('app','Order Status') ?></div>
									<div class="col-md-6" id="orderstatus"><b><?php  if(isset($model[0]['orderstatus'])) { echo $model[0]['orderstatus']; } ?></b></div>
									
								<div>
							</div>
						</div>
							</div>
					  </div>
				<div class="col-md-12 ">
								<div class=" panel panel-primary">
								<div class="panel-heading">    
								<div class="pull-right"></div>
									<!-- Header Title-->
									<h3 class="panel-title"><?= Yii::t('app','Account Information') ?> </h3>
									<!-- Header Title end -->
									<div class="clearfix"></div>
								</div>
								<div class="box-header with-border">
									<div class="col-md-6"><?= Yii::t('app','Customer Name') ?></div>
									<div class="col-md-6"><b><?php if(isset($model[0]['fullName'])) {  echo $model[0]['fullName']; } ?></b></div>
									<div class="col-md-6"><?= Yii::t('app','Email') ?></div>
									<div class="col-md-6"> <b><?php  if(isset($model[0]['useremail']))  { echo $model[0]['useremail']; } ?></b></div>
									<div class="col-md-6"><?= Yii::t('app','Customer Group') ?> </div>
									<div class="col-md-6"><b><?= Yii::t('app','General') ?></b></div>
									
								<div>
							</div>
						</div>
							</div>
							
					  
				</div>
			<!--	<div class="col-md-12">
				<div class="col-md-6">
						<div class="col-md-12 ">
								<div class=" panel panel-primary">
								<div class="panel-heading">    
								<div class="pull-right"></div>
									<!-- Header Title--
									<h3 class="panel-title"><?= Yii::t('app','Biling Address') ?></h3>
									<!-- Header Title end --
									<div class="clearfix"></div>
								</div>
								<div class="box-header with-border">
									<div class="col-md-12">
										<?php //if(isset($model[0]['billing_address']) && $model[0]['billing_address']!='') { echo $model[0]['billing_address']; } else{ echo 'N/A';}?>
										</div>
									
								<div>
							</div>
						</div>
							</div>
					   </div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12 ">
								<div class=" panel panel-primary">
								<div class="panel-heading">    
								<div class="pull-right"></div>
									<!-- Header Title--
									<h3 class="panel-title"><?= Yii::t('app','Shipping  Address') ?></h3>
									<!-- Header Title end --
									<div class="clearfix"></div>
								</div>
								<div class="box-header with-border">
									<div class="col-md-12">
									<?php // if(isset($model[0]['shipping_address']) && $model[0]['shipping_address']!='') { echo $model[0]['shipping_address'];}else{ echo 'N/A';}?>
									</div>
									
									
								<div>
							</div>
						</div>
							</div>
					   </div>
				</div>
			</div> -->
				<div class="col-md-12 ">
								<div class=" panel panel-primary">
								<div class="panel-heading">    
								<div class="pull-right"></div>
									<!-- Header Title-->
									<h3 class="panel-title"><?= Yii::t('app','Payment Information') ?></h3>
									<!-- Header Title end -->
									<div class="clearfix"></div>
								</div>
								<div class="box-header with-border">
									<div class="col-md-6"><?= Yii::t('app','Credit Card Type') ?></div>
									<div class="col-md-6"><b><?php if(isset($model[0]['cardType'])) {  
										echo $model[0]['cardType']; }else{ echo Yii::t('app','N/A');} ?></b></div>
									<div class="col-md-6"><?= Yii::t('app','Credit Card Number') ?></div>
									<div class="col-md-6"> <b><?php if(isset($model[0]['maskedNumber'])) {  
										echo $model[0]['maskedNumber']; }else{ echo Yii::t('app','N/A');} ?></b></div>
									
									<div class="col-md-6"><?= Yii::t('app','Currency code') ?></div>
									<div class="col-md-6"><b><?php if(isset($model[0]['customerLocation'])) {  
										echo $model[0]['customerLocation']; }else{ echo Yii::t('app','N/A');} ?></b></div>
								<div>
							</div>
						</div>
							</div>
					   </div>
			<!--	<div class="col-md-12 ">
								<div class=" panel panel-primary">
								<div class="panel-heading">    
								<div class="pull-right"></div>
									<!-- Header Title--
									<h3 class="panel-title"><?= Yii::t('app','Shipping & Handing Information') ?></h3>
									<!-- Header Title end --
									<div class="clearfix"></div>
								</div>
								<div class="box-header with-border">
									<div class="col-md-6"><?= Yii::t('app','FedEx-Groung') ?></div>
									<div class="col-md-6"></div>
									
									
								<div>
							</div>
						</div>
							</div>
					   </div> -->
				<div class="col-md-12">
			  <div class="regions-index">
<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
       // 'filterModel'=>$searchModel,
        // 'rowOptions' => function ($model, $key, $index, $grid) {
		//return ['id' => $model['id']];
//},
        'columns'=>  [
			[
				'attribute'=>'deal_id',
				'label'=>'Offer Title',
				'value'=>'workorderpopup.offerTitle',
            ],
			[
            'attribute'=>'product_name',
            'value'=>'product.name',
            ],            
            [
            'attribute'=>'product price',
            'value'=>
            function ($data) {
                        return '$'.$data->product_price;
                    }
            ,
            ],
           'qty',
           'deal_count',           
            [
            'attribute'=>'Tax',
            'value'=> function ($data) {
                        return '$'.$data->tax;
                    }
            ,
            ],
              [
            'attribute'=>'Discount',
            'value'=> function ($data) {
                        return '$'.$data->discount;
                    }
            ,
            ],
			[
            'attribute'=>'Total',
            'value'=> function ($data) {
                        return '$'.$data->payable;
                    }
            ,
            ],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
        'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                // remove this row from export
            ]
        ],
        // set your toolbar
        
        // set export properties
       
        // parameters from the demo form
        'bordered'=>true,
		'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>Yii::t('app','Items Ordered'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
</div>	   
			   </div>
				<div class="col-md-12 ">
				  <div class="col-md-5 ">
								<div class=" panel panel-primary">
								<div class="panel-heading">    
								<div class="pull-right"></div>
									<!-- Header Title-->
									<h3 class="panel-title"><?= Yii::t('app','Comments History') ?></h3>
									<!-- Header Title end -->
									<div class="clearfix"></div>
								</div>
								<div class="box-header with-border">
									
									<div class="col-md-12"><?= Yii::t('app','Add  order Comments') ?></div>
									<div class="col-md-12">
						     
										<?php 
										if(isset($model[0]['useremail'])){
											$email = $model[0]['useremail'];
										}else{
											 $email ='';
											}
										if(isset($model[0]['id'])){
											$id = $model[0]['id'];
								    	}else{
											$id ='';
											}
										 $baseurl = Yii::$app->getUrlManager()->getBaseUrl();
										$form = \yii\widgets\ActiveForm::begin([
										'id' => 'my-form-id',
										'action' =>  $baseurl.'/orders/comment',
										'enableAjaxValidation' => false]); ?>
										<?php echo $form->errorSummary($orderComment); ?>
										<?php   $OrderStatusId =  ArrayHelper::map(OrderStatus::find()->all(), 'id', 'status'); 
										
										?>
										<?= $form->field($orderComment, 'status')->dropDownList($OrderStatusId, array('status'=>Yii::t('app','Select'))) ?>
										<?= $form->field($orderComment, 'comment')->textArea(['rows' => '6']); ?>
										<?= $form->field($orderComment, 'onWeb')->checkBox(); ?>
										<?= $form->field($orderComment, 'email_to')->hiddenInput(['value'=> $email])->label(false); ?>
										<?= $form->field($orderComment, 'author_id')->hiddenInput(['value'=> $usertypeid])->label(false); ?>
										<?= $form->field($orderComment, 'order_id')->hiddenInput(['value'=> $id])->label(false); ?>
										<?= Html::submitButton(Yii::t('app','Submit Comment'),['id' => 'submitid']); ?>
										<?php $form->end(); ?>
						
					  	      </div>
					  	      	<div class="col-md-12" id="commentlisting">	
									<?php 
									if(isset($model[0]['id'])){
											$id = $model[0]['id'];
								    	}else{
											$id ='';
											}
									$modelordercomment = OrderComment::find()->where(['order_id' => $id])->orderBy(['id' => SORT_DESC,])->all(); 
									foreach($modelordercomment as $modelordercommentinfo) { 
										 $modelorderstatus = OrderStatus::findOne(['id' => $modelordercommentinfo['status']]);
										$datetime =  date('M d Y h:i:s A', $modelordercommentinfo['created_at']);
										?>
										<div class="col-md-12"><b><?php echo $datetime.' '.$modelorderstatus['status'] ?></b>	</div>	
								<div class="col-md-12"><?= Yii::t('app','customer Notified') ?></div>
								<div class="col-md-12"><?php echo $modelordercommentinfo['comment'] ?></div>
								<?php 
								} ?>
								</div>
								<div>
							</div>
						</div>
							</div>
					  
				</div>
			<!----------- next div----------->
			<div class="col-md-7">
				<div class="col-md-12 ">
								<div class=" panel panel-primary">
								<div class="panel-heading">    
								<div class="pull-right"></div>
									<!-- Header Title-->
									<h3 class="panel-title"><?= Yii::t('app','Order Total') ?></h3>
									<!-- Header Title end -->
									<div class="clearfix"></div>
								</div>
								<div class="box-header with-border">
									<div class="col-md-6"><?= Yii::t('app','Subtotal') ?></div>
									
									<div class="col-md-6"><?php  if(isset($model[0]['subtotal'])) { echo '$'.$model[0]['subtotal']; } ?></div>
									
									<div class="col-md-6"><?= Yii::t('app','Shipping & Handing') ?></div>
									
									<div class="col-md-6"><?= '$'.$model[0]['shipping_amount']; ?></div>
									
									<div class="col-md-6"><?= Yii::t('app','Tax') ?></div>
									
									<div class="col-md-6"><?php  if(isset($model[0]['tax_amount'])) { echo '$'.$model[0]['tax_amount']; } ?></div>
									
									<div class="col-md-6"><?= Yii::t('app','Grand Total') ?></div>
									
									<div class="col-md-6"><?php  if(isset($model[0]['total_paid'])) { echo '$'.$model[0]['total_paid']; } ?></div>
									
							
																		
								<div>
							</div>
						</div>
							</div>
					   </div>
				</div>
			<!--------- end here---------->
			</div>
         <!------ end here----->      
                </div><!-- /.box-header -->
                <!-- form start -->
                </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
	<script>
	$( document ).ready(function() {
	$("#my-form-id").on("beforeSubmit",function(){
	var formid = $("#my-form-id").attr('action');
	$.ajax({
	url: formid,
	type: 'post',
	data: $("#my-form-id").serialize(),
	success: function (response) {
		var res = JSON.parse(response);
		$( "div.field-ordercomment-comment" ).removeClass( "has-success" );
		$( "div.field-ordercomment-status" ).removeClass( "has-success" );
		$( "#ordercomment-comment" ).val('');
	$("#commentlisting" ).replaceWith(res.html );
	$("#orderstatus" ).replaceWith(res.ordedrStatus );
	return false;
	}
	});
	return false;
	})
	});
	</script>

