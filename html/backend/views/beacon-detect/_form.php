<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Regions */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php $form = ActiveForm::begin(); ?>
	 <div class="box-body">
      <div class="form-group">
	 <?php //echo $form->errorSummary($model); ?>
    <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'region_code')->textInput(['maxlength' => true]) ?>

	 <?= $form->field($model, 'status')->dropDownList(array('' => Yii::t('app','Select'), 1 =>Yii::t('app','Enable'), 0 =>Yii::t('app','Disable') )) ?>

   
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Users')]) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>
