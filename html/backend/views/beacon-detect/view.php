<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

 
 
$this->title = Yii::t('app','View Beacon Detect: ') . ' ' .ucfirst($model->beaconUUID);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Beacon'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="fa fa-gears"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
        
     		   <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a(Yii::t('app','Cancel'), ['index'], ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
			[     
 'attribute'=>'customerId',			
            'label' => Yii::t('app','customer Name'),
            'value' => $model->customer->fullName,
             ],
            'beaconUUID',

			 'minorValue',
			 'majorValue',
			 'detect_at',
             
        ],
    ]) ?>

</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->