
 <div class="panel panel-primary">
    <div class="panel-heading">    <div class="pull-right">
        <div class="summary"></div>
    </div>
    <h3 class="panel-title">
        <i class="fa fa-edit"></i><?= Yii::t('app','Categories') ?>
    </h3>
    <div class="clearfix"></div></div>
    
<?php
use kartik\tree\TreeView;
use common\models\Categories;
 
echo TreeView::widget([
    // single query fetch to render the tree
    // use the Product model you have in the previous step
    'query' => Categories::find()->addOrderBy('root, lft'), 
    'headingOptions' => ['label' => Yii::t('app','Categories')],
    'fontAwesome' => false,     // optional
    'isAdmin' => false,         // optional (toggle to enable admin mode)
    'displayValue' => 1,        // initial display value
    'softDelete' => false,       // defaults to true
    'cacheSettings' => [        
     'enableCache' => true   // defaults to true
    ],
    'showCheckbox' => false,
    'emptyNodeMsgOptions' => ['class'=>'kv-node-message'],
    'showFormButtons' => true,
    'showTooltips' => true  
]);



?>

</div>
