<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
$usertype = Yii::$app->user->identity->user_type;

if($usertype==1) {
	$this->title = Yii::t('app','Alpha Wallet Admin Portal');
}else{
	$this->title = Yii::t('app','Alpha Wallet Partner Portal');
}
?>

<!-- charts start from here -->

	<div class="dashTopSection">
		<div class="topTabbing">
			<div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
				<?php 
				
					echo Html::a(Yii::t('app','AT A GLANCE'),['site/index'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','USERS'),['site/dashboard-user'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','DEAL/PASSES'),['site/dashboard-deal'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','BEACONS'),['site/dashboard-beacon'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','CAMPAIGN'),['site/dashboard-campaign'],['class'=>'btn btn-default active']);
					echo Html::a(Yii::t('app','LOYALTY'),['site/dashboard-loyality'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','SHOPPING CART/PAYMENT'),['site/dashboard-shoppping'],['class'=>'btn btn-default']);
				
				?>
			</div>
		</div>
		<div class="row margin">
			
			<div class="col-md-4"><?= Yii::t('app','DASHBOARD : CAMPAIGNS') ?></div>
			<div class="col-md-5">
				<button type="button" class="btn btn-default" onclick="return actionTypes('D')"><?= Yii::t('app','DAY') ?></button>
				<button type="button" onclick="return actionTypes('W')" class="btn btn-default"><?= Yii::t('app','WEEK') ?></button>
				<button type="button" onclick="return actionTypes('M')" class="btn btn-default"><?= Yii::t('app','MONTH') ?></button>
				<button type="button"  onclick="return actionTypes('Y')" class="btn btn-default"><?= Yii::t('app','YEAR') ?></button>
			</div>
			<div class="col-md-3"><?php 
						
							$end_date 	= new \DateTime(date('m/d/Y'));
							$end_date->modify('+1 day');
							$maxDate 	= $end_date->format('Y-m-d');
							
							echo DateRangePicker::widget([
											'id'=>'datetime_range',
											'name'=>'datetime_range',
											'readonly'=>'readonly',
											'convertFormat'=>true,
											'options' => ['placeholder' => Yii::t('app','Select date ...'),
															  'class' => 'form-control'
															  ],
											'pluginOptions'=>[
													'timePicker'=>true,
													'timePickerIncrement'=>30,
													'locale'=>[
														'format'=>'Y/m/d h:i:s A'
													],
													'maxDate'=> $maxDate
												]          
										]);
										
						?></div>
		</div>
		
		<section class="content">
			<div class="row">
				<div class="col-md-6">
				
					<!-- TOTAL CAMPAIGNS RUNNING -->
					<div class="box box-primary">
						<div class="box-header with-border text-left">
						  <i class="fa fa-bar-chart-o text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','TOTAL CAMPAIGNS RUNNING') ?></h4>
						</div>
						<div class="box-body">
							<div class="text-left">
							
								<?php 
									if(isset($campaignRunning) && !empty($campaignRunning)){
										$running_camp = isset($campaignRunning['running_camp'])?$campaignRunning['running_camp']:0;
										
									}
								
								?>
									<p><?= Yii::t('app','How many total campaigns running?') ?></p>
									<h3><?php echo  $running_camp; ?></h3>
							</div>
						
						</div><!-- /.box-body-->
						
					</div><!-- /.box -->
					
					<!-- CAMPAIGNS REDEMPTION -->
					
					<div class="box box-primary">
						<div class="box-header with-border">
						  <i class="fa fa-bar-chart-o text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','CAMPAIGNS REDEMPTION') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div class="campaign-redemption-chart" id="campaign-redemption-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->
					
					
				</div><!-- /.col -->

				<div class="col-md-6">
					
						<!-- TOP PERFORMING CAMPAIGNS -->
						
						<div class="box box-primary">
							<div class="box-header with-border">
							  <i class="fa fa-bar-chart-o text-primary"></i>
							  <h4 class="box-title"><?= Yii::t('app','TOP 5 PERFORMING CAMPAIGNS') ?></h4>
							</div>
							<div class="box-body">
								<div class="loading-image text-center">
									<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
								</div>
								<div id="top-perform-chart"></div>
							</div><!-- /.box-body-->
						</div><!-- /.box -->
						
						<!-- CAMPAIGNS BY CATEGORY -->
						<div class="box box-primary">
							<div class="box-header with-border">
							  <i class="fa fa-bar-chart-o text-primary"></i>
							  <h4 class="box-title"><?= Yii::t('app','CAMPAIGNS BY CATEGORY') ?></h4>
							</div>
							<div class="box-body">
								<div class="loading-image text-center">
									<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
								</div>
								<div id="line-chart"></div>
							</div><!-- /.box-body-->
						</div><!-- /.box -->
						
				</div><!-- /.col -->
				
			</div>
			
		</section>
	</div>
	
	<!--  Active Users chart  -->

	<!-- charts end from here -->
<link href="<?= Yii::$app->request->BaseUrl.'/css/'?>app.v1.css" rel="stylesheet">	
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>highcharts.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>create-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.easy-pie-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>export.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>app.plugin.js"></script>

<script>

	$('#datetime_range').on('change', function () { 

		var daterange_val 	= $(this).val();
		
		actionTypes('O',daterange_val);
		
	});
	
	var topCampaigns	=	[];
	var campRedeemp		=	[];
	
	function actionTypes(type,daterange,linktype){
		
		daterange 	= daterange || '';
		linktype 	= linktype || 'CA';
		
		var allData;
		
		var url = '<?php echo Yii::$app->request->BaseUrl; ?>/site/get-dashboard-report';
		
		allData = graphGenerator(type,url,daterange,linktype);
		
		var JSONObject 	= JSON.stringify(allData);
		data 			= jQuery.parseJSON(JSONObject);
						
		/* var sumTotal = 0;
		
		for(var i = 0; i < data.topCampaign.length; i++ ){
			sumTotal +=Number(data.topCampaign[i].topPerform);
		} */
		
		for(var i = 0; i < data.topCampaign.length; i++ ){
			
			// var perCamp = (Number(data.topCampaign[i].topPerform) / sumTotal) * 100;
			
			// topCampaigns.push([data.topCampaign[i].name,perCamp]);
			topCampaigns.push([data.topCampaign[i].name,Number(data.topCampaign[i].topPerform)]);
			
		}
		
		var inStore 	= [];
		var inApp 		= [];
		var campaignDate = [];
		
		for(var i = 0; i < data.campaignRedempt.length; i++ ){
			
			inStore.push(Number(data.campaignRedempt[i].inStore));
			inApp.push(Number(data.campaignRedempt[i].inApp));
			campaignDate.push(data.campaignRedempt[i].datetime);
			
		}
		
		
		// 	TOP 5 PERFORMING CAMPAIGNS
	
		$('#top-perform-chart').highcharts({
			chart: {
				height:150
			},
			title: {
				text: ''
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>Top 5 performing campaigns</small></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{point.name}: </td>' +
					'<td style="padding:0"><b>{point.percentage:.1f}%</b></td></tr>',
				footerFormat: '</table>',
				shared: false,
				useHTML: true
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b>',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					}
				}
			},
			series: [{
					type: 'pie',
					name: 'Top Campaigns',
					data: topCampaigns	  
				}]
		});
		
		// CAMPAIGNS REDEMPTION 
		// 
		$('#campaign-redemption-chart').highcharts({
					chart: {
						zoomType: 'x',
						height:250
					},
					title: {
						text: ''
					},
					xAxis: {
						type: 'datetime',
						categories:campaignDate
					},
					yAxis: {
						min:0,
						title: {
							text: 'Campaigns Redemption'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px"><small>How many campaigns were redeemed in-store versus in-app?</small><br /><b>{point.x}</b></span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y}</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					legend: {
						enabled: false
					},
					plotOptions: {
						area: {
							fillColor: {
								linearGradient: {
									x1: 0,
									y1: 0,
									x2: 0,
									y2: 1
								},
								stops: [
									[0, Highcharts.getOptions().colors[0]],
									[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
								]
							},
							marker: {
								radius: 2
							},
							lineWidth: 1,
							states: {
								hover: {
									lineWidth: 1
								}
							},
							threshold: null
						}
					},

					series: [{
						type: 'area',
						name: 'in Store',
						data: inStore
					},{
						type: 'area',
						name: 'in App',
						data: inApp
					}]
				});
		
		var Cats = [];
		var catName = [];
		
		
		
		if(data.categories.top5 == '' || data.categories.top5 == null || data.categories.top5 == 'undefined'){
			Cats = [];
			catName = [];
			
		}else{
			
			for(var i = 0; i < data.categories.top5.length; i++ ){
			
				Cats.push(Number(data.categories.top5[i].counts));
				catName.push(data.categories.top5[i].name);
				
				
			}
			
		}
		
		
		if(data.categories.botton5 == '' || data.categories.botton5 == null || data.categories.botton5=='undefined'){
			Cats = [];
			catName = [];
			
		}else{
			
			for(var i = 0; i < data.categories.botton5.length; i++ ){
			
				Cats.push(Number(data.categories.botton5[i].counts));
				catName.push(data.categories.botton5[i].name);
			
			}
			
		}
		
		//  CAMPAIGNS BY CATEGORY
		
		$('#line-chart').highcharts({
			chart: {
				type: 'column',
				height:200
			},
			title: {
				text: 'Top/Bottom 5 Campaigns'
			},
			xAxis: {
				categories: catName,
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: 'campaigns'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">What are the top and bottom 5 performing campaigns <br />by category?</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Campaigns By Category',
				data: Cats

			}]
		});
		
		topCampaigns	=	[];
		campRedeemp		=	[];
		
		Cats 			= [];
		catName 		= [];
		
		inStore 		= [];
		inApp 			= [];
		campaignDate 	= [];
		
		
		
	}
	
	
	$(function () {
		
		actionTypes('Y');
		$(".loading-image").hide();
		
	});

</script>

<style>

.row.bottom-line { background-color: #fff; border: 1px solid #337ab7;  margin-bottom: 20px;
box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);}
.bottomTotatlUser{padding-bottom: 27px;}
.well-well-lg{	margin-left:10px;	margin-right:5px;}
.page-header.text-center {  border-bottom: 1px solid #ccc;}
.bottomTotatlUserDiv{ margin-top: 2px;border-top: 1px solid #ccc; }
.text-center.borderright{ border-right: 1px solid #ccc;}
.chart-heading {
  background-color: #337ab7;
  color: #fff;
  margin: 0 -15px;
  padding: 11px;
}

.loading-image>img{
	
	width:50px !important;
	position:relative;
}

</style>

<script type="text/javascript">

$(document).ready(function() {
	 
	function getCookie(cname) {
		
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
		}
		
		return "";
	
	}
	
	var dt 	= new Date();
	var m 	= dt.getMonth();
	var z 	= m + 1; 
	
	user 	= dt.getFullYear()+"-"+z+"-"+dt.getDate()+" "+dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
	
	document.cookie="username="+user;
	
	var xx= getCookie('username') ;

});
</script>
