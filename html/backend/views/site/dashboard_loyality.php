<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
$usertype = Yii::$app->user->identity->user_type;

if($usertype==1) {
	$this->title = Yii::t('app','Alpha Wallet Admin Portal');
}else{
	$this->title = Yii::t('app','Alpha Wallet Partner Portal');
}
?>

<!-- charts start from here -->

	<div class="dashTopSection">
		<div class="topTabbing">
			<div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
				<?php

					echo Html::a(Yii::t('app','AT A GLANCE'),['site/index'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','USERS'),['site/dashboard-user'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','DEAL/PASSES'),['site/dashboard-deal'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','BEACONS'),['site/dashboard-beacon'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','CAMPAIGN'),['site/dashboard-campaign'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','LOYALTY'),['site/dashboard-loyality'],['class'=>'btn btn-default active']);
					echo Html::a(Yii::t('app','SHOPPING CART/PAYMENT'),['site/dashboard-shoppping'],['class'=>'btn btn-default']);

				?>
			</div>
		</div>
		<div class="row margin">
			<div class="col-md-4"><?= Yii::t('app','DASHBOARD : LOYALTY') ?></div>
			<div class="col-md-5">
				<button type="button" class="btn btn-default" onclick="return actionTypes('D')"><?= Yii::t('app','DAY') ?></button>
				<button type="button" onclick="return actionTypes('W')" class="btn btn-default"><?= Yii::t('app','WEEK') ?></button>
				<button type="button" onclick="return actionTypes('M')" class="btn btn-default"><?= Yii::t('app','MONTH') ?></button>
				<button type="button"  onclick="return actionTypes('Y')" class="btn btn-default"><?= Yii::t('app','YEAR') ?></button>
			</div>
			<div class="col-md-3"><?php

							$end_date 	= new \DateTime(date('m/d/Y'));
							$end_date->modify('+1 day');
							$maxDate 	= $end_date->format('Y-m-d');

							echo DateRangePicker::widget([
											'id'=>'datetime_range',
											'name'=>'datetime_range',
											'readonly'=>'readonly',
											'convertFormat'=>true,
											'options' => ['placeholder' => Yii::t('app','Select date ...'),
															  'class' => 'form-control'
															  ],
											'pluginOptions'=>[
													'timePicker'=>true,
													'timePickerIncrement'=>30,
													'locale'=>[
														'format'=>'Y/m/d h:i:s A'
													],
													'maxDate'=> $maxDate
												]
										]);

						?></div>
		</div>
		<section class="content">
			<div class="row">
				<div class="col-md-6">

					<!-- OFFLINE TRANSACTIONS -->

					<div class="box box-primary">
						<div class="box-header with-border">
						  <i class="fa fa-circle text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','OFFLINE TRANSACTIONS') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div id="offline-transaction-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->

					<!-- TOTAL POINTS ACCRUED -->

					<div class="box box-primary">
						<div class="box-header with-border">
						  <i class="glyphicon glyphicon-object-align-top text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','TOTAL LOYALTY DOLLORS ACCRUED') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div class="point-accrued-chart" id="point-accrued-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->

					<!--  AVERAGE TRANSACTION VALUE  -->

					<div class="box box-primary text-center">
						<div class="box-header with-border text-left">
						  <i class="glyphicon glyphicon-credit-card text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','AVERAGE TRANSACTION VALUE') ?></h4>
						</div>

						<div class="box-body">
						<div class="text-left">

								<?php

									if(isset($avgLoyalTrans) && !empty($avgLoyalTrans)){

										$avg_transc = isset($avgLoyalTrans['avg_transc'])?$avgLoyalTrans['avg_transc']:0;


									}

								?>
								<p><?= Yii::t('app','What is the average transaction value of loyal customers?') ?></p>
								<h3> <?php echo  round($avg_transc,2); ?></h3>
						</div><!-- /.box-body-->
						</div>

					</div><!-- /.box -->

				</div><!-- /.col -->

				<div class="col-md-6">

					<!-- TOTAL POINTS ACCRUED / REDEEMED -->
					<div class="box box-primary">
						<div class="box-header with-border">
						  <i class="glyphicon glyphicon-object-align-top text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','TOTAL LOYALTY DOLLORS ACCRUED / REDEEMED') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div class="point-accrued-chart" id="point-redeem-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->


					<!-- POINTS LIABILITY -->
					<div class="box box-primary text-center">
						<div class="box-header with-border text-left">
						  <i class="fa fa-circle text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','LOYALTY DOLLORS LIABILITY') ?></h4>
						</div>

						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div class="point-liability-chart" id="point-liability-chart"></div>
						</div><!-- /.box-body-->

					</div><!-- /.box -->

					<!-- ACTIVE LOYAL CUSTOMERS -->
					<div class="box box-primary text-center">
						<div class="box-header with-border text-left">
						  <i class="fa fa-users text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','ACTIVE LOYAL CUSTOMERS') ?></h4>
						</div>

						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div class="active-customer-chart" id="active-customer-chart"></div>
						</div><!-- /.box-body-->

					</div><!-- /.box -->

			</div><!-- /.col -->


			</div>

		</section>
	</div>

	<!--  Active Users chart  -->

	<!-- charts end from here -->
<link href="<?= Yii::$app->request->BaseUrl.'/css/'?>app.v1.css" rel="stylesheet">
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>highcharts.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>create-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.easy-pie-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>export.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>app.plugin.js"></script>

<script>

	$('#datetime_range').on('change', function () {

		var daterange_val 	= $(this).val();

		actionTypes('O',daterange_val);

	});

	var offTransData	=	[];
	var offTransDateTime	=	[];

	var maleAccuredData	=	[];
	var femaleAccuredData	=	[];
	var ageRangeData	=	[];

	function actionTypes(type,daterange,linktype){

		daterange 	= daterange || '';
		linktype 	= linktype || 'LO';

		var allData;

		var url = '<?php echo Yii::$app->request->BaseUrl; ?>/site/get-dashboard-report';

		allData = graphGenerator(type,url,daterange,linktype);

		var JSONObject 	= JSON.stringify(allData);
		data 			= jQuery.parseJSON(JSONObject);



		for(var i = 0; i < data.offTransac.length; i++ ){

			offTransData.push(Number(data.offTransac[i].offlineTransac));
			offTransDateTime.push(data.offTransac[i].created_at);


		}

		for(var i = 0; i < data.getPointsAccrued.length; i++ ){

			maleAccuredData.push(Number(data.getPointsAccrued[i].maleAccured));
			femaleAccuredData.push(Number(data.getPointsAccrued[i].femaleAccured));
			ageRangeData.push(data.getPointsAccrued[i].name.replace('Between ',''));


		}

		var maleAccures =[];
		var femaleAccures =[];
		var maleRedeems =[];
		var femaleRedeems =[];
		var ageRanges =[];

		for(var i = 0; i < data.pointsAccruedRedeemed.length; i++ ){

			maleAccures.push(Number(data.pointsAccruedRedeemed[i].maleAccured));
			femaleAccures.push(Number(data.pointsAccruedRedeemed[i].femaleAccured));	maleRedeems.push(Number(data.pointsAccruedRedeemed[i].maleAccured));
			femaleRedeems.push(Number(data.pointsAccruedRedeemed[i].femaleAccured));
			ageRanges.push(data.pointsAccruedRedeemed[i].name.replace('Between ',''));


		}

		// OFFLINE TRANSACTIONS

		$('#offline-transaction-chart').highcharts({
			chart: {
				zoomType: 'x',
				height:200
			},
			title: {
				text: ''
			},
			subtitle: '',
			xAxis: {
				type: 'datetime',
				categories: offTransDateTime
			},
			yAxis: {
				min:0,
				title: {
					text: 'Offline Transaction'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many offline loyalty transactions are there?</small><br />{point.x}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			legend: {
				enabled: false
			},
			plotOptions: {
				area: {
					fillColor: {
						linearGradient: {
							x1: 0,
							y1: 0,
							x2: 0,
							y2: 1
						},
						stops: [
							[0, Highcharts.getOptions().colors[0]],
							[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
						]
					},
					marker: {
						radius: 2
					},
					lineWidth: 1,
					states: {
						hover: {
							lineWidth: 1
						}
					},
					threshold: null
				}
			},

			series: [{
				type: 'area',
				name: 'Transactions',
				data: offTransData
			}]
		});

		// TOTAL POINTS ACCRUED

		$('#point-accrued-chart').highcharts({
			chart: {
				type: 'column',
				height:200
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: ageRangeData,
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: 'POINTS'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many points accrued(by age and gender)?</small><br />Age : {point.x}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Male',
				data: maleAccuredData

			},{
				name: 'Female',
				data: femaleAccuredData

			}]
		});

		// TOTAL POINTS ACCRUED / REDEEMED

		$('#point-redeem-chart').highcharts({
			chart: {
				type: 'column',
				height:200
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: ageRanges,
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: 'POINTS'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many points accrued(by age and gender) and redeemed?</small><br />Age : {point.x}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Male Accrued',
				data: maleAccures

			},{
				name: 'Female Accrued',
				data: femaleAccures

			},{
				name: 'Male Redeemed',
				data: maleRedeems

			},{
				name: 'Female Redeemed',
				data: femaleRedeems

			}]
		});

		//  POINTS LIABILITY

		var pointLiabilities = [];

		for(var i = 0; i < data.totalLiability.length; i++ ){

			if(data.totalLiability[i].pointLiability ==0.00){

				continue;

			}else{

				var liabilityPoint = Number(data.totalLiability[i].pointLiability);
				var storename = data.totalLiability[i].storename?data.totalLiability[i].storename:'Unknown Store';

				pointLiabilities.push([storename,liabilityPoint]);
			}

		}

		$('#point-liability-chart').highcharts({
			chart: {
				height:150
			},
			title: {
				text: ''
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>Total dollar value accrues by users available for redemption</small></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">Points Liability of {point.name}: </td>' +
					'<td style="padding:0"><b>{point.percentage:.1f}%</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			/* tooltip: {
				pointFormat: '<b>{point.name}</b>: {point.percentage:.1f}%'
			}, */
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b>',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					}
				}
			},
			series: [{
					type: 'pie',
					name: 'Points Liability',
					data: pointLiabilities
				}]
		});


		// ACTIVE LOYAL CUSTOMERS

		var activeCustomer = [];

		for(var i = 0; i < data.activeCustomers.length; i++ ){

			var active_cust = Number(data.activeCustomers[i].active_cust);

			activeCustomer.push([data.activeCustomers[i].storename,active_cust]);

		}

		$('#active-customer-chart').highcharts({
			chart: {
				height:150
			},
			title: {
				text: ''
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many active loyal customers are there?</small></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">Active Loyal Customers of {point.name}: </td>' +
					'<td style="padding:0"><b>{point.percentage:.1f}%</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b>',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					}
				}
			},
			series: [{
					type: 'pie',
					name: 'Points Liability',
					data: activeCustomer
				}]
		});



		offTransData	=	[];
		offTransDateTime	=	[];

		maleAccuredData	=	[];
		femaleAccuredData	=	[];
		ageRangeData	=	[];


		maleAccures =[];
		femaleAccures =[];
		maleRedeems =[];
		femaleRedeems =[];
		ageRanges =[];
		pointLiabilities =[];
		activeCustomer =[];

	}


	$(function () {

		actionTypes('Y');
		$(".loading-image").hide();

	});

</script>

<style>

.row.bottom-line { background-color: #fff; border: 1px solid #337ab7;  margin-bottom: 20px;
box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);}
.bottomTotatlUser{padding-bottom: 27px;}
.well-well-lg{	margin-left:10px;	margin-right:5px;}
.page-header.text-center {  border-bottom: 1px solid #ccc;}
.bottomTotatlUserDiv{ margin-top: 2px;border-top: 1px solid #ccc; }
.text-center.borderright{ border-right: 1px solid #ccc;}
.chart-heading {
  background-color: #337ab7;
  color: #fff;
  margin: 0 -15px;
  padding: 11px;
}

.loading-image>img{

	width:50px !important;
	position:relative;
}

</style>

<script type="text/javascript">

$(document).ready(function() {

	function getCookie(cname) {

		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
		}

		return "";

	}

	var dt 	= new Date();
	var m 	= dt.getMonth();
	var z 	= m + 1;

	user 	= dt.getFullYear()+"-"+z+"-"+dt.getDate()+" "+dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

	document.cookie="username="+user;

	var xx= getCookie('username') ;

});
</script>
