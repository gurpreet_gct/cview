<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
$usertype = Yii::$app->user->identity->user_type;

if($usertype==1) {
	$this->title = Yii::t('app','Alpha Wallet Admin Portal');
}else{
	$this->title = Yii::t('app','Alpha Wallet Partner Portal');
}
?>

<!-- charts start from here -->

	<div class="dashTopSection">
		<div class="topTabbing">
			<div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
				<?php 
				
					echo Html::a(Yii::t('app','AT A GLANCE'),['site/index'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','USERS'),['site/dashboard-user'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','DEAL/PASSES'),['site/dashboard-deal'],['class'=>'btn btn-default active']);
					echo Html::a(Yii::t('app','BEACONS'),['site/dashboard-beacon'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','CAMPAIGN'),['site/dashboard-campaign'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','LOYALTY'),['site/dashboard-loyality'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','SHOPPING CART/PAYMENT'),['site/dashboard-shoppping'],['class'=>'btn btn-default']);
				
				?>
			</div>
		</div>
		<div class="row margin">
			
			<div class="col-md-4"><?= Yii::t('app','DASHBOARD : DEALS / PASSES') ?></div>
			<div class="col-md-5">
				<button type="button" class="btn btn-default" onclick="return actionTypes('D')"><?= Yii::t('app','DAY') ?></button>
				<button type="button" onclick="return actionTypes('W')" class="btn btn-default"><?= Yii::t('app','WEEK') ?></button>
				<button type="button" onclick="return actionTypes('M')" class="btn btn-default"><?= Yii::t('app','MONTH') ?></button>
				<button type="button"  onclick="return actionTypes('Y')" class="btn btn-default"><?= Yii::t('app','YEAR') ?></button>
			</div>
			<div class="col-md-3"><?php 
						
							$end_date 	= new \DateTime(date('m/d/Y'));
							$end_date->modify('+1 day');
							$maxDate 	= $end_date->format('Y-m-d');
							
							echo DateRangePicker::widget([
											'id'=>'datetime_range',
											'name'=>'datetime_range',
											'readonly'=>'readonly',
											'convertFormat'=>true,
											'options' => ['placeholder' => Yii::t('app','Select date ...'),
															  'class' => 'form-control'
															  ],
											'pluginOptions'=>[
													'timePicker'=>true,
													'timePickerIncrement'=>30,
													'locale'=>[
														'format'=>'Y/m/d h:i:s A'
													],
													'maxDate'=> $maxDate
												]          
										]);
										
						?></div>
		</div>
		
		<section class="content">
			<div class="row">
				<div class="col-md-6">
				
					<!-- PASSES (SAVED VS. REDEEMED) -->
					
					<div class="box box-primary">
						<div class="box-header with-border">
						  <i class="glyphicon glyphicon-credit-card text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','PASSES (SAVED VS. REDEEMED)') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div id="line-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->

					<!-- MONTHLY QUOTA - PASSES -->
					<div class="box box-primary">
						<div class="box-header with-border text-left">
						  <i class="glyphicon glyphicon-glass text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','MONTHLY QUOTA - PASSES') ?></h4>
						</div>
						<div class="box-body">
						
						<?php 
							if(isset($monthlyQuota) && !empty($monthlyQuota)){
								
								$saved = isset($monthlyQuota[0])?$monthlyQuota[0]:0;
								$available 		= isset($monthlyQuota[1])?$monthlyQuota[1]:0;
								
								
							}
						?>
						
						<div class="text-left">
						  <!-- small box -->
						  
							<p><?= Yii::t('app','How many passes have been saved vs how many still available to be sent?') ?></p>
							<p><b><?= Yii::t('app','Saved Passes') ?> : <?php echo  $saved; ?></b></p>
							<p><b><?= Yii::t('app','Available Passes') ?> : <?php echo  $available; ?></b></p>
							
						</div>
						</div><!-- /.box-body-->
						
					</div><!-- /.box -->
					
					<!-- POP-UPS (BY CATEGORY) -->
					<div class="box box-primary">
						<div class="box-header with-border">
						  <i class="glyphicon glyphicon-credit-card text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','POP-UPS (BY CATEGORY)') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div class="popup-by-category" id="popup-by-category"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->
					
				</div><!-- /.col -->

				<div class="col-md-6">
				
						<!-- PASSES PER CATEGORY -->
						<div class="box box-primary">
							<div class="box-header with-border">
							  <i class="glyphicon glyphicon-credit-card text-primary"></i>
							  <h4 class="box-title"><?= Yii::t('app','PASSES PER CATEGORY') ?></h4>
							</div>
							<div class="box-body">
								<div class="loading-image text-center">
									<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
								</div>
								<div class="pass-per-category" id="pass-per-category"></div>
							</div><!-- /.box-body-->
						</div><!-- /.box -->
						
						<!-- NOTIFICATIONS (SENT VS. OPENED) -->
						<div class="box box-primary">
							<div class="box-header with-border">
								<i class="fa fa-bell-o text-primary"></i>
								<h4 class="box-title"><?= Yii::t('app','NOTIFICATIONS (SENT VS. OPENED)') ?></h4>
							</div>
							<div class="box-body">
								<div class="loading-image text-center">
									<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
								</div>
								<div id="notification-chart"></div>
							</div><!-- /.box-body-->
						</div><!-- /.box -->
				</div><!-- /.col -->
				
			</div>
			
		</section>
	</div>
	
	<!--  Active Users chart  -->

	<!-- charts end from here -->
<link href="<?= Yii::$app->request->BaseUrl.'/css/'?>app.v1.css" rel="stylesheet">	
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>highcharts.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>create-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.easy-pie-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>export.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>app.plugin.js"></script>

<script>

	$('#datetime_range').on('change', function () { 

		var daterange_val 	= $(this).val();
		
		actionTypes('O',daterange_val);
		
	});
	
	var savedPassData	=	[];
	var redeemPassData	=	[];
	var datePassData 	= 	[];
	
	var catSavedPassData	=	[];
	var catRedeemPassData	=	[];
	var catNameData 		= 	[];
	
	function actionTypes(type,daterange,linktype){
		
		daterange 	= daterange || '';
		linktype 	= linktype || 'DE';
		
		var allData;
		
		var url = '<?php echo Yii::$app->request->BaseUrl; ?>/site/get-dashboard-report';
		
		allData = graphGenerator(type,url,daterange,linktype);
		
		var JSONObject 	= JSON.stringify(allData);
		data 			= jQuery.parseJSON(JSONObject);
		
		for(var i = 0; i < data.savedRedeemPass.length; i++ ){
			
			savedPassData.push(Number(data.savedRedeemPass[i].saved));
			redeemPassData.push(Number(data.savedRedeemPass[i].redeemed));
			datePassData.push(data.savedRedeemPass[i].datetime);
			
		}
		
		
		//  PASSES (SAVED VS. REDEEMED)
		
		$('#line-chart').highcharts({
			chart: {
				zoomType: 'x',
				height:200
			},
			title: {
				text: ''
			},
			subtitle: '',
			xAxis: {
				type: 'datetime',
				categories: datePassData
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Passes'
				}
			},
			legend: {
				enabled: false
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many passes have been saved to wallet versus <br />how many passes redeemed?</small><br /><b>{point.x}</b><br /></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				area: {
					fillColor: {
						linearGradient: {
							x1: 0,
							y1: 0,
							x2: 0,
							y2: 1
						},
						stops: [
							[0, Highcharts.getOptions().colors[0]],
							[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
						]
					},
					marker: {
						radius: 2
					},
					lineWidth: 1,
					states: {
						hover: {
							lineWidth: 1
						}
					},
					threshold: null
				}
			},

			series: [{
				type: 'area',
				name: 'Saved',
				data: savedPassData
			},{type: 'area',
				name: 'Redeemed',
				data: redeemPassData
			}]
		});
		
		
		// PASSES PER CATEGORY
		
		for(var i = 0; i < data.passPerCategory.length; i++ ){
			
			catSavedPassData.push(Number(data.passPerCategory[i].saved));
			catRedeemPassData.push(Number(data.passPerCategory[i].redeemed));
			catNameData.push(data.passPerCategory[i].name);
			
		}
		
		// console.log(catSavedPassData);
		
		$('#pass-per-category').highcharts({
			chart: {
				type: 'column',
				height:200
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: catNameData,
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Passes Per Category'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many passes have been saved to wallet versus <br />how many passes redeemed?</small></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Saved',
				data: catSavedPassData

			}, {
				name: 'Redeemed',
				data: catRedeemPassData

			}]
		});
		
		
		// NOTIFICATIONS (SENT VS. OPENED)
		
		var openedData 		= 	[];
		var notOpenedData 	= 	[];
		var notifDateData 	= 	[];
		
		for(var i=0;i<data.notificationData.length;i++){
			openedData.push(Number(data.notificationData[i].opended));
			notOpenedData.push(Number(data.notificationData[i].notOpened));
			notifDateData.push(data.notificationData[i].datetime);
			
		}
		
		
		
		$('#notification-chart').highcharts({
			chart: {
				zoomType: 'x',
				height:200
			},
			title: {
				text: ''
			},
			xAxis: {
				type: 'datetime',
				categories: notifDateData
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Notification'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many notifications were sent versus how many are opened?</small><br /><b>{point.x}</b><br /></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			legend: {
				enabled: false
			},
			plotOptions: {
				area: {
					fillColor: {
						linearGradient: {
							x1: 0,
							y1: 0,
							x2: 0,
							y2: 1
						},
						stops: [
							[0, Highcharts.getOptions().colors[0]],
							[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
						]
					},
					marker: {
						radius: 2
					},
					lineWidth: 1,
					states: {
						hover: {
							lineWidth: 1
						}
					},
					threshold: null
				}
			},

			series: [{
				type: 'area',
				name: 'Opened',
				data: openedData
			},{type: 'area',
				name: 'Sent',
				data: notOpenedData
			}]
		});
		
		// POP-UPS (BY CATEGORY) 
		
		$('#popup-by-category').highcharts({
			chart: {
				zoomType: 'x',
				height:200
			},
			title: {
				text: ''
			},
			subtitle: '',
			xAxis: {
				type: 'datetime',
				categories: ['Fashoin','Book','Accessories','Other']
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Beacon pop-ups'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many beacon pop-ups were bookmarked by category?</small></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: false,
				useHTML: true
			},
			legend: {
				enabled: false
			},
			plotOptions: {
				area: {
					fillColor: {
						linearGradient: {
							x1: 0,
							y1: 0,
							x2: 0,
							y2: 1
						},
						stops: [
							[0, Highcharts.getOptions().colors[0]],
							[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
						]
					},
					marker: {
						radius: 2
					},
					lineWidth: 1,
					states: {
						hover: {
							lineWidth: 1
						}
					},
					threshold: null
				}
			},
			series: [{
				type: 'area',
				name: 'Pop-Ups',
				data: [10,50,100,40]
			}]
		});
		
		savedPassData	=	[];
		redeemPassData	=	[];
		datePassData 	= 	[];
		
		catSavedPassData	=	[];
		catRedeemPassData	=	[];
		catNameData 		= 	[];
		
	}
	
	
	$(function () {
		
		actionTypes('Y');
		$(".loading-image").hide();
		
	});

</script>

<style>

.row.bottom-line { background-color: #fff; border: 1px solid #337ab7;  margin-bottom: 20px;
box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);}
.bottomTotatlUser{padding-bottom: 27px;}
.well-well-lg{	margin-left:10px;	margin-right:5px;}
.page-header.text-center {  border-bottom: 1px solid #ccc;}
.bottomTotatlUserDiv{ margin-top: 2px;border-top: 1px solid #ccc; }
.text-center.borderright{ border-right: 1px solid #ccc;}
.chart-heading {
  background-color: #337ab7;
  color: #fff;
  margin: 0 -15px;
  padding: 11px;
}

.loading-image>img{
	
	width:50px !important;
	position:relative;
}

</style>

<script type="text/javascript">

$(document).ready(function() {
	 
	function getCookie(cname) {
		
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
		}
		
		return "";
	
	}
	
	var dt 	= new Date();
	var m 	= dt.getMonth();
	var z 	= m + 1; 
	
	user 	= dt.getFullYear()+"-"+z+"-"+dt.getDate()+" "+dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
	
	document.cookie="username="+user;
	
	var xx= getCookie('username') ;

});
</script>
