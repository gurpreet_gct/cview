<?php
/* @var $this yii\web\View */

$usertype = Yii::$app->user->identity->user_type;

if($usertype==1) {
	$this->title = Yii::t('app','Alpha Wallet Admin Portal');
}else{
	$this->title = Yii::t('app','Alpha Wallet Partner Portal');
}
?>
<!---       charts start from here         --------------------->
<section class="well-well-lg">
<!--   Active Users chart       -->
<div class="row bottom-line">
<div class="col-md-12 col-lg-12">
	<div class="chart-heading">    
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Active Users(shows number of active users per day, per week, per month)') ?>
                    </h3>
   </div>
	<div  id="ActiveUserChart"> </div>
	<div class="bottomTotatlUserDiv">
		<div class="col-lg-3 col-md-3"><div class="text-center borderright"><h2><?= $activeUser['perDay'] ?></h2><?= Yii::t('app','Per Day') ?></div></div>
		<div class="col-lg-3 col-md-3"><div class="text-center borderright"><h2><?= $activeUser['perWeek'] ?></h2><?= Yii::t('app','Per Week') ?></div></div>
		<div class="col-lg-3 col-md-3"><div class="text-center borderright"><h2><?= $activeUser['perMonth'] ?></h2><?= Yii::t('app','Per Month') ?></div></div>
		<div class="col-lg-3 col-md-3 bottomTotatlUser"><div class="text-center"><h2><?= $activeUser['totalUser'] ?></h2><?= Yii::t('app','Total') ?></div></div>	
		<input type="hidden" value="<?php print_r(implode('-',$activeUser)); ?>" id="activeUserVal"/>		
	</div>
</div>
</div>
<!--   New Users chart       -->
<div class="row bottom-line">
<div class="col-md-12 col-lg-12">
<div class="chart-heading">    
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','New Users(shows number of new users per day, per week, per month)') ?>
                    </h3>
   </div>
<div id="newUserChart"> </div>
<div class="bottomTotatlUserDiv">
		<div class="col-lg-3 col-md-3 "><div class="text-center borderright"><h2><?= $UserDatas['perDay']?></h2><?= Yii::t('app','Per Day') ?></div></div>
		<div class="col-lg-3 col-md-3 "><div class="text-center borderright"><h2><?= $UserDatas['perWeek']?></h2><?= Yii::t('app','Per Week') ?></div></div>
		<div class="col-lg-3 col-md-3 "><div class="text-center borderright"><h2><?= $UserDatas['perMonth']?></h2><?= Yii::t('app','Per Month') ?></div></div>
		<div class="col-lg-3 col-md-3 bottomTotatlUser"><div class="text-center"><h2><?= $UserDatas['totalUser']?></h2><?= Yii::t('app','Total') ?></div></div>
			<input type="hidden" value="<?php print_r(implode('-',$UserDatas));?>" id="newUserDatVal" />
</div>
</div>
</div>
<!--   Sessions chart       -->
<div class="row bottom-line">
	<div class="col-md-12 col-lg-12">
		<div class="chart-heading">    
			<!-- Header Title-->
			<h3 class="panel-title">
				<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Sessions(shows number of times users have opened the app and browsed)') ?>
			</h3>
	   </div>
		<div  id="sessionChart"> </div>
		<input type="hidden" value="<?php print_r(implode('-',$userSession));?>" id="userSession" />
	</div>
</div>
<!--   Sessions By Area chart       -->
<div class="row bottom-line">
	<div class="col-md-12 col-lg-12">		
		<div class="chart-heading">    
			<!-- Header Title-->
			<h3 class="panel-title">
				<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Sessions By Area(shows where users are browsing the app)') ?>
			</h3>
		</div>
		<div  id="sessionByAreaChart"> </div>
	</div>
</div>
<div class="row bottom-line">
    <div class="col-md-12">              
		<div class="chart-heading">    
			<!-- Header Title-->
			<h3 class="panel-title">
				<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii:t('app','Screen Views(shows the variety of different customers using your app)') ?>
			</h3>
		</div>
       <div id="flot-pie-donut" style="height:286px"></div>   
	<input type="hidden" value="<?php print_r(implode('-',$actionScreenview));?>" id="screenView" />	   
    </div>
</div>	
<div class="row bottom-line">
    <div class="col-md-12 col-lg-12">
       	<div class="chart-heading">    
			<!-- Header Title-->
			<h3 class="panel-title">
				<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Screen Views By Screen Name(shows which parts of the app users are looking at the most)') ?>
			</h3>
		</div>		
        <div id="cc5" style="height:285px;" ></div>
    </div>
</div>
<div class="row bottom-line">
 <div class="col-md-12">	
	  <div class="chart-heading">    
			<!-- Header Title-->
			<h3 class="panel-title">
				<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Total Campaigns (shows number of campaigns running currently)') ?>
			</h3>
		</div>	
	  <div class="panel-body text-center">	  
		<h4><strong><?php echo $totalCampaigns[1]; ?></strong></h4>
		<!--<small class="text-muted block">Number of campaigns running currently</small>-->
		<div class="inline">
		<?php $percentage = 0; ?>
		<?php 
		if($totalCampaigns[1])
		$percentage =  ($totalCampaigns[0]/$totalCampaigns[1])*100; ?>
		  <div class="easypiechart" data-percent="<?php echo $percentage; ?>" data-line-width="10" data-track-color="#eee" data-bar-color="#1ccc88" data-scale-color="#fff" data-size="188" data-line-cap='butt'>
			<div> <span class="h2 m-l-sm step"><?php echo intval($percentage); ?></span>%
			  <div class="text text-sm"><?php echo  $totalCampaigns[0]; ?><?= Yii::t('app','Campaigns') ?></div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="panel-footer" style="text-align:center;"><small><?= Yii::t('app','Number of campaigns running currently') ?></small></div>
  </div>
 </div>
 <div class="row bottom-line">
  <div class="col-md-12 col-lg-12" style="width:;">		 
		  <div class="chart-heading">    
			<!-- Header Title-->
			<h3 class="panel-title">
				<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Top Campaigns (shows the most popular campaigns on the app)') ?>
			</h3>
		</div>	
	  <div class="panel-body text-center">
	   <div id="cc6" style="height:340px;" ></div>
	   </div>
  </div>
</div>
<div class="row bottom-line">
	<div class="col-md-12">
		
		  <div class="chart-heading">    
			<!-- Header Title-->
			<h3 class="panel-title">
				<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Passes Sent(shows number of passes sent per day)') ?>
			</h3>
		</div>	
		  <div id="c1_pass" style="height:240px;" ></div>
		  <input type="hidden" value="<?php print_r(implode('-',$actionPassessent));?>" id="actionPassessent" />
	</div>
</div>
<div class="row bottom-line">
	<div class="col-md-12">
		  <div class="chart-heading">    
			<!-- Header Title-->
			<h3 class="panel-title">
				<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Passes Redeemed (shows number of passes redeemed)') ?>
			</h3>
		</div>
		   <div id="cc1_pass1" style="height:293px;"></div>
	</div>
</div>
<!-- END --NEW CODE FOR PASSES SENT GRAPH -->
             
             <div class="row bottom-line">
              <div class="col-md-12">  
				<div class="chart-heading">    
					<!-- Header Title-->
					<h3 class="panel-title">
					<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Total Number of Beacons Online Now') ?>
					</h3>
				</div>
                  <div class="panel-body text-center" style="height:368px" >
                    <h4><?= $beaconsOnline[0] ?></h4>
                    <small class="text-muted block"><?= Yii::t('app','Number of beacons online now out of' ) ?> <?= $beaconsOnline[0] ?></small>
                    <div class="inline">
                      <div class="easypiechart" data-percent="<?= $beaconsOnline[0]?($beaconsOnline[1]/$beaconsOnline[0])*100:0; ?>" data-bar-color="#fcc633" data-line-width="16" data-loop="false" data-size="188">
                        <div> <span class="h2 m-l-sm step"><?= $beaconsOnline[1] ?></span>
                          <div class="text text-sm"><?= Yii::t('app','Online Beacons') ?></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel-footer" style="text-align:center;"><small><?= Yii::t('app','Number of beacons online now') ?></small></div>
                </div>
              </div>
         <div class="row bottom-line">
              <div class="col-md-12">
             	<div class="chart-heading">    
					<!-- Header Title-->
					<h3 class="panel-title">
					<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Active clients (Number of active clients - retailers, brands etc)') ?>
					</h3>
				</div>
				<?php print_r(implode('-',$actionActiveclients));?>
              <div class="panel-body">
                <div id="cc9" style="height:280px"></div>
              </div>
              <footer class="panel-footer bg-white">
                <div class="row text-center no-gutter">
                  <div class="col-xs-4 b-r b-light">
                    <p class="h3 font-bold m-t"><?= Yii::t('app','5,860') ?></p>
                    <p class="text-muted"><?= Yii::t('app','Retailers') ?></p>
                  </div>
                  <div class="col-xs-4 b-r b-light">
                    <p class="h3 font-bold m-t"><?= Yii::t('app','10,450') ?></p>
                    <p class="text-muted"><?= Yii::t('app','Brands') ?></p>
                  </div>
                  <div class="col-xs-4 b-light">
                    <p class="h3 font-bold m-t"><?= Yii::t('app','21,230') ?></p>
                    <p class="text-muted"><?= Yii::t('app','Others') ?></p>
                  </div>
                  
                </div>
              </footer>
             </div>            
   </div>
   <!--  Devices chart       -->
<div class="row bottom-line">
<div class="col-md-12 col-lg-12">
		<div class="chart-heading">    
					<!-- Header Title-->
					<h3 class="panel-title">
					<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Devices (devices being used: iPhone, iPad, Android, Blackberry, Windows)') ?>
					</h3>
		</div>
	<div  id="UsingdeviceChart"> </div>
	<input type="hidden" value="<?php print_r(implode('-',$userDevices)); ?>" id="userDevices"/> 
</div>
</div>
<!--  Devices chart       -->
<div class="row bottom-line">
<div class="col-md-12 col-lg-12">
		<div class="chart-heading">    
			<!-- Header Title-->
			<h3 class="panel-title">
			<i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?= Yii::t('app','Membership Segmentation (shows membership segmentation according to age, gender, postcode)') ?>
			</h3>
		</div>
	<div id="membershipChart"> </div>
	<input type="hidden" value="<?php print_r(implode('-',$membershipUsers)); ?>" id="membershipVal"/> 
</div>
</div>
</section>
		<!-------    charts end from here           -->
<script type="text/javascript">
$(document).ready(function() {
setTimeout(function(){
//$.getScript("<?= Yii::$app->request->BaseUrl.'/js/'?>app.v1.js");
}, 2000); 
   });

 </script>
 <link href="<?= Yii::$app->request->BaseUrl.'/css/'?>app.v1.css" rel="stylesheet">
  <!--script src="<?= Yii::$app->request->BaseUrl.'/js/'?>app.v1.js"></script-->
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>highcharts.js"></script>
<!-- <script src="<?= Yii::$app->request->BaseUrl.'/js/'?>highcharts-3d1.js"></script>  -->
 <script src="<?= Yii::$app->request->BaseUrl.'/js/'?>highcharts-more.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>funnel.js"></script>
  <script src="<?= Yii::$app->request->BaseUrl.'/js/'?>d3_charts.js"></script>  
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.easy-pie-chart.js"></script>
<!--script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.flot.grow.js"></script-->
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.flot.min.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.flot.pie.min.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.flot.resize.js"></script>
<!-- <script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.flot.spline.js"></script> -->
<!-- <script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.flot.tooltip.min.js"></script> -->
<!-- <script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.sparkline.min.js"></script> -->
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>app.plugin.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>flot-demo.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>chart.js"></script>
 <style>
.row.bottom-line { background-color: #fff; border: 1px solid #337ab7;  margin-bottom: 20px;
box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);}
.bottomTotatlUser{padding-bottom: 27px;}
.well-well-lg{	margin-left:10px;	margin-right:5px;}
.page-header.text-center {  border-bottom: 1px solid #ccc;}
.bottomTotatlUserDiv{ margin-top: 2px;border-top: 1px solid #ccc; }
.text-center.borderright{ border-right: 1px solid #ccc;}
.chart-heading {
  background-color: #337ab7;
  color: #fff;
  margin: 0 -15px;
  padding: 11px;
}
 </style>
 <script type="text/javascript">
 $( document ).ready(function() {
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}
var dt = new Date();
var m = dt.getMonth();
var z = m + 1; 
user = dt.getFullYear()+"-"+z+"-"+dt.getDate()+" "+dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
document.cookie="username="+user;
var xx= getCookie('username') ;


});
</script>
