<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
$usertype = Yii::$app->user->identity->user_type;

if($usertype==1) {
	$this->title = Yii::t('app','Alpha Wallet Admin Portal');
}else{
	$this->title = Yii::t('app','Alpha Wallet Partner Portal');
}
?>

<!-- charts start from here -->

	<div class="dashTopSection">
		<div class="topTabbing">
			<div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
				<?php 
				
					echo Html::a(Yii::t('app','AT A GLANCE'),['site/index'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','USERS'),['site/dashboard-user'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','DEAL/PASSES'),['site/dashboard-deal'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','BEACONS'),['site/dashboard-beacon'],['class'=>'btn btn-default active']);
					echo Html::a(Yii::t('app','CAMPAIGN'),['site/dashboard-campaign'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','LOYALTY'),['site/dashboard-loyality'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','SHOPPING CART/PAYMENT'),['site/dashboard-shoppping'],['class'=>'btn btn-default']);
				
				?>
			</div>
		</div>
		
		<div class="row margin">
			
			<div class="col-md-4"><?= Yii::t('app','DASHBOARD : BEACONS') ?></div>
			<div class="col-md-5">
				<button type="button" class="btn btn-default" onclick="return actionTypes('D')"><?= Yii::t('app','DAY') ?></button>
				<button type="button" onclick="return actionTypes('W')" class="btn btn-default"><?= Yii::t('app','WEEK') ?></button>
				<button type="button" onclick="return actionTypes('M')" class="btn btn-default"><?= Yii::t('app','MONTH') ?></button>
				<button type="button"  onclick="return actionTypes('Y')" class="btn btn-default"><?= Yii::t('app','YEAR') ?></button>
			</div>
			
			<div class="col-md-3"><?php 
				
					$end_date 	= new \DateTime(date('m/d/Y'));
					$end_date->modify('+1 day');
					$maxDate 	= $end_date->format('Y-m-d');
					
					echo DateRangePicker::widget([
									'id'=>'datetime_range',
									'name'=>'datetime_range',
									'readonly'=>'readonly',
									'convertFormat'=>true,
									'options' => ['placeholder' => Yii::t('app','Select date ...'),
													  'class' => 'form-control'
													  ],
									'pluginOptions'=>[
											'timePicker'=>true,
											'timePickerIncrement'=>30,
											'locale'=>[
												'format'=>'Y/m/d h:i:s A'
											],
											'maxDate'=> $maxDate
										]          
								]);
								
				?></div>
		</div>
		<section class="content">
			<div class="row">
				<div class="col-md-6">
				
					<!-- BEACONS ONLINE -->
					<div class="box box-primary">
						<div class="box-header with-border text-left">
						  <i class="fa fa-star-o text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','BEACONS ONLINE') ?></h4>
						</div>
						<div class="box-body">
						
							<div class="text-left">
								
							  <!-- small box -->
							<p><?= Yii::t('app','How many beacons are online?') ?></p>
							<?php 
								
								if(isset($onlineBeacons[1]) && !empty($onlineBeacons[1])){
									
									$onlineBeacons = isset($onlineBeacons[1])?$onlineBeacons[1]:0;
									
									
								}else{
									
									$onlineBeacons = 0;
								}
								
							?>
							<h3><?php echo $onlineBeacons; ?></h3>
							  
							</div>
						</div><!-- /.box-body-->
						
					</div><!-- /.box -->
					
					<!-- Area chart -->
					
					<div class="box box-primary">
						<div class="box-header with-border">
						  <i class="glyphicon glyphicon-credit-card text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','PASS RATE') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div class="pass-per-category" id="pass-rate-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->
					
					
				</div><!-- /.col -->

				<div class="col-md-6">
				
					<!-- HIT RATE (BY REGION / TAG) -->
					<div class="box box-primary">
						<div class="box-header with-border text-left">
						  <i class="fa fa-star-o text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','HIT RATE (BY REGION / TAG)')?></h4>
						</div>
						<div class="box-body">
						
						<div class="text-left">
							
						  <!-- small box -->
							<div class="btn-group text-center">
							  <button type="button" class="btn"><?= Yii::t('app','-- choose regions  OR tags --') ?> </button>
							  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
								<span class="sr-only"><?= Yii::t('app','Toggle Dropdown') ?></span>
							  </button>
							  <ul class="dropdown-menu" role="menu">
								<li><a href="#"><?= Yii::t('app','Australia') ?></a></li>
								<li><a href="#"><?= Yii::t('app','Canada') ?></a></li>
								<li><a href="#"><?= Yii::t('app','India') ?></a></li>
							  </ul>
							</div>
							<h3><?php echo  0; ?></h3>
							<p><?= Yii::t('app','Hit Rate (By Region / Tag)') ?></p>
								
						</div>
						
						</div><!-- /.box-body-->
						
					</div><!-- /.box -->
					
			</div><!-- /.col -->
				
				
			</div>
			
		</section>
	</div>
	
	<!--  Active Users chart  -->

	<!-- charts end from here -->
<link href="<?= Yii::$app->request->BaseUrl.'/css/'?>app.v1.css" rel="stylesheet">	
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>highcharts.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>create-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.easy-pie-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>export.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>app.plugin.js"></script>

<script>

	$('#datetime_range').on('change', function () { 

		var daterange_val 	= $(this).val();
		
		actionTypes('O',daterange_val);
		
	});
	
	var onlineBeaconData	=	[];
	
	
	function actionTypes(type,daterange,linktype){
		
		daterange 	= daterange || '';
		linktype 	= linktype || 'BE';
		
		var allData;
		
		var url = '<?php echo Yii::$app->request->BaseUrl; ?>/site/get-dashboard-report';
		
		allData = graphGenerator(type,url,daterange,linktype);
		
		var JSONObject 	= JSON.stringify(allData);
		data 			= jQuery.parseJSON(JSONObject);
			
		
		// console.log(onlineBeaconData);
				
		//  PASS RATE
		
		$('#pass-rate-chart').highcharts({
					chart: {
						zoomType: 'x',
						height:200
					},
					title: {
						text: ''
					},
					xAxis: {
						type: 'datetime'
					},
					yAxis: {
						title: {
							text: 'PASS RATE'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px"><small>How many people pass the store versus walk in the store?</small></span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y}</b></td></tr>',
						footerFormat: '</table>',
						shared: false,
						useHTML: true
					},
					legend: {
						enabled: false
					},
					plotOptions: {
						area: {
							fillColor: {
								linearGradient: {
									x1: 0,
									y1: 0,
									x2: 0,
									y2: 1
								},
								stops: [
									[0, Highcharts.getOptions().colors[0]],
									[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
								]
							},
							marker: {
								radius: 2
							},
							lineWidth: 1,
							states: {
								hover: {
									lineWidth: 1
								}
							},
							threshold: null
						}
					},

					series: [{
						type: 'area',
						name: 'PASS RATE',
						data: [
								[Date.UTC(2016,5,2),0.7695],
								[Date.UTC(2016,5,3),0.7648],
								[Date.UTC(2016,5,4),0.7645],
								[Date.UTC(2016,5,5),0.7638],
								[Date.UTC(2016,5,6),0.7549],
								[Date.UTC(2016,5,7),0.7562],
								[Date.UTC(2016,5,9),0.7574],
								[Date.UTC(2016,5,10),0.7543],
								[Date.UTC(2016,5,11),0.7510],
								[Date.UTC(2016,5,12),0.7498],
								[Date.UTC(2016,5,13),0.7477]
							]
						}]
				});
		
		
		//  HIT RATE (BY REGION / TAG)
			
		onlineBeaconData =	[];
		redeemPassData	=	[];
		datePassData 	= 	[];
		
	}
	
	
	$(function () {
		
		actionTypes('Y');
		$(".loading-image").hide();
		
	});
	

</script>

<style>

.row.bottom-line { background-color: #fff; border: 1px solid #337ab7;  margin-bottom: 20px;
box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);}
.bottomTotatlUser{padding-bottom: 27px;}
.well-well-lg{	margin-left:10px;	margin-right:5px;}
.page-header.text-center {  border-bottom: 1px solid #ccc;}
.bottomTotatlUserDiv{ margin-top: 2px;border-top: 1px solid #ccc; }
.text-center.borderright{ border-right: 1px solid #ccc;}
.chart-heading {
  background-color: #337ab7;
  color: #fff;
  margin: 0 -15px;
  padding: 11px;
}

.loading-image>img{
	
	width:50px !important;
	position:relative;
}

</style>

<script type="text/javascript">

$(document).ready(function() {
	 
	function getCookie(cname) {
		
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
		}
		
		return "";
	
	}
	
	var dt 	= new Date();
	var m 	= dt.getMonth();
	var z 	= m + 1; 
	
	user 	= dt.getFullYear()+"-"+z+"-"+dt.getDate()+" "+dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
	
	document.cookie="username="+user;
	
	var xx= getCookie('username') ;

});
</script>
