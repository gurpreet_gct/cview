<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
$usertype = Yii::$app->user->identity->user_type;

if($usertype==1) {
	$this->title = Yii::t('app','Alpha Wallet Admin Portal');
}else{
	$this->title = Yii::t('app','Alpha Wallet Partner Portal');
}
?>

<!-- charts start from here -->

	<div class="dashTopSection">
		<div class="topTabbing">
			<div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
				<?php 
				
					echo Html::a(Yii::t('app','AT A GLANCE'),['site/index'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','USERS'),['site/dashboard-user'],['class'=>'btn btn-default active']);
					echo Html::a(Yii::t('app','DEAL/PASSES'),['site/dashboard-deal'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','BEACONS'),['site/dashboard-beacon'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','CAMPAIGN'),['site/dashboard-campaign'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','LOYALTY'),['site/dashboard-loyality'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','SHOPPING CART/PAYMENT'),['site/dashboard-shoppping'],['class'=>'btn btn-default']);
				
				?>
			</div>
		</div>
		<div class="row margin">
			
			<div class="col-md-4"><?= Yii::t('app','DASHBOARD : USERS') ?></div>
			<div class="col-md-5">
				<button type="button" class="btn btn-default" onclick="return actionTypes('D')"><?= Yii::t('app','DAY') ?></button>
				<button type="button" onclick="return actionTypes('W')" class="btn btn-default"><?= Yii::t('app','WEEK') ?></button>
				<button type="button" onclick="return actionTypes('M')" class="btn btn-default"><?= Yii::t('app','MONTH') ?></button>
				<button type="button"  onclick="return actionTypes('Y')" class="btn btn-default"><?= Yii::t('app','YEAR') ?></button>
			</div>
			<div class="col-md-3"><?php 
						
							$end_date 	= new \DateTime(date('m/d/Y'));
							$end_date->modify('+1 day');
							$maxDate 	= $end_date->format('Y-m-d');
							
							echo DateRangePicker::widget([
											'id'=>'datetime_range',
											'name'=>'datetime_range',
											'readonly'=>'readonly',
											'convertFormat'=>true,
											'options' => ['placeholder' => Yii::t('app','Select date ...'),
															  'class' => 'form-control'
															  ],
											'pluginOptions'=>[
													'timePicker'=>true,
													'timePickerIncrement'=>30,
													'locale'=>[
														'format'=>'Y/m/d h:i:s A'
													],
													'maxDate'=> $maxDate
												]          
										]);
										
						?></div>
		</div>
		
		<section class="content">
			<div class="row">
				<div class="col-md-6">
				
					<!-- ACTIVE USERS -->
					
					<div class="box box-primary">
						<div class="box-header with-border">
						  <i class="fa fa-users text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','ACTIVE USERS(BY GENDER/AGE)') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div id="line-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->

					<!-- DEVICES USED -->
					<div class="box box-primary">
						<div class="box-header with-border">
						  <i class="glyphicon glyphicon-credit-card text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','DEVICES USED') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div class="" id="deviceused-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->
					
					<!-- CATEGORIES (TOP / BOTTOM 3) -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<i class="fa fa-users text-primary"></i>
							<h4 class="box-title"><?= Yii::t('app','CATEGORIES (TOP / BOTTOM 3)') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
							<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
						</div>
							<div id="top-bottom-cat-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->
					
					<!-- ACTIVE USERS ON THE APP -->
					
					<div class="box box-primary">
						<div class="box-header with-border text-left">
						  <i class="fa fa-users text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','RIGHT NOW ACTIVE USERS ON APP') ?></h4>
						</div>
						<div class="box-body">
						<div class="text-left">
						  <!-- small box -->
							<?php $activesession = isset($activeappuser['activeUser'])?$activeappuser['activeUser']:0; ?>
							  <h3><?php echo intval($activesession); ?></h3>
							  <p><?= Yii::t('app','Right now active users on the APP') ?></p>
							
						</div>
						
						</div><!-- /.box-body-->
						
					</div><!-- /.box -->
					
				</div><!-- /.col -->

				<div class="col-md-6">
				
					  <!-- NEW USERS -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<i class="fa fa-users text-primary"></i>
							<h4 class="box-title"><?= Yii::t('app','NEW USERS(BY GENDER/AGE)') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
							<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
						</div>
							<div id="newusers-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->

					<!-- USERS BY REGION -->
					<div class="box box-primary">
						<div class="box-header with-border">
						  <i class="fa fa-users text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','USERS BY REGION') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div class="" id="user-region-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->
					  
					<!-- SESSIONS -->
					<div class="box box-primary">
						<div class="box-header with-border">
						  <i class="fa fa-users text-primary"></i>
						  <h4 class="box-title"><?= Yii::t('app','SESSIONS') ?></h4>
						</div>
						<div class="box-body">
							<div class="loading-image text-center">
								<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
							</div>
							<div id="user-session-chart"></div>
						</div><!-- /.box-body-->
					</div><!-- /.box -->
				</div><!-- /.col -->	
	<!-- User referal -->
		<div class="col-md-6">
			<div class="box box-primary">
			<div class="box-header with-border">
			<i class="glyphicon glyphicon-credit-card text-primary"></i>
			<h4 class="box-title"><?= Yii::t('app','Referral Earnings') ?></h4>
			<div class="row">
					<?php 
					$url = Yii::$app->request->BaseUrl.'/user/userlist';
					if($usertype==1 || $usertype==7) { ?>
					<div class="col-lg-12 col-xs-12">
						<?php
						$url2 = Yii::$app->request->BaseUrl.'/referral-earning/user-earning';
						echo '<label class="control-label">User</label>';
						echo Select2::widget([
						'name' => 'referral-user',
						'id' => 'referral-user',
						'initValueText' => '', // set the initial display text
						'options' => ['placeholder' => 'Search Username ...'],
						'pluginEvents'=> [
						"change" => "function() {
								var userId =	$('#referral-user').val();
								$.ajax({
									type:'post',
									url:'$url2',
									data:{'userId':userId}
								})
								.done(function(result){
									if(result){										$('.referral-all-usr').text(result);						
									}
								});
						}",
						],
						'pluginOptions' => [
						'allowClear' => true,
						'minimumInputLength' => 3,
						'language' => [
						'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
						],
						'ajax' => [
							'url' => $url,
							'dataType' => 'json',
							'data' => new JsExpression('function(params) { return {q:params.term}; }')
						],
						'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
						'templateResult' => new JsExpression('function(city) { return city.text; }'),
						'templateSelection' => new JsExpression('function (city) { return city.text; }'),
						],
						]);

						?>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="box-body">
				<div class="loading-image text-center">
				<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
				</div>
				<div class="col-sm-12 col-md-12 col-lg-12">
					<div class="info-box">
						<span class="info-box-icon bg-orange"><i class="ion ion-social-usd"></i></span>
						<div class="info-box-content">
						<span class="info-box-text">Total Referral Earnings</span>
						<span class="info-box-number referral-all-usr">0</span>
						</div><!-- /.info-box-content -->
					</div><!-- /.info-box -->
				</div>		
				
			</div><!-- /.box-body-->
			</div><!-- /.box -->
		</div>
				
				
			</div>
		</section>
	</div>
	
	<!--  Active Users chart  -->

	<!-- charts end from here -->
<link href="<?= Yii::$app->request->BaseUrl.'/css/'?>app.v1.css" rel="stylesheet">	
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>highcharts.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>create-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.easy-pie-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>export.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>app.plugin.js"></script>

<script>

	$('#datetime_range').on('change', function () { 

		var daterange_val 	= $(this).val();
		
		actionTypes('O',daterange_val);
		
	});
	
	var femaleData	=	[];
	var maleData	=	[];
	var ageranges 	= 	[];
	var deviceUsed 	= 	[];
	
	var	newMaleData		=	[];
	var	newFemaleData	=	[];
	var	newAgerange		=	[];
	
	var	userSession	=	[];
	var	sessionDate		=	[];
	
	function actionTypes(type,daterange,linktype){
		
		daterange 	= daterange || '';
		linktype 	= linktype || 'US';
		
		var allData;
		
		var url = '<?php echo Yii::$app->request->BaseUrl; ?>/site/get-dashboard-report';
		
		allData = graphGenerator(type,url,daterange,linktype);
		
		var JSONObject 	= JSON.stringify(allData);
		data 			= jQuery.parseJSON(JSONObject);
		// referral user data push
		$('.referral-all-usr').text(data.referralCount);
		
		for(var i = 0; i < data.activeUser.length; i++ ){
			
			maleData.push(data.activeUser[i].maleActiveUser);
			femaleData.push(data.activeUser[i].femaleActiveUser);
			ageranges.push(data.activeUser[i].name.replace('Between ', ''));
			
		}
		
		for(var i = 0; i < data.newUsers.length; i++ ){
		
			newMaleData.push(Number(data.newUsers[i].maleData));
			newFemaleData.push(Number(data.newUsers[i].femaleData));
			newAgerange.push(data.newUsers[i].agerange.replace('Between ', ''));
			
		}
		
		
		var sumTotal = 0;
		
		for(var i = 0; i < data.devicesUsed.length; i++ ){
			
			sumTotal +=Number(data.devicesUsed[i].total);
			
		}
		
		for(var i = 0; i < data.devicesUsed.length; i++ ){
			
			var perDeviceUsed = (Number(data.devicesUsed[i].total) / sumTotal) * 100;
			
			deviceUsed.push([data.devicesUsed[i].device,perDeviceUsed]);
			
		}
		
		for(var i = 0; i < data.userSessions.length; i++ ){
			
			userSession.push(Number(data.userSessions[i].totalsession));
			sessionDate.push(data.userSessions[i].loginat);
			
		}
		
		// ACTIVE USERS
		
		$('#line-chart').highcharts({
			chart: {
				type: 'column',
				height: 250
			},
			title: {
				text: 'Active user in last 1 hour'
			},
			xAxis: {
				categories: ageranges,
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Genders (Male/Female)'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many active users(user that has opened up the app) – <br /> represented by gender and age?</small><br />Age : {point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Male',
				data: maleData

			}, {
				name: 'Female',
				data: femaleData

			}]
		});
		
		// NEW USERS
		$('#newusers-chart').highcharts({
			chart: {
				type: 'area',
				height: 270
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: newAgerange,
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Users (Male/Female)'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many new users – represented by gender and age?</small><br />Age : {point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: false,
				useHTML: true
			},
			plotOptions: {
				area: {
					marker: {
						enabled: false,
						symbol: 'circle',
						radius: 2,
						states: {
							hover: {
								enabled: true
							}
						}
					}
				}
			},
			series: [
					{
						name: 'Male',
						data: newMaleData
					},{
						name: 'Female',
						data: newFemaleData
					}
					]
		});
		
		// 	DEVICES USED 
		$('#deviceused-chart').highcharts({
			chart: {
				height: 200
			},
			title: {
				text: ''
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>Which devices are being used?</small></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{point.name}: </td>' +
					'<td style="padding:0"><b>{point.percentage:.1f} %</b></td></tr>',
				footerFormat: '</table>',
				shared: false,
				useHTML: true
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b>',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					}
				}
			},
			series: [{
					type: 'pie',
					name: 'Device Used',
					data: deviceUsed	  
				}]
				
		});
		
		
		// 	USERS BY REGION
		
		$('#user-region-chart').highcharts({
			chart: {
				type: 'column',
				height: 200
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: ['Australia','Canada','Indonesia','india',],
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Users (Male/Female)'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many users by region?</small></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Male',
				data: [50,550,250,450,]

			}, {
				name: 'Female',
				data: [150,350,250,550,]

			}]
		});
		
		
		// SESSIONS
		$('#user-session-chart').highcharts({
			chart: {
				type: 'area',
				height: 250
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: sessionDate,
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Users Sessions'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many times users have opened the app and browsed?</small></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: false,
				useHTML: true
			},
			plotOptions: {
				area: {
					marker: {
						enabled: false,
						symbol: 'circle',
						radius: 2,
						states: {
							hover: {
								enabled: true
							}
						}
					}
				}
			},
			series: [
					{
						name: 'User Sessions',
						data: userSession
					}]
		});
		
		
		// CATEGORIES (TOP / BOTTOM 3)
		
		var topCatData 		= [];
		var catNameData 	= [];
		
		for(var i = 0; i < data.topCategory.length; i++ ){
			
				topCatData.push(Number(data.topCategory[i].counts));
				catNameData.push(data.topCategory[i].name);
			
		}
		
		$('#top-bottom-cat-chart').highcharts({
			chart: {
				type: 'column',
				height: 200
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: catNameData,
				crosshair: true,
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Categories'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>What are the top 3 categories selected and bottom 3 categories selected?</small></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: false,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Category',
				data: topCatData

			}]
		});
		
		
		
		femaleData		=	[];
		maleData		=	[];
		ageranges 		= 	[];
		newMaleData		=	[];
		newFemaleData	=	[];
		newAgerange		=	[];
		deviceUsed 		= 	[];
		userSession		=	[];
		sessionDate		=	[];
		
		
	}
	
	
	$(function () {
		
		actionTypes('Y');
		$(".loading-image").hide();
		
	});

</script>

<style>

.row.bottom-line { background-color: #fff; border: 1px solid #337ab7;  margin-bottom: 20px;
box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);}
.bottomTotatlUser{padding-bottom: 27px;}
.well-well-lg{	margin-left:10px;	margin-right:5px;}
.page-header.text-center {  border-bottom: 1px solid #ccc;}
.bottomTotatlUserDiv{ margin-top: 2px;border-top: 1px solid #ccc; }
.text-center.borderright{ border-right: 1px solid #ccc;}
.chart-heading {
  background-color: #337ab7;
  color: #fff;
  margin: 0 -15px;
  padding: 11px;
}

.loading-image>img{
	
	width:50px !important;
	position:relative;
}

</style>

<script type="text/javascript">

$(document).ready(function() {
	 
	function getCookie(cname) {
		
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
		}
		
		return "";
	
	}
	
	var dt 	= new Date();
	var m 	= dt.getMonth();
	var z 	= m + 1; 
	
	user 	= dt.getFullYear()+"-"+z+"-"+dt.getDate()+" "+dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
	
	document.cookie="username="+user;
	
	var xx= getCookie('username') ;

});
</script>
