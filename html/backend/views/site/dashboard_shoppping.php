<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\models\Usermanagement;

$usertype = Yii::$app->user->identity->user_type;
$userId = $usertype == Usermanagement::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;

if($usertype==1) {
	$this->title = Yii::t('app','Alpha Wallet Admin Portal');
}else{
	$this->title = Yii::t('app','Alpha Wallet Partner Portal');
}
?>

<!-- charts start from here -->

<div class="dashTopSection">
		<div class="topTabbing">
			<div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
				<?php

					echo Html::a(Yii::t('app','AT A GLANCE'),['site/index'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','USERS'),['site/dashboard-user'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','DEAL/PASSES'),['site/dashboard-deal'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','BEACONS'),['site/dashboard-beacon'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','CAMPAIGN'),['site/dashboard-campaign'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','LOYALTY'),['site/dashboard-loyality'],['class'=>'btn btn-default']);
					echo Html::a(Yii::t('app','SHOPPING CART/PAYMENT'),['site/dashboard-shoppping'],['class'=>'btn btn-default active']);

				?>
			</div>
		</div>
		<div class="row margin">
			<div class="col-md-4"><?= Yii::t('app','DASHBOARD : SHOPPING CART / PAYMENT') ?></div>
			<div class="col-md-5">
				<button type="button" class="btn btn-default" onclick="return actionTypes('D')"><?= Yii::t('app','DAY') ?></button>
				<button type="button" onclick="return actionTypes('W')" class="btn btn-default"><?= Yii::t('app','WEEK') ?></button>
				<button type="button" onclick="return actionTypes('M')" class="btn btn-default"><?= Yii::t('app','MONTH') ?></button>
				<button type="button"  onclick="return actionTypes('Y')" class="btn btn-default"><?= Yii::t('app','YEAR') ?></button>
			</div>
			<div class="col-md-3"><?php

							$end_date 	= new \DateTime(date('m/d/Y'));
							$end_date->modify('+1 day');
							$maxDate 	= $end_date->format('Y-m-d');

							echo DateRangePicker::widget([
											'id'=>'datetime_range',
											'name'=>'datetime_range',
											'readonly'=>'readonly',
											'convertFormat'=>true,
											'options' => ['placeholder' => Yii::t('app','Select date ...'),
															  'class' => 'form-control'
															  ],
											'pluginOptions'=>[
													'timePicker'=>true,
													'timePickerIncrement'=>30,
													'locale'=>[
														'format'=>'Y/m/d h:i:s A'
													],
													'maxDate'=> $maxDate
												]
										]);

						?></div>
		</div>
</div>

<section class="content">
	<div class="row">
		<!-- AVERAGE TRANSACTION VALUE -->
		<div class="col-md-12">
			<div class="box box-primary text-center">
			<div class="box-header with-border text-left">
			<i class="glyphicon glyphicon-credit-card text-primary"></i>
			<h4 class="box-title"><?= Yii::t('app','AVERAGE TRANSACTION VALUE') ?></h4>
			</div>
			<div class="row">
			<div class="col-lg-12 col-xs-12 text-left margin">
			<p><?= Yii::t('app','What is the average transaction value?') ?></p>
			<h3 id="avg_transc_val"> 0 </h3>

			</div><!-- /.box-body-->
			</div>
			</div><!-- /.box -->
		</div>

		<!-- TRANSACTIONS BY GENDER / AGE / UNITS -->
		<div class="col-md-12">
			<div class="box box-primary">
			<div class="box-header with-border">
			<i class="glyphicon glyphicon-credit-card text-primary"></i>
			<h4 class="box-title"><?= Yii::t('app','TRANSACTIONS BY GENDER / AGE') ?></h4>
			</div>
			<div class="box-body">
			<div class="loading-image text-center">
			<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
			</div>
			<div class="transaction-gender-chart" id="transaction-gender-chart"></div>
			</div><!-- /.box-body-->
			</div><!-- /.box -->
		</div>

		<!-- TRANSACTIONS BY UNITS -->
		<div class="col-md-12">
			<div class="box box-primary">
			<div class="box-header with-border">
			<i class="glyphicon glyphicon-credit-card text-primary"></i>
			<h3 class="box-title"><?= Yii::t('app','UNITS PER TRANSACTION BY GENDER') ?></h3>
			</div>
			<div class="box-body">
			<div class="loading-image text-center">
			<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
			</div>
			<div class="transaction-units-chart" id="transaction-units-chart"></div>
			</div><!-- /.box-body-->
			</div><!-- /.box -->
		</div>
		<!-- PAYMENT VIA -->
		<div class="col-md-12">
			<div class="box box-primary">
			<div class="box-header with-border">
			<i class="glyphicon glyphicon-credit-card text-primary"></i>
			<h4 class="box-title"><?= Yii::t('app','PAYMENT VIA') ?></h4>
			</div>
			<div class="box-body">
			<div class="loading-image text-center">
			<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
			</div>
			<div class="payment-via-chart" id="payment-via-chart"></div>
			</div><!-- /.box-body-->
			</div><!-- /.box -->
		</div>

		<!-- CART ABANDONMENT -->
		<div class="col-md-12">
			<div class="box box-primary">
			<div class="box-header with-border">
			<i class="fa fa-shopping-cart text-primary"></i>
			<h4 class="box-title"><?= Yii::t('app','CART ABANDONMENT') ?></h4>
			</div>
			<div class="box-body">
			<div class="loading-image text-center">
			<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
			</div>
			<div class="cart-abdonment-chart" id="cart-abdonment-chart"></div>
			</div><!-- /.box-body-->
			</div><!-- /.box -->
		</div>
		<!--  CREDIT CARD USE  -->
		<div class="col-md-12">
			<div class="box box-primary">
			<div class="box-header with-border">
			<i class="glyphicon glyphicon-credit-card text-primary"></i>
			<h4 class="box-title"><?= Yii::t('app','CREDIT CARD USE') ?></h4>
			</div>
			<div class="box-body">
			<div class="loading-image text-center">
			<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
			</div>
			<div class="card-used-chart" id="card-used-chart"></div>
			</div><!-- /.box-body-->
			</div><!-- /.box -->
		</div>
		<!-- Transactions Fess -->
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
				<i class="glyphicon glyphicon-credit-card text-primary"></i>
				<h4 class="box-title"><?= Yii::t('app',' Transactions Fess') ?></h4>
				<!-- start filters   -->
				<div class="row">
					<?php
					$url = Yii::$app->request->BaseUrl.'/site/trans-fees';
					if($usertype==1) { ?>
					<div class="col-sm-4 col-xs-12">
						<?php
						$partners = ArrayHelper::map(Usermanagement::find()
						->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
						echo '<label class="control-label">Partner</label>';
						echo Select2::widget([
						'name' => 'partners',
						'id' => 'partners',
						'data' => $partners,
						'options' => [
						'placeholder' => 'Select partner ...',
						'multiple' => false
						],
						'pluginOptions' => [
						'allowClear' => true
						],
						'pluginEvents'=> [
						"select2:unselecting" => "function() { $('#storeList,#deal_drop,#select2-storeList-container,#select2-deal_drop-container').empty();	 }",
						"change" => "function() {							
							var partnerId =	$('#partners').val();
							var dealId =	$('#deal_drop').val();
							var storeId =	$('#storeList').val();							$('#storeList,#deal_drop,#select2-storeList-container,#select2-deal_drop-container').empty();	
							if(partnerId==''){ return false; }													
							$.ajax({
								type:'post',
								url:'$url',
								data:{'partnerId':partnerId,'dealId':dealId,'storeId':storeId}
							})
							.done(function(result){
								if(result){
								var data = $.parseJSON(result);
									$('#total-amt').html('$'+data[2]);
									$('#total-paid-amt').html('$'+data[0]);
									$('#total-dues-amt').html('$'+data[1]);
									if(data[3].length>0){
											var i;
											var option = '<option>Select store ...</option>';
											for(i=0;i<data[3].length;i++){
												option += '<option value='+data[3][i].id+'>'+data[3][i].storename+'</option>';
									}
										$('#storeList').html(option);
									}								
								if(data[4].length>0){
										var i;
										var option = '<option>Select deal ...</option>';
										for(i=0;i<data[4].length;i++){
											option += '<option value='+data[4][i].id+'>'+data[4][i].name+'</option>';
									}
									$('#deal_drop').html(option);
								}
							}

							});
						}",
						],
						]);

						?>
					</div>
					<?php } ?>
					<div class="col-sm-4 col-xs-12">
						<?php
						echo '<label class="control-label">Store</label>';
						echo Select2::widget([
						'name' => 'storeList',
						'id' => 'storeList',
						'data' => array(),
						'options' => [
						'placeholder' => 'Select store ...',
						'multiple' => false
						],
						'pluginOptions' => [
						'allowClear' => true
						],
						'pluginEvents'=> [
						"change" => "function() {
							var partnerId =	$('#partners').val();
							var dealId =	$('#deal_drop').val();
							var storeId =	$('#storeList').val();
							$.ajax({
								type:'post',
								url:'$url',
								data:{'partnerId':partnerId,'dealId':dealId,'storeId':storeId}
							})
							.done(function(result){
								if(result){
									var data = $.parseJSON(result);
									$('#total-amt').html('$'+data[2]);
									$('#total-paid-amt').html('$'+data[0]);
									$('#total-dues-amt').html('$'+data[1]);
								}
							});
						}",
						],
						]);

						?>
					</div>
					<div class="col-sm-4 col-xs-12">
						<?php
						echo '<label class="control-label">Deal</label>';
						echo Select2::widget([
						'name' => 'deal_drop',
						'id' => 'deal_drop',
						'data' => array(),
						'options' => [
						'placeholder' => 'Select deal ...',
						'multiple' => false
						],
						'pluginOptions' => [
						'allowClear' => true
						],
						'pluginEvents'=> [
						"change" => "function() {
								var partnerId =	$('#partners').val();
								var dealId =	$('#deal_drop').val();
								var storeId =	$('#storeList').val();
							$.ajax({
								type:'post',
								url:'$url',
								data:{'partnerId':partnerId,'dealId':dealId,'storeId':storeId}
							})
							.done(function(result){
								if(result){
									var data = $.parseJSON(result);
									$('#total-amt').html('$'+data[2]);
									$('#total-paid-amt').html('$'+data[0]);
									$('#total-dues-amt').html('$'+data[1]);
								}
							});
						}",
						],
						]);

						?>
					</div>
				</div>	<!-- end filters   -->
				</div>

				<div class="box-body">

				<div class="loading-image text-center">
				<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="small-box bg-aqua">
					<div class="inner">
					<h3 id="total-amt">$0.00</h3>
					<p>Total Amount</p>
					</div>
					<div class="icon">
					<i class="ion-ios-briefcase"></i>
					</div>
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="small-box bg-green">
					<div class="inner">
					<h3 id="total-paid-amt">$0.00</h3>
					<p>Total Paid Amount</p>
					</div>
					<div class="icon">
					<i class="ion ion-cash"></i>
					</div>
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="small-box bg-red">
					<div class="inner">
					<h3 id="total-dues-amt">$0.00</h3>
					<p>Total Dues Amount</p>
					</div>
					<div class="icon">
					<i class="ion-ios-calculator"></i>
					</div>
					</div>
				</div>

				</div><!-- /.box-body-->
			</div><!-- /.box -->
		</div>
		<!-- Transactions Fess end -->
		<!-- Subscription Fees -->
		<div class="col-md-12">
			<div class="box box-primary">
			<div class="box-header with-border">
			<i class="glyphicon glyphicon-credit-card text-primary"></i>
			<h4 class="box-title"><?= Yii::t('app','Subscription Charges') ?></h4>
			<div class="row">
					<?php
					$url = Yii::$app->request->BaseUrl.'/site/subs-charges';
					if($usertype==1) { ?>
					<div class="col-sm-4 col-xs-12">
						<?php
						$partners = ArrayHelper::map(Usermanagement::find()
						->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
						echo '<label class="control-label">Partner</label>';
						echo Select2::widget([
							'name' => 'subpartners',
							'id' => 'subpartners',
							'data' => $partners,
							'options' => [
							'placeholder' => 'Select partner ...',
							'multiple' => false
						],
						'pluginOptions' => [
						'allowClear' => true
						],
						'pluginEvents'=> [
						"change" => "function() {
								var partnerId =	$('#subpartners').val();
								$.ajax({
									type:'post',
									url:'$url',
									data:{'partnerId':partnerId}
								})
								.done(function(result){
									if(result){
										var data = $.parseJSON(result);
										$('.subscription-all-amt').html('$'+data[2]);
										$('.subscription-paid-amt').html('$'+data[0]);
										$('.subscription-unpaid-amt').html('$'+data[1]);
									}
								});
						}",
						],
						]);

						?>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="box-body">
				<div class="loading-image text-center">
				<?= Html::img('@web/images/ajax-loader.gif',array('alt'=>'loading ...','title'=>'loading ...'));?>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-orange"><i class="ion ion-social-usd"></i></span>
						<div class="info-box-content">
						<span class="info-box-text">Total Amount</span>
						<span class="info-box-number subscription-all-amt">$0</span>
						</div><!-- /.info-box-content -->
					</div><!-- /.info-box -->
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-blue"><i class="ion ion-ios-box"></i></span>
						<div class="info-box-content">
						<span class="info-box-text">Paid Amount</span>
						<span class="info-box-number subscription-paid-amt">$0</span>
						</div><!-- /.info-box-content -->
					</div><!-- /.info-box -->
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-purple"><i class="ion ion-battery-low"></i></span>
						<div class="info-box-content">
						<span class="info-box-text">Due Amount</span>
						<span class="info-box-number subscription-unpaid-amt">$0</span>
						</div><!-- /.info-box-content -->
					</div><!-- /.info-box -->
				</div>
			</div><!-- /.box-body-->
			</div><!-- /.box -->
		</div>
	</div>
</section>
<!--  Active Users chart  -->

	<!-- charts end from here -->
<link href="<?= Yii::$app->request->BaseUrl.'/css/'?>app.v1.css" rel="stylesheet">
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>highcharts.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>create-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>jquery.easy-pie-chart.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>export.js"></script>
<script src="<?= Yii::$app->request->BaseUrl.'/js/'?>app.plugin.js"></script>

<script>
	$('#datetime_range').on('change', function () {
		var daterange_val 	= $(this).val();

		actionTypes('O',daterange_val);

	});

	var loyaltyData				=	[];
	var loyaltyandpayData		=	[];
	var onlypayData				=	[];
	var dateTimeData			=	[];
	var creditCardData			=	[];
	var cartData				=	[];


	function actionTypes(type,daterange,linktype){

		daterange 	= daterange || '';
		linktype 	= linktype || 'SH';

		var allData;

		var url = '<?php echo Yii::$app->request->BaseUrl; ?>/site/get-dashboard-report';

		allData = graphGenerator(type,url,daterange,linktype);

		var JSONObject 	= JSON.stringify(allData);
		data 			= jQuery.parseJSON(JSONObject);

		// AVERAGE TRASNSACTION VALUE

		if(data.avgTranscValue.avg_transc != '' ){

			$('#avg_transc_val').html(Math.round(Number(data.avgTranscValue.avg_transc) * 100) / 100);

		}

		// Transactions Fess
			$('#total-amt').html('$'+data.transFees[2]);
			$('#total-paid-amt').html('$'+data.transFees[0]);
			$('#total-dues-amt').html('$'+data.transFees[1]);

		// subscriptionFees
		$('.subscription-all-amt').html('$'+data.subscriptionFees[2]);
		$('.subscription-paid-amt').html('$'+data.subscriptionFees[0]);
		$('.subscription-unpaid-amt').html('$'+data.subscriptionFees[1]);

		// PAYMENT VIA


		for(var i = 0; i < data.orderCountPay.length; i++ ){

			loyaltyData.push(Number(data.orderCountPay[i].loyalty));
			loyaltyandpayData.push(Number(data.orderCountPay[i].loyaltyandpay));
			onlypayData.push(Number(data.orderCountPay[i].onlypay));
			dateTimeData.push(data.orderCountPay[i].datetime);


		}


		$('#payment-via-chart').highcharts({
			chart: {
				type: 'column',
				height:200
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: dateTimeData,
				crosshair: true,
				type:'datetime'
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Payments'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>What is the total number of transactions paid via credit card <br />versus paid via points versus paid via points & pay. </small><br />{point.x}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Pay via Points',
				data: loyaltyData

			},{
				name: 'Credit Card Pay',
				data: onlypayData

			},{
				name: 'Points & Pay',
				data: loyaltyandpayData

			}]
		});

		//  CREDIT CARD USE

		var cards;
		for(var i = 0; i < data.creditCardUsed.length; i++ ){

			if(data.creditCardUsed[i].cardType != null){

				cards = data.creditCardUsed[i].cardType;

			}else{
				cards = 'Other';
			}

			creditCardData.push([cards,Number(data.creditCardUsed[i].totalusedcard)]);


		}

		$('#card-used-chart').highcharts({
			chart: {
				height:150
			},
			title: {
				text: ''
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>Which credit card is used for each transaction?</small></span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{point.name}: </td>' +
					'<td style="padding:0"><b>{point.percentage:.1f}%</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b>',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					}
				}
			},
			series: [{
					type: 'pie',
					name: 'Credit Card Used',
					data: creditCardData
				}]
		});

		// CART ABANDONMENT

		var cartDateTime=[];

		for(var i = 0; i < data.cartAbdonmnt.length; i++ ){

			cartDateTime.push(data.cartAbdonmnt[i].datetime);

			cartData.push(Number(data.cartAbdonmnt[i].cart_abdonment));

		}

		$('#cart-abdonment-chart').highcharts({
			chart: {
				type: 'column',
				height:200
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: cartDateTime,
				crosshair: true,
				type:'datetime'
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Cart Abandonment'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How often does cart abandonment happen?</small><br />{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Cart Abandonment',
				data: cartData

			}]
		});


		var MaleTransactions = [];
		var femaleTransactions = [];
		var maleUnits = [];
		var femaleUnits = [];
		var ageRange = [];

		for(var i = 0; i < data.getTransacByGen.length; i++ ){

			MaleTransactions.push(Number(data.getTransacByGen[i].MaleTransaction));
			femaleTransactions.push(Number(data.getTransacByGen[i].femaleTransaction));
			maleUnits.push(Number(data.getTransacByGen[i].maleUnit));
			femaleUnits.push(Number(data.getTransacByGen[i].femaleUnit));
			ageRange.push(data.getTransacByGen[i].name.replace('Between ',''));

		}


		// transaction-gender-chart
		$('#transaction-gender-chart').highcharts({
			chart: {
				type: 'column',
				height:200
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: ageRange,
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Gender'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many transactions broken down by gender by age?</small><br />Age : {point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Male Transactions',
				data: MaleTransactions

			}, {
				name: 'Female Transactions',
				data: femaleTransactions

			}]
		});

		// transaction-units-chart
		$('#transaction-units-chart').highcharts({
			chart: {
				type: 'column',
				height:200
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: ageRange,
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Gender'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px"><small>How many units per transaction?</small><br />Age : {point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Units Per Transaction (Male)',
				data: maleUnits

			}, {
				name: 'Units Per Transaction (Female)',
				data: femaleUnits

			}]
		});


		loyaltyData				=	[];
		loyaltyandpayData		=	[];
		onlypayData				=	[];
		dateTimeData			=	[];
		creditCardData			=	[];
		cartData				=	[];

		MaleTransactions = [];
		femaleTransactions = [];
		maleUnits = [];
		femaleUnits = [];
		ageRange = [];

	}


	$(function () {

		actionTypes('Y');
		$(".loading-image").hide();

	});

</script>

<style>

.row.bottom-line { background-color: #fff; border: 1px solid #337ab7;  margin-bottom: 20px;
box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);}
.bottomTotatlUser{padding-bottom: 27px;}
.well-well-lg{	margin-left:10px;	margin-right:5px;}
.page-header.text-center {  border-bottom: 1px solid #ccc;}
.bottomTotatlUserDiv{ margin-top: 2px;border-top: 1px solid #ccc; }
.text-center.borderright{ border-right: 1px solid #ccc;}
.chart-heading {
  background-color: #337ab7;
  color: #fff;
  margin: 0 -15px;
  padding: 11px;
}

.loading-image>img{

	width:50px !important;
	position:relative;
}

</style>

<script type="text/javascript">

$(document).ready(function() {

	function getCookie(cname) {

		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
		}

		return "";

	}

	var dt 	= new Date();
	var m 	= dt.getMonth();
	var z 	= m + 1;

	user 	= dt.getFullYear()+"-"+z+"-"+dt.getDate()+" "+dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

	document.cookie="username="+user;

	var xx= getCookie('username') ;

});
</script>
