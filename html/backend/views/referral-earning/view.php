<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ReferralEarnings */

$this->title = Yii::t('app', ' Referral Earnings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Referral Earnings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-random"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
     
     		 <?= Html::a(Yii::t('app','OK'), ['index', 'id' => $model->id], ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
             'id',
            [
             'attribute'=>'user_id',
             'value'=>\common\models\User::getUserNameById($model->user_id),
            ],
            [
             'attribute'=>'refer_by',
             'value'=>\common\models\User::getUserNameById($model->refer_by),
            ],
            [
             'attribute'=>'commission_amount',
             'value'=>'$'.$model->commission_amount,
            ],  
            [
             'attribute'=>'commission_percentage',
             'value'=>$model->commission_percentage.'%',
            ],           
            'order_id',
            [
             'attribute'=>'order_total',
             'value'=>'$'.$model->order_total,
            ],   
            [
             'attribute'=>'status',
             'value'=> $model->status == 0 ? 'Pending' : 'Used',
            ],
            
            [
			 'attribute'=>'created_at',
			 'value'=>date('Y-m-d h:i',$model->created_at),
            ],                  
            [
             'attribute'=>'created_by',
             'value'=>\common\models\User::getUserNameById($model->created_by),
            ]
            
        ]
    ]); ?>

</div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->