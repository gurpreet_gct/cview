<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\SubscriptionFees */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Refer via email');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Refer via email'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
   <?php if(\Yii::$app->session->hasFlash('error')) { ?>
                <div class="alert alert-error">	<?= Yii::$app->session->getFlash('error'); ?></div>
                <?php } ?>
                  <?php if(\Yii::$app->session->hasFlash('success')) { ?>
                <div class="alert alert-success">	<?= Yii::$app->session->getFlash('success'); ?></div>
                <?php } ?>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class=" panel panel-primary">
                <div class="panel-heading">
                    <div class="pull-right">
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-plus"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              
                    <div class="box-body" style="min-height:350px;">
						<br><br> 
                      <?php $form = ActiveForm::begin(); ?>
						<?= $form->field($model, 'email')->textInput() ?>
						<div class="form-group">
							<?= Html::submitButton(Yii::t('app', 'Send now') , ['class' =>'btn btn-primary']) ?>
						</div>
                       <?php ActiveForm::end(); ?>
                    </div>
               
                
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>

