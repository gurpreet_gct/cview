<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use common\models\ReferralEarnings;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReferralEarningsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Referral Commission History');
$this->params['breadcrumbs'][] = $this->title;
$obj = new ReferralEarnings();
$visible = 1;
if(Yii::$app->user->identity->user_type!=1){
	$visible = 0;
}
?>

<div class="setting-index">

  <?php
      echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
			return ['id' => $model['id']];
		},
        'columns'=>  [
			 [
              	'attribute'=>'user_id',                  		
				'value'=>function($model){
					return \common\models\User::getUserNameById($model->user_id);
				},                   
                'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $obj->getUsers('user_id'),
				'size' => Select2::SMALL,
				'options' => ['placeholder' => 'Select User'],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

				],
            ],
             [
              	'attribute'=>'refer_by',  
              	'visible'=> $visible,      		         			
				'value'=>function($model){
					return \common\models\User::getUserNameById($model->refer_by);
				},                   
                'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $obj->getUsers('refer_by'),
				'size' => Select2::SMALL,
				'options' => ['placeholder' => 'Select User'],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

				],
            ],			
           'commission_amount',
            'commission_percentage',
            'order_total',
            [
            'attribute'=>'created_at',
			 'value'=>function($model){
				 return date('Y-m-d h:i',$model->created_at);
			 }            
            ],
           ['class'=>'kartik\grid\ActionColumn','template'=>'{view}'],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [               
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>            
               Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-list"></i>'. Yii::t('app',' Referral Commission History'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
</div>
