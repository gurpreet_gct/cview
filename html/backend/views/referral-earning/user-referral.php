<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use common\models\ReferralEarnings;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReferralEarningsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User cash back dollars');
$this->params['breadcrumbs'][] = $this->title;
$obj = new ReferralEarnings();
$visible = 0;
if(Yii::$app->user->identity->user_type==1){
	$visible = 1;
}

?>
<?php if(\Yii::$app->session->hasFlash('error')) { ?>
                <div class="alert alert-error">	<?= Yii::$app->session->getFlash('error'); ?></div>
<?php } ?>
<?php if(\Yii::$app->session->hasFlash('success')) { ?>
                <div class="alert alert-success">	<?= Yii::$app->session->getFlash('success'); ?></div>
<?php } ?>
<div class="setting-index">

  <?php
      echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {
			return ['user_id' => $model['user_id']];
		},
        'columns'=>  [
			 [
              	'attribute'=>'user_id',      
              	'visible'=> $visible,      	     			
				'value'=>function($model){
					return \common\models\User::getUserNameById($model->user_id);
				},                   
                'filterType'=>GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [			
				'data' => $obj->getUsers('user_id'),
				'size' => Select2::SMALL,
				'options' => ['placeholder' => 'Select User'],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
					

				],
            ],
			'lifetime_commission',
            'unused_commission',
            'used_commission',
            
          ['class'=>'kartik\grid\ActionColumn','template'=>'{casout}','buttons' => [
				'casout' => function ($url, $model, $key) {
						$minimumAmt = \common\models\Setting::getCashOutLimit();
						if($model->unused_commission<$minimumAmt){
							return '';
						}
						return Html::a('Cash Out', ['cashout', 'id'=>$model->user_id],['title'=>'Cash Out']);
					},
				]],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [               
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>            
               Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="glyphicon glyphicon-list"></i>'. Yii::t('app',' User cash back dollars'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
</div>
