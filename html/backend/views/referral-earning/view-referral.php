<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ReferralEarnings */

$this->title = Yii::t('app', ' Referral Cashout');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Referral Earnings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$minimumAmt = \common\models\Setting::getCashOutLimit();
?>

<section class="content">
          <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="glyphicon glyphicon-random"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
      		 <?= Html::a(Yii::t('app','OK'), ['index', 'id' => $model->user_id], ['class' => 'btn bg-green']) ?>
     		<?php  if($model->unused_commission>$minimumAmt){  ?>
			 <?= Html::a(Yii::t('app','Cashout'), ['cashout', 'id' => $model->user_id], ['class' => 'btn  btn-primary']) ?>
			<?php } ?>	
		
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                [
             'attribute'=>'user_id',
             'value'=>\common\models\User::getUserNameById($model->user_id),
            ],            
            [
             'attribute'=>'lifetime_commission',
             'value'=>'$'.$model->lifetime_commission,
            ],  
            [
             'attribute'=>'unused_commission',
             'value'=>$model->unused_commission.'%',
            ],           
            [
             'attribute'=>'used_commission',
             'value'=>'$'.$model->used_commission,
            ],   
                      
        ]
    ]); ?>

</div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>


