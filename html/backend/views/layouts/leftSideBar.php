<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    $usertype = Yii::$app->user->identity->user_type;
    $usertypeid = Yii::$app->user->identity->id;
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
jQuery(function ($) {
    var color = $.cookie('color');
    $('.click').on('click', function (e) {
        color = $(this).data('color')
        $.cookie('color', color, {
            expires: 365,
            path: '/'
        });
        location.reload();
    });
});
</script>
<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
                <?php  if(Yii::$app->user->identity->image!='') {  echo Html::img(Yii::$app->params['userUploadPath']."/".Yii::$app->user->identity->image ,array('class'=>'img-circle','alt'=>'User Image')); } else {
                echo Html::img(Yii::$app->params['userUploadPath']."/".'no-image.jpg',array('class'=>'img-circle','alt'=>'User Image'));} ?>
            </div>
            <div class="pull-left info">
              <p><?=Yii::$app->user->identity->fullName;?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..." />
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <!--<li class="header">MAIN NAVIGATION</li>-->
            <li class="<?= $this->context->route == "site/index"?"active":"" ?>">
            <a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('site/index');?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <?php if($usertype ==1 ) {  ?>
			<li class="<?= $this->context->route == "setting/view?id=1"?"active":"" ?>">
				<a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('setting/view?id=1'); ?>">
					<i class="fa fa-cog"></i><span>General Setting</span>
				</a>
			</li>
            <li class="treeview <?= in_array(Yii::$app->controller->id, ["administrators","partners","store","customers","employee"])?"active":"" ?> ">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>User Managements</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "administrators/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('administrators/index');?>"><i class="fa fa-circle-o"></i> Administrators</a></li>
                <li class="<?= $this->context->route == "partners/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('partners/index');?>"><i class="fa fa-circle-o"></i> Partners</a></li>
                <li class="<?= $this->context->route == "store/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('store/index'); ?>"><i class="fa fa-circle-o"></i> Stores</a></li>
                <li class="<?= $this->context->route == "employee/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('employee/index'); ?>"><i class="fa fa-circle-o"></i> Employee</a></li>
                <li class="<?= $this->context->route == "customers/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('customers/index');?>"><i class="fa fa-circle-o"></i> Customers</a></li>
              </ul>
            </li>
            <li class="<?= $this->context->route == "categories/index"?"active":"" ?>">
			<a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('categories/index'); ?>">
			<i class="fa fa-edit"></i>
			<span>Categories</span>
			</a>
			</li>
			<li class="<?= $this->context->route == "regions/index"?"active":"" ?>">
			<a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('regions/index'); ?>">
			<i class="fa fa-map-marker"></i>
			<span>Regions</span>
			</a>
			</li>
			<li class="treeview <?= in_array(Yii::$app->controller->id, ["beacongroup","beaconmanager","beacon-detect"])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-gears"></i>
                <span>Beacon</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "beacongroup/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('beacongroup/index'); ?>"><i class="fa fa-circle-o"></i>Create a beacon group</a></li>
                <li class="<?= $this->context->route == "beaconmanager/index"?"active":"" ?>"><a href="<?=  Yii::$app->urlManagerBackEnd->createAbsoluteUrl('beaconmanager/index'); ?>"><i class="fa fa-circle-o"></i>Add a new beacon</a></li>
                <li class="<?= $this->context->route == "beacon-detect/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('beacon-detect/index'); ?>"><i class="fa fa-circle-o"></i>Beacon Detect Log</a></li>
              </ul>
            </li>
            <li class="<?= $this->context->route == "catalog/index"?"active":"" ?>">
              <a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('catalog/index'); ?>">
                <i class="glyphicon glyphicon-list"></i><span>Catalogue</span>
              </a>
			</li>
			<!--Payment cards -->
             <li class="treeview <?= in_array(Yii::$app->controller->id, ["qr","workorders"])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-plus-square"></i>
                <span>Workorders</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "workorders/index"?"active":"" ?>"><a href="<?=  Yii::$app->urlManagerBackEnd->createAbsoluteUrl('workorders/index'); ?>"><i class="fa fa-circle-o"></i>Create Workorder</a></li>
                <li class="<?= $this->context->route == "workorders/index"?"active":"" ?>"><a href="<?=  Yii::$app->urlManagerBackEnd->createAbsoluteUrl('geo/index'); ?>"><i class="fa fa-circle-o"></i>Set Geolocation</a></li>
                <li class="<?= $this->context->route == "qr/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('qr/index'); ?>"><i class="fa fa-circle-o"></i>Create QR Code / Barcode</a></li>
              </ul>
            </li>
            <!-- Partners Fees -->
             <li class="treeview <?= in_array(Yii::$app->controller->id, ["transactions-fees",'subscription-fees','partner-transactions-fees','subscription-charged'])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-money "></i>
                <span>Partners Fees</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "transactions-fees/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('transactions-fees/index'); ?>"><i class="fa fa-circle-o"></i>Transactions General Rules</a></li>
                <li class="<?= $this->context->route == "partner-transactions-fees/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('partner-transactions-fees/index'); ?>"><i class="fa fa-circle-o"></i>Transactions charges</a></li>
                <li class="<?= $this->context->route == "subscription-charges/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('subscription-charges/index'); ?>"><i class="fa fa-circle-o"></i>Subscription charges</a></li>
              </ul>
            </li>
            <!-- Referral earning -->
             <li class="treeview <?= in_array(Yii::$app->controller->id, ["referral-earning",'referral-earning'])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-user-plus "></i>
                <span>Referral</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "referral-earning/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('referral-earning/index'); ?>"><i class="fa fa-circle-o"></i>Referral History</a></li>
                <li class="<?= $this->context->route == "referral-earning/user-referral"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('referral-earning/user-referral'); ?>"><i class="fa fa-circle-o"></i>Referral cash out</a></li>
              </ul>
            </li>
			<li class="treeview <?= in_array(Yii::$app->controller->id, ["orders","tax","taxclass","taxrule"])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-line-chart "></i>
                <span>Sales</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "orders/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('orders/index'); ?>"><i class="fa fa-circle-o"></i> Orders</a></li>
                <li class="<?= $this->context->route == "orders/cancelorder"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('orders/cancelorder'); ?>"><i class="fa fa-circle-o"></i>Return Orders</a></li>
                <li class="<?= $this->context->route == "tax/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('tax/index'); ?>"><i class="fa fa-circle-o"></i> Taxs</a></li>
                <li class="<?= $this->context->route == "taxclass/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('taxclass/index'); ?>"><i class="fa fa-circle-o"></i> Taxs Class</a></li>
                <li class="<?= $this->context->route == "taxrule/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('taxrule/index'); ?>"><i class="fa fa-circle-o"></i> Taxs Rule</a></li>
              </ul>
            </li>
			<li class="treeview <?= in_array(Yii::$app->controller->id, ["foodproduct","foodcategories","foodorders"])?"active":"" ?>">
				 <a href="#">
					<i class="fa fa-cutlery"></i>
					<span>Food Orders</span>
					<span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li class="<?= $this->context->route == "foodproduct/index"?"active":"" ?>">
						<a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('foodproduct/index'); ?>">
							<i class="glyphicon glyphicon-list" aria-hidden="true"></i>
							<span>Products</span>
						</a>
					</li>
					<li class="<?= $this->context->route == "foodcategories/index"?"active":"" ?>">
						<a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('foodcategories/index'); ?>">
							<i class="fa fa-edit" aria-hidden="true"></i>
							<span>Add Category</span>
						</a>
					</li>
					<li class="<?= $this->context->route == "foodorders/index"?"active":"" ?>">
						<a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('foodorders/index'); ?>">
							<i class="fa fa-cutlery" aria-hidden="true"></i>
							<span>Food Orders</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="treeview <?= in_array(Yii::$app->controller->id, ["cardtype","walletid"])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-gears"></i>
                <span>Wallet</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "cardtype/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('cardtype/index'); ?>"><i class="fa fa-circle-o"></i>ID Card Types</a></li>
              </ul>
            </li>
            <!-- Auctions Module -->  
			<li class="treeview <?= in_array(Yii::$app->controller->id, ["auctions"]) ? "active" : "" ?>">
              <a href="#">
                <i class="fa fa-gears"></i>
                <span>Auctions</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "auctions/index" ? "active" : "" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('auctions/index'); ?>"><i class="fa fa-circle-o"></i>List Auctions</a></li>
              </ul>
            </li>              
            <!-- Payment cards -->
            <li class="treeview <?= in_array(Yii::$app->controller->id, ["paymentcardtype","paymentcard"])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-credit-card"></i>
                <span>Cards &amp; Bank details</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "paymentcardtype/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('paymentcardtype/index'); ?>"><i class="fa fa-circle-o"></i>Payment Card Types</a></li>
                <li class="<?= $this->context->route == "paymentcard/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('paymentcard/index'); ?>"><i class="fa fa-circle-o"></i>Credit cards</a></li>
				<li class="<?= $this->context->route == "bankdetail/index"?"active":"" ?>">
					<a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('bankdetail/index'); ?>">
					<i class="fa fa-bank"></i>
					<span>Bank Details</span>
					</a>
				</li>
              </ul>
            </li>
              <li class="treeview <?= in_array(Yii::$app->controller->id, ["administrators","partners","store","customers"])?"active":"" ?> ">
              <a href="#">
                <i class="glyphicon glyphicon-list"></i>
                <span>RBAC</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "rbac/role"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('rbac/role');?>"><i class="fa fa-circle-o"></i>RBAC Role</a></li>
                <li class="<?= $this->context->route == "rbac/assignment"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('rbac/assignment');?>"><i class="fa fa-circle-o"></i>RBAC Assignment</a></li>
                <li class="<?= $this->context->route == "rbac/permission"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('rbac/permission');?>"><i class="fa fa-circle-o"></i>RBAC Permission</a></li>
                <li class="<?= $this->context->route == "rbac/rule"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('rbac/rule');?>"><i class="fa fa-circle-o"></i>RBAC Rule</a></li>
              </ul>
            </li>
			<?php }elseif($usertype ==7){    ?>
			  <li class="<?= $this->context->route == 'partners/view?id='.$usertypeid ?"active":"" ?>" style="display:none">
              <a href="<?=Url::toRoute(['partners/view?id='.$usertypeid])?>">
                <i class="fa fa-users"></i>
                <span>View Profile</span>
                <span class="label label-primary pull-right"></span>
              </a>
            </li>
             <li class="treeview <?= in_array(Yii::$app->controller->id, ["store","customers"])?"active":"" ?> ">
             <a href="#">
                <i class="fa fa-users"></i>

                <span>User Managements</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="treeview <?= in_array(Yii::$app->controller->id, ["store"])?"active":"" ?>">
                    <a href="#">
                        <i class="fa fa-gears"></i><span>Stores </span><span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?= $this->context->route == "store/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('store/index'); ?>"><i class="fa fa-circle-o"></i> Stores</a></li>
                        <li class="<?= $this->context->route == "employee/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('employee/index'); ?>"><i class="fa fa-circle-o"></i> Employee</a></li>
                    </ul>
                </li>
                <li class="<?= $this->context->route == "customers/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('customers/index');?>"><i class="fa fa-circle-o"></i> Customers</a></li>
              </ul>
            </li>
			<li class="treeview <?= in_array(Yii::$app->controller->id, ["beacongroup","beaconmanager","beacon-detect"])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-gears"></i>
                <span>Beacon</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "beacongroup/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('beacongroup/index'); ?>"><i class="fa fa-circle-o"></i>Create a beacon group</a></li>
                <li class="<?= $this->context->route == "beaconmanager/index"?"active":"" ?>"><a href="<?=  Yii::$app->urlManagerBackEnd->createAbsoluteUrl('beaconmanager/index'); ?>"><i class="fa fa-circle-o"></i> Add a new beacon</a></li>
                <li class="<?= $this->context->route == "beacon-detect/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('beacon-detect/index'); ?>"><i class="fa fa-circle-o"></i>Beacon Detect Log</a></li>
               </ul>
            </li>
            <li class="<?= $this->context->route == "catalog/index"?"active":"" ?>">
			<a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('catalog/index'); ?>">
			<i class="glyphicon glyphicon-list"></i>
			<span>Catalog</span>
			</a>
			</li>
			  <li class="treeview <?= in_array(Yii::$app->controller->id, ["qr","workorders"])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-plus-square"></i>
                <span>Workorders</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "workorders/index"?"active":"" ?>"><a href="<?=  Yii::$app->urlManagerBackEnd->createAbsoluteUrl('workorders/index'); ?>"><i class="fa fa-circle-o"></i>Create Workorder</a></li>
                <li class="<?= $this->context->route == "workorders/index"?"active":"" ?>"><a href="<?=  Yii::$app->urlManagerBackEnd->createAbsoluteUrl('geo/index'); ?>"><i class="fa fa-circle-o"></i>Set Geolocation</a></li>
                <li class="<?= $this->context->route == "qr/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('qr/index'); ?>"><i class="fa fa-circle-o"></i>Create QR Code / Barcode</a></li>
              </ul>
            </li>
			 <li class="treeview <?= in_array(Yii::$app->controller->id, ["foodproduct","foodorders"])?"active":"" ?>">
				 <a href="#">
					<i class="fa fa-cutlery"></i>
					<span>Food Orders</span>
					<span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li class="<?= $this->context->route == "foodproduct/index"?"active":"" ?>">
						<a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('foodproduct/index'); ?>">
							<i class="glyphicon glyphicon-list" aria-hidden="true"></i>
							<span>Products</span>
						</a>
					</li>
						<li class="<?= $this->context->route == "foodorders/index"?"active":"" ?>">
						<a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('foodorders/index'); ?>">
							<i class="fa fa-cutlery" aria-hidden="true"></i>
							<span>Food Orders</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="treeview <?= in_array(Yii::$app->controller->id, ["orders","tax","taxclass","taxrule"])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-line-chart"></i>
                <span>Sales</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "orders/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('orders/index'); ?>"><i class="fa fa-circle-o"></i> Orders</a></li>
              </ul>
            </li>
			<!--li class="treeview <?= in_array(Yii::$app->controller->id, ["cardtype","walletid"])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-gears"></i>
                <span>Wallet</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "cardtype/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('cardtype/index'); ?>"><i class="fa fa-circle-o"></i> Card Types</a></li>
              </ul>
            </li-->
            	<!-- Partners Fees -->
               <li class="treeview <?= in_array(Yii::$app->controller->id, ["transactions-fees"])?"active":"" ?>">
                 <a href="#">
                   <i class="fa fa-money"></i>
                   <span>Fee &amp; Charges</span>
                   <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
                 </a>
                 <ul class="treeview-menu">
                   <li class="<?= $this->context->route == "transactions-fees/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('partner-transactions-fees/index'); ?>"><i class="fa fa-circle-o"></i>Transactions fees</a></li>              
                   <li class="<?= $this->context->route == "transactions-fees/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('subscription-charges/index'); ?>"><i class="fa fa-circle-o"></i>Subscription charges</a></li>
                 </ul>
               </li>
                <!-- Referral earning -->
             <li class="treeview <?= in_array(Yii::$app->controller->id, ["referral-earning",'referral-earning'])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-user-plus "></i>
                <span>Referral</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "referral-earning/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('referral-earning/index'); ?>"><i class="fa fa-circle-o"></i>Referral History</a></li>
                <li class="<?= $this->context->route == "referral-earning/user-referral"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('referral-earning/user-referral'); ?>"><i class="fa fa-circle-o"></i>Referral cash out</a></li>              

              </ul>
            </li>
            <!-- Payment cards -->
            <li class="treeview <?= in_array(Yii::$app->controller->id, ["paymentcardtype","paymentcard"])?"active":"" ?>">
              <a href="#">
                <i class="fa fa-credit-card"></i>
                <span>Cards &amp; Bank details</span>
                <span class="label label-primary pull-right"></span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?= $this->context->route == "paymentcard/index"?"active":"" ?>"><a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('paymentcard/index'); ?>"><i class="fa fa-circle-o"></i>Credit cards</a></li>
                <li class="<?= $this->context->route == "bankdetail/index"?"active":"" ?>">
                    <a href="<?= Yii::$app->urlManagerBackEnd->createAbsoluteUrl('bankdetail/index'); ?>">
                    <i class="fa fa-bank"></i>
                    <span>Bank Details</span>
                    </a>
                </li>
              </ul>
            </li>
			<?php } ?>
			<li class="<?= $this->context->route == "#"?"active":"" ?>">
				<a href="#"><i class="fa fa-th"></i><span>Theme</span></a>
				<ul class="treeview-menu">
					<li class="<?= $this->context->route == "#"?"active":"" ?>">
					<a href="#" class='click' data-color='skin-red'>
					<i class="fa fa-circle-o text-red"></i> Red</a>
					</li>
					<li class="<?= $this->context->route == "#"?"active":"" ?>">
					<a href="#" class='click' data-color='skin-black'>
					<i class="fa fa-circle-o text-black"></i> Black</a>
					</li>
					<li class="<?= $this->context->route == "#"?"active":"" ?>">
					<a href="#" class='click' data-color='skin-green'>
					<i class="fa fa-circle-o text-green"></i> Green</a>
					</li>
					<li class="<?= $this->context->route == "#"?"active":"" ?>">
					<a href="#" class='click' data-color='skin-blue'>
					<i class="fa fa-circle-o text-blue"></i> Blue</a>
					</li>
					<li class="<?= $this->context->route == "#"?"active":"" ?>"  >
					<a href="#" class='click' data-color='skin-yellow'>
					<i class="fa fa-circle-o text-yellow"></i> Yellow</a>
					</li>
                    <li class="<?= $this->context->route == "#"?"active":"" ?>"  >
					<a href="#" class='click' data-color='skin-pink'>
					<i class="fa fa-circle-o text-pink"></i> Pink</a>
					</li>
				</ul>
			</li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
<style>
.sidebar-form{ display:none; }
</style>