<?php use yii\helpers\Html; 
 use yii\helpers\Url;
 
$usertypeid = Yii::$app->user->identity->id;
$usertype = Yii::$app->user->identity->user_type;

?>
   <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::toRoute(['site/index'])?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <?php  if($usertype==1) { ?>
          <span class="logo-mini"><b>AW</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><?=Yii::$app->name;?></b></span>
          <?php }else{  ?>
			   <span class="logo-mini"><b>AW</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><?=Yii::$app->name;?></b></span>
		  <?php } ?>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <?php if(Yii::$app->user->identity->user_type==7){ ?>
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                 <i class="fa fa-user-plus" aria-hidden="true"></i> INVITE A FRIEND
                </a>    
                 <ul class="dropdown-menu">
					 <li class="header">Your Referral code : <b><?= Yii::$app->user->identity->user_code;?></b></li>
					  <li>
						  <ul class="menu">
							  <li>
								<a href="<?=Url::toRoute(['referral-earning/referviaemail'])?>">
								 <i class="fa fa-envelope-square" aria-hidden="true"></i> Share via Email
								</a>
							  </li>
							 <li>
								<a href="#">
								  <i class="fa fa-facebook-square" aria-hidden="true"></i> Share via Facebook
								</a>
							  </li>
							  <li>
								<a href="#">
								  <i class="fa fa-twitter-square"></i> Share via Twitter
								</a>
							  </li>
							  <li>
								<a href="#">
								  <i class="fa fa-linkedin-square"></i> Share via Linkedin
								</a>
							  </li>
						  </ul>
					  </li>
				 </ul>           
              </li>
             <?php } ?>
             
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">                  
                  <?php if(Yii::$app->user->identity->image!='') { echo Html::img(Yii::$app->params['userUploadPath']."/".Yii::$app->user->identity->image,array('class'=>'user-image','alt'=>'User Image')); } else {
					 echo Html::img(Yii::$app->params['userUploadPath']."/".'no-image.jpg',array('class'=>'user-image','alt'=>'User Image')); 
					  
					 } ?>
                  <span class="hidden-xs"><?= ucfirst(Yii::$app->user->identity->fullName);?></span>
                </a>
                <ul class="dropdown-menu userView">
                  <!-- User image -->
                  <li class="user-header">                    
                    <?php if(Yii::$app->user->identity->image!='') { echo Html::img(Yii::$app->params['userUploadPath']."/".Yii::$app->user->identity->image,array('class'=>'img-circle','alt'=>'User Image')); } else {
						
					echo Html::img(Yii::$app->params['userUploadPath']."/".'no-image.jpg',array('class'=>'img-circle','alt'=>'User Image'));	
					} ?>
                    <p>
                      <?= ucfirst(Yii::$app->user->identity->fullName);?> - <?php echo Yii::$app->user->identity->usertype->usertype; ?>
                      <small>Member since <?= date('M. Y',Yii::$app->user->identity->created_at);?></small>
                    </p>
                  </li>
             
                  <!-- Menu Footer -->
                  <li class="user-footer">
                    <!--<div class="pull-left"> -->
						<?php  if($usertype==1) { ?>
                      <a href="<?=Url::toRoute(['administrators/view?id='.$usertypeid])?>" class="btn btn-primary btn-block">Profile</a>
                      <?php } else { ?>
						  
						   <a href="<?=Url::toRoute(['partners/view?id='.$usertypeid])?>" class="btn btn-primary btn-block">Profile</a>
						   
						<?php } ?>
                   <!-- </div> 
                    <div class="pull-right">-->
                      <a href="<?= \Yii::$app->urlManager->createAbsoluteUrl('site/logout');?>" class="btn btn-success btn-default btn-block">Sign out</a>
                    <!-- </div> -->
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!-- <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li> -->
            </ul>
          </div>
        </nav>
      </header>
