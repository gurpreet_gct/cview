<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$usertype = Yii::$app->user->identity->user_type;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    
    <?php $this->head() ?>
       
</head>

<?php if( isset($_COOKIE['color']) && !empty($_COOKIE['color'])){ ?>   
	<body class="<?php  echo $_COOKIE['color']; ?> sidebar-mini">
<?php } else { ?>	
	<body class="<?php echo 'skin-blue'; ?> sidebar-mini">	
<?php } ?>	
  
    <div class="wrapper">
      <!-- Header -->
      <?= $this->render("header"); ?>
      <!-- Left Menu -->
      <?= $this->render("leftSideBar"); ?>
      
       <!-- Content --> 
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       <!-- Content Header (Page header) -->
        <section class="content-header">
		<div class="topBreadcrumbs">
		 <!--<h1>
            <?php // $this->title;?>
             <small>Control panel</small>
            <br /> 
          </h1> -->
      
        
      <?= Breadcrumbs::widget([
           
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
           
        ]) ?>  
		</div>
        </section>

   
      <section class="content">  
     <?= $content ?>
     </section>     
      </div><!-- /.content-wrapper -->    
      
      
  
 
    
       <!-- Right Menu -->
      <?= $this->render("footer"); ?>

    
    <?php //= $this->render("rightSideBar");?>
    
         <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
      
    </div><!-- ./wrapper -->
    
       
    
    
    <?php $this->endBody() ?>
	<!-- Bootstrap WYSIHTML5 -->
	

    <script src="<?php echo Yii::$app->homeUrl; ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script> 
	  <!-- FastClick -->
	  	 
            
    <script src="<?php echo Yii::$app->homeUrl; ?>plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
  
   
 <script src="<?php echo Yii::$app->homeUrl; ?>/dist/js/custom.js" type="text/javascript"></script>
</body>

   
    
</html>
<?php $this->endPage() ?>
