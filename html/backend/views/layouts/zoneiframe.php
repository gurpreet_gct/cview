<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);


 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
         <div class="" style="100% !important;">
               <?= $content ?>
        </div>
    </div>

   <?php $this->endBody() ?>
    <script src="<?php echo Yii::$app->homeUrl; ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script> 
	  <!-- FastClick -->
	  	 
            
    <script src="<?php echo Yii::$app->homeUrl; ?>plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::$app->homeUrl; ?>/dist/js/custom.js" type="text/javascript"></script>
</body>
</html>
<?php $this->endPage() ?>
