<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BeaconGroup */

$this->title = yii::t('app','Create Beacon Group');
$this->params['breadcrumbs'][] = ['label' => yii::t('app','Beacon Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beacon-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
