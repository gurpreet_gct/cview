<?php

use yii\helpers\Html;
#use yii\grid\GridView;
use kartik\grid\GridView;
use backend\models\Usermanagement;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Beaconmanager */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Beacon Manager');
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$visiable = 0;
if($userType==1){
$advertiser = ArrayHelper::map(Usermanagement::find() ->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
$visiable = 1;
}else{
		$advertiser='';
	}
?>


<div class="beacon-group-index">


<?php

        echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
        'columns'=>  [
           
            'sn',
            'name',
            [   
            'attribute'=>'locationOwnerID',
            'label' => Yii::t('app','Advertiser'),
			'visible'=> $visiable,
            'value' => 'locationownername',
            'filterType'=>GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
             'data' => $advertiser,
             'size' => Select2::SMALL,
             'options' => ['placeholder' => 'Select advertiser'],
				'pluginOptions' => [					
					'width'=>'150px',
					'allowClear' => true,
				],
             ],
            ],
            'major',
            'minor',
           
             'manufacturer',
            // 'manufacturerSerial',
            // 'uuid',
            // 'major',
            // 'minor',
            // 'rssiID',
            // 'intervalID',
            [
            'attribute'=>'status',
            'value'=>'statustype',
            'filter' => array('1' => Yii::t('app','Enable'), '0' => Yii::t('app','Disable') ),
            ],
            
           ['class'=>'kartik\grid\ActionColumn'],
            ],
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
      //   'export'=>false,       
        'beforeHeader'=>[
            [
                /*'columns'=>[
                    ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                    ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
                ],*/
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        // set your toolbar
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],[ 'title'=>Yii::t('app','Add a new beacon'),'data-pjax'=>1, 'class'=>'btn btn-success', ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Reset Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
       'export'=>[
            'fontAwesome'=>true
        ],
        // parameters from the demo form
        'bordered'=>true,
       'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-gears"></i>'.Yii::t('app','Beacon Manager'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    

?>
 
</div>
