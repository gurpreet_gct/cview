<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Beaconmanager */

$this->title = Yii::t('app','Update Beacon: ') . ' ' . ucfirst($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Beacon Manager'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => ucfirst($model->name), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
    <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
               <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="fa fa-gears"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
                <!-- form start -->
                
                 <?= $this->render('_form', [
        'model' => $model,
        'storebeacon'=>$storebeacon,
        'storeBeaconModel'=>$storeBeaconModel,
    ]) ?>
              
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->