<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use common\models\Country;
use common\models\Profile;
use common\models\Store;
use backend\models\Beacongroup;
use backend\models\Beaconlocation;
use backend\models\City;
use backend\models\Regions;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
use yii\web\JsExpression;

$usertype = Yii::$app->user->identity->user_type; 


/* @var $this yii\web\View */
/* @var $model app\models\Beaconmanager */
/* @var $form yii\widgets\ActiveForm */

		/* check for free beacon */
		if($usertype==1){
			if($model->id){
				$storebeaconlist =  ArrayHelper::map(Store::find()
				->joinWith('storebeacons', true, 'LEFT JOIN')
				->select('tbl_store_beacons.beacon_id ,tbl_store_beacons.store_id ,tbl_store.id,tbl_store.storename,tbl_store.status')
				->Where([Store::tableName().'.status'=>1, Store::tableName().'.owner'=> $model->locationOwnerID])
				->andWhere('store_id IS NULL' )
				->orWhere('beacon_id ='.($model->id!=""?$model->id:0))
				->all(), 'id', 'storename');
			
			}else{
				$storebeaconlist = array();
			} 
		}else{
			
			if($model->id){
				$storebeaconlist =  ArrayHelper::map(Store::find()
				->joinWith('storebeacons', true, 'LEFT JOIN')
				->select('tbl_store_beacons.beacon_id ,tbl_store_beacons.store_id ,tbl_store.id,tbl_store.storename,tbl_store.status')
				->andWhere('tbl_store.status=1' )
				->andWhere('tbl_store.owner='.Yii::$app->user->identity->id )
				->andWhere('store_id IS NULL' )
				->orWhere('beacon_id ='.($model->id!=""?$model->id:0))
				->all(), 'id', 'storename'); 
			}else{
				

				$storebeaconlist = array();
			}
		}



?>



    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-beacon-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]);
    ?>
	 <div class="box-body">
      <div class="form-group">
		
	  <?php // echo $form->errorSummary($model); ?>
	  
		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
   
		<?= $form->field($model, 'sn')->textInput(['maxlength' => true]) ?>
      
      
		
		<?php 
		
		if($usertype==1){  
			
			$locationOwner =  ArrayHelper::map(User::find()
			->where(['user_type' => 7,'status' => 10])->all(), 'id', 'orgName');  
			echo $form->field($model, 'locationOwnerID')->widget(Select2::classname(), [
			'data' => $locationOwner,
			'options' => ['placeholder' => Yii::t('app','Select Relevant Partner...')],
			'pluginOptions' => [
			'allowClear' => true
			],
		]);
		}else{
			
			$usertype = Yii::$app->user->identity->user_type;
			$userId = $usertype==7?Yii::$app->user->id : Yii::$app->user->identity->advertister_id; ?>
			<?= $form->field($model, 'locationOwnerID')->hiddenInput(['value' =>$userId])->label(false) ?>
			
			<?php $locationOwner =  ArrayHelper::map(User::find()
			->where(['user_type' => 7,'status' => 10,
			'id' => Yii::$app->user->identity->id])->all(), 'id', 'orgName');  
			
		}  
		?>  
		
		<?php 
		?>
		<?php   $locationBrocker =  ArrayHelper::map(Profile::find()->joinWith('user', true, 'INNER JOIN')->select('tbl_users.id ,tbl_users.user_type ,tbl_profile.id ,tbl_profile.user_id, tbl_profile.companyName')->where(['user_type' => 8,'status' => 10 ] )->all(), 'id', 'companyName');
      
		?> 
		
		<?php /*echo $form->field($model, 'locationBrockerID')->widget(Select2::classname(), [
			'data' => $locationBrocker,
			'options' => ['placeholder' => Yii::t('app','Select Brocker...')],
			'pluginOptions' => [
			'allowClear' => true
			],
		]);*/
		?>
		
    
   
     <div class="box box-success">
    <h3>Beacon Location </h3>

		<?= $form->field($model, 'streetAddress')->textInput(['maxlength' => true]) ?>

		<?php   $Countryname =  ArrayHelper::map(Country::find()->all(), 'id_countries', 'name'); ?>
    
		<?= $form->field($model, 'countryID')->dropDownList($Countryname, array('prompt'=>Yii::t('app','Select'))) ?>   
         
     </div>
	 <div class="box box-success">
    <h3>Beacon Parameters</h3>
    
		<?= $form->field($model, 'manufacturer')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'major')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'minor')->textInput(['maxlength' => true]) ?>
		
	
		
		<?= $form->field($model, 'notesBox')->widget(CKEditor::className(), [
				'options' => ['rows' => 6],
				'preset' => 'basic',
				
			]) ?>
			
		
		<?= $form->field($storebeacon, 'store_id')->widget(Select2::classname(), [
			'data' => $storebeaconlist,
			'options' => ['placeholder' => Yii::t('app','Select Store Location ...')],
			'pluginOptions' => [
			'allowClear' => true
			],
		]);
		
		//if(empty($storebeaconlist)) { echo '<div class="help-block">'."Yii::t('app','No Any Free Beacon Device Available.')	".'</div>'; }		
		?>
		
		 <?php
		/*   $data = '';
		   $group_name =  ArrayHelper::map(Beacongroup::find()->orderBy('group_name')->all(), 'id', 'group_name');
			 
		   if(isset($model->groups)) {
			   
			   if(!is_array($model->groups)) {
					$data = explode(',',$model->groups);  
				}else {
					$data =$model->groups; 
				}
			} */
			   
			?>
		
		   <?php   
		   if($model->id){
			   $groups ='';
			   if(is_array($model->groups)){
				 $groups = implode(', ',$model->groups);
				 }else{
				 $groups = $model->groups;
				 }
			 
				$beaconGroup=Beacongroup::find()->where('id IN('.$groups.')')->orderBy('group_name')->asArray()->all();     
				$model->groups =  array_column($beaconGroup,"id");
				$cityDesc= array_column($beaconGroup,"group_name");  
				
			
			}else{
				$cityDesc =array();
			}            
				$url = \yii\helpers\Url::to(['beacon-groupname']);
             ?>
           
  <!-- Assign multiple beacon group-->
<?php
           echo $form->field($model, 'groups')->widget(Select2::classname(), [
           'initValueText' => $cityDesc, // set the initial display text
           'options' => ['placeholder' => 'Search for beacon group ...','multiple' => true],
           'pluginOptions' => [
               'allowClear' => true,
               'minimumInputLength' => 3,
               'language' => [
                   'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
               ],
               'ajax' => [
                       'url' => $url,
                       'method'=>'POST',
                       'dataType' => 'json',                    
                       'data' => new JsExpression('function(params) { var selectedVal=($("#beaconmanager-groups").val()); return {q:params.term,sq:selectedVal}; }')
                   
               ],
               'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
               'templateResult' => new JsExpression('function(city) { return city.text; }'),
               'templateSelection' => new JsExpression('function (city) { return city.text; }'),
           ],
       ]);
   ?>


     <?= $form->field($model, 'status')->dropDownList(array('' => Yii::t('app','Select'), 1 =>Yii::t('app','Enable'), 0 =>Yii::t('app','Disable') )) ?>
    </div>

   
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Add Beacon') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Users')]) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>

<?php $siteurl = Yii::$app->getUrlManager()->getBaseUrl(); 
	$storelist =$siteurl.'/beaconmanager/storelist'; 
?>   
    
<script type="text/javascript">
	
	$("#beaconmanager-locationownerid").on("change", function () {			
		$('#select2-storebeacons-store_id-container').empty();			
		var values = this.value;	
		
		$.ajax({
			method: "POST",
			url: "<?php echo $storelist; ?>",
			data: { id: values }	
	
	})
	.done(function(result) {
		data1 = JSON.parse(result);	
		
		$('#storebeacons-store_id').empty();	
		 var div_data1="<option value=''><?= Yii::t('app','select') ?></option>";         	
		$.each(data1, function (key, data) {   
			
			$.each(data, function (index, data) {	
						 
				 var div_data="<option value="+index+">"+data+"</option>"; 
					$(div_data1).appendTo('#storebeacons-store_id'); 
					$(div_data).appendTo('#storebeacons-store_id'); 
				
			})
		});
		
	  });
		
	});
var usertype = '<?php echo $usertype ; ?>';
var usertype2 = '<?php echo $model->isNewRecord ; ?>';
if(usertype==7 && usertype2==1 ){	
$(document).ready(function(){
		var values = $("#beaconmanager-locationownerid").val();
	    $.ajax({
			method: "POST",
			url: "<?php echo $storelist; ?>",
			data: { id: values }	
	
	})
	.done(function(result) {
		data1 = JSON.parse(result);	
		
		$('#storebeacons-store_id').empty();	
		 var div_data1="<option value=''><?= Yii::t('app','select') ?></option>";         	
		$.each(data1, function (key, data) {   
			
			$.each(data, function (index, data) {	
						 
				 var div_data="<option value="+index+">"+data+"</option>"; 
					$(div_data1).appendTo('#storebeacons-store_id'); 
					$(div_data).appendTo('#storebeacons-store_id'); 
				
			})
		});
		
	  });
});
}
</script>
