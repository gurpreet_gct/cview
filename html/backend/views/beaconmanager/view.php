<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Beaconmanager */

 
 
$this->title = Yii::t('app','View Beacon: ') . ' ' .ucfirst($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Beacon Manager'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$visible = 0;
if($userType==1){
$visible = 1;
}
else{
	$visiable = 0;
}
?>
    <div class="row">
            <!-- left column -->
          
             <div class="col-md-12">
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="fa fa-gears"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
             <?= Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
     		   <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a(Yii::t('app','OK'), ['index'], ['class' => 'btn bg-green']) ?>
           </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'sn',
            [                     
            'label' => Yii::t('app','Relevant Advertiser'),
            'visible'=>$visible,
            'value' => $model->locationownername,
            ], 
                      
            'streetAddress',
           
            
             [                     
            'label' => Yii::t('app','Country'),
            'value' => $model->countryname->name,
            ], 
         
     
            'manufacturer',
            'major',
            'minor',
            'notesBox:html',
                      
             [                     
            'label' => Yii::t('app','Store Location'),
            'value' => $model->storename,
             ],
             [                     
            'label' => Yii::t('app','Beacon Group'),
            'value' => $model->beacongroupname,
             ], 
             
            
           
           [                     
            'label' => Yii::t('app','Status'),
            'value' => $model->statustype,
             ],
            
            
        ],
    ]) ?>

</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->