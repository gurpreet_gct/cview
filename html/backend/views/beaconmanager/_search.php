<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BeaconManagerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="beacon-manager-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'identifier') ?>

    <?= $form->field($model, 'linksUrl') ?>

    <?= $form->field($model, 'majorValue') ?>

    <?php // echo $form->field($model, 'minorValue') ?>

    <?php // echo $form->field($model, 'powerValue') ?>

    <?php // echo $form->field($model, 'rSSIValue') ?>

    <?php // echo $form->field($model, 'soundFileUrl') ?>

    <?php // echo $form->field($model, 'timeStamp') ?>

    <?php // echo $form->field($model, 'interval') ?>

    <?php // echo $form->field($model, 'category') ?>

    <?php // echo $form->field($model, 'groups') ?>

    <?php // echo $form->field($model, 'businessContactNumber') ?>

    <?php // echo $form->field($model, 'businessDetail') ?>

    <?php // echo $form->field($model, 'businessDetailDescription') ?>

    <?php // echo $form->field($model, 'businessEmailAddress') ?>

    <?php // echo $form->field($model, 'businessFacetime') ?>

    <?php // echo $form->field($model, 'businessGalleryCaptions') ?>

    <?php // echo $form->field($model, 'businessGalleryPictures') ?>

    <?php // echo $form->field($model, 'businessLocation') ?>

    <?php // echo $form->field($model, 'beaconCountry') ?>

    <?php // echo $form->field($model, 'businessName') ?>

    <?php // echo $form->field($model, 'businessPictureUrl') ?>

    <?php // echo $form->field($model, 'businessSubscribe') ?>

    <?php // echo $form->field($model, 'businessType') ?>

    <?php // echo $form->field($model, 'businessWebAddress') ?>

    <?php // echo $form->field($model, 'beaconSoundTitle') ?>

    <?php // echo $form->field($model, 'passID') ?>

    <?php // echo $form->field($model, 'passUrl') ?>

    <?php // echo $form->field($model, 'locationOwnerID') ?>

    <?php // echo $form->field($model, 'locationBrockerID') ?>

    <?php // echo $form->field($model, 'locationID') ?>

    <?php // echo $form->field($model, 'streetNumber') ?>

    <?php // echo $form->field($model, 'streetAddress') ?>

    <?php // echo $form->field($model, 'countryID') ?>

    <?php // echo $form->field($model, 'cityID') ?>

    <?php // echo $form->field($model, 'suburbID') ?>

    <?php // echo $form->field($model, 'postCode') ?>

    <?php // echo $form->field($model, 'latitude') ?>

    <?php // echo $form->field($model, 'longitude') ?>

    <?php // echo $form->field($model, 'imgBeaconLoc') ?>

    <?php // echo $form->field($model, 'manufacturer') ?>

    <?php // echo $form->field($model, 'manufacturerSerial') ?>

    <?php // echo $form->field($model, 'uuid') ?>

    <?php // echo $form->field($model, 'major') ?>

    <?php // echo $form->field($model, 'minor') ?>

    <?php // echo $form->field($model, 'rssiID') ?>

    <?php // echo $form->field($model, 'intervalID') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app','Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
