<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use backend\models\Usermanagement;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FoodordersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Food Order');
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$visiable = 0;
if($userType==1){
$advertiser = ArrayHelper::map(Usermanagement::find()->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
$visiable = 1;
}else{
		$advertiser='';
	}

?>

<div class="foodorders-index">
<?php
Pjax::begin();
echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
    return ['id' => $model['id']];
},
         'columns'=>  [
						[
							'attribute'=>Yii::t('app','user_id'),
							'value'=>'username',
						],
						[
							'attribute'=>Yii::t('app','store_id'),              
							'value'=> 'sname',  
							 'visible'=> $visiable,                 
							'filterType'=>GridView::FILTER_SELECT2,
							'filterWidgetOptions' => [   
							'data' => $advertiser,
							'size' => Select2::SMALL,
							'options' => ['placeholder' => Yii::t('app','Select Store')],
								'pluginOptions' => [     
								'width'=>'150px',
								'allowClear' => true,
								],
							],
						],

						'grand_total',
						'qty',

						[
							'attribute'=>'created_at',
							'value' => function ($dataProvider) {
							return date("Y-m-d",$dataProvider["created_at"]);

							},
						], 
						['class'=>'kartik\grid\ActionColumn','template' => '{view}{delete}'],
				],
        'containerOptions'=>['style'=>'overflow: auto'], 
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true,       
        'beforeHeader'=>[
            [           
                'options'=>['class'=>'skip-export']
            ]
        ],
          'toolbar'=> [
            ['content'=>
                 Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> Yii::t('app','Refresh Grid')])
            ],
            '{export}',
            '{toggleData}',
        ],
          'export'=>[
            'fontAwesome'=>true
        ],       
        'bordered'=>true,
        'striped'=>false,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'responsiveWrap' => false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'<i class="fa fa-cutlery"></i>'.Yii::t('app','Food Orders'),
        ],
        'persistResize'=>false,
        'exportConfig'=>['csv'=>'csv'],
    ]);
    Pjax::end();
?>
</div>
