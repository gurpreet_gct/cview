<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Foodorders;
use backend\models\FoodordersSearch;
use common\models\users;

/* @var $this yii\web\View */
/* @var $model common\models\Foodorders */

$this->title = Yii::t('app','Order# '). $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$userType=Yii::$app->user->identity->user_type;
$visible = 0;
if($userType==1){
$visible = 1;
}
else{
	$visiable = 0;
}
?>
    <div class="row">
            <!-- left column -->
             <div class="col-md-12">
			 <div id="message">
					<?= Yii::$app->session->getFlash('sucess');?>
			</div>
            <div class=" panel panel-primary">
                <div class="panel-heading">    
                    <div class="pull-right">                       
                    </div>
                    <!-- Header Title-->
                    <h3 class="panel-title">
                        <i class="fa fa-cutlery"></i>  <?= Html::encode($this->title) ?>
                    </h3>
                    <!-- Header Title end -->
                    <div class="clearfix"></div>
                </div>
              <!-- general form elements -->
            
                <div class="box-header with-border">
                  <h3 class="box-title"> <!--<?= Html::encode($this->title) ?>--> </h3>
              <p>   
             <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>     		
          <?= Html::a('OK', ['index'], ['class' => 'btn bg-green']) ?>
		   </p>
        
                </div><!-- /.box-header -->
                <!-- form start -->

   
 <div class="table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            ['attribute'=>Yii::t('app','user_id'),
			'value'=>$model->username,
			],
            'qty',
            //'store_id',
			[
			 'attribute'=>Yii::t('app','store_id'),
			 'visible'=>$visible,
			 'value'=>$model->sname,
			],
            [
				'label'=>Yii::t('app','is offline'),
				'value'=>$model->is_offline = $model->is_offline==0?Yii::t('app','No'):Yii::t('app','Yes')
			],
            //'coupon_code',
            'grand_total',
            'tax_amount',
            'discount_amount',
            [
				'label'=>Yii::t('app','Status'),
				'value'=>$model->status = $model->status==2?Yii::t('app','Completed'):Yii::t('app','Pending')
			],
			
           [
			'label'=>Yii::t('app','Create at'),
			'value'=>date('Y-m-d H:s',$model->created_at),
		   ], 
		   [
			'label'=>Yii::t('app','Updated at'),
			'value'=>date('Y-m-d H:s',$model->updated_at),
		   ],  
        ],
    ]) ?>
</div>
</div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->