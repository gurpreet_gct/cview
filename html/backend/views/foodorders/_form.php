<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Foodorders;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\Foodorders */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$form = ActiveForm::begin([
	'options' => [
	'id' => 'create-food-orders-form',	
	]
]);
?>
 <div class="box-body">
      <div class="form-group">
      
      
     <?php //echo $form->errorSummary($model); ?>
     <?= $form->field($model, 'user_id')->textInput(['maxlength' => true]) ?>
     <?= $form->field($model, 'is_offline')-> dropDownList(array('' => 'select',  '1'=> 'Yes', '0' =>'No')) ?>
     <?= $form->field($model, 'status')->dropDownList(array('' => yii::t('app','Select'),1 => yii::t('app','Enable'), 0 => yii::t('app','Disable'))) ?>
     <?= $form->field($model, 'qty')->textInput(['maxlength' => true]) ?>
     <?= $form->field($model, 'grand_total')->textInput(['maxlength' => true]) ?>
     <?= $form->field($model, 'tax_amount')->textInput(['maxlength' => true]) ?>
     <?= $form->field($model, 'discount_amount')->textInput(['maxlength' => true]) ?>
     <?= $form->field($model, 'created_at')->widget(
     DatePicker::className(), [
        
		'language' => 'en',
		'size' => 'lg',
        'clientOptions' => [
            'autoclose' => true,
            'endDate' => date('2050-12-31'),
            'format' => 'yyyy-mm-dd',
        ]
        ]);?>
     <?= $form->field($model, 'updated_at')->widget(
     DatePicker::className(), [
        
		'language' => 'en',
		'size' => 'lg',
        'clientOptions' => [
            'autoclose' => true,
            'endDate' => date('2050-12-31'),
            'format' => 'yyyy-mm-dd',
        ]
        ]);?>

  </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? yii::t('app','Create') : yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> yii::t('app','Users')]) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>



  
