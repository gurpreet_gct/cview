<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Store;
use backend\models\Beaconmanager;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Storebeacons */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="storebeacons-form">

    <?php $form = ActiveForm::begin(); ?>

     <div class="box-body">
      <div class="form-group">
    
     <?php   $storeNames =  ArrayHelper::map(Store::find()->all(), 'id', 'storename'); ?>
    
    <?= $form->field($model, 'store_id')->dropDownList($storeNames, array('prompt'=>Yii::t('app','Select')); ?>
    
     <?php   $beaconNames =  ArrayHelper::map(Beaconmanager::find()->all(), 'id', 'name'); ?>
    
    <?= $form->field($model, 'beacon_id')->dropDownList($beaconNames, array('prompt'=>Yii::t('app','Select'))); ?>
    
     <?= $form->field($model, 'status')->dropDownList(array('' => Yii::t('app','Select'), 1 =>Yii::t('app','Enable'), 0 =>Yii::t('app','Disable') )) ?>

 </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
     </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
