<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\cardtype */
/* @var $form yii\widgets\ActiveForm */
?>



   <?php
    $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-cardtype-form',
                    'enctype' =>'multipart/form-data',
                ]
    ]);
    ?>
	 <div class="box-body">
      <div class="form-group">
	 <?php //echo $form->errorSummary($model); ?>
     <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

   
	  <?= $form->field($model, 'imageFile')->fileInput() ?>
    

	 <?= $form->field($model, 'status')->dropDownList(array('' => 'Select', 1 =>'Enable', 0 =>'Disable' )) ?>

   
    </div><!-- /.box-body -->
     <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),  ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Users']) ?>
    </div>
     </div>
     

    <?php ActiveForm::end(); ?>
