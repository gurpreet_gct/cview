<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);



return [
   'name'=>'Alpha Wallet',
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
      'rbac' =>  [
          'class' => 'johnitvn\rbacplus\Module'
  		],
      'gii' => 'yii\gii\Module',
       'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'treemanager' =>  [
        'class' => '\kartik\tree\Module',
        // other module settings, refer detailed documentation
    ]


    ],
    'components' => [
        'user' => [
    'identityClass' => 'common\models\User',
    'enableAutoLogin' => true,
    'identityCookie' => [
        'name' => '_backendUser', // unique for backend
        'path'=>'/backend/web'  // correct path for the backend app.
    ]
],
'session' => [
    'name' => '_backendSessionId', // unique for backend
    'savePath' => __DIR__ . '/../runtime', // a temporary folder on backend
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],


    ],
    'params' => $params,
	'as beforeRequest' =>
							[  //if guest user access site so, redirect to login page.
							'class' => 'yii\filters\AccessControl',
							'rules' => [
								[
									'actions' => ['login', 'error' , 'forgot','reset'],
									'allow' => true,
								],
								[
									'allow' => true,
									'roles' => ['@'],
								],
							],
						],
];
