<?php
return [
    'partners/index' => [
        'type' => 2,
        'description' => 'allow permission to admin',
    ],
    'categories/index' => [
        'type' => 2,
        'description' => 'allow admin to access categories index page',
    ],
    'categories/update' => [
        'type' => 2,
        'description' => 'allow admin to update categories',
    ],
];
