<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    /*public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];*/
    

    public $sourcePath = '@app/themes/';
    public $css = ['admin-lte/dist/css/AdminLTE.css',
    'admin-lte/dist/css/custom.css',
    'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
    'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',    
      //'admin-lte/bootstrap/css/bootstrap.min.css',
        //'admin-lte/plugins/font-awesome/css/font-awesome.min.css',
        //'admin-lte/plugins/ionicons/css/ionicons.min.css',
        //'admin-lte/dist/css/AdminLTE.min.css',
        'admin-lte/dist/css/skins/_all-skins.css',
        //'admin-lte/plugins/iCheck/flat/blue.css',
        //'admin-lte/plugins/morris/morris.css',
        //'admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        //'admin-lte/plugins/datepicker/datepicker3.css',
        //'admin-lte/plugins/daterangepicker/daterangepicker-bs3.css',
        //'admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        //'css/style.css',
        'admin-lte/dist/css/customfroned.css',
    ];
    
        public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public $js = ['admin-lte/dist/js/app.js',
    'admin-lte/dist/js/custom.js' ];
    
public $jsOptions = array(
    'position' => \yii\web\View::POS_HEAD
);
	
	
	

}
