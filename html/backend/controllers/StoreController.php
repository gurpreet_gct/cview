<?php
namespace backend\controllers;
use Yii;
use yii\base\Model;
use common\models\Store;
use common\models\Profile;
use backend\models\StoreSearch;
use backend\models\Storebeacons;
use yii\filters\AccessControl;
use common\models\Categories;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\db\Query;
/**
 * StoreController implements for Store model.
 */
class StoreController extends Controller
{
    public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}
    /**
     * Lists all Store models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StoreSearch();
        $searchParameter=Yii::$app->request->queryParams;
					if((Yii::$app->request->getQueryParam("pid")))
					{$searchModel->pid=Yii::$app->request->getQueryParam("pid");
						unset($searchParameter["pid"]);
					}
					
        
        $dataProvider = $searchModel->search($searchParameter);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    
	}
    /**
     * Displays a single Store model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		
        return $this->render('view', [
            'model' => $this->findModel($id),
					
        ]);
    }
    /**
     * Creates a new Store model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Store();
      
		
       $model->load(Yii::$app->request->post());
	  
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {     
             
             if($model->save()) {
		       	  // no need for validation rule on user_id as you set it yourself
											
								
					if(Yii::$app->request->post("savenew")==1){
						\Yii::$app->getSession()->setFlash('success','Store location has been added successfully.');
						$redirectUrl=['create', 'id' => $model->id,'pid'=>$model->owner];			
					}else{
						\Yii::$app->getSession()->setFlash('success','Store location has been added successfully.');
						$redirectUrl=['view', 'id' => $model->id];			
					}					
					if((Yii::$app->request->getQueryParam("pid"))){
						$redirectUrl['pid']=Yii::$app->request->getQueryParam("pid");
					}
					
		          return $this->redirect($redirectUrl);
		       } 
		      
          
        } else {
            return $this->render('create', [
                'model' => $model,               
                              
            ]);
        }
    }
    /**
     * Updates an existing Store model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		
        $model = $this->findModel($id);         
		
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
	
    		  $model->save(false);					
					if(Yii::$app->request->post("savenew")==1){
						\Yii::$app->getSession()->setFlash('success','Store location has been updated successfully.');
						$redirectUrl=['create','pid'=>$model->owner];			
					}else{
						\Yii::$app->getSession()->setFlash('success','Store location has been updated successfully.');
						$redirectUrl=['view', 'id' => $model->id];			
					}					
					if((Yii::$app->request->getQueryParam("pid"))){
						$redirectUrl['pid']=Yii::$app->request->getQueryParam("pid");
					} 
		    		return $this->redirect($redirectUrl);	       
			  }else {
				                          
             return $this->render('update', [
            'model' => $model,           
			
        ]);
        
        }
    }
    /**
     * Deletes an existing Store model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();        
        Storebeacons::deleteAll('store_id ='.$id);
      //  StoreCategory::deleteAll('store_id ='.$id);
			$redirectUrl=['index'];	
					if((Yii::$app->request->getQueryParam("pid")))
					$redirectUrl['pid']=Yii::$app->request->getQueryParam("pid");
        return $this->redirect($redirectUrl);
    }
   
      /* delete via ajax */   
  public function actionDeletebyajax() 
 
	  {
			$pid = $_POST['id']; 
					
			if($pid) {
			$this->findModel($pid)->delete();
			Storebeacons::deleteAll('store_id ='.$pid);			
			echo $pid; exit;
			}
	 }
    /**
     * Finds the Store model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Store the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        $usertype = Yii::$app->user->identity->user_type;
		$userId = $usertype == 7 ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
		if($usertype == 1){
			$model = Store::findOne($id);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
			
		} else {
			$model = Store::findOne(['id'=>$id,'owner'=>$userId]);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
    }
    
    /**
     * Finds the Store model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Store the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
   
    
    protected function finddelbeaconstoreModel($id)
    {
		       
		if (($beaconstore = Storebeacons::delete()->where(['store_id'=>$id])->all()) === null) { 	
            return true;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionSetstatus()  
	{
		$id = $_POST['id']; 
		$valueid = $_POST['value']; 
		$model = $this->findModel($id);		
		$model->status = $valueid;		
		$model->save(false);
		
       echo $valueid; exit;
		
 }
 public function actionGeoradious(){
	 
		$postData = Yii::$app->request->get('partnerId');
		$geo = Profile::find()->select(['geo_radius','foodEstablishment'])->where('user_id='.$postData)->one();
		if(isset($geo->foodEstablishment)){
			return json_encode(array('geo_radius'=>$geo->geo_radius,'isFood'=>$geo->foodEstablishment)); die;
		}
		exit;
	 }
	
	
}
