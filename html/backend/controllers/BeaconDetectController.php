<?php

namespace backend\controllers;

use Yii;
use common\models\BeaconDetectLog;
use common\models\BeaconDetectLogSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BeaconDetectController implements for Regions model.
 */
class BeaconDetectController extends Controller
{
    public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}

    /**
     * Lists all BeaconDetectLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BeaconDetectLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BeaconDetectLog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

  

   

    /**
     * Deletes an existing BeaconDetectLog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /* delete via ajax */   
	public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];  
		if($pid) {
			$this->findModel($pid)->delete();
			echo $pid; exit;
		}
	}

    /**
     * Finds the BeaconDetectLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BeaconDetectLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = BeaconDetectLog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
