<?php

namespace backend\controllers;

use Yii;
#use yii\filters\AccessControl;
use yii\web\Controller;
#use backend\components\Controller;
use common\models\User;
use yii\helpers\Url;
use yii\db\Query;

/**
 * User controller
 */
 
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public $layout='login';
   
    /* Forget password */
    public function actionForgot()    
	{				
		$model = new User();
		  
		if(isset(Yii::$app->request->post()['User']['email'])) {
					
				$getEmail=Yii::$app->request->post()['User']['email'];
				$getuserdetails= User::findByEmail($getEmail);
				 
				
			if(!empty($getuserdetails)) {
				
                $validtoken = $model->generatePasswordResetToken();             
                $getuserdetails->password_reset_token = $validtoken;
                $getuserdetails->save();
                
            
            if($model->isPasswordResetTokenValid($validtoken))
            {
				
			/* if valid token, then email will be send */            
                
				$subject	= $_SERVER['HTTP_HOST']." : Forgot PIN";
				$email		= $getuserdetails->email;
				$emailData  = ['Name'=> $getuserdetails->fullName,
							   'validtoken'=> $validtoken
							];
							
				$emailsend	=	Yii::$app->mail->compose(['html' => 'passwordReset-mail'],$emailData)
								->setTo($email)
								->setFrom(Yii::$app->params["noReplyEmail"],'No Reply')
								->setSubject($subject)
								->send(); 
				
				if($emailsend){
					
					echo Yii::$app->getSession()->setFlash('success', 'please check your email and reset your PIN');
						 return $this->render('forgot', [
						'model' => $model,
					]);
					
				}
            }else{
				
				Yii::$app->getSession()->setFlash('error', 'your token has been expired, please try again');
				 
				return $this->render('forgot', [
										'model' => $model,
									]);
			}
 
          
           
            
				}else {
			 
				
				 Yii::$app->getSession()->setFlash('error', 'invalid email address, please try again');
				 return $this->render('forgot', [
                'model' => $model,
            ]);	
			
				}		  
	      
		}else {
			  
			   return $this->render('forgot', [
                'model' => $model,
            ]);
			  
		}
        
        
	}
	
	/* Reset password */
	public function actionReset($token){
		
				
		$model = new User();

		if (isset($_REQUEST['app_id']) && $_REQUEST['app_id'] == User::USER_TYPE_BIDDER) {
			$model->scenario = User::SCENARIO_CVIEW_REQUEST;
		} else {
			$model->scenario = User::SCENARIO_WEB_REQUEST;
		}
		
		
		if(isset(Yii::$app->request->post()['User']['password_hash'])) {
					
				$getnewpassword=Yii::$app->request->post()['User']['password_hash'];
				$getuserdetails= User::findByPasswordResetToken($token);
				
				
				
				if(!empty($getuserdetails)) {
					
					$gettoken = $getuserdetails->password_reset_token;
															
					if($gettoken==$token){ 
						
					$newpasswordhash = $model->setPassword($getnewpassword);
					
					$getuserdetails->password_hash= $newpasswordhash;
					
					$getuserdetails->password_reset_token= $model->removePasswordResetToken();
					
					$getuserdetails->save();				
					
						
				    Yii::$app->getSession()->setFlash('success', 'Login with new PIN');
					
					
					return $this->redirect(Url::to(['site/login'], true));
				
										
					}				
				   else {
					
					Yii::$app->getSession()->setFlash('error', 'token not match');
			
					return $this->redirect(Url::to(['user/forgot'], true));
					
				}
				
		}else {
			
			Yii::$app->getSession()->setFlash('error', 'invalid token please try again');
			
			return $this->redirect(Url::to(['user/forgot'], true));
		
			}
		
		}else{
	    return $this->render('reset', [
                'model' => $model,
            ]);
		} 
	}

	/** 
	 *
	 * Reset password for Cview Users
	 *
	 */
	public function actionResetCview($token){
		
				
		$model = new User();

		if (! isset($_REQUEST['app_id']) && ($_REQUEST['app_id'] != User::USER_TYPE_BIDDER)) {
			Yii::$app->getSession()->setFlash('error', 'Invalid request!');
			return $this->redirect(Url::to(['user/forgot'], true));
		}
			

		$model->scenario = User::SCENARIO_CVIEW_REQUEST;
		
		if (isset(Yii::$app->request->post()['User']['password_hash'])) {
					
			$getnewpassword=Yii::$app->request->post()['User']['password_hash'];
			$getuserdetails= User::findByPasswordResetToken($token);
				
			if(!empty($getuserdetails)) {
					
				$gettoken = $getuserdetails->password_reset_token;
														
				if($gettoken==$token){ 
					
					$newpasswordhash = $model->setPassword($getnewpassword);
					$getuserdetails->password_hash= $newpasswordhash;
					$getuserdetails->password_reset_token= $model->removePasswordResetToken();

					$getuserdetails->save();				
						
				    Yii::$app->getSession()->setFlash('success', 'Login with new PIN');
					return $this->redirect(Url::to(['site/login'], true));
							
				} else {
					Yii::$app->getSession()->setFlash('error', 'token not match');
					return $this->redirect(Url::to(['user/forgot'], true));
				}
				
			} else {
				Yii::$app->getSession()->setFlash('error', 'invalid token please try again');
			
				return $this->redirect(Url::to(['user/forgot'], true));
		
			}
		
		} else {
	    	return $this->render('reset-cview', [
                'model' => $model,
                'app_id' => $REQUEST['app_id']
            ]);
		} 
	}
	
	
	public function actionUserlist($q = null, $id = null) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$out = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($q)) {		
		
			$query = new Query;
			$query->select('id, username AS text')
				->from('{{%users}}')
				->where(['like', 'username', $q]);
			if(Yii::$app->user->identity->user_type==User::ROLE_Advertiser){	
				$refCode = Yii::$app->user->identity->user_code;
				$query = $query->andWhere("referralCode='$refCode'");
			}					
			$query = 	$query->limit(20);
			$command = $query->createCommand();
			$data = $command->queryAll();
			$out['results'] = array_values($data);
		}
		elseif ($id > 0) {
			$out['results'] = ['id' => $id, 'text' => self::find($id)->username];
		}
		return $out;
	}
    
    
}
