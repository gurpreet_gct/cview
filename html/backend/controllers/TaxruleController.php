<?php

namespace backend\controllers;

use Yii;
use common\models\Tax;
use common\models\Taxrule;
use common\models\TaxruleItem;
use common\models\TaxruleSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;


/**
 * TaxruleController implements for Taxrule model.
 */
class TaxruleController extends Controller
{
     public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}

    /**
     * Lists all Taxrule models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaxruleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Taxrule model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Taxrule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Taxrule();
        $taxruleItem = new TaxruleItem();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			/* Check if multiple value */
			if(is_array($model->productclass)){
			     $model->productclass = implode(', ',$model->productclass);
			}
			$model->save(false);
			
			/* Check if multiple value of tax Rule */
			if(is_array($model->taxrate)){
					$data_sql = array();
					
						
					foreach($model->taxrate as $key=>$value) {						
					  	array_push($data_sql,array("tax_id"=>$value,"rule_id"=>$model->id,"created_at"=>time(),"updated_at"=>time(),"created_at"=>time(),"created_by"=>Yii::$app->user->id));						 
						}
					
					  if(count($data_sql))
								Yii::$app->db->createCommand()->batchInsert(TaxruleItem::tableName(),array_keys($data_sql[0]), $data_sql)->execute();
						
						
			}
			
			
			
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                
            ]);
        }
    }

    /**
     * Updates an existing Taxrule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			/* Check if multiple value */
			if(is_array($model->productclass)){
			     $model->productclass = implode(', ',$model->productclass);
			}
			$model->save(false);
			TaxruleItem::deleteAll('rule_id ='.$id);	
			/* Check if multiple value of tax Rule */
			if(is_array($model->taxrate)){
					$data_sql = array();
					
						
					foreach($model->taxrate as $key=>$value) {						
					  	array_push($data_sql,array("tax_id"=>$value,"rule_id"=>$model->id,"created_at"=>time(),"updated_at"=>time(),"created_at"=>time(),"created_by"=>Yii::$app->user->id));						 
						}
					
					  if(count($data_sql))
								Yii::$app->db->createCommand()->batchInsert(TaxruleItem::tableName(),array_keys($data_sql[0]), $data_sql)->execute();
						
						
			}
			
						
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Taxrule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /* delete via ajax */   
	public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];  
		if($pid) {
			$this->findModel($pid)->delete();
			echo $pid; exit;
		}
	}
	
	/* Ajax Based country wise tax rule */
	public function actionCountrywisetaxtate(){
	
	
	$countryID = $_POST['id'];  
		if($countryID) {			
			$stateName =  ArrayHelper::map(Tax::find()->where(['country_id' =>$countryID])->all(), 'id', 'id');
			$st = array('data'=> $stateName);
			echo json_encode($st); exit;
		}
	
	}
	
	

    /**
     * Finds the Taxrule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tax the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Taxrule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
