<?php

namespace backend\controllers;

use Yii;
use common\modules\auth\models\AuthItem;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RbacController implements the CRUD actions for AuthItem model.
 */
class RbacController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * user assignment
     */
    public function actionAssigment(){
		/* create assigment*/
		$auth = Yii::$app->authManager;    
		$customer = $auth->createRole('customer');
		$partner = $auth->createRole('partner');
		$admin = $auth->createRole('admin');
		 
		// usually implemented in your Usertype model.
        $auth->assign($admin, 1);
        $auth->assign($partner, 2);
        $auth->assign($customer, 3);
		
	
	}     
    /* create user role */
    public function actionCreate_role()
    {
	   $auth = Yii::$app->authManager;    
     // Author -> index/create/	view
     // Admin -> (Author) and update/delete -> index/create/view/update/delete
		$index = $auth->createPermission('partners/index');
		$create = $auth->createPermission('partners/create');
		$view = $auth->createPermission('partners/view');
		$update = $auth->createPermission('partners/update');
		$delete = $auth->createPermission('partners/delete');
		/* create rule for customer */
		$customer = $auth->createRole('customer');
        $auth->add($customer);
        
		/* create rule for partner */
        $partner = $auth->createRole('partner');
        $auth->add($partner);
        $auth->addChild($partner, $index);
        $auth->addChild($partner, $create);
        $auth->addChild($partner, $view);

        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);        
        $auth->addChild($admin, $partner);
        $auth->addChild($admin, $customer);
        $auth->addChild($admin, $update);
        $auth->addChild($admin, $delete);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
       
    }
         
    public function actionCreate_permission()
    {
        $auth = Yii::$app->authManager;             
        // show all "indexPartner" permission
        $indexPartner = $auth->createPermission('partners/index');
        $indexPartner->description = 'show all partners';
        $auth->add($indexPartner);
        
        // view "viewPost" permission
        $viewPartner = $auth->createPermission('partners/view');
        $viewPartner->description = 'view partner';
        $auth->add($viewPartner);
        
        // delete "deletePartner" permission
        $deletePartner = $auth->createPermission('partners/delete');
        $deletePartner->description = 'delete partner';
        $auth->add($deletePartner);

        // add "createPartner" permission
        $createPartner = $auth->createPermission('partners/create');
        $createPartner->description = 'Create partner';
        $auth->add($createPartner);

        // add "updatePartner" permission
        $updatePartner = $auth->createPermission('partners/update');
        $updatePartner->description = 'Update partner';
        $auth->add($updatePartner);
        
	}
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => AuthItem::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuthItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
