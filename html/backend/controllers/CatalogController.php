<?php

namespace backend\controllers;

use Yii;
use yz\shoppingcart\ShoppingCart;
use common\models\Product;
use backend\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\filters\AccessControl;
use common\models\FoodCategories;
use common\models\FoodcategoryProducts;
use common\models\Userfoodcategory;
use common\models\ProductOption;
use yii\db\Query;
use yii\helpers\ArrayHelper;
/**
 * CatalogController implements for Product model.
 */
class CatalogController extends Controller
{
    /* public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}*/
	
	public function actions()
	{
		return [
			'uploadPhoto' => [
				'class' => 'paras\cropper\actions\UploadAction',
				'url' => Yii::$app->urlManagerFrontEnd->createAbsoluteUrl(['uploads/catalog/']),
				'path' => '@frontend/web/uploads/catalog/',
			]
		];
	}

    /**
     * Lists all Product models data.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Regions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$catTree	=	FoodcategoryProducts::find()->select('category_id,name')
										->joinWith('foodcategories',false)
										->where(['product_id'=>$id])
										->asArray()->all();
				$catTree	=	ArrayHelper::map($catTree,'category_id','name');
		  return $this->render('view', [
            'model' => $this->findModel($id),
			'optionVal' => $this->getOptionVal($id),
			'categoryTree'=>$catTree,
        ]);
    }

    /**
     * Creates a new Regions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $model->scenario = 'create';  
		$FoodcategoryProducts = new FoodcategoryProducts();		
		 if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$cateTreeVal = Yii::$app->request->post('categoryTree');
			$model->category = !empty($cateTreeVal)?implode(',',$cateTreeVal): '0';
			/* upload product image  */
			   
						if ($model->pimage) {
						
						$path = parse_url($model->pimage, PHP_URL_PATH);						
						$model->image=basename($path);
						
						$thumbimage=Yii::$app->params['productPath'].'thumb/'.$model->image;
						//print_r($thumbimage);die;
						Image::getImagine()
						->open($model->pimage)
						->thumbnail(new Box(100,100))
						->save($thumbimage);
					}
					$model->save(false);
				/* save custom option ingredients  */
					$options 		= Yii::$app->request->post('Product');
					if(!empty($options['title'][0]) && !empty($options['oprice'])){			
						$match = array();						
						$i=0;
						$_price = $options['oprice'];
						$isIncluded 	= $options['is_included'];
						foreach($options['title'] as $_product)
						{
							if($_product){
								$data = array();
								$data['product_id'] = $model->id;
								$data['title'] = $_product;
								$data['price'] = $_price[$i]==0 ? 0.00: $_price[$i];
								$data['is_included'] = $isIncluded[$i]==0?0:1;
								$match[] = $data;
							}
							$i++;
						}
							
							$column=['product_id','title','price','is_included'];
							Yii::$app->db->createCommand()->batchInsert(ProductOption::tableName(),$column,$match)->execute();
						
					} //if(!empty($options['title']) && !empty($options['oprice']))
					/*save category items */	
								
					if(!empty($cateTreeVal)){						
						$rows = array();
						foreach($cateTreeVal as $_cateTreeVal){
							$rows[] = [
							'product_id'=>$model->id,
							'category_id' => $_cateTreeVal,
							'status' => 1,
							'created_at' => time(),
							'updated_at' => time()							
							];
						}
						$columnNameArray=['product_id','category_id','status','created_at','updated_at'];
						Yii::$app->db->createCommand()->batchInsert(FoodcategoryProducts::tableName(),$columnNameArray, $rows)->execute();
					}
						
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'categoryTree' => $this->getCategoryTree(0,0),
	     ]);
        }
    }

    /**
     * Updates an existing Regions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$FoodcategoryProducts = new FoodcategoryProducts();
		$pOptionModel = $this->findModelpOption(array('product_id'=>$id));
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {		
			$cateTreeVal = Yii::$app->request->post('categoryTree');
			$model->category = $cateTreeVal!=''?implode(',',$cateTreeVal): '';
			/* chanage product image */						
			if ($model->pimage) {
					
				$uniqueName = $model->sku.substr(md5(date("Ymdhis")),0,15).'.'.$model->pimage;
				
						if(strstr($model->image,".")) {							
							$oldImage = explode(".",$model->image); 
							
						}else { 
							$oldImage[0] = $uniqueName;
						}
						if ($model->pimage) {
								$path = parse_url($model->pimage, PHP_URL_PATH);						
						$model->image=basename($path);
						
						$thumbimage=Yii::$app->params['productPath'].'thumb/'.$model->image;
						//print_r($thumbimage);die;
						Image::getImagine()
						->open($model->pimage)
						->thumbnail(new Box(100,100))
						->save($thumbimage);
					}
        		
        			
          	 }          	
          	 $model->save(false);
			/* save custom option ingredients  */
				$options 		= Yii::$app->request->post('Product');				
				if(!empty($options['title']) && !empty($options['oprice'])){
					$existingOpVal  = $this->getOptionVal($id);					
						$match = array();
						$i=0;
						$_price 		= $options['oprice'];
						$isIncluded 	= $options['is_included'];
						foreach($options['title'] as $_product)
						{						
							if($_product){
								$data = array();
								$data['product_id'] = $id;
								$data['title'] = $_product;
								$data['price'] = $_price[$i];
								$data['is_included'] = $isIncluded[$i]==0?0:1;
								$match[] = $data;
							}
							$i++;
						}					
						$ifdefference = $match==$existingOpVal;							
						if($ifdefference== false) { 
							$ip= 0;
							if(isset($match)){
								ProductOption::deleteAll('product_id ='.$id);
								$column=['product_id','title','price','is_included'];
								Yii::$app->db->createCommand()->batchInsert(ProductOption::tableName(),$column,$match)->execute();
							} //if(($optionTitle) && ($oPrice))
						} //if($ifdefference== true) 
				} //if(!empty($options['title']) && !empty($options['oprice'])){
			/*save categories in database */
			$cateTreeVal = Yii::$app->request->post('categoryTree');
			if(!empty($cateTreeVal)){				
				$rows = array();
				foreach($cateTreeVal as $_cateTreeVal){
					$rows[] = [
						'product_id'=>$model->id,
						'category_id' => $_cateTreeVal,
						'status' => 1,
						'created_at' => time(),
						'updated_at' => time()							
					];
				}
				FoodcategoryProducts::deleteAll('product_id ='.$model->id);
				$columnNameArray =['product_id','category_id','status','created_at','updated_at'];
				Yii::$app->db->createCommand()->batchInsert(FoodcategoryProducts::tableName(),$columnNameArray, $rows)->execute();	
			}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'categoryTree' => $this->getCategoryTree($model->store_id,$model->id),
				'optionVal' => $this->getOptionVal($id),
            ]);
        }
    }
	public function getOptionVal($pId){
		return ProductOption::find()->select(['product_id','title','price','is_included'])->where('product_id='.$pId)->asArray()->all();
		
	}
  protected function findModelpOption($id)
    {
        	if (($pOption = ProductOption::findOne(['product_id'=>$id])) !== null) { 
        	
            return $pOption;
        } else {
            return true;
        }
    }
    /**
     * Deletes an existing Regions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        ProductOption::deleteAll('product_id ='.$id);
        return $this->redirect(['index']);
    }
    
    /* delete via ajax */   
	public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];  
		if($pid) {
			$this->findModel($pid)->delete();
			ProductOption::deleteAll('product_id ='.$pid);
			echo $pid; exit;
		}
	}

    /**
     * Finds the Regions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Regions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/* get list of category by ajax*/
	public function actionFoodcat()
	{
		$storeId = $_POST['id'];  
		return $this->getCategoryTreeajax($storeId);
	}
		/* get list of category */
	protected function getCategoryTreeajax($workorder_id)	
	{	
		$query = new Query;	
		$query	->select([FoodCategories::tableName().'.id as id','name as title',FoodCategories::tableName().'.id as value',  '(lvl+2) as level','lft','ifnull(status,0) as checked','lvl as removelvl'])  
				->from(FoodCategories::tableName())
				->rightJoin(Userfoodcategory::tableName(),Userfoodcategory::tableName().'.category_id ='.FoodCategories::tableName().'.id and '.Userfoodcategory::tableName().'.store_id='.$workorder_id)
				->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC]); 	
		$command = $query->createCommand();
		
		$data = $command->queryAll();
		$requiredResult=array();
		$oldLevel=2;
		$levelKeys=array();
		$ifnul = 0;
		foreach($data as $value)
		{
			if($value['id']=='' || $value['removelvl']>1)
			{
				unset($data[$ifnul]);
			}
			
			$ifnul++;
		}
		
		foreach($data as $key=>$value)
		{
			//$value['path']="root/";
			$key=$value['id'];
            $value['title']=htmlspecialchars_decode($value['title']);
           $value['has_children']=false;
           $value['children']=[];
		   $value['checked']=$value['checked']==1?false:false;
		   
			if($value['level']==2)
			{
		
				$requiredResult[$key]=$value;
				
			}
			else
			{	
				$array_ptr=&$requiredResult;
				
				for($i=2; $i<$value['level']; $i++)
				{
					//$value['path']= $array_ptr[ strval($levelKeys[$i])]['path'];						 
					 $array_ptr[ strval($levelKeys[$i]) ]['has_children']=true;
					 $array_ptr = &$array_ptr[ strval($levelKeys[$i]) ]['children'];
					 
					 
				}
			//	$value['path']=$value['path']."/".$value['name'];
				$array_ptr[$key]=$value;
			//print_r($requiredResult);die;
			}            
    
			$levelKeys[$value['level']]=$value['id'];			
		}	
		
		$this->recursiveRemoval($requiredResult,'children');
      $requiredResult=is_array($requiredResult)?array_values($requiredResult):$requiredResult;
	  	$data=array('id'=>0,'value'=>0,'title'=>'Root','level'=>1,'has_children'=>true,'children'=>$requiredResult);
		return (json_encode($data));

	}
/* get list of ctaegory */
	protected function getCategoryTree($storeId=NULL,$productId)	
	{	
		$userType = Yii::$app->user->identity->user_type;
		if($userType==2 || $userType==7){
			$storeId =Yii::$app->user->identity->id;
		}
		$query = new Query;	
		$query	->select([FoodCategories::tableName().'.id as id',Userfoodcategory::tableName().'.id as selected','name as title',FoodCategories::tableName().'.id as value',  '(lvl+1) as level','lft','ifnull(tbl_foodcategory_products.status,0) as checked','lvl as removelvl'])  
				->from(FoodCategories::tableName())
				->leftJoin(FoodcategoryProducts::tableName(),FoodcategoryProducts::tableName().'.category_id ='.FoodCategories::tableName().'.id and '.FoodcategoryProducts::tableName().'.product_id='.$productId)
				->leftJoin(Userfoodcategory::tableName(),Userfoodcategory::tableName().'.category_id ='.FoodCategories::tableName().'.id and '.Userfoodcategory::tableName().'.store_id='.$storeId)
				->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC]); 	
		$command = $query->createCommand();		
		$data = $command->queryAll();
		$ifnul = 0;
		foreach($data as $value)
		{
			if($value['selected']=='' || $value['id']=='' || $value['removelvl']>1)
			{
				unset($data[$ifnul]);
			}
			$ifnul++;
		}
		$requiredResult=array();
		$oldLevel=1;
		$levelKeys=array();
	
		foreach($data as $key=>$value)
		{
			//$value['path']="root/";
			$key=$value['id'];
            $value['title']=htmlspecialchars_decode($value['title']);
           $value['has_children']=false;
           $value['children']=[];
		   $value['checked']=$value['checked']==1?true:false;
		  	if($value['level']==1)
			{
		
				$requiredResult[$key]=$value;
				
			}
			else
			{	
				$array_ptr=&$requiredResult;
				
				for($i=1; $i<$value['level']; $i++)
				{
					if($value['selected']>0){
					 $array_ptr[ strval($levelKeys[$i]) ]['has_children']=true;
					 $array_ptr = &$array_ptr[ strval($levelKeys[$i]) ]['children'];
					}
					 
				}
				if($value['selected']>0)
				$array_ptr[$key]=$value;
			}            
    
			$levelKeys[$value['level']]=$value['id'];			
		}	
		
		$this->recursiveRemoval($requiredResult,'children');
      $requiredResult=is_array($requiredResult)?array_values($requiredResult):$requiredResult;
	  	$data=array('id'=>0,'value'=>0,'title'=>'Root','level'=>1,'has_children'=>true,'children'=>$requiredResult);
		return (json_encode($data));

	}
	
			 public $i=0;  
	protected function recursiveRemoval(&$array, $val)
	{
		if(is_array($array))
		{
			foreach($array as $key=>&$arrayElement)
			{
				if(is_array($arrayElement))
				{
					if($key===$val)
					{ 
				$this->i++;
				
				 if($this->i==2)
				 {
			 }
				$arrayElement=array_values($arrayElement);

				}
					$this->recursiveRemoval($arrayElement, $val);
				}
				else
				{
					if($key == $val)
					{
						
					}
				}
			}
		}
	}
	
}
