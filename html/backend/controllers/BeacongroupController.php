<?php

namespace backend\controllers;

use Yii;
use backend\models\Beacongroup;
use yii\filters\AccessControl;
use backend\models\BeacongroupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;


/**
 * BeaconGroupController implements for BeaconGroup model.
 */
class BeacongroupController extends Controller
{
    public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}

    /**
     * Lists all BeaconGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BeacongroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BeaconGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BeaconGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Beacongroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BeaconGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		// && $model->save()		
        if ($model->load(Yii::$app->request->post())) {
			$post = Yii::$app->request->post();
			if(isset($post['Beacongroup']['type']) && $post['Beacongroup']['type']==0 || 
				isset($post['Beacongroup']['type']) && $post['Beacongroup']['type']==1 ||
				$post['Beacongroup']['type']>=4){
				
				$model->group_name 	= isset($post['Beacongroup']['group_name'])?$post['Beacongroup']['group_name']:'';
				$model->status 		= isset($post['Beacongroup']['status'])?$post['Beacongroup']['status']:'';
				$model->grandparent_id = '';
				$model->parent_id 	= '';
				$model->city = '';
				$model->type = isset($post['Beacongroup']['type'])?$post['Beacongroup']['type']:'';
				$model->save(false);
			}else{
				$model->save();
			}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BeaconGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /* delete via ajax */   
	public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];		
		if($pid) {
			$this->findModel($pid)->delete();
			echo $pid; exit;
		}
	}
	
	
	/* get country according state */
	public function actionCountrywisestate()  
	{
		$countryID = $_POST['id'];  
		if($countryID) {
			
			 $stateName =   ArrayHelper::map(Beacongroup::find()->Where(['type' =>2, 'grandparent_id' => $countryID ])->all(), 'id', 'group_name');
			$st = array('data'=> $stateName);
			echo json_encode($st); exit;
			
		}
	}


    /**
     * Finds the BeaconGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BeaconGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Beacongroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
