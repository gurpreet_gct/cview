<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use backend\models\Usermanagement;
use backend\models\ChangePin;
use app\models\UsersSearch;
use yii\data\ActiveDataProvider;
use backend\components\Controller;

use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\web\UploadedFile;
use yii\db\Query;

#use yii\data\Pagination;


/**
 * UsersController implements for Users model.
 */
class AdministratorsController extends Controller
{
	public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	} 
	
	public function actions()
	{
		return [
			'uploadPhoto' => [
				'class' => 'paras\cropper\actions\UploadAction',
				'url' => Yii::$app->urlManagerFrontEnd->createAbsoluteUrl(['profile']),
				'path' => '@frontend/web/profile',
			]
		];
	}
    /**
     * Lists all Users models.
     * @return mixed
     */
   //$postid= $model->input->get('id');
				


	 
    public function actionIndex()
    {
        $searchModel = new UsersSearch();        
        
        $dataProvider = $searchModel->searchadmin(Yii::$app->request->queryParams);
        
      
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
				
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usermanagement();
        $model->scenario = 'create';
		$model->imageFile = UploadedFile::getInstance($model, 'imageFile');
		
		if($model->load(Yii::$app->request->post()) && $model->validate()){				
				
       		$model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);       		 
			$model->user_type = 1; 
								
				/* save image in user model */
				if ($model->imageFile) {
						
						$path = parse_url($model->imageFile, PHP_URL_PATH);						
						$model->image=basename($path);
						      			     		
					}
        			/*if ($model->imageFile) {
						$uniqueimage = $model->username.substr(md5(date("Ymdhis")),0,10); 
						$imageName = $uniqueimage;
						$model->image = $imageName.'.'.$model->imageFile->extension;       			     		
						$imagePath =str_replace("/",DIRECTORY_SEPARATOR,Yii::getAlias('@frontend')."/" .'web/profile'."/".$model->image);
						$model->imageFile->saveAs($imagePath);
						
						Image::getImagine()
							->open($imagePath)
							->thumbnail(new Box(160,160))
							->save($imagePath);
							
					}*/
				$model->save(false);
				
				/* auto assign rbac auth role */				
				Yii::$app->Permission->setPermission('Admin',$model->id);
     
				$sendmail = $model->Sendconfirmation(); 
				if($sendmail==1 ) { 
					Yii::$app->getSession()->setFlash('success','Check Your email!');
					return $this->redirect(['view', 'id' => $model->id]);
				}else{
					Yii::$app->getSession()->setFlash('warning','Failed, contact Admin!');
					}
		}else{
			
            return $this->render('create', [
                'model' => $model,
            ]);
			
        }
        
    }
    
    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		
         $model = $this->findModel($id);
			        
        	if ($model->load(Yii::$app->request->post()) && $model->validate()) {			
				
				$uniqueimage = $model->username.substr(md5(date("Ymdhis")),0,15);           
 			/* get old file name */
 			
				if(isset($model->image) && $model->image !='') {
					
					$oldImageName = explode(".",$model->image); 
				
				}else {
					
					$oldImageName[0] = $uniqueimage;
					
				}
				
				$model->dob 		= strtotime($model->dob);
				$model->imageFile 	= UploadedFile::getInstance($model, 'imageFile');
				
				/*save image in user model */ 
        	  
				/*if ($model->imageFile) {
					
					$imageName 		= 	$oldImageName[0];
					$model->image	=	$oldImageName[0].'.'.$model->imageFile->extension;
					$imagePath		=	str_replace("/",DIRECTORY_SEPARATOR,Yii::getAlias('@frontend')."/" .'web/profile'."/".$model->image);
	         
					$model->imageFile->saveAs($imagePath);   	
	          	
			    }*/
			    if ($model->imageFile) {	$path = parse_url($model->imageFile, PHP_URL_PATH);						
						$model->image=basename($path);
						
						
					} 
				
			   $model->save(false);
			
			 Yii::$app->getSession()->setFlash('success', "Your profile has been updated successfully"); 
             return $this->redirect(['view', 'id' => $model->id]);			  
			 
        
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

      /* change partner password  */
	public function actionChangepassword($id) {	
			
		$model= new ChangePin; 
		$partner = Usermanagement::findOne($id);  
		

        if ($model->load(Yii::$app->getRequest()->getBodyParams()) && $model->validate() && $model->newPassword!='' && $partner->password_hash!='') {        	
			
			 $model->newPassword = Yii::$app->security->generatePasswordHash($model->newPassword);
			 $partner->password_hash =  $model->newPassword;			 
			 $partner->save(false);
			  Yii::$app->getSession()->setFlash('success', "Your new PIN has been updated");       
		     return $this->redirect(['view', 'id' => $partner->id]);
	
		
		}else{
		 return $this->render('changepassword', [
                'model' => $model,
            ]);	
		
		}	
		 
	}

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

       return $this->redirect(['index']);
    }
    
 
	/* delete via ajax */   
	public function actionDeletebyajax() 
 
		{
			$pid = $_POST['id'];  
			if($pid) {
				$this->findModel($pid)->delete();
				echo $pid; exit;
			}
		}
    
   
  

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Usermanagement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
