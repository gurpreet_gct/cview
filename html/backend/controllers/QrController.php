<?php

namespace backend\controllers;

use Yii;
use common\models\Qr;
use common\models\QrSearch;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use dosamigos\qrcode\QrCode;
/**
 * QrController implements for Qr model.
 */
class QrController extends Controller
{
    public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}

    /**
     * Lists all Qr models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QrSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
   
	
}

    /**
     * Displays a single Regions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionViewpopup($id)
    {
		 $this->layout='qrcode';
        return $this->render('viewpopup', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Regions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		 
        $model = new Qr();
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
			if($model->type)				
				Yii::$app->barcode->drawBars($model->data,$model->qrkey.".png");		
				else
				$data=QrCode::png($model->data,Yii::$app->basePath.'/web/qrcodes/'.$model->qrkey.".png");		
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    /* create iframe code  */
    
    public function actionCreateframe()
    {
		$this->layout='qrcode';
        $model = new Qr();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		
			if($model->type)				
				Yii::$app->barcode->drawBars($model->data,$model->qrkey.".png");		
				else
				$data=QrCode::png($model->data,Yii::$app->basePath.'/web/qrcodes/'.$model->qrkey.".png");		
            return $this->redirect(['viewpopup', 'id' => $model->id]);
        } else {
            return $this->render('createframe', [
                'model' => $model,
            ]);
        }
    }
    
    
   

    /**
     * Updates an existing Regions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
				if($model->type)				
				Yii::$app->barcode->drawBars($model->data,$model->qrkey.".png");		
				else
				$data=QrCode::png($model->data,Yii::$app->basePath.'/web/qrcodes/'.$model->qrkey.".png");		
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    /* update iframe code */

	public function actionUpdateframe($id)
    {   
		$this->layout='qrcode';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
				if($model->type)				
				Yii::$app->barcode->drawBars($model->data,$model->qrkey.".png");		
				else
				$data=QrCode::png($model->data,Yii::$app->basePath.'/web/qrcodes/'.$model->qrkey.".png");		
            return $this->redirect(['viewpopup', 'id' => $model->id]);
        } else {
            return $this->render('updateframe', [
                'model' => $model,
            ]);
        }
    }
	
    /**
     * Deletes an existing Regions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
     
     /* delete frame*/
     
     public function actionDeleteframe($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['createframe']);
    }
    
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /* delete via ajax */   
	public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];  
		if($pid) {
			$this->findModel($pid)->delete();
			echo $pid; exit;
		}
	}

    /**
     * Finds the Regions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Regions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Qr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
