<?php

namespace backend\controllers;

use Yii;
use common\models\Bankdetail;
use common\models\BankdetailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * BankdetailController implements for Bankdetail model.
 */
class BankdetailController extends Controller
{
    public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}

    /**
     * Lists all Bankdetail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BankdetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bankdetail model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bankdetail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Bankdetail();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
				$user =  \common\models\User::findOne(['id'=>$model->partner_id]);
				if($user->promise_uid){
					$model->user_id=$user->promise_uid;
				}else{
					$result=Yii::$app->promisePay->createUser($user);
					if(isset($result->id)){	
						$user->promise_uid=$result->id;
						$user->save(false);
						$model->user_id=$result->id;
					}
					
				}
				$result=Yii::$app->promisePay->createBankAccount($model);	
				if(is_string($result)){
					$whatIWant = substr($result, strpos($result, "Error Message") + 15);
					$property = explode(':',$whatIWant);
					$model->addError('id',$whatIWant);
					return $this->render('update',[
					'model' => $model,
					]);
				}else{
					$model->promise_acid = $result['id'];
					$model->save(false);
				}			
			if(!empty($_GET['payid'])){
				 return $this->redirect(['subscription-charges/paynow','id'=>$_GET['payid']]);
		   }	
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Bankdetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {			
			
			if($model->promise_acid==''){
				$user =  \common\models\User::findOne(['id'=>$model->partner_id]);
				if($user->promise_uid){
					$model->user_id=$user->promise_uid;
				}else{
					$result=Yii::$app->promisePay->createUser($user);
					if(isset($result->id)){	
						$user->promise_uid=$result->id;
						$user->save(false);
						$model->user_id=$result->id;
					}
					
				}
				$result=Yii::$app->promisePay->createBankAccount($model);	
				if(is_string($result)){
					$whatIWant = substr($result, strpos($result, "Error Message") + 15);
					$property = explode(':',$whatIWant);
					$model->addError('id',$whatIWant);
					return $this->render('update',[
					'model' => $model,
					]);
				}else{
					$model->promise_acid = $result['id'];
				}
			}
			
			$model->save();		
			/*if($model->disbursement==1){
					$result=Yii::$app->promisePay->setDisbursementAccount($model->user_id,$model->promise_acid);	
					var_dump($result); die;
			} */
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Bankdetail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /* delete via ajax */   
	public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];  
		if($pid) {
			$model = $this->findModel($pid);
			Yii::$app->promisePay->removeBankAcc($model->promise_acid);
			$model->delete();
			echo $pid; exit;
		}
	}

    /**
     * Finds the Bankdetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bankdetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
            
        $usertype = Yii::$app->user->identity->user_type;
		$userId = $usertype == 7 ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
		if($usertype == 1){
			$model = Bankdetail::findOne($id);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
			
		} else {
			$model = Bankdetail::findOne(['id'=>$id,'partner_id'=>$userId]);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
    }
}
