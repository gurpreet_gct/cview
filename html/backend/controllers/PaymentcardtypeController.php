<?php

namespace backend\controllers;

use Yii;
use common\models\Paymentcardtype;
use common\models\PaymentcardtypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PaymentcardtypeController implements for Paymentcardtype model.
 */
class PaymentcardtypeController extends Controller
{
    public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}


    /**
     * Lists all Paymentcardtype models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentcardtypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Paymentcardtype model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Paymentcardtype model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    
    {
        $model = new Paymentcardtype();
         $model->scenario = 'create';
		if($model->load(Yii::$app->request->post()) && ($model->imageFile = UploadedFile::getInstance($model,'imageFile'))	&& $model->save()	)
		
		
        {
			
			   if(isset($model->imageFile)) {
			//echo $model->imageFile = UploadedFile::getInstance($model, 'imageFile'); die();
			
				$uniqueimage = substr(md5(date("Ymdhis")),0,15); 
				/* save image in user model */
        			if ($model->imageFile) {
  	     	 		$imageName = $uniqueimage;
        			$model->icon = $imageName.'.'.$model->imageFile->extension;
       			
        			$model->imageFile->saveAs(Yii::$app->params['icons'].'/'.$imageName.'.'.$model->imageFile->extension);
        			
        			  Image::getImagine()
					->open(Yii::$app->params['icons'].'/'.$model->icon)
					->thumbnail(new Box(300,300))
						->save(Yii::$app->params['icons'].'/'.$model->icon	); 
						
          	 }
		 }
		
          $model->save(false); 
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Paymentcardtype model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
        	  /*save image in user model */ 
        	  		  $uniqueimage = substr(md5(date("Ymdhis")),0,15); 
 			/* get old file name */
 			
 			if(isset($model->icon) && $model->icon !='') {				
			   $oldImageName = explode("icons/",$model->icon); 
			   $oldImageName = explode(".",$oldImageName[1]); 
			
			}else { 
				$oldImageName[0] = $uniqueimage;
			}
        	   $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
	          if($model->imageFile) {
			   
        	 
	          	$imageName = $oldImageName[0];
	          
	            $model->icon= $imageName.'.'.$model->imageFile->extension;
	           
	           
	            $model->imageFile->saveAs(Yii::$app->params['icons'].'/'.$imageName.'.'.$model->imageFile->extension);
	          	 /* Resize Image */
	          	 Image::getImagine()
					->open(Yii::$app->params['icons'].'/'.$model->icon)
					->thumbnail(new Box(300,300))
						->save(Yii::$app->params['icons'].'/'.$model->icon	); 
			    } 
			    if(strstr($model->icon,'http')){
					$icon =  explode("icons/",$model->icon); 
					$model->icon = $icon[1]; 
				}
			 
			     $model->save(false);
			    	
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	
	
	/*Delete by ajax */
	
	public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];  
		if($pid) {
			$this->findModel($pid)->delete();
			echo $pid; exit;
		}
	}
	
    /**
     * Deletes an existing Paymentcardtype model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Paymentcardtype model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Paymentcardtype the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Paymentcardtype::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
