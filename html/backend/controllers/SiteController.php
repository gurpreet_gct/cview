<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
#use yii\web\Controller;
use backend\components\Controller;
use common\models\LoginForm;
use common\models\LoginLogs;
use common\models\Usertype;
use yii\filters\VerbFilter;
use yii\web\Session;
use common\models\User;
use common\models\Workorders;
use common\models\Beaconmanager;
use common\models\Pass;
use common\models\Orders;
use common\models\Loyaltypoint;
use common\models\OrderLoyalty;
use common\models\UserLoyalty;
use common\models\Transaction;
use common\models\PaymentMethod;
use common\models\UserPsa;
use common\models\BeaconDetectLog;
use common\models\PartnerTransactionsFees;

/**
* Site controller
*/

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public $layout='login';
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','activeuser','newuser','userdevice','membership','screenview','usersession','totalcampaigns','beaconsonline','activeclients','passessent','get-dashboard-report','dashboard-user','report-dashboard','dashboard-deal','dashboard-beacon','dashboard-campaign','dashboard-loyality','dashboard-shoppping','trans-fees','subs-charges'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

	//21 Not in use
    public function actionReportDashboard()
    {

        $this->layout='main';

		$UserDatas = $this->actionNewuser();
		$userDevices = $this->actionUserdevice();
		$activeUser = $this->actionActiveuser();
		$membershipUsers = $this->actionMembership();
		$actionScreenview = $this->actionScreenview();
		$userSession = $this->actionUsersession();
		$totalCampaigns = $this->actionTotalcampaigns();
		$beaconsOnline = $this->actionBeaconsonline();
		$actionActiveclients = $this->actionActiveclients();
		$actionPassessent = $this->actionPassessent();

        return $this->render('report-dashboard',[
									'UserDatas' => $UserDatas,
									'userDevices' => $userDevices,
									'userSession' => $userSession,
									'activeUser' => $activeUser,
									'membershipUsers' => $membershipUsers,
									'actionScreenview' => $actionScreenview,
									'totalCampaigns' => $totalCampaigns,
									'beaconsOnline' => $beaconsOnline,
									'actionActiveclients' => $actionActiveclients,
									'actionPassessent' => $actionPassessent,
							]);


    }

	/*
	* Dashboard report Data
	*
	*/
	public function actionIndex(){

        $this->layout = 'main';
		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){
			// user not administrator
			$userField 	= ['userType'=>$userType,'userId'=>$userId];
		}else{
			$userField 	= [];
		}

		$workorder		= new Workorders();
		$totalCampaigns = $this->actionTotalcampaigns($userField);
		$monthlyQuota 	= $workorder->monthlyQuotaPass($userField);

        return $this->render('index',['totalCampaigns'=>$totalCampaigns,'monthlyQuota'=>$monthlyQuota]);

    }

	/*
	* Login action for users
	*
	*/

    public function actionLogin()
    {

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			 $userModel = Yii::$app->user->identity;
			 if($userModel->user_code==''){
				$userModel->user_code = Yii::t('app', 'AW-').$userModel->username; 
			 }
			 $userModel->save();
            return $this->goBack();

        }else{

            return $this->render('login', [
                'model' => $model,
            ]);

        }
    }

	/*
	* Logout action for users
	*
	*/

    public function actionLogout()
    {

        try{
                $loginlog=LoginLogs::findOne(['sessionid'=>Yii::$app->session->id,'user_id'=>Yii::$app->user->id]);
                if($loginlog){
                    $loginlog->logout_at=time();
                    $loginlog->save();
                }
            }
            catch(Exception $e){}
        Yii::$app->user->logout();

        return $this->goHome();

    }

/* New user to show on chart */

	public function actionNewuser(){

		$totalUser 	= 	User::find()->where('user_type = 3')->count();
		$hour 		= 	12;
		$today  	= 	strtotime($hour . ':00:00');
		$yesterday  = 	strtotime('-1 day', $today);
		$threeday  	= 	strtotime('-2 day', $today);
		$fourdayOld = 	strtotime('-3 day', $today);
		$fivedayOld = 	strtotime('-4 day', $today);
		$sixdayOld 	= 	strtotime('-5 day', $today);
		$OneWeek 	= 	strtotime('-6 day', $today);
		$dayEight 	= 	strtotime('-7 day', $today);
		$OneMonth   = 	strtotime('-30 day', $today);
		$day1	    = 	User::find()->select('id')->where('created_at > :created_at', [':created_at' => $yesterday])->andWhere('user_type = :user_type', [':user_type' => 3])->count();
		$dayWise	= 	User::find()->select('created_at,firstName,fullName')->where('user_type = 3 ')->asArray()->all();
		$PerWeek	= 	User::find()->select('id')->where('created_at > :created_at', [':created_at' => $OneWeek])
						->andWhere('user_type = :utype', [':utype' => '3'])->count();
		$PerMonth	= 	User::find()->select('id')->where('created_at > :created_at', [':created_at' => $OneMonth])->andWhere('user_type = :utype', [':utype' => '3'])->count();
		$day2 = array(); $day3 = array(); $day4 = array(); $day5 = array(); $day6 = array(); $day7 = array();
		foreach($dayWise as $data)
		{
			if(date('y-m-d',$yesterday) == date('y-m-d',$data['created_at']))
			{
				$day2[] = $data['created_at'];
			}
			else if(date('y-m-d',$threeday) == date('y-m-d',$data['created_at']))
			{
				$day3[] = $data['created_at'];
			}
			else if(date('y-m-d',$fourdayOld) == date('y-m-d',$data['created_at']))
			{
				$day4[] = $data['created_at'];
			}
			else if(date('y-m-d',$fivedayOld) == date('y-m-d',$data['created_at']))
			{
				$day5[] = $data['created_at'];
			}
			else if(date('y-m-d',$sixdayOld) == date('y-m-d',$data['created_at']))
			{
				$day6[] = $data['created_at'];
			}
			else if(date('y-m-d',$OneWeek) == date('y-m-d',$data['created_at']))
			{
				$day7[] = $data['created_at'];
			}

		}
		 $userCounts = array('perDay'=>$day1,'day2'=>count($day2),'day3'=>count($day3),'day4'=>count($day4),'day5'=>count($day5),'day6'=>count($day6),'day7'=>count($day7),
				 'perWeek'=>$PerWeek,'perMonth'=>$PerMonth,'totalUser'=>$totalUser);
			return $userCounts;
	}

	/* Active user to show on chart */

	public function actionActiveuser()
	{
			$totalUser 	= 	User::find()->where('user_type = 3')->count();
			$hour 		= 	12;
			$today  	= 	strtotime($hour . ':00:00');
			$yesterday  = 	strtotime('-1 day', $today);
			$threeday  	= 	strtotime('-2 day', $today);
			$fourdayOld = 	strtotime('-3 day', $today);
			$fivedayOld = 	strtotime('-4 day', $today);
			$sixdayOld 	= 	strtotime('-5 day', $today);
			$OneWeek 	= 	strtotime('-6 day', $today);
			$dayEight 	= 	strtotime('-7 day', $today);
			$OneMonth   = 	strtotime('-30 day', $today);
			$day1	    = 	User::find()->where('lastLogin > :created_at', [':created_at' => $yesterday])
								->andWhere('user_type = 3')->count();
			$dayWise	= 	User::find()->select('created_at,firstName,fullName')
								->where('user_type = 3')->asArray()->all();
			$PerWeek	= 	User::find()->select('id')->where('lastLogin > :created_at', [':created_at' => $OneWeek])
										->andWhere('user_type = 3')->count();
			$PerMonth	= 	User::find()->select('id')->where('lastLogin > :created_at', [':created_at' => $OneMonth])
												->where('user_type = 3')->count();
			$day2 = array(); $day3 = array(); $day4 = array(); $day5 = array(); $day6 = array(); $day7 = array();
			foreach($dayWise as $data)
			{
				if(date('y-m-d',$yesterday) == date('y-m-d',$data['created_at']))
				{
					$day2[] = $data['created_at'];
				}
				else if(date('y-m-d',$threeday) == date('y-m-d',$data['created_at']))
				{
					$day3[] = $data['created_at'];
				}
				else if(date('y-m-d',$fourdayOld) == date('y-m-d',$data['created_at']))
				{
					$day4[] = $data['created_at'];
				}
				else if(date('y-m-d',$fivedayOld) == date('y-m-d',$data['created_at']))
				{
					$day5[] = $data['created_at'];
				}
				else if(date('y-m-d',$sixdayOld) == date('y-m-d',$data['created_at']))
				{
					$day6[] = $data['created_at'];
				}
				else if(date('y-m-d',$OneWeek) == date('y-m-d',$data['created_at']))
				{
					$day7[] = $data['created_at'];
				}

			}
			 $userActive = array('perDay'=>$day1,'day2'=>count($day2),'day3'=>count($day3),'day4'=>count($day4),'day5'=>count($day5),'day6'=>count($day6),'day7'=>count($day7),
					 'perWeek'=>$PerWeek,'perMonth'=>$PerMonth,'totalUser'=>$totalUser);

			return $userActive;

	}

	/* Access website on devices  */
	public function actionUserdevice()
	{
		 $web 		= 	User::find()->select('id')->where('device = :web', [':web' => 'web'])
									->andWhere('user_type = :utype', [':utype' => '3'])->count();
		 $android   = 	User::find()->select('id')->where('device = :web', [':web' => 'android'])
									->andWhere('user_type = :utype', [':utype' => '3'])->count();
		 $ios 		= 	User::find()->select('id')->where('device = :web', [':web' => 'ios'])
									->andWhere('user_type = :utype', [':utype' => '3'])->count();
		 $userDevices = array('web'=>$web,'android'=>$android,'ios'=>$ios);
		return $userDevices;
	}

	/* Membership Segmentation */

	public function actionMembership(){
		$maleUser = User::find()->select('id,firstName,dob')->where('sex = :sex',[':sex'=>'1'])
									->andWhere('user_type = 3')->all();
		$femaleUser = User::find()->select('id,firstName,dob')->where('sex = :sex',[':sex'=>'0'])
										->andWhere('user_type = 3')->all();
			$userGroup1 = array(); $userGroup2 = array(); $userGroup3 = array(); $userGroup4 = array(); $userGroup5 = array(); $userGroup6 = array(); $userGroup7 = array(); $userGroup8 = array();
		 foreach($maleUser as $male){
			 $userAge = date_diff(date_create($male->dob), date_create('today'))->y;
			if($userAge >= 15 && $userAge <= 18)
			{
				$userGroup1[] = $male->id;
			}
			else if($userAge >= 19 && $userAge <= 25)
			{
				$userGroup2[] = $male->id;
			}
			else if($userAge >= 26 && $userAge <= 30)
			{
				$userGroup3[] = $male->id;
			}
			else if($userAge >= 31 && $userAge <= 35)
			{
				$userGroup4[] = $male->id;
			}
			else if($userAge >= 36 && $userAge <= 40)
			{
				$userGroup5[] = $male->id;
			}
			else if($userAge >= 41 && $userAge <= 45)
			{
				$userGroup6[] = $male->id;
			}
			else if($userAge >= 46 && $userAge <= 60)
			{
				$userGroup7[] = $male->id;
			}
		else if($userAge >60)
			{
				$userGroup8[] = $male->id;
			}

		}
		 $userGroup9 = array(); $userGroup10 = array(); $userGroup11 = array(); $userGroup12 = array(); $userGroup13 = array(); $userGroup14 = array(); $userGroup15 = array(); $userGroup16 = array();
		 foreach($femaleUser as $_femaleUser){
			 $femaleUserAge = date_diff(date_create($_femaleUser->dob), date_create('today'))->y;
			if($femaleUserAge >= 15 && $femaleUserAge <= 18)
			{
				$userGroup9[] = $_femaleUser->id;
			}
			else if($femaleUserAge >= 19 && $femaleUserAge <= 25)
			{
				$userGroup10[] = $_femaleUser->id;
			}
			else if($femaleUserAge >= 26 && $femaleUserAge <= 30)
			{
				$userGroup11[] = $_femaleUser->id;
			}
			else if($femaleUserAge >= 31 && $femaleUserAge <= 35)
			{
				$userGroup12[] = $_femaleUser->id;
			}
			else if($femaleUserAge >= 36 && $femaleUserAge <= 40)
			{
				$userGroup13[] = $_femaleUser->id;
			}
			else if($femaleUserAge >= 41 && $femaleUserAge <= 45)
			{
				$userGroup14[] = $_femaleUser->id;
			}
			else if($femaleUserAge >= 46 && $femaleUserAge <= 60)
			{
				$userGroup15[] = $_femaleUser->id;
			}
			else if($femaleUserAge >60)
			{
				$userGroup16[] = $_femaleUser->id;
			}

		}

	 $userAges = array(count($userGroup1),count($userGroup2),count($userGroup3),count($userGroup4),count($userGroup5),count($userGroup6),count($userGroup7),count($userGroup8),count($userGroup9),count($userGroup10),count($userGroup11),count($userGroup12),count($userGroup13),count($userGroup14),count($userGroup15),count($userGroup16));
				return $userAges;
	}

	/* Screen Views(shows the variety of different customers using your app)    */

	public function actionScreenview()
	{
		$screenSize1 = LoginLogs::find()->select('resolution')->joinWith('user',false,'INNER JOIN')
						->where('resolution <= :web', [':web' => '320'])->andWhere('user_type = 3')->count();
		$screenSize2 = LoginLogs::find()->select('resolution')->joinWith('user',false,'INNER JOIN')
						->where('resolution > :web', [':web' => '320'])->andWhere('user_type = 3')
						->andWhere('resolution <= :size', [':size' => '640'])->count();
		$screenSize3 = LoginLogs::find()->select('resolution')->joinWith('user',false,'INNER JOIN')
						->where('resolution > :web', [':web' => '640'])->andWhere('user_type = 3')
						->andWhere('resolution <= :size', [':size' => '1024'])->count();
		$screenSize4 = LoginLogs::find()->select('resolution')->joinWith('user',false,'INNER JOIN')
						->where('resolution > :web', [':web' => '1024'])->andWhere('user_type = 3')->count();
		$deviceSize = array('device1'=>$screenSize1,'device2'=>$screenSize2,'device3'=>$screenSize3,'device4'=>$screenSize4);
		return $deviceSize;
	}

	/* Sessions(shows number of times users have opened the app and browsed)  */

    public function actionUsersession(){

		$hour 		= 	12;
		$today  	= 	strtotime($hour . ':00:00');
		$yesterday  = 	strtotime('-1 day', $today);
		$threeday  	= 	strtotime('-2 day', $today);
		$fourdayOld = 	strtotime('-3 day', $today);
		$fivedayOld = 	strtotime('-4 day', $today);
		$sixdayOld 	= 	strtotime('-5 day', $today);
		$OneWeek 	= 	strtotime('-6 day', $today);
		$UserDay_1 = LoginLogs::find()->select('login_at')->joinWith('user',false,'INNER JOIN')->where('login_at > "'.$OneWeek.'"')
															->andWhere('user_type = 3')->all();

			$day1 = array();$day2 = array(); $day3 = array(); $day4 = array(); $day5 = array(); $day6 = array(); $day7 = array();

		foreach($UserDay_1 as $data)
		{
			//echo date('y-m-d',$data['login_at']).'<br>';
			if(date('y-m-d',$today) == date('y-m-d',$data['login_at']))
			{
				$day1[] = $data['login_at'];
			}
			if(date('y-m-d',$yesterday) == date('y-m-d',$data['login_at']))
			{
				$day2[] = $data['login_at'];
			}
			else if(date('y-m-d',$threeday) == date('y-m-d',$data['login_at']))
			{
				$day3[] = $data['login_at'];
			}
			else if(date('y-m-d',$fourdayOld) == date('y-m-d',$data['login_at']))
			{
				$day4[] = $data['login_at'];
			}
			else if(date('y-m-d',$fivedayOld) == date('y-m-d',$data['login_at']))
			{
				$day5[] = $data['login_at'];
			}
			else if(date('y-m-d',$sixdayOld) == date('y-m-d',$data['login_at']))
			{
				$day6[] = $data['login_at'];
			}
			else if(date('y-m-d',$OneWeek) == date('y-m-d',$data['login_at']))
			{
				$day7[] = $data['login_at'];
			}

		}
		 $userTottalSession = array('day1'=>count($day1),'day2'=>count($day2),'day3'=>count($day3),'day4'=>count($day4),'day5'=>count($day5),'day6'=>count($day6),'day7'=>count($day7));
		return $userTottalSession;
	}

	/*
	* Total running campaign
	*/
	public function actionTotalcampaigns($userField){

		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= Workorders::tableName().'.workorderpartner='.$userId;

		}else{

			$custQuery		= '';

		}

		$hour 		= 	12;
		$today  	= strtotime($hour . ':00:00');
		$data	    = Workorders::find()->select('dateFrom,dateTo')->leftJoin(User::tableName(), User::tableName().'.id='.Workorders::tableName() .'.workorderpartner')->where('dateTo >= "'.strtotime(date('m/d/Y H:i:s')).'"')->count();

		$totalComp  = Workorders::find()->leftJoin(User::tableName(), User::tableName().'.id='.Workorders::tableName() .'.workorderpartner')->where('dateTo >= "'.strtotime(date('m/d/Y H:i:s')).'"')->andWhere($custQuery)->count();
		$totalActiveComp = array($data,$totalComp);

		return $totalActiveComp;
	}

	/*
	* return no. of online beacons
	*/
	public function actionBeaconsonline($userField){

		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= User::tableName().'.user_type='.$userType.' AND '.User::tableName().'.id='.$userId;

		}else{

			$custQuery		= '';

		}

		$totalBecon 		= Beaconmanager::find()->count();
		$totalBeconOnline 	= Beaconmanager::find()->select('id')->leftJoin(User::tableName(), User::tableName().'.id='.Beaconmanager::tableName() .'.locationOwnerID')->where(Beaconmanager::tableName().'.status=1')->andWhere($custQuery)->count();

		return array($totalBecon,$totalBeconOnline);

	}

	public function actionActiveclients()
	{
		$mediaAgency = User::find()->select('id')->where('user_type = 4')->count();
		$Reseller	 = User::find()->select('id')->where('user_type = 5')->count();
		$Advertiser	 = User::find()->select('id')->where('user_type = 6')->count();
		$locationOwner = User::find()->select('id')->where('user_type = 7')->count();
		$broker = User::find()->select('id')->where('user_type = 8')->count();
		$usersData = array($mediaAgency,$Reseller,$Advertiser,$locationOwner,$broker);
		return $usersData;
	}

	public function actionPassessent()
	{
		$hour 		= 	12;
		$today  	= 	strtotime($hour . ':00:00');
		$yesterday  = 	strtotime('-1 day', $today);
		$threeday  	= 	strtotime('-2 day', $today);
		$fourdayOld = 	strtotime('-3 day', $today);
		$fivedayOld = 	strtotime('-4 day', $today);
		$sixdayOld 	= 	strtotime('-5 day', $today);
		$OneWeek 	= 	strtotime('-6 day', $today);
		$day_1  = Workorders::find()->select('created_at')->joinWith('workorderpopup',false,'INNER JOIN')->all();
				$day1 = array();$day2 = array(); $day3 = array(); $day4 = array(); $day5 = array(); $day6 = array(); $day7 = array();

		foreach($day_1 as $data)
		{

			if(date('y-m-d',$today) == date('y-m-d',$data['created_at']))
			{
				$day1[] = $data['created_at'];
			}
			if(date('y-m-d',$yesterday) == date('y-m-d',$data['created_at']))
			{
				$day2[] = $data['created_at'];
			}
			else if(date('y-m-d',$threeday) == date('y-m-d',$data['created_at']))
			{
				$day3[] = $data['created_at'];
			}
			else if(date('y-m-d',$fourdayOld) == date('y-m-d',$data['created_at']))
			{
				$day4[] = $data['created_at'];
			}
			else if(date('y-m-d',$fivedayOld) == date('y-m-d',$data['created_at']))
			{
				$day5[] = $data['created_at'];
			}
			else if(date('y-m-d',$sixdayOld) == date('y-m-d',$data['created_at']))
			{
				$day6[] = $data['created_at'];
			}
			else if(date('y-m-d',$OneWeek) == date('y-m-d',$data['created_at']))
			{
				$day7[] = $data['created_at'];
			}

		}

		 $totalPassessent = array('day1'=>count($day1),'day2'=>count($day2),'day3'=>count($day3),'day4'=>count($day4),'day5'=>count($day5),'day6'=>count($day6),'day7'=>count($day7));

		return $totalPassessent;

	}

	################################################################################
	########	Report for dashboard
	#################################################################################


	public function actionGetDashboardReport(){

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		if(Yii::$app->request->isAjax) {

			$param = Yii::$app->request->bodyParams;
			$type  = isset($param['type'])?$param['type']:'';
			$linktype  = isset($param['linktype'])?$param['linktype']:'';

			switch($type){

				case 'D':

						$date 			= new \DateTime(date('m/d/Y 23:59:59', time()));

						$unixDateEnd  	= $date->getTimeStamp(); //max

						$date->sub(new \DateInterval('PT24H'));

						$unixDateStart 	= $date->getTimeStamp(); //min

						// get all report data from model and render view with ajax

						$graphData		= $this->callFunction($unixDateStart,$unixDateEnd,$linktype,$type);

						break;

				case 'W':

						// $date 			= new \DateTime(date('m/d/Y  23:59:59', time()));

						$startDate = new \DateTime(date('m/d/Y', time()));

						$startDate->modify(('Sunday' == $startDate->format('l')) ? 'Monday last week' : 'Monday this week');

						$unixDateStart 	= $startDate->getTimeStamp();

						$endDate 	= new \DateTime(date('Y-m-d 23:59:59',time()));

						$unixDateEnd  	= $endDate->getTimeStamp(); //max

						$graphData		= $this->callFunction($unixDateStart,$unixDateEnd,$linktype,$type);

					break;

				case 'M':

						$startDate 			= new \DateTime('first day of this month');
						$unixDateStart 		= $startDate->getTimeStamp();

						$endDate 			= new \DateTime(date('Y-m-d 23:59:59',time()));

						$unixDateEnd		= $endDate->getTimeStamp();

						// get all report data from model and render view with ajax

						$graphData		= $this->callFunction($unixDateStart,$unixDateEnd,$linktype,$type);

					break;

				case 'Y':

						$date 				= new \DateTime(date('Y-m-d 23:59:59',time()));

						$unixDateEnd 	  	= $date->getTimeStamp();

						$startDate 				= new \DateTime(date('Y-1-1 00:00:00',time()));


						// $date->sub(new \DateInterval('P12M'));

						$unixDateStart 		= $startDate->getTimeStamp();

						// get all report data from model and render view with ajax

						$graphData		= $this->callFunction($unixDateStart,$unixDateEnd,$linktype,$type);

					break;

				case 'O':

							$rangeData	=	$param['daterange'];
							$Dates			=	explode('-',$rangeData);
							$startDate		= 	$Dates[0];
							$endDate		= 	$Dates[1];

							$dateEnd 		= new \DateTime($endDate);

							$unixDateEnd  	= $dateEnd->getTimeStamp(); // max

							$dateStart 		= new \DateTime($startDate);

							$unixDateStart  = $dateStart->getTimeStamp(); //min

							// get graph data


							$graphData		= $this->callFunction($unixDateStart,$unixDateEnd,$linktype,$type);

					break;

			}

			return $graphData;

        }

		die;

	}

	/**
	* @getGlance
	* return data of AT A GLANCE
	*
	*/

	protected function getGlance($start=NULL,$end=NULL,$filterType){
		//


		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){

			// user not administrator

			$userField 	= ['userType'=>$userType,'userId'=>$userId];

		}else{

			$userField 	= [];

		}

		$user 			= new User();
		$pass 			= new Pass();
		$workorder		= new Workorders();
		$userPsa		= new UserPsa();

		$reportData 	= [];

		$activeUser 	= $user->activeAppUsers($start,$end,$userField);

		$topCampaign 		= $workorder->topPerformCampaign($start,$end,$userField);

		$savedRedeemedPass	= $pass->getSavedRedeemedPass($start,$end,$filterType,$userField);



		$notificationData	= $userPsa->notifications($start,$end,$filterType,$userField);

		$reportData['activeUser'] 			= $activeUser;
		// $reportData['actionTotalcampaigns'] = $this->actionTotalcampaigns();
		$reportData['savedRedeemedPass'] 	= $savedRedeemedPass;

		$reportData['topCampaign'] 			= $topCampaign;
		$reportData['notificationData'] 	= $notificationData;

		return $reportData;

	}

	/*
	*@actionDashboardUser
	* used to render view of users
	*/

	public function actionDashboardUser(){

		$this->layout 		= 'main';

		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){

			// user not administrator

			$userField 	= ['userType'=>$userType,'userId'=>$userId];

		}else{

			$userField 	= [];

		}


		$user 				= new User();
		$activeappuser 		= $user->getRightNowActiveUsers($userField);

        return $this->render('dashboard_users',['activeappuser'=>$activeappuser]);


	}


	/*
	*@userReport
	* return data of USERS
	*/

	protected function userReport($start=NULL,$end=NULL,$filterType){

		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){

			// user not administrator

			$userField 	= ['userType'=>$userType,'userId'=>$userId];

		}else{

			$userField 	= [];

		}

		$user 				= new User();
		$LoginLogs 			= new LoginLogs();
		$referralModel      = new \common\models\ReferralEarnings();	
		$reportData 		= [];

		$categories 		= $user->getCategories($start,$end,$userField);


		$topCategory    	= !empty($categories)?array_merge(array_slice($categories,0,3),array_slice($categories,count($categories)-3,3)):0;

		$activeUser 		= $user->activeAppUsers($start,$end,$userField);

		$newUsers 			= $user->newUsers($start,$end,$userField);



		$devicesUsed 		= $user->devicesUsed($start,$end,$userField);

		$sessions 			= $LoginLogs->countUserSession($start,$end,$userField);
		
		
		$reportData['activeUser'] 			= $activeUser;
		$reportData['newUsers'] 			= $newUsers;
		$reportData['devicesUsed'] 			= $devicesUsed;
		$reportData['userSessions'] 		= $sessions;
		$reportData['topCategory'] 			= $topCategory;
		$reportData['referralCount'] 		= $referralModel->refEarning($start,$end,$userField);

		return $reportData;


	}

	/*
	*@actionDashboardDeal
	* used to render view of DEALS / PASSES
	*/

	public function actionDashboardDeal(){

		$this->layout 		= 'main';

		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){

			// user not administrator

			$userField 	= ['userType'=>$userType,'userId'=>$userId];

		}else{

			$userField 	= [];

		}

		$workorders 		= new Workorders();
		$monthlyQuota = $workorders->monthlyQuotaPass($userField);

        return $this->render('dashboard_deals',['monthlyQuota'=>$monthlyQuota]);

	}


	/*
	*@dealReport
	* return data of DEALS / PASSES
	*/

	protected function dealReport($start=NULL,$end=NULL,$filterType){

		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){

			// user not administrator

			$userField 	= ['userType'=>$userType,'userId'=>$userId];

		}else{

			$userField 	= [];

		}



		$pass 				= new Pass();
		$userPsa			= new UserPsa();

		$reportData 		= [];

		$passPerCategory 	= $pass->passPerCategory($start,$end,$userField);

		$savedRedeemedPass	= $pass->getSavedRedeemedPass($start,$end,$filterType,$userField);

		$notificationData	= $userPsa->notifications($start,$end,$filterType,$userField);

		// var_dump($notificationData);
		// die;

		$reportData['savedRedeemPass']   	= $savedRedeemedPass;
		$reportData['notificationData'] 	= $notificationData;

		$reportData['passPerCategory'] 	= $passPerCategory;

		return $reportData;


	}


	/*
	*@actionDashboardBeacon
	* used to render view of BEACONS
	*/

	public function actionDashboardBeacon(){

		$this->layout 		= 'main';

		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){

			// user not administrator

			$userField 	= ['userType'=>$userType,'userId'=>$userId];

		}else{

			$userField 	= [];

		}

		$onlineBeacons 		= $this->actionBeaconsonline($userField);

        return $this->render('dashboard_beacon',['onlineBeacons'=>$onlineBeacons]);

	}


	/*
	*@beaconReport
	* return data of BEACONS
	*/

	protected function beaconReport($start=NULL,$end=NULL){

		$reportData 		= [];
		$beacons 			= new BeaconDetectLog();

		$totalBeacons 		= 0;

		$unixDateEnd  		= $end; // max

		$unixDateStart    	= $start; //min

		$interval 			= (int) ((int) $unixDateEnd - (int) $unixDateStart)/4;

		$dateStart 			= new \DateTime(date('m/d/Y H:i:s',$start));


		return $reportData;


	}

	/*
	*@actionDashboardCampaign
	* used to render view of CAMPAIGNS
	*/

	public function actionDashboardCampaign(){

		$this->layout 		= 'main';

		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){

			// user not administrator

			$userField 	= ['userType'=>$userType,'userId'=>$userId];

		}else{

			$userField 	= [];

		}

		$workorders 		= new Workorders();

		$campaignRunning 	= $workorders->campaignRunning($userField);

        return $this->render('dashboard_campaign',['campaignRunning'=>$campaignRunning]);

	}


	/*
	*@campaignReport
	* return data of CAMPAIGNS
	*/

	protected function campaignReport($start=NULL,$end=NULL,$filterType){

		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){

			// user not administrator

			$userField 	= ['userType'=>$userType,'userId'=>$userId];

		}else{

			$userField 	= [];

		}

		$workorders 			= new Workorders();
		$orders 				= new Orders();
		$user 					= new User();

		$categories 			= $user->getCategories($start,$end,$userField);

		$topCategory    		= !empty($categories)?['top5'=>array_slice($categories,0,5),'botton5'=>array_slice($categories,count($categories)-5,5)]:0;

		$reportData 			= [];

		$campRedemptions 		= $orders->campaignRedemption($start,$end,$filterType,$userField);

		$reportData['topCampaign'] 		= $workorders->topPerformCampaign($start,$end,$userField);
		$reportData['campaignRedempt'] 	= $campRedemptions;

		$reportData['categories'] 		= $topCategory;

		return $reportData;


	}


	/*
	*@actionDashboardLoyality
	* used to render view of LOYALTY
	*/

	public function actionDashboardLoyality(){

		$this->layout 		= 'main';

		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){

			// user not administrator

			$userField 	= ['userType'=>$userType,'userId'=>$userId];

		}else{

			$userField 	= [];

		}

		$orderLoyalty 		= new OrderLoyalty();
		$userLoyalty 		= new UserLoyalty();

		$avgLoyalTrans		= $orderLoyalty->avgLoyalTransaction($userField);

        return $this->render('dashboard_loyality',[
													'avgLoyalTrans'=>$avgLoyalTrans
												]);

	}


	/*
	*@loyalityReport
	* return data of LOYALTY
	*/

	protected function loyalityReport($start=NULL,$end=NULL){

		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){

			// user not administrator

			$userField 	= ['userType'=>$userType,'userId'=>$userId];

		}else{

			$userField 	= [];

		}

		$loyaltypoint 			= new Loyaltypoint();
		$user 					= new User();
		$orderLoyalty 			= new OrderLoyalty();
		$userLoyalty 			= new UserLoyalty();

		$reportData 			= [];

		$getPointsAccrued 		= $user->getPointsAccrued($start,$end,$userField);


		$pointsAccruedRedeemed 	= $user->getPointsAccruedRedeemed($start,$end,$userField);

		$pointLoyality			= $orderLoyalty->pointLiability($start,$end,$userField);


		$activeCustomers		= $userLoyalty->activeLoyalCust($start,$end,$userField);

		$reportData['offTransac'] 				= $loyaltypoint->offlineTransaction($start,$end,$userField);
		$reportData['getPointsAccrued'] 		= $getPointsAccrued;
		$reportData['pointsAccruedRedeemed'] 	= $pointsAccruedRedeemed;
		$reportData['totalLiability'] 			= $pointLoyality;
		$reportData['activeCustomers'] 			= $activeCustomers;


		return $reportData;


	}

	/*
	*@actionDashboardShoppping
	* used to render view of SHOPPING CART / PAYMENT
	*/

	public function actionDashboardShoppping(){

		$this->layout 		= 'main';


        return $this->render('dashboard_shoppping');


	}


	/*
	*@shopppingReport
	* return data of SHOPPING CART / PAYMENT
	*/

	protected function shopppingReport($start=NULL,$end=NULL,$filterType){

		$userType 	= (int) Yii::$app->user->identity->user_type;
		$userId 	= (int) Yii::$app->user->identity->id;

		if($userType>1){

			// user not administrator

			$userField 	= ['userType'=>$userType,'userId'=>$userId];

		}else{

			$userField 	= [];

		}


		$transaction 			= new Transaction();
		$orders 				= new Orders();
		$paymentMethod 			= new PaymentMethod();
		$user		 			= new User();
		$transFees				= new PartnerTransactionsFees();
		$subscriptionFees = new \common\models\SubscriptionCharges();

		$reportData 			= [];

		$getTransacByGen		= $user->getTransacByGen($start,$end,$userField);

		$orderCountPay 			= $orders->payViaTransc($start,$end,$filterType,$userField);

		$cartAbdonments			= $orders->cartAbdonment($start,$end,$filterType,$userField);


		$reportData['avgTranscValue'] 	= $transaction->avgTranscValue($start,$userField);

		$reportData['creditCardUsed'] 	= $paymentMethod->creditCardUsed($start,$end,$userField);
		$reportData['orderCountPay'] 	= $orderCountPay;
		$reportData['cartAbdonmnt'] 	= $cartAbdonments;
		$reportData['getTransacByGen'] 	= $getTransacByGen;
		$reportData['transFees'] 		= $transFees->feeAmount($start,$end,$filterType,$userField);
    $reportData['subscriptionFees'] 		=$subscriptionFees->chargesHistory($start,$end,$filterType,$userField);
		return $reportData;


	}


	/*
	*@callFunction
	* used to redirect actions
	*/


	protected function callFunction($unixDateStart,$unixDateEnd,$type,$filterType){

		switch($type){

			case "AG": return $this->getGlance($unixDateStart,$unixDateEnd,$filterType);
				break;

			case "US": return $this->userReport($unixDateStart,$unixDateEnd,$filterType);
				break;

			case "DE": return $this->dealReport($unixDateStart,$unixDateEnd,$filterType);
				break;

			case "BE": return $this->beaconReport($unixDateStart,$unixDateEnd,$filterType);
				break;

			case "CA": return $this->campaignReport($unixDateStart,$unixDateEnd,$filterType);
				break;

			case "LO": return $this->loyalityReport($unixDateStart,$unixDateEnd,$filterType);
				break;

			case "SH": return $this->shopppingReport($unixDateStart,$unixDateEnd,$filterType);
				break;

		}

	}
	public function actionTransFees(){

		$postData = Yii::$app->request->post();
		$query = Yii::$app->getDb();
		$custQuery  = $stores = $deals= '';
		if(!empty($postData['partnerId']) && is_numeric($postData['partnerId'])){
			$custQuery .= ' AND partner_id='.$postData['partnerId'];
      $stores =   \common\models\Store::find()
                ->select(['id','storename'])
                ->where('owner='.$postData['partnerId'])
                ->asArray()->all();
      $deals = \common\models\Workorders::find()
              ->select(['id','name'])
              ->where('workorderpartner='.$postData['partnerId'])
              ->asArray()->all();
		 }
		 if(!empty($postData['dealId']) && is_numeric($postData['dealId'])){
			$custQuery .= ' AND workorder_id='.$postData['dealId'];
		 }
		 if(!empty($postData['storeId']) && is_numeric($postData['storeId'])){
			$custQuery .= ' AND store_id='.$postData['storeId'];
		 }

		  $usertype = Yii::$app->user->identity->user_type;
         $userId = $usertype == User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;

		if($usertype==User::ROLE_Advertiser){
			$custQuery .= ' AND partner_id='.$userId;
		}
		 $tableName = PartnerTransactionsFees::tableName();

		 $command 	= $query->createCommand("SELECT IFNULL(sum(transactionFees),0) as paid FROM $tableName where is_deducted=1 $custQuery union all SELECT IFNULL(sum(transactionFees),0) as dues FROM $tableName where is_deducted=0 $custQuery union all SELECT IFNULL(sum(transactionFees),0) as total FROM $tableName where is_deducted!=100 $custQuery ;");
		$result 	= $command->queryAll();
    $data = array_column($result,'paid');
    array_push($data,$stores,$deals);
    echo json_encode($data); die;
	}

  public function actionSubsCharges(){

		$postData = Yii::$app->request->post();
		$query = Yii::$app->getDb();
		$custQuery = '';
		if(!empty($postData['partnerId'])){
			$custQuery .= ' AND partner_id='.$postData['partnerId'];
		 }
		  $usertype = Yii::$app->user->identity->user_type;
       $userId = $usertype == User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;

		if($usertype==User::ROLE_Advertiser){
			$custQuery .= ' AND partner_id='.$userId;
		}
		 $tableName = \common\models\SubscriptionCharges::tableName();
     $command 	= $query->createCommand("SELECT IFNULL(sum(amount),0) as paid FROM $tableName where payment_status=1  $custQuery union all SELECT IFNULL(sum(amount),0) as paid FROM $tableName where payment_status=0  $custQuery union all SELECT IFNULL(sum(amount),0) as paid FROM $tableName where payment_status!=100 $custQuery ;");
     $result 	= $command->queryAll();
     return json_encode(array_column($result,'paid')); die;
	}


}
