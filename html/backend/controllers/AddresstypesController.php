<?php
namespace backend\controllers;
use Yii;
use yii\db\Query;
use common\models\Addresstypes;
use backend\models\AddresstypesSearch;
use backend\models\OrdersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;



/**
 * RegionsController implements for Regions model.
 */
class AddresstypesController extends Controller
{
  public function behaviors()
    {
        return [ 
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],        
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
            
        ];
    }

    /**
     * Lists all Regions models.
     * @return mixed
     */
    public function actionIndex()
    {
		$searchModel = new AddresstypesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Regions model.
     * @param integer $id
     * @return mixed
     */
   /* public function actionView($id)
    {
		
        $dataProvider = $searchModel->search($id);
		return $this->render('view', [
            'model' => Orders::findOne(['id' => $id]),
            
        ]);
     }
*/
    /**
     * Creates a new Regions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Addresstypes();
        if ($model->load(Yii::$app->request->post()) &&  $model->validate() ) {
           
            if($model->save()){
             return $this->redirect(['view', 'id' => $model->id]);
		}
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
      public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

  public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];  
		if($pid) {
			$this->findModel($pid)->delete();
			echo $pid; exit;
		}
	}

    /**
     * Updates an existing Regions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    { 
		//$model = $this->findModel($id);
        $model = Orders::findOne(['id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/
		
   

    /**
     * Deletes an existing Regions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/
    
   
   
    protected function findModel($id)
    {
        if (($model = Addresstypes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
