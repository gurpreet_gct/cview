<?php

namespace backend\controllers;

use Yii;
use common\models\SubscriptionCharges;
use backend\models\SubscriptionChargesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * SubscriptionChargesController implements the CRUD actions for SubscriptionCharges model.
 */
class SubscriptionChargesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}

    public function actionPaynow($id){

      $amount = \common\models\SubscriptionCharges::findOne($id);

      $usertype = Yii::$app->user->identity->user_type;
      $userId = $usertype==\common\models\User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
      $partnerID = Yii::$app->user->id;
      if($usertype==\common\models\User::ROLE_ADMIN){
          $partnerID = $amount->partner_id;
      }

      $accDetails = $this->accdetail($partnerID);
      $getAmount = $amount->amount !='' ? $amount->amount : 0;

      $attributes=['amount','account','acc_type'];
	  $model=new \yii\base\DynamicModel($attributes);
      $model->addRule(['amount','account'],'required');
      $model->addRule(['acc_type'],'safe');

      $model->amount=$getAmount;
      if($model->load(Yii::$app->request->post()) && $model->validate()) {
        $date = date('m-Y');
        $userName = Yii::$app->user->identity->username;
        $name = "Subscription charges- $userName";
		// $model->acc_type= 1 = Bank ,$model->acc_type =2 = credit card
		
		if($model->acc_type==1){
			$res = Yii::$app->promisePay->createAuthority($model->account,$model->amount);
			
		}else {
			
		}
        $payment=Yii::$app->promisePay->createCharges($name,$model->account,$model->amount,Yii::$app->user->identity->email);
        if(is_string($payment)){
          $break = explode(':',$payment);
          \Yii::$app->getSession()->setFlash('error',end($break));
          return $this->render('paynow',['model'=>$model,'acc'=>$accDetails]);
        }
        $amount->payment_status = 1;
        $amount->account_id = $model->account;
        $amount->updated_by = Yii::$app->user->identity->id;
        $amount->transaction_id = $payment['id'];
        $amount->save(false);
        $msg = 'You have successfully paid subscription amount.';
        Yii::$app->getSession()->setFlash('success',$msg);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
      } else {
        return $this->render('paynow',['model'=>$model,'acc'=>$accDetails]);
      }
    }

    public function accdetail($id){

     $bankAcc = \common\models\Bankdetail::find()->select(['concat("Bank account -  ",account_name) as account_name','promise_acid'])
                  ->where(['partner_id'=>$id])
                  ->andWhere('promise_acid is not null')
                  ->asArray()->all();
    $creditCards=   \common\models\Paymentcard::find()->select(['concat("Credit card - ",cardholderName) as account_name','uniqueNumberIdentifier as promise_acid'])
        ->where(['user_id'=>$id])
        ->andWhere('uniqueNumberIdentifier is not null')
        ->andWhere("issuingBank='promisePay'")
        ->asArray()->all();
     $allAcs = array_merge($bankAcc,$creditCards);
      if(count($allAcs)>0){
          $key = array_column($allAcs,'promise_acid');
          $val = array_column($allAcs,'account_name');
          return array_combine($key,$val);
      }
      return false;
    }
    /**
     * Lists all SubscriptionCharges models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubscriptionChargesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SubscriptionCharges model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SubscriptionCharges model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SubscriptionCharges();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SubscriptionCharges model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SubscriptionCharges model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SubscriptionCharges model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SubscriptionCharges the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {          
        $usertype = Yii::$app->user->identity->user_type;
		$userId = $usertype == 7 ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
		if($usertype == 1){
			$model = SubscriptionCharges::findOne($id);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
			
		} else {
			$model = SubscriptionCharges::findOne(['id'=>$id,'partner_id'=>$userId]);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
    }
}
