<?php

namespace backend\controllers;

use Yii;
//use common\models\FoodProduct;
use common\models\FoodCategories;
use common\models\FoodcategoryProducts;
//use backend\models\FoodProductSearch;
use common\models\Product;
use backend\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\db\Query;
/**
 * FoodproductController implements the CRUD actions for FoodProduct model.
 */
class FoodproductController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FoodProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->searchFoodProduct(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FoodProduct model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FoodProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FoodProduct();
        $FoodcategoryProducts = new FoodcategoryProducts();

        if ($model->load(Yii::$app->request->post()) && $model->validate())  {	
			$cateTreeVal = Yii::$app->request->post('categoryTree');
			/*save categories in database */
			if(!empty($cateTreeVal)){				
				$model->category = implode(',',$cateTreeVal);				
			}
			$model->imageFile = UploadedFile::getInstance($model, 'imageFile');		
			$uniqueimage = substr(md5(date("Ymdhis")),0,10);
		
			/* save image in user model */
        		if ($model->imageFile) {					
  	     	 		$imageName = $uniqueimage;
        			$model->image = $imageName.'.'.$model->imageFile->extension;    		
        			$model->save(false);      			     		
					$imagePath =str_replace("/",DIRECTORY_SEPARATOR,Yii::getAlias('@backend')."/" .'web/uploads/foodProductImages'."/".$model->image);
					
					$thumbImagePath =str_replace("/",DIRECTORY_SEPARATOR,Yii::getAlias('@backend')."/" .'web/uploads/foodProductImages/thumb'."/".$model->image);							
        			$model->imageFile->saveAs($imagePath);
					Image::getImagine()
						->open($imagePath)
						->thumbnail(new Box(100,100))
						->save($thumbImagePath);
					}else{						
						$model->save();	
						/*save category items */
						$rows = array();
						foreach($cateTreeVal as $_cateTreeVal){
						$rows[] = [
							'product_id'=>$model->id,
							'category_id' => $_cateTreeVal,
							'status' => 1,
							'created_at' => time(),
							'updated_at' => time()							
						];
					}
					$columnNameArray=['product_id','category_id','status','created_at','updated_at'];
					Yii::$app->db->createCommand()->batchInsert(FoodcategoryProducts::tableName(),$columnNameArray, $rows)->execute();
				}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'categoryTree' => $this->getCategoryTree(0),
            ]);
        }
    }

    /**
     * Updates an existing FoodProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
			$model = $this->findModel($id);		
			$FoodcategoryProducts = new FoodcategoryProducts();
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$uniqueimage = substr(md5(date("Ymdhis")),0,15); 	       
 			/* save image name in database */
			if(strstr($model->image,"http") && strstr($model->image,".") ){					
				$imageName = explode("foodProductImages/",$model->image);
				$model->image = $imageName[1]; 			 
			}				
				$model->imageFile = UploadedFile::getInstance($model, 'imageFile');
        	  /*save image in user model */         	
				if ($model->imageFile) {
					$uniqueName = md5(rand(5,50));	
						
					if(strstr($model->image,".")) {							
							$oldImage = explode(".",$model->image); 							
						}else { 
							$oldImage[0] = $uniqueName;
						}
				 $model->image = $oldImage[0].'.'.$model->imageFile->extension;	
					$imagePath =str_replace("/",DIRECTORY_SEPARATOR,Yii::getAlias('@backend')."/" .'web/uploads/foodProductImages'."/".$model->image);
					
					$thumbImagePath =str_replace("/",DIRECTORY_SEPARATOR,Yii::getAlias('@backend')."/" .'web/uploads/foodProductImages/thumb'."/".$model->image);	
					$model->imageFile->saveAs($imagePath); 
						/*image thumb*/
					Image::getImagine()
						->open($imagePath)
						->thumbnail(new Box(100,100))
						->save($thumbImagePath);					
	          	
			    }
			/*save categories in database */
			$cateTreeVal = Yii::$app->request->post('categoryTree');
			if(!empty($cateTreeVal)){				
				$model->category = implode(',',$cateTreeVal);
				
			}
					
				$model->save();	
				/*save category items */
						$rows = array();
						foreach($cateTreeVal as $_cateTreeVal){
						$rows[] = [
							'product_id'=>$model->id,
							'category_id' => $_cateTreeVal,
							'status' => 1,
							'created_at' => time(),
							'updated_at' => time()							
						];
					}
					FoodcategoryProducts::deleteAll('product_id ='.$model->id);
					$columnNameArray=['product_id','category_id','status','created_at','updated_at'];
					Yii::$app->db->createCommand()->batchInsert(FoodcategoryProducts::tableName(),$columnNameArray, $rows)->execute();			
            return $this->redirect(['view', 'id' => $model->id]);
        } else {			
            return $this->render('update', [
                'model' => $model,
				'categoryTree' => $this->getCategoryTree($model->id),
            ]);
        }
    }

    /**
     * Deletes an existing FoodProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FoodProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return FoodProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FoodProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	protected function getCategoryTree($workorder_id)	
	{	
	$query = new Query;	
		$query	->select([FoodCategories::tableName().'.id as id','name as title',FoodCategories::tableName().'.id as value',  '(lvl+2) as level','lft','ifnull(status,0) as checked'])  
				->from(FoodCategories::tableName())
				->leftJoin(FoodcategoryProducts::tableName(),FoodcategoryProducts::tableName().'.category_id ='.FoodCategories::tableName().'.id and '.FoodcategoryProducts::tableName().'.product_id='.$workorder_id)
				->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC]); 	
		$command = $query->createCommand();
		
		$data = $command->queryAll();
		$requiredResult=array();
		$oldLevel=2;
		$levelKeys=array();
	
		foreach($data as $key=>$value)
		{
			//$value['path']="root/";
			$key=$value['id'];
            $value['title']=htmlspecialchars_decode($value['title']);
           $value['has_children']=false;
           $value['children']=[];
		   $value['checked']=$value['checked']==1?true:false;
		   
			if($value['level']==2)
			{
		
				$requiredResult[$key]=$value;
				
			}
			else
			{	
				$array_ptr=&$requiredResult;
				
				for($i=2; $i<$value['level']; $i++)
				{
					 $array_ptr[ strval($levelKeys[$i]) ]['has_children']=true;
					 $array_ptr = &$array_ptr[ strval($levelKeys[$i]) ]['children'];
					 
					 
				}
				$array_ptr[$key]=$value;
			
			}            
    
			$levelKeys[$value['level']]=$value['id'];			
		}	
		
		$this->recursiveRemoval($requiredResult,'children');
      $requiredResult=is_array($requiredResult)?array_values($requiredResult):$requiredResult;
	  	$data=array('id'=>0,'value'=>0,'title'=>'Root','level'=>1,'has_children'=>true,'children'=>$requiredResult);
		return (json_encode($data));

	}
	
			 public $i=0;  
	protected function recursiveRemoval(&$array, $val)
	{
		if(is_array($array))
		{
			foreach($array as $key=>&$arrayElement)
			{
				if(is_array($arrayElement))
				{
					if($key===$val)
					{ 
				$this->i++;
				
				 if($this->i==2)
				 {//    print_r($arrayElement);echo $key;
			 }
				$arrayElement=array_values($arrayElement);

				}
					$this->recursiveRemoval($arrayElement, $val);
				}
				else
				{
					if($key == $val)
					{
						//print_r($arrayElement);
					}
				}
			}
		}
	}
	   public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];  
		if($pid) {
			$this->findModel($pid)->delete();
			echo $pid; exit;
		}
	}
}
