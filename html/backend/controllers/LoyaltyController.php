<?php
namespace backend\controllers;
use Yii;
use yii\db\Query;
use common\models\User;
use common\models\Loyaltypoint;
use common\models\Loyalty;
use backend\models\LoyaltypointSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;


/**
 * OrdersController implements for Orders model.
 */
class LoyaltyController extends Controller
{
  public function behaviors()
    {
        return [ 
         'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                      
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],        
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
            
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
		$searchModel = new LoyaltypointSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

 
    
    public function actionCreate()
    {
        $model = new Loyalty();
        if ($model->load(Yii::$app->request->post()) &&  $model->validate() ) {
            //return $this->redirect(['view', 'id' => $model->id]);
            if($model->save()){
            return $this->redirect(['create']);
		}
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

   
   /* public function actionUpdate($id)
    { 
		//$model = $this->findModel($id);
        $model = Loyalty::findOne(['id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/
		
    
   
   public function actionDelete($id){
        $this->findModel($id)->delete();
		return $this->redirect(['index']);
    }
    
    /* delete via ajax */   
	public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];  
		if($pid) {
			$this->findModel($pid)->delete();
			echo $pid; exit;
		}
	}

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
   protected function findModel($id)
    {
        if (($model = Loyalty::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
