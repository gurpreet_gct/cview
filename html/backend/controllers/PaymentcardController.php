<?php

namespace backend\controllers;

use Yii;
use common\models\PaymentcardSearch;
use common\models\Paymentcard;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\filters\VerbFilter;

/**
 * WalletidController implements for Cardtype model.
 */
class PaymentcardController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cardtype models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentcardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionCreate()
    {
		$model = new Paymentcard();
        if ( $model->load(Yii::$app->request->post()) && $model->validate()) {

            $data = (object)['user_id'=>$model->user_id,'full_name'=>$model->cardholderName,'number'=>$model->maskedNumber,'expiry_month'=>$model->expirationMonth,'expiry_year'=>$model->expirationYear,'cvv'=>$model->bin];
            $cards = Yii::$app->promisePay->createCardAccount($data);
            if(is_array($cards)){
              $cardsDetails = Yii::$app->promisePay->getCardDetails($cards['id']);
            if(is_array($cardsDetails['card'])){
                $model->maskedNumber=$cardsDetails['card']['number'];
                $model->cardType=$cardsDetails['card']['type'];
              }
              $model->cardholderName=$model->cardholderName;
              $model->expirationMonth=$model->expirationMonth;
              $model->expirationYear=$model->expirationYear;
              $model->uniqueNumberIdentifier=$cards['id'];
              $model->issuingBank='promisePay';
              $model->bin='';
              $model->expirationDate=$model->expirationMonth.'/'.$model->expirationYear;
              $model->save(false);
              
          } else{
			  $message = explode(':',$cards);
			   \Yii::$app->getSession()->setFlash('error',end($message));
			return $this->render('create', [
                'model' => $model,
            ]); 
			  }
		  if(!empty($_GET['payid'])){
				 return $this->redirect(['subscription-charges/paynow','id'=>$_GET['payid']]);
		   }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    public function getUserPromiseId($userId){
      $user = \common\models\User::find()->select(['promise_uid','firstName','lastName','email'])
                ->where('id='.$userId)->one();
      if(is_object($user)){
        return $user->promise_uid;
      }else{
        $data = (object)['firstName'=>$user->firstName,'lastName'=>$user->lastName,'email'=>$user->email];
        $result = Yii::$app->promisePay->createUser($data);
      	if(isset($result->id)){
					$user->promise_uid=$result->id;
          $user->save(false);
          return $result->id;
				}
      }
    }
    /**
     * Displays a single Cardtype model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }



	/*Delete by ajax */

	public function actionDeletebyajax()
	{
		$pid = $_POST['id'];
		if($pid) {
			$model = $this->findModel($pid);
			Yii::$app->promisePay->removeCard($model->uniqueNumberIdentifier);
			$model->delete();
			echo $pid; exit;
		}
	}

    /**
     * Deletes an existing Cardtype model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Id model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Id the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $usertype = Yii::$app->user->identity->user_type;
		$userId = $usertype == 7 ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
		if($usertype == 1){
			$model = Paymentcard::findOne($id);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
			
		} else {
			$model = Paymentcard::findOne(['id'=>$id,'user_id'=>$userId]);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
    }
}
