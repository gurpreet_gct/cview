<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\Beaconmanager;
use common\models\Store;
use backend\models\BeaconmanagerSearch;
use backend\models\Storebeacons;
use backend\models\Beacongroup;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\db\Query;

/**
 * BeaconManagerController implements for BeaconManager model.
 */
class BeaconmanagerController extends Controller
{
    public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}

    /**
     * Lists all Beaconmanager models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BeaconmanagerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Beaconmanager model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Beaconmanager model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Beaconmanager();
		$storebeacon = new Storebeacons();
        
        if ($model->load(Yii::$app->request->post()) && $storebeacon->load(Yii::$app->request->post()) && Model::validateMultiple([$model, $storebeacon])) {			
			          	
          if(is_array($model->groups)){
			$beaconid = implode(', ',$_POST['BeaconManager']['groups']);
			$str = rtrim($beaconid,',');			
			$model->groups = $str;   }		
			
			
				$model->save(false);
					$data = [];
					
					if($storebeacon->store_id) {
					
						$data[] = ['beacon_id' => $model->id, 'store_id' => $storebeacon->store_id, 'status' => $model->status];
					
					
						Yii::$app->db->createCommand()->batchInsert(
						Storebeacons::tableName(), 
						['beacon_id', 'store_id', 'status'], 
						$data
						)->execute();
			        }
			
			
          	 
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'storebeacon' => $storebeacon,
            ]);
        }
    }

    /**
     * Updates an existing Beaconmanager model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		
        $model = $this->findModel($id);
     
        $storebeacon = $this->findbeaconstoreModel($id);        
        $storeBeaconModel= new Storebeacons();
        $storeBeaconModel->store_id= $storebeacon;
         

        if ($model->load(Yii::$app->request->post()) && $storeBeaconModel->load(Yii::$app->request->post()) && Model::validateMultiple([$model, $storeBeaconModel])) {
				
			$model->imageFile = UploadedFile::getInstance($model, 'imageFile');
			
			$uniqueimage = substr(md5(date("Ymdhis")),0,15); 
 			/* get old file name */
 			if(isset($model->imgBeaconLoc) && $model->imgBeaconLoc !='') {
			$oldimage = explode("/",$model->imgBeaconLoc);  $oldImageName = explode(".",$oldimage[1]); 
			}else { 
				$oldImageName[0] = $uniqueimage;
			}
        
            /* save image */
        	if ($model->imageFile) {
  	     	 		$imageName = $oldImageName[0];
        			$model->imgBeaconLoc = 'uploads/'.$imageName.'.'.$model->imageFile->extension;
       			
        			$model->imageFile->saveAs('uploads/'.$imageName.'.'.$model->imageFile->extension);
          	 }
          	 /* update multiple beacon */
			if(is_array($model->groups)){
			$beaconid = implode(', ',$_POST['BeaconManager']['groups']);
			$str = rtrim($beaconid,',');			
			$model->groups = $str;  	 }
			
			$model->locationOwnerID = Yii::$app->request->post()['BeaconManager']['locationOwnerID'];
			//$model->locationBrockerID = Yii::$app->request->post()['BeaconManager']['locationBrockerID'];
			//$model->intervalID = Yii::$app->request->post()['BeaconManager']['intervalID'];
			
			$model->save(false);  //  save all data in model
			
			
		       	     if($storeBeaconModel->store_id){
							Storebeacons::deleteAll('beacon_id ='.$id);
							
							$storeBeaconModel->store_id = $storeBeaconModel->store_id;
							$storeBeaconModel->beacon_id = $model->id;
							$storeBeaconModel->status = $model->status;
							$storeBeaconModel->save(false);
											
						}
											
					
						
						
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'storebeacon' => $storebeacon,
				'storeBeaconModel' => $storeBeaconModel,
            ]);
        }
    }

    /**
     * Deletes an existing Beaconmanager model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
		 Storebeacons::deleteAll('beacon_id ='.$id);
        return $this->redirect(['index']);
    }
    
    /* delete via ajax */   
	public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];  		
		if($pid) {
			$this->findModel($pid)->delete();
			Storebeacons::deleteAll('beacon_id ='.$pid);
			echo $pid; exit;
		}
	}
	
	public function actionStorelist()  
	{
	
	$partnerID = $_POST['id'];  
		if($partnerID) {
						 
			 $storebeaconlist =  ArrayHelper::map(Store::find()
				->joinWith('storebeacons', true, 'LEFT JOIN')
				->select('tbl_store_beacons.beacon_id ,tbl_store_beacons.store_id ,tbl_store.id,tbl_store.storename,tbl_store.status')
				->Where([Store::tableName().'.status'=>1, Store::tableName().'.owner'=>$partnerID, ])
			//	->andWhere('store_id IS NULL' )				
				->all(), 'id', 'storename');
				
			$st = array('data'=> $storebeaconlist);
			echo json_encode($st); exit;
		}
	
	}	
	
			public function actionBeaconGroupname() {
			$data=Yii::$app->request->post();
			
			$q = $data['q'];
			
			#$sq = is_array($data['sq'])?array_reverse($data['sq']):'';
			#$sq = is_array($sq)?array_shift($sq):'';
			$data['sq'] = is_array($data['sq'])?$data['sq']:'';
						
			$id = null;
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$out = ['results' => ['id' => '', 'text' => '']];
			if (!is_null($q)) {
				$query = new Query;
				$query->select('id, group_name AS text')
								->from('tbl_beacon_group')
								->where(['like','group_name', $q])->andWhere(['status' =>1]);
								
				if(is_array($data['sq'])){
					$beaconDetail =   Beacongroup::find()->where('id IN('.implode(',',$data['sq']).')')->all();
					$alreadyState=false;
					foreach($beaconDetail as $beaconType){					
						
				
					
						if($beaconType["type"]==1){								
								$query->andwhere(["!=",'type',1])
								->andWhere(('id!='.$beaconType["id"]))->andWhere(['status' =>1]);
										
						}elseif($beaconType["type"]==2){
							$query->andWhere('type in (3,0,4)') 
								->andWhere(["OR",'parent_id' => $beaconType["id"]])->andWhere(['status' =>1]);
								$alreadyState=true;
												
						}elseif($beaconType["type"]==3){
							$query->andWhere('type in (2,0,4)');
							if($alreadyState==false)							
							$query->andWhere(['id' => $beaconType["parent_id"]])->andWhere(['status' =>1]);
							else
							$query->andWhere(['OR','id' =>$beaconType["parent_id"],'type in (2,0,4)'])->andWhere(['status' =>1]);
							//->orWhere(['type'=>'0']);
								//->andWhere(['OR','id' =>$beaconType["grandparent_id"],'type'=>'0']);	
								
																	
											
						}elseif($beaconType["type"]==0){
							//$query->orWhere('type=0');
														
																	
											
						}
						
					
					
				}
				
				$query->limit(20);
				
				}else{ 
				$query->select('id, group_name AS text')
					->from('tbl_beacon_group')
					->where(['like', 'group_name', $q]) 
					->andwhere(['type' => [0,1]])->andWhere(['status' =>1])               
					->limit(20);
				}	
				
				$command = $query->createCommand();
				$data = $command->queryAll();
			   
				$out['results'] = array_values($data);        
			}
			elseif ($id > 0) {		
				$out['results'] = ['id' => $id, 'text' => Beacongroup::find($id)->group_name];
			}
				return $out;
			}
	
 

    /**
     * Finds the Beaconmanager model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Beaconmanager the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Beaconmanager::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
     protected function findbeaconstoreModel($id)
     {
		 
	if (($beaconstore = Storebeacons::findOne(['beacon_id'=>$id])) !== null) {
			
            return $beaconstore;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
     /*   if (($beaconstore = Storebeacons::findOne(['beacon_id'=>$id])) !== null) {
			
            return $beaconstore;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    } */
    
  
		
	 
    
}
