<?php

namespace backend\controllers;

use Yii;
use common\models\ReferralEarnings;
use common\models\User;
use backend\models\ReferralEarningsSearch;
use backend\models\ReferralCashamountSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReferralEarningController implements the CRUD actions for ReferralEarnings model.
 */
class ReferralEarningController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReferralEarnings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReferralEarningsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ReferralEarnings model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	// Refer user via email
	
	public function actionReferviaemail(){
		
		
		$model = new \yii\base\DynamicModel(['email']);
		$model->addRule('email', 'email');
		$model->addRule(['email'],'required');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
				
			$referCode = Yii::$app->user->identity->user_code;				
				
			$subject = "Register using Referral Code";
			$message=sprintf("Dear Friend,<br/> <br/>Join AlphaWallet today using my referral code : %s<br /><br/>Thanks<br /><br/>%s",$referCode,Yii::$app->user->identity->fullName);
			
			$emailsend =Yii::$app->mail->compose()
				->setTo($model->email)
				->setFrom(Yii::$app->params["salesEmail"],'No Reply')
				->setSubject($subject)
				->setHtmlBody($message);					
			if($emailsend->send()){
				$message =  'Referral email sent successfully';
				$type= 'success';
			}else{
				$type	= 'error';
				$message =  'Referral email sent failed';
			}
			\Yii::$app->getSession()->setFlash($type,$message);
			return $this->render('referviaemail', [
                'model' => $model,
            ]);
			
        } else {
            return $this->render('referviaemail', [
                'model' => $model,
            ]);
        }
	}

    /**
     * Deletes an existing ReferralEarnings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ReferralEarnings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReferralEarnings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReferralEarnings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionUserEarning(){
		$postData = Yii::$app->request->post();
		$query = Yii::$app->getDb();
		$custQuery = '';
		if(!empty($postData['userId'])){
			$custQuery .= ' where refer_by='.$postData['userId'];
		 }
		$usertype = Yii::$app->user->identity->user_type;
		$userId = $usertype == User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;

		if($usertype==User::ROLE_Advertiser && !Yii::$app->request->post()){
			$custQuery .= ' refer_by='.$userId;
		}
		
		$table = ReferralEarnings::tableName();
		$db = Yii::$app->getDb();
		$query = $db->createCommand("SELECT IFNULL(sum(commission_amount),0) as total FROM $table $custQuery;");
		return $query->queryScalar();
	}
	
	public function actionUserReferral(){
		 $searchModel = new ReferralCashamountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('user-referral', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
	}
	 public function actionViewReferral($id)
    {
        return $this->render('view-referral', [
            'model' => $this->findUsrRefModel($id),
        ]);
    }
	protected function findUsrRefModel($id)
    {
		  if (($model = \common\models\ReferralCashamount::findOne(['user_id'=>$id])) !== null) {
            return $model;
        } else {
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
	public function actionCashout(){
		
		 $userId = Yii::$app->request->get('id');
		$setting = \common\models\Setting::find()->orderBy(['id'=>SORT_DESC])->one();
		//print_r($setting);die;
		 $bankAcc = \common\models\Bankdetail::find()->select(['concat("Bank account -  ",account_name) as account_name','promise_acid'])
                  ->where(['partner_id'=>$userId])
                  ->andWhere('promise_acid is not null')
                  ->asArray()->all();
		$message =  '';
		$type= '';
        $model = $this->findUsrRefModel($userId);
       
        if(count($bankAcc)>0 && is_object($model)){		
				 $itemId = md5(time().'aw');			
				 $itemName = Yii::$app->user->identity->username;
				 $promiseUid = Yii::$app->user->identity->promise_uid;
				 $item = Yii::$app->promisePay->createItemCashout($itemId,$model->unused_commission,$promiseUid,$itemName);
				if(isset($item['id'])){
					$pay = Yii::$app->promisePay->referralCashOut($item['id']);
					if(isset($pay['id'])){
						//print_r($model->unused_commission - ($model->unused_commission* $setting->cashout/100));die;
						$model->used_commission = $model->used_commission+($model->unused_commission - ($model->unused_commission* $setting->cashout/100));
						$model->unused_commission = 0;	
						$model->save(false)	;				
						$message =  'Cash out success.';
						$type= 'success';
						Yii::$app->getSession()->setFlash($type,$message);		 
						return $this->redirect(['user-referral']);
					} else {
						$message =  'Cash out failed.';
						$type= 'error';
						Yii::$app->getSession()->setFlash($type,$message);		 
						return $this->redirect(['user-referral']);
					} 
					
				}else{
					
					$message =  'Cash out failed.';
					$type= 'error';
					Yii::$app->getSession()->setFlash($type,$message);		 
					return $this->redirect(['user-referral']);
				}
				
				

				
			
		} else {	
	
			$message =  'Please update your bank account for cashout';
			$type= 'error';
			Yii::$app->getSession()->setFlash($type,$message);	
			return $this->redirect(['user-referral']);
		}
		
		
	}

	
}
