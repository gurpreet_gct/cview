<?php

namespace backend\controllers;

use Yii;
use backend\models\GeoZone;
use backend\models\GeoZoneSearch;
use yii\web\Controller;
use common\models\Workorders;
use common\models\PolygonsRegion;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GeoController implements the CRUD actions for GeoZone model.
 */
class GeoController extends Controller
{
 /* public function behaviors(){
      
		$behaviors['access'] = [
			'class' => AccessControl::className(),
			'rules' => [
				[                       
				   'allow' => true,
				   'roles' => ['@'],
				   'matchCallback' => function ($rule, $action) {						    
					$module = Yii::$app->controller->module->id;                     
					$action = Yii::$app->controller->action->id;                         
					$controller = Yii::$app	->controller->id;                        
					$route = "$controller/$action";                                                    	
					$post = Yii::$app->request->post();
					if(\Yii::$app->user->can($route)){
						return true;
					}
					 
					} 
			   
				],
			],
		];
		
		return $behaviors;
		
	}*/

    /**
     * Lists all GeoZone models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GeoZoneSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GeoZone model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GeoZone model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GeoZone();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GeoZone model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
      /**
     * Finds the GeoZone model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GeoZone the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
protected function findModel($id)
    {
        if (($model = GeoZone::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * Deletes an existing GeoZone model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
public function actionDelete($id)  
  {
	        $this->findModel($id)->delete();
		 GeoZone::deleteAll('id ='.$id); 
		 PolygonsRegion::deleteAll('id ='.$id);
		return $this->redirect(['index']);
 } 
     
     
     
  public function actionDeletebyajax() 
 
  {
		$pid = $_POST['id']; 		
		if($pid) { 			
		 GeoZone::deleteAll('id ='.$pid); 
		 PolygonsRegion::deleteAll('id ='.$pid);
     
   }
       echo $pid; exit;
 }

  
    
}
