<?php

namespace backend\controllers;
use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use backend\models\Usermanagement;
use backend\models\ChangePin;
use common\models\Profile;
use common\models\UserLoyalty;
use common\models\Loyalty;
use common\models\StoreCategory;
use common\models\Workorders;
use common\models\WorkorderCategory;
use common\models\Workorderpopup;
use common\models\Store;
use common\models\Deal;
use app\models\UsersSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\db\Query;
use common\models\FoodCategories;
use common\models\FoodcategoryProducts;
use common\models\Userfoodcategory;
use common\models\TransactionsFees;
use common\models\PartnerFees;
use common\models\Bankdetail;


#use yii\data\Pagination;


/**
 * UsersController implements the CRUD actions for Users model.
 */
class PartnersController extends Controller
{
    
   
    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$catTree =StoreCategory::find()->select('category_id,name')
		->joinWith('categories',false)
		->where(['store_id'=>$id])
		->asArray()->all();
		
		$catTree=ArrayHelper::map($catTree,'category_id','name');
		$loyaltyData=Loyalty::find()->where(['user_id'=>$id,'status'=>1])->select('*')->orderBy(['id' => SORT_DESC,])->asArray()->all();
       
	   $foodcatTree =Userfoodcategory::find()->select('category_id,name')
		->joinWith('categories',false)
		->where(['store_id'=>$id])
		->asArray()->all();
		
		$foodCategoryTree = ArrayHelper::map($foodcatTree,'category_id','name');
		  
        return $this->render('view', [
            'model' => $this->findModel($id),
				'profile' => $this->findModeluserprofile($id),
				'categoryTree'=>$catTree,			
				'foodCategoryTree'=>$foodCategoryTree,			
				'loyaltyData'=>$loyaltyData,			
            
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
	 * require_once("../components/array_colmn.php");
     */
    public function actionCreate()
    {
		 $transList = TransactionsFees::getRules();
		 $transModel = new PartnerFees();	
		 	$setting =\common\models\Setting::find()->orderBy([
				   'id' => SORT_DESC,
					])->one();
         $model   = new Usermanagement();
         $profile = new Profile();
         $loyalty = new Loyalty();
     
         $profile->scenario ='create';
   		 $profile->usertype_id=isset(Yii::$app->request->post()['Usermanagement']['user_type'])&&(Yii::$app->request->post()['Usermanagement']['user_type']>0)?Yii::$app->request->post()['Usermanagement']['user_type']:0;
                
		 if ($transModel->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())   && $loyalty->load(Yii::$app->request->post()) && Model::validateMultiple([$model, $profile,$loyalty,$transModel])) {
			  
			  /* password hash */ 			    				
			$model->confirmpass= $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
		   /* insert value firstname and lastname by fullname */
			$model->fullName = $model->firstName.' '.$model->lastName;		   
		   	$model->orgName	= $profile->companyName;
		  		    		
		    /* save data profile model */
		    
		     if($model->save()) {
				// save transaction rule
				$transModel->partner_id	= $model->id;
				$transModel->save();
				/* auto assign rbac auth role */				
				Yii::$app->Permission->setPermission('partner',$model->id);
				
				 /* save categories in store categories table */
				 if(isset(Yii::$app->request->post()['categoryTree']))
						{
							$categoryTree=Yii::$app->request->post()['categoryTree'];	
							unset($categoryTree[0]);
							$catTree=[];
							foreach($categoryTree as $key=>$value)
							{
								array_push($catTree,array("category_id"=>$value,"store_id"=>$model->id,"status"=>1,'created_at'=>time(),'updated_at'=>time()));
							}
						   if(count($catTree))
								Yii::$app->db->createCommand()->batchInsert(StoreCategory::tableName(),array_keys($catTree[0]), $catTree)->execute();
						}
					/*save food category in tbl_userfoodcategory  *****************/			
					if(isset(Yii::$app->request->post()['foodcatname'])){
						$FoodCat = Yii::$app->request->post()['foodcatname'];
						$ids = implode(',',$FoodCat);
						$root =FoodCategories::find()->select('root')->where("id in($ids)")->asArray()->all();							
						$catTree=[];
						if($root){
							$rootIds = array_values(array_unique(array_column($root,'root')));
							$FoodCat = array_merge($FoodCat,$rootIds);
						}
						$FoodCat = array_unique($FoodCat); 
						foreach($FoodCat as $key=>$value)
							{
								array_push($catTree,array("category_id"=>$value,"store_id"=>$model->id,"status"=>1));
							}
						 if(count($catTree))
						   {
							    Yii::$app->db->createCommand()->batchInsert(Userfoodcategory::tableName(),array_keys($catTree[0]), $catTree)->execute();
						   }
					}					
						
				 	 $profile->user_id = $model->id; // no need for validation rule on user_id as you set it yourself
				 	 
				 	 /* upload brand logo  */
				 	 
				 	 /* brand description */
				 	 $profile->brandDescription = str_replace("\r\n","",$profile->brandDescription);
					
					
					/* save Brand background image */
					if ($profile->brandimgLogo) {
						$path = parse_url($profile->brandimgLogo, PHP_URL_PATH);						
						$profile->brandLogo=basename($path);
						
						
					}
						        
					
					
					/* save Brand background image */
					if ($profile->backgroundImage) {
						$path = parse_url($profile->backgroundImage, PHP_URL_PATH);						
						$profile->backgroundHexColour=basename($path);
						
					}
          	 					         
				  $profile->save(false);
				  
				   if($profile->addLoyalty==1){
					if(isset($loyalty->loyalty) && $loyalty->loyalty!='' && isset($loyalty->dolor) && $loyalty->dolor!='' && isset($loyalty->redeem_loyalty) && $loyalty->redeem_loyalty!='' && isset($loyalty->redeem_dolor) && $loyalty->redeem_dolor!='') {
					$loyalty->user_id = $model->id;
					$loyalty->pointvalue = $loyalty['pointvalue'];
					$loyalty->redeem_pointvalue = $loyalty['redeem_pointvalue'];
					$loyalty->status = '1';
					$loyalty->save(false);
					}
				}
				
				/* send account detail to partner user */
				$subject	= $_SERVER['HTTP_HOST']." : Partner Created";
				$email		= $model->email;
				$emailData  = ['uname'=> $model->username,
								'Name'=> $model->fullName,
								'Email'=> $model->email,
								'Usertype'=> 'partner',
								'Phone'=> $model->phone,
							];
							
				Yii::$app->mail->compose(['html' => 'partner-mail'],$emailData)
							->setTo($email)
							->setFrom(Yii::$app->params["noReplyEmail"],'No Reply')
							->setSubject($subject)
							->send(); 
				
		          return $this->redirect(['store/create?pid='.$model->id]);
		       }	     		       
					
		       
		    } else {
		        return $this->render('create', [
		            'model' => $model,
		            'userdetails' => $profile,
		            'setting'=>$setting,
		            'transList' => $transList,
		            'transModel' => $transModel,
		             'categoryTree' => $this->getCategoryTree(0),
		             'loyalty' => $loyalty,
					'foodCategoryTree' => $this->getCategoryTreeFood(0),
		        ]);
		    }

}

public function actions()
{
    return [
        'uploadPhoto' => [
            'class' => 'paras\cropper\actions\UploadAction',
            'url' => Yii::$app->urlManagerFrontEnd->createAbsoluteUrl(['profile']),
            'path' => '@frontend/web/profile',
        ]
    ];
}




    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$transList = TransactionsFees::getRules();
		$setting =\common\models\Setting::find()->orderBy([
				   'id' => SORT_DESC,
					])->one();
					//print_r($setting);die;
		$partnerId = '';
		$transModel = PartnerFees::findOne(['partner_id'=>$id]);
		if(!$transModel){
			$transModel = new PartnerFees();
		}
		
		  	$model = $this->findModel($id);
        	$profile = $this->findModelprofile(array('user_id'=>$id));
			//$loyalty = $this->findModelloyalty(array('user_id'=>$id));	
        	$loyalty = Loyalty::find()->where(['user_id' => $id ,'status'=>1])->orderBy(['id' => SORT_DESC])->one();	
        	
        	if(!$loyalty){
				$loyalty = new Loyalty();
				
			}
        if ($transModel->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) && $loyalty->load(Yii::$app->request->post())&& Model::validateMultiple([$model, $profile,$loyalty,$transModel])) {		
			$transModel->partner_id = $id;		
       	 	$transModel->save();	
       	 	  	/* update firstname and lastname by fullname */
        	  $model->fullName = $model->firstName.' '.$model->lastName;		
        	/* update user table org name */
        	  $model->orgName = $profile->companyName;
        	  $brandName = str_replace(" ","-",$profile->brandName);
        	 $uniqueimage = $brandName.substr(md5(date("Ymdhis")),0,15);             
 			/* get old file name */
 			
 			if(isset($profile->brandLogo) && $profile->brandLogo !='') {
			$oldImageName = explode(".",$profile->brandLogo); 
			}else { 
				$oldImageName[0] = $uniqueimage;
			}
			
			/* background new image name */
			$uniqueBackimg = $brandName.'_back'.substr(md5(date("Ymdhis")),0,15);  			 
 			 			
 			if(isset($profile->backgroundHexColour) && $profile->backgroundHexColour !='') {
			$oldBackimg = explode(".",$profile->backgroundHexColour); 
			}else { 
				$oldBackimg[0] = $uniqueBackimg;
			}
			
			if($profile->socialcheck!=1){
				$profile->socialmediaFacebook = '';
				$profile->socialmediaTwitter = '';
				$profile->socialmediaGoogle ='';
			
			}
			
			  $profile->brandDescription = str_replace("\r\n","",$profile->brandDescription);
			 
        	  /* save Brand background image */
					if ($profile->brandimgLogo) {						
						$path = parse_url($profile->brandimgLogo, PHP_URL_PATH);						
						$profile->brandLogo=basename($path);
						
					}
						        
					
					
					/* save Brand background image */
					if ($profile->backgroundImage) {
						
						$path = parse_url($profile->backgroundImage, PHP_URL_PATH);						
						$profile->backgroundHexColour=basename($path);
						
					}
          	 				
			    			   
						$model->save(false);
						
						if(isset(Yii::$app->request->post()['categoryTree']))
						{
							StoreCategory::deleteAll('store_id ='.$id);	
							$categoryTree=Yii::$app->request->post()['categoryTree'];
							unset($categoryTree[0]);
							$catTree=[];
							foreach($categoryTree as $key=>$value)
							{
								array_push($catTree,array("category_id"=>$value,"store_id"=>$model->id,"status"=>1));
							}
						   if(count($catTree))
						   {
							   $storeCategory =StoreCategory::find()->where(['store_id'=>$model->id])->select('category_id as id,status')->asArray()->all();

							   Yii::$app->db->createCommand()->batchInsert(StoreCategory::tableName(),array_keys($catTree[0]), $catTree)->execute();
						   }	
						}
				/*save food category in tbl_userfoodcategory  *****************/			
					if(isset(Yii::$app->request->post()['foodcatname'])){
						Userfoodcategory::deleteAll('store_id='.$model->id);	
						$FoodCat = Yii::$app->request->post()['foodcatname'];
						$ids = implode(',',$FoodCat);
						$root =FoodCategories::find()->select('root')->where("id in($ids)")->asArray()->all();							
						$catTree=[];
						if($root){
							$rootIds = array_values(array_unique(array_column($root,'root')));
							$FoodCat = array_merge($FoodCat,$rootIds);
						}
						$FoodCat = array_unique($FoodCat); 
						foreach($FoodCat as $key=>$value)
							{
								array_push($catTree,array("category_id"=>$value,"store_id"=>$model->id,"status"=>1));
							}
						 if(count($catTree))
						   {
							    Yii::$app->db->createCommand()->batchInsert(Userfoodcategory::tableName(),array_keys($catTree[0]), $catTree)->execute();
						   }
					}
        	if($model->save(false)){
        	   $profile->save(false);
        	  if($profile->addLoyalty==1){
				if(isset($loyalty->loyalty) && $loyalty->loyalty!='' && isset($loyalty->dolor) && $loyalty->dolor!='') 
				{
				 
				    $loyalty->updated_by= Yii::$app->user->id;
			    	$loyalty->updated_at= time();
			    	$loyalty->created_by= Yii::$app->user->id;
			    	$loyalty->created_at= time();
					$loyaltyid=Loyalty::find()->where(['user_id'=>$model->id])->select('*')->orderBy(['id' => SORT_DESC,])->asArray()->all();
				/***************** get loyalty value from table***************************/	 
					 if(isset($loyaltyid[0]['loyalty']) && $loyaltyid[0]['loyalty']!=''){
							$loyalty_value =  number_format($loyaltyid[0]['loyalty'],2); 
					}else{
							$loyalty_value=0;
						}
					if(isset($loyaltyid[0]['dolor']) && $loyaltyid[0]['dolor']!=''){
						$dolor_value =  number_format($loyaltyid[0]['dolor'],2); 
					}else{
						$dolor_value =0;
					}
					if(isset($loyaltyid[0]['redeem_loyalty']) && $loyaltyid[0]['redeem_loyalty']!=''){
							$redeem_loyalty =  number_format($loyaltyid[0]['redeem_loyalty'],2); 
					}else{
							$redeem_loyalty=0;
						}
						if(isset($loyaltyid[0]['redeem_dolor']) && $loyaltyid[0]['redeem_dolor']!=''){
							$redeem_dolor =  number_format($loyaltyid[0]['redeem_dolor'],2); 
					}else{
							$redeem_dolor=0;
						}
				 /******************** end here ****************************************/
				 /****** check post value ***********************************/		
					if(isset($loyalty->loyalty) && $loyalty->loyalty!=''){
						$loyalty_post_value =  number_format($loyalty->loyalty,2);
					}else{
						$loyalty_post_value= 0;
					}
					if(isset($loyalty->dolor) && $loyalty->dolor!=''){
						$dolor_post_value =  number_format($loyalty->dolor,2);
					}else{
						$dolor_post_value = 0;
					}
					if(isset($loyalty->redeem_loyalty) && $loyalty->redeem_loyalty!=''){
						$redeem_loyalty_post_value =  number_format($loyalty->redeem_loyalty,2);
					}else{
						$redeem_loyalty_post_value= 0;
					}
					if(isset($loyalty->redeem_dolor) && $loyalty->redeem_dolor!=''){
						$redeem_dolor_post_value =  number_format($loyalty->redeem_dolor,2);
					}else{
						$redeem_dolor_post_value= 0;
					}
				/**************** end here **************************************/	
					$condtion = 'user_id="'.$model->id.'"';
					if(($loyalty_value==$loyalty_post_value) && ($dolor_post_value==$dolor_value) && ($redeem_loyalty==$redeem_loyalty_post_value) && ($redeem_dolor==$redeem_dolor_post_value)){
						$loyalty->dolor;
						Yii::$app->db->createCommand()->update(Loyalty::tableName(), ['loyalty_pin' => $loyalty->loyalty_pin], $condtion)->execute();
					}else if(($loyalty_value!=$loyalty_post_value) || ($dolor_post_value==$dolor_value) || ($redeem_loyalty==$redeem_loyalty_post_value) || ($redeem_dolor==$redeem_dolor_post_value)){
						Yii::$app->db->createCommand()->update(Loyalty::tableName(), ['status' => 0], $condtion)->execute();
						Yii::$app->db->createCommand()->insert(Loyalty::tableName(),['loyalty'=>$loyalty->loyalty,'dolor'=>$loyalty->dolor,'pointvalue'=>$loyalty->pointvalue,'user_id'=>$model->id,'status'=>1,'created_at'=>$loyalty->created_at,'updated_at'=>$loyalty->updated_at,'created_by'=>$loyalty->created_by,'updated_by'=>$loyalty->updated_by,'loyalty_pin'=>$loyalty->loyalty_pin,'redeem_loyalty'=>$loyalty->redeem_loyalty,'redeem_dolor'=>$loyalty->redeem_dolor,'redeem_pointvalue'=>$loyalty->redeem_pointvalue])->execute();
						
					}else if(($loyalty_value==$loyalty_post_value) || ($dolor_post_value!=$dolor_value) || ($redeem_loyalty==$redeem_loyalty_post_value) || ($redeem_dolor==$redeem_dolor_post_value)){
						
						Yii::$app->db->createCommand()->update(Loyalty::tableName(), ['status' => 0], $condtion)->execute();
						Yii::$app->db->createCommand()->insert(Loyalty::tableName(),['loyalty'=>$loyalty->loyalty,'dolor'=>$loyalty->dolor,'pointvalue'=>$loyalty->pointvalue,'user_id'=>$model->id,'status'=>1,'created_at'=>$loyalty->created_at,'updated_at'=>$loyalty->updated_at,'created_by'=>$loyalty->created_by,'updated_by'=>$loyalty->updated_by,'loyalty_pin'=>$loyalty->loyalty_pin,'redeem_loyalty'=>$loyalty->redeem_loyalty,'redeem_dolor'=>$loyalty->redeem_dolor,'redeem_pointvalue'=>$loyalty->redeem_pointvalue])->execute();
						}
						else if(($loyalty_value==$loyalty_post_value) || ($dolor_post_value==$dolor_value) || ($redeem_loyalty!=$redeem_loyalty_post_value) || ($redeem_dolor==$redeem_dolor_post_value)){
						
						Yii::$app->db->createCommand()->update(Loyalty::tableName(), ['status' => 0], $condtion)->execute();
						Yii::$app->db->createCommand()->insert(Loyalty::tableName(),['loyalty'=>$loyalty->loyalty,'dolor'=>$loyalty->dolor,'pointvalue'=>$loyalty->pointvalue,'user_id'=>$model->id,'status'=>1,'created_at'=>$loyalty->created_at,'updated_at'=>$loyalty->updated_at,'created_by'=>$loyalty->created_by,'updated_by'=>$loyalty->updated_by,'loyalty_pin'=>$loyalty->loyalty_pin,'redeem_loyalty'=>$loyalty->redeem_loyalty,'redeem_dolor'=>$loyalty->redeem_dolor,'redeem_pointvalue'=>$loyalty->redeem_pointvalue])->execute();
						}
						else if(($loyalty_value==$loyalty_post_value) || ($dolor_post_value==$dolor_value) || ($redeem_loyalty==$redeem_loyalty_post_value) || ($redeem_dolor!=$redeem_dolor_post_value)){
						
						Yii::$app->db->createCommand()->update(Loyalty::tableName(), ['status' => 0], $condtion)->execute();
						Yii::$app->db->createCommand()->insert(Loyalty::tableName(),['loyalty'=>$loyalty->loyalty,'dolor'=>$loyalty->dolor,'pointvalue'=>$loyalty->pointvalue,'user_id'=>$model->id,'status'=>1,'created_at'=>$loyalty->created_at,'updated_at'=>$loyalty->updated_at,'created_by'=>$loyalty->created_by,'updated_by'=>$loyalty->updated_by,'loyalty_pin'=>$loyalty->loyalty_pin,'redeem_loyalty'=>$loyalty->redeem_loyalty,'redeem_dolor'=>$loyalty->redeem_dolor,'redeem_pointvalue'=>$loyalty->redeem_pointvalue])->execute();
						}
					else{
						
						Yii::$app->db->createCommand()->update(Loyalty::tableName(), ['status' => 0], $condtion)->execute();
				    Yii::$app->db->createCommand()->insert(Loyalty::tableName(),['loyalty'=>$loyalty->loyalty,'dolor'=>$loyalty->dolor,'pointvalue'=>$loyalty->pointvalue,'user_id'=>$model->id,'status'=>1,'created_at'=>$loyalty->created_at,'updated_at'=>$loyalty->updated_at,'created_by'=>$loyalty->created_by,'updated_by'=>$loyalty->updated_by,'loyalty_pin'=>$loyalty->loyalty_pin,'redeem_loyalty'=>$loyalty->redeem_loyalty,'redeem_dolor'=>$loyalty->redeem_dolor,'redeem_pointvalue'=>$loyalty->redeem_pointvalue])->execute();
				      }
					}
				}else{
					$condtion = 'user_id="'.$model->id.'"';
					Yii::$app->db->createCommand()->update(Loyalty::tableName(), ['status' => 0], $condtion)->execute();
					}
					//$loyalty->save(false);
        	   }
        	  Yii::$app->getSession()->setFlash('success', "Your profile has been updated successfully"); 
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
            'model' => $model,
            'userdetails' => $profile,
            'transList' => $transList,
			'transModel' => $transModel,
            'categoryTree' => $this->getCategoryTree($id),
             'loyalty' =>  $loyalty,
			 'foodCategoryTree' => $this->getCategoryTreeFood($model->id),
			 'setting' =>$setting,
        ]);
        }
    }
    
    
    /* change partner password  */
	public function actionChangepassword($id) {	
			
		$model= new ChangePin; 
		$partner = Usermanagement::findOne($id);  
		

        if ($model->load(Yii::$app->getRequest()->getBodyParams()) && $model->validate() && $model->newPassword!='' && $partner->password_hash!='') {        	
			
			 $model->newPassword = Yii::$app->security->generatePasswordHash($model->newPassword);
			 $partner->password_hash =  $model->newPassword;			 
			 $partner->save(false);
			Yii::$app->getSession()->setFlash('success', "Your new PIN has been updated");       
		  return $this->redirect(['view', 'id' => $partner->id]);
	
		
		}else{
		 return $this->render('changepassword', [
                'model' => $model,
            ]);	
		
		}	
		 
	}

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		
		if($id) {
			
			// delete from user management
			
			$reslt = $this->findModel($id)->delete();
			
			if($reslt){
				// delete from profile,Loyalty,UserLoyalty
				$profileData = Profile::find()->select('id')->where(['user_id'=>$id])->one();
				
				if($profileData){
					
					Profile::deleteAll('user_id ='.$id);
				}
				
				$Loyalty	 = Loyalty::find()->where(['user_id'=>$id])->one();
				
				if($Loyalty){
					
					Loyalty::deleteAll('user_id ='.$id);
				}
				
				$UserLoyalty	= UserLoyalty::find()->where(['user_id'=>$id])->one();
				
				if($UserLoyalty){
					
					UserLoyalty::deleteAll('store_id ='.$id);
				}
				
				
				$store	= store::find()->where(['owner'=>$id])->one();
				
				if($store){
					
					store::deleteAll('owner ='.$id);
				}
				
				// get all workorder of partner 
				
				$workordersIds	= workorders::find()->select('id')->where(['workorderpartner'=>$id])->asArray()->all();
				
				$workOrderId = [];
				
				if(!empty($workordersIds) && is_array($workordersIds)){
					
					foreach($workordersIds as $workordId){
						
						$workOrderId[] = $workordId['id'];
						
					}
					
				}
				
				if (!empty($workOrderId)) {
					
					$workOrderIds = implode(', ', $workOrderId);
					
					workorders::deleteAll('id IN (' . $workOrderIds . ')');
					Workorderpopup::deleteAll('workorder_id IN (' . $workOrderIds . ')');
					Deal::deleteAll('workorder_id IN (' . $workOrderIds . ')');
					WorkorderCategory::deleteAll('workorder_id IN (' . $workOrderIds . ')');
				}
				
			}
			
		}
			
		return $this->redirect(['index']);
		
    }
    
     
 /* delete by ajax */   
  public function actionDeletebyajax() 
  {
		$pid = $_POST['id']; 	 
		
		if($pid) {
			
		// delete from user management
		
        $this->findModel($pid)->delete();
		
		// delete from profile,Loyalty,UserLoyalty
		$profileData = Profile::find()->select('id')->where(['user_id'=>$pid])->one();
		
		if($profileData){
			
			Profile::deleteAll('user_id ='.$pid);
		}
		
		$Loyalty	 = Loyalty::find()->where(['user_id'=>$pid])->one();
		
		if($Loyalty){
			
			Loyalty::deleteAll('user_id ='.$pid);
		}
		
		$UserLoyalty	= UserLoyalty::find()->where(['user_id'=>$pid])->one();
		
		if($UserLoyalty){
			
			UserLoyalty::deleteAll('store_id ='.$pid);
		}
		
		
		$store	= store::find()->where(['owner'=>$pid])->one();
		
		if($store){
			
			store::deleteAll('owner ='.$pid);
		}
		
		// get all workorder of partner 
		
		$workordersIds	= Workorders::find()->select('id')->where(['workorderpartner'=>$pid])->asArray()->all();
		
		$workOrderId = [];
		
		if(!empty($workordersIds) && is_array($workordersIds)){
			
			foreach($workordersIds as $workordId){
				
				$workOrderId[] = $workordId['id'];
				
			}
			
		}
		
		if (!empty($workOrderId)) {
			
			$workOrderIds = implode(', ', $workOrderId);
			
			workorders::deleteAll('id IN (' . $workOrderIds . ')');
			Workorderpopup::deleteAll('workorder_id IN (' . $workOrderIds . ')');
			Deal::deleteAll('workorder_id IN (' . $workOrderIds . ')');
			WorkorderCategory::deleteAll('workorder_id IN (' . $workOrderIds . ')');
		}
		
       echo $pid; exit;
	   
	}
 }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
     
        $usertype = Yii::$app->user->identity->user_type;		
		if($usertype == 1){
			$model = Usermanagement::findOne($id);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
			
		} else {
			$model = Usermanagement::findOne(['id'=>Yii::$app->user->id ]);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
    }
     protected function findModelprofile($id)
    {
      
        $usertype = Yii::$app->user->identity->user_type;		
		if($usertype == 1){
			$model = Profile::findOne($id);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
			
		} else {
			$model = Profile::findOne(['user_id'=>Yii::$app->user->id ]);
			 if (($model) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
    }
    
    protected function findModelloyalty($id)
    {
		
        	if (($loyalty = Loyalty::find()->where(['user_id' => $id])->orderBy(['id' => SORT_DESC])->one()) !== null) { 
        	//if (($loyalty = Loyalty::findOne($id)) !== null) { 
        	
            return $loyalty;
        } else {
			return true;
            //throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
     protected function findModeluserprofile($id)
    {
        		if (($profile = Profile::find()->where('user_id ="'.$id.'"' )->all()) !== null) { 
        	
            return $profile;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /* set status */
    public function actionSetstatus()  
	{
		$id = $_POST['id']; 
		$valueid = $_POST['value']; 
		$model = $this->findModel($id);		
		$model->status = $valueid;	
			
		$model->save(false);		
		echo $valueid; exit;
		
	}
 
    
    protected function getCategoryTree($store_id)
	//public function actionTree()
	{
		
		$requiredResult=array();
	  
		$query = new Query;
		$query	->select(['tbl_categories.id as id','name as title','tbl_categories.id as value',  '(lvl+2) as level','lft','ifnull(status,0) as checked'])  
				->from('tbl_categories')
				->leftJoin('tbl_storeCategory', 'tbl_storeCategory.category_id = tbl_categories.id and tbl_storeCategory.store_id='.$store_id)
				//->limit(2)
				->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC]); 	
		$command = $query->createCommand();
		$data = $command->queryAll();
			$requiredResult=array();
		$oldLevel=2;
		$levelKeys=array();
	
		foreach($data as $key=>$value)
		{
			//$value['path']="root/";
			$key=$value['id'];
            $value['title']=htmlspecialchars_decode($value['title']);
           $value['has_children']=false;
           $value['children']=[];
		   $value['checked']=$value['checked']==1?true:false;
		   
			if($value['level']==2)
			{
		//	$value['path']=$value['path'].$value['name'];
			$requiredResult[$key]=$value;
				
			}
			else
			{	
				$array_ptr=&$requiredResult;
				
				for($i=2; $i<$value['level']; $i++)
				{
					//$value['path']= $array_ptr[ strval($levelKeys[$i])]['path'];						 
					 $array_ptr[ strval($levelKeys[$i]) ]['has_children']=true;
					 $array_ptr = &$array_ptr[ strval($levelKeys[$i]) ]['children'];
					 
					 
				}
			//	$value['path']=$value['path']."/".$value['name'];
				$array_ptr[$key]=$value;
			//print_r($requiredResult);die;
			}            
    
			$levelKeys[$value['level']]=$value['id'];			
		}	
		
		$this->recursiveRemoval($requiredResult,'children');
      $requiredResult=is_array($requiredResult)?array_values($requiredResult):$requiredResult;
	  	$data=array('id'=>0,'value'=>0,'title'=>'Root','level'=>1,'has_children'=>true,'children'=>$requiredResult);
		return (json_encode($data));

	}
	
	
	  public $i=0;  
protected function recursiveRemoval(&$array, $val)
{
    if(is_array($array))
    {
        foreach($array as $key=>&$arrayElement)
        {
            if(is_array($arrayElement))
            {
                if($key===$val)
                { 
            $this->i++;
            
             if($this->i==2)
             {//    print_r($arrayElement);echo $key;
         }
            $arrayElement=array_values($arrayElement);
/*if($this->i==2)                   
{ 
echo ($key===$val);
   echo $key;
echo $val;

                    print_r($arrayElement);
                    
die;}*/
            }
                $this->recursiveRemoval($arrayElement, $val);
            }
            else
            {
                if($key == $val)
                {
                    //print_r($arrayElement);
                }
            }
        }
    }
}
    
/* get list of food category */
	protected function getCategoryTreeFood($workorder_id)	
	{	
		$query = new Query;	
		$query	->select([FoodCategories::tableName().'.id as id','name as title',FoodCategories::tableName().'.id as value',  '(lvl+2) as level','lft','ifnull(status,0) as checked'])  
				->from(FoodCategories::tableName())
				->leftJoin(Userfoodcategory::tableName(),Userfoodcategory::tableName().'.category_id ='.FoodCategories::tableName().'.id and '.Userfoodcategory::tableName().'.store_id='.$workorder_id)
				->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC]); 	
			$command = $query->createCommand();
		
		$data = $command->queryAll();
		$requiredResult=array();
		$oldLevel=2;
		$levelKeys=array();
	
		foreach($data as $key=>$value)
		{
			//$value['path']="root/";
			$key=$value['id'];
            $value['title']=htmlspecialchars_decode($value['title']);
           $value['has_children']=false;
           $value['children']=[];
		   $value['checked']=$value['checked']==1?true:false;
		   
			if($value['level']==2)
			{
		
				$requiredResult[$key]=$value;
				
			}
			else
			{	
				$array_ptr=&$requiredResult;
				
				for($i=2; $i<$value['level']; $i++)
				{
					//$value['path']= $array_ptr[ strval($levelKeys[$i])]['path'];						 
					 $array_ptr[ strval($levelKeys[$i]) ]['has_children']=true;
					 $array_ptr = &$array_ptr[ strval($levelKeys[$i]) ]['children'];
					 
					 
				}
			//	$value['path']=$value['path']."/".$value['name'];
				$array_ptr[$key]=$value;
			//print_r($requiredResult);die;
			}            
    
			$levelKeys[$value['level']]=$value['id'];			
		}	
		
		$this->recursiveRemovalfood($requiredResult,'children');
      $requiredResult=is_array($requiredResult)?array_values($requiredResult):$requiredResult;
	  	$data=array('id'=>0,'value'=>0,'title'=>'Root','level'=>1,'has_children'=>true,'children'=>$requiredResult);
		return (json_encode($data));

	}
	
	protected function recursiveRemovalfood(&$array, $val)
	{
		if(is_array($array))
		{
			foreach($array as $key=>&$arrayElement)
			{
				if(is_array($arrayElement))
				{
					if($key===$val)
					{ 
				$this->i++;
				
				 if($this->i==2)
				 {//    print_r($arrayElement);echo $key;
			 }
				$arrayElement=array_values($arrayElement);

				}
					$this->recursiveRemoval($arrayElement, $val);
				}
				else
				{
					if($key == $val)
					{
						//print_r($arrayElement);
					}
				}
			}
		}
	}
	// add transaction rule
	 public function actionTransaction(){
		$this->layout='zoneiframe';
		$transList = TransactionsFees::getRules();
		$partnerId = '';
		$model = PartnerFees::find()
								->where(['partner_id'=>Yii::$app->request->get('partnerid')])
								->one();
		if(isset($model->partner_id)){
			$partnerId = $model->partner_id;	
		}else{
			$partnerId = Yii::$app->request->get('partnerid')!='' ? Yii::$app->request->get('partnerid') : Yii::$app->request->get('workid');
			$model = new PartnerFees();	
			
		}	
		if($model->load(Yii::$app->request->post()) && $model->validate()){	
			$model->partner_id = $partnerId;			
			$model->save(false);
			$profile = Profile::findOne(['user_id'=>$partnerId]);
			if($profile){
				$profile->transactionrule =1;
				$profile->save(false);
			}
			if($model->amount_1=='' && $model->amount_2=='' && $model->amount_3=='' && $model->amount_4=='' && $model->amount_5=='' && $model->amount_6=='' && $model->amount_7=='') {
				$message = 'You have not specified any transaction rule.';
				$type = 'error';
			}else{
				$message = 'Transaction rule saved.';
				$type = 'success';	
			}
			Yii::$app->getSession()->setFlash($type,$message);       	
			return $this->render('transform', [
				'model' => $model,
				'transList' => $transList,
			]);	
		}
		else{
			return $this->render('transform', [
				'model' => $model,
				'transList' => $transList,
			]);
			
		}
	}
	// addget transaction general rule
	public function actionGeneralFees(){		
		$id = Yii::$app->request->post('id');
		$model = TransactionsFees::findOne($id);
		if(is_object($model)){
			$data = ['id'=>$model->id,'charge_type'=>$model->charge_type,'amount'=>$model->amount,'paid_within'=>$model->paid_within];
			return json_encode($data);
		}
		return false;
	}

	// add bank account
	public function actionBankacc(){
		$this->layout='zoneiframe';
		$model = new Bankdetail();
		// check if user have already registered bank acc
		
		if($model->load(Yii::$app->request->post()) && $model->validate()){	
			
		} 
		else { 
			return $this->render('_bankform', [
				'model' => $model
			]);
		}
	}
}
