<?php
namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\Categories;
use yii\filters\AccessControl;

/**
 * CategoriesController implements for Categories model.
 */
 
class CategoriesController extends \yii\web\Controller {
	
   public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionGetallcategories (){
		
		 return $this->render('getallcategories', [
            'model' => $this->findCategories(),
        ]);
	
		
	}
	
	protected function findCategories()
	{
		
        if (($model = Categories::find()->orderBy(['root' => SORT_ASC, 'lft' => SORT_ASC,])->all()) !== null) {
			                     
			return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}



	
