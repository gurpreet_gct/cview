<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\base\Model;
use common\models\Workorders;
use common\models\Workorderpopup;
use common\models\Deal;
use common\models\Product;
use common\models\Profile;
use common\models\User;
use backend\models\Dealstore;
use backend\models\WorkordersSearch;
use backend\models\Beacongroup;
use backend\models\GeoZone;
use backend\models\Usermanagement;
use common\models\PolygonsRegion;
use common\models\WorkorderCategory;
use common\models\Categories;
use common\models\StoreCategory;
use common\models\StampcardQr;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;
use yii\imagine\Image;
use Imagine\Image\Box;
use dosamigos\qrcode\QrCode;
use yii\db\Query;
use mPDF;
use yii\db\Expression;
/**
 * WorkordersController implements for Workorders model.
 */
class WorkordersController extends Controller
{

 /*  public function behaviors(){

		$behaviors['access'] = [
			'class' => AccessControl::className(),
			'rules' => [
				[
				   'allow' => true,
				   'roles' => ['@'],
				   'matchCallback' => function ($rule, $action) {
					$module = Yii::$app->controller->module->id;
					$action = Yii::$app->controller->action->id;
					$controller = Yii::$app	->controller->id;
					$route = "$controller/$action";
					$post = Yii::$app->request->post();
					if(\Yii::$app->user->can($route)){
						return true;
					}

					}

				],
			],
		];

		return $behaviors;

	}*/



    /**
     * Lists all Workorders models.
     * @return mixed
     */

    public function actionIndex(){

        $searchModel = new WorkordersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateqr($id)
    {	 $this->layout='qrcode';
		 return $this->render('qrimages', [
            'model' => $this->findModel($id),
        ]);


	}

    /* Export QR Code For Digital Stamp Card  */
    public function actionExportqr($id)
    {
	 $model = $this->findModel($id);
	 if(isset($model->name)){
	  $pdfname = $model->name;
	  }else{
		$pdfname = 'digital-stampcard';
	 }

	  $qrdata = StampcardQr::find()->where(['workorder_id' => $id])->all();
       if(!empty($qrdata)){
          foreach($qrdata as $qrvalue){

		   QrCode::png($qrvalue->qr_code,Yii::$app->basePath.'/web/qrcodes/'.$qrvalue->qr_code.".png");
		  }
		}
	$content = $this->actionCreateqr($id);

		 $mpdf=new mPDF();

			$mpdf->WriteHTML($content);
			$mpdf->Output($pdfname.'.pdf', 'D');
			exit;


	}





    /* copy workorder */
    public function actionCopy()
    {

			$id =  $_GET['id'];
			$model = $this->findModel($id); // record that we want to duplicate
			unset($model->id);
			$model->name = $_GET['camname'];
			$model->status = 2;
			$model->isNewRecord = true;
			if($model->save(false)){
				/* copy geo zone */
				$this->copyZone($id,$model->id);
				$offermodel = $this->findoffermodel(array('workorder_id'=>$id));
				 unset($offermodel->id);
				 if($offermodel->product_id!=''){
				 unset($offermodel->product_id);
				}
				if($offermodel->product_id_combo!=''){
				 unset($offermodel->product_id_combo);
				}
				if($offermodel->passUrl!=''){
					$passUrl = explode('/',$offermodel->passUrl);
					$offermodel->passUrl = end($passUrl);					
				}
				
				 $offermodel->workorder_id = $model->id;
				 $offermodel->isNewRecord = true;
				 if($offermodel->save(false)){
					 $dealmodel = $this->finddealmodel(array('workorder_id'=>$id));
					 $old_deal_id = $dealmodel->id;
					  unset($dealmodel->id);
				 $dealmodel->workorder_id = $model->id;
				 $dealmodel->isNewRecord = true;
				  if($dealmodel->save(false)){
				 $catTree = $this->findcatmodel(array('workorder_id'=>$id));
				 $DealStore = $this->findstoredealmodel(array('deal_id'=> $old_deal_id));

				 if(!empty($DealStore[0]->deal_id)){
					 foreach ($DealStore as $newdealstore){
						unset($newdealstore->deal_id);
						$newdealstore->deal_id = $dealmodel->id;
						$newdealstore->isNewRecord = true;
						$newdealstore->save(false);
						}

					}

				 if(!empty($catTree[0]->id)) {
					foreach ($catTree as $categories){
						unset($categories->id);
						$categories->workorder_id = $model->id;
						$categories->isNewRecord = true;
						$categories->save(false);

						}
						  return $this->redirect(['index']);
					}else{

								  return $this->redirect(['index']);
						}
				}

			}

			}



	}

public function copyZone($oldDealid,$deal_id)
 {
	 $zoneModel = GeoZone::find()->where('deal_id='.$oldDealid )->asArray()->all();
	foreach($zoneModel as $_zoneModel){
		$geomodel = new GeoZone();
		$geomodel->isNewRecord = true;
		$geomodel->region_id 	=	$_zoneModel['region_id'];
		$geomodel->area_name 	=	$_zoneModel['area_name'];
		$geomodel->latitude 	= 	$_zoneModel['latitude'];
		$geomodel->longitude	=	$_zoneModel['longitude'];
		$geomodel->deal_id 		=	$deal_id;
		$geomodel->agency_id	=	$_zoneModel['agency_id'];
		$geomodel->created_at	=	time();
		$geomodel->updated_at	=	time();
		$geomodel->save(false);

	}
	 $polygensModel = PolygonsRegion::find()->where('deal_id='. $oldDealid )->asArray()->all();

	foreach($polygensModel as $_polygensModel){
		$polygensmodel = new PolygonsRegion();
		$polygensmodel->isNewRecord = true;
		$polygensmodel->deal_id 		= $deal_id;
		$polygensmodel->polygon_shape 	= $_polygensModel['polygon_shape'];
		$polygensmodel->message_limit 	= $_polygensModel['message_limit'];
		$polygensmodel->created_at 		= time();
		$polygensmodel->updated_at 		= time();
		$polygensmodel->save(false);

	}

 }
	/* copy psa */
    public function actionCopypsa($id)
			{

   	        $id =  $_GET['id'];
			$model = $this->findModel($id); // record that we want to duplicate
			 unset($model->id);
			$model->name = $_GET['camname'];
			$model->status = 2;
			$model->isNewRecord = true;
			if($model->save(false)){
				$offermodel = $this->findoffermodel(array('workorder_id'=>$id));
				 unset($offermodel->id);
				 $offermodel->workorder_id = $model->id;
				 $offermodel->isNewRecord = true;
				 if($offermodel->save(false)){
					 $catTree = $this->findcatmodel(array('workorder_id'=>$id));

				 if(!empty($catTree[0]->id)) {
					foreach ($catTree as $categories){
						unset($categories->id);
						$categories->workorder_id = $model->id;
						$categories->isNewRecord = true;
						$categories->save(false);
						}
						  return $this->redirect(['index']);
					}else{

								  return $this->redirect(['index']);
							}

					}

				}


		}


    /**
     * Displays a single Workorders model.
     * @param integer $id
     * @return mixed
     */
   public function actionView($id){

	$catTree	=	WorkorderCategory::find()->select('category_id,name')
										->joinWith('categories',false)
										->where(['workorder_id'=>$id])
										->asArray()
										->all();

		$catTree	=	ArrayHelper::map($catTree,'category_id','name');

		 $dealModel  = $this->finddealmodel(array('workorder_id'=>$id));
		 $offerModel = $this->findoffermodel(array('workorder_id'=>$id));

		if(isset($dealModel->id) && isset($offerModel->id)){
	       return $this->render('view', [
									'model' => $this->findModel($id), /* view for campaign */
									'offermodel' =>$offerModel, /* view for offer model */
									'dealmodel' => $dealModel, /* view for deal model */
									'categoryTree'=>$catTree,
							]);
		}else if(isset($offerModel->id) && !isset($dealModel->id)){
				 return $this->render('view', [
									'model' => $this->findModel($id), /* view for campaign */
									'categoryTree'=>$catTree,
									'offermodel' =>$offerModel,
									'dealmodel' =>'',
							]);
		}else if(!isset($offerModel->id) && isset($dealModel->id)){
			 return $this->render('view', [
									'model' => $this->findModel($id), /* view for campaign */
									'categoryTree'=>$catTree,
									'offermodel' =>'',
									'dealmodel' =>$dealModel,
							]);
		}else{
			 return $this->render('view', [
									'model' => $this->findModel($id), /* view for campaign */
									'categoryTree'=>$catTree,
									'offermodel' =>'',
									'dealmodel' =>'',
							]);
		}

    }

      /*preview pass pdf view*/
       public function actionPdfpass($id){
		 $this->layout='poppdf';

		$catTree	=	WorkorderCategory::find()->select('category_id,name')
										->joinWith('categories',false)
										->where(['workorder_id'=>$id])
										->asArray()
										->all();

		$catTree	=	ArrayHelper::map($catTree,'category_id','name');


        return $this->render('pdfpass', [
									'model' => $this->findModel($id), /* view for campaign */
									'offermodel' => $this->findoffermodel(array('workorder_id'=>$id)), /* view for offer model */
									'dealmodel' => $this->finddealmodel(array('workorder_id'=>$id)), /* view for deal model */
									'categoryTree'=>$catTree,
							]);

    }


    public function actionPdfpassexport($id)
    {
	$this->layout='poppdf';
	 $model = $this->findModel($id);
	 if(isset($model->name)){
		$pdfname = $model->name.'-Pass-'.date('Y-m-D');
	 }else{
		$pdfname = 'View-PDF-'.date('Y-m-D');
	 }

		$content = $this->actionPdfpass($id);
			$mpdf = new mPDF();
			#$mpdf->SetDisplayMode('fullpage');
			$mpdf->WriteHTML($content);

			$mpdf->Output($pdfname.'.pdf', 'D');
			exit;


	}

    /*popup pdf view*/
       public function actionPopupview($id){
		 $this->layout='poppdf';

		$catTree	=	WorkorderCategory::find()->select('category_id,name')
										->joinWith('categories',false)
										->where(['workorder_id'=>$id])
										->asArray()
										->all();

		$catTree	=	ArrayHelper::map($catTree,'category_id','name');


        return $this->render('popupview', [
									'model' => $this->findModel($id), /* view for campaign */
									'offermodel' => $this->findoffermodel(array('workorder_id'=>$id)), /* view for offer model */
									'dealmodel' => $this->finddealmodel(array('workorder_id'=>$id)), /* view for deal model */
									'categoryTree'=>$catTree,
							]);

    }


    public function actionPdfpopupexport($id)
    {
	#$this->layout='poppdf';
	 $model = $this->findModel($id);
	 if(isset($model->name)){
		$pdfname = $model->name.'-PopUp-'.date('Y-m-D');
	 }else{
		$pdfname = 'View-PDF-'.date('Y-m-D');
	 }

		$content = $this->actionPopupview($id);

		 $mpdf=new mPDF();

			$mpdf->WriteHTML($content);
			$mpdf->Output($pdfname.'.pdf', 'D');
			exit;


	}

     public function actionPdfview($id)
    {    $this->layout='qrcode';
		 $catTree=WorkorderCategory::find()->select('category_id,name')
		->joinWith('categories',false)
		->where(['workorder_id'=>$id])
		->asArray()->all();

			$catTree=ArrayHelper::map($catTree,'category_id','name');


        return $this->render('view', [
            'model' => $this->findModel($id), /* view for campaign */
            'offermodel' => $this->findoffermodel(array('workorder_id'=>$id)), /* view for offer model */
            'dealmodel' => $this->finddealmodel(array('workorder_id'=>$id)), /* view for deal model */
            'categoryTree'=>$catTree,
        ]);
    }

    public function actionPdfdetailexport($id)
    {
	 $model = $this->findModel($id);
	 if(isset($model->name)){
		$pdfname = $model->name.'-'.date('Y-m-D');
	 }else{
		$pdfname = 'View-PDF-'.date('Y-m-D');
	 }

	$content = $this->actionPdfview($id);

		 $mpdf=new mPDF();

			$mpdf->WriteHTML($content);
			$mpdf->Output($pdfname.'.pdf', 'D');
			exit;


	}




    /* ceate view for psa */

	public function actionViewpsa($id)
    {
		$catTree=WorkorderCategory::find()->select('category_id,name')
		->joinWith('categories',false)
		->where(['workorder_id'=>$id])
		->asArray()->all();

			$catTree=ArrayHelper::map($catTree,'category_id','name');

        return $this->render('viewpsa', [
            'model' => $this->findModel($id), /* view for campaign */
            'offermodel' => $this->findoffermodel(array('workorder_id'=>$id)), /* view for offer model */
            'categoryTree'=>$catTree,
        ]);

    }
 public function setZoneDealId($id,$dealid,$partner){
				$Polygonmodel = new GeoZone();
				$ret = $Polygonmodel->updateAll(array('deal_id' =>$dealid,'agency_id'=>$partner), 'deal_id='.$id);
					$Polygonmodel 	= new PolygonsRegion();
					$ret = $Polygonmodel->updateAll(array('deal_id' =>$dealid), 'deal_id='.$id);
}

    /**
     * Creates a new Workorders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Workorders();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
		
			// unset unwanted fields

			$beacons = isset($_POST['Workorders']['beaconGroup']) && !empty($_POST['Workorders']['beaconGroup'])?$_POST['Workorders']['beaconGroup']:[];
			$groupid 	= implode(', ',$beacons);
			$userType 	= implode(', ',$_POST['Workorders']['userType']);
			$AgeGroup 	= implode(', ',$_POST['Workorders']['agedGroup']);

			$model->advertiserID=$model->workorderpartner;
			$str = trim($groupid,",");
			$model->beaconGroup = $str;
			$model->userType = $userType;
			$model->agedGroup = $AgeGroup;

			$model->workorderType = 1; /* 1 for workorder and 2 for PSA */

			 $tempworid = Yii::$app->request->post()['tempworid'];

			if(isset(Yii::$app->request->post()['categoryTree']))
			{
				$categoryTree=Yii::$app->request->post()['categoryTree'];

				/* save data workorder table */

				$catTree = implode(', ',$categoryTree);
				$model->catIDs = $catTree;

				$reslt = $model->save(false);

				// save zone if has value
				if($reslt){
					$workorderId = Yii::$app->db->getLastInsertID();


					$latlong = Yii::$app->request->post('alllstaLongs');
					if(!empty($workorderId) && isset($latlong[0]) && !empty($latlong[0])){

						$allLatLong 	= 	$latlong[0];

						$this->savegeolocation($workorderId,$allLatLong);

					}

				}
				// save zone if has value

				unset($categoryTree[0]);
				$catTree=[];

				foreach($categoryTree as $key=>$value)
				{
					array_push($catTree,array("category_id"=>$value,"workorder_id"=>$model->id,"status"=>1,'created_at'=>time(),'updated_at'=>time()));
				}
			   if(count($catTree))
					Yii::$app->db->createCommand()->batchInsert(WorkorderCategory::tableName(),array_keys($catTree[0]), $catTree)->execute();
			}else{

					$reslt = $model->save(false);

				 /* *****************************************************/
					// save zone if has value
					if($reslt){
						$workorderId = Yii::$app->db->getLastInsertID();


						$latlong = Yii::$app->request->post('alllstaLongs');
						if(!empty($workorderId) && isset($latlong[0]) && !empty($latlong[0])){

							$allLatLong 	= 	$latlong[0];

							$this->savegeolocation($workorderId,$allLatLong);

						}

					}
					// save zone if has value

			}
			if($tempworid ){
				$this->setZoneDealId($tempworid,$model->id,$model->workorderpartner);
			}
				/*redirect form as according to type */
				if(Yii::$app->request->post("savenew")==1){
					return $this->redirect(['view', 'id' => $model->id]);
				}
				if($model->type==5){
					$this->redirect(['createstore','id' => $model->id,'step'=>2]);
				}else{
					$this->redirect(['createoffer','id' => $model->id,'step'=>2]);
				}



			} else {
            return $this->render('create', [
                'model' => $model,
                 'categoryTree' => $this->getCategoryTree(0),

            ]);
        }

    }


    /* create PSA */
     public function actionCreatepsa()

    {
        $model = new Workorders();
         //$model->scenario = 'createpsa';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

			$groupid = implode(', ',$model->beaconGroup	);
			$userType = implode(', ',$model->userType	);
			$AgeGroup = implode(', ',$model->agedGroup	);

			$str = rtrim($groupid,',');
			$model->beaconGroup = $str;
			$model->userType = $userType;
			$model->agedGroup = $AgeGroup;
			$model->workorderType = 2;


			if(isset(Yii::$app->request->post()['categoryTree']))
						{
							$categoryTree=Yii::$app->request->post()['categoryTree'];
							$catTree = implode(', ',$categoryTree);
							$model->catIDs = $catTree;
							$model->save(false);

							unset($categoryTree[0]);
							$catTree=[];

							foreach($categoryTree as $key=>$value)
							{
								array_push($catTree,array("category_id"=>$value,"workorder_id"=>$model->id,"status"=>1,'created_at'=>time(),'updated_at'=>time()));
							}
						   if(count($catTree))
								Yii::$app->db->createCommand()->batchInsert(WorkorderCategory::tableName(),array_keys($catTree[0]), $catTree)->execute();
						}else {

							$model->save(false);

							}

			$this->redirect(['createofferpsa','id' => $model->id ]);




			} else {
            return $this->render('createpsa', [
                'model' => $model,
                'categoryTree' => $this->getCategoryTree(0),

            ]);
        }
    }

     /* Create PSA offer model */

	   public function actionCreateofferpsa($id)
		{
		  $model = new Workorderpopup();
		    $model->scenario = 'create';

      if ($model->load(Yii::$app->request->post()) && $model->save()) {


			 $model->workorder_id = $id;
			  $model->iphoneNotificationText = $_POST['Workorderpopup']['iphoneNotificationText'];


             /* upload image */
				$uniqueimage = substr(md5(date("Ymdhis")),0,15);

        			if ($model->imageFile) {
  	     	 		$imageName = $uniqueimage;

         		$model->save();
         		/* update workorder_id in deal model for beacon detect  */
       			$dealmodel = new Deal();
       			$dealmodel->workorder_id = $id;
       			$dealmodel->save(false);


        			if ($model->imageFile) {
						$path = parse_url($model->imageFile, PHP_URL_PATH);
						$model->photo=basename($path);

						$model->save(false);
					}
          	 }


             return $this->redirect(['viewpsa', 'id' => $id]);

        } else {
            return $this->render('createofferpsa', [
                'model' => $model,

            ]);
        }
    }


    /* Create offer model */


	public function actionCreateoffer($id){
		$partnerProducts = $this->getPartnerProduct($id);
		$user_id = Yii::$app->user->id;

		if(Yii::$app->request->get("step")==2){
			 $model = new Workorderpopup(['scenario' => 'stepone']);
		} else {
			$model 	 = new Workorderpopup(['scenario' => 'create']);
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
  			$model->workorder_id = $id;
  			$model->save(false);
        /* save deal model */
        $deal = new Deal();
        $deal->workorder_id = $id;
        $deal->save(false);
  			if(Yii::$app->request->post("savenew")==2){
  				return $this->redirect(['updateoffer','id' => $id,'step'=>3]);
  			}
        return $this->redirect(['view','id' => $id]);
			} else {
            return $this->render('createoffer', [
                'model' => $model,
                'partnerProducts' => $partnerProducts

            ]);
        }
    }
    /* update offer */

 public function actionUpdateoffer($id){

  $partnerProducts = $this->getPartnerProduct($id);
  $dealmodel = $this->finddealmodel(array('workorder_id'=>$id));
  if(!is_object($dealmodel)){
    $dealmodel = new Deal();
    $dealmodel->workorder_id = $id;
    $dealmodel->save(false);
  }
  $user_id = Yii::$app->user->id;
  $offermodel = $this->findoffermodel(array('workorder_id'=>$id));
  $workorderModel = $this->findModel($id);

  if(isset($offermodel->passUrl) && strstr($offermodel->passUrl,'http')){
    if(strstr($offermodel->passUrl,'pass')){
      $passURL =  explode('pass/',$offermodel->passUrl);
      $offermodel->passUrl = $passURL[1];
    }elseif(strstr($offermodel->passUrl,'qr')){
      $passURL =  explode('qr/',$offermodel->passUrl);
      $offermodel->passUrl = $passURL[1];
    }
  }
  if(Yii::$app->request->get("step")==2){
    $offermodel->scenario ='stepone';
  } else {
    $offermodel->scenario ='create';
  }

  if($offermodel->load(Yii::$app->request->post()))
  {
      $offermodel->videoWorkorderName = UploadedFile::getInstance($offermodel,'videoWorkorderName');
    if($offermodel->validate()) {
      if(isset($_POST['Workorderpopup']['iphoneNotificationText'])) {
        $offermodel->iphoneNotificationText = $_POST['Workorderpopup']['iphoneNotificationText'];
      }
    /* logo upload */
    if ($offermodel->imageFilelogo) {
      $uniqueimagelogo = substr(md5(date("Ymdhis").'12'),0,15);
      if(isset($offermodel->logo) && $offermodel->logo !='') {
        $oldImageName = explode("workorder/",$offermodel->logo);
        $oldImageName = $uniqueimagelogo;
      }else {
        $oldImageName[0] = $uniqueimagelogo;
      }
      $imageName = $oldImageName[0];
      $imagePathpopup = $offermodel->imageFilelogo;
      if ($offermodel->imageFilelogo) {
        $path = parse_url($offermodel->imageFilelogo, PHP_URL_PATH);			$offermodel->logo=basename($path);
      }
    }

    if ($offermodel->videoWorkorderImage) {
      $uniqueimagelogo = substr(md5(date("Ymdhis").'12'),0,15);
      if(isset($offermodel->videoImage) && $offermodel->videoImage !='') {
        $oldImageName = explode("workordervideo/",$offermodel->videoImage);
        $oldImageName = $uniqueimagelogo;
      }else {
        $oldImageName[0] = $uniqueimagelogo;
      }
      $imageName = $oldImageName[0];
      $imagePathpopup = $offermodel->videoWorkorderImage;
      if($offermodel->videoWorkorderImage) {
        $path = parse_url($offermodel->videoWorkorderImage, PHP_URL_PATH);
        $offermodel->videoImage=basename($path);
      }
    }


    if (($offermodel->videoTypes==1) && $offermodel->videoWorkorderName) {
        $uniqueimagelogo = substr(md5(date("Ymdhis").'12'),0,15);
      if(isset($offermodel->videokey) && $offermodel->videoName !='') {
        $oldImageName = explode("workordervideo/",$offermodel->videokey);
        $oldImageName = $uniqueimagelogo;
      }else {
        $oldImageName[0] = $uniqueimagelogo;
      }
      $imageName = $oldImageName[0];
      $offermodel->videokey = Yii::$app->urlManagerBackEnd->createAbsoluteUrl(Yii::$app->params['workordervideo'] .'/'.$imageName.'.'.$offermodel->videoWorkorderName->extension);
      $imagePathpopup = Yii::$app->basePath.'/web/'.Yii::$app->params['workordervideo'].'/'.$imageName.'.'.$offermodel->videoWorkorderName->extension;
      $offermodel->videoWorkorderName->saveAs($imagePathpopup);
    }

    if ($offermodel->videoWorkorderUrl && $offermodel->videoWorkorderName=='') {
      $offermodel->videokey = $offermodel->videoWorkorderUrl;
    }

    if($offermodel->videoTypes==2)
    {
      $thumbvalue =  $offermodel->getVideothumbnail();
      $offermodel->videoImage = $thumbvalue ;
    }


      if($offermodel->uploadTypes==1) {
      $offermodel->videokey = '';
      $offermodel->videoName ='';
      /* main photo upload */
      if ($offermodel->imageFile) {
        $uniqueimage = substr(md5(date("Ymdhis")),0,15);
              /* get old file name */
        if(isset($offermodel->photo) && $offermodel->photo !='') {
        $oldImageName = explode("workorder/",$offermodel->photo);
        $oldImageName = $uniqueimage;

        }else {
          $oldImageName = $uniqueimage;
        }
        $imageName = $oldImageName;
        if ($offermodel->imageFile) {
          $path = parse_url($offermodel->imageFile, PHP_URL_PATH);
          $offermodel->photo=basename($path);
        }
    }
    }else{
      if($offermodel->videoTypes==1){
        //$offermodel->videokey = '';
      }
      if($offermodel->videoTypes==2){
        //$offermodel->videoImage ='';
        $offermodel->videoName ='';
      }
    }
    //18 Digital Stamp Card and Go to Store image save
    if($offermodel->imageFile) {
      $offermodel->videoImage ='';
      $uniqueimage = substr(md5(date("Ymdhis")),0,15);
      /* get old file name */
      if(isset($offermodel->photo) && $offermodel->photo !='') {
        $oldImageName2 = explode("workorder/",$offermodel->photo);
        $imageName = $oldImageName2[0];
      }else {
        $imageName = $uniqueimage.'.jpg';
      }
      $offermodel->photo = $imageName;
      if ($offermodel->imageFile) {
        $path = parse_url($offermodel->imageFilelogo, PHP_URL_PATH);
        $offermodel->videoName=basename($path);
      }
    }



    $offermodel->offerTitle = ucfirst($offermodel->offerTitle);
    /* if Workorders type is voucher */
    $wType = Workorders::find()->select('type')->where(['id' => $id])->one();
    if($wType->type==1){
      $offermodel->eventDate = '';
      $offermodel->ticketinfo = '';
    }elseif($wType->type==2){
    /* for make qr code */
    if(!empty($offermodel->customOfferSelect1) && ($offermodel->customOfferSelect2)){
    StampcardQr::deleteAll('workorder_id ='.$id);
    $totalQr = $offermodel->customOfferSelect1+$offermodel->customOfferSelect2;
    $qrdata = [];
    $x = 1;
    while($x <= $totalQr) {
      array_push($qrdata,array("workorder_id"=>$id,"qr_code"=>md5(rand(10,time()).'-'.$id),'created_at'=>time(),'updated_at'=>time(),'updated_by' => $user_id));
      $x++;
    }

    if(count($qrdata))
    Yii::$app->db->createCommand()->batchInsert(StampcardQr::tableName(),array_keys($qrdata[0]), $qrdata)->execute();
    $offermodel->offerText  = 'Buy ' .$offermodel->customOfferSelect1.' get '.$offermodel->customOfferSelect2.' FREE ';
    }
    $offermodel->sspMoreinfo =  strip_tags($offermodel->sspMoreinfo);

    }
    $offermodel->iphoneNotificationText = ucfirst($offermodel->iphoneNotificationText);
    if($offermodel->offertype==3){
      if($offermodel->offertext1==1){
        $offermodel->offerText  = 'Buy ' .$offermodel->customOfferSelect1.' get '.$offermodel->customOfferSelect2.' FREE ';
        $offermodel->offerwas = 0;
        $offermodel->offernow  =0;
        $offermodel->product_id_combo = '';
        $offermodel->comboProductType = '';

      }elseif($offermodel->offertext1==2){
        $offermodel->offerText  = 'Get '.$offermodel->customOfferSelect1.'% OFF';
        $offermodel->offerwas = 0;
        $offermodel->offernow  =0;
        $offermodel->product_id_combo = '';
        $offermodel->comboProductType = '';

      }elseif($offermodel->offertext1==3){

        $offermodel->offerText  = 'Enjoy '.$offermodel->customOfferSelect1.'% off on '.$offermodel->customOfferSelect2;
        $offermodel->offerwas = 0;
        $offermodel->offernow  =0;
        $offermodel->product_id_combo = '';
        $offermodel->comboProductType = '';
      }elseif($offermodel->offertext1==4){

        $offermodel->offerText  = 'Buy '.$offermodel->customOfferSelect1.' get '.$offermodel->customOfferSelect2.'% off';
        $offermodel->offerwas = 0;
        $offermodel->offernow  =0;
        $offermodel->product_id_combo = '';
        $offermodel->comboProductType = '';
      }elseif($offermodel->offertext1==5){

        $offermodel->offerText  = 'Buy '.$offermodel->customOfferSelect1.' for $'.$offermodel->customOfferSelect2.' -> Get '.$offermodel->customOfferSelect3.' for $'.$offermodel->customOfferSelect4;
        $offermodel->offerwas = 0;
        $offermodel->offernow  =0;
      }else{
        $offermodel->offerText  = '';
        $offermodel->offertext1  = '';
        $offermodel->offerwas = 0;
        $offermodel->offernow  =0;
        $offermodel->customOfferSelect1 = '';
        $offermodel->customOfferSelect2 = '';
        $offermodel->customOfferSelect3 = '';
        $offermodel->customOfferSelect4 ='';
      }
    }

    if($offermodel->offertype==1){
      $offermodel->offerText  = '';
      $offermodel->product_id_combo = '';
      $offermodel->comboProductType = '';
      $offermodel->offertext1  = '';
      $offermodel->customOfferSelect1 = '';
      $offermodel->customOfferSelect2 = '';
      $offermodel->customOfferSelect3 = '';
      $offermodel->customOfferSelect4 ='';
      $offermodel->offerwas = $offermodel->offerwas;
      $offermodel->offernow =$offermodel->offernow;
    }

    if($offermodel->buynow==0 && Yii::$app->request->get("step")==5){
      $offermodel->product_id ='';
    }

    $offermodel->readTerms = str_replace("\r\n","",$offermodel->readTerms);
    if($offermodel->uploadTypes==2){
      $offermodel->photo = '';
    }
    if($offermodel->uploadTypes==1){
      //$offermodel->videokey = '';
    }
    /* save model */
    $offermodel->save(false);
    /* save deal model same data */
    if($dealmodel) {
      $dealmodel->load(Yii::$app->request->post());
      $dealmodel->dealvideoWorkorderName = UploadedFile::getInstance($dealmodel,'dealvideoWorkorderName');
      if ($dealmodel->imageFile3) {
        $uniqueimagelogo = substr(md5(date("Ymdhis").'12'),0,15);
        if(isset($dealmodel->mainPhotoUpload) && $dealmodel->mainPhotoUpload !='') {
          $oldImageName = explode("workorder/",$dealmodel->mainPhotoUpload);
          $oldImageName = $uniqueimagelogo;
        }else {
          $oldImageName[0] = $uniqueimagelogo;
        }
        $imageName = $oldImageName[0];
        if ($dealmodel->imageFile3) {
          $path = parse_url($dealmodel->imageFile3, PHP_URL_PATH);
          $dealmodel->mainPhotoUpload=basename($path);
        }
      }
      if ($dealmodel->dealvideoWorkorderImage && $dealmodel->dealvideoTypes==1) {
        $uniqueimagelogo = substr(md5(date("Ymdhis").'12'),0,15);
      if(isset($dealmodel->dealvideoImage) && $dealmodel->dealvideoImage!='')
      {
        $oldImageName = explode("workorder/",$dealmodel->dealvideoImage);
        $oldImageName = $uniqueimagelogo;
      }else {
        $oldImageName[0] = $uniqueimagelogo;
      }
      $imageName = $oldImageName[0];
      if($dealmodel->dealvideoWorkorderImage) {
        $path = parse_url($dealmodel->dealvideoWorkorderImage, PHP_URL_PATH);
        $dealmodel->dealvideoImage=basename($path);
      }
      }
      if(($dealmodel->dealvideoTypes==1) && $dealmodel->dealvideoWorkorderName) {
        $uniqueimagelogo = substr(md5(date("Ymdhis").'12'),0,15);
        if(isset($dealmodel->dealvideokey)) {
          $oldImageName = explode("workorder/",$dealmodel->dealvideokey);
          $oldImageName = $uniqueimagelogo;
        }else {
          $oldImageName[0] = $uniqueimagelogo;
        }
        $imageName = $oldImageName[0];
        $dealmodel->dealvideokey = $imageName.'.'.$dealmodel->dealvideoWorkorderName->extension;
        $imagePathpopup = Yii::$app->params['workorderimage'].'/'.$imageName.'.'.$dealmodel->dealvideoWorkorderName->extension;
        $dealmodel->dealvideoWorkorderName->saveAs($imagePathpopup);
      }
      if (($dealmodel->dealvideoTypes==1) && $dealmodel->dealvideoWorkorderUrl && $dealmodel->dealvideoWorkorderName=='') {
        $dealmodel->dealvideokey = $dealmodel->dealvideoWorkorderUrl;
      }
      if($dealmodel->dealvideoTypes==2)
      {
        $thumbvalue =  $dealmodel->getVideothumbnail();
        $dealmodel->dealvideoImage = $thumbvalue ;
      }
      if(strstr($dealmodel->frontPhotoUpload,'http')){
        $frontPhotoUpload = explode("workorder/",$dealmodel->frontPhotoUpload);
        $dealmodel->frontPhotoUpload = $frontPhotoUpload[1];
      }
      if(strstr($dealmodel->logoUpload,'http')){
        $logoUpload = explode("workorder/",$dealmodel->logoUpload);
        $dealmodel->logoUpload = $logoUpload[1];
      }
      if($dealmodel->dealuploadTypes==1) {
        $dealmodel->dealvideoImage = '';
        $dealmodel->dealvideokey = '';
        $dealmodel->dealvideoImage= '';
      }else{
        $dealmodel->mainPhotoUpload = '';
      }

      $dealmodel->dealTitle 			 =	$offermodel->offerTitle;
      $dealmodel->offerwas 			 =	$offermodel->offerwas;
      $dealmodel->offernow	 		 =  $offermodel->offernow;
      $dealmodel->text 		 		 =  $offermodel->offerText;
      $dealmodel->save(false);

    }
    $comaign = $workorderModel;
    $savenew = Yii::$app->request->post("savenew");
    $step =		Yii::$app->request->get("step");
    $cpType = $comaign->type;
    if($step==3){
      $comaign->buynow = Yii::$app->request->post('Workorders')['buynow'];
      $comaign->save(false);
    }

    if($savenew==2  && ($comaign->type==4 || $cpType==1 && $step!=5)){
      return $this->redirect(['updateoffer', 'id' => $id,'step'=>$step+1]);
    } else if($savenew==2 && $cpType==2 && $step!=5){
      return $this->redirect(['updateoffer', 'id' => $id,'step'=>$step+1]);
    }

  //  echo 'savenew-> '.$savenew.'--cp-->'.$cpType.' -<step->'.$step; die;
    return $this->redirect(['view', 'id' => $id]);
    }else{
      return $this->render('updateoffer', [
        'offermodel' => $offermodel,
        'workorderModel' => $workorderModel,
        'dealmodel'=>$dealmodel,
        'partnerProducts' => $partnerProducts,
      ]);


      }
    }
    else{

    return $this->render('updateoffer', [
    'offermodel' => $offermodel,
    'dealmodel'=>$dealmodel,
    'workorderModel' => $workorderModel,
    'partnerProducts' => $partnerProducts,

    ]);

    }

  }
	/* create store */
	/* Create store model */

   public function actionCreatestore($id){

		$user_id = Yii::$app->user->id;
		$model = new Workorderpopup();
		$model->scenario = 'create';

       if ($model->load(Yii::$app->request->post()) && $model->validate()) {

			$model->workorder_id 	= $id;
			$model->offerTitle 		= ucfirst($model->offerTitle);
          	$model->readTerms 		= str_replace("\r\n","",$model->readTerms);

          	$model->iphoneNotificationText = ucfirst($model->iphoneNotificationText);
			/* upload logo */

			if ($model->imageFilelogo) {

				$uniqueimage = substr(md5(date("Ymdhis").'15'),0,15);
				$imageName = $uniqueimage;

					if ($model->imageFilelogo) {

				  $path = parse_url($model->imageFilelogo, PHP_URL_PATH);
						$model->logo=basename($path);
						  }
						$model->save(false);
			}


			/* upload image */
			if($model->uploadTypes==1) {

				$model->videokey = '';
					if ($model->imageFile) {
						$uniqueimage = substr(md5(date("Ymdhis")),0,15);
						$imageName = $uniqueimage;

							if ($model->imageFile) {

				  $path = parse_url($model->imageFile, PHP_URL_PATH);
						$model->photo=basename($path);   }
						$model->save(false);
					}
			}else{

				$model->photo = '';
				$model->save(false);

			}

			/* upload image */
			if ($model->imageFile) {

				$model->videoImage ='';

				$uniqueimage = substr(md5(date("Ymdhis")),0,15);
				$imageName = $uniqueimage;

				if ($model->imageFile) {

				  $path = parse_url($model->imageFile, PHP_URL_PATH);
						$model->photo=basename($path);  }


			}

				return $this->redirect(['view', 'id' => $id]);


        }else{

            return $this->render('createstore', [
									'model' => $model,

								]);
        }
    }


    /** Create deal model */

    public function actionCreatedeal($id){

		$model = new Deal();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {

		 $model->dealvideoWorkorderName = UploadedFile::getInstance($model,'dealvideoWorkorderName');

				$model->workorder_id = $id;
				/* save deal model image */
				/* save and crop Main Photo upload image */
				if ($model->imageFile3) {

				    if ($model->imageFile3) {

						$path = parse_url($model->imageFile3, PHP_URL_PATH);
						$model->mainPhotoUpload=basename($path);
					}
						$model->save(false);
					}
					if ($model->dealvideoWorkorderName) {
					$uniqueimage = substr(md5(date("Ymdhis").'15'),0,15);

  	     	 		$imageName = $uniqueimage;
  	       			$model->dealvideokey = $imageName.'.'.$model->dealvideoWorkorderName->extension;
        			$model->save(false);
					$imagePathpopup = Yii::$app->params['workorderimage'].'/'.$imageName.'.'.$model->dealvideoWorkorderName->extension;
					$model->dealvideoWorkorderName->saveAs($imagePathpopup);
        			}

        			if ($model->dealvideoWorkorderImage && $model->dealvideoTypes==1) {

					$uniqueimage = substr(md5(date("Ymdhis").'15'),0,15);
  	     	 		$imageName = $uniqueimage;

        			if ($model->dealvideoWorkorderImage) {

						$path = parse_url($model->dealvideoWorkorderImage, PHP_URL_PATH);
						$model->dealvideoImage=basename($path);

					$model->save(false);

						}

				}


				if ($model->dealvideokey && $model->dealvideoTypes==2) {
				 $thumbvalue =  $model->getVideothumbnail();

				$model->dealvideoImage = $thumbvalue;
				$model->dealvideokey = $model->dealvideokey;
			}

        		if ($model->dealvideoWorkorderUrl && $model->dealvideoTypes==1) {

				$model->dealvideokey = $model->dealvideoWorkorderUrl;
			}
				/* save model */
				$model->save(false);
		       if(Yii::$app->request->post("savenew")==2){
						return $this->redirect(['updateoffer', 'id' => $id,'step'=>5]);
				}
		 return $this->redirect(['view', 'id' => $id]);
	 }else{

		 return $this->render('createdeal', [
                'model' => $model,


            ]);
		}


  }
 /* get store products */
 public function getPartnerProduct($id) {
	$checkbuy = Workorders::find()
				->select(['buynow','workorderpartner'])
				->where(['id' => $_GET['id']])->one();
	/* get existing product of the partner */
	$p =  \common\models\Product::tableName();
	$wp =  Workorderpopup::tableName();
	$w =  Workorders::tableName();
	$excludeIds = array();
	if(isset($checkbuy->id)){
		$query = new Query;
		$query	->select(["$wp.product_id"])
				->from($wp)
				->join(	'INNER JOIN',$p,"$p.id=$wp.product_id")
				->join(	'INNER JOIN',$w,"$w.id=$wp.workorder_id")
				->where('workorderpartner='.$checkbuy->workorderpartner)
				->andWhere("$wp.workorder_id!=$id");
		$command = $query->createCommand();
		$data = $command->queryAll();

		$excludeIds = array_unique(array_column($data,'product_id'));
	}


	$AllFreeProduct =  Product::find()
				  ->select(['id','name'])
				  ->where(['status' => 1]);
		if(count($excludeIds)){
			 $AllFreeProduct->andWhere(['store_id' => $checkbuy->workorderpartner])
			->andWhere(['NOT IN','id',$excludeIds]);
		}

	 $AllFreeProduct = $AllFreeProduct->orderBy('id ASC')->asArray()->all();
	 if($AllFreeProduct) {
		 $ids = array_column($AllFreeProduct,'id');
		 $name = array_column($AllFreeProduct,'name');
		 $AllFreeProduct = array_combine($ids,$name);
	 }
     return $AllFreeProduct;
 }
 /* update store start */
  /* update store */

	public function actionUpdatestore($id){

		$user_id 	= Yii::$app->user->id;

		$offermodel = $this->findoffermodel(array('workorder_id'=>$id));

		if($offermodel->load(Yii::$app->request->post())){


			if($offermodel->validate()){

				/* logo upload */

				if($offermodel->imageFilelogo){

					$uniqueimagelogo = substr(md5(date("Ymdhis").'12'),0,15);

						if(isset($offermodel->logo) && $offermodel->logo !='') {

							$oldImageName = explode("workorder/",$offermodel->logo);
							$oldImageName = $uniqueimagelogo;

						}else {

							$oldImageName[0] = $uniqueimagelogo;

						}

						$imageName = $oldImageName[0];

						 if($offermodel->imageFilelogo) {

							$path = parse_url($offermodel->imageFilelogo, PHP_URL_PATH);
						$offermodel->logo=basename($path);


						 }

				}


				if($offermodel->uploadTypes==1) {

					$offermodel->videokey = '';

					/* main photo upload */

					if ($offermodel->imageFile) {

							$uniqueimage = substr(md5(date("Ymdhis")),0,15);
							/* get old file name */
							if(isset($offermodel->photo) && $offermodel->photo !='') {
							 $oldImageName = explode("workorder/",$offermodel->photo);
							 $oldImageName = $uniqueimage;
							}else {
								$oldImageName[0] = $uniqueimage;
							}

							$imageName = $oldImageName[0];

							if($offermodel->imageFile) {

							$path = parse_url($offermodel->imageFile, PHP_URL_PATH);
						$offermodel->photo=basename($path);
					   }



					}

				}else{

					// $offermodel->photo = '';		//18

				}

				if ($offermodel->imageFile) {

					$offermodel->videoImage = '';

					$uniqueimage = substr(md5(date("Ymdhis")),0,15);

					/* get old file name */

					if(isset($offermodel->photo) && $offermodel->photo !='') {

						$oldImageName = explode("workorder/",$offermodel->photo);
						$oldImageName = $uniqueimage;

					}else {

						$oldImageName[0] = $uniqueimage;

					}

					$imageName = $oldImageName[0];
					if($offermodel->imageFilelogo) {

							$path = parse_url($offermodel->imageFilelogo, PHP_URL_PATH);
						$offermodel->photo=basename($path);

						  }
					}


				$offermodel->offerTitle = ucfirst($offermodel->offerTitle);
				$offermodel->iphoneNotificationText = ucfirst($offermodel->iphoneNotificationText);


				/* save model */

				$offermodel->save(false);

				return $this->redirect(['view', 'id' => $id]);

			}else{

				 return $this->render('updatestore', [
					'model' => $offermodel,

				]);


			}

		}else{

			return $this->render('updatestore', [
									'model' => $offermodel,

								]);

		}

	}
	/* update deal model */
public function actionUpdatedeal($id){
  $dealmodel = $this->finddealmodel(array('workorder_id'=>$id));
    if($dealmodel->load(Yii::$app->request->post())){
    $dealmodel->dealvideoWorkorderName = UploadedFile::getInstance($dealmodel,'dealvideoWorkorderName');
    if($dealmodel->validate()) {
    if ($dealmodel->imageFile3) {
      $uniqueimagelogo = substr(md5(date("Ymdhis").'12'),0,15);
      if(isset($dealmodel->mainPhotoUpload) && $dealmodel->mainPhotoUpload !='') {
        $oldImageName = explode("workorder/",$dealmodel->mainPhotoUpload);
        $oldImageName = $uniqueimagelogo;
      }else {
        $oldImageName[0] = $uniqueimagelogo;
      }
      $imageName = $oldImageName[0];
      if ($dealmodel->imageFile3) {
        $path = parse_url($dealmodel->imageFile3, PHP_URL_PATH);
        $dealmodel->mainPhotoUpload=basename($path);
      }
    }
    if ($dealmodel->dealvideoWorkorderImage && $dealmodel->dealvideoTypes==1) {
      $uniqueimagelogo = substr(md5(date("Ymdhis").'12'),0,15);
    if(isset($dealmodel->dealvideoImage) && $dealmodel->dealvideoImage!='')
    {
      $oldImageName = explode("workorder/",$dealmodel->dealvideoImage);
      $oldImageName = $uniqueimagelogo;
    }else {
      $oldImageName[0] = $uniqueimagelogo;
    }
    $imageName = $oldImageName[0];
    if($dealmodel->dealvideoWorkorderImage) {
      $path = parse_url($dealmodel->dealvideoWorkorderImage, PHP_URL_PATH);
      $dealmodel->dealvideoImage=basename($path);
    }
    }
    if(($dealmodel->dealvideoTypes==1) && $dealmodel->dealvideoWorkorderName) {
      $uniqueimagelogo = substr(md5(date("Ymdhis").'12'),0,15);
      if(isset($dealmodel->dealvideokey)) {
        $oldImageName = explode("workorder/",$dealmodel->dealvideokey);
        $oldImageName = $uniqueimagelogo;
      }else {
        $oldImageName[0] = $uniqueimagelogo;
      }
      $imageName = $oldImageName[0];
      $dealmodel->dealvideokey = $imageName.'.'.$dealmodel->dealvideoWorkorderName->extension;
      $imagePathpopup = Yii::$app->params['workorderimage'].'/'.$imageName.'.'.$dealmodel->dealvideoWorkorderName->extension;
      $dealmodel->dealvideoWorkorderName->saveAs($imagePathpopup);
    }
    if (($dealmodel->dealvideoTypes==1) && $dealmodel->dealvideoWorkorderUrl && $dealmodel->dealvideoWorkorderName=='') {
      $dealmodel->dealvideokey = $dealmodel->dealvideoWorkorderUrl;
    }
    if($dealmodel->dealvideoTypes==2)
    {
      $thumbvalue =  $dealmodel->getVideothumbnail();
      $dealmodel->dealvideoImage = $thumbvalue ;
    }
    if(strstr($dealmodel->frontPhotoUpload,'http')){
      $frontPhotoUpload = explode("workorder/",$dealmodel->frontPhotoUpload);
      $dealmodel->frontPhotoUpload = $frontPhotoUpload[1];
    }
    if(strstr($dealmodel->logoUpload,'http')){
      $logoUpload = explode("workorder/",$dealmodel->logoUpload);
      $dealmodel->logoUpload = $logoUpload[1];
    }
    if($dealmodel->dealuploadTypes==1) {
      $dealmodel->dealvideoImage = '';
      $dealmodel->dealvideokey = '';
      $dealmodel->dealvideoImage= '';
    }else{
      $dealmodel->mainPhotoUpload = '';
    }
    $dealmodel->save(false);
    if(Yii::$app->request->post("savenew")==2){
      return $this->redirect(['updateoffer', 'id' => $id,'step'=>5]);
    }
      return $this->redirect(['view', 'id' => $id]);
    }else{
      return $this->render('updatedeal', [
      'dealmodel' => $dealmodel,
      ]);
    }
  }else{
    return $this->render('updatedeal', [
      'dealmodel' => $dealmodel,
    ]);
  }
}
      /**
     * Updates an existing Workorders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post()) && $model->validate()) {

			/* check beacon group data */


			if(isset($_POST['Workorders']['beaconGroup']) && is_array($_POST['Workorders']['beaconGroup'])) {
				$groupid = implode(', ',$_POST['Workorders']['beaconGroup']);
				$str = rtrim($groupid,',');
				$model->beaconGroup = $str;
			}elseif(isset($_POST['beaconGroup']) && is_array($_POST['beaconGroup'])){
				$groupid = implode(', ',$_POST['beaconGroup']);
				$str = rtrim($groupid,',');
				$model->beaconGroup = $str;

			}else{
				$model->beaconGroup = null;

				}

			if(isset($_POST['Workorders']['userType']) && is_array($_POST['Workorders']['userType']) ){
				$model->userType = implode(', ',$_POST['Workorders']['userType']);
			}
			if(isset($_POST['Workorders']['agedGroup']) && is_array($_POST['Workorders']['agedGroup'])){
				$model->agedGroup = implode(', ',$_POST['Workorders']['agedGroup']);
			}


			if($model->temperature==1){ $model->temperaturestart=null; $model->temperaturend=null;  }

			if($model->rulecondition != 1){

				$model->behaviourvisit1 = '';
				$model->behaviourvisit2 = '';
				$model->behaviourvisit3 = '';
				$model->behaviourDwell = '';
				$model->purchaseHistory1 = '';
				$model->purchaseHistory2 = '';
				$model->purchaseHistory3 = '';
				$model->Loyalty1 = '';
				$model->Loyalty2 = '';
				$model->Loyalty3 = '';
				$model->usereturned	= '';
				$model->userenter = '';
				$model->dayparting = '';
				$model->daypartingcustom1 = '';
				$model->daypartingcustom2 = '';
				$model->daypartingcustom3 = '';
				$model->temperature = '';
				$model->temperaturestart = '';
				$model->temperaturend = '';
				$model->daypartingcustom1end = '';
				$model->daypartingcustom2end = '';
				$model->daypartingcustom3end = '';
				$model->behaviourDwellMinutes = '';

			}

			if($model->dayparting==1){ $model->daypartingcustom1 =null; $model->daypartingcustom1end =null; $model->daypartingcustom2 =null; $model->daypartingcustom2end =null; $model->daypartingcustom3 =null; $model->daypartingcustom3end =null;
			}elseif($model->dayparting==3){
				if($model->daypartingcustom1 == '' && $model->daypartingcustom1end == '') {
					$model->daypartingcustom1 = $model->daypartingcustom2;
					$model->daypartingcustom1end = $model->daypartingcustom2end;
					$model->daypartingcustom2 = '';
					$model->daypartingcustom2end = '';
					if($model->daypartingcustom1 == '' && $model->daypartingcustom1end == ''){
						$model->daypartingcustom1 = $model->daypartingcustom3;
						$model->daypartingcustom1end = $model->daypartingcustom3end;
						$model->daypartingcustom3 = '';
						$model->daypartingcustom3end = '';
					}

				}

			}




					/*update store categories model */
					WorkorderCategory::deleteAll('workorder_id ='.$id);
						if(isset(Yii::$app->request->post()['categoryTree']))
						{
							/* delete old categories */


							$categoryTree=Yii::$app->request->post()['categoryTree'];
							$catTree = implode(', ',$categoryTree);
							$model->catIDs = $catTree;
							$model->save(false);
							unset($categoryTree[0]);
							$catTree=[];
							foreach($categoryTree as $key=>$value)
							{
								array_push($catTree,array("category_id"=>$value,"workorder_id"=>$model->id,"status"=>1));
							}
						   if(count($catTree))
						   {
							   $storeCategory=WorkorderCategory::find()->where(['workorder_id'=>$model->id])->select('category_id as id,status')->asArray()->all();

							   Yii::$app->db->createCommand()->batchInsert(WorkorderCategory::tableName(),array_keys($catTree[0]), $catTree)->execute();
						   }
						}else {

							$model->save(false);

						}

						/***** update buynow in Workorder pop up table *******/
						$workorderpopmodel = new Workorderpopup();
						Yii::$app->db->createCommand()->update(Workorderpopup::tableName(), ['buynow' => $model->buynow], ['workorder_id' =>$model->id])->execute();
					/************ end here ***************************/
				$wopop = Workorderpopup::findOne(['workorder_id'=>$id]);
				$savenew = Yii::$app->request->post("savenew");
				$cpType = $model->type;
				if($savenew==2 && $cpType!=5){
					$action = isset($wopop->id)?'updateoffer':'createoffer';
					return $this->redirect([$action,'id' =>$id,'step'=>2]);
				} else if($savenew==2 && ($cpType==5)) {

					$action = isset($wopop->id)?'updatestore':'createstore';
					return $this->redirect([$action,'id' =>$id]);
				}
					return $this->redirect(['view', 'id' => $model->id]);
			}else {

            return $this->render('update', [
                'model' => $model,
                'categoryTree' => $this->getCategoryTree($id),
            ]);

			}

    }
public function actions()
{
    return [
        'uploadPhoto' => [
            'class' => 'paras\cropper\actions\UploadAction',
            'url' => Yii::$app->urlManagerBackEnd->createAbsoluteUrl(['uploads/workorder']),
          //  'url' => 'http://localhost/SweetSpotV2/frontend/web/uploads/workorders',
            'path' => '@backend/web/uploads/workorder',

        ]
    ];
}

   /* update psa workorder */

     public function actionUpdatepsa($id)
    {
        $model = $this->findModel($id);
         //$model->scenario = 'updatepsa';
        $offermodel = $this->findoffermodel(array('workorder_id'=>$id));

        if ($model->load(Yii::$app->request->post()) ) {

			  $offermodel->load(Yii::$app->request->post());
			  //$offermodel->imageFile = UploadedFile::getInstance($offermodel, 'imageFile');

		if( $model->validate() && $offermodel->validate()) {


			if(isset($_POST['Workorders']['beaconGroup'])) {
				$groupid = implode(', ',$_POST['Workorders']['beaconGroup']);
				$str = rtrim($groupid,',');
				$model->beaconGroup = $str;
			}elseif(isset($_POST['beaconGroup'])){
				$groupid = implode(', ',$_POST['beaconGroup']);
				$str = rtrim($groupid,',');
				$model->beaconGroup = $str;

			}else{
				$model->beaconGroup = null;

				}


			if($model->rulecondition != 1){

				$model->behaviourvisit1 = '';
				$model->behaviourvisit2 = '';
				$model->behaviourvisit3 = '';
				$model->behaviourDwell = '';
				$model->purchaseHistory1 = '';
				$model->purchaseHistory2 = '';
				$model->purchaseHistory3 = '';
				$model->Loyalty1 = '';
				$model->Loyalty2 = '';
				$model->Loyalty3 = '';
				$model->usereturned	= '';
				$model->userenter = '';
				$model->dayparting = '';
				$model->daypartingcustom1 = '';
				$model->daypartingcustom2 = '';
				$model->daypartingcustom3 = '';
				$model->temperature = '';
				$model->temperaturestart = '';
				$model->temperaturend = '';
				$model->daypartingcustom1end = '';
				$model->daypartingcustom2end = '';
				$model->daypartingcustom3end = '';
				$model->behaviourDwellMinutes = '';

			}

			$userType = implode(', ',$_POST['Workorders']['userType']);
			$AgeGroup = implode(', ',$_POST['Workorders']['agedGroup']);
			if($model->dayparting==1){ $model->daypartingcustom1 =null; $model->daypartingcustom1end =null; $model->daypartingcustom2 =null; $model->daypartingcustom2end =null; $model->daypartingcustom3 =null; $model->daypartingcustom3end =null; }


			$model->userType = $userType;
			$model->agedGroup = $AgeGroup;



			/*update store categories model */
					WorkorderCategory::deleteAll('workorder_id ='.$id);
						if(isset(Yii::$app->request->post()['categoryTree']))
						{

							$categoryTree=Yii::$app->request->post()['categoryTree'];
							$catTree = implode(', ',$categoryTree);
							$model->catIDs = $catTree;
							$model->save(false);
							unset($categoryTree[0]);
							$catTree=[];
							foreach($categoryTree as $key=>$value)
							{
								array_push($catTree,array("category_id"=>$value,"workorder_id"=>$id,"status"=>1));
							}
						   if(count($catTree))
						   {
							   $storeCategory=WorkorderCategory::find()->where(['workorder_id'=>$model->id])->select('category_id as id,status')->asArray()->all();

							   Yii::$app->db->createCommand()->batchInsert(WorkorderCategory::tableName(),array_keys($catTree[0]), $catTree)->execute();
						   }
						}else{

						$model->save(false);
						}




			/* save/update offer model */

			$offermodel->iphoneNotificationText = $_POST['Workorderpopup']['iphoneNotificationText'];

			if ($offermodel->imageFile) {

				$uniqueimagelogo = substr(md5(date("Ymdhis").'12'),0,15);
						if(isset($offermodel->photo) && $offermodel->photo !='') {
							$oldImageName = explode("workorder/",$offermodel->photo);
							$oldImageName = $uniqueimagelogo;
						}else {
							$oldImageName[0] = $uniqueimagelogo;
						}


					$imageName = $oldImageName[0];
        			if ($offermodel->imageFile) {

							$path = parse_url($offermodel->imageFile, PHP_URL_PATH);
						$offermodel->photo=basename($path);

							}
          	  }
				  $offermodel->save(false);






            return $this->redirect(['viewpsa', 'id' => $model->id]);
        } else {
            return $this->render('updatepsa', [
                'model' => $model,
                'offermodel' => $offermodel,
                 'categoryTree' => $this->getCategoryTree($id),

            ]);
        }
		}else {
            return $this->render('updatepsa', [
                'model' => $model,
                'offermodel' => $offermodel,
                 'categoryTree' => $this->getCategoryTree($id),

            ]);
        }
    }



    /**
     * Deletes an existing Workorders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

         Workorderpopup::deleteAll('workorder_id ='.$id);
		 Deal::deleteAll('workorder_id ='.$id);
		 WorkorderCategory::deleteAll('workorder_id ='.$id);
        return $this->redirect(['index']);
    }


 /* delete via ajax */
  public function actionDeletebyajax()

  {
		$pid = $_POST['id'];
		if($pid) {
        $this->findModel($pid)->delete();
        Workorderpopup::deleteAll('workorder_id ='.$pid);
        Deal::deleteAll('workorder_id ='.$pid);
        WorkorderCategory::deleteAll('workorder_id ='.$pid);
        GeoZone::deleteAll('deal_id ='.$pid);
        PolygonsRegion::deleteAll('deal_id ='.$pid);
       echo $pid; exit;
   }
 }

 /* set staus */
 public function actionSetstatus()

  {
		$id = $_POST['id'];
		$valueid = $_POST['value'];
		$model = $this->findModel($id);

		$model->status = $valueid;
		$model->save(false);

       echo $valueid; exit;

 }
  public function actionProductprice()

  {

		$id = $_POST['id'];
		$valueid = $_POST['value'];

		if($valueid) {

			$model = Product::find()->select('price')->where(['id' => $valueid])->one();
			echo $model->price; exit;


	  }
 }

 public function actionProductpricecombo()

  {

		$id = $_POST['id'];
		$valueid = $_POST['value'];

		if($valueid) {

			$model = Product::find()->select('price')->where(['id' => $valueid])->one();
			echo $model->price; exit;


	  }
 }

 public function actionPartnergetshopnow()
  {
	$valueid = $_POST['value'];

		if($valueid) {

			$model = Profile::find()->select('shopNow')->where(['user_id' => $valueid])->one();
			echo $model->shopNow; exit;


	  }
 }

public function actionZoneupdate()
 {
	 $partner			 = $_POST['partner'];
	 $newDealID 		 = $_POST['newDealID'];
	 $oldDealid 		 = $_POST['oldDealid'];
	 $partner_id 		 = $_POST['partner_id'];
	 $zoneModel = GeoZone::find()->where('deal_id='.$oldDealid )->asArray()->all();
	foreach($zoneModel as $_zoneModel){
		$geomodel = GeoZone::findOne($_zoneModel['id']);
		$geomodel->deal_id = $newDealID;
		$geomodel->agency_id = $partner_id;
		$geomodel->save();

	}
	 $polygensModel = PolygonsRegion::find()->where('deal_id='. $oldDealid )->asArray()->all($polygensModel);

	foreach($polygensModel as $_polygensModel){
		$polygensmodel = PolygonsRegion::findOne($_polygensModel['id']);
		$polygensmodel->deal_id = $newDealID;
		$polygensmodel->save(false);

	}

 }

 /* show beacon groups */
 public function actionBeacongroup()
	{
		$beaconID = $_POST['id'];
		if($beaconID) {

			$beaconType =   Beacongroup::findOne(['id' => $beaconID ]);
			if($beaconType) {
				if($beaconType->type==1){
					$beaconNames =   ArrayHelper::map(Beacongroup::find()->Where(['type' => [0,1]])->all(), 'id', 'group_name');
				}elseif($beaconType->type==2){
					$beaconNames =   ArrayHelper::map(Beacongroup::find()->Where('parent_id IS NULL')->andWhere('id!='.$beaconType->grandparent_id)->all(), 'id', 'group_name');
				}elseif($beaconType->type==3){
					$beaconNames =   ArrayHelper::map(Beacongroup::find()->Where(['!=', 'parent_id', ''])->orWhere('parent_id IS NULL')->andWhere('id!='.$beaconType->grandparent_id)->all(), 'id', 'group_name');
				}

			$st = array('data'=> $beaconNames);
			echo json_encode($st); exit;
			}
		}
	}

		public function actionBeaconGroupname() {
			$data=Yii::$app->request->post();

			$q = $data['q'];

			$sq = is_array($data['sq'])?array_reverse($data['sq']):'';
			$sq = is_array($sq)?array_shift($sq):'';

			$id = null;
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$out = ['results' => ['id' => '', 'text' => '']];
			if (!is_null($q)) {
				$query = new Query;

				if($sq){
				$beaconType =   Beacongroup::find()->where(['id' => $sq])->orWhere(['like', 'group_name', $sq])->one();

					if($beaconType) {
						if($beaconType->type==1){
							$query->select('id, group_name AS text')
								->from('tbl_beacon_group')
								#->where(['like', 'group_name', $q])
								->where(['type' => [0,1]])
								->limit(20);

						}elseif($beaconType->type==2){
							$query->select('id, group_name AS text')
								->from('tbl_beacon_group')
								->Where('parent_id IS NULL')
								->andWhere('id!='.$beaconType->grandparent_id)
								->limit(20);

						}elseif($beaconType->type==3){
							$query->select('id, group_name AS text')
								->from('tbl_beacon_group')
								->Where('type!=2')
								->andWhere('id!='.$beaconType->grandparent_id)
								->limit(20);

						}elseif($beaconType->type==0){
							$query->select('id, group_name AS text')
							->from('tbl_beacon_group')
							->where(['like', 'group_name', $q])
							->limit(20);

						}


					}

				}else{
				$query->select('id, group_name AS text')
					->from('tbl_beacon_group')
					->where(['like', 'group_name', $q])
					->limit(20);
				}

				$command = $query->createCommand();
				$data = $command->queryAll();

				$out['results'] = array_values($data);
			}
			elseif ($id > 0) {
				$out['results'] = ['id' => $id, 'text' => Beacongroup::find($id)->group_name];
			}
				return $out;
			}





    /**
     * Finds the Workorders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Workorders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Workorders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /* view of offer model */
    protected function findoffermodel($id)
    {
    	 if (($offermodel = Workorderpopup::findOne($id)) !== null) {

            return $offermodel;
        } else {
			return $offermodel;
            #throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

   /* find store_id in Dealstore Model */
    protected function finddealstoreModel($id)
    {


		if (($dealstore = Dealstore::find()->where(['deal_id'=>$id])->select(['store_id'])->asArray()->all()) !== null) {
            return ArrayHelper::map($dealstore,'store_id','store_id');
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	  protected function findcatmodel($id)
    {
    	 if (($catsmodel = WorkorderCategory::findAll($id)) !== null) {
            return $catsmodel;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findstoredealmodel($id)
    {
    	 if (($Dealsmodel = Dealstore::findAll($id)) !== null) {
            return $Dealsmodel;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    protected function finddealmodel($id)
    {
    	 if (($dealmodel = Deal::findOne($id)) !== null) {

            return $dealmodel;
        } else {
			return true;
           # throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


/* get list of by ajax on the basis of parterre selection category */
	public function actionWorkcat()
	{
		$storeId = $_POST['id'];
		return $this->getCategoryTreeajax($storeId);
	}

	protected function getCategoryTreeajax($workorder_id)
	{
		$query = new Query;
		$query	->select([Categories::tableName().'.id as id',StoreCategory::tableName().'.id as selected','name as title',Categories::tableName().'.id as value',  '(lvl+2) as level','lft','ifnull(status,0) as checked'])
				->from(Categories::tableName())
				->rightJoin(StoreCategory::tableName(),StoreCategory::tableName().'.category_id ='.Categories::tableName().'.id and '.StoreCategory::tableName().'.store_id='.$workorder_id)
				->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC]);
		$command = $query->createCommand();

		$data = $command->queryAll();
		$requiredResult=array();
		$oldLevel=2;
		$levelKeys=array();
		$ifnul = 0;
		foreach($data as $value)
		{
			if($value['selected']=='' || $value['id']=='')
			{
				unset($data[$ifnul]);
			}
			$ifnul++;
		}
		foreach($data as $key=>$value)
		{
		   $key=$value['id'];
           $value['title']=htmlspecialchars_decode($value['title']);
           $value['has_children']=false;
           $value['children']=[];
		   $value['checked']=$value['checked']==1?false:false;

			if($value['level']==2)
			{

				$requiredResult[$key]=$value;

			}
			else
			{
				$array_ptr=&$requiredResult;

				for($i=2; $i<$value['level']; $i++)
				{

					 $array_ptr[ strval($levelKeys[$i]) ]['has_children']=true;
					 $array_ptr = &$array_ptr[ strval($levelKeys[$i]) ]['children'];


				}
				$array_ptr[$key]=$value;
			}

			$levelKeys[$value['level']]=$value['id'];
		}

		$this->recursiveRemoval($requiredResult,'children');
      $requiredResult=is_array($requiredResult)?array_values($requiredResult):$requiredResult;
	  	$data=array('id'=>0,'value'=>0,'title'=>'Root','level'=>1,'has_children'=>true,'children'=>$requiredResult);
		return (json_encode($data));

	}
/* end  list of by ajax on the basis of parterre selection category */
    protected function getCategoryTree($workorder_id)
	//public function actionTree()
	{

		$requiredResult=array();
		$query = new Query;
	  $userType = Yii::$app->user->identity->user_type;
		if($userType==2 || $userType==7){
			$storeId =Yii::$app->user->identity->id;
			$query	->select([Categories::tableName().'.id as id',StoreCategory::tableName().'.id as selected','name as title',Categories::tableName().'.id as value',  '(lvl+2) as level','lft','ifnull(status,0) as checked'])
				->from(Categories::tableName())
				->rightJoin(StoreCategory::tableName(),StoreCategory::tableName().'.category_id ='.Categories::tableName().'.id and '.StoreCategory::tableName().'.store_id='.$storeId)
				->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC]);
				$command = $query->createCommand();
				$data = $command->queryAll();
					$ifnul = 0;
					$ifnul = 0;
					foreach($data as $value)
					{
						if($value['selected']=='' || $value['id']=='')
						{
							unset($data[$ifnul]);
						}
						$ifnul++;
					}
		}
		else{
		$query	->select(['tbl_categories.id as id','name as title','tbl_categories.id as value',  '(lvl+2) as level','lft','ifnull(status,0) as checked'])
				->from('tbl_categories')
				->leftJoin('tbl_workorderCategory', 'tbl_workorderCategory.category_id = tbl_categories.id and tbl_workorderCategory.workorder_id='.$workorder_id)
				//->limit(2)
				->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC]);
		$command = $query->createCommand();
		$data = $command->queryAll();
		}

		$requiredResult=array();
		$oldLevel=2;
		$levelKeys=array();
		foreach($data as $key=>$value)
		{
			//$value['path']="root/";
			$key=$value['id'];
            $value['title']=htmlspecialchars_decode($value['title']);
           $value['has_children']=false;
           $value['children']=[];
		   $value['checked']=$value['checked']==1?true:false;

			if($value['level']==2)
			{
		//	$value['path']=$value['path'].$value['name'];
			$requiredResult[$key]=$value;

			}
			else
			{
				$array_ptr=&$requiredResult;

				for($i=2; $i<$value['level']; $i++)
				{

					 $array_ptr[ strval($levelKeys[$i]) ]['has_children']=true;
					 $array_ptr = &$array_ptr[ strval($levelKeys[$i]) ]['children'];


				}
					$array_ptr[$key]=$value;
				}

			$levelKeys[$value['level']]=$value['id'];
		}

		$this->recursiveRemoval($requiredResult,'children');
      $requiredResult=is_array($requiredResult)?array_values($requiredResult):$requiredResult;
	  	$data=array('id'=>0,'value'=>0,'title'=>'Root','level'=>1,'has_children'=>true,'children'=>$requiredResult);
		return (json_encode($data));

	}


		 public $i=0;
	protected function recursiveRemoval(&$array, $val)
	{
		if(is_array($array))
		{
			foreach($array as $key=>&$arrayElement)
			{
				if(is_array($arrayElement))
				{
					if($key===$val)
					{
				$this->i++;

				 if($this->i==2)
				 {
			 }
				$arrayElement=array_values($arrayElement);

				}
					$this->recursiveRemoval($arrayElement, $val);
				}
				else
				{
					if($key == $val)
					{

					}
				}
			}
		}
	}



	/*export-pdf*/

	public function actionExportPdf($id) {

		$pdf = new Pdf([
			// set to use core fonts only
			'mode' => Pdf::MODE_CORE,
			// A4 paper format

			'format' => Pdf::FORMAT_A4,

			// portrait orientation
			'orientation' => Pdf::ORIENT_PORTRAIT,
			// stream to browser inline
			'destination' => Pdf::DEST_BROWSER,
			// your html content input
			'content' => $this->renderPartial('text'),
			// format content from your own css file if needed or use the
			// enhanced bootstrap css built by Krajee for mPDF formatting
			'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
			// any css to be embedded if required
			'cssInline' => '.kv-heading-1{font-size:18px}',
			 // set mPDF properties on the fly
			'options' => ['title' => 'Krajee Report Title'],
			 // call mPDF methods on the fly
			'methods' => [
				'SetHeader'=>['Krajee Report Header'],
				'SetFooter'=>['{PAGENO}'],
			]
		]);

		return $pdf->render();

	}

	/* Save zone after saving workorder */

	protected function savegeolocation($workOrderId=NULL,$zoneLatLng=NULL) {

			$model 	= new GeoZone();
			$zonemodel = new GeoZone();

			$polygon_latlng = '';

			if(!empty($workOrderId) && !empty($zoneLatLng)){

				$area_name 		= 'not set';
				$dealid 		= $workOrderId;
				$notific_limit	= 1;
				$latlng			= $zoneLatLng?$zoneLatLng:'';
				$created_at 	= strtotime(date('Y-m-d H:i:s'));
				$updated_at		= strtotime(date('Y-m-d H:i:s'));

				// $regionId 		= isset($param['regionId'])?$param['regionId']:'';
				if(!empty($latlng)){

					$LatLngValue 		 = (array) str_replace(')(', ',', substr(rtrim($latlng), 1,-1));

					$firstLatLng = explode(',',str_replace(')(', ',', substr(rtrim($latlng), 1,-1)));

					foreach($LatLngValue as $coord){

						$args     = explode(",",$coord);
						$args1     = explode(",",$coord);

						for($l=0;$l<sizeof($args1)/2;$l++){

								$i= 2*$l;

							$polygon_latlng .= $args1[$i].' '.$args1[$i+1].',';

						}

						$allLatLngPol = $polygon_latlng.$args1[0].' '.$args1[1];

						// save to region

						$sql = "INSERT INTO {{%polygons_region}} (deal_id,polygon_shape,message_limit,created_at,updated_at) VALUES($dealid,GeomFromText('POLYGON(($allLatLngPol))'),$notific_limit,$created_at,$updated_at);";

						$ret = Yii::$app->db->createCommand($sql)->execute();

						$region_id = Yii::$app->db->getLastInsertID();

						if($region_id){

							for($l=0;$l<sizeof($args)/2;$l++){

								$i= 2*$l;


								$rows[] = [
											$region_id,
											$area_name,
											$args[$i],
											$args[$i+1],
											$dealid,
											$created_at,
											$updated_at
										];

							}

						}

						$columns 	 = ['region_id','area_name','latitude','longitude','deal_id','created_at','updated_at'];

						// save to geo zone

						$reslt = Yii::$app->db->createCommand()->batchInsert('{{%geo_zone}}',$columns,$rows)->execute();


					}


				}

			}


	}

	/*
		Render Geo Map view to create zone
	*/

	public function actionCreatezone($id=NULL) {


		$model = new GeoZone();

		if(Yii::$app->user->identity->user_type==1){

			$partner = ArrayHelper::map(Usermanagement::find() ->joinwith('workorders',false,'INNER JOIN')->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');

		}else{

			$partner = ArrayHelper::map(Usermanagement::find() ->joinwith('workorders',false,'INNER JOIN')->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
		}



		return $this->render('createzone', [
							'model' => $model,
							'partner' => $partner,
						]);

	}
		/*
		Render Geo Map view to create zone
	*/

	public function actionCreatezoneiframe($id=NULL) {

		$this->layout='zoneiframe';
		$model = new GeoZone();

		if(Yii::$app->user->identity->user_type==1){

			$partner = ArrayHelper::map(Usermanagement::find() ->joinwith('workorders',false,'INNER JOIN')->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');

		}else{

			$partner = ArrayHelper::map(Usermanagement::find() ->joinwith('workorders',false,'INNER JOIN')->where(['user_type' => 7])->orderBy('orgName ASC')->groupBy('orgName')->all(), 'id', 'orgName');
		}



		return $this->render('createzoneiframe', [
							'model' => $model,
							'partner' => $partner,
						]);

	}

	/* Create zone on map and save to DB */

	public function actionSavezone(){

		if(Yii::$app->request->isAjax){

			$model 	= new GeoZone();
			$zonemodel = new GeoZone();

			$param  = Yii::$app->request->post();

			$polygon_latlng = '';

			if(!empty($param)){

				$area_name 		= isset($param['area_name'])?$param['area_name']:'';
				$partner_id 	= isset($param['partner_id'])?$param['partner_id']:'';
				$dealid 		= isset($param['dealid'])?$param['dealid']:'';
				$notific_limit	= $param['notific_limit']?$param['notific_limit']:1;
				$latlng			= $param['latlng']?$param['latlng']:'';
				$created_at 	= strtotime(date('Y-m-d H:i:s'));
				$updated_at		= strtotime(date('Y-m-d H:i:s'));

				// $regionId 		= isset($param['regionId'])?$param['regionId']:'';
				if(!empty($latlng)){

					$LatLngValue 		 = (array) str_replace(')(', ',', substr(rtrim($latlng), 1,-1));

					$firstLatLng = explode(',',str_replace(')(', ',', substr(rtrim($latlng), 1,-1)));

					foreach($LatLngValue as $coord){

						$args     = explode(",",$coord);
						$args1     = explode(",",$coord);

						for($l=0;$l<sizeof($args1)/2;$l++){

								$i= 2*$l;

							$polygon_latlng .= $args1[$i].' '.$args1[$i+1].',';

						}

						$allLatLngPol = $polygon_latlng.$args1[0].' '.$args1[1];

						// save to region

						$sql = "INSERT INTO {{%polygons_region}} (deal_id,polygon_shape,message_limit,created_at,updated_at) VALUES($dealid,GeomFromText('POLYGON(($allLatLngPol))'),$notific_limit,$created_at,$updated_at);";

						$ret = Yii::$app->db->createCommand($sql)->execute();

						$region_id = Yii::$app->db->getLastInsertID();

						if($region_id){

							for($l=0;$l<sizeof($args)/2;$l++){

								$i= 2*$l;


								$rows[] = [
											$region_id,
											$area_name,
											$args[$i],
											$args[$i+1],
											$dealid,
											$created_at,
											$updated_at,
											$partner_id,
										];

							}

						}

						$columns 	 = ['region_id','area_name','latitude','longitude','deal_id','created_at','updated_at','agency_id'];

						// save to geo zone

						$reslt = Yii::$app->db->createCommand()->batchInsert('{{%geo_zone}}',$columns,$rows)->execute();

						if($reslt){

							echo "SUCCESS";

						}else{

							echo "Zone creation is failed.";

						}

					}


				}

			}

        }

		die;

	}

	/* Render zones on map */

	public function actionGetzone() {

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$model 			= new GeoZone();
		$Polygonmodel 	= new PolygonsRegion();

		$usertype 		= Yii::$app->user->identity->user_type;
		$user_id  		= Yii::$app->user->identity->id;


		if(Yii::$app->request->isAjax){

			$param  		= Yii::$app->request->get();

			if(!empty($param) && $param['partner_id']=='ALL'){

				// get all zones of partners

				if($usertype==1){

					$query = Workorders::find()->select([Workorders::tableName().'.id'])->joinWith('workorderpopup',false,'INNER JOIN')->where(['>=', Workorders::tableName().".dateTo",strtotime(date('d-m-Y H:i:s'))])->asArray()->all();

				}else{

					$query = Workorders::find()->select([Workorders::tableName().'.id'])->joinWith('workorderpopup',false,'INNER JOIN')->where([Workorders::tableName().'.workorderpartner' => $user_id])->where(['>=', Workorders::tableName().".dateTo",strtotime(date('d-m-Y H:i:s'))])->asArray()->all();
				}



				if(!empty($query)){
					foreach($query as $workOrderIds){
						$workOrderId[]	= $workOrderIds['id'];

					}

				}else{

					$workOrderId = [];

				}

				$Polygonmode 	= $Polygonmodel::find()
									->select([PolygonsRegion::tableName().'.id',PolygonsRegion::tableName().'.message_limit',])
									->where(['deal_id'=>$workOrderId])
									->asArray()
									->all();


				if($Polygonmode){

					foreach($Polygonmode as $polygonData){

						$GetZoneLatLng 	= $model::find()
											->select([GeoZone::tableName().'.*',Workorders::tableName().'.name'])
											->leftJoin('{{%workorders}}', Workorders::tableName().'.id='.GeoZone::tableName().'.deal_id')
											->where([GeoZone::tableName().'.region_id'=>$polygonData['id']])
											->OrderBy([GeoZone::tableName().'.id'=>SORT_ASC])
											->asArray()
											->all();

						if(!empty($GetZoneLatLng)){

								$retrnData[] = array(
										'region_id'=>$polygonData['id'],
										'message_limit'=>$polygonData['message_limit'],
										'deal_name'=>$GetZoneLatLng[0]['name'],
										'area_name'=>$GetZoneLatLng[0]['area_name'],
										'deal_id'=>$GetZoneLatLng[0]['deal_id'],
										'latlongData'=>$GetZoneLatLng,
									 );

						}else{

							$retrnData[] = ['region_id'=>$polygonData['id'],'message_limit'=>$polygonData['message_limit'],'latlongData'=>''];
						}


					}

					return $retrnData;

				}else{

					echo "false";

				}


			}else{

			// get only particular partner zones

				$query = Workorders::find()->select([Workorders::tableName().'.id'])->joinWith('workorderpopup',false,'INNER JOIN')->where(['>=', Workorders::tableName().".dateTo",strtotime(date('d-m-Y H:i:s'))])->andWhere([Workorders::tableName().'.workorderpartner' => $param['partner_id']])->asArray()->all();

				if(!empty($query)){
					foreach($query as $workOrderIds){
						$workOrderId[]	= $workOrderIds['id'];

					}

				}else{

					$workOrderId = [];

				}

				$Polygonmode 	= $Polygonmodel::find()
									->select([PolygonsRegion::tableName().'.id',PolygonsRegion::tableName().'.message_limit',])
									->where(['deal_id'=>$workOrderId])
									->asArray()
									->all();


				if($Polygonmode){

					foreach($Polygonmode as $polygonData){

						$GetZoneLatLng 	= $model::find()
											->select([GeoZone::tableName().'.*',Workorders::tableName().'.name'])
											->leftJoin('{{%workorders}}', Workorders::tableName().'.id='.GeoZone::tableName().'.deal_id')
											->where([GeoZone::tableName().'.region_id'=>$polygonData['id']])
											->OrderBy([GeoZone::tableName().'.id'=>SORT_ASC])
											->asArray()
											->all();

						if(!empty($GetZoneLatLng)){

								$retrnData[] = array(
										'region_id'=>$polygonData['id'],
										'message_limit'=>$polygonData['message_limit'],
										'deal_name'=>$GetZoneLatLng[0]['name'],
										'area_name'=>$GetZoneLatLng[0]['area_name'],
										'deal_id'=>$GetZoneLatLng[0]['deal_id'],
										'latlongData'=>$GetZoneLatLng,
									 );

						}else{

							$retrnData[] = ['region_id'=>$polygonData['id'],'message_limit'=>$polygonData['message_limit'],'latlongData'=>''];
						}


					}

					return $retrnData;

				}else{

					echo "false";

				}

			}

		}

		die;

	}

	/* edit zone from map */

	public function actionEditzone() {

		if(Yii::$app->request->isAjax){

			$param  = Yii::$app->request->post();

			if(!empty($param)){

				$region_id 	= $param['region_id'];
				$deal_id 	= $param['deal_id'];
				if(($model = PolygonsRegion::findOne($region_id)) !== null){

					// delete

					$reslt1 = \Yii::$app
								->db
								->createCommand()
								->delete('{{%polygons_region}}', ['id' => $region_id,'deal_id'=>$deal_id])
								->execute();

					$reslt 	= GeoZone::deleteAll('region_id ='.$region_id);

					if($reslt && $reslt1){

						$model 	= new GeoZone();
						$zonemodel  = new GeoZone();

						$param  	= Yii::$app->request->post();

					$polygon_latlng = '';

					$area_name 		= isset($param['area_name'])?$param['area_name']:'';
					$dealid 		= isset($param['dealid'])?$param['dealid']:'';
					$notific_limit	= $param['notific_limit']?$param['notific_limit']:1;
					$latlng			= $param['latlng']?$param['latlng']:'';
					$created_at 	= strtotime(date('Y-m-d H:i:s'));
					$updated_at		= strtotime(date('Y-m-d H:i:s'));
					$partner_id 	= isset($param['partner_id'])?$param['partner_id']:'';
					// $regionId 		= isset($param['regionId'])?$param['regionId']:'';
					if(!empty($latlng) && !empty($area_name)){

						$LatLngValue 		 = (array) str_replace(')(', ',', substr(rtrim($latlng), 1,-1));

						$firstLatLng = explode(',',str_replace(')(', ',', substr(rtrim($latlng), 1,-1)));

						foreach($LatLngValue as $coord){

							$args     	= explode(",",$coord);
							$args1     	= explode(",",$coord);

							for($l=0;$l<sizeof($args1)/2;$l++){

									$i= 2*$l;

								$polygon_latlng .= $args1[$i].' '.$args1[$i+1].',';

							}

							$allLatLngPol = $polygon_latlng.$args1[0].' '.$args1[1];

							// save to region

							$sql = "INSERT INTO {{%polygons_region}} (deal_id,polygon_shape,message_limit,created_at,updated_at) VALUES($dealid,GeomFromText('POLYGON(($allLatLngPol))'),$notific_limit,$created_at,$updated_at);";

							$ret = Yii::$app->db->createCommand($sql)->execute();

							$region_id = Yii::$app->db->getLastInsertID();

							if($region_id){

								for($l=0;$l<sizeof($args)/2;$l++){

									$i= 2*$l;


									$rows[] = [
												$region_id,
												$area_name,
												$args[$i],
												$args[$i+1],
												$dealid,
												$created_at,
												$updated_at,
												$partner_id
											];

								}

							}

							$columns 	 = ['region_id','area_name','latitude','longitude','deal_id','created_at','updated_at','agency_id'];

							// save to geo zone

							$reslt = Yii::$app->db->createCommand()->batchInsert('{{%geo_zone}}',$columns,$rows)->execute();

							if($reslt){

								echo "SUCCESS";

							}else{

								echo "Zone creation is failed.";

								}

						}
					}


						// echo "SUCCESS";

					}else{

						echo "Zone can not be deleted.Please try later.";

					}


				}else{

					echo "No record found in our system.";

				}

			}

		}

		die;

	}

	/* Delete zone from map */

	public function actionDeletezone() {

		if(Yii::$app->request->isAjax){

			$param  = Yii::$app->request->post();

			if(!empty($param)){

				$region_id 	= $param['region_id'];
				$deal_id 	= $param['deal_id'];

				if(($model = PolygonsRegion::findOne($region_id)) !== null){

					// delete

					$reslt1 = \Yii::$app
								->db
								->createCommand()
								->delete('{{%polygons_region}}', ['id' => $region_id,'deal_id'=>$deal_id])
								->execute();

					$reslt 	= GeoZone::deleteAll('region_id ='.$region_id);

					if($reslt && $reslt1){

						echo "SUCCESS";

					}else{

						echo "Zone can not be deleted.Please try later.";

					}


				}else{

					echo "No record found in our system.";

				}

			}

		}

		die;

	}


	/* Save notification limit */

	public function actionNotificationlimit() {

		if(Yii::$app->request->isAjax){

			$param  = Yii::$app->request->post();

			if(!empty($param)){

					$deal_id 				= $param['deal_id'];
					$notification_limit 	= $param['notification_limit'];

					$Polygonmodel 			= new PolygonsRegion();

					$ret = $Polygonmodel->updateAll(array('message_limit' =>$notification_limit), 'deal_id='.$deal_id);

					echo "SUCCESS";


				}else{

					echo "No record found in our system.";

				}

			}

		die;

	}

	/*
	*	Get partner address
	*/
	public function actionPartneraddress(){

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		if(Yii::$app->request->isAjax){

			$param  		= Yii::$app->request->get();

			// set lat long location on map

			$addressData 	= Profile::find()
								->select(["CONCAT(street, ',', city, ',' , state, ',' ,country) AS address"])
								->where([Profile::tableName().'.user_id'=>$param['partner_id']])
								->asArray()
								->one();


			if(!empty($addressData['address'])){

				$addrData = $addressData;

			}else{

				// set default to australia

				$addrData = array("address" => "Bourke Street Mall, Melbourne VIC 3004, Australia");


			}

			return $addrData;
		}

	}

	/* Get all the partners */
	public function actionGetdeals(){

		// die;
		$param  		= Yii::$app->request->get();

		$workOrderOpt 	= '';

		if(!empty($param) && !empty($param['partner_id'])){

			$partner_id = $param['partner_id'];

			$query 			= Workorders::find()->select([Workorders::tableName().'.id',Workorders::tableName().'.name'])->joinWith('workorderpopup',false,'INNER JOIN')->where([Workorders::tableName().'.workorderpartner' => $partner_id])->andWhere(['>=', Workorders::tableName().".dateTo",strtotime(date('d-m-Y H:i:s'))])->andWhere([Workorders::tableName().".geoProximity"=>[1,3]])->asArray()->all();

			$workOrderOpt .= '<option value="">ALL</option>';

			if(!empty($query)){

				foreach($query as $workOrderIds){

					$workOrderOpt .= '<option value="'.$workOrderIds['id'].'">'.$workOrderIds['name'].'</option>';

				}

			}else{

					$workOrderOpt 		= '';

			}

			echo $workOrderOpt;
		}

		die;

	}


	/* Render zones on map for deal */

	public function actionGetdealzone() {

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		$model 			= new GeoZone();
		$Polygonmodel 	= new PolygonsRegion();

		if(Yii::$app->request->isAjax){

			$param  		= Yii::$app->request->get();

			if($param['workOrderId']=='ALL'){

				// get zone of only one deal

				$partner_id = $param['partner_id'];

				$query = Workorders::find()->select([Workorders::tableName().'.id'])->joinWith('workorderpopup',false,'INNER JOIN')->where(['>=', Workorders::tableName().".dateTo",strtotime(date('d-m-Y H:i:s'))])->andWhere([Workorders::tableName().'.workorderpartner' => $partner_id])->asArray()->all();

				if(!empty($query)){
					foreach($query as $workOrderIds){
						$workOrderId[]	= $workOrderIds['id'];

					}

				}else{

					$workOrderId = [];

				}

				$Polygonmode 	= $Polygonmodel::find()
									->select([PolygonsRegion::tableName().'.id',PolygonsRegion::tableName().'.message_limit',])
									->where(['deal_id'=>$workOrderId])
									->asArray()
									->all();


				if($Polygonmode){

					foreach($Polygonmode as $polygonData){

						$GetZoneLatLng 	= $model::find()
											->select([GeoZone::tableName().'.*',Workorders::tableName().'.name'])
											->leftJoin('{{%workorders}}', Workorders::tableName().'.id='.GeoZone::tableName().'.deal_id')
											->where([GeoZone::tableName().'.region_id'=>$polygonData['id']])
											->OrderBy([GeoZone::tableName().'.id'=>SORT_ASC])
											->asArray()
											->all();

						if(!empty($GetZoneLatLng)){

								$retrnData[] = array(
										'region_id'=>$polygonData['id'],
										'message_limit'=>$polygonData['message_limit'],
										'deal_name'=>$GetZoneLatLng[0]['name'],
										'area_name'=>$GetZoneLatLng[0]['area_name'],
										'deal_id'=>$GetZoneLatLng[0]['deal_id'],
										'latlongData'=>$GetZoneLatLng,
									 );

						}else{

							$retrnData[] = ['region_id'=>$polygonData['id'],'message_limit'=>$polygonData['message_limit'],'latlongData'=>''];
						}


					}

					return $retrnData;

				}else{

					echo "false";
				}

			}elseif(!empty($param['workOrderId'])){

				// get zone of all deal of partner
				// set address and lat/long ends

				$Polygonmode 	= $Polygonmodel::find()
								->select([PolygonsRegion::tableName().'.id',PolygonsRegion::tableName().'.message_limit',])
								->where(['deal_id'=>$param['workOrderId']])
								->asArray()
								->all();

				if($Polygonmode){

					foreach($Polygonmode as $polygonData){

						$GetZoneLatLng 	= $model::find()
									->select([GeoZone::tableName().'.*',Workorders::tableName().'.name'])
									->leftJoin('{{%workorders}}', Workorders::tableName().'.id='.GeoZone::tableName().'.deal_id')
									->where([GeoZone::tableName().'.deal_id'=>$param['workOrderId']])
									->andWhere([GeoZone::tableName().'.region_id'=>$polygonData['id']])
									->OrderBy([GeoZone::tableName().'.id'=>SORT_ASC])
									->asArray()
									->all();

						if(!empty($GetZoneLatLng)){

								$retrnData[] = array(
										'region_id'=>$polygonData['id'],
										'message_limit'=>$polygonData['message_limit'],
										'deal_name'=>$GetZoneLatLng[0]['name'],
										'area_name'=>$GetZoneLatLng[0]['area_name'],
										'deal_id'=>$GetZoneLatLng[0]['deal_id'],
										'latlongData'=>$GetZoneLatLng,
									 );

						}else{

							$retrnData[] = ['region_id'=>$polygonData['id'],'message_limit'=>$polygonData['message_limit'],'latlongData'=>''];
						}

					}

					return $retrnData;

				}else{

					echo "false";
				}
			}else{

					echo "false";
			}

		}

		die;

	}

	public function actionArealist(){

		$dealId = Yii::$app->request->post('dealId');
		$geoZone = GeoZone::find()->select(['id','area_name'])
					->where('deal_id='.$dealId)
					->groupBy(['deal_id','area_name'])
					->asArray()->all();
		return json_encode(array('data'=>$geoZone));
	}


public function actionMaplist(){
	$qry = GeoZone::find()->select(['latitude','longitude'])
	->where('Id='.$_POST['Id'])
	->asArray()->one();
	return json_encode($qry);
}

public function actionNamelist(){
	$qry = GeoZone::find()->select(['latitude','longitude'])
	->where('Id='.$_POST['Id'])
	->asArray()->one();
	return json_encode($qry);
}

	public function actionSavelist(){
		 $id		    = $_POST['Id'];
		 $workOrderId	= $_POST['workOrderId'];
		 $partnerId	= $_POST['partner_id'];
		 $time = time();
		 $zoneModel = GeoZone::find()->where('id='.$id )->asArray()->one();
		 $zoneModel2 = GeoZone::find()->where(['deal_id'=>$workOrderId,'area_name'=>$zoneModel['area_name']])->asArray()->one();
		if(isset($zoneModel2['id'])){
			return json_encode(array('message' => 'The area has already saved on this deal.'));
		}
		$allPoly = PolygonsRegion::find()->select(['message_limit','polygon_shape',new Expression("$workOrderId as deal_id"),new Expression("$time as created_at"),new Expression("$time as updated_at")])
			->where(['deal_id'=>$zoneModel['deal_id'],'id'=>$zoneModel['region_id']])
			->asArray()->all();
		if(isset($allPoly[0]))
		{
			 $resionId = '';
			/* save ploygon region */
			if(isset($allPoly[0]))
			{
				$keys = array_keys($allPoly[0]);
				$resltpoly = Yii::$app->db->createCommand()->batchInsert('{{%polygons_region}}',$keys,$allPoly)->execute();
				$resionId = Yii::$app->db->getLastInsertID();
			}
			/* save geo zone */
			if(isset($resionId))
			{
				$allNode = GeoZone::find()->select([new Expression("$resionId as region_id"),'area_name','latitude','longitude',new Expression("$workOrderId as deal_id"),new Expression("$time as created_at"),new Expression("$time as updated_at"),new Expression("$partnerId as agency_id")])
					->where(['deal_id'=>$zoneModel['deal_id'],'area_name'=>$zoneModel['area_name']])
					->asArray()->all();
				$keys = array_keys($allNode[0]);
				$reslt = Yii::$app->db->createCommand()->batchInsert('{{%geo_zone}}',$keys,$allNode)->execute();
			}
			return json_encode(array('message' => 'The selected area has been Saved on your deal.'));

		}

	}

}
