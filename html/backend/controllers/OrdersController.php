<?php
namespace backend\controllers;
use Yii;
use yii\db\Query;
use common\models\Orders;
use common\models\User;
use backend\models\OrdersSearch;
use common\models\OrderComment;
use backend\models\OrderStatus;
use common\models\OrderItem;
use common\models\PaymentMethod;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;


/**
 * OrdersController implements for Orders model.
 */
class OrdersController extends Controller
{
  public function behaviors()
    {
      
            $behaviors['access'] = [
                'class' => AccessControl::className(),
                'rules' => [
                    [                       
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {						    
                        $module = Yii::$app->controller->module->id;                     
                        $action = Yii::$app->controller->action->id;                         
                        $controller = Yii::$app	->controller->id;                        
                        $route = "$controller/$action";                                                    	
                        $post = Yii::$app->request->post();
                        if(\Yii::$app->user->can($route)){
							return true;
						}
                         
                        } 
                   
                    ],
                ],
            ];
            return $behaviors;
	}

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
		$searchModel = new OrdersSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

 
    public function actionView($id)
    {
		if((Orders::findOne($id)) !== null){ 
		$orderComment = new OrderComment();
		$searchModel = new OrderItem();
		$dataProvider = new ActiveDataProvider([
            'query' => OrderItem::find()->joinWith('product')->where(['order_id' => $id])  ,
        ]);
      
	/********** all record of order view list **************/
	$query = new Query;
	$query->select([Orders::tableName().'.*',User::tableName().'.username',User::tableName().'.fullName',User::tableName().'.email as useremail',PaymentMethod::tableName().'.cardType' ,PaymentMethod::tableName().'.maskedNumber',PaymentMethod::tableName().'.customerLocation','tbl_transaction_detail.amount',OrderStatus::tableName().'.status as orderstatus'])  
		->from(Orders::tableName())
		->leftJoin(PaymentMethod::tableName(), Orders::tableName().'.id = '.PaymentMethod::tableName().'.order_id')
		->leftJoin(User::tableName(), Orders::tableName().'.user_id = '.User::tableName().'.id')
		->leftJoin('tbl_transaction_detail', Orders::tableName().'.id = tbl_transaction_detail.order_id')
		->leftJoin(OrderStatus::tableName(), Orders::tableName().'.status = '.OrderStatus::tableName().'.id')
		->where([Orders::tableName().'.id' => $id]); 
		$command = $query->createCommand();
		$getallorderrecords = $command->queryAll();
	/************ end here ********************************/
      
       // $dataProvider = $searchModel->search($id);
		return $this->render('view', [
            'model' => $getallorderrecords,
            'orderComment'=>$orderComment,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
     }else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCreate()
    {
        $model = new Orders();
        if ($model->load(Yii::$app->request->post()) &&  $model->validate() ) {
            //return $this->redirect(['view', 'id' => $model->id]);
            if($model->save()){
            return $this->redirect(['create']);
		}
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

   
    public function actionUpdate($id)
    { 
		//$model = $this->findModel($id);
        $model = Orders::findOne(['id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
		
     public function actionComment(){ 
		$model = new OrderComment();
		$Id = $_POST['OrderComment']['order_id'];
		$orderstatusId = $_POST['OrderComment']['status'];
		
		 if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           if($model->save()){
			   $query = new Query;
			   $condtion = 'id="'.$Id.'"';
			   $query->createCommand()->update(Orders::tableName(), ['status' => $orderstatusId], $condtion)->execute();
			   $modelordercomment = OrderComment::find()->where(['order_id' => $Id])->orderBy(['id' => SORT_DESC,])->all(); 
			   $countcomment = count($modelordercomment);
	
			if($countcomment > 0){
				
				$html='<div class="col-md-12" id="commentlisting">';	
			foreach($modelordercomment as $modelordercommentinfo){
			$modelorderstatus = OrderStatus::findOne(['id' => $modelordercommentinfo['status']]);
			$datetime =  date('M d Y h:i:s A', $modelordercommentinfo['created_at']);
			$html.='
				<div class="col-md-12"><b>'.$datetime.' '.$modelorderstatus['status'].'</b>		
				</div>	
				<div class="col-md-12"> customer Notified		
				</div>
				<div class="col-md-12">'.$modelordercommentinfo['comment'].'</div>
			';
			
			}
			$html.='</div>';
			 $result =array();
			 $result['html'] = $html;
			 $result['ordedrStatus'] = '<div class="col-md-6" id="orderstatus"> <b>'.$modelorderstatus['status'].'</b></div>';
			 return json_encode($result);
			die();
			}
			
	      }
        }
       
     }

   
   /* public function actionDelete($id){
        $this->findModel($id)->delete();
		return $this->redirect(['index']);
    }*/
    
    /* delete via ajax */   
	public function actionDeletebyajax()  
	{
		$pid = $_POST['id'];  
		if($pid) {
			$this->findModel($pid)->delete();
			echo $pid; exit;
		}
	}

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
   public function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	public function actionCancelorder(){
		
		$searchModel = new OrdersSearch();
		$dataProvider = $searchModel->CancelOrderSearch(Yii::$app->request->queryParams);

        return $this->render('cancelorder', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
		
	}
}
