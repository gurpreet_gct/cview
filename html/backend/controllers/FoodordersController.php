<?php
namespace backend\controllers;

use Yii;
//use yii\base\Model;
use common\models;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\db\Query;
use common\models\Foodorders;
use backend\models\FoodordersSearch;
 
class FoodordersController extends \yii\web\Controller {
	
   public function behaviors()
    {
       return [
             'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
             'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
         ];
     
	}
    public function actionIndex()
    {
      $searchModel = new FoodordersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
    
   public function actionCreate()
   {
	   $model= new Foodorders();
	   if($model-> load(yii::$app->request->post()) && $model->validate())
	   {
		   $model->save();
		   return $this->refresh();
		   }
	   return $this->render('create',[
	   'model' => $model,
	   ]);
	   }
	   
	   public function actionView($id)
	   {
		  return $this->render('view',[
		  'model'=> $this->findmodel($id),
		  ]); 
		   }
		   protected function findmodel($id)
		   {
			  if(($model = Foodorders::findOne($id)) !== null){
				  
				  return $model;
				  }
				  else{
					  throw new NotFoundHttpException('Request page does not exist.');
					  }
			   }
  public function actionUpdate($id)
  {
	   $model = $this->findModel($id);
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) { 	
				$model->save();				
             return $this->redirect(['view', 'id' => $model->id]);			  
			
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
	  }
  
  public function actionDelete($id)
   {
	   
	$this-> findmodel($id)->delete();
	return $this->redirect(['index']); 
   }
/* delete via ajax */   
	public function actionDeletebyajax() 
 
		{
			$pid = $_POST['id'];  
			if($pid) {
				$this->findModel($pid)->delete();
				echo $pid; exit;
			}
		}
}



	
