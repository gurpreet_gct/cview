alert("test");
$(document).ready(function() {
	$("#create-store-form").on('beforeSubmit', function (e) {
		var geocoder = new google.maps.Geocoder();

		
		if(($("#store-address").val()=="")&&(($("#store-latitude").val()=="" )||(($("#store-longitude").val()==""))))
		{
			
			var address =["store-street","store-city","store-state","store-country"];
			var faddress="";
			var sep="";
				for(var i=0;i<address.length;i++)
				{
					if(document.getElementById(address[i]).value!="")
					{
						faddress=faddress + sep + document.getElementById(address[i]).value;
						sep=", ";
					}
				}
				
			  geocoder.geocode({'address': faddress}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
				 
				 $("#store-address").val(results[0].formatted_address);
				  $("#store-latitude").val(results[0].geometry.location.lat());
				  $("#store-longitude").val(results[0].geometry.location.lng());
					var ret=true;
					if($("store-latitude").val()=="")
					{
						$(".field-store-latitude .help-block").innerHtml("Latitude can not be blank.");
						ret=false;
					}
					if($("store-longitue").val()=="")
					{
						$(".field-store-longitue .help-block").innerHtml("Longitude can not be blank.");
						ret=false;
					}
					alert($("#store-address").val());
					return false;
					if(ret)
					$("#create-store-form").submit();
				} else {
				  alert('Geocode was not successful for the following reason: ' + status);
				}
			  });

		
		}
		else if($("#store-address").val()=="")
		{
			  
			  var latlng = {lat: parseFloat($("#store-latitude").val()), lng: parseFloat($("#store-longitude").val())};
			  geocoder.geocode({'location': latlng}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
				  if (results[1]) {
					$("#store-address").val(results[1].formatted_address);
					
				  } else {
					alert('No address match with given geo location.');
				  }
				} else {
				  alert('No address match with given geo location.');
				}
			  });

		}
		else
			return true;
		
		return false;
	});
	
	});

// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm ={
    "street_number": {
        "field": "store-street",
        "value": "short_name",
		"joint":false,
		"dropdown":false
    },
    "route": {
        "field": "store-street",
        "value": "short_name",
		"joint":true,
		"dropdown":false
    },
    "sublocality_level_1": {
        "field": "store-street",
        "value": "short_name",
		"joint":true,
		"dropdown":false
    },
    "locality": {
        "field": "store-city",
        "value": "long_name",
		"joint": false,
		"dropdown":false
    },
    "administrative_area_level_2": {
        "field": "store-city",
        "value": "long_name",
		"joint": false,
		"dropdown":false
    },
    "administrative_area_level_1": {
        "field": "store-state",
        "value": "long_name",
		"joint": false,
		"dropdown":false
    },
    "country": {
        "field": "store-country",
        "value": "long_name",
		"joint": false,
		"dropdown":true		
    },
    "postal_code": {
        "field": "store-postal_code",
        "value": "long_name",
		"joint": false		
    }
};

/* {
  "street": {field:"store-street",value:'short_name'},
 // route: 'long_name',
  "locality": {field:"storadministrative_area_level_1e-city",value:'long_name'},
  //: 'short_name',
  "country": {field:"store-country",value:'long_name'}
  //postal_code: 'short_name'
};*/

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('store-address')),
      {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function resetAddress()
{
	
  for (var component in componentForm) {

    document.getElementById(componentForm[component].field).value = '';
    document.getElementById(componentForm[component].field).disabled = false;
  }
    document.getElementById('store-address').value='';
    document.getElementById('store-latitude').value='';
    document.getElementById('store-longitude').value='';
}
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {

    document.getElementById(componentForm[component].field).value = '';
    document.getElementById(componentForm[component].field).disabled = true;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];


    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType].value];

		if(componentForm[addressType].joint)
		{
			var sep="";
			if(document.getElementById(componentForm[addressType].field).value!="")
				sep=", ";
		document.getElementById(componentForm[addressType].field).value =document.getElementById(componentForm[addressType].field).value +sep+ val;	
		}
		else if(componentForm[addressType].dropdown)
		{
		
				var dd = document.getElementById(componentForm[addressType].field);
				for (var i = 0; i < dd.options.length; i++) {
				if (dd.options[i].text === val) {
					dd.selectedIndex = i;
					break;
				}
			}
	
		}
		else
		document.getElementById(componentForm[addressType].field).value = val;
		
	
	}
  }
  
  if(place.geometry.location)
  {
	  document.getElementById("store-latitude").value=place.geometry.location.lat();
	  document.getElementById("store-longitude").value=place.geometry.location.lng();
  }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
