
// To generate graph reports
$('#loading-image').hide();
	
function graphGenerator(type,url,daterange,linktype){
	
	$('.loading-image').show();
	
	var result = {};
	
    $.ajax({
		url:url,
		type:'post',
		dataType:'json',
		data:{'type':type,'daterange':daterange,'linktype':linktype},
		async: false,  
		success:function(data) {
			
			result = data; 
			
				$('.loading-image').hide();
			
			} 
	});
   
   return result;
   
}
