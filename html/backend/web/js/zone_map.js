var map;
var path;
var markers;
var infowindow;
var lastinfow;
var coordinates = [];
var polygons = [];
var infoWind;
var drwnRectangle;
var drawingManager

	
var baseUrl = $('#base_url').val();
$(".loading-image").hide();
$(".limit-update").hide();
$(".extra-input").hide();


function partnerSelect(){
	
	var partner_id = $('#partner_id').val();
	var workId = $('#workId').val();
	
	if(partner_id>0){
		
		$(".loading-image").show();
		$(".extra-input").show();		
		var srcPath = baseUrl+'/workorders/getzone';
	
		$.getJSON(srcPath,{ partner_id: partner_id }, function (data){
		
			initialize(data);
			$(".loading-image").hide();
		});
		
		/* get dropdown workorders */
		
		$(".limit-update").hide();
		
		$.ajax({
				type: "GET",
				url: baseUrl+'/workorders/getdeals',
				data: {'partner_id':partner_id},
				success: function( response ) {
					
					$("#workOrderId").html(response);
					$('#workOrderId > option[value="'+workId+'"]').attr('selected','selected');
					$('#workOrderId').trigger('change');
					
				}
		});
		
		/* get dropdown workorders */
		
		// set map address
		
		var srcaddressPath = baseUrl+'/workorders/partneraddress';
		$.getJSON(srcaddressPath,{ partner_id: partner_id }, function (data){
			
			set_advertizer_map(data.address);
			
		});
		
		
	}else{
	
		$(".loading-image").show();
		$(".extra-input").hide();
		
		var srcPath = baseUrl+'/workorders/getzone';
	
		$.getJSON(srcPath,{ partner_id: 'ALL' },function (data){
		
			initialize(data);
			$(".loading-image").hide();
		});
		
		
		set_advertizer_map("");
	}
	
}

window.onload = partnerSelect;

$('#partner_id').change(function(){
	
	partnerSelect();
	
});


function dealSelect(){
	
	var workOrderId = $('#workOrderId').val();
	var partner_id = $('#partner_id').val();
	
	if(workOrderId>0){
		
		// alert(deal_id);
		$(".loading-image").show();
		$(".extra-input").show();		
		var srcPath = baseUrl+'/workorders/getdealzone';
	
		$.getJSON(srcPath,{ workOrderId: workOrderId }, function (data){
		
			initialize(data);
			$(".limit-update").show();
			$(".loading-image").hide();
		});
		
			
		
		
	}else{
		
		//alert(workOrderId);
		$(".loading-image").show();
		$(".extra-input").show();
		
		var srcPath = baseUrl+'/workorders/getdealzone';
	
		$.getJSON(srcPath,{ workOrderId: 'ALL',partner_id:partner_id },function (data){
		
			initialize(data);
			$(".limit-update").hide();
			$(".loading-image").hide();
		});
		
	}
	
	// set map address
		
	var srcaddressPath = baseUrl+'/workorders/partneraddress';
	$.getJSON(srcaddressPath,{ partner_id: partner_id }, function (data){
		
		set_advertizer_map(data.address);
		
	});
	
}

$('#workOrderId').change(function(){
	
	dealSelect();
	if($(this).val()){
			$('#geo_id').show();
				
			
	}
	
});


var notific_limit; 

$("#notification_limit").keyup(function(){
	
	notific_limit 		= $("#notification_limit").val() ; 	
	
	$(".notific_limit").val(notific_limit);
	
});

			

function initialize(data){
	
	var myLatlng = new google.maps.LatLng( -37.814107, 144.963280 );

	 var myOptions = {
            zoom: Number($("#prev_zoom").val()),
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

	 map 			= new google.maps.Map(document.getElementById("map-canvas"), myOptions);
	
	// drawingmanager 
	
	 drawingManager = new google.maps.drawing.DrawingManager({
															drawingControl: true,
															drawingControlOptions: {
															  position: google.maps.ControlPosition.TOP_CENTER,
															  drawingModes: [
																google.maps.drawing.OverlayType.POLYGON
															  ]
															},
															polygonOptions:{
																strokeColor: '#FF0000',
																fillColor: '#FF0000',
																draggable: false,
																fillOpacity: 0.35,
																strokeOpacity: 0.8
															}
														  });
	drawingManager.setMap(map);
		
	google.maps.event.addDomListener(drawingManager, 'polygoncomplete', function(polygon) {
			
			drwnRectangle = polygon;
			polygon.setEditable(true);
			
			drawingManager.setOptions({
				  drawingControl: false
				});
			drawingManager.setMap(null);
			
			openInfoWindowPolygon(polygon);

			google.maps.event.addListener(polygon.getPath(), 'set_at', function () {
				
					if (infoWind) {
						infoWind.close();
					}
				openInfoWindowPolygon(polygon);
				});
			
			google.maps.event.addListener(polygon.getPath(), 'insert_at', function () {
				
				if (infoWind) {
						infoWind.close();
				}
				openInfoWindowPolygon(polygon);
			});
	});
		
	// drawingmanager 
		
	// Saved zones 
	
	var mypolygons  = [];
	var bounds      = new google.maps.LatLngBounds();
	
	if(data.length>0){
		
			var message_limit	= Number(data[0].message_limit);
			
			$("#notification_limit").val(message_limit);
			
			for(i = 0; i < data.length; i++){   
				
				// alert('asasa');
				
				var area_name 		= data[i].area_name ; 
				var deal_name 		= data[i].deal_name ; 
				var deal_id 		= Number(data[i].deal_id) ; 
				var region_id	 	= Number(data[i].region_id);
				var message_limit 	= Number(data[i].message_limit);
				
				LatLng 				= [];
				
				if(data[i].latlongData.length>0){
					
					for (j = 0; j < data[i].latlongData.length; j++) {

						var lat = Number(data[i].latlongData[j].latitude);
						var lng = Number(data[i].latlongData[j].longitude);

						LatLng.push(new google.maps.LatLng(lat,lng));
						bounds.extend(LatLng[LatLng.length-1]);
						map.fitBounds(bounds);
					}
					
				}
					
					
				var polygonOptions = {
						paths:LatLng,
						strokeColor: '#FF0000',
						fillColor: '#FF0000',
						draggable: false,
						fillOpacity: 0.35,
						strokeOpacity: 0.8
					};  
					
				mypolygons.push(new google.maps.Polygon(polygonOptions));
				
				mypolygons[mypolygons.length-1].setMap(map);
				
				//map.fitBounds(bounds);
				if(deal_name==null){
					deal_name = 'N/A';
				}
				content1 	= '<form id="delete_zone" class="delete_zone" action="" method="post"><table wdth="100%">'+
								'<tr><td><strong>'+'Geolocation : </strong></td><td> <input type="text" id="area_name"  class="form-control" name="area_name" value="'+area_name+'" required="required" /></td></tr>'+
								'<tr><td><strong>'+'Deal : </strong></td><td>'+deal_name+
								'<input type="hidden" id="region_id" name="region_id" value="'+region_id+'" />'+
								'<input type="hidden" id="partner_id" name="partner_id" value="'+$("#partner_id").val()+'" />'+
								'<input type="hidden" id="dealid" name="dealid" value="'+deal_id+'" />'+
								'<input type="hidden" id="notific_limit"  class="notific_limit" name="notific_limit" value="+'+message_limit+'" /><input type="hidden" id="deal_id" name="deal_id" value="'+deal_id+'" />'+
								'</td></tr>';
				content2 	= '<tr><td colspan="2"><input type="button" class="btn btn-primary btn_submit" id="save" name="save" value="Edit" onclick="return edit_polygon();" /><button type="button" class="btn btn-danger text-center margin" id="delete" class="deleteInfo" onclick="return delete_polygon();">Delete</button></td></tr>'+
								
							  '</table></form>';
				
				
				
			show_infowindow(mypolygons[mypolygons.length-1],content1,content2); /* */
			
			
			
			
		}
	
	}else{
		
		var mypolygons  = [];	
		$("#notification_limit").val('');
		
	}	
	
	/* google.maps.event.addListener(map, "zoom_changed", function () {
		//var zoomValue = map.getZoom();
		
		if (map.getZoom() > 15) {
			
			map.setZoom(Number($("#prev_zoom").val()));
		}
		
	}); */
	
}


// info window for DB zones

function show_infowindow(poly,content1,content2){
	
	google.maps.event.addListener(poly, 'click', function (event) {
		
		poly.setEditable(true);
		var vertices 	= this.getPath();
		var infowindow 	= new google.maps.InfoWindow();
		
		NewAllLatLng	= [];
		
			vertices.forEach(function(vert, index){
				NewAllLatLng += vert.toString();
			});
		
			
		
			
		var contents = content1+'<input type="hidden" id="latlng" name="latlng" value="'+NewAllLatLng+'" />'+content2;	
		
		
		
		
		google.maps.event.addListener(vertices, 'set_at', function(){
			
				if (lastinfow) {
					lastinfow.close();
				}
				
			show_infowindow(poly,content1,content2);
			
		});
		
		google.maps.event.addListener(vertices, 'insert_at', function(){
			
			if (lastinfow) {
					lastinfow.close();
			}
			
			show_infowindow(poly,content1,content2);
			
		});
		
		if (lastinfow) {
				lastinfow.close();
			}
			
			infowindow.setContent(contents);
			var bounds 		= new google.maps.LatLngBounds();
			
			vertices.forEach(function(xy,i){
					bounds.extend(xy);
			});
			
			infowindow.setPosition(bounds.getCenter());
			infowindow.open(map);	
			
		
		lastinfow = infowindow;
		
	});
	
	
}

	
	
// info window for drawing manager
	
// info window for drawing manager
function openInfoWindowPolygon(polygon) {

	var vertices 	= polygon.getPath();
	var infowindow1 = new google.maps.InfoWindow();
	allLatLng		= [];
	vertices.forEach(function(vert, index){
		allLatLng += vert.toString();
	});
	
	
	contents 	= '<form id="savezone" name="savezone" methd="post"><table width="100%">'+
				  '<div class="form-group field-geozone-area_name required"><label class="control-label" for="geozone-area_name">Area Name</label><input type="text" id="area_name" class="form-control" name="area_name"><div class="help-block"></div></div><div class="form-group field-geozone-deal_id"><input type="hidden" id="dealid" name="dealid" value="'+$("#workOrderId").val()+'" /></div><p class="text-center">'+
				  '<input type="hidden" id="latlng" name="latlng" value="'+allLatLng+'" /><input type="hidden" id="notific_limit"  class="notific_limit" name="notific_limit" value="" />'+'<input type="hidden" id="latlng" name="latlng" value="'+allLatLng+'" />'+'<input type="hidden" id="partner_id" name="partner_id" value="'+$("#partner_id").val()+'" />'+
				  '<input type="button" class="btn btn-primary btn_submit" id="save" name="save" value="Save" onclick="return save_polygon();" /><button type="button" class="btn btn-danger text-center margin" id="delete" class="deleteInfo" onclick="return clear_shape();">Delete</button></p></form>'+
				  '</table></form>';
				  
	infowindow1.setContent(contents);

	var bounds 		= new google.maps.LatLngBounds();
	
	vertices.forEach(function(xy,i){
		bounds.extend(xy);
	});
	
	infowindow1.setPosition(bounds.getCenter());
	infowindow1.open(map);
	
	infoWind = infowindow1;
	
}

google.maps.event.addDomListener(window, 'load', initialize);

// edit_polygon 
	function edit_polygon() {
	var redirect 			= $("#redirect").val();	
	var gotourl 			= $("#gotourl").val();			
		var area_name 	= $("#area_name").val();
		
		if(area_name==""){
			alert("Please enter geolocation name.");
			$("#area_name").focus();
		}else{
			$.ajax({
			type: "POST",
			url: baseUrl+'/workorders/editzone',
			data: $("#delete_zone").serialize(),
			success: function( response ) {
				
				if(response=="SUCCESS"){
					alert("Geolocation updated successfully."); 
					if(gotourl){
						  window.location.href = baseUrl+gotourl;
						}else{
						window.location.href = redirect;
					}
				}else{
					
					alert(response); 
				}
				
			}
		});
		
		}
		
	}
	

// delete_polygon 
function delete_polygon() {
		var gotourl 	= $("#gotourl").val();
		var redirect 			= $("#redirect").val();	
		$.ajax({
			type: "POST",
			url: baseUrl+'/workorders/deletezone',
			data: $("#delete_zone").serialize(),
			success: function( response ) {
				 if(response=="SUCCESS"){
					alert("Geolocation deleted successfully."); 
					if(gotourl){
						  window.location.href = baseUrl+gotourl;
						}else{
					window.location.href = redirect;
				}
					
				}else{
					alert(response); 
				}
					
				} 
		});
	
}

// clear new shape


function clear_shape(){
	
	drawingManager.setOptions({
	  drawingControl: true
	});
	
	drawingManager.setMap(map);
	drwnRectangle.setMap(null);
	infoWind.setMap(null);
	
}	

// save Notification limit in a day 

$("#save_limit").click(function(){
	
	var deal_id 			= $("#workOrderId").val();
	var notification_limit 	= $("#notification_limit").val();
	
	if(deal_id==""){
		alert("Please select a deal.");
		
		
	}else if(!(Math.floor(notification_limit) == notification_limit) && !$.isNumeric(notification_limit)){
		
		alert("Please enter valid number.");
		$("#notification_limit").focus();
		
	}else if(notification_limit<=0){
		
		alert("Please enter notification limit greater than 0.");
		$("#notification_limit").focus();
		
	}else{
		
		
		$.ajax({
			type: "POST",
			url: baseUrl+'/workorders/notificationlimit',
			data: {'deal_id':deal_id,'notification_limit':Number(notification_limit)},
			success: function( response ) {
				
			 if(response=="SUCCESS"){
					 
					alert("Notification limit is set successfully."); 
					
					// $('#deal_id').trigger('change');
					
				}else{
					
					alert(response); 
					
				} 
				
			}
		});
		
	}
	
});


/* place Search  */
var placeSearch, autocomplete;

// fetch suggested address from google api

function search_address(){
	
			var searchInput 	= document.getElementById('map_address');
			var autocomplete 	= new google.maps.places.Autocomplete(searchInput);
			
			google.maps.event.addListener(autocomplete, 'place_changed', function () {
				var place = autocomplete.getPlace();
				document.getElementById('latitude').value 	= place.geometry.location.lat();
				document.getElementById('longitude').value 	= place.geometry.location.lng();
				$("#go_to").trigger('click');
				
			});
			
}
google.maps.event.addDomListener(window, 'load', search_address);

// validate correct address for address field

$("#map_address").keyup(function(){
	
	var searchInput 	=   document.getElementById('map_address');
	var StoreAddress	=	$.trim(searchInput.value);
		geocoder 		= 	new google.maps.Geocoder();	
		
		if( StoreAddress == '' ){
	
			searchInput.value='';
			searchInput.focus();
			return false;
			
		}else if(StoreAddress !=''){
			
		
			geocoder.geocode({'address': StoreAddress}, function(results, status){
				
				if(status=='OK'){
						var lat 	= results[0].geometry.location.lat();
						var lng 	= results[0].geometry.location.lng();
						// var address	= results[0].formatted_address;
						var myLatLng = new google.maps.LatLng( lat, lng );
						var bounds = new google.maps.LatLngBounds();
						bounds.extend(myLatLng);
						map.fitBounds(bounds);
						
					var listener = google.maps.event.addListener(map, "idle", function(){ 
						   // map.setZoom(6); 
						   // if (map.getZoom() > 16)
						  google.maps.event.removeListener(listener); 
					});
						
						$('#latitude').val(lat) ;
						$('#longitude').val(lng) ;
						
						// $(formObj).submit(); 
					
				}
				/*else if(status=='ZERO_RESULTS'){
					
						alert('Please enter correct address'); 	
						
						searchInput.value='';
						return false;
					
				}else{
				
					alert('An error occured while validating this address'); 	
					searchInput.value='';
					return false;
				}*/
			});
		} 
});


$(".check_addr_type").click(function(){
	var addr_type = $(this).val();
	if(addr_type=="address"){
		$("#txt_addr").show();
		$("#latilong").hide();
	}else{
		$("#txt_addr").hide();
		
		$("#latilong").show();
		$("#latitude").attr("type","text");
		$("#longitude").attr("type","text");
	}
	
});


var longitude = document.getElementById("longitude").value;


$("#go_to").click(function(){
	
	var latitude 	= $("#latitude").val();
	var longitude 	= $("#longitude").val();
	var searchInput 	=   document.getElementById('map_address');
	var address = document.getElementById("map_address").value;
	
	if(latitude=='' || longitude==''){
		
		alert("Please enter a valid address or Latitude/Longitude");
	}else{
		
		var point = new google.maps.LatLng(latitude, longitude);
		var geocoder = new google.maps.Geocoder();
		
		geocoder.geocode({latLng: point}, function(results, status) {
			
		  if (status == google.maps.GeocoderStatus.OK) {
			  
			if (results[0]) {
				
				map.setCenter(results[0].geometry.location);
						  
				var lat 	= results[0].geometry.location.lat();
				var lng 	= results[0].geometry.location.lng();
						
				// var myLatLng 	= new google.maps.LatLng( lat, lng );
				// var bounds 		= new google.maps.LatLngBounds();
					// bounds.extend(myLatLng);
					// map.fitBounds(bounds);
					
				var infowindow 	= new google.maps.InfoWindow();
				var marker = new google.maps.Marker({
									map: map,
									position: results[0].geometry.location,
									title: address
								});

				// Adding a click event to the marker
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.setContent(address);
					infowindow.open(map, this);
				}); 
				
				var listener 	= google.maps.event.addListener(map, "idle", function(){ 
										  // if (map.getZoom() > 16) map.setZoom(11); 
										 map.setZoom(14); 
										  google.maps.event.removeListener(listener); 
									});
						
				$('#latitude').val(lat);
				$('#longitude').val(lng);
				
				if(document.getElementById('addr_type').checked){
					
					$('#map_address').val(results[0].formatted_address)
				}
				
			}
			
		  }else if(status=='ZERO_RESULTS'){
					
					alert('Please enter a valid address or Latitude/Longitude'); 	
					
					searchInput.value='';
					return false;
				
			}else{
			
				alert('An error occured while validating this address'); 	
				searchInput.value='';
				return false;
			}
			
		});
		
		
	}
	
});


// reset lat long of map for advertiser address

function set_advertizer_map(addressInput){
	// alert(addressInput);
	var searchInput 	= addressInput?addressInput:'Melbourne VIC, Australia';
	
	var StoreAddress	=	$.trim(searchInput);
		geocoder 		= 	new google.maps.Geocoder();	
		
		if(StoreAddress !=''){
			
		
			geocoder.geocode({'address': StoreAddress}, function(results, status){
				
				if(status=='OK'){
					
						var lat 	= results[0].geometry.location.lat();
						var lng 	= results[0].geometry.location.lng();
						var address	= results[0].formatted_address;
						var myLatLng = new google.maps.LatLng( lat, lng );
						var bounds = new google.maps.LatLngBounds();
						bounds.extend(myLatLng);
						map.fitBounds(bounds);
						
					var listener = google.maps.event.addListener(map, "idle", function(){ 
						
						  google.maps.event.removeListener(listener); 
					});
						
						$('#latitude').val(lat) ;
						$('#longitude').val(lng) ;
						$('#map_address').val(address) ;
											
				}
			});
		}
}

$(document).ajaxError(function( event, request, settings ) {
	alert("Error requesting page " + settings.url);
});


// save_polygon 
	function save_polygon() {
		var redirect 			= $("#redirect").val();	
		var area_name 			= $("#area_name").val();
		var deal_id 			= $("#workOrderId").val();
		var gotourl 			= $("#gotourl").val();
		var  notification_limit = '';
		if(gotourl){
			var  notification_limit = 1;
		}
		if($("#notification_limit").val())
		{
			notification_limit 	= $("#notification_limit").val();
		}
		
		if(deal_id==""){
			
			alert("Please select a partner and deal.");
			
			
		}
		else if(area_name==""){
			
			alert("Please enter area name.");
			$("#area_name").focus();
			
		}else if(!(Math.floor(notification_limit) == notification_limit) && !$.isNumeric(notification_limit)){
			
			alert("Please enter valid number.");
			$("#notification_limit").focus();
			
		}else if(notification_limit<=0){
			
			alert("Please enter notification limit greater than 0.");
			$("#notification_limit").focus();
			
		}else{
			
			
			$.ajax({
				type: "POST",
				url: baseUrl+'/workorders/savezone',
				data: $("#savezone").serialize(),
				success: function( response ) {
					
					 if(response=="SUCCESS"){
						 
						alert("Geolocation created successfully."); 
						if(gotourl){
						  window.location.href = baseUrl+gotourl;
						}else{
						window.location.href = redirect;
					 }			
						
					}else{
						
						alert(response); 
						
					} 
					
				}
			});
			
		}
		
	}


/* change deal */
