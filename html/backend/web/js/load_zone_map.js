var map;
var path;
var markers;
var infowindow;
var lastinfow;
var coordinates = [];
var polygons = [];
var infoWind;
var drwnRectangle;
var inputNo;
var drawingManager;

	
var baseUrl = $('#base_url').val();

$(".loading-image").hide();

function initialize(){
	
	var myLatlng = new google.maps.LatLng( -37.814107, 144.963280 );

	 var myOptions = {
            zoom: Number($("#prev_zoom").val()),
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

	 map 			= new google.maps.Map(document.getElementById("map-canvas"), myOptions);
	
	// drawingmanager 
	
	drawingManager = new google.maps.drawing.DrawingManager({
															drawingControl: true,
															drawingControlOptions: {
															  position: google.maps.ControlPosition.TOP_CENTER,
															  drawingModes: [
																google.maps.drawing.OverlayType.POLYGON
															  ]
															},
															polygonOptions:{
																strokeColor: '#FF0000',
																fillColor: '#FF0000',
																draggable: false,
																fillOpacity: 0.35,
																strokeOpacity: 0.8
															}
														  });
														  
	drawingManager.setMap(map);
	inputNo = 1;
	
	google.maps.event.addDomListener(drawingManager, 'polygoncomplete', function(polygon) {
		
			
			
			drwnRectangle = polygon;
			polygon.setEditable(true);
			
			drawingManager.setOptions({
				  drawingControl: false
				});
			
			drawingManager.setDrawingMode(null);
			
			openInfoWindowPolygon(polygon);

			google.maps.event.addListener(polygon.getPath(), 'set_at', function () {
				
					if (infoWind) {
						infoWind.close();
					}
				openInfoWindowPolygon(polygon);
				
				
			});
			
			google.maps.event.addListener(polygon.getPath(), 'insert_at', function () {
				
				if (infoWind) {
						infoWind.close();
				}
				openInfoWindowPolygon(polygon);
			});
			
			inputNo++;
	});
		
	// drawingmanager 
		
	/* google.maps.event.addListener(map, "zoom_changed", function () {
		//var zoomValue = map.getZoom();
		
		if (map.getZoom() > 15) {
			
			map.setZoom(Number($("#prev_zoom").val()));
		}
		
	}); */
	
}

	
// info window for drawing manager
function openInfoWindowPolygon(polygon) {
	
	var vertices 	= polygon.getPath();
	var infowindow1 = new google.maps.InfoWindow();
	allLatLng		= [];
	vertices.forEach(function(vert, index){
		allLatLng += vert.toString();
	});
	
	document.getElementById('geoloc_lat_lng').innerHTML = "<div id='element_container_"+inputNo+"'><input type='hidden' class='alllstaLongs' id='alllstaLongs_"+inputNo+"' name='alllstaLongs[]' value='"+allLatLng+"' /></div>";	
	
	
	contents 	= '<form id="savezone" name="savezone" methd="post"><table width="100%">'+
				  '<div class="form-group field-geozone-area_name required"><div class="help-block"></div></div><div class="form-group field-geozone-deal_id"><input type="hidden" id="dealid" name="dealid" value="'+$("#deal_id").val()+'" /></div><p class="text-center">'+
				  '<input type="hidden" id="latlng" name="latlng" value="'+allLatLng+'" /><input type="hidden" id="notific_limit"  class="notific_limit" name="notific_limit" value="" />'+'<input type="hidden" id="latlng" name="latlng" value="'+allLatLng+'" />'+
				  '<button type="button" class="btn btn-danger text-center margin" id="delete" class="deleteInfo" onclick="return clear_shape('+inputNo+');">Delete</button></p></form>'+
				  '</table></form>';
				  
	infowindow1.setContent(contents);

	var bounds 		= new google.maps.LatLngBounds();
	
	vertices.forEach(function(xy,i){
		bounds.extend(xy);
	});
	
	infowindow1.setPosition(bounds.getCenter());
	infowindow1.open(map);
	
	infoWind = infowindow1;
	
}

google.maps.event.addDomListener(window, 'load', initialize);



// place Search 

var placeSearch, autocomplete;

// fetch suggested address from google api

/**/ 

function search_address(){
	
		if($("#map_addresses").val()!='' && $("#map_addresses").val().length>1){
			
			$(".pac-container.pac-logo").css({'display':'','z-index':0,'position':''});
			$(".pac-container.pac-logo").css({'display':'block','z-index':1511,'position':'relative'});
			
		}
		
			var searchInput 	= document.getElementById('map_addresses');
			var autocomplete 	= new google.maps.places.Autocomplete(searchInput);
			//console.log(autocomplete);
			google.maps.event.addListener(autocomplete, 'place_changed', function () {
				var place = autocomplete.getPlace();
				document.getElementById('latitude').value 	= place.geometry.location.lat();
				document.getElementById('longitude').value 	= place.geometry.location.lng();
				
				
				$("#go_to").trigger('click');
			});
			
			
}

// window.onload = search_address(); 

google.maps.event.addDomListener(window, 'load', search_address);



// validate correct address for address field


//$(".pac-container.pac-logo").css({'display':'block','z-index':1511,'position':'relative'});

$("#map_addresses").keyup(function(){
	
	search_address(); 
	
	var searchInput 	=   document.getElementById('map_addresses');
	
	var StoreAddress	=	$.trim(searchInput.value);
	
		geocoder 		= 	new google.maps.Geocoder();	
		
		if( StoreAddress == '' ){
	
			searchInput.value='';
			searchInput.focus();
			return false;
			
		}else if(StoreAddress !=''){
			
			geocoder.geocode({'address': StoreAddress}, function(results, status){
				
				if(status=='OK'){
						var lat 	= results[0].geometry.location.lat();
						var lng 	= results[0].geometry.location.lng();
						// var address	= results[0].formatted_address;
						var myLatLng = new google.maps.LatLng( lat, lng );
						var bounds = new google.maps.LatLngBounds();
						bounds.extend(myLatLng);
						map.fitBounds(bounds);
					
					var listener = google.maps.event.addListener(map, "idle", function(){ 
						   // map.setZoom(6); 
						   // if (map.getZoom() > 16)
						  google.maps.event.removeListener(listener); 
					});
						
						$('#latitude').val(lat) ;
						$('#longitude').val(lng) ;
						
						// $(formObj).submit(); 
					
				}
				/* else if(status=='ZERO_RESULTS'){
					
						alert('Please enter correct address'); 	
						
						searchInput.value='';
						return false;
					
				}else{
				
					alert('An error occured while validating this address'); 	
					searchInput.value='';
					return false;
				} */
			});
		} 
});


$(".check_addr_type").click(function(){
	var addr_type = $(this).val();
	if(addr_type=="address"){
		$("#txt_addr").show();
		$("#latilong").hide();
	}else{
		$("#txt_addr").hide();
		
		$("#latilong").show();
		$("#latitude").attr("type","text");
		$("#longitude").attr("type","text");
	}
	
});


var longitude = document.getElementById("longitude").value;


$("#go_to").click(function(){
	
	var latitude 	= $("#latitude").val();
	var longitude 	= $("#longitude").val();
	var searchInput 	=   document.getElementById('map_addresses');
	var address = document.getElementById("map_addresses").value;
	
	if(latitude=='' || longitude==''){
		
		alert("Please enter a valid address or Latitude/Longitude");
	}else{
		
		var point = new google.maps.LatLng(latitude, longitude);
		var geocoder = new google.maps.Geocoder();
		
		geocoder.geocode({latLng: point}, function(results, status) {
			
		  if (status == google.maps.GeocoderStatus.OK) {
			  
			if (results[0]) {
				
				map.setCenter(results[0].geometry.location);		  
				var lat 	= results[0].geometry.location.lat();
				var lng 	= results[0].geometry.location.lng();
					
				
				var myLatLng 	= new google.maps.LatLng( lat, lng );
				/* var bounds 		= new google.maps.LatLngBounds();
					bounds.extend(myLatLng);
					map.fitBounds(bounds); */
				var infowindow 	= new google.maps.InfoWindow();
				var marker = new google.maps.Marker({
									map: map,
									position: results[0].geometry.location,
									title: address
								});

				// Adding a click event to the marker
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.setContent(address);
					infowindow.open(map, this);
				}); 
				
				var listener 	= google.maps.event.addListener(map, "idle", function(){ 
										  // if (map.getZoom() > 16) map.setZoom(11); 
										  // map.setZoom(6); 
										  google.maps.event.removeListener(listener); 
									});
						
				$('#latitude').val(lat) ;
				$('#longitude').val(lng) ;
				
				if(document.getElementById('addr_type').checked){
					
					$('#map_addresses').val(results[0].formatted_address)
				}
				
				//$('#map_addresses').val(results[0].formatted_address) ;
			  
			}
			
		  }else if(status=='ZERO_RESULTS'){
					
					alert('Please enter a valid address or Latitude/Longitude'); 	
					
					searchInput.value='';
					return false;
				
			}else{
			
				alert('An error occured while validating this address'); 	
				searchInput.value='';
				return false;
			}
			
		});
		
		
	}
	
});


function clear_shape(ele_no){
	
	document.getElementById('element_container_'+ele_no).innerHTML = ' ';
	// To show:
	drawingManager.setOptions({
	  drawingControl: true
	});
	
	drawingManager.setMap(map);
	
	drwnRectangle.setMap(null);
	infoWind.setMap(null);
	
	
}	
