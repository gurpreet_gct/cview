$(function () {
var now = new Date();
var start = new Date(now.getFullYear(), 0, 0);
var diff = now - start;
var oneDay = 1000 * 60 * 60 * 24;
var currentYear = (new Date).getFullYear();
var day = Math.floor(diff / oneDay);

/* New User chart Begin here */	
	var temp = new Array();
	var temp = $('#newUserDatVal').val().split('-');
	for (a in temp ) {
		temp[a] = parseInt(temp[a], 10); // Explicitly include base as per Álvaro's comment
	}
		$('#newUserChart').highcharts({
       chart: {
                                        zoomType: 'x'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    subtitle: {
                                        text: document.ontouchstart === undefined ?
                                                'Click and drag in the plot area to zoom in' :
                                                'Pinch the chart to zoom in'
                                    },
                                    xAxis: {
                                        type: 'datetime',
                                        minRange: 7 * 24 * 3600000 // fourteen days
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'New Users Per day'
                                        }
                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    plotOptions: {
                                        area: {
                                            fillColor: {
                                                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                                                stops: [
                                                    [0, Highcharts.getOptions().colors[0]],
                                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                                ]
                                            },
                                            marker: {
                                                radius: 2
                                            },
                                            lineWidth: 1,
                                            states: {
                                                hover: {
                                                    lineWidth: 1
                                                }
                                            },
                                            threshold: null
                                        }
                                    },

                                    series: [{
                                        type: 'area',
                                        name: 'New Users',
                                        pointInterval: 24 * 3600 * 1000,
                                        pointStart: Date.UTC(currentYear, 0, day-6),
                                        data:[temp[6],temp[5],temp[4],temp[3],temp[2],temp[1],temp[0]]
                                    }]
                                });
								
	/* New User chart End here */								
	/* Active User Chart start here */	
	var temp2 = new Array();
	var temp2 = $('#activeUserVal').val().split('-');
	for (a in temp2 ) {
		temp2[a] = parseInt(temp2[a], 10); // Explicitly include base as per Álvaro's comment
	}
		$('#ActiveUserChart').highcharts({
                                    chart: {
                                        zoomType: 'x'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    subtitle: {
                                        text: document.ontouchstart === undefined ?
                                                'Click and drag in the plot area to zoom in' :
                                                'Pinch the chart to zoom in'
                                    },
                                    xAxis: {
                                        type: 'datetime',
                                        minRange: 7 * 24 * 3600000 // fourteen days
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Active Users Per day'
                                        }
                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    plotOptions: {
                                        area: {
                                            fillColor: {
                                                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                                                stops: [
                                                    [0, Highcharts.getOptions().colors[0]],
                                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                                ]
                                            },
                                            marker: {
                                                radius: 2
                                            },
                                            lineWidth: 1,
                                            states: {
                                                hover: {
                                                    lineWidth: 1
                                                }
                                            },
                                            threshold: null
                                        }
                                    },

                                    series: [{
                                        type: 'area',
                                        name: 'Active Users',
                                        pointInterval: 24 * 3600 * 1000,
                                        pointStart: Date.UTC(currentYear, 0, day-6),
                                        data: [temp2[6],temp2[5],temp2[4],temp2[3],temp2[2],temp2[1],temp2[0]]
                                    }]
                                });
		/* Active User Chart end here */	
/* Sessions Chart Start here **********************/	
var userSession = new Array();
	var userSession = $('#userSession').val().split('-');
	for (a in userSession ) {
		userSession[a] = parseInt(userSession[a], 10); // Explicitly include base as per Álvaro's comment
	}
		$('#sessionChart').highcharts({
                                    chart: {
                                        zoomType: 'x'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    subtitle: {
                                        text: document.ontouchstart === undefined ?
                                                'Click and drag in the plot area to zoom in' :
                                                'Pinch the chart to zoom in'
                                    },
                                    xAxis: {
                                        type: 'datetime',
                                        minRange: 7 * 24 * 3600000 // fourteen days
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Sessions Per day'
                                        }
                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    plotOptions: {
                                        area: {
                                            fillColor: {
                                                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                                                stops: [
                                                    [0, Highcharts.getOptions().colors[0]],
                                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                                ]
                                            },
                                            marker: {
                                                radius: 2
                                            },
                                            lineWidth: 1,
                                            states: {
                                                hover: {
                                                    lineWidth: 1
                                                }
                                            },
                                            threshold: null
                                        }
                                    },

                                    series: [{
                                        type: 'area',
                                        name: 'Users',
                                        pointInterval: 24 * 3600 * 1000,
                                        pointStart: Date.UTC(currentYear, 0, day-6),
                                        data:[userSession[6],userSession[5],userSession[4],userSession[3],userSession[2],userSession[1],userSession[0]]
                                    }]
                                });
		/* Session by area chart start here */
		$('#sessionByAreaChart').highcharts({
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: ''
                                },
                                subtitle: {
                                    text: ''
                                },
                                xAxis: {
                                    type: 'category',
                                    labels: {
                                        rotation: -45,
                                        style: {
                                            fontSize: '13px',
                                            fontFamily: 'Verdana, sans-serif'
                                        }
                                    }
                                },
                                yAxis: {
                                    min:0,
                                    title: {
                                        text: 'No. of Users browsing the app'
                                    }
                                },
                                legend: {
                                    enabled: false
                                },
                                tooltip: {
                                    pointFormat: 'No. of Users browsing the app: <b>{point.y:.1f}</b>'
                                },
                                series: [{
                                    name: 'Population',
                                    data: [['Kuala Lumpur', 105],
                                        ['Singapore', 200],
                                        ['Jakarta', 250],
                                        ['Bangkok', 350],
                                        ['Sydney', 150],
                                        ['Melbourne', 180],
                                        ['Manila', 250],
                                        ['Indonesia', 350],
                                        ['Thailand', 450]
                                        

                                    ],
                                    dataLabels: {
                                        enabled: true,
                                        rotation: -90,
                                        color: '#FFFFFF',
                                        align: 'right',
                                        x: 4,
                                        y: 10,
                                        style: {
                                            fontSize: '13px',
                                            fontFamily: 'Verdana, sans-serif',
                                            textShadow: '0 0 3px black'
                                        }
                                    }
                                }]
                            });
/* Device chart ***********************************************************************************/
	var userdevice = new Array();
	var userdevice = $('#userDevices').val().split('-');
	for (a in userdevice ) {
		userdevice[a] = parseInt(userdevice[a], 10); // Explicitly include base as per Álvaro's comment
	}
   $('#UsingdeviceChart').highcharts({
        chart: {
            type: 'funnel',
            marginRight: 100
        },
        title: {
            text: '',
            x: -50
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b> ({point.y:,.0f})',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                neckWidth: '30%',
                neckHeight: '25%'

                //-- Other available options
                // height: pixels or percent
                // width: pixels or percent
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Device users',
            data: [
                ['iPhone',   userdevice[2]],               
                ['Android', userdevice[1]],              
                ['Desktop',userdevice[0]],
                ['Windows',0]
            ]
        }]
    });	
/* Membership Sedmention chart */
	var memval = new Array();
	var memval = $('#membershipVal').val().split('-');
	for (a in memval ) {
		memval[a] = parseInt(memval[a], 10); // Explicitly include base as per Álvaro's comment
	}
 $('#membershipChart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
       
        xAxis: { title: {
                text: 'Age in Years'
            },
            categories: [
                '15-18',
                '19-25',
                '26-30',
                '31-35',
                '36-40',
                '41-45',
                '46-60',
                '61+'
                
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Users'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} User</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Male',
    
            data:
                    [memval[0],memval[1],memval[2],memval[3],memval[4],memval[5],memval[6],memval[7]]

        }, {
            name: 'Female',
            data: [memval[8],memval[9],memval[10],memval[11],memval[12],memval[13],memval[14],memval[15]]

        }]
    });

/*   Screen Views By Screen Name  *********************/

    $('#cc5').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Screen Views'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: ''
        },
        series: [{
            name: 'Population',
            data: [
               
               
                ['My Preferences', 10.0],
                ['My Deals', 9.3],
                ['My Loyalty', 9.3],
                ['My Wallet', 9.0],
                ['My Store', 8.9],
				['Help', 8.9]
                
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                x: 4,
                y: 10,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
        }]
    });
	/* Pass Sent     ****************************************/
	var c1_pass = new Array();
	var c1_pass = $('#actionPassessent').val().split('-');
	for (a in c1_pass ) {
		c1_pass[a] = parseInt(c1_pass[a], 10); // Explicitly include base as per Álvaro's comment
	}
	     $('#c1_pass').highcharts({
                                    chart: {
                                        zoomType: 'x'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    subtitle: {
                                        text: document.ontouchstart === undefined ?
                                                'Click and drag in the plot area to zoom in' :
                                                'Pinch the chart to zoom in'
                                    },
                                    xAxis: {
                                        type: 'datetime',
                                        minRange: 7 * 24 * 3600000 // fourteen days
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Passes sent per day'
                                        }
                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    plotOptions: {
                                        area: {
                                            fillColor: {
                                                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                                                stops: [
                                                    [0, Highcharts.getOptions().colors[0]],
                                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                                ]
                                            },
                                            marker: {
                                                radius: 2
                                            },
                                            lineWidth: 1,
                                            states: {
                                                hover: {
                                                    lineWidth: 1
                                                }
                                            },
                                            threshold: null
                                        }
                                    },

                                    series: [{
                                        type: 'area',
                                        name: 'Passes Sent',
                                        pointInterval: 24 * 3600 * 1000,
                                        pointStart: Date.UTC(currentYear, 0, day-6),
                                        data: [c1_pass[6],c1_pass[5],c1_pass[4],c1_pass[3],c1_pass[2],c1_pass[1],c1_pass[0]]
                                    }]
                                });


	/* Pass Sent     ****************************************/

		  $('#cc1_pass1').highcharts({
                                    chart: {
                                        zoomType: 'x'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    subtitle: {
                                        text: document.ontouchstart === undefined ?
                                                'Click and drag in the plot area to zoom in' :
                                                'Pinch the chart to zoom in'
                                    },
                                    xAxis: {
                                        type: 'datetime',
                                        minRange: 7 * 24 * 3600000 // fourteen days
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Passes Redeemed Per Day'
                                        }
                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    plotOptions: {
                                        area: {
                                            fillColor: {
                                                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                                                stops: [
                                                    [0, Highcharts.getOptions().colors[0]],
                                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                                ]
                                            },
                                            marker: {
                                                radius: 2
                                            },
                                            lineWidth: 1,
                                            states: {
                                                hover: {
                                                    lineWidth: 1
                                                }
                                            },
                                            threshold: null
                                        }
                                    },

                                    series: [{
                                        type: 'area',
                                        name: 'Passes Redeemed',
                                        pointInterval: 24 * 3600 * 1000,
                                        pointStart: Date.UTC(currentYear, 0, day-6),
                                        data:[0,0,0,50,100,152,245 ]
                                    }]
                                });

/*   Top Campaigns  ******************************************/


    // Make monochrome colors and set them as default for all pies
    Highcharts.getOptions().plotOptions.pie.colors = (function () {
        var colors = [],
            base = Highcharts.getOptions().colors[0],
            i;

        for (i = 0; i < 10; i += 1) {
            // Start out with a darkened base color (negative brighten), and end
            // up with a much brighter color
            colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
        }
        return colors;
    }());

    // Build the chart
    $('#cc6').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: ''
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            data: [
                
                                                ['CocaCola',   45.0],
                            ['Adidas',       26.8],
                            {
                                name: 'Reebok',
                                y: 12.8,
                                sliced: true,
                                selected: true
                            },
                            ['Giant',    8.5],
                            ['Puma',     6.2],
                            ['Guess',   0.7]
                        
                                        ]   
        }]
    });
});
