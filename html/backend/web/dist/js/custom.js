

/* auto file full name */
$('#usermanagement-firstname').keyup(function() {
	
    var firstname = $(this).val();
  var lastname  = $('#usermanagement-lastname').val();
	$('#usermanagement-fullname').val(firstname+' '+lastname);
   });
     
  $('#usermanagement-lastname').keyup(function() {
    
    var firstname = $('#usermanagement-firstname').val();
    var lastname = $(this).val();
    
    $('#usermanagement-fullname').val(firstname+' '+lastname);
});


/* Workorder start date and end date */
 
$('#workorders-dateto').change(function() { 
	
	
	var oneDay = 24*60*60*1000;
	
	var firstDate =  new Date($('#workorders-datefrom').val());		
	var secondDate	 =  new Date($('#workorders-dateto').val());	
	var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
if(diffDays!='' || diffDays==0 ){
	
 $('#workorders-duration').val(diffDays+1);	
}


	
});
	
        


/* delete data via ajax */

function DeleteAction(id)
{

 var url =  $('#url').val();
 

 
var delconfirm = confirm ("Are you sure to delete this item?");
	
	
	if (delconfirm) {		
		$.ajax({
		method: "POST",
		url: url+'byajax',
		data: { id: id }	
		})
	  .done(function( result ) {		
	   $( "#"+id ).fadeOut( "slow");
	 
	  });

}

}



/* work order model show hide partners */
/* first hide all partner fields*/
	 var Countryval  =  $('#workorders-country_name').val();	
	  if(Countryval != '' ){
			$('#workorders-partner_ma').show();
			$('#workorders-partner_ad').show();
			$('#workorders-partner_re').show();
			$('#select-a-partner').show();
	 		  
		  	}else{
			$('#workorders-partner_ma').hide();
			$('#workorders-partner_ad').hide();
			$('#workorders-partner_re').hide();
			$('#select-a-partner').hide();
		}	

$('#workorders-country_name').change(function(){
	
 var Countryval  =  $('#workorders-country_name').val();
 
 if(Countryval != '' ){
		$('#workorders-partner_ma').show();
		$('#workorders-partner_ad').show();
	    $('#workorders-partner_re').show();
	    $('#select-a-partner').show();
	 
	}else{
		
			$('#workorders-partner_ma').hide();
			$('#workorders-partner_ad').hide();
			$('#workorders-partner_re').hide();
			$('#select-a-partner').hide();
		
	}

 
 });
 
	
 $( "#createurl" ).click(function() {
 
  $('#ss_passes').toggle(); 
   
     
	});
	
/*	 $( "#createpassurl" ).click(function() {
     
		$('#makeurl').show(); 
  
		$('#makeurl').attr('src','https://create.purplecowtv.com');
		
		$('#createpassurl').hide(); 
		$('#closebutton').show(); 
     
	}); */
	
	
	 $( "#closebutton" ).click(function() {
     $('#makeurl').hide();
     $('#closebutton').hide();
     $('#createpassurl').show();
     
}); 
	
  $( "#showoffer" ).click(function() {
     $('#makeurl').hide();
     $('#showoffer').hide();
     $('#showofferform').show();
     
});

  $( "#showoffers" ).click(function() {
     $('#makeurl').hide();
     $('#showoffer').hide();
     $('#showoffers').hide();     
     $('#showofferform').show();
     
});


function allSelection(){
		  		
      var length = document.getElementById("loselect").options.length;

      for(var i = 1;i<length;i++)
        document.getElementById("loselect").options[i].selected = "selected";
        document.getElementById("loselect").options[0].selected = "";     

  }
  
   function allAgegroup(){
		  		
      var length = document.getElementById("allselectage").options.length;

      for(var i = 1;i<length;i++)
        document.getElementById("allselectage").options[i].selected = "selected";
        document.getElementById("allselectage").options[0].selected = "";     

  }
  
   function alluserstype(){
		  		
      var length = document.getElementById("allselectuser").options.length;

      for(var i = 1;i<length;i++)
        document.getElementById("allselectuser").options[i].selected = "selected";
        document.getElementById("allselectuser").options[0].selected = "";     

  }


$(document).ready(function () {
	
	var checkvalues = $('input[name="Workorders[temperature]"]:checked').val();
	if(checkvalues==2){
	 $('#show-temp').show();
	}else{
	$('#show-temp').hide();
	}
	
	$("input[name='Workorders[temperature]']").on("change", function () {	  
	var values = this.value;
   
		if (values == "2"){ 
			 $('#show-temp').show();
		}else {
			$('#show-temp').hide();
		}	
	});
	
});


$(document).ready(function () {
	var checkoffertext1 = $('input:radio[name="Workorderpopup[offertext1]"]:checked').val();
	if(!checkoffertext1){	
	$('.selectrow select').prop('disabled', 'disabled');	
	$("input:radio[name='Workorderpopup[offertext1]']").on("change", function () {
		$('.help-block2').empty();		 
		$('.selectrow select').prop('selectedIndex',0); 
		$('.selectrow select').prop('disabled', 'disabled');
		var rowid = $(this).val();
		$('#row'+rowid+' select').prop('disabled',false);
		if(rowid==1 || rowid==3 || rowid==4 || rowid==5){
			$('#row'+rowid+' div:nth-child(2) select').prop('required',true);
			$('#row'+rowid+' div:nth-child(4) select').prop('required',true);
		} else if(rowid==2) {
			$('#row'+rowid+' div:nth-child(2) select').prop('required',true);
		}
		var offertext1 = this.value;	
		$("input:hidden[name='Workorderpopup[offertext1]']").val(offertext1);
	});
	} else {		
		$('.selectrow select').prop('disabled', 'disabled');		
		var rowid = checkoffertext1;
		$('#row'+rowid+' select').prop('disabled',false);
		if(rowid==1 || rowid==3 || rowid==4 || rowid==5){
			$('#row'+rowid+' div:nth-child(2) select').prop('required',true);
			$('#row'+rowid+' div:nth-child(4) select').prop('required',true);
		}else if(rowid==2){
			$('#row'+rowid+' div:nth-child(2) select').prop('required',true);
		}
		for(var i = 1;i<=5;i++){
		if(rowid != i){
			$('#row'+i+' select').prop('selectedIndex',0); 
		}			
		$("input:radio[name='Workorderpopup[offertext1]']").on("change", function () {	 

		$('.selectrow select').prop('selectedIndex',0); 
		$('.selectrow select').prop('disabled', 'disabled');
		var rowid = $(this).val();					
		$('#row'+rowid+' select').prop('disabled',false);
		if(rowid==1 || rowid==3 || rowid==4 || rowid==5){
			$('#row'+rowid+' div:nth-child(2) select').prop('required',true);
			$('#row'+rowid+' div:nth-child(4) select').prop('required',true);
		}else if(rowid==2){
			$('#row'+rowid+' div:nth-child(2) select').prop('required',true);
		}
		$("input:hidden[name='Workorderpopup[offertext1]']").val(rowid);		
		});
		}
	}
	$('#update-offer-data').click(function(){
		var checkoffertext1 = $('input:radio[name="Workorderpopup[offertext1]"]:checked').val();
		$("input:hidden[name='Workorderpopup[offertext1]']").val(checkoffertext1);
	});
});
	
		
/* end deal text*/

/* beacon pop up offer start  */



$(document).ready(function () {
	
	
	var checkvalues = $('input[name="Workorderpopup[offertype]"]:checked').val();
	
	if(checkvalues==3){
			document.getElementById("custom-offer-text").style.display = "block";
			document.getElementById("offer-mumaric").style.display = "none";		
	}
	
	if(checkvalues==1){
			document.getElementById("offer-mumaric").style.display = "block";
			document.getElementById("custom-offer-text").style.display = "none";	
	}
	
	
	$("input[name='Workorderpopup[offertype]']").on("change", function () {	 
		
	var values = this.value;
	$('.help-block2').empty();	
		if (values == 1){ 
			document.getElementById("offer-mumaric").style.display = "block";	
				$('#custom-offer-text select').prop('required',false);
				$('input[name="Workorderpopup[offertext1]"]').val(['']);
		    }else{
			 document.getElementById("offer-mumaric").style.display = "none";	
			}
			
			if(values == 3) {
				document.getElementById("custom-offer-text").style.display = "block";	
				document.getElementById("offer-mumaric").style.display = "none";	
		    }else{
				document.getElementById("custom-offer-text").style.display = "none";	
			}	
	});
	
}); 

/* beacon pop up offer end */



$(document).ready(function () {
	
	var checkvalues = $('input[name="Workorders[dayparting]"]:checked').val();
	
	if(checkvalues==3){
	document.getElementById("show-dayparting ").style.display = "block";	
	
	}else{
	
			//document.getElementById("show-dayparting ").style.display = "none";
	
	}
	
	$("input[name='Workorders[dayparting]']").on("change", function () {	  
	var values = this.value;
 
		if (values == "3"){ 
			document.getElementById("show-dayparting ").style.display = "block";	
		}else {
			document.getElementById("show-dayparting ").style.display = "none";	
		}	
	});
	
});


/* change label of workorder type */

$(document).ready(function () {	
	
	
	  var checkuploadtypes = $('input:radio[name="Workorderpopup[uploadTypes]"]:checked').val();
	  
   if(checkuploadtypes){
	  
	  if(checkuploadtypes == 1){
			document.getElementById("image-block").style.display = "block";
			//document.getElementById("video-block").style.display = "none";
			if(document.getElementById('video-block-types')) {
			document.getElementById("video-block-types").style.display = "none";	
		 }
				
						
		}			
		if(checkuploadtypes == 2){
			document.getElementById("image-block").style.display = "none";
			if(document.getElementById('video-block-types')) {
				document.getElementById("video-block-types").style.display = "block";
			}
			var checkvideoTypes = $('input:radio[name="Workorderpopup[videoTypes]"]:checked').val();
			
			if(checkvideoTypes==1)	{
			document.getElementById("video-block-types-system").style.display = "block";
			document.getElementById("video-block-types-youtube").style.display = "none";
			$('#workorderpopup-videokey').val(' ');	
			}
			if(checkvideoTypes==2)	{
			document.getElementById("video-block-types-system").style.display = "none";
			document.getElementById("video-block-types-youtube").style.display = "block";	
			}
		} 
	 
	 }
	
	
	
	$("input:radio[name='Workorderpopup[uploadTypes]']").on("change", function () {
		var uploadtypes  = $(this).val();
		if(uploadtypes == 1){
			document.getElementById("image-block").style.display = "block";
			if(document.getElementById("video-block-types"))
			{
				document.getElementById("video-block-types").style.display = "none";	
			}
			document.getElementById("video-block-types-system").style.display = "none";	
			document.getElementById("video-block-types-youtube").style.display = "none";				
		}			
		if(uploadtypes == 2){
			document.getElementById("image-block").style.display = "none";
			if(document.getElementById("video-block-types"))
			{
				document.getElementById("video-block-types").style.display = "block";
			}
			var checkvideoTypes = $('input:radio[name="Workorderpopup[videoTypes]"]:checked').val();
			if(checkvideoTypes==1)	{
			if(document.getElementById("video-block-types"))
			{
				document.getElementById("video-block-types").style.display = "block";
			}
			document.getElementById("video-block-types-system").style.display = "block";
			document.getElementById("video-block-types-youtube").style.display = "none";	
			}
			if(checkvideoTypes==2)	{
				if(document.getElementById("video-block-types"))
			{
				document.getElementById("video-block-types").style.display = "block";
			}
			document.getElementById("video-block-types-system").style.display = "none";
			document.getElementById("video-block-types-youtube").style.display = "block";	
			}	
			//document.getElementById("video-block-types-system").style.display = "block";	
			//document.getElementById("video-block-types-youtube").style.display = "block";			
		}
		
	});	
	$("input:radio[name='Workorderpopup[videoTypes]']").on("change", function () {
		var uploadtypes  = $(this).val();
		if(uploadtypes == 1){
			document.getElementById("video-block-types-youtube").style.display = "none";
			document.getElementById("video-block-types-system").style.display = "block";			
		}			
		if(uploadtypes == 2){
			document.getElementById("video-block-types-youtube").style.display = "block";
			document.getElementById("video-block-types-system").style.display = "none";		
		}
		
	});	
	/***************** deal section update file********************/
	 var checkdealuploadTypes = $('input:radio[name="Deal[dealuploadTypes]"]:checked').val();
	
	   if(checkdealuploadTypes){
		  if(checkdealuploadTypes == 1){
				document.getElementById("deal-video-block-types").style.display = "none";
			    document.getElementById("deal-image-block").style.display = "block";		
				}			
			if(checkdealuploadTypes == 2){
			
				document.getElementById("deal-video-block-types").style.display = "block";
				document.getElementById("deal-image-block").style.display = "none";		
				
				var checkvideoTypes = $('input:radio[name="Deal[dealvideoTypes]"]:checked').val();
				if(checkvideoTypes==1)	{
				document.getElementById("deal-video-block-types-youtube").style.display = "none";
			    document.getElementById("deal-video-block-types-system").style.display = "block";	
				//$('#workorderpopup-videokey').val(' ');	
				}
				if(checkvideoTypes==2)	{
				document.getElementById("deal-video-block-types-youtube").style.display = "block";
				document.getElementById("deal-video-block-types-system").style.display = "none";		
				}
			} 
	 
	 }
	/*************deal section on create file*************/
	 $("input:radio[name='Deal[dealuploadTypes]']").on("change", function () {
		
		 var dealuploadTypes  = $(this).val();
		 if(dealuploadTypes == 1){
			document.getElementById("deal-video-block-types").style.display = "none";
			//document.getElementById("deal-dealvideokey").style.display = "none";
			document.getElementById("deal-image-block").style.display = "block";
			document.getElementById("deal-video-block-types-system").style.display = "none";
			document.getElementById("deal-video-block-types-youtube").style.display = "none";
						
		}			
		if(dealuploadTypes == 2){
			document.getElementById("deal-video-block-types").style.display = "block";
				document.getElementById("deal-image-block").style.display = "none";		
				
				var checkvideoTypes = $('input:radio[name="Deal[dealvideoTypes]"]:checked').val();
				if(checkvideoTypes==1)	{
				document.getElementById("deal-video-block-types-youtube").style.display = "none";
			    document.getElementById("deal-video-block-types-system").style.display = "block";	
				//$('#workorderpopup-videokey').val(' ');	
				}
				if(checkvideoTypes==2)	{
				document.getElementById("deal-video-block-types-youtube").style.display = "block";
				document.getElementById("deal-video-block-types-system").style.display = "none";		
				}		
		}
		 
	});	 
	
		$("input:radio[name='Deal[dealvideoTypes]']").on("change", function () {
		var dealvideoTypes  = $(this).val();
		if(dealvideoTypes == 1){
			document.getElementById("deal-video-block-types-youtube").style.display = "none";
			document.getElementById("deal-video-block-types-system").style.display = "block";			
		}			
		if(dealvideoTypes == 2){
			document.getElementById("deal-video-block-types-youtube").style.display = "block";
			document.getElementById("deal-video-block-types-system").style.display = "none";		
		}
		
	});
	var psatype = document.getElementById("workorderpopup-psaoffertype");			
		if(psatype) {
				var psatypeid = psatype.options[psatype.selectedIndex].value;
				var data1 = $('.form-group.field-workorderpopup-offertitle >label').text();		
				var data2 = $('.form-group.field-workorderpopup-offertext >label').text();		
				if(psatypeid==1){				
				$('.form-group.field-workorderpopup-offertitle >label').text('PSA (info) '+data1);	
				$('.form-group.field-workorderpopup-offertext >label').text('PSA (info) '+data2);					
				}
				if(psatypeid==2){
				$('.form-group.field-workorderpopup-offertitle >label').text('PSA (deal) '+data1);	
				$('.form-group.field-workorderpopup-offertext >label').text('PSA (deal) Text (100 characters max)');	
				}		
			}
				
	$('#workorderpopup-psaoffertype').on("change", function () {
		var values = this.value;
		var data1 = $('.form-group.field-workorderpopup-offertitle >label').text();		
		var data2 = $('.form-group.field-workorderpopup-offertext >label').text();		
		if(values == 1){
				var data1 = data1.replace('PSA (deal)','');
				var data2 = data2.replace('PSA (deal) Text (100 characters max)','');
				
				$('.form-group.field-workorderpopup-offertitle >label').text('PSA (info) '+data1);	
				$('.form-group.field-workorderpopup-offertext >label').text('PSA (info) Text (220 characters max)');	
		}
		if(values == 2){
				
				var data1 = data1.replace('PSA (info)','');
				var data2 = data2.replace('PSA (info)','');
								
				$('.form-group.field-workorderpopup-offertitle >label').text('PSA (deal) '+data1);	
				$('.form-group.field-workorderpopup-offertext >label').text('PSA (deal) Text (100 characters max)');	
		}		
	});
});



/* changes partner color */

var backcolor = $('#profile-backgroundhexcolour').val();

if(backcolor!=''){
	if(backcolor == '#000000') {
	$('#profile-backgroundhexcolour').css('background',backcolor);
	$('#profile-backgroundhexcolour').css('color','#ffffff');
	}else{
		$('#profile-backgroundhexcolour').css('background',backcolor);
		$('#profile-backgroundhexcolour').css('color','#000000');
	}	

}

var textcolor = $('#profile-foregroundhexcolour').val();

if(textcolor!=''){
	if(textcolor == '#ffffff') {
	$('#profile-foregroundhexcolour').css('background',textcolor);
	$('#profile-foregroundhexcolour').css('color','#000000');
	}else{
		$('#profile-foregroundhexcolour').css('background',textcolor);
		$('#profile-foregroundhexcolour').css('color','#ffffff');
	}	

}
 
$('#profile-backgroundhexcolour').keyup(function() {
	
	 var background = $(this).val();
	if(background == '#000000') {
	$('#profile-backgroundhexcolour').css('background',background);
	$('#profile-backgroundhexcolour').css('color','#ffffff');
	}else{
		$('#profile-backgroundhexcolour').css('background',background);
		$('#profile-backgroundhexcolour').css('color','#000000');
	}
 
 });
 
 $('#profile-foregroundhexcolour').keyup(function() {
	
	 var fontColor = $(this).val();
	if(fontColor == '#ffffff') {
	$('#profile-foregroundhexcolour').css('background',fontColor);
	$('#profile-foregroundhexcolour').css('color','#000000');
	}else{
		$('#profile-foregroundhexcolour').css('background',fontColor);
		//$('#profile-foregroundhexcolour').css('color','#ffffff');
	}
 
 });

// To accept only numeric numbers
$(".numericInput").keydown(function(event) {
    // Allow only backspace and delete
    if ( event.keyCode == 46 || event.keyCode == 8 ) {
        // let it happen, don't do anything
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.keyCode < 48 || event.keyCode > 57 ) {
            event.preventDefault();
        }
    }
});

	// check upload type image/video 