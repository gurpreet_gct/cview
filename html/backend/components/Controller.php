<?php 
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
namespace backend\components;
class Controller extends \yii\web\Controller {
    public function beforeAction($event)
    {      
        return parent::beforeAction($event);
    }
}
