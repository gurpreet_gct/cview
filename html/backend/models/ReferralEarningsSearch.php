<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ReferralEarnings;

/**
 * ReferralEarningsSearch represents the model behind the search form about `common\models\ReferralEarnings`.
 */
class ReferralEarningsSearch extends ReferralEarnings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'refer_by', 'commission_percentage', 'order_id', 'status', 'created_at', 'created_by'], 'integer'],
            [['commission_amount', 'order_total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		 // add conditions that should always apply here
		 
      
		$usertype = Yii::$app->user->identity->user_type;
        $userId = $usertype==\common\models\User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
       
        $query = ReferralEarnings::find();       
       
        if($usertype!=\common\models\User::ROLE_ADMIN){
            $query->where(['refer_by'=>$userId]);
        }		
       

        $dataProvider = new ActiveDataProvider([
            'query' => $query->orderBy('id desc'),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'refer_by' => $this->refer_by,
            'commission_amount' => $this->commission_amount,
            'commission_percentage' => $this->commission_percentage,
            'order_id' => $this->order_id,
            'order_total' => $this->order_total,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        return $dataProvider;
    }
}
