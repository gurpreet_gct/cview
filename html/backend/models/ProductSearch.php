<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','p_type', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],            
            [['store_id','name','sku','status'],'safe']
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Product Name'),
            'sku' => Yii::t('app','Sku'),
            'image' => Yii::t('app','Product Image'),
            'pimage' => Yii::t('app','Product Image'),
            'description' => Yii::t('app','Description'),
            'urlkey' => Yii::t('app','Url key'),
            'price' => Yii::t('app','Price'),
            'is_online' => Yii::t('app','Is online'),
            'store_id' => Yii::t('app','Store/Brand Name'),
            'status' => Yii::t('app','Status'),
            'p_type' => Yii::t('app','Product Type'),
            'created_at'=>Yii::t('app','Created At'),
            'updated_at'=>Yii::t('app','Updated At'),
            'created_by'=>Yii::t('app','Created By'),
            'updated_by'=>Yii::t('app','Updated By'), 
            
            
        ];
    }
    
    
    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		$usertype = Yii::$app->user->identity->user_type;
        $userId = $usertype==7?Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
        if($usertype==1){
			$query = Product::find();
		}else{
			$query = Product::find()->where(['store_id' => $userId]);
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'status' => $this->status,
            'is_online' => $this->is_online,
            'store_id' => $this->store_id,
            'p_type' => $this->p_type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
               ->andFilterWhere(['like', 'sku', $this->sku])
               ->andFilterWhere(['like', 'description', $this->description])
               ->andFilterWhere(['like', 'urlkey', $this->urlkey]);

        return $dataProvider;
    }
    
     public function searchFoodProduct($params)
    {
		$usertype = Yii::$app->user->identity->user_type;
        $userId = $usertype==7?Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
        if($usertype==1){
			$query = Product::find()->where('p_type IN(2,3)')->orderby('id desc');
		}else{
		$query = Product::find()->where(['store_id' => $userId])
								->andWhere('p_type IN(2,3)')->orderby('id desc');
		}

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category' => $this->category,
            'price' => $this->price,
            'status' => $this->status,
            'p_type' => $this->p_type,
            'store_id' => $this->store_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

   $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'sku', $this->sku])          
            ->andFilterWhere(['like', 'urlkey', $this->urlkey])
            ->andFilterWhere(['like', 'related_product', $this->related_product]);

        return $dataProvider;
    }
}
