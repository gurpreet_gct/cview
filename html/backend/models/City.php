<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

#use common\models\User;
#use common\models\Country;

/**
 * This is the model class for table "tbl_city".

 */
 
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_city';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
        ];
    }
    
    
   
}
