<?php

namespace backend\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;
use yii\db\Query;
use common\models\User;
use backend\models\OrderStatus;
use common\models\OrderItem;

/**
 * RegionsSearch represents the model behind the search form about `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
      public static function tableName()
    {
        return '{{%orders}}';
    }
    public function rules()
    {
        return [
            
            [['user_id'], 'safe'],
             [['id'], 'integer'],
             [['grand_total'], 'integer'],
              [['status'], 'safe'],
              [['created_at'], 'safe'],
          	];
    }
    
    
    
    public function attributeLabels()
    {
        return [
           'id' => Yii::t('app','#Order ID'),
        	'user_id' => Yii::t('app','User Name'),
        	'created_at' => Yii::t('app','Date'),
        	'grand_total' => Yii::t('app','Grand Total'),
        	'status' => Yii::t('app','Status'),
        	'shipping_description' => Yii::t('app','Shipping Description'),
        	'shipper_id' => Yii::t('app','Shipper Id'),
        	'is_offline' => Yii::t('app','Is Offline'),
        	'discount_amount' => Yii::t('app','Discount Amount'),
        	'grand_total' => Yii::t('app','Grand Total'),
        	'shipping_amount' => Yii::t('app','Shipping Amount'),
        	'shipping_tax_amount' => Yii::t('app','Shipping Tax Amount'),
        	'subtotal' => Yii::t('app','Subtotal'),
        	'tax_amount' => Yii::t('app','Tax Amount'),
        	'total_offline_refunded' => Yii::t('app','Total Offline Refunded'),
        	'total_online_refunded' => Yii::t('app','Total Online Refunded'),
        	'total_paid' => Yii::t('app','Total Paid'),
        	'qty_ordered' => Yii::t('app','Quantity Ordered'),
        	'total_refunded' => Yii::t('app','Total Refunded'),
        	'can_ship_partially' => Yii::t('app','Can Ship Partially'),
        	'customer_note_notify' => Yii::t('app','Customer Note Notify'),
        	'billing_address_id' => Yii::t('app','Billing Address Id'),
        	'billing_address' => Yii::t('app','Billing Address'),
        	'shipping_address_id' => Yii::t('app','Shipping Address Id'),
        	'shipping_address' => Yii::t('app','Shipping Address'),
        	'forced_shipment_with_invoice' => Yii::t('app','Forced Shipment With Invoice'),
        	'weight' => Yii::t('app','Weight'),
        	'email_sent' => Yii::t('app','Email Sent'),
        	'edit_increment' => Yii::t('app','Edit Increment'),
        	'created_at' => Yii::t('app','created At'),
			'updated_at' => Yii::t('app','updated At'),
			'created_by' => Yii::t('app','created By'),
			'updated_by' => Yii::t('app','updated By'),
        ];
    }
    
    
    
    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params){
	  
	  	
	  	 $usertype = Yii::$app->user->identity->user_type;
         $userId = $usertype == User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
       
         if($usertype==User::ROLE_ADMIN){
			 
			$query = Orders::find();
			
		}else{
				$query = Orders::find()
					->joinWith('orderitemp',false,'INNER JOIN')
					->where(OrderItem::tableName().'.store_id='. $userId);
		}
	  	
	  	$query->orderBy('id DESC');
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
		if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		if($this->user_id!=''){
			//$newvalue =  User::find()->select('id')->where(['username' => $this->user_id])->one();
			$new_user_id =  User::find()->select('id')->where(['LIKE', 'username', $this->user_id])->all();
			$result_arr = array();
		    foreach($new_user_id as $key=>$new_user_ids){
				$result_arr[] = $new_user_ids->id;
			}
			$idS = implode(',', $result_arr);
			if($idS!=''){
			 $idnewS = $idS;
		    }else{
				 $idnewS = 0;
			}
		    $query->andWhere('user_id IN('.$idnewS.')')->all(); 
			}
			
			$timestamp = strtotime($this->created_at);
			if($this->created_at!=''){
			$datanewtime = explode(' - ',$this->created_at);
			$start_date = strtotime($datanewtime[0]);
			$end_date = strtotime($datanewtime[1]);
			$end_date =  $end_date+86400;
			if($start_date!=''){
				$query->andFilterWhere(['and', "created_at>=$start_date", "created_at<$end_date"]);
			 }
		 }
			//echo $this->created_at;
			
			$query->andFilterWhere([
						'id' => $this->id,
						
				]);
				$query->andFilterWhere([
						'status' => $this->status,
						
				]);
				
			$query->andFilterWhere(['like', 'grand_total', $this->grand_total]);
           return $dataProvider;
    }
 public function CancelOrderSearch($params){
	  	$query = Orders::find()->where('status=5')->orderBy('id DESC');
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
		if (!$this->validate()) {           
            return $dataProvider;
        }
		if($this->user_id!=''){			
			$new_user_id =  User::find()->select('id')
							->where(['LIKE', 'username', $this->user_id])->all();
			$result_arr = array();
		    foreach($new_user_id as $key=>$new_user_ids){
				$result_arr[] = $new_user_ids->id;
			}
			$idS = implode(',', $result_arr);
			if($idS!=''){
			 $idnewS = $idS;
		    }else{
				 $idnewS = 0;
			}
		    $query->andWhere('user_id IN('.$idnewS.')')->all(); 
			}
			
			$timestamp = strtotime($this->created_at);
			if($this->created_at!=''){
			$datanewtime = explode(' - ',$this->created_at);
			$start_date = strtotime($datanewtime[0]);
			$end_date = strtotime($datanewtime[1]);
			$end_date =  $end_date+86400;
			if($start_date!=''){
				$query->andFilterWhere(['and', "created_at>=$start_date", "created_at<$end_date"]);
			 }
		 }
			//echo $this->created_at;
			
			$query->andFilterWhere([
						'id' => $this->id,
						
				]);
				$query->andFilterWhere([
						'status' => $this->status,
						
				]);
				
			$query->andFilterWhere(['like', 'grand_total', $this->grand_total]);
           return $dataProvider;
    }
}

