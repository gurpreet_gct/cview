<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Usermanagement;

/**
 * UserSearch represents the model behind the search form about `common\Models\User`.
 */
class UsersSearch extends Usermanagement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'role_id', 'walletPinCode', 'phone', 'status','advertister_id'], 'integer'],
            [['firstName', 'lastName','dob', 'fullName', 'email', 'orgName','username', 'password_hash', 'password_reset_token', 'image', 'sex', 'auth_key', 'activationKey', 'confirmationKey', 'access_token', 'hashKey','user_type', 'lastLogin'], 'safe'],
        ];
    }
    
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last Name'),
			'fullName' => Yii::t('app','Full Name'),
			'orgName' => Yii::t('app','Org Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb Identifier'),
			'role_id' => Yii::t('app','Role Id'),
			'user_type' => Yii::t('app','User Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New Password'),
			'password_reset_token' => Yii::t('app','Password Reset Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date Of Birth'),
			'ageGroup_id' => Yii::t('app','Age Group Id'),
			'walletPinCode' => Yii::t('app','Wallet Pin Code'),
			'loyaltyPin' => Yii::t('app','Loyalty Pin'),
			'auth_key' => Yii::t('app','Auth Key'),
			'activationKey' => Yii::t('app','Activation Key'),
			'confirmationKey' => Yii::t('app','Confirmation Key'),
			'access_token' => Yii::t('app','Access Token'),
			'hashKey' => Yii::t('app','Hash Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device Token'),
			'lastLogin' => Yii::t('app','Last Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created At'),
            'updated_at'=>Yii::t('app','Updated At'),
            
        ];
    }
    
    
    

    /**
     * @inheritdoc
  
     */

  public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }



    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchadmin($params)
    {
        $query = Usermanagement::find()->where(['user_type' => 1]);
        
       

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'user_type' => $this->user_type,
            'phone' => $this->phone,
            'walletPinCode' => $this->walletPinCode,
            'status' => $this->status,            
                
        ]);
        $query->andFilterWhere(["!=","id",Yii::$app->user->id]);
        $query->andFilterWhere(['like', 'firstName', $this->firstName])
            ->andFilterWhere(['like', 'lastName', $this->lastName])
            ->andFilterWhere(['like', 'fullName', $this->fullName])
            ->andFilterWhere(['like', 'dob', $this->dob])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'orgName', $this->orgName]);
            

        return $dataProvider;
    }
    public function searchcustomer($params)
    {
        $query = Usermanagement::find()->where(['user_type' => 3]);
        					
        
       

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'user_type' => $this->user_type,
            'walletPinCode' => $this->walletPinCode,
            'status' => $this->status,
                
        ]);

        $query->andFilterWhere(['like', 'firstName', $this->firstName])
            ->andFilterWhere(['like', 'lastName', $this->lastName])
            ->andFilterWhere(['like', 'fullName', $this->fullName])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'orgName', $this->orgName]);
            

        return $dataProvider;
    }
    
    public function searchemployee($params)
    {
		$usertype = Yii::$app->user->identity->user_type;
		$userId = $usertype == 7 ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
		if($usertype == 1){
			$query = Usermanagement::find()->where(['user_type' => 9]);      
		} else {
			$query = Usermanagement::find()->where(['user_type' => 9,'advertister_id'=>$userId]);  
		}

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'user_type' => $this->user_type,
            'walletPinCode' => $this->walletPinCode,
            'status' => $this->status,
            'advertister_id' => $this->advertister_id,
                
        ]);

        $query->andFilterWhere(['like', 'firstName', $this->firstName])
            ->andFilterWhere(['like', 'lastName', $this->lastName])
            ->andFilterWhere(['like', 'fullName', $this->fullName])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'orgName', $this->orgName]);
            

        return $dataProvider;
    }
public function search($params)
    {
        $query = Usermanagement::find()->where(['user_type' => 7]);
        
       

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'user_type' => $this->user_type,           
            'walletPinCode' => $this->walletPinCode,
            'status' => $this->status,
                
        ]);

        $query->andFilterWhere(['like', 'firstName', $this->firstName])
            ->andFilterWhere(['like', 'lastName', $this->lastName])
            ->andFilterWhere(['like', 'fullName', $this->fullName])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'orgName', $this->orgName]);
            

        return $dataProvider;
    }
}
