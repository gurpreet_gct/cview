<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Storebeacons;

/**
 * StorebeaconsSearch represents the model behind the search form about `backend\models\Storebeacons`.
 */
class StorebeaconsSearch extends Storebeacons
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_id', 'beacon_id', 'status'], 'integer'],
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'store_id' => Yii::t('app','Store'),
            'beacon_id' => Yii::t('app','Beacon Devices'),
            'status' => Yii::t('app','Status'),
        ];
    }
    
    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Storebeacons::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'store_id' => $this->store_id,
            'beacon_id' => $this->beacon_id,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
