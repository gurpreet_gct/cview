<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use common\models\Country;
use common\models\States;


/**
 * This is the model class for table "tbl_beacon_group".
 *
 * @property integer $id
 * @property string $group_name
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Beacongroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_beacon_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
				[['group_name', 'status','type'], 'required'],    
				# ['group_name','unique'],        
			[['city','grandparent_id','parent_id'],'safe'],        
            [['group_name'], 'string', 'max' => 100],
            ['group_name', 'unique', 'targetAttribute' => ['group_name', 'grandparent_id' => 'grandparent_id'],'when'=>function($model){				
				if($model->type==2)				
					return true;
			},
			'whenClient' => "function (attribute, value) {                    
               
                   var wtval= $('#beacongroup-type').val(); 
                                              
                 if (wtval==2)  {                
                   return true; }
                   else {
                   return false; }
               
            }"    
			],
			
			['group_name', 'unique', 'targetAttribute' => ['group_name', 'parent_id' => 'parent_id'],'when'=>function($model){
				
				if($model->type==3)				
					return true;
			},
			'whenClient' => "function (attribute, value) {                    
               
                   var wtval= $('#beacongroup-type').val(); 
                                              
                 if (wtval==3)  {                
                   return true; }
                   else {
                   return false; }
               
            }"    
			],
			
		  [['group_name'], 'unique', 'when' => function($model) {
				if($model->type == 0 || $model->type == 1) { return true;}               
           },    
           'whenClient' => "function (attribute, value) {                    
               
                   var wtval= $('#beacongroup-type').val(); 
                                              
                 if (wtval==0 || wtval==1 )  {                
                   return true; }
                   else {
                   return false; }
               
            }"    
   
           ],
           
            [['grandparent_id'], 'required', 'when' => function($model) {
				if($model->type == 2) { return true;}               
           },    
           'whenClient' => "function (attribute, value) {                    
               
                   var wtval= $('#beacongroup-type').val(); 
                                              
                 if (wtval==2) {                
                   return true; }
                   else {
                   return false; }
               
            }"    
   
           ],
           
               [['grandparent_id'], 'required', 'when' => function($model) {
				if($model->type == 3) { return true;}               
           },    
           'whenClient' => "function (attribute, value) {                    
               
                   var wtval= $('#beacongroup-type').val(); 
                                              
                 if (wtval==3) {                
                   return true; }
                   else {
                   return false; }
               
            }"    
   
           ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'group_name' => Yii::t('app','Group Name'),
            'city' => Yii::t('app','City'),
            'type' => Yii::t('app','Level'),
            'grandparent_id' => Yii::t('app','Country'),
            'parent_id' => Yii::t('app','State'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_by' => Yii::t('app','Updated By'),
        ];
    }
    
    public function getStatustype()
    {
    	  if($this->status==1){
         return 'Enable';    	  
    	  }else {
    	  return 'Disable';
       }
    }
    
     public function getLevel()
    {
				 $level = array("0"=>"General","1"=>"Country","2"=>"State","3"=>"City","4"=>"Food Order Checkout");
			 if(isset($this->type)){
        				return $level[$this->type];
					}
    }
    
    
    public function getCreateduser()
    {
    	  return $this->hasOne(User::className(), ['id' => 'created_by']);
       
    }
    
    public function getCountry(){
		
		return $this->hasOne(Country::className(), ['id_countries' => 'grandparent_id']);	
		
		}
		
		public function getStates(){
		
		return $this->hasOne(States::className(), ['id' => 'parent_id']);	
		
		}
    
     public function getUpdateduser()
    {
    	  return $this->hasOne(User::className(), ['id' => 'updated_by']);
       
    }
    
    /*Get Country Name By country_id */
		public function getCountryNameval(){
		
			if(isset($this->grandparent_id)){
			$result = self::find()->where(['id' =>$this->grandparent_id])->one();
			if($result)
				return $result->group_name;
				else
				return '';
			}else{
				return '';
			}
		
		}
		public function getStatelabel(){
			if($this->type !=='' || $this->type==3){
				return 'State';
			}else{
				return '';
			}
		}
		
		public function getCountrylabel(){
			if(($this->type !=='') || ($this->type==2 || $this->type==3 ) ){
				return 'Country';
			}else{
				return '';
			}
		}
		
		public function getLevelNameval(){
			if(isset($this->type)){
				
				if($this->type==0){
					return 'General';
				}elseif($this->type==1){
					return 'Country';
				}elseif($this->type==2){
					return 'State';
				}elseif($this->type==3){
					return 'City';
				}elseif($this->type==4){
					return 'Food Order Checkout';
				}
			
			
			}
			
		}
		
		
		
		/*Get State Name By state_id */
		public function getStateNameval(){
					
			if(($this->parent_id !='') && ($this->grandparent_id !='')){					
			$result = self::find()->where(['id' =>$this->parent_id])->one();
				return $result->group_name;	
			}else{
				return '';
			}
		
		}
    
    
 	 public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
			    	if($insert){			    		
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}
}
