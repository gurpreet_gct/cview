<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PartnerTransactionsFees;
use common\models\User;

/**
 * PartnerTransactionsFeesSearch represents the model behind the search form about `common\models\PartnerTransactionsFees`.
 */
class PartnerTransactionsFeesSearch extends PartnerTransactionsFees
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['offer_type','store_id','id', 'partner_id', 'order_id', 'workorder_id', 'is_deducted', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['orderAmount', 'transactionFees'], 'number'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		 $usertype = Yii::$app->user->identity->user_type;
         $userId = $usertype == User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
         
	   if($usertype==User::ROLE_ADMIN){
			 
			$query = PartnerTransactionsFees::find();
			
		}else{
				$query = PartnerTransactionsFees::find()
						->where('partner_id='.$userId);
		}      
		$query->orderBy('id desc');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([         
            'partner_id' => $this->partner_id,
            'store_id' => $this->store_id,
            'offer_type' => $this->offer_type,
            'order_id' => $this->order_id,
            'orderAmount' => $this->orderAmount,
            'transactionFees' => $this->transactionFees,
            'workorder_id' => $this->workorder_id,
            'is_deducted' => $this->is_deducted,
         
        ]);
       	/* Apply date range created_at */                     
		if($this->created_at!=''){				
			$datanewtime = explode(' - ',$this->created_at);
			$start_date = strtotime($datanewtime[0]);
			$end_date = strtotime($datanewtime[1]);
			$end_date =  $end_date+86400;
			if($start_date!=''){
				$query->andFilterWhere(['between','created_at',$start_date,$end_date]);
				 $this->created_at = null;
			 }
		 }
        return $dataProvider;
    }
}
