<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransactionsFees;

/**
 * TransactionsFeesSearch represents the model behind the search form about `common\models\TransactionsFees`.
 */
class TransactionsFeesSearch extends TransactionsFees
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'charge_type'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransactionsFees::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'charge_type' => $this->charge_type,
            'amount' => $this->amount,
        ]);

        return $dataProvider;
    }
}
