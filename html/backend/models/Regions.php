<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "tbl_regions".
 *	
 * @property integer $id
 * @property string $region
 * @property string $region_code
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Regions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_regions';
    }
    
 
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
          	[['region', 'region_code', 'status'], 'required'],
          	 ['region_code','unique'],
            [['region'], 'string', 'max' => 100],
            
            [['region_code'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'region' => Yii::t('app','Region'),
            'region_code' => Yii::t('app','Region Code'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_by' => Yii::t('app','Updated By'),
        ];
    }
    
    
    /* get user status type */
    public function getStatustype()
    {
    	  if($this->status==1){
         return 'Enable';    	  
    	  }else {
    	  return 'Disable';
       }
    }
    
    /* get created user name from User model */
    public function getCreateduser()
    {
    	  return $this->hasOne(User::className(), ['id' => 'created_by']);
       
    }
    
    /* get the updated name form the User model */
     public function getUpdateduser()
    {
    	  return $this->hasOne(User::className(), ['id' => 'updated_by']);
       
    }
    
    
 	public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
			    	if($insert){			    		
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}
		  
    
}
