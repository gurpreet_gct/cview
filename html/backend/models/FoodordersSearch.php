<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Foodorders;
use common\models\Orders;

/**
 * FoodorderSearch represents the model behind the search form about `common\models\Foodorders`.
 */
class FoodordersSearch extends Foodorders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'is_offline', 'status', 'qty',  'store_id', 'created_at', 'updated_at'], 'integer'],
            [['grand_total', 'item_total_price','sub_item_total_price', 'tax_amount', 'discount_amount'], 'number'],
          //  [['coupon_code'], 'string', 'max' => 250]
        ];
    }
    
    public function attributeLabels()
    {
        return [
			'id'=> Yii::t('app','ID'),
			'user_id' => Yii::t('app','User Id'),
			'is_offline' => Yii::t('app','Is Offline'),
			'status' => Yii::t('app','Status'),
			'qty' => Yii::t('app','Quantity'),
			'grand_total' => Yii::t('app','Grand Total'),
			'item_total_price' => Yii::t('app','Item Total Price'),
			'sub_item_total_price' => Yii::t('app','Sub Item Total Price'),
			'tax_amount' => Yii::t('app','Tax Amount'),
			'store_id' => Yii::t('app','Store Id'),
			'discount_amount' => Yii::t('app','Discount Amount'),
			'created_at' => Yii::t('app','Created At'),
			'updated_at' => Yii::t('app','Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    
    public function search($params)
       {
		   $usertype = Yii::$app->user->identity->user_type;
		$userId = $usertype==7?Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
	if($usertype==1){
    $query = Foodorders::find();
   }else{
	   $query=Foodorders::find()->where(['store_id' => $userId]);
   }
   $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>['defaultOrder'=>['id'=> SORT_DESC]],
            ]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        else
        {
 $query->andFilterWhere(['id' => $this->id,]);
    $query->andFilterWhere(['like', 'status', $this->status])
          ->andFilterWhere(['like', 'user_id', $this->user_id])
          ->andFilterWhere(['like', 'qty', $this->qty])
          ->andFilterWhere(['like', 'store_id', $this->store_id])
          ->andFilterWhere(['like', 'status', $this->status])
          ->andFilterWhere(['like', 'is_offline', $this->is_offline])
          ->andFilterWhere(['like', 'grand_total', $this->grand_total])
          ->andFilterWhere(['like', 'created_at', $this->created_at])
          ->andFilterWhere(['like', 'updated_at', $this->updated_at])
          ->andFilterWhere(['like', 'tax_amount', $this->tax_amount]);
 
    return $dataProvider;
   }
      }
 }
