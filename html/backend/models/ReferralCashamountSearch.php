<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ReferralCashamount;

/**
 * ReferralCashamountSearch represents the model behind the search form about `common\models\ReferralCashamount`.
 */
class ReferralCashamountSearch extends ReferralCashamount
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at', 'updated_by'], 'integer'],
            [['lifetime_commission', 'unused_commission', 'used_commission'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // add conditions that should always apply here
		$usertype = Yii::$app->user->identity->user_type;
        $userId = $usertype==\common\models\User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
       
        $query = ReferralCashamount::find();       
       
        if($usertype!=\common\models\User::ROLE_ADMIN){
            $query->where(['user_id'=>$userId]);
        }	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'lifetime_commission' => $this->lifetime_commission,
            'unused_commission' => $this->unused_commission,
            'used_commission' => $this->used_commission,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
