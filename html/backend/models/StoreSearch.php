<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Store;

/**
 * StoreSearch represents the model behind the search form about `app\models\Store`.
 */
class StoreSearch extends Store
{
	public $pid="";
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country', 'owner', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['storename', 'address', 'street', 'city', 'state'], 'safe'],
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
            'storename' => Yii::t('app','Store Name'),
            'email' => Yii::t('app','Store Email'),
            'phoneNumber' => Yii::t('app','Phone Number'),
            'countrycode' => Yii::t('app','Country Code'),
            'addopeninghours' => Yii::t('app','Add "Store Opening Hours"'),
            'address' => Yii::t('app','Address'),
            'street' => Yii::t('app','Street'),
            'city' => Yii::t('app','City'),            
            'state' => Yii::t('app','State'),
            'postal_code' => Yii::t('app','Postcode'),
            'latitude' => Yii::t('app','Latitude'),
            'longitude' => Yii::t('app','Longitude'),
            'country' => Yii::t('app','Country'),
            'owner' => Yii::t('app','Advertiser'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_by' => Yii::t('app','Updated By'),
			'mondayopeninghoursAM' =>Yii::t('app','Monday opening time') ,
			'mondayopeninghoursPM' =>Yii::t('app','Monday closing time'),
			'tuesdayopeninghoursAM' => Yii::t('app','Tuesday opening time'),
			'tuesdayopeninghoursPM' => Yii::t('app','Tuesday closing time'),
			'wednesdayopeninghoursAM' => Yii::t('app','Wednesday opening time'),
			'wednesdayopeninghoursPM' =>Yii::t('app','Wednesday closing time') ,
			'thursdayopeninghoursAM' => Yii::t('app','Thursday opening time'),
			'thursdayopeninghoursPM' => Yii::t('app','Thursday closing time'),
			'fridayopeninghoursAM' => Yii::t('app','Friday opening time'),
			'fridayopeninghoursPM' => Yii::t('app','Friday closing time'),
			'saturdayopeninghoursAM' => Yii::t('app','Saturday opening time'),
			'saturdayopeninghoursPM' => Yii::t('app','Saturday closing time'),
			'sundayopeninghoursAM' => Yii::t('app','Sunday opening time'),
			'sundayFullDayOpen' => Yii::t('app','Sunday Full Day Open'),
			'sundayFullDayClose' => Yii::t('app','Sunday Full Day Close'),
			'mondayFullDayOpen' => Yii::t('app','Monday Full Day Open'),
			'mondayFullDayClose' => Yii::t('app','Monday Full Day Close'),
			'tuesdayFullDayOpen' => Yii::t('app','Tuesday Full Day Open'),
			'tuesdayFullDayClose' => Yii::t('app','Tuesday Full Day Close'),
			'wednesdayFullDayOpen' =>Yii::t('app','Wednesday Full Day Open'),
			'wednesdayFullDayClose' => Yii::t('app','Wednesday Full Day Close'),
			'thursdayFullDayOpen' => Yii::t('app','Thursday Full Day Open'),
			'thursdayFullDayClose' => Yii::t('app','Thursday Full Day Close'),
			'fridayFullDayOpen' => Yii::t('app','Friday Full Day Open'),
			'fridayFullDayClose' => Yii::t('app','Friday Full Day Close'),
			'saturdayFullDayOpen' => Yii::t('app','Saturday Full Day Open'),
			'saturdayFullDayClose' =>Yii::t('app','Saturday Full Day Close') ,
        ];
    }
    
    
    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		$usertype = Yii::$app->user->identity->user_type;
        $userId = $usertype==7?Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
         if($usertype==1){
			 if($this->pid)
				$query = Store::find()->where([ 'owner' => $this->pid]);
				else
				$query = Store::find();
        }else{
			 $query = Store::find()->where(['owner' => $userId]);
		}

        $dataProvider = new ActiveDataProvider([
			
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        

        $query->andFilterWhere([
            'id' => $this->id,
            'country' => $this->country,
            'owner' => $this->owner,           
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'storename', $this->storename])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
