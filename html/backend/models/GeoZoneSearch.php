<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\Workorders;
use yii\data\ActiveDataProvider;
use backend\models\GeoZone;

/**
 * GeoZoneSearch represents the model behind the search form about `backend\models\GeoZone`.
 */
class GeoZoneSearch extends GeoZone
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'deal_id','agency_id', 'created_at', 'updated_at'], 'integer'],
            [['area_name'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
public function attributeLabels()
{
	return[
	             'area_name' => Yii::t('app','Area Name'),
            'latitude' => Yii::t('app','Latitude'),
            'longitude' => Yii::t('app','Longitude'),
            'deal_id' => Yii::t('app','Deal'),
             'agency_id' => Yii::t('app','Agency'),
];
}
     
     
     
     
     
     
     
    public function search($params)
    {
        
		$userType = Yii::$app->user->identity->user_type;
		if ($userType == 7){
		$userId = $userType== 7 ? Yii::$app->user->identity->id : Yii::$app->user->identity->advertister_id;
			$query = GeoZone::find()
			->joinWith('workorders')
			->where(GeoZone::tableName().'.deal_id='.Workorders::tableName().'.id')
			->andWhere('tbl_geo_zone.agency_id='.$userId)
			->groupBy(['deal_id','area_name']);
		}else{
			$query = GeoZone::find()
					->joinWith('workorders')
					->where(GeoZone::tableName().'.deal_id ='.Workorders::tableName().'.id')
					->groupBy(['deal_id','area_name']);
			}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,]);
         $query->andFilterWhere(['like','area_name',$this->area_name])
         ->andFilterWhere(['like','latitude',$this->latitude])
         ->andFilterWhere(['like','longitude',$this->longitude])
         ->andFilterWhere(['like','deal_id',$this->deal_id])
          ->andFilterWhere(['like','agency_id',$this->agency_id]);



        return $dataProvider;
    }
}
