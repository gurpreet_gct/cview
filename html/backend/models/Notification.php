<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use common\models\User;


class Notification extends ActiveRecord
{
	 public static function tableName()
	 {
			return '{{%notifications}}';
	 }
	
	public function rules()
    {
        return [
            ['user_id', 'integer'],
          	[['msg', 'user_id'], 'required'],
          	 ['msg','string'], 
          	 ['created_at','integer'], 
        ];
    }
    public function beforeSave($insert)
		{
			    if (parent::beforeSave($insert)) {
			    	
			    	$this->updated_at= time();
			    	if($insert){			    		
			    		
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
		}
		
	public function attributeLabels()
    {
        return [
			'id'=> Yii::t('app','ID'),
            'user_id' => Yii::t('app','User Id'),
            'msg' => Yii::t('app','Msg'),
            'type' => Yii::t('app','Type'),
            'is_read' => Yii::t('app','Is Read'),
            'is_delete' => Yii::t('app','Is Delete'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }			
   
}
