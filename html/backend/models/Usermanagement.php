<?php
	
namespace backend\models;
use Yii;
use common\models\Usertype;
use common\models\User;
use common\models\Store;
use common\models\Profile;
use common\models\Workorders;
use backend\models\Regions;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property integer $status
 * @property string $email
 * @property integer $contact_no
 * @property string $role
 * @property integer $active
 * @property string $tokenhash
 * @property integer $tokenexpired
 * @property string $created
 * @property string $modified
 */
class Usermanagement extends User
{
    /**
     * @inheritdoc
     */
   public $imageFile;
   public $when;
   public $number_max;
   public $confirmpass;
   public $Advertiser;
   public $Loyaltypoints;
      
    
    
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storeid','status','loyaltyPin'], 'integer'], 
            //['referral_percentage','number','min'=>0.1],
            [['referral_percentage'], 'double','min'=>0.1,'max'=>0.99],
            [['advertister_id','user_code'],'string'],
            [['phone'], 'integer','min'=>9,'message' => 'Please enter a valid mobile number.'], 
           // [['phone'], 'match', 'pattern' => '/^\d{10}$/','message' => 'Please enter a valid mobile number.'],
           // ['username','match', 'not' => true, 'pattern' => '/^[a-zA-Z0-9]*\.[a-zA-Z ]*$/','message' => 'Space and special symbols are not allowed.'],
            ['username', 'match', 'pattern' => '/^(?=.{4})(?!.{21})[\w.-]*[a-z]([a-z]*[\w])*$/i', 'message'=>"Please use only letters (a-z), numbers, and periods."],
            ['confirmpass', 'required', 'on' => 'create'],                          
            [['email','firstName', 'lastName', 'password_hash','phone', 'user_type', 'username'], 'required'],           
             [['username','fullName'], 'string', 'max' => 50],
             [['loyaltyPin'], 'string', 'max' => 6],
             [['loyaltyPin'], 'string', 'min' => 6],
            [['imageFile'],'safe'],
				//['password_hash','required','on'=>'create'],           
            ['email','unique'],['username','unique'],
            ['email', 'email'],  
            //[['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],                     
            //[['username','fullName'], 'string', 'max' => 50],
            [['password_hash'], 'string', 'min' => 4,'when' => function($model) {
        return in_array($model->usertype->id,array(1,2,4,5,6,7,8));
    }, ],
            ['confirmpass', 'compare','compareAttribute'=>'password_hash','operator'=>'==','message'=>'Your confirm password does not match.'],
            ['bdayOffer', 'integer'],
            [['sex', 'dob', 'firstName', 'lastName', 'status'], 'required', 'when' => function($model) {
        return !in_array($model->usertype->id,array(2,4,5,6,7,8));
    },    
'whenClient' => "function (attribute, value) {
        var rs=$.inArray($('#usermanagement-user_type').val(), ['2','4','5','6','7','8']);
       
        if(rs<=0)        
        return true;
        else 
        return false;
    }"    
   
    ], 
    
        [['Loyaltypoints','Advertiser'],'required','when'=>function($model){
			return in_array($model->usertype->id,array('2'));
			},
			'whenClient'=>"function (attribute, value) {
					var res= ('#usermanagement-user_type').val();
				   
					if(rs<=0)        
					return true;
					else 
					return false;
				}"
			], 
			
			[['loyaltyPin','advertister_id'],'required','when'=>function($model){
				
				if($model->usertype->id==9)				
					return true;
			}
			],
			 ['loyaltyPin', 'unique', 'targetAttribute' => ['loyaltyPin', 'advertister_id' => 'advertister_id'],'when'=>function($model){
				
				if($model->usertype->id==9)				
					return true;
			}],
			 
           
        ]; 
         
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last Name'),
			'fullName' => Yii::t('app','Full Name'),
            'email' => Yii::t('app','Email'),
            'orgName' => Yii::t('app','Company Name'),
            'fbidentifier' => Yii::t('app','fb Identifier'),
			'role_id' => Yii::t('app','Role'),
            'user_type' => Yii::t('app','User Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','Password'),
			'password_reset_token' => Yii::t('app','Password Reset Token'),
			'phone' => Yii::t('app','Mobile Number'),
            'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date of Birth'),
			'ageGroup_id' => Yii::t('app','Age Group Id'),
			'walletPinCode' => Yii::t('app','Wallet Pin Code'),
			'loyaltyPin' => Yii::t('app','Loyalty Pin'),
			'auth_key' => Yii::t('app','Auth Key'),
			'activationKey' => Yii::t('app','Activation Key'),
			'confirmationKey' => Yii::t('app','Confirmation Key'),
			'access_token' => Yii::t('app','Access Token'),
			'hashKey' => Yii::t('app','Hash Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device Token'),
			'lastLogin' => Yii::t('app','Last Login'),
			'confirmpass' => Yii::t('app','Confirm Password'),
			'Loyaltypoints' => Yii::t('app','Loyalty Pins'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created At'),
            'updated_at'=>Yii::t('app','Updated At'),
            'advertister_id'=>Yii::t('app','Partner'),
            'storeid'=>Yii::t('app','Store Location'),
            'bdayOffer'=>Yii::t('app','Do you agree to gift every user double loyalty points during the week of their birthday?'),
        ];
    }
    
     
      /**
     * Generates Account Confirmation key
     */
    public function confirmationAccountKey()
    {
        $this->confirmationKey = Yii::$app->security->generateRandomString();
    }
    
    public function getUsertype()
    {
         return $this->hasOne(Usertype::className(), ['id' => 'user_type']);
    }
    
    public function getStore()
    {
         return $this->hasOne(Store::className(), ['id' => 'storeid']);
    }
    
    public function getWorkorders()
    {
         return $this->hasOne(Workorders::className(), ['workorderpartner' => 'id']);
    }
   
    
    /* get advertiser */
    public function getadvertiser(){
		if($this->advertister_id){
			$Advertiser = User::findOne(['id' => $this->advertister_id ]);
			
				return $Advertiser->orgName; 
		}
		else{
			return 'not set';
		}
	
	}
    
        
     /* get user status type */
    public function getStatustype()
    {
		
    	  if($this->status==10){
         return 'Enable';    	  
    	  }else {
    	  return 'Disable';
       }
    }
    
    public function getsextype()
    {
         $sex = $this->sex;
        		 if($sex==1){         	
        				return 'Male';
        		 }else {
        			  	return 'Female';
        		 }
    } 
    
    public function getimagename(){
		 if($this->fbidentifier!=""){
			 return $this->image;
		  }else if(($this->image!="")&&(!strstr($this->image,'http'))) {
		   return Yii::$app->params['userUploadPath']."/".$this->image; 
		 }else {
			  return Yii::$app->params['userUploadPath']."/".'no-image.jpg'; 
		 }
     }
     
     public function afterFind()
			{
				parent::afterFind();
				
					if(!strstr($this->dob,"-"))
					{               
						$this->dob=date("Y-m-d",$this->dob);
					}
					
			
			return true;		
			}
			
    public function getSsdRegionval(){
	    $ssd = Profile::find()->select('ssdRegion')->where(['user_id' =>$this->id])->one();
	    if(isset($ssd->ssdRegion)){   	   
			$ssdval = Regions::find()->select('region')->where(['id'=> $ssd->ssdRegion])->one();
			 
			if(isset($ssdval->region)){
				return $ssdval->region;
			}else{
			return false;
			}
		}else{
			return false;
		}
    } 
		
	 public function beforeSave($insert)
	 
	 // the following three lines were added for auto role assignment:
     
     
		{
			if(strstr($this->dob,"-"))	{			
				$this->dob = strtotime($this->dob);
			}
        	
	    if (parent::beforeSave($insert)) {
	    	
	    	$this->updated_at= time();
	    	if($insert){			    		
	    		$this->status =10;
	    		$this->confirmationAccountKey();
	    		$this->created_at= time();
	    		$this->user_code = Yii::t('app', 'AW-').$this->username;
	     	}
			    
	    	return true;
		    } else {
	    	
		     return false; 
			    }
			}
			
			
		/* send new registration confirmation email */
		public function Sendconfirmation(){
			
			$subject	= $_SERVER['HTTP_HOST']." : Account Confirmation Mail";
			$email		= $this->email;
			$emailData  = ['Name'=> $this->fullName,
						   'confirmationKey'=> $this->confirmationKey
						];
						
			$emailsend	=	Yii::$app->mail->compose(['html' => 'admin-confirm-mail'],$emailData)
								->setTo($email)
								->setFrom(Yii::$app->params["noReplyEmail"],'No Reply')
								->setSubject($subject)
								->send(); 
                                
			if($emailsend)
					return true;                                             
				else
					return false;
						
			
			
		}
			
	    
		public function getBdayOffervalue() {
		
		  if($this->bdayOffer==1) {
		   return 'True'; 
		 }else {
			  return 'false'; 
		 }
     }
	
	public static function getStorelist($partnerId)
    {
		if($partnerId!=''){
		return   ArrayHelper::map(Store::find()
								->select(['id', new \yii\db\Expression("CONCAT(`storename`, ' - ', `city`,' - ',`state`) as storename")])
								->asArray()
								->where('owner='.$partnerId)
								->all(),'id','storename'); 
	 }else{
		return array();
	 }
	}
	
	
		

}
