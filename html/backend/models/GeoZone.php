<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\Deal;
use common\models\Workorders;
use backend\models\Usermanagement;


/**
 * This is the model class for table "tbl_geo_zone".
 *	
 */
class GeoZone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
       public static function tableName()
    {
        return '{{%geo_zone}}';
    }
    
 
    public function rules()
    {
        return [
            [['id', 'agency_id','deal_id', 'created_at', 'updated_at'], 'integer'],
          	[['area_name'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
	 
    public function attributeLabels()
    {
        return [
            'area_name' => Yii::t('app','Area Name'),
            'deal_id' => Yii::t('app','Deal'),
            'agency_id' => Yii::t('app','Partner'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
public function getAdvertiser()
{
		if(isset($this->deal_id)){
			$ad = User::find()->select(["tbl_users.orgName"])
				->join('INNER JOIN','tbl_workorders','tbl_workorders.workorderpartner=tbl_users.id')
				->where('tbl_workorders.id='.$this->deal_id)
					->asArray()
						->one();
		if(isset($ad['orgName'])){
			return $ad['orgName'];
			}else{
				return 'N/A';
				}
			}

}

    
    
    
    
public function getDealtype()
    {
	 if($this->deal_id){
		  $data =  Workorders::find()->select('name')
						->where('id='.$this->deal_id)
						->one();
						return $data['name'];
		 }
		 else{
			 return 'N/A';
			 }

	}
	
	public function advertiserId($id)
{
	//print_r($id);die();
	 if($id){
	  $ad = User::find()->select(["tbl_users.id"])
		->join('INNER JOIN','tbl_workorders','tbl_workorders.workorderpartner=tbl_users.id')
		->where('tbl_workorders.id='.$id)
		->asArray()
		->one();
		if(isset($ad['id'])){
			return $ad['id'];
			}else{
				return '';
				}
		 }else{
				return '';
				}

	}
	public function getWorkorders()
	{
		 return $this->hasOne(Workorders::className(), ['id' => 'deal_id']);
	}

    
}
