<?php

namespace backend\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Loyalty;
use yii\db\Query;
use common\models\User;


/**
 * RegionsSearch represents the model behind the search form about `app\models\Orders`.
 */
class LoyaltySearch extends Loyalty
{
    /**
     * @inheritdoc
     */
      public static function tableName()
    {
        return '{{%loyalty}}';
    }
    public function rules()
    {
         return [[['id'], 'integer'],
          [['user_id'], 'safe'],
           [['created_at'], 'safe'],
          ];


    }



     public function attributeLabels()
    {
        return [
            'id'=>Yii::t('app','ID'),
            'loyalty' => Yii::t('app','Loyalty'),
            'dolor' => Yii::t('app','Dollar'),
            'redeem_dolor' =>Yii::t('app','Redeem Dollar') ,
            'user_id' => Yii::t('app','User name'),
            'pointvalue' => Yii::t('app','Loyalty Dollars'),
            'redeem_pointvalue' => Yii::t('app','Redeem Dollars'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At '),
            'updated_at' => Yii::t('app','Updated At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_by' => Yii::t('app','Updated By'),
            'loyalty_pin' => Yii::t('app','Loyalty Pin'),
            'redeem_loyalty' =>Yii::t('app','Redeem Loyalty'),
           ];
    }





    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params){

	  	$query = Loyalty::find();
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
		if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }if($this->user_id!=''){

			$new_user_id =  User::find()->select('id')->where(['LIKE', 'username', $this->user_id])->all();
			$result_arr = array();
		    foreach($new_user_id as $key=>$new_user_ids){
				$result_arr[] = $new_user_ids->id;
			}
			$idS = implode(',', $result_arr);
			if($idS!=''){
			 $idnewS = $idS;
		    }else{
				 $idnewS = 0;
			}
		    $query->andWhere('user_id IN('.$idnewS.')')->all();
			}
			$timestamp = strtotime($this->created_at);
			if($this->created_at!=''){
			$datanewtime = explode(' - ',$this->created_at);
			$start_date = strtotime($datanewtime[0]);
			$end_date = strtotime($datanewtime[1]);
			$end_date =  $end_date+86400;
			if($start_date!=''){
				$query->andFilterWhere(['and', "created_at>=$start_date", "created_at<$end_date"]);
			 }
		 }

			$query->andFilterWhere([
						'id' => $this->id,])->orderBy(['id' => SORT_DESC,])->all();


			//$query->andFilterWhere(['like', 'grand_total', $this->grand_total]);
           return $dataProvider;
    }
}
