<?php

namespace backend\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
//use common\models\Loyalty;
use common\models\Loyaltypoint;
//use common\models\Loyaltypoint;
use yii\db\Query;
use common\models\User;


/**
 * RegionsSearch represents the model behind the search form about `app\models\Orders`.
 */
class LoyaltypointSearch extends Loyaltypoint
{
    /**
     * @inheritdoc
     */
      public static function tableName()
    {
        return '{{%loyaltypoints}}';
    }
    public function rules()
    {
         return [[['id'], 'integer'],
          [['user_id'], 'safe'],
           [['created_at'], 'safe'],
          ];


    }



     public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'user_id' =>Yii::t('app','User Name'),
			'order_id' =>Yii::t('app','Order ID') ,
			'store_id' =>Yii::t('app','Store ID') ,
			'status' => Yii::t('app','Status'),
			'method' => Yii::t('app','Method'),
			'transaction_id' => Yii::t('app','Transaction Id'),
			'created_at' => Yii::t('app','created At'),
			'updated_at' => Yii::t('app','updated At'),
			'created_by' => Yii::t('app','created By'),
			'updated_by' => Yii::t('app','updated By'),
			'pointvalue' =>Yii::t('app','Loyalty Value') ,
			'valueOnDay' => Yii::t('app','Value On Day'),
			'rValueOnDay' =>Yii::t('app','R Value On Day') ,
			'loyaltypoints' =>Yii::t('app','Loyalty Dollars') ,
           ];
    }





    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params){

	  	$query = Loyaltypoint::find();
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
		if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }if($this->user_id!=''){

			$new_user_id =  User::find()->select('id')->where(['LIKE', 'username', $this->user_id])->all();
			$result_arr = array();
		    foreach($new_user_id as $key=>$new_user_ids){
				$result_arr[] = $new_user_ids->id;
			}
			$idS = implode(',', $result_arr);
			if($idS!=''){
			 $idnewS = $idS;
		    }else{
				 $idnewS = 0;
			}
		    $query->andWhere('user_id IN('.$idnewS.')')->all();
			}
			$timestamp = strtotime($this->created_at);
			if($this->created_at!=''){
			$datanewtime = explode(' - ',$this->created_at);
			$start_date = strtotime($datanewtime[0]);
			$end_date = strtotime($datanewtime[1]);
			$end_date =  $end_date+86400;
			if($start_date!=''){
				$query->andFilterWhere(['and', "created_at>=$start_date", "created_at<$end_date"]);
			 }
		 }

			$query->andFilterWhere([
						'id' => $this->id,])->orderBy(['id' => SORT_DESC,])->all();


			//$query->andFilterWhere(['like', 'grand_total', $this->grand_total]);
           return $dataProvider;
    }
}
