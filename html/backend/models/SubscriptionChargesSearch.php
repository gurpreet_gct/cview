<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SubscriptionCharges;

/**
 * SubscriptionChargesSearch represents the model behind the search form about `common\models\SubscriptionCharges`.
 */
class SubscriptionChargesSearch extends SubscriptionCharges
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'partner_id', 'payment_status','updated_at'], 'integer'],
            [['amount'], 'number'],
            [['due_date','transaction_id', 'account_id','created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $usertype = Yii::$app->user->identity->user_type;
        $userId = $usertype==\common\models\User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
        $query = SubscriptionCharges::find();
        if($usertype!=\common\models\User::ROLE_ADMIN){
            $query->where(['partner_id'=>$userId]);
        }



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query->orderBy('id desc'),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'partner_id' => $this->partner_id,
            'amount' => $this->amount,
            'payment_status' => $this->payment_status,
            'updated_at' => $this->updated_at,
        ]);

 	/* Apply date range due_date */
		if($this->due_date!=''){
			$datanewtime = explode(' - ',$this->due_date);
			$start_date = strtotime($datanewtime[0]);
			$end_date = strtotime($datanewtime[1]);
			$end_date =  $end_date+86400;
			if($start_date!=''){
				$query->andFilterWhere(['between','due_date',$start_date,$end_date]);
				 $this->due_date = null;
			 }
		 }
        return $dataProvider;
    }
}
