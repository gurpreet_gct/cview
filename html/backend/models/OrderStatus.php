<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;



/**
 * This is the model class for table "tbl_order_status".

 */
 
class OrderStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_order_status';
    }

    /**
     * @inheritdoc
     */
   
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_by' => Yii::t('app','Updated By'),
        ];
    }
    
    
   
}
