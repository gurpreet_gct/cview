<?php

namespace backend\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;
use common\models\Addresstypes;


/**
 * RegionsSearch represents the model behind the search form about `app\models\Orders`.
 */
class AddresstypesSearch extends Addresstypes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['type'], 'string'],
          	];
    }
    
    
    public function attributeLabels()
    {
        return [
              'id' => Yii::t('app','Id'),
              'type' => Yii::t('app','Type'),
              'is_delete' => Yii::t('app','Is Delete'),
             'created_at' => Yii::t('app','Created At'),
             'updated_at' => Yii::t('app','Updated At'),
             'created_by' => Yii::t('app','Created By'),
             'updated_by' => Yii::t('app','Updated By') ,
            
        ];
    }
    
    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params){
        $query = AddresstypesSearch::find();
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
		if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		$query->andFilterWhere([
					'is_delete' => '0',
				]);
		$query->andFilterWhere(['like', 'type', $this->type]);
         return $dataProvider;
    }
}
