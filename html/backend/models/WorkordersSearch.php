<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Workorders;


/**
 * WorkordersSearch represents the model behind the search form about `backend\models\Workorders`.
 */
class WorkordersSearch extends Workorders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'duration'], 'integer'],

            [['name', 'type', 'countryID', 'mediaContractNumber', 'noPasses', 'dateFrom', 'dateTo', 'expires', 'userType', 'agedGroup','beaconGroup','offerTitle','workorderpartner','status'], 'safe'],

        ];
    }
    
    
    public function attributeLabels()
    {
        return [
             'id' => Yii::t('app','Id'),
            'name' => Yii::t('app','Campaign Name'),
            'type' => Yii::t('app','Campaign Type'),
            'countryID' => Yii::t('app','Select Country'),
            #'advertiserID' => Yii::t('app','Advertiser'),
            'resellerID' => Yii::t('app','Reseller'),
            'mediaContractNumber' => Yii::t('app','Media Contract Number / PO Number'),
            'noPasses' => Yii::t('app','Number of Passes'),
            'allowpasstouser' => Yii::t('app','Select how many times the pass can be used'),
            'workorderpartner' => Yii::t('app','Select Partner'),
            'duration' => Yii::t('app','Duration'),
            'dateFrom' => Yii::t('app','Start Date'),
            'dateTo' => Yii::t('app','End Date'),
            'expires' => Yii::t('app','Campaign Expires'),
            'userType' => Yii::t('app','User Type'),
            'rulecondition' => Yii::t('app','Rules/Conditions'),
            'agedGroup' => Yii::t('app','Age Group'),
            'buynow' => Yii::t('app','Buy in-app'),
            'beaconGroup' => Yii::t('app','Beacon Group'),
            'qrtitle' => Yii::t('app','Enter Text for QR image'),
            'workorderType' => Yii::t('app','Workorder Type'),
            'mediaContractNumber' => Yii::t('app','Media Contract Number'),
            'usedPasses' => Yii::t('app','Used Passes'),
            'qtyPerPass' => Yii::t('app','Quantity Per Pass'),
            'catIDs' => Yii::t('app','Cat IDs'),
            'uniqueID' => Yii::t('app','Unique ID'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','created at'),
			'updated_at' => Yii::t('app','updated at'),
			'created_by' => Yii::t('app','created by'),
			'updated_by' => Yii::t('app','updated by'),
			'dayparting' => Yii::t('app','Day Parting'),
			'daypartingcustom1' => Yii::t('app','Day Parting Custom1'),
			'daypartingcustom2' => Yii::t('app','Day Parting Custom2'),
			'temperature' => Yii::t('app','Temperature'),
			'temperaturestart' => Yii::t('app','Temperature Start'),
			'temperaturend' => Yii::t('app','Temperature End'),
			'daypartingcustom1end' => Yii::t('app','Day Parting Custom1 End'),
			'daypartingcustom2end' => Yii::t('app','Day Parting Custom2 End'),
			'daypartingcustom3end' => Yii::t('app','Day Parting Custom3 End'),
			'rulecondition' => Yii::t('app','Rule_condition'),
			'behaviourvisit1' => Yii::t('app','Behaviour Visit 1'),
			'behaviourvisit2' => Yii::t('app','Behaviour Visit 2'),
			'behaviourvisit3' => Yii::t('app','Behaviour Visit 3'),
			'behaviourDwell' => Yii::t('app','Behaviour Dwell'),
			'purchaseHistory1' => Yii::t('app','Purchase History 1'),
			'purchaseHistory3' => Yii::t('app','purchase History 3'),
			'Loyalty1' => Yii::t('app','Loyalty 1'),
			'Loyalty2' => Yii::t('app','Loyalty 2'),
			'Loyalty3' => Yii::t('app','Loyalty 3'),
			'purchaseHistory2' => Yii::t('app','Purchase History 2'),
			'usereturned' => Yii::t('app','Use Returned'),
			'userenter' => Yii::t('app','User Enter'),
			'behaviourDwellMinutes' => Yii::t('app','Behaviour Dwell Minutes'),
        ];
    }
    
    
    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		
		$usertype = Yii::$app->user->identity->user_type;
		$user_id = Yii::$app->user->identity->id;
		


       if($usertype==1){
        $query = Workorders::find()->joinWith('workorderpopup',false,'LEFT JOIN');
        
		}else{
			  $query = Workorders::find()->joinWith('workorderpopup',false,'LEFT JOIN')->where([Workorders::tableName().'.workorderpartner' => $user_id]);
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
             'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        
        $dataProvider->sort->attributes['offerTitle'] = [       
			'asc' => ['tbl_workorder_popup.offerTitle' => SORT_ASC],
			'desc' => ['tbl_workorder_popup.offerTitle' => SORT_DESC],
    ];
        

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
         
            return $dataProvider;
        }


        $query->andFilterWhere([
            Workorders::tableName().".id" => $this->id,  
            'duration' => $this->duration,
            'type' => $this->type,
            'workorderpartner' => $this->workorderpartner,
        ]);
		
		

        $query->andFilterWhere(['like', 'name', $this->name])
          #  ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'countryID', $this->countryID])
            ->andFilterWhere(['like', 'mediaContractNumber', $this->mediaContractNumber])
            ->andFilterWhere(['like', 'noPasses', $this->noPasses])
           # ->andFilterWhere(['and', "dateFrom>=$dateRange[0]", "dateFrom<=$dateRange[1]"])
            ->andFilterWhere(['like', 'dateFrom', strtotime($this->dateFrom)])
            ->andFilterWhere(['like', 'dateTo', strtotime($this->dateTo)])
            ->andFilterWhere(['like', 'expires', $this->expires])
            ->andFilterWhere(['like', 'userType', $this->userType])      
			->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'agedGroup', $this->agedGroup])          
            ->andFilterWhere(['like', 'offerTitle', $this->offerTitle])          

            ->andFilterWhere(['like', 'beaconGroup', $this->beaconGroup]);
            
            
             /* Apply date range created_at */                     
		if($this->dateFrom!=''){				
			$datanewtime = explode(' - ',$this->dateFrom);
			$start_date = strtotime($datanewtime[0]);
			$end_date = strtotime($datanewtime[1]);
			$end_date =  $end_date+86400;
			if($start_date!=''){
				$query->andFilterWhere(['between', Workorders::tableName().".dateFrom", $start_date, $end_date])->orderby('dateFrom asc');
			 }
		 }
		 
		         /* Apply date range created_at */                     
		if($this->dateTo!=''){				
			$datanewtime = explode(' - ',$this->dateTo);
			$start_date = strtotime($datanewtime[0]);
			$end_date = strtotime($datanewtime[1]);
			$end_date =  $end_date+86400;
			if($start_date!=''){
				$query->andFilterWhere(['between', Workorders::tableName().".dateTo", $start_date, $end_date])->orderby('dateTo asc');
			 }
		 }

        return $dataProvider;
    }
}  
