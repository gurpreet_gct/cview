<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;

/**
 * notification setting model
 */
class Notificationsetting extends ActiveRecord
{
    /**
     * @inheritdoc
     */
	 
	public static function tableName()
	{
		return '{{%notification_setting}}';
	}
	 
    public function rules()
    {
		
        return [
            [['id', 'user_id'], 'integer'],
            [['notification_allow'], 'required'],
			['notification_allow', 'match', 'pattern' => '/^[0-9]+$/', 'message' => 'Please enter valid positive integer.'],
            [['is_on'], 'required'],
			['is_on', 'match', 'pattern' => '/^[0-9]+$/', 'message' => 'Please enter valid positive integer.'],
        ];
		
    }
	
	
	public function attributeLabels(){
		
		
        return [
			'id'=> Yii::t('app','ID'),
            'user_id' => Yii::t('app','User Id'),
            'notification_allow' => Yii::t('app','Notification Allow'),
            'is_on' => Yii::t('app','Notification setting'),
        ];
		
    }
    

}
