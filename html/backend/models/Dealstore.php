<?php

namespace backend\models;



use Yii;


/**
 * This is the model class for table "tbl_deal_store".
 *
 * @property integer $id
 * @property integer $store_id
 * @property integer $beacon_id
 * @property integer $status
 */
class Dealstore extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_deal_store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
             [['store_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'deal_id' => Yii::t('app','Deal'),
			'store_id' => Yii::t('app','Store'),
			'storename' => Yii::t('app','Store Name'),
        ];
    }
    
    
}
