<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Beaconmanager;
use common\models\User;
/**
 * BeaconManagerSearch represents the model behind the search form about `app\models\BeaconManager`.
 */
class BeaconmanagerSearch extends Beaconmanager
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', /*'passID',*/ 'locationOwnerID','sn', /*'locationBrockerID',*/ 'locationID', 'countryID', 'cityID', 'suburbID', 'uuid', 'major', 'minor', 'rssiID', 'intervalID'], 'integer'],
            [['name', 'linksUrl', 'majorValue', 'minorValue', 'powerValue', 'soundFileUrl', 'timeStamp', 'category', 'groups', /*'businessType', 'businessWebAddress', 'beaconSoundTitle', 'passUrl',*/ 'streetNumber', 'streetAddress', 'postCode', /*'imgBeaconLoc',*/ 'manufacturer', 'manufacturerSerial', 'status'], 'safe'],
            [['rSSIValue', 'interval', 'latitude', 'longitude'], 'number'],
        ];
    }



	 public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
            'sn' => Yii::t('app','SN'),
            'identifier' => Yii::t('app','Identifier'),
            'linksUrl' => Yii::t('app','Links Url'),
            'majorValue' => Yii::t('app','Major Value'),
            'minorValue' =>Yii::t('app','Minor Value'),
            'powerValue' =>Yii::t('app','Power Value'),
            'rSSIValue' =>  Yii::t('app','RSSI value'),
            'soundFileUrl' => Yii::t('app','Sound File Url'),
            'timeStamp' =>Yii::t('app','Time Stamp') ,
            'interval' => Yii::t('app','Interval'),
            'category' => Yii::t('app','Category'),
            'groups' => Yii::t('app','Beacon Groups'),          
            'businessType' => Yii::t('app','Business Type'),
            'businessWebAddress' => Yii::t('app','Business Web Address'),
            'beaconSoundTitle' => Yii::t('app','Beacon Sound Title'),           
            'passUrl' => Yii::t('app','Pass Url'),
            'locationOwnerID' =>Yii::t('app','Relevant Location Owner') ,            
            'locationID' => Yii::t('app','Location'),
            'streetNumber' => Yii::t('app','Street Number'),
            'streetAddress' => Yii::t('app','Street Address'),
            'countryID' => Yii::t('app','Country'),
            'cityID' => Yii::t('app','City'),
            'suburbID' =>Yii::t('app','Region/Suburb') ,
            'postCode' => Yii::t('app','Postcode'),
            'latitude' => Yii::t('app','Latitude'),
            'longitude' =>Yii::t('app','Longitude') ,
            'imgBeaconLoc' => Yii::t('app','Picture of Beacon Location'),
            'manufacturer' => Yii::t('app','Manufacturer'),
            'manufacturerSerial' =>Yii::t('app','Manufacturer Serial Number') ,
            'uuid' => Yii::t('app','UUID'),
            'major' => Yii::t('app','Major'),
            'minor' => Yii::t('app','Minor'),
            'rssiID' => Yii::t('app','RSSI'),
            'intervalID' =>Yii::t('app','Ad Interval') ,
            'status' => Yii::t('app','Status'),
          ];
    }
	
	
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $usertype = Yii::$app->user->identity->user_type;
         $userId = $usertype==User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
        if($usertype==User::ROLE_ADMIN){
        $query = BeaconManager::find();
		}else{
			 $query = BeaconManager::find()->where(['locationOwnerID' =>$userId]);
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'rSSIValue' => $this->rSSIValue,
            'interval' => $this->interval,            
            'locationOwnerID' => $this->locationOwnerID,           
            'locationID' => $this->locationID,
            'countryID' => $this->countryID,
            'cityID' => $this->cityID,
            'suburbID' => $this->suburbID,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'uuid' => $this->uuid,
            'sn' => $this->sn,
            'major' => $this->major,
            'minor' => $this->minor,
            'rssiID' => $this->rssiID,
            'intervalID' => $this->intervalID,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'notesBox', $this->notesBox])
            ->andFilterWhere(['like', 'linksUrl', $this->linksUrl])
            ->andFilterWhere(['like', 'majorValue', $this->majorValue])
             ->andFilterWhere(['like', 'sn', $this->sn])
            ->andFilterWhere(['like', 'minorValue', $this->minorValue])
            ->andFilterWhere(['like', 'powerValue', $this->powerValue])
            ->andFilterWhere(['like', 'soundFileUrl', $this->soundFileUrl])
            ->andFilterWhere(['like', 'timeStamp', $this->timeStamp])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'groups', $this->groups])        
            
            ->andFilterWhere(['like', 'streetNumber', $this->streetNumber])
            ->andFilterWhere(['like', 'streetAddress', $this->streetAddress])
            ->andFilterWhere(['like', 'postCode', $this->postCode])
            
            ->andFilterWhere(['like', 'manufacturer', $this->manufacturer])
            ->andFilterWhere(['like', 'manufacturerSerial', $this->manufacturerSerial])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
