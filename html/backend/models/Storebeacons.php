<?php

namespace backend\models;



use Yii;


/**
 * This is the model class for table "tbl_store_beacons".
 *
 * @property integer $id
 * @property integer $store_id
 * @property integer $beacon_id
 * @property integer $status
 */
class Storebeacons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_store_beacons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
             [['store_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'store_id' => Yii::t('app','Store Location'),
            'beacon_id' => Yii::t('app','Beacon Devices'),
            'status' => Yii::t('app','Status'),
        ];
    }
    
  
  
		
    
    public function getStatustype()
    {
    	  if($this->status==1){
         return 'Enable';    	  
    	  }else {
    	  return 'Disable';
       }
    }
    
   /* public function getStorename(){
		
		return $this->hasOne(Store::className(), ['id' => 'store_id']);
	}
	
	 public function getBeaconname(){
		
		return $this->hasOne(Beaconmanager::className(), ['id' => 'beacon_id']);
	}
	*/
	
	
    
    
}
