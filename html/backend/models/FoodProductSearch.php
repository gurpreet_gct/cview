<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FoodProduct;

/**
 * FoodProductSearch represents the model behind the search form about `common\models\FoodProduct`.
 */
class FoodProductSearch extends FoodProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category', 'status', 'store_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['p_type','name', 'sku', 'description','urlkey', 'related_product'], 'safe'],
            [['price'], 'number'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
			'id'=> Yii::t('app','ID'),
			'p_type' => Yii::t('app','P Type'),
			'name' => Yii::t('app','Name'),
			'sku' => Yii::t('app','SKU'),
			'category' => Yii::t('app','Category'),
			'description' => Yii::t('app','Description'),
			'short_description' => Yii::t('app','Short Description'),
			'urlkey' => Yii::t('app','Url Key'),
			'price' => Yii::t('app','Price'),
			'qty' => Yii::t('app','Quantity'),
			'status' => Yii::t('app','Status'),
			'is_online' => Yii::t('app','Is Online'),
			'image' => Yii::t('app','Image'),
			'store_id' => Yii::t('app','Store Id'),
			'related_product' => Yii::t('app','Related Product'),
			'is_added' => Yii::t('app','Is Added'),
			'created_at' => Yii::t('app','Created At'),
			'updated_at' => Yii::t('app','Updated At'),
			'created_by' => Yii::t('app','Created By'),
			'updated_by' => Yii::t('app','Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FoodProduct::find()->where('p_type IN(2,3)');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category' => $this->category,
            'price' => $this->price,
            'status' => $this->status,
            'p_type' => $this->p_type,
            'store_id' => $this->store_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

			$query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'sku', $this->sku])          
            ->andFilterWhere(['like', 'urlkey', $this->urlkey])
            ->andFilterWhere(['like', 'related_product', $this->related_product]);

        return $dataProvider;
    }
}
