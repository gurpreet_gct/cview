define({ "api": [
  {
    "type": "",
    "url": "Instructions",
    "title": "",
    "name": "General_Instructions",
    "version": "0.1.1",
    "group": "A__Commercial_View",
    "description": "<h3>Points to be taken care:</h3> <ul> <li><em>Base Url is</em> <code>http://cview.civitastech.com/</code></li> <li><em>Authentication</em> <code>Some Requests need an Authentication before executing, To satisfy Authentication you have to pass a valid &quot;auth_key&quot; Bearer Token as a Authorization under headers</code></li> <li><em>auth_key</em> <code>To get a valid &quot;auth_key&quot; please follow &quot;Get An Auth Key&quot;</code></li> </ul>",
    "filename": "api/modules/v1/controllers/webhooks/UserController.php",
    "groupTitle": "A__Commercial_View"
  },
  {
    "type": "post",
    "url": "api/web/v1/webhooks/users/get-auth-key",
    "title": "Get An Auth Key",
    "name": "Get_An_Auth_Key",
    "version": "0.1.1",
    "group": "A__Commercial_View",
    "description": "<p>To Get An Auth Key, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>john.doe</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>password123</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"username\": \"john.doe\",\n    \"email\": \"john_doe@example.com\",\n    \"auth_key\": \"fW11CiNa4E5IhUmFRcdQttEXdXB8ee6u\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"username\",\n      \"message\": \"Username in here!! cannot be blank.\"\n    },\n    {\n      \"field\": \"password\",\n      \"message\": \"Password cannot be blank.\"\n    },\n  ],\n  \"status\": 422\n}\nOr\nHTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"password\",\n      \"message\": \"That Pin does not match the registered email address. Please try again or click on Forgot  Pin? to reset it.\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/UserController.php",
    "groupTitle": "A__Commercial_View"
  },
  {
    "type": "post",
    "url": "/api/web/v1/account-verification/verify-bid",
    "title": "Verify account",
    "name": "Verify_Account",
    "version": "0.1.1",
    "group": "AccountVerification",
    "description": "<p>To save bid, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "listing_id",
            "description": "<p>Listing id of the property bid by user</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>User id of the bidder</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>1|manual, 2|api</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n   {\n       \"message\": \"\",\n\t\t  \"data\": {\n\t\t      \"id\": 1,\n\t\t\t  \"register_bid_id\": 2,\n\t\t\t  \"verified_by\": \"351\",\n\t\t\t  \"type\": 1,\n\t\t\t  \"data\": \"{\\\"passport_number\\\":0,\\\"driver_license_number\\\":0,\\\"mediacare_number\\\":0,\\\"recent_utility_bill\\\":0}\",\n\t\t\t  \"complete\": 1,\n\t\t\t  \"created_at\": null,\n\t\t\t  \"created_by\": null,\n\t\t\t  \"updated_at\": null,\n\t\t\t  \"updated_by\": null\n\t\t  },\n\t\t  \"status\": 200\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AccountVerificationController.php",
    "groupTitle": "AccountVerification"
  },
  {
    "type": "post",
    "url": "/api/web/v1/agency/create",
    "title": "Create Agency",
    "name": "CreateUser",
    "version": "0.1.1",
    "group": "Agency",
    "description": "<p>To create new agency, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Agencies's name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address[0]",
            "description": "<p>Agencies first Address</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "latitude[0]",
            "description": "<p>for address[0]</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "longitude[0]",
            "description": "<p>for address[0]</p>"
          },
          {
            "group": "Parameter",
            "type": "text",
            "optional": false,
            "field": "about_us",
            "description": "<p>About Agency</p>"
          },
          {
            "group": "Parameter",
            "type": "base64",
            "optional": false,
            "field": "logo",
            "description": "<p>Agencies logo(path frontend/web/agencylogo).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>(Optional) '10' to force agencies create</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of Agency.</p>"
          },
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "address",
            "description": "<p>Address of the Agency.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n   \"message\": \"\",\n   \"data\": {\n      \"status\": 10,\n      \"name\": \"Doe\",\n      \"address\": \"[\\\"3314, Sector 19D, Sector 19, Chandigarh, 160019\\\",\\\"House No. 3031, Sector 19-D, Chandigarh, 160019\\\"]\",\n     \"logo\": \"http://localhost/AlphaWallet/html/frontend/web/agencylogo/imagename.jpeg\",\n     \"latitude\": \"[\\\"30.72811\\\",\\\"30.719059\\\"]\",\n     \"longitude\": \"[\\\"76.77065\\\",\\\"76.748704\\\"]\",\n     \"about_us\": \"Gross Waddell Pty Ltd is a\\ncommercial real estate agency,\\noperating for over 20 years in the\\nretail, office, industrial and\\ndevelopment property sectors. We\\nprovide Sales and Leasing, Asset\\nManagement\\nand\\nCorporate\\nServices to Vendors, Landlords and\\nInvestors.\",\n     \"email\": \"john_doe@gmail.com\",\n     \"mobile\": \"+911234569874\",\n     \"updated_at\": \"1523960975\",\n     \"created_at\": \"1523960975\",\n     \"id\": 12,\n   },\n   \"status\": 200\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 422 Validation Error\n    {\n      \"message\": \"\",\n      \"data\": [\n        {\n          \"field\": \"name\",\n          \"message\": \"Name \\\"Doe\\\" has already been taken. \"\n        },\n        {\n          \"field\": \"mobile\",\n          \"message\": \"Mobile must be an integer.\"\n        },\n        {\n          \"field\": \"email\",\n          \"message\": \"Email \\\"john_doe@gmail.com\\\" has already been taken.\"\n        }\n      ],\n      \"status\": 422\n    }\nOr\n   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n       {\n         \"field\": \"name\",\n         \"message\": \"Name cannot be blank.\"\n       },\n       {\n         \"field\": \"mobile\",\n         \"message\": \"Mobile cannot be blank.\"\n       },\n       {\n         \"field\": \"logo\",\n         \"message\": \"Logo cannot be blank.\"\n       },\n       {\n         \"field\": \"address\",\n         \"message\": \"Address cannot be blank.\"\n       },\n       {\n         \"field\": \"latitude\",\n         \"message\": \"Latitude cannot be blank.\"\n       },\n       {\n         \"field\": \"longitude\",\n         \"message\": \"Longitude cannot be blank.\"\n       },\n \t\t {\n\t\t\t\"field\": \"about_us\",\n\t\t\t\"message\": \"About Us cannot be blank.\"\n\t\t },\n       {\n         \"field\": \"email\",\n         \"message\": \"Email cannot be blank.\"\n       }\n      ],\n      \"status\": 422\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgencyController.php",
    "groupTitle": "Agency"
  },
  {
    "type": "get",
    "url": "api/web/v1/agency/list-agencies?sort=favourite",
    "title": "Shows list of Agencies with sorting by favourite (if the user is logged-in)",
    "name": "List_Agencies",
    "version": "0.1.1",
    "group": "Agency",
    "description": "<p>To show agencies, Form-Data must be x-www-form-urlencoded</p> <h3>Supported keys for sorting:</h3> <ul> <li><em>favourite</em> | ascending=&gt;'favourite', 'descending' =&gt; '-favourite'</li> </ul> <h3><code>Note: Append (-) for sorting in descending order</code></h3> <h3>Examples for reference:</h3> <h4><code>/api/web/v1/agency/list-agencies?sort=-favourite</code></h4>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>(optional) favourites or -favourites</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "favourites",
            "description": "<p>(optional) 1=&gt; show only favourited list 0=&gt; show all</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n   {\n       \"message\": \"\",\n\t      \"data\": {\n\t\t      \"items\": [\n               {\n                   \"status\": 10,\n\t\t\t\t\t  \"id\": 10,\n\t\t\t\t\t  \"name\": \"Abercromby’s\",\n\t\t\t\t\t  \"email\": \"aastha@gmail.com\",\n\t\t\t\t\t  \"mobile\": 1234569874,\n\t\t\t\t\t  \"logo\": \"http://localhost/AlphaWallet/html/frontend/web/agencylogo/Abercromby’s819.jpg\",\n\t\t\t\t\t  \"backgroungImage\": null,\n\t\t\t\t\t  \"address\": \"[\\\" sco 206 - 207, sector 34 a, chandigarh - 160022\\\",\\\"sco 72 a & 73 a sector 26 grain market chandigarh pin-160019\\\"]\",\n\t\t\t\t\t  \"latitude\": \"[\\\"30.72589\\\",\\\"30.72267\\\"]\",\n\t\t\t\t\t  \"longitude\": \"[\\\"76.75787\\\",\\\"76.79825\\\"]\",\n\t\t\t\t\t  \"about_us\": \"Gross Waddell Pty Ltd is a\\ncommercial real estate agency,\\noperating for over 20 years in the\\nretail, office, industrial and\\ndevelopment property sectors. We\\nprovide Sales and Leasing, Asset\\nManagement\\nand\\nCorporate\\nServices to Vendors, Landlords and\\nInvestors.\",\n\t\t\t\t\t  \"created_at\": 1524034419,\n\t\t\t\t\t  \"updated_at\": 1524034419,\n\t\t\t\t\t  \"is_favourite\": \"\",\n\t\t\t\t\t  \"facebook\": \"https://www.facebook.com/\",\n\t\t\t\t\t  \"twitter\": \"https://twitter.com/\",\n\t\t\t\t\t  \"instagram\": \"https://www.instagram.com/\"\n\t\t\t\t  }\n\t\t\t  ],\n\t\t\t  \"_links\": {\n\t\t          \"self\": {\n\t\t          \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/agencies/list-agencies?sort=-favourite&page=1\"\n\t\t         }\n          },\n\t         \"_meta\": {\n\t\t         \"totalCount\": 6,\n\t             \"pageCount\": 1,\n\t\t         \"currentPage\": 1,\n\t\t         \"perPage\": 20\n\t         }\n   \t  },\t\n       \"status\": 200\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgencyController.php",
    "groupTitle": "Agency"
  },
  {
    "type": "get",
    "url": "api/web/v1/agency/view?id=14&lat=30.741482&lng=76.768066",
    "title": "Show list of offices addresses",
    "name": "ShowAddressAgencies",
    "version": "0.1.1",
    "group": "Agency",
    "description": "<p>To show agencies addresses with address and disatnce from the current location, Form-Data must be x-www-form-urlencoded</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n     \"items\": [\n         {\n             \"id\": \"1\",\n             \"name\": \"Abercromby’s14\",\n             \"email\": \"aastha4@gmail.com\",\n             \"address\": \" sco 206 - 207, sector 34 a, chandigarh - 160022\",\n             \"latitude\": \"30.725890000000000\",\n             \"longitude\": \"76.757870000000000\",\n             \"distance\": \"1.988860491131707\"\n        },\n        {\n             \"id\": \"2\",\n             \"name\": \"Abercromby’s14\",\n             \"email\": \"aastha4@gmail.com\",\n             \"address\": \"sco 72 a & 73 a sector 26 grain market chandigarh pin-160019\",\n             \"latitude\": \"30.722670000000000\",\n             \"longitude\": \"76.798250000000000\",\n             \"distance\": \"3.5635191814253315\"\n        }\n      ],\n       \"_links\": {\n           \"self\": {\n                 \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/agency/view?id=14&lat=30.741482&lng=76.768066&page=1\"\n          }\n       },\n   \"_meta\": {\n      \"totalCount\": 2,\n      \"pageCount\": 1,\n      \"currentPage\": 1,\n      \"perPage\": 20\n     }\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgencyController.php",
    "groupTitle": "Agency"
  },
  {
    "type": "get",
    "url": "api/web/v1/agency/view-detail?id=1",
    "title": "View detail for office address",
    "name": "ShowAddressdetail",
    "version": "0.1.1",
    "group": "Agency",
    "description": "<p>To show agencies addresses with address and disatnce from the current location, Form-Data must be x-www-form-urlencoded</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n       \"name\": \"Abercromby’s14\",\n       \"logo\": \"http://localhost/AlphaWallet/html/frontend/web/agencylogo/Abercromby’s14882.jpg\",\n       \"email\": \"aastha4@gmail.com\",\n       \"mobile\": 1234569874,\n       \"address\": \" sco 206 - 207, sector 34 a, chandigarh - 160022\",\n       \"latitude\": \"30.725890000000000\",\n       \"longitude\": \"76.757870000000000\"\n   },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgencyController.php",
    "groupTitle": "Agency"
  },
  {
    "type": "get",
    "url": "api/web/v1/agency/agency-profile?id=8",
    "title": "View Agency profile",
    "name": "ShowAgencydetail",
    "version": "0.1.1",
    "group": "Agency",
    "description": "<p>To show agencies details, Form-Data must be x-www-form-urlencoded</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n   {\n       \"message\": \"\",\n\t      \"data\": {\n\t\t\t  \"status\": 10,\n\t\t\t  \"id\": 8,\n\t\t\t  \"name\": \"Biggin & Scott\",\n\t\t\t  \"email\": null,\n\t\t\t  \"mobile\": null,\n\t\t\t  \"logo\": \"http://localhost/AlphaWallet/html/frontend/web/agencylogo/Biggin & Scott389.jpg\",\n\t\t\t  \"backgroungImage\": null,\n\t\t\t  \"address\": \"[\\\"sco no2917 18,sector 22 c,chandigarh 160022\\\",\\\"SCO- 1473, 1473, 43B, Sector 43, Chandigarh, 160047, India\\\"]\",\n\t\t\t  \"latitude\": \"[\\\"30.72811\\\",\\\"30.719059\\\"]\",\n\t\t\t  \"longitude\": \"[\\\"76.77065\\\",\\\"76.748704\\\"]\",\n\t\t\t  \"about_us\": \"Gross Waddell Pty Ltd is a\\ncommercial real estate agency,\\noperating for over 20 years in the\\nretail, office, industrial and\\ndevelopment property sectors. We\\nprovide Sales and Leasing, Asset\\nManagement\\nand\\nCorporate\\nServices to Vendors, Landlords and\\nInvestors.\",\n\t\t\t  \"created_at\": 1524027072,\n\t\t\t  \"updated_at\": 1524027072,\n\t\t\t  \"is_favourite\": 1,\n\t\t\t  \"facebook\": \"https://www.facebook.com/\",\n\t\t\t  \"twitter\": \"https://twitter.com/\",\n\t\t\t  \"instagram\": \"https://www.instagram.com/\"\n       },\n\t      \"status\": 200\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgencyController.php",
    "groupTitle": "Agency"
  },
  {
    "type": "post",
    "url": "/api/web/v1/webhooks/agency/add-agency",
    "title": "Add an Agency",
    "name": "AddAgency",
    "version": "0.1.1",
    "group": "Agency_Webhooks",
    "description": "<p>To add an Agency (Webhook), Form-Data must be of raw type</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "RAW Data\n{\n  \"data\": {\n    \"cv_agency_id\": \"CV-82AA85\",\n    \"name\": \"Offsice_titled\",\n    \"address\": \" sco 206 - 207, sector 34 a, chandigarh - 160022\",\n    \"logo\": \"logo_192x168.jpg\",\n    \"backgroungImage\" : \"bgimage_192x168.jpg\",\n    \"latitude\": \"30.72589\",\n    \"longitude\": \"76.75787\",\n    \"email\": \"usesr_testingd@yopmail.com\",\n    \"mobile\": \"9468594658\",\n    \"about_us\": \"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": \"35\",\n    \"cv_agency_id\": \"CV-82AA85\",\n    \"name\": \"Office_name\",\n    \"address\": \" sco 206 - 207, sector 34 a, chandigarh - 160022\",\n    \"logo\": \"logo_192x1x68.jpg\",\n    \"backgroungImage\" : \"bgimage_192x168.jpg\",\n    \"latitude\": \"30.72589\",\n    \"longitude\": \"76.75787\",\n    \"email\": \"usesr_123@yopmail.com\",\n    \"mobile\": \"9468594658\",\n    \"about_us\": \"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\",\n    \"created_at\": \"2018-05-21 11:14:14\",\n    \"updated_at\": \"2018-05-21 11:20:36\"*         \n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"name\",\n      \"message\": \"Name \\\"Test office 1\\\" has already been taken.\"\n    },\n    {\n      \"field\": \"email\",\n      \"message\": \"Email \\\"testuser@yopmail.com\\\" has already been taken.\"\n    }       \n  ],\n  \"status\": 422\n}\nOR\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"name\",\n      \"message\": \"Name cannot be blank.\"\n    },\n    {\n      \"field\": \"mobile\",\n      \"message\": \"Mobile cannot be blank.\"\n    },\n    {\n      \"field\": \"email\",\n      \"message\": \"Email cannot be blank.\"\n    },\n    {\n      \"field\": \"cv_agency_id\",\n      \"message\": \"cv_agency_id cannot be blank.\"\n    },\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AgencyController.php",
    "groupTitle": "Agency_Webhooks"
  },
  {
    "type": "get",
    "url": "api/web/v1/webhooks/agency/all",
    "title": "Agency Listing",
    "name": "AgencyListing",
    "version": "0.1.1",
    "group": "Agency_Webhooks",
    "description": "<p>List all Agency, Result Set is in Pagination of 10 records in each page. To fetch resultset in pagination url would like this: api/web/v1/webhooks/agency/all?page=2, Where page is the page number.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": {\n    \"items\": [\n      {\n        \"status\": 10,\n        \"id\": 33,\n        \"name\": \"Offsice_dtitled\",\n        \"email\": \"usesr_testdingd@yopmail.com\",\n        \"mobile\": 9468594658,\n        \"logo\": \"http://localhost/AlphaWallet/html/frontend/web/agencylogo/logo_192x168.jpg\",\n        \"backgroungImage\": null,\n        \"address\": \" sco 206 - 207, sector 34 a, chandigarh - 160022\",\n        \"latitude\": \"30.72589\",\n        \"longitude\": \"76.75787\",\n        \"about_us\": \"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\",\n        \"created_at\": 1526899067,\n        \"updated_at\": 1526899067,\n        \"is_favourite\": \"\",\n        \"facebook\": \"https://www.facebook.com/\",\n        \"twitter\": \"https://twitter.com/\",\n        \"instagram\": \"https://www.instagram.com/\"\n      },\n      {\n        \"status\": 10,\n        \"id\": 34,\n        \"name\": \"Offsice_sdtitled\",\n        \"email\": \"usesr_testsdingd@yopmail.com\",\n        \"mobile\": 9468594658,\n        \"logo\": \"http://localhost/AlphaWallet/html/frontend/web/agencylogo/logo_192x168.jpg\",\n        \"backgroungImage\": null,\n        \"address\": \" sco 206 - 207, sector 34 a, chandigarh - 160022\",\n        \"latitude\": \"30.72589\",\n        \"longitude\": \"76.75787\",\n        \"about_us\": \"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\",\n        \"created_at\": 1526899107,\n        \"updated_at\": 1526899107,\n        \"is_favourite\": \"\",\n        \"facebook\": \"https://www.facebook.com/\",\n        \"twitter\": \"https://twitter.com/\",\n        \"instagram\": \"https://www.instagram.com/\"\n      },\n    ],  \n    \"_links\": {\n      \"self\": {\n        \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/webhooks/agency/all?page=2&per-page=10\"\n      },\n      \"first\": {\n        \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/webhooks/agency/all?page=1&per-page=10\"\n      },\n      \"prev\": {\n        \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/webhooks/agency/all?page=1&per-page=10\"\n      }\n    },\n    \"_meta\": {\n      \"totalCount\": 17,\n      \"pageCount\": 2,\n      \"currentPage\": 2,\n      \"perPage\": 10\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AgencyController.php",
    "groupTitle": "Agency_Webhooks"
  },
  {
    "type": "post",
    "url": "api/web/v1/webhooks/agency/delete-agency",
    "title": "Delete an Agency",
    "name": "DeleteAgency",
    "version": "0.1.1",
    "group": "Agency_Webhooks",
    "description": "<p>Delete an Agency (Webhook). One must requires login|access token to perform the action, Form-Data must be of raw type</p> <h4><code>&quot;Please take care to mention Agency id as ID&quot;</code></h4>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"ID\": 12\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"Agency has been deleted successfully\",\n  \"data\":{\n    \"status\": \"success\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response in case of invalid / expired Access token:",
          "content": "HTTP/1.1 401 Invalid credentials\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response in case of record not found:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"No Record Found\",\n  \"data\":{\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AgencyController.php",
    "groupTitle": "Agency_Webhooks"
  },
  {
    "type": "post",
    "url": "/api/web/v1/webhooks/agency/update-agency",
    "title": "Update an Agency",
    "name": "UpdateAgency",
    "version": "0.1.1",
    "group": "Agency_Webhooks",
    "description": "<p>To uodate an Agency (Webhook), Form-Data must be of raw type</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"cv_agency_id\": \"CV-82AA85\",\n    \"name\": \"New Name\",\n    \"address\": \"Sco 115-118, sector 17 A, chandigarh - 160022\",\n    \"logo\": \"logo_192x168.jpg\",\n    \"backgroungImage\" : \"bgimage_192x168.jpg\",\n    \"latitude\": \"30.72589\",\n    \"longitude\": \"76.75787\",\n    \"email\": \"usesr_testingd@yopmail.com\",\n    \"mobile\": \"9468594658\",\n    \"about_us\": \"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"name\": \"New Name\",\n    \"address\": \"Sco 115-118, sector 17 A, chandigarh - 160022\",\n    \"logo\": \"logo_192x1x68.jpg\",\n    \"backgroungImage\" : \"bgimage_192x168.jpg\",\n    \"latitude\": \"30.72589\",\n    \"longitude\": \"76.75787\",\n    \"email\": \"usesr_123@yopmail.com\",\n    \"mobile\": \"9468594658\",\n    \"about_us\": \"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\",\n    \"created_at\": \"2018-05-21 11:14:14\",\n    \"updated_at\": \"2018-05-21 11:20:36\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"id\",\n      \"message\": \"id cannot be blank.\"\n    },\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AgencyController.php",
    "groupTitle": "Agency_Webhooks"
  },
  {
    "type": "post",
    "url": "api/web/v1/user-agent/forgot",
    "title": "Agent Forgot Password",
    "name": "Agent_Forgot_Password",
    "version": "0.1.1",
    "group": "Agent",
    "description": "<p>To recover forgotten password of a user-agent, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>UserAgent's email.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "app_id",
            "description": "<p>'2' for CVIEW</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n           {\n           }\n       ],\n       \"status\": 422\n   }\nOr\n   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n      ],\n      \"status\": 422\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserAgentController.php",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "api/web/v1/user-agent/login",
    "title": "Agent Login",
    "name": "Agent_Login",
    "version": "0.1.1",
    "group": "Agent",
    "description": "<p>To login an agent, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's username.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device",
            "description": "<p>'ios|android|web'.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_token",
            "description": "<p>Random String token 'assss2mlkamladsasd289'.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "app_id",
            "description": "<p>'2' to force user login as bidder</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Lastname of the User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\tHTTP/1.1 200 OK\n {\n     \"message\": \"\",\n     \"data\": {\n         \"login\": {\n             \"status\": 10,\n             \"user_type\": 10,\n             \"app_id\": \"2\",\n             \"id\": 449,\n             \"username\": \"john.doe.agent\",\n             \"email\": \"john.doe.agent@graycelltech.com\",\n             \"auth_key\": \"dld8JhMCAcFY2blg-IdlUxPLeoDqn82Z\",\n             \"firstName\": \"john\",\n             \"lastName\": \"doe.agent\",\n             \"fullName\": \"john doe.agent\",\n             \"orgName\": null,\n             \"fbidentifier\": null,\n             \"linkedin_identifier\": null,\n             \"google_identifier\": null,\n             \"role_id\": null,\n             \"password_hash\": \"$2y$13$qkqQ63QkWshQaCHiFgp0L.oNm7MvaluzmcFgDVLVM/6CS7euDYDDy\",\n             \"password_reset_token\": null,\n             \"phone\": null,\n             \"image\": null,\n             \"sex\": null,\n             \"dob\": \"1993-05-24\",\n             \"ageGroup_id\": 1,\n             \"bdayOffer\": 0,\n             \"walletPinCode\": null,\n             \"loyaltyPin\": null,\n             \"advertister_id\": null,\n             \"activationKey\": null,\n             \"confirmationKey\": null,\n             \"access_token\": null,\n             \"hashKey\": null,\n             \"device\": \"android\",\n             \"device_token\": \"\",\n             \"lastLogin\": 1527766400,\n             \"latitude\": null,\n             \"longitude\": null,\n             \"timezone\": null,\n             \"timezone_offset\": null,\n             \"created_at\": 1527765671,\n             \"updated_at\": 1527766400,\n             \"storeid\": null,\n             \"promise_uid\": \"4495b0fdaaf46a28\",\n             \"promise_acid\": null,\n             \"user_code\": \"AW-john.doe.agent\",\n             \"referralCode\": null,\n             \"referral_percentage\": null,\n             \"agency_id\": null,\n             \"logo\": \"\"\n         },\n         \"streaming_server_info\": {\n             \"host\": \"XXXX.XXXX.XXXX.XXXX\",\n             \"port\": \"XXXX\",\n             \"application\": \"app_name\",\n             \"source\": {\n                 \"username\": \"username\",\n                 \"password\": \"password\"\n             }\n         }\n     },\n     \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "Or\n\tHTTP/1.1 422 Unprocessable entity\n\t{\n\t\t\"message\": \"\",\n\t\t\"data\": [\n\t\t\t{\n\t\t\t\t\"field\": \"password\",\n\t\t\t\t\"message\": \"That  Pin does not match the registered email address. Please try again or click on Forgot  Pin? to reset it.\"\n\t\t\t}\n\t\t],\n\t\t\"status\": 422\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserAgentController.php",
    "groupTitle": "Agent"
  },
  {
    "type": "get",
    "url": "api/web/v1/bidding/agent/change-auction-bidding-status",
    "title": "Change Auction type (Online Bidding, Offline Bidding, Live Streaming)",
    "name": "Change_Auction_Bidding_Status",
    "version": "0.1.1",
    "group": "Agent",
    "description": "<p>To update Auction Bidding Status, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>for which you you want to update offsite address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "bidding_status",
            "description": "<p>To update Auction Bidding Status (1 =&gt; No Online Bidding, 2 =&gt; Online Bidding, 3 =&gt; Offsite bidding)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 50,\n    \"listing_id\": 37,\n    \"auction_type\": null,\n    \"estate_agent_id\": 381,\n    \"agency_id\": 60,\n    \"date\": 1525996800,\n    \"time\": \"11:00:00\",\n    \"datetime_start\": 1526036400,\n    \"datetime_expire\": null,\n    \"starting_price\": null,\n    \"auction_currency_id\": 0,\n    \"home_phone_number\": null,\n    \"estate_agent\": null,\n    \"picture_of_listing\": null,\n    \"duration\": null,\n    \"real_estate_active_himself\": null,\n    \"real_estate_active_bidders\": null,\n    \"reference_relevant_electronic\": null,\n    \"reference_relevant_bidder\": null,\n    \"created_at\": 1525341805,\n    \"auction_status\": 3,\n    \"streaming_url\": null,\n    \"completed_streaming_url\": \"\",\n    \"streaming_status\": null,\n    \"templateId\": \"\",\n    \"envelopeId\": \"\",\n    \"documentId\": \"\",\n    \"document\": \"\",\n    \"recipientId\": \"\",\n    \"bidding_status\": \"3\",\n    \"bidding_pause_status\": 0,\n    \"auction_bidding_status\": 0,\n    \"updated_at\": 1528122709,\n    \"created_by\": 0,\n    \"updated_by\": 0,\n    \"agent\": {\n      \"status\": 10,\n      \"user_type\": 10,\n      \"app_id\": \"2\",\n      \"id\": 381,\n      \"username\": \"agent.user\",\n      \"email\": \"agent.user@yopmail.com\",\n      \"auth_key\": \"aEZIaVu2u-HO1qg1hElbjN2oyP7q6TBS\",\n      \"firstName\": \"test_fname\",\n      \"lastName\": \"test_lname\",\n      \"fullName\": \"test_fname test_lname\",\n      \"orgName\": null,\n      \"fbidentifier\": null,\n      \"linkedin_identifier\": null,\n      \"google_identifier\": null,\n      \"role_id\": null,\n      \"password_hash\": \"$2y$13$qkqQ63QkWshQaCHiFgp0L.oNm7MvaluzmcFgDVLVM/6CS7euDYDDy\",\n      \"password_reset_token\": null,\n      \"phone\": null,\n      \"image\": null,\n      \"sex\": null,\n      \"dob\": \"1989-05-25\",\n      \"ageGroup_id\": 1,\n      \"bdayOffer\": 0,\n      \"walletPinCode\": null,\n      \"loyaltyPin\": null,\n      \"advertister_id\": null,\n      \"activationKey\": null,\n      \"confirmationKey\": null,\n      \"access_token\": null,\n      \"hashKey\": null,\n      \"device\": \"android\",\n      \"device_token\": \"\",\n      \"lastLogin\": 1528120518,\n      \"latitude\": null,\n      \"longitude\": null,\n      \"timezone\": null,\n      \"timezone_offset\": null,\n      \"created_at\": 1526541364,\n      \"updated_at\": 1528120518,\n      \"storeid\": null,\n      \"promise_uid\": \"3815b10f2fe811eb\",\n      \"promise_acid\": null,\n      \"user_code\": \"AW-testasname\",\n      \"referralCode\": null,\n      \"referral_percentage\": null,\n      \"agency_id\": 60,\n      \"logo\": \"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png\"\n    },\n    \"agency\": {\n      \"status\": 10,\n      \"id\": 60,\n      \"cv_agency_id\": \"CV-B4CB59\",\n      \"name\": \"Paul Bell Real Estate \",\n      \"email\": \"paul@paulbellrealestate.com.au\",\n      \"mobile\": 448772355,\n      \"fax\": null,\n      \"logo\": \"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png\",\n      \"backgroungImage\": null,\n      \"address\": null,\n      \"street_address\": null,\n      \"full_address\": null,\n      \"suburb_id\": null,\n      \"latitude\": null,\n      \"longitude\": null,\n      \"about_us\": null,\n      \"slug\": \"paul-bell-real-estate-highton\",\n      \"abn\": \"\",\n      \"created_at\": 1527749074,\n      \"updated_at\": 1527749074,\n      \"web\": null,\n      \"twitter\": \"https://twitter.com/\",\n      \"facebook\": \"https://www.facebook.com/\",\n      \"is_favourite\": \"\",\n      \"instagram\": \"https://www.instagram.com/\",\n      \"agency_agents\": [\n        {\n          \"status\": 10,\n          \"user_type\": 10,\n          \"app_id\": \"2\",\n          \"id\": 381,\n          \"username\": \"agent.user\",\n          \"email\": \"agent.user@yopmail.com\",\n          \"auth_key\": \"aEZIaVu2u-HO1qg1hElbjN2oyP7q6TBS\",\n          \"firstName\": \"test_fname\",\n          \"lastName\": \"test_lname\",\n          \"fullName\": \"test_fname test_lname\",\n          \"orgName\": null,\n          \"fbidentifier\": null,\n          \"linkedin_identifier\": null,\n          \"google_identifier\": null,\n          \"role_id\": null,\n          \"password_hash\": \"$2y$13$qkqQ63QkWshQaCHiFgp0L.oNm7MvaluzmcFgDVLVM/6CS7euDYDDy\",\n          \"password_reset_token\": null,\n          \"phone\": null,\n          \"image\": null,\n          \"sex\": null,\n          \"dob\": \"1989-05-25\",\n          \"ageGroup_id\": 1,\n          \"bdayOffer\": 0,\n          \"walletPinCode\": null,\n          \"loyaltyPin\": null,\n          \"advertister_id\": null,\n          \"activationKey\": null,\n          \"confirmationKey\": null,\n          \"access_token\": null,\n          \"hashKey\": null,\n          \"device\": \"android\",\n          \"device_token\": \"\",\n          \"lastLogin\": 1528120518,\n          \"latitude\": null,\n          \"longitude\": null,\n          \"timezone\": null,\n          \"timezone_offset\": null,\n          \"created_at\": 1526541364,\n          \"updated_at\": 1528120518,\n          \"storeid\": null,\n          \"promise_uid\": \"3815b10f2fe811eb\",\n          \"promise_acid\": null,\n          \"user_code\": \"AW-testasname\",\n          \"referralCode\": null,\n          \"referral_percentage\": null,\n          \"agency_id\": 60,\n          \"logo\": \"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png\"\n        },\n        {\n          \"status\": 10,\n          \"user_type\": 10,\n          \"app_id\": \"2\",\n          \"id\": 431,\n          \"username\": \"paul_640\",\n          \"email\": \"eb160e46@commercialview.com.au\",\n          \"auth_key\": \"01N637_7-BpeWaSfR57m3AC-ppTTViwu\",\n          \"firstName\": \"Paul\",\n          \"lastName\": \"Bell\",\n          \"fullName\": \"Paul Bell\",\n          \"orgName\": null,\n          \"fbidentifier\": null,\n          \"linkedin_identifier\": null,\n          \"google_identifier\": null,\n          \"role_id\": null,\n          \"password_hash\": \"\",\n          \"password_reset_token\": null,\n          \"phone\": 448772355,\n          \"image\": null,\n          \"sex\": null,\n          \"dob\": \"1970-01-01\",\n          \"ageGroup_id\": 1,\n          \"bdayOffer\": 0,\n          \"walletPinCode\": null,\n          \"loyaltyPin\": null,\n          \"advertister_id\": null,\n          \"activationKey\": null,\n          \"confirmationKey\": null,\n          \"access_token\": null,\n          \"hashKey\": null,\n          \"device\": \"web\",\n          \"device_token\": \"\",\n          \"lastLogin\": null,\n          \"latitude\": null,\n          \"longitude\": null,\n          \"timezone\": null,\n          \"timezone_offset\": null,\n          \"created_at\": 1527749074,\n          \"updated_at\": 1527749074,\n          \"storeid\": null,\n          \"promise_uid\": null,\n          \"promise_acid\": null,\n          \"user_code\": \"AW-paul_640\",\n          \"referralCode\": null,\n          \"referral_percentage\": null,\n          \"agency_id\": 60,\n          \"logo\": \"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png\"\n        }\n      ]\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error No Record Found\\",
          "content": "HTTP/1.1 422 Unprocessable entity\t  \t  \n{\n  \"message\": \"No Record Found\",\n  \"data\": {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgentController.php",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "/api/web/v1/users/create-agent",
    "title": "Create a new Agent",
    "name": "CreateUser",
    "version": "0.1.1",
    "group": "Agent",
    "description": "<p>To create new agents, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstName",
            "description": "<p>User's firstname.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastName",
            "description": "<p>User's lastname.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's username.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "agency_id",
            "description": "<p>Agency Id get from the Agency Table.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "app_id",
            "description": "<p>(Optional) '2' to force user registration as agent and for access from CVIEW APP</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_type",
            "description": "<p>(Optional) '10' to force user registration as agent</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Lastname of the User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"firstName\": \"John\",\n    \"lastName\": \"Doe\",\n    \"email\": \"john_doe@mail.com\",\n    \"username\": \"john_hoe\",\n    \"agency_id\": \"1\",\n    \"auth_key\": \"AHSGAWERQWSAFRGEQw2Q21-A2W4G_13S\",\n    \"fullName\": \"john doe\",\n    \"user_code\": \"XX-john_doe\",\n    \"id\": 12,\n    \"promise_uid\": \"3145ac4b4251fd43\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 422 Validation Error\n  {\n    \"message\": \"\",\n    \"data\": [\n      {\n        \"field\": \"username\",\n        \"message\": \"The username \\'john_doe\\' has already been taken. The following usernames are available: \\'john_doe, john_doe.600, john_doe_110, john.doe, johnddoe\\'. \"\n      },\n      {\n        \"field\": \"email\",\n        \"message\": \"Email john_doe@mail.com has already been registered. Please enter a valid email address.\"\n      }\n    ],\n    \"status\": 422\n  }\nOr\n HTTP/1.1 422 Validation Error\n {\n     \"message\": \"\",\n     \"data\": [\n      {\n         \"field\": \"firstName\",\n         \"message\": \"First Name cannot be blank.\"\n      },\n      {\n         \"field\": \"lastName\",\n         \"message\": \"Last Name cannot be blank.\"\n      },\n      {\n         \"field\": \"agency_id\",\n         \"message\": \"Agency Id cannot be blank.\"\n      },\n      {\n         \"field\": \"dob\",\n         \"message\": \"Date Of Birth cannot be blank.\"\n      },\n      {\n         \"field\": \"username\",\n         \"message\": \"Username in here!! cannot be blank.\"\n      },\n      {\n         \"field\": \"email\",\n         \"message\": \"Email cannot be blank.\"\n      }\n    ],\n    \"status\": 422\n }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/email-agent",
    "title": "Email an agent",
    "name": "EmailAgent",
    "version": "0.1.1",
    "group": "Agent",
    "description": "<p>To email an agent, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "property_id",
            "description": "<p>Unique property id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "enquiry_name",
            "description": "<p>Sender's name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "enquiry_email",
            "description": "<p>Sender's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "enquiry_phone",
            "description": "<p>Sender's phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "enquiry_message",
            "description": "<p>Sender's message</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "enquiry_i_would_like_to[]",
            "description": "<p>I would like selections</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "contact_me",
            "description": "<p>1=&gt;Yes, 0=&gt;No</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"message\": \"\",\n      \"data\":  {\n\t\t},\n      \"status\": 200\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "api/web/v1/user-agent/logout",
    "title": "Logout",
    "name": "Logout",
    "version": "0.1.1",
    "group": "Agent",
    "description": "<p>To logout an agent, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>(required) The access-token of the logged-in agent //If valid access-token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:  ",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": \"Logout Successfully\",\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n    \"message\": \"\",\n    \"data\": [\n        \"Please provide a validate user token\"\n    ],\n    \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserAgentController.php",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/agent-list?agency_id=1",
    "title": "Show list of Agent",
    "name": "ShowAgents",
    "version": "0.1.1",
    "group": "Agent",
    "description": "<p>To show agents, Form-Data must be x-www-form-urlencoded</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\":  {\n    \"items\": [\n      { \n        \"firstName\": \"John\",\n        \"lastName\": \"doe\",\n        \"email\": \"john@gmail.com\",\n        \"fullName\": \"John Doe\",\n        \"auth_key\": \"wBhrAI7uD-y1Ow5xrmZ5EyFdJJwE4IOe\",\n        \"agency_id\": \"10\",\n        \"logo\": \"Abercromby’s819.jpg\"\n      },\n      {\n        \"firstName\": \"John\",\n        \"lastName\": \"doe1\",\n        \"email\": \"john1@gmail.com\",\n        \"fullName\": \"john doe1\",\n        \"auth_key\": \"41oBK-OHkauNIq5b8jyQlF4jcZPTsXT_\",\n        \"agency_id\": \"10\",\n        \"logo\": \"Abercromby’s819.jpg\"\n      },\n    ],\n    \"_links\": {\n      \"self\": {\n        \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/user/agent-list?agency_id=10&page=1\"\n      }\n    },\n    \"_meta\": {\n      \"totalCount\": 3,\n      \"pageCount\": 1,\n      \"currentPage\": 1,\n      \"perPage\": 20\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "Agent"
  },
  {
    "type": "get",
    "url": "api/web/v1/bidding/agent/save-auction-address",
    "title": "Update Offsite Auction Address",
    "name": "Update_Offsite_Auction_Address",
    "version": "0.1.1",
    "group": "Agent",
    "description": "<p>Update Offsite Auction Address, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>for which you you want to update offsite address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address_line_1",
            "description": "<p>To update address_line_1 text</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address_line_2",
            "description": "<p>To update address_line_2 text</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>To update City</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "state",
            "description": "<p>To update State</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pincode",
            "description": "<p>To update Pincode</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country",
            "description": "<p>To update Country</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>To update Mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>To update Email</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 3,\n    \"auction_event_id\": \"50\",\n    \"address_line_1\": \"address_line_1 text\",\n    \"address_line_2\": \"address_line_2 text\",\n    \"city\": \"Auction City\",\n    \"state\": \"Auction State\",\n    \"pincode\": \"123456\",\n    \"country\": \"15\",\n    \"mobile\": \"9569635775\",\n    \"email\": \"info.offauction@yopmail.com\",\n    \"auction_start_time\": \"2018-05-11 11:00:00\",\n    \"created_at\": \"2018-06-04\",\n    \"updated_at\": 1528118518,\n    \"country_name\": \"Azerbaijan\",\n    \"auction_detail\": {\n      \"id\": 50,\n      \"listing_id\": 37,\n      \"auction_type\": null,\n      \"estate_agent_id\": 381,\n      \"agency_id\": 0,\n      \"date\": \"2018-05-11\",\n      \"time\": \"11:00:00\",\n      \"datetime_start\": \"2018-05-11 11:00:00\",\n      \"datetime_expire\": null,\n      \"starting_price\": null,\n      \"auction_currency_id\": 0,\n      \"home_phone_number\": null,\n      \"estate_agent\": null,\n      \"picture_of_listing\": null,\n      \"duration\": null,\n      \"real_estate_active_himself\": null,\n      \"real_estate_active_bidders\": null,\n      \"reference_relevant_electronic\": null,\n      \"reference_relevant_bidder\": null,\n      \"created_at\": 1525341805,\n      \"auction_status\": 3,\n      \"streaming_url\": null,\n      \"completed_streaming_url\": \"\",\n      \"streaming_status\": null,\n      \"templateId\": \"\",\n      \"envelopeId\": \"\",\n      \"documentId\": \"\",\n      \"document\": \"\",\n      \"recipientId\": \"\",\n      \"bidding_status\": 2,\n      \"bidding_pause_status\": 0,\n      \"auction_bidding_status\": 0,\n      \"updated_at\": \"2018-06-04\",\n      \"created_by\": 0,\n      \"updated_by\": 0,\n      \"agent\": {\n        \"status\": 10,\n        \"user_type\": 10,\n        \"app_id\": \"2\",\n        \"id\": 381,\n        \"username\": \"agent.user\",\n        \"email\": \"agent.user@yopmail.com\",\n        \"auth_key\": \"cfxxtq6L_YyhvnIMaR5nicqDC9yCMFF5\",\n        \"firstName\": \"test_fname\",\n        \"lastName\": \"test_lname\",\n        \"fullName\": \"test_fname test_lname\",\n        \"orgName\": null,\n        \"fbidentifier\": null,\n        \"linkedin_identifier\": null,\n        \"google_identifier\": null,\n        \"role_id\": null,\n        \"password_hash\": \"$2y$13$qkqQ63QkWshQaCHiFgp0L.oNm7MvaluzmcFgDVLVM/6CS7euDYDDy\",\n        \"password_reset_token\": null,\n        \"phone\": null,\n        \"image\": null,\n        \"sex\": null,\n        \"dob\": \"1989-05-25\",\n        \"ageGroup_id\": 1,\n        \"bdayOffer\": 0,\n        \"walletPinCode\": null,\n        \"loyaltyPin\": null,\n        \"advertister_id\": null,\n        \"activationKey\": null,\n        \"confirmationKey\": null,\n        \"access_token\": null,\n        \"hashKey\": null,\n        \"device\": \"android\",\n        \"device_token\": \"\",\n        \"lastLogin\": 1528109492,\n        \"latitude\": null,\n        \"longitude\": null,\n        \"timezone\": null,\n        \"timezone_offset\": null,\n        \"created_at\": 1526541364,\n        \"updated_at\": 1528109493,\n        \"storeid\": null,\n        \"promise_uid\": \"3815b10f2fe811eb\",\n        \"promise_acid\": null,\n        \"user_code\": \"AW-testasname\",\n        \"referralCode\": null,\n        \"referral_percentage\": null,\n        \"agency_id\": 11,\n        \"logo\": \"\"\n      },\n      \"agency\": null\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error No Record Found",
          "content": "HTTP/1.1 422 Unprocessable entity\t  \t  \n{\n  \"message\": \"No Record Found\",\n  \"data\": {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgentController.php",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "/api/web/v1/webhooks/users/add-agent",
    "title": "Add an Agent",
    "name": "AddAgent",
    "version": "0.1.1",
    "group": "Agent_Webhooks",
    "description": "<p>To add an Agent (Webhook), Form-Data must be of raw type</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"firstName\": \"test_fname\",\n    \"lastName\": \"test_lname\",\n    \"username\": \"test_username\",\n    \"password_hash\": \"generated password in hash format\",\n    \"email\": \"abc@yourdomain.com\",\n    \"agency_id\": 11,\n    \"dob\": \"1995-05-15\",\n    \"passport_number\": \"UPN_15896542\",\n    \"driver_license_number\": \"DLN-15896/2004-05\",\n    \"companyName\" : \"companyNameasd\",\n    \"about\" : \"We are thought leaders in the franchise industry shaping the processes, the systems and the people that have developed a large number of the leading franchise networks in Australia, many of which we’ve taken to the world.\\r\\n\\r\\nThe foundations of DC Strategy were laid in 1983 with the purpose of helping entrepreneurs from start-ups to medium enterprises to large corporations, grow their businesses through franchising.\",\n    \"preferred_contact_number\" : \"9569635775\",\n    \"licence_number\" : \"CV-13234\",\n    \"awards\" : \"Arwards Description\",\n    \"specialties\" : \"specialties Description\",\n    \"members\" : \"['CIAB']\",\n    \"accreditation\" : \"accreditation Text\",\n    \"languages\" : \"English, French, Spanish, Turkey, Dutch, Russian\"    \n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 405,\n    \"username\": \"test_username\",\n    \"email\": \"abc@yourdomain.com\",\n    \"firstName\": null,\n    \"lastName\": null,\n    \"fullName\": \"\",\n    \"dob\": \"1989-05-25\",\n    \"user_tpe\": 10,\n    \"app_id\": 2,\n    \"status\": 10,\n    \"agency_id\": null,\n    \"passport_number\": \"UPN_15896542\",\n    \"driver_license_number\": \"DLN-15896/2004-05\",\n    \"companyName\": \"companyNameasd\",\n    \"preferred_contact_number\": \"9569635775\",\n    \"about\": \"We are thought leaders in the franchise industry shaping the processes, the systems and the people that have developed a large number of the leading franchise networks in Australia, many of which we’ve taken to the world.\\r\\n\\r\\nThe foundations of DC Strategy were laid in 1983 with the purpose of helping entrepreneurs from start-ups to medium enterprises to large corporations, grow their businesses through franchising.\",\n    \"licence_number\": \"CV-13234\",\n    \"awards\": \"Arwards Description\",\n    \"specialties\": \"specialties Description\",\n    \"members\": \"['CIAB']\",\n    \"accreditation\": \"English, French, Spanish, Turkey, Dutch, Russian\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"username\",\n      \"message\": \"The username \\\"test_username\\\" has already been taken. The following usernames are available: \\\"test_fname_141, test_lname.929, test_username_786, test_fname.test_lname, test_fnametest_lname\\\". \"\n    },\n    {\n      \"field\": \"email\",\n      \"message\": \"Email abc@yourdomain.com has already been registered. Please enter a valid email address.\"\n    }\n  ],\n \"status\": 422\n}\nOr\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n       \"field\": \"username\",\n       \"message\": \"Username in here!! cannot be blank.\"\n    },\n    {\n       \"field\": \"email\",\n       \"message\": \"Email cannot be blank.\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/UserController.php",
    "groupTitle": "Agent_Webhooks"
  },
  {
    "type": "post",
    "url": "api/web/v1/webhooks/users/delete-agent",
    "title": "Delete an Agent",
    "name": "DeleteAgent",
    "version": "0.1.1",
    "group": "Agent_Webhooks",
    "description": "<p>Delete an Agent (Webhook). One must requires login|access token to perform the action, Form-Data must be of raw type</p> <h4><code>&quot;Please take care to mention Agent id as ID&quot;</code></h4>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"ID\": 12\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"Agent has been deleted successfully\",\n  \"data\":{\n    \"status\": \"success\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response in case of record not found:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"No Record Found\",\n  \"data\":{\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        },
        {
          "title": "Error-Response in case of invalid / expired Access token:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/UserController.php",
    "groupTitle": "Agent_Webhooks"
  },
  {
    "type": "post",
    "url": "api/web/v1/webhooks/user/all-agents",
    "title": "Agent Listing",
    "name": "ShowAgents",
    "version": "0.1.1",
    "group": "Agent_Webhooks",
    "description": "<p>To show agents, Form-Data must be x-www-form-urlencoded</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": {\n    \"items\": [\n      {\n        \"agent_fname\": \"aastha\",\n        \"agent_lname\": \"chawla\",\n        \"agent_email\": \"aastha1@graycelltech.com\",\n        \"agent_fullname\": \"aastha chawla\",\n        \"auth_key\": \"41oBK-OHkauNIq5b8jyQlF4jcZPTsXT_\",\n        \"agency_id\": \"10\",\n        \"id\": \"10\",\n        \"name\": \"Abercromby’s\",\n        \"email\": \"aastha@gmail.com\",\n        \"mobile\": \"1234569874\",\n        \"logo\": \"Abercromby’s819.jpg\",\n        \"backgroungImage\": null,\n        \"address\": \"[\\\" sco 206 - 207, sector 34 a, chandigarh - 160022\\\",\\\"sco 72 a & 73 a sector 26 grain market chandigarh pin-160019\\\"]\",\n        \"latitude\": \"[\\\"30.72589\\\",\\\"30.72267\\\"]\",\n        \"longitude\": \"[\\\"76.75787\\\",\\\"76.79825\\\"]\",\n        \"about_us\": \"Gross Waddell Pty Ltd is a\\ncommercial real estate agency,\\noperating for over 20 years in the\\nretail, office, industrial and\\ndevelopment property sectors. We\\nprovide Sales and Leasing, Asset\\nManagement\\nand\\nCorporate\\nServices to Vendors, Landlords and\\nInvestors.\",\n        \"status\": \"10\",\n        \"created_at\": \"1524034419\",\n        \"updated_at\": \"1524034419\"\n      },\n      {\n        \"agent_fname\": \"aastha\",\n        \"agent_lname\": \"chawla\",\n        \"agent_email\": \"aastha3@graycelltech.com\",\n        \"agent_fullname\": \"aastha chawla\",\n        \"auth_key\": \"ZWuX9J6HhhArfvQYWlNAdsUp678h87gN\",\n        \"agency_id\": \"10\",\n        \"id\": \"10\",\n        \"name\": \"Abercromby’s\",\n        \"email\": \"aastha@gmail.com\",\n        \"mobile\": \"1234569874\",\n        \"logo\": \"Abercromby’s819.jpg\",\n        \"backgroungImage\": null,\n        \"address\": \"[\\\" sco 206 - 207, sector 34 a, chandigarh - 160022\\\",\\\"sco 72 a & 73 a sector 26 grain market chandigarh pin-160019\\\"]\",\n        \"latitude\": \"[\\\"30.72589\\\",\\\"30.72267\\\"]\",\n        \"longitude\": \"[\\\"76.75787\\\",\\\"76.79825\\\"]\",\n        \"about_us\": \"Gross Waddell Pty Ltd is a\\ncommercial real estate agency,\\noperating for over 20 years in the\\nretail, office, industrial and\\ndevelopment property sectors. We\\nprovide Sales and Leasing, Asset\\nManagement\\nand\\nCorporate\\nServices to Vendors, Landlords and\\nInvestors.\",\n        \"status\": \"10\",\n        \"created_at\": \"1524034419\",\n        \"updated_at\": \"1524034419\"\n      },\n      {\n        \"agent_fname\": \"aastha\",\n        \"agent_lname\": \"chawla\",\n        \"agent_email\": \"aastha341@graycelltech.com\",\n        \"agent_fullname\": \"aastha chawla\",\n        \"auth_key\": \"YB0wG3_xbkKIjnwy4-WU0C7tPAIubXSd\",\n        \"agency_id\": \"23\",\n        \"id\": \"23\",\n        \"name\": \"aastha1\",\n        \"email\": \"aastha1@gmail.cpm\",\n        \"mobile\": \"919876543215\",\n        \"logo\": \"aastha1601.jpg\",\n        \"backgroungImage\": null,\n        \"address\": \"[\\\"sco 206 - 207, sector 34 a, chandigarh - 160022\\\"]\",\n        \"latitude\": \"[\\\"30.72589\\\"]\",\n        \"longitude\": \"[\\\"76.75787\\\"]\",\n        \"about_us\": \"Gross Waddell Pty Ltd is a\\ncommercial real estate agency,\\noperating for over 20 years in the\\nretail, office, industrial and\\ndevelopment property sectors. We\\nprovide Sales and Leasing, Asset\\nManagement\\nand\\nCorporate\\nServices to Vendors, Landlords and\\nInvestors.\",\n        \"status\": \"10\",\n        \"created_at\": \"1524059912\",\n        \"updated_at\": \"1524059912\"\n      }\n    ],\n    \"_links\": {\n        \"self\": {\n            \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/webhooks/user/all-agents?page=1\"\n        }\n    },\n    \"_meta\": {\n        \"totalCount\": 3,\n        \"pageCount\": 1,\n        \"currentPage\": 1,\n        \"perPage\": 20\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/UserController.php",
    "groupTitle": "Agent_Webhooks"
  },
  {
    "type": "post",
    "url": "/api/web/v1/webhooks/users/update-agent",
    "title": "Update an Agent",
    "name": "UpdateAgent",
    "version": "0.1.1",
    "group": "Agent_Webhooks",
    "description": "<p>To update an Agent (Webhook), One must requires login|access token to perform the action, Form-Data must be of raw type</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"id\" : 396,\n    \"firstName\": \"test_fname\",\n    \"lastName\": \"test_lname\",\n    \"username\": \"test_username\",\n    \"password_hash\": \"generated password in hash format\",\n    \"email\": \"abc@yourdomain.com\",\n    \"agency_id\": 11,\n    \"dob\": \"1995-05-15\",\n    \"passport_number\": \"UPN_15896542\",\n    \"driver_license_number\": \"DLN-15896/2004-05\",\n    \"companyName\" : \"companyNameasd\",\n    \"about\" : \"We are thought leaders in the franchise industry shaping the processes, the systems and the people that have developed a large number of the leading franchise networks in Australia, many of which we’ve taken to the world.\\r\\n\\r\\nThe foundations of DC Strategy were laid in 1983 with the purpose of helping entrepreneurs from start-ups to medium enterprises to large corporations, grow their businesses through franchising.\",\n    \"preferred_contact_number\" : \"9569635775\",\n    \"licence_number\" : \"CV-13234\",\n    \"awards\" : \"Arwards Description\",\n    \"specialties\" : \"specialties Description\",\n    \"members\" : \"['CIAB']\",\n    \"accreditation\" : \"accreditation Text\",\n    \"languages\" : \"English, French, Spanish, Turkey, Dutch, Russian\"    \n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 396,\n    \"username\": \"test_username\",\n    \"email\": \"abc@yourdomain.com\",\n    \"firstName\": null,\n    \"lastName\": null,\n    \"fullName\": \"\",\n    \"dob\": \"1989-05-25\",\n    \"user_tpe\": 10,\n    \"app_id\": \"2\",\n    \"status\": 10,\n    \"agency_id\": 10,\n    \"passport_number\": \"UPN_15896542\",\n    \"driver_license_number\": \"DLN-15896/2004-05\",\n    \"companyName\": \"companyNameasd\",\n    \"preferred_contact_number\": \"9569635775\",\n    \"about\": \"We are thought leaders in the franchise industry shaping the processes, the systems and the people that have developed a large number of the leading franchise networks in Australia, many of which we’ve taken to the world.\\r\\n\\r\\nThe foundations of DC Strategy were laid in 1983 with the purpose of helping entrepreneurs from start-ups to medium enterprises to large corporations, grow their businesses through franchising.\",\n    \"licence_number\": \"CV-13234\",\n    \"awards\": \"Arwards Description\",\n    \"specialties\": \"specialties Description\",\n    \"members\": \"['CIAB']\",\n    \"accreditation\": \"English, French, Spanish, Turkey, Dutch, Russian\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"id\",\n      \"message\": \"ID cannot be blank.\"\n    }\n  ],\n  \"status\": 422\n}\nOr\nHTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/UserController.php",
    "groupTitle": "Agent_Webhooks"
  },
  {
    "type": "post",
    "url": "/api/web/v1/auction/add-to-calendar?listing_id=2",
    "title": "Rating",
    "name": "AddToCalendar",
    "version": "0.1.1",
    "group": "Auction",
    "description": "<p>Add a listing to calendar, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>User's Id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>Auction Id (Auction Id)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Used For rate to property(in between 1 to 5)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": {\n        \"auction_id\": \"1\",\n        \"is_favourite\": \"0\",\n        \"rating\": \"1\",\n        \"user_id\": \"351\",\n        \"id\": 16,\n        \"user_name\": \"gurpreet.gct\"\n    },\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to use submit correct data to save an rating</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"rating\",\n      \"message\": \"Must be one star\"\n    },\n    {\n      \"field\": \"user_id\",\n    \"message\": \"The user with user_id \\\"351\\\" does not exist\"\n    },\n    {\n       \"field\": \"auction_id\",\n    \"message\": \"The auction with auction \\\"1\\\" and user_id \\\"351\\\" already saved\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AuctionController.php",
    "groupTitle": "Auction"
  },
  {
    "type": "post",
    "url": "api/web/v1/auction-event/start-streaming-auction",
    "title": "Start a Streaming Auction",
    "name": "Start_Streaming_Auction",
    "version": "0.1.1",
    "group": "AuctionEvent",
    "description": "<p>To start a streaming auction. Is called prior to starting the actual stremaing process Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Auction id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"Time to start the auction\",\n    \"data\": {\n        \"status\": true,\n        \"auction\": {\n            \"id\": 24,\n            \"listing_id\": 6,\n            \"auction_type\": null,\n            \"estate_agent_id\": 0,\n            \"agency_id\": 0,\n            \"date\": null,\n            \"time\": null,\n            \"datetime_start\": 1212633531,\n            \"datetime_expire\": null,\n            \"starting_price\": null,\n            \"home_phone_number\": null,\n            \"estate_agent\": null,\n            \"picture_of_listing\": null,\n            \"duration\": null,\n            \"real_estate_active_himself\": null,\n            \"real_estate_active_bidders\": null,\n            \"reference_relevant_electronic\": null,\n            \"reference_relevant_bidder\": null,\n            \"created_at\": 0,\n            \"auction_status\": 2,\n            \"streaming_url\": null,\n            \"completed_streaming_url\": \"\",\n            \"streaming_status\": null,\n            \"templateId\": \"\",\n            \"envelopeId\": \"\",\n            \"documentId\": \"\",\n            \"document\": \"\",\n            \"recipientId\": \"\",\n            \"bidding_status\": 2,\n            \"updated_at\": 1524806263,\n            \"created_by\": 0,\n            \"updated_by\": 0,\n            \"is_favourite\": \"\",\n            \"facebook\": \"https://www.facebook.com/\",\n            \"twitter\": \"https://twitter.com/\",\n            \"instagram\": \"https://www.instagram.com/\",\n            \"agency_agents\": []\n        }\n    },\n    \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Invalid Streaming Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"This auction is not a streaming auction\",\n    \"data\": {\n        \"status\": false\n    },\n    \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Missing id:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"Missing id param\",\n    \"data\": {\n        \"status\": false\n    },\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AuctionEventController.php",
    "groupTitle": "AuctionEvent"
  },
  {
    "type": "post",
    "url": "api/web/v1/auction-event/update-bidding-pause-status",
    "title": "Change Auction Bidding Pause status",
    "name": "Update_auction_bidding_pause_status",
    "version": "0.1.1",
    "group": "AuctionEvent",
    "description": "<p>To udpate bidding status of an auction. Form-Data must be x-www-form-urlencoded.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Auction id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Auction bidding pause status.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"message\": \"Auction pause status updated.\",\n    \"data\": {\n        \"status\": true,\n        \"auction\": {\n            \"id\": 2,\n            \"listing_id\": 3,\n            \"auction_type\": null,\n            \"estate_agent_id\": 381,\n            \"agency_id\": 0,\n            \"date\": 1529469351,\n            \"time\": null,\n            \"datetime_start\": 1529469351,\n            \"datetime_expire\": 1527539432,\n            \"starting_price\": null,\n            \"auction_currency_id\": 3,\n            \"home_phone_number\": null,\n            \"estate_agent\": null,\n            \"picture_of_listing\": null,\n            \"duration\": null,\n            \"real_estate_active_himself\": null,\n            \"real_estate_active_bidders\": null,\n            \"reference_relevant_electronic\": null,\n            \"reference_relevant_bidder\": null,\n            \"created_at\": 0,\n            \"auction_status\": 3,\n            \"streaming_url\": null,\n            \"completed_streaming_url\": \"\",\n            \"streaming_status\": null,\n            \"templateId\": \"\",\n            \"envelopeId\": \"\",\n            \"documentId\": \"\",\n            \"document\": \"\",\n            \"recipientId\": \"\",\n            \"bidding_status\": 2,\n            \"bidding_pause_status\": \"3\",\n            \"updated_at\": 1528111144,\n            \"created_by\": 0,\n            \"updated_by\": 0,\n            \"is_favourite\": \"\",\n            \"facebook\": \"https://www.facebook.com/\",\n            \"twitter\": \"https://twitter.com/\",\n            \"instagram\": \"https://www.instagram.com/\",\n            \"agency_agents\": []\n        }\n    },\n    \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Invalid Auction Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"Invalid auction requested\",\n    \"data\": {\n        \"status\": false\n    },\n    \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Missing id:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"Missing id param\",\n    \"data\": {\n        \"status\": false\n    },\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AuctionEventController.php",
    "groupTitle": "AuctionEvent"
  },
  {
    "type": "get",
    "url": "/api/web/v1/auction/search",
    "title": "List of Save Auction",
    "name": "ListofSaveAuction",
    "version": "0.1.1",
    "group": "Auction",
    "description": "<p>To search or sort for saved Auction</p> <h3>Supported keys for sorting:</h3> <ul> <li><em>price</em> | ascending=&gt;'price', 'descending' =&gt; '-price'</li> <li><em>date</em> | ascending=&gt;'date', 'descending' =&gt; '-date'</li> <li><em>distance</em> | ascending=&gt;'distance', 'descending' =&gt; '-distance'</li> </ul> <h3><code>Note: Append (-) for sorting in descending order</code></h3> <h3>Examples for reference:</h3> <h4><code>/api/web/v1/auction/search?sort=price,-date</code></h4> <h4><code>/api/web/v1/auction/search?property_type=Commercial Farming, Land/Development</code> | <code>property_type=Commercial Farming</code></h4> <h4><code>/api/web/v1/auction/search?from_date=2018-04-01</code> | <code>/api/web/v1/property/search?to_date=2018-04-20</code> | <code>/api/web/v1/property/search?from_date=2018-04-01&amp;to_date=2018-04-20</code></h4> <h4><code>/api/web/v1/auction/search?sale_from=2000</code> | <code>/api/web/v1/property/search?sale_to=5000</code> | <code>/api/web/v1/property/search?sale_from=2000&amp;sale_to=5000</code></h4> <h4><code>/api/web/v1/auction/search?min_land=244</code> | <code>/api/web/v1/property/search?max_land=1200</code> | <code>/api/web/v1/property/search?min_land=230&amp;max_land=2200</code></h4> <h4><code>/api/web/v1/auction/search?min_floor=244</code> | <code>/api/web/v1/property/search?max_floor=1200</code> | <code>/api/web/v1/property/search?min_floor=230&amp;max_floor=2200</code></h4> <h4><code>/api/web/v1/auction/search?auction_only=1</code> To display only auctions</h4> <h4><code>/api/web/v1/auction/search?auction_all=1</code> (Optional) To display all auctions</h4> <h4><code>http://cview.civitastech.com/api/web/v1/auction/search?suburb=&amp;property_type=Commercial Farming, Land/Development, Hotel/Leisure, Offices, Showrooms/Bulky Good&amp;from_date=2018-04-01&amp;to_date=2018-04-20&amp;sale_from=2000&amp;sale_to=5000&amp;min_land=500&amp;max_land=1000&amp;min_floor=200&amp;max_floor=500&amp;auction_only=1&amp;auction_all=0</code></h4> <h4>For location based searching: <code>http://cview.civitastech.com/api/web/v1/auction/search?lat=30.741482&amp;long=76.768066&amp;sort=distance</code>, and for descending <code>http://cview.civitastech.com/api/web/v1/auction/search?lat=30.741482&amp;long=76.768066&amp;sort=distance</code></h4> <h2>Response</h2> <ul> <li>Key =&gt; Value</li> <li><em>data.items.propertyType</em> =&gt; PropertyType Id (Represents types like Commercial Farming, Land/Development, Hotel/Leisure, Industrial/Warehouse, etc.)</li> <li><em>data.items.state</em> =&gt; The corresponding state id from the saved states in DB. Refer to GeoState for getting the state id and names</li> <li><em>data.items.country</em> =&gt; The corresponding country id from the saved countries in DB. Refer to GeoCountry for getting the state id and names</li> <li><em>data.items.auctionDetails.live</em> =&gt; True if the auction is currently live</li> <li><em>data.items.auctionDetails.past</em> =&gt; True if the auction has taken place already.</li> <li><em>data.items.auctionDetails.upcoming</em> =&gt; True is the auction will take place in the future.</li> <li><em>data.items.auctionDetails.seconds</em> =&gt; Integer representing number of seconds left for the auction to start, 0 incase of the auction has already taken place or is live right now.</li> <li><em>data.items.auction.datetime_start</em> =&gt; The auction start date</li> <li><em>data.items.auction.auction_status</em> =&gt; Integer representing the type of auction. i.e. 1 =&gt; auction, 2 =&gt; Streaming, 3 =&gt; Offline</li> <li><em>data.items.saveauction</em> =&gt; Will consist a list of users that have saved that auction.</li> </ul>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "suburb",
            "description": "<p>To Filter by city/state/suburb/postcode</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "property_type",
            "description": "<p>Comma separated set of property types</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sale_from",
            "description": "<p>Min Cost of property</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sale_to",
            "description": "<p>Max Cost of property</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "from_date",
            "description": "<p>Auction Start date, format YYYY-M-D eg. 2018-01-23</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "to_date",
            "description": "<p>Auction End date, format YYYY-M-D eg. 2018-01-23</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "min_land",
            "description": "<p>eg. 2032</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "max_land",
            "description": "<p>eg. 23321</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "min_floor",
            "description": "<p>eg. 323</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "max_floor",
            "description": "<p>eg. 2938</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "auction_only",
            "description": "<p>1=&gt;Display only auction types</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "auction_all",
            "description": "<p>1=&gt; Display all auction types i.e. auction|streaming|offline</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "list",
            "description": "<p>1=&gt;Past Property, 2=&gt;Upcoming Property</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>1- For Sale, 2- For Rent. 3 - For Lease</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>(Optional) Comma separated list of sort keys, append (-) for descending order sort.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lat",
            "description": "<p>User's lat, or lat to be used for location filtering</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "long",
            "description": "<p>User's long, or long to be used for location filtering</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 \n   {\n       \"message\": \"\",\n       \"data\": {\n       \"items\": [\n         {\n          \"id\": 1,\n          \"title\": \"Brand New Office/Warehouse Development\",\n          \"description\": \"Cameron is pleased to offer this brand new Office/ Warehouse complex that consists of 23 units ranging between 240sqm to 609sqm. Offering easy access to major arterial roads including Frankston Dandenong Road, Abbotts Road, South Gippsland Freeway and Eastlink via Fox Drive and Discovery Road.\\n\\nEdison Park, being Dandnenong South's newest Industrial Park development; is situated on the corner of Taylors Road and Edison Road and will provide easy dual drive-thru access. Construction is expected to commence in June 2018.\\n\\nEach warehouse include the following:\\n- Office with carpet tiles, air-conditioning, power points, lighting and load-bearing ceiling\\n- Kitchenette\\n- Toilet amenities with shower\\n- Container height roller shutter door\\n- 3 Phase power\",\n          \"status\": null,\n          \"propertyType\": 4,\n          \"price\": 480000,\n          \"landArea\": null,\n          \"floorArea\": 742,\n          \"conditions\": 0,\n          \"address\": \"4/18 Luisa Avenue, DANDENONG VIC, 3175 \",\n          \"latitude\": \"37.9810\",\n          \"longitude\": \"145.2150\",\n          \"unitNumber\": 0,\n          \"streetNumber\": 0,\n          \"streetName\": null,\n          \"suburb\": null,\n          \"city\": \"Mohali\",\n          \"state\": \"32\",\n          \"postcode\": \"123456\",\n          \"country\": \"13\",\n          \"parking\": 1,\n          \"parkingList\": null,\n          \"parkingOther\": null,\n          \"floorplan\": null,\n          \"media\": null,\n          \"video\": null,\n          \"school\": 1,\n          \"communityCentre\": 1,\n          \"parkLand\": 1,\n          \"publicTransport\": 0,\n          \"shopping\": 1,\n          \"bedrooms\": 3,\n          \"bathrooms\": 4,\n          \"features\": null,\n          \"tags\": null,\n          \"estate_agent_id\": null,\n          \"second_agent_id\": null,\n          \"agency_id\": null,\n          \"created_by\": null,\n          \"propertyStatus\": 0,\n          \"updated_by\": null,\n          \"created_at\": \"2018-04-18 14:02:04\",\n          \"updated_at\": \"2018-04-18 14:02:04\",\n          \"inspection1\": null,\n          \"inspection2\": null,\n          \"inspection3\": null,\n          \"inspection4\": null,\n          \"inspection5\": null,\n          \"inspection6\": null,\n          \"inspection7\": null,\n          \"inspection8\": null,\n          \"is_featured\": 0,\n          \"distance\": \"\",\n          \"propertyTypename\": \"Industrial/Warehouse\",\n          \"auctionDetails\": \n          {\n               \"live\": false,\n               \"past\": true,\n               \"upcoming\": false,\n               \"seconds\": 0\n          },\n          \"gallery\": [],\n          \"auction\": {\n               \"id\": 1,\n               \"listing_id\": 1,\n               \"auction_type\": null,\n               \"date\": null,\n               \"time\": null,\n               \"datetime_start\": \"2018-04-14 05:25:31\",\n               \"datetime_expire\": 1523769931,\n               \"starting_price\": null,\n               \"home_phone_number\": null,\n               \"estate_agent\": null,\n               \"picture_of_listing\": null,\n               \"duration\": null,\n               \"real_estate_active_himself\": null,\n               \"real_estate_active_bidders\": null,\n               \"reference_relevant_electronic\": null,\n               \"reference_relevant_bidder\": null,\n               \"auction_status\": 1,\n               \"streaming_url\": null,\n               \"streaming_status\": null,\n               \"templateId\": \"\",\n               \"envelopeId\": \"\",\n               \"documentId\": \"\",\n               \"document\": \"\",\n               \"recipientId\": \"\",\n               \"bidding_status\": 2\n          },\n          \"agent\": null,\n          \"agency\": null,\n          \"saveauction\": {\n               \"id\": 33,\n               \"user_id\": 351,\n               \"listing_id\": 1,\n               \"is_favourite\": 1,\n               \"status\": 0,\n               \"rating\": null,\n               \"user_name\": \"gurpreet.gct\"\n           }\n           },\n       ],\n       \"_links\": {\n           \"self\": {\n               \"href\": \"http://cview.civitastech.com/api/web/v1/properties/search?list=1&auction_only=1&page=1\"\n           }\n       },\n       \"_meta\": {\n           \"totalCount\": 7,\n           \"pageCount\": 1,\n           \"currentPage\": 1,\n           \"perPage\": 20\n       }\n   },\n   \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AuctionController.php",
    "groupTitle": "Auction"
  },
  {
    "type": "post",
    "url": "/api/web/v1/webhooks/auction/saveauction",
    "title": "To save an auction",
    "name": "Saveauction",
    "version": "0.1.1",
    "group": "Auction",
    "description": "<p>To save auction, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>auction_id</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "is_favourite",
            "description": "<p>Used For Favorite Auction, 1 =&gt; set as favourite, 0 =&gt; unfavourite</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "callback_url",
            "description": "<p>Callback url to post the data on success</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n     \"id\": 3\n     \"user_id\": \"1\",\n     \"auction_id\": \"1\",\n     \"is_favourite\": \"1\",\n     \"status\": 1,\n     \"rating\": null,\n     \"user_name\": \"john_doe\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"auction_id\",\n      \"message\": \"Auction Id must be an integer.\"\n    },\n    {\n      \"field\": \"is_favourite\",\n      \"message\": \"Is Favourite must be an integer.\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AuctionController.php",
    "groupTitle": "Auction"
  },
  {
    "type": "post",
    "url": "/api/web/v1/auction/saveauction",
    "title": "To favourite or unfavourite an auction",
    "name": "Saveauction",
    "version": "0.1.1",
    "group": "Auction",
    "description": "<p>To save auction, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "listing_id",
            "description": "<p>Listing Id (Auction Id)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "is_favourite",
            "description": "<p>Used For Favorite Listing, 1 =&gt; set as favourite, 0 =&gt; unfavourite</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n     \"id\": 3\n     \"user_id\": \"1\",\n     \"auction_id\": \"1\",\n     \"is_favourite\": \"1\",\n     \"status\": 1,\n     \"rating\": null,\n     \"user_name\": \"john_doe\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to use submit correct data to save an auction</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"auction_id\",\n      \"message\": \"Auction Id must be an integer.\"\n    },\n    {\n      \"field\": \"is_favourite\",\n      \"message\": \"Is Favourite must be an integer.\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AuctionController.php",
    "groupTitle": "Auction"
  },
  {
    "type": "post",
    "url": "/api/web/v1/register-bid/create",
    "title": "Register to Bid",
    "name": "Register_bid",
    "version": "0.1.1",
    "group": "Bid",
    "description": "<p>To save bid, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>Auction Id</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "mobile",
            "description": "<p>User's Mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address_1",
            "description": "<p>User's Address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address_2",
            "description": "<p>User's Address</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "state",
            "description": "<p>The state/county where the property is located</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "postcode",
            "description": "<p>User's Postcode</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country_id",
            "description": "<p>User's Country</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>Gender of user 1=Male /2=Female</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "passport_number",
            "description": "<p>User's Passport</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "driving_licence_number",
            "description": "<p>User's Licence</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mediacare_number",
            "description": "<p>Image in base64 encoded Format</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "recent_utility_bill",
            "description": "<p>Image in base64 encoded Format</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "solicitor_firstname",
            "description": "<p>Solicitor's First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "solicitor_lastname",
            "description": "<p>Solicitor's Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "solicitor_email",
            "description": "<p>Solicitor's Email</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "solicitor_mobile",
            "description": "<p>Solicitor's Mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bidding_panel_number",
            "description": "<p>Bidding Panel Number</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bidding_colour",
            "description": "<p>Represents the bidder at auction</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guarantor_first_name",
            "description": "<p>Guarantor First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guarantor_lastname",
            "description": "<p>Guarantor Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guarantor_email",
            "description": "<p>Guarantor Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "callback_url",
            "description": "<p>Callback url to post the data on success</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n   {\n       \"message\": \"\",\n\t      \"data\": {\n           \"property_id\": \"1\",\n           \"user_id\": 351,\n   \t\t  \"bidding_colour\": \"red\",\n  \t\t  \"bidding_panel_number\": \"12\",\n   \t\t  \"id\": 1,\n   \t\t  \"profile\": {\n       \t      \"id\": 732,\n       \t      \"companyName\": null,\n       \t\t  \"houseNumber\": \"\",\n       \t\t  \"street\": \"\",\n       \t\t  \"city\": \"\",\n       \t\t  \"state\": \"\",\n       \t\t  \"country\": \"\",\n       \t\t  \"postcode\": \"\",\n       \t\t  \"brandName\": \"\",\n       \t\t  \"brandLogo\": \"\",\n       \t\t  \"brandAddress\": \"\",\n       \t\t  \"brandDescription\": \"\",\n       \t\t  \"addLoyalty\": null,\n       \t\t  \"shopNow\": null,\n       \t\t  \"foodEstablishment\": 0,\n       \t\t  \"minimumSpend\": null,\n       \t\t  \"socialmediaFacebook\": \"\",\n       \t\t  \"socialmediaTwitter\": \"\",\n       \t\t  \"socialmediaGoogle\": \"\",\n       \t\t  \"socialmediaPinterest\": \"\",\n       \t\t  \"socialmediaInstagram\": \"\",\n       \t\t  \"backgroundHexColour\": \"\",\n       \t\t  \"foregroundHexColour\": \"\",\n       \t\t  \"socialcheck\": null,\n       \t\t  \"addcolors\": null,\n       \t\t  \"geo_radius\": null,\n       \t\t  \"tempid\": \"\",\n       \t\t  \"transactionrule\": null,\n       \t\t  \"subscriptionFees\": null,\n       \t\t  \"passport_number\": \"123\",\n       \t\t  \"driver_license_number\": \"212\",\n       \t\t  \"mediacare_number\": \"http://localhost/AlphaWallet/html/frontend/web/profile/docs/mediacare_number776.jpg\",\n       \t\t  \"recent_utility_bill\": \"http://localhost/AlphaWallet/html/frontend/web/profile/docs/UtilityBill_Number550.jpg\",\n       \t\t  \"subscriptionDueDate\": null,\n      \t\t  \"subscriptionPay\": null\n  \t\t  }\n\t\t  },\n       \"status\": 200\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to use submit correct data to save an bid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n    \"message\": \"\",\n    \"data\": [\n        {\n            \"field\": \"user_id\",\n            \"message\": \"The bid with property_id \\\"1\\\" and user_id \\\"351\\\" already saved\"\n        },\n        {\n            \"field\": \"property_id\",\n            \"message\": \"The bid with property_id \\\"1\\\" and user_id \\\"351\\\" already saved\"\n        }\n    ],\n    \"status\": 422\n}\nOr\n{\n    \"message\": \"\",\n    \"data\": [\n        \"Unable to place a bid, time exceeded.\"\n    ],\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/RegisterBidController.php",
    "groupTitle": "Bid"
  },
  {
    "type": "post",
    "url": "/api/web/v1/register-bid/updatebid",
    "title": "Update Bid",
    "name": "Updatebid",
    "version": "0.1.1",
    "group": "Bid",
    "description": "<p>To Update bid, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>User's Id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "mobile",
            "description": "<p>User's Mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "otp",
            "description": "<p>OTP sent on mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address_1",
            "description": "<p>User's Address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address_2",
            "description": "<p>User's Address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "state_id",
            "description": "<p>The state/county where the property is located</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "postcode",
            "description": "<p>User's Postcode</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country_id",
            "description": "<p>User's Country</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>Gender of user 1=Male /2=Female</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "passport_number",
            "description": "<p>User's Passport</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "driving_licence_number",
            "description": "<p>User's Licence</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mediacare_number",
            "description": "<p>Image in base64 encoded Format</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "recent_utility_bill",
            "description": "<p>Image in base64 encoded Format</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "solicitor_firstname",
            "description": "<p>Solicitor's First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "solicitor_lastname",
            "description": "<p>Solicitor's Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "solicitor_email",
            "description": "<p>Solicitor's Email</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "solicitor_mobile",
            "description": "<p>Solicitor's Mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "property_id",
            "description": "<p>Bidding Property</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bidding_panel_number",
            "description": "<p>Bidding Panel Number</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bidding_colour",
            "description": "<p>Represents the bidder at auction</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guarantor_first_name",
            "description": "<p>Guarantor First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guarantor_last_name",
            "description": "<p>Guarantor Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guarantor_email",
            "description": "<p>Guarantor Email</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n     \"message\": \"\",\n\t   \"data\": {\n     \"id\": 15,\n     \"user_id\": 362,\n     \"mobile\": \"324536546\",\n     \"otp\": null,\n     \"address_1\": \"address 1\",\n     \"address_2\": \"address 2\",\n     \"state_id\": 0,\n     \"postcode\": null,\n     \"country_id\": 0,\n     \"gender\": \"3\",\n     \"passport_number\": \"2312432dfds\",\n     \"driving_licence_number\": \"cdsf34324\",\n     \"medicare_file\": \"mediacare_number910.jpg\",\n     \"utilitybill_file\": \"UtilityBill_Number_362_371.jpg\",\n     \"solicitor_firstname\": \"test\",\n     \"solicitor_lastname\": \"test2\",\n     \"solicitor_email\": \"test@solicitor.com\",\n     \"solicitor_mobile\": \"5885155842\",\n     \"property_id\": \"1\",\n     \"bidding_panel_number\": \"243265475673\",\n     \"bidding_colour\": \"10\",\n     \"guarantor_first_name\": \"gurentor1\",\n     \"guarantor_lastname\": \"gurentor last\",\n     \"guarantor_email\": \"gui@gui.com\",\n     },\n     \"status\": 200\n     }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to use submit correct data to save an bid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"user_id\",\n      \"message\": \"User Id must be an integer.\"\n    },\n    {\n      \"field\": \"property_id\",\n      \"message\": \"Property Id must be an integer.\"\n    },\n    {\n      \"field\": \"is_favourite\",\n      \"message\": \"Is Favourite must be an integer.\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/RegisterBidController.php",
    "groupTitle": "Bid"
  },
  {
    "type": "post",
    "url": "/api/web/v1/webhooks/users/add-bidder",
    "title": "Add a Bidder",
    "name": "AddBidder",
    "version": "0.1.1",
    "group": "Bidder_Webhooks",
    "description": "<p>To add a Bidder (Webhook), Form-Data must be of raw type</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"firstName\": \"test_fname\",\n    \"lastName\": \"test_lname\",\n    \"username\": \"test_username\",\n    \"password_hash\": \"generated password in hash format\",\n    \"email\": \"abc@yourdomain.com\",\n    \"dob\": \"1995-05-15\",\n    \"passport_number\": \"UPN_15896542\",\n    \"driver_license_number\": \"DLN-15896/2004-05\",\n    \"companyName\" : \"companyNameasd\",\n    \"houseNumber\" : \"#123 A\",\n    \"city\" : \"cityname\",\n    \"state\" : \"stateName\",\n    \"country\" : \"India\",\n    \"postcode\" : 134202,\n    \"officePhone\" : \"0172-42014247\",\n    \"officeFax\" : \"0172-42011547\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 382,\n    \"username\": \"testusername\",\n    \"email\": \"axyz@yourdomain.com\",\n    \"firstName\": \"test_fnameu\",\n    \"lastName\": \"test_lname\",\n    \"fullName\": \"test_fnameu test_lname\",\n    \"dob\": \"1989-05-30\",\n    \"passport_number\": \"UPN_15896542_u\",\n    \"driver_license_number\": \"DLN-15896/2004-05u\",\n    \"companyName\": \"companyName_asd\",\n    \"houseNumber\": \"#123 U\",\n    \"city\": \"cityname_u\",\n    \"state\": \"stateName_u\",\n    \"country\": \"India\",\n    \"postcode\": 134202,\n    \"officePhone\": \"0172-42014200\",\n    \"officeFax\": \"0172-42011500\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"username\",\n      \"message\": \"The username \\\"test_username\\\" has already been taken. The following usernames are available: \\\"test_fname_141, test_lname.929, test_username_786, test_fname.test_lname, test_fnametest_lname\\\". \"\n    },\n    {\n      \"field\": \"email\",\n      \"message\": \"Email abc@yourdomain.com has already been registered. Please enter a valid email address.\"\n    }\n  ],\n \"status\": 422\n}\nOr\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n       \"field\": \"email\",\n       \"message\": \"Email cannot be blank.\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/UserController.php",
    "groupTitle": "Bidder_Webhooks"
  },
  {
    "type": "post",
    "url": "/api/web/v1/webhooks/users/update-bidder",
    "title": "Update a Bidder",
    "name": "UpdateBidder",
    "version": "0.1.1",
    "group": "Bidder_Webhooks",
    "description": "<p>To update a Bidder (Webhook), One must requires login|access token to perform the action, Form-Data must be of raw type</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"id\" : \"Bidder Id\",\n    \"firstName\": \"fname_update\",\n    \"lastName\": \"lname_update\",\n    \"username\": \"test_username\",\n    \"email\": \"abc@yourdomain.com\",\n    \"agency_id\": 11,\n    \"dob\": \"1995-05-15\",\n    \"passport_number\": \"UPN_15896542_N\",\n    \"driver_license_number\": \"DLN-15896/2004-05_N\",\n    \"companyName\" : \"companyName_u\",\n    \"houseNumber\" : \"#123 A_u\",\n    \"city\" : \"cityname_u\",\n    \"state\" : \"stateName_u\",\n    \"country\" : \"India\",\n    \"postcode\" : 134202,\n    \"officePhone\" : \"0172-42014200\",\n    \"officeFax\" : \"0172-42011200\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 382,\n    \"username\": \"testusername\",\n    \"email\": \"axyz@yourdomain.com\",\n    \"firstName\": \"test_fnameu\",\n    \"lastName\": \"test_lname\",\n    \"fullName\": \"test_fnameu test_lname\",\n    \"dob\": \"1989-05-30\",\n    \"agency_id\": 11,\n    \"passport_number\": \"UPN_15896542_u\",\n    \"driver_license_number\": \"DLN-15896/2004-05u\",\n    \"companyName\": \"companyName_asd\",\n    \"houseNumber\": \"#123 U\",\n    \"city\": \"cityname_u\",\n    \"state\": \"stateName_u\",\n    \"country\": \"India\",\n    \"postcode\": 134202,\n    \"officePhone\": \"0172-42014200\",\n    \"officeFax\": \"0172-42011500\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"id\",\n      \"message\": \"ID cannot be blank.\"\n    }\n  ],\n  \"status\": 422\n}\nOr\nHTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/UserController.php",
    "groupTitle": "Bidder_Webhooks"
  },
  {
    "type": "post",
    "url": "/api/web/v1/bidding/auction-details",
    "title": "Auction details",
    "name": "AuctionDetails",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>To Auction details</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>Auction Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n  {\n      \"message\": \"\",\n      \"data\": {\n          \"items\": [\n              {\n                  \"id\": 2,\n                  \"title\": \"MULTI-TENANCY INVESTMENT WITH UPSIDE\",\n                  \"description\": \"For Sale by Expressions of Interest closing Wednesday 16 May 2018 at 3pm.\\n\\nSubstantial retail/office opportunity with upside.\\n\\n- Land Area: 532m2 approx.\\n- Build Area: 1,190m2 approx.\\n- Combined Income: $351,390 pa plus GST\\n- 4 tenancies\\n- Significant development potential (14 levels - STCA)\\n- Rear access\",\n                  \"status\": 1,\n                  \"propertyType\": 4,\n                  \"price\": 234.34,\n                  \"landArea\": null,\n                  \"land_area_unit\": null,\n                  \"floorArea\": 1190,\n                  \"floor_area_unit\": null,\n                  \"conditions\": 0,\n                  \"address\": \"37-41 Hall Street, MOONEE PONDS VIC, 3039 \",\n                  \"latitude\": \"37.9810\",\n                  \"longitude\": \"145.2150\",\n                  \"unitNumber\": 0,\n                  \"streetNumber\": 0,\n                  \"streetName\": null,\n                  \"suburb\": null,\n                  \"city\": \"Richmond\",\n                  \"state\": \"1\",\n                  \"postcode\": null,\n                  \"country\": \"13\",\n                  \"parking\": 1,\n                  \"parkingList\": null,\n                  \"parkingOther\": null,\n                  \"floorplan\": null,\n                  \"media\": null,\n                  \"video\": \"https://youtu.be/YXSXe0LVFV8\",\n                  \"videoImage\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n                 \"document\": null,\n                  \"school\": 1,\n                  \"communityCentre\": 0,\n                  \"parkLand\": 0,\n                  \"publicTransport\": 0,\n                  \"shopping\": 1,\n                  \"bedrooms\": 8,\n                  \"bathrooms\": 6,\n                  \"features\": null,\n                  \"tags\": null,\n                  \"created_by\": null,\n                  \"propertyStatus\": 0,\n                  \"updated_by\": null,\n                  \"created_at\": \"2018-04-18 14:06:29\",\n                  \"updated_at\": \"2018-04-18 14:06:29\",\n                  \"is_featured\": 0,\n                  \"cview_listing_id\": \"\",\n                 \"is_deleted\": 0,\n                  \"business_email\": null,\n                  \"website\": null,\n                  \"valuation\": null,\n                  \"exclusivity\": null,\n                  \"tenancy\": null,\n                  \"zoning\": null,\n                  \"authority\": null,\n                  \"sale_tax\": null,\n                  \"tender_closing_date\": null,\n                  \"current_lease_expiry\": null,\n                  \"outgoings\": null,\n                  \"lease_price\": null,\n                  \"lease_price_show\": 0,\n                  \"lease_period\": null,\n                  \"lease_tax\": null,\n                  \"external_id\": null,\n                  \"video_url\": null,\n                  \"slug\": null,\n                  \"distance\": \"\",\n                  \"propertyTypename\": \"Industrial/Warehouse\",\n                  \"auctionDetails\": null,\n                  \"gallery\": [],\n                  \"auction\": [\n                      {\n                          \"id\": 2,\n                          \"listing_id\": 2,\n                          \"auction_type\": null,\n                          \"estate_agent_id\": 0,\n                          \"agency_id\": 0,\n                          \"date\": null,\n                          \"time\": null,\n                          \"datetime_start\": \"2018-05-21 06:55:57\",\n                          \"datetime_expire\": 1526972180,\n                          \"starting_price\": null,\n                          \"home_phone_number\": null,\n                          \"estate_agent\": null,\n                          \"picture_of_listing\": null,\n                          \"duration\": null,\n                          \"real_estate_active_himself\": null,\n                          \"real_estate_active_bidders\": null,\n                          \"reference_relevant_electronic\": null,\n                          \"reference_relevant_bidder\": null,\n                          \"created_at\": 0,\n                          \"auction_status\": 2,\n                          \"streaming_url\": null,\n                          \"completed_streaming_url\": \"\",\n                          \"streaming_status\": null,\n                          \"templateId\": \"\",\n                          \"envelopeId\": \"\",\n                          \"documentId\": \"\",\n                          \"document\": \"\",\n                          \"recipientId\": \"\",\n                          \"bidding_status\": 2,\n                          \"updated_at\": \"2018-04-27\",\n                          \"created_by\": 0,\n                          \"updated_by\": 0,\n                          \"agent\": null,\n                          \"agency\": null\n                      }\n                  ],\n                  \"saveauction\": null,\n                  \"sold\": \"0\",\n                  \"calendar\": 0\n              }\n          ],\n          \"_links\": {\n              \"self\": {\n                  \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/bidding/auction-details?page=1\"\n              }\n          },\n          \"_meta\": {\n              \"totalCount\": 1,\n              \"pageCount\": 1,\n              \"currentPage\": 1,\n              \"perPage\": 20\n          }\n      },\n      \"status\": 200\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"name\": \"Unauthorized\",\n    \"message\": \"Your request was made with invalid credentials.\",\n    \"code\": 0,\n    \"status\": 401,\n    \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgentController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "post",
    "url": "/api/web/v1/bidding/auction-details",
    "title": "Auction details",
    "name": "AuctionDetails",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>To Auction details</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>Auction Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n  {\n      \"message\": \"\",\n      \"data\": {\n          \"items\": [\n              {\n                  \"id\": 2,\n                  \"title\": \"MULTI-TENANCY INVESTMENT WITH UPSIDE\",\n                  \"description\": \"For Sale by Expressions of Interest closing Wednesday 16 May 2018 at 3pm.\\n\\nSubstantial retail/office opportunity with upside.\\n\\n- Land Area: 532m2 approx.\\n- Build Area: 1,190m2 approx.\\n- Combined Income: $351,390 pa plus GST\\n- 4 tenancies\\n- Significant development potential (14 levels - STCA)\\n- Rear access\",\n                  \"status\": 1,\n                  \"propertyType\": 4,\n                  \"price\": 234.34,\n                  \"landArea\": null,\n                  \"land_area_unit\": null,\n                  \"floorArea\": 1190,\n                  \"floor_area_unit\": null,\n                  \"conditions\": 0,\n                  \"address\": \"37-41 Hall Street, MOONEE PONDS VIC, 3039 \",\n                  \"latitude\": \"37.9810\",\n                  \"longitude\": \"145.2150\",\n                  \"unitNumber\": 0,\n                  \"streetNumber\": 0,\n                  \"streetName\": null,\n                  \"suburb\": null,\n                  \"city\": \"Richmond\",\n                  \"state\": \"1\",\n                  \"postcode\": null,\n                  \"country\": \"13\",\n                  \"parking\": 1,\n                  \"parkingList\": null,\n                  \"parkingOther\": null,\n                  \"floorplan\": null,\n                  \"media\": null,\n                  \"video\": \"https://youtu.be/YXSXe0LVFV8\",\n                  \"videoImage\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n                 \"document\": null,\n                  \"school\": 1,\n                  \"communityCentre\": 0,\n                  \"parkLand\": 0,\n                  \"publicTransport\": 0,\n                  \"shopping\": 1,\n                  \"bedrooms\": 8,\n                  \"bathrooms\": 6,\n                  \"features\": null,\n                  \"tags\": null,\n                  \"created_by\": null,\n                  \"propertyStatus\": 0,\n                  \"updated_by\": null,\n                  \"created_at\": \"2018-04-18 14:06:29\",\n                  \"updated_at\": \"2018-04-18 14:06:29\",\n                  \"is_featured\": 0,\n                  \"cview_listing_id\": \"\",\n                 \"is_deleted\": 0,\n                  \"business_email\": null,\n                  \"website\": null,\n                  \"valuation\": null,\n                  \"exclusivity\": null,\n                  \"tenancy\": null,\n                  \"zoning\": null,\n                  \"authority\": null,\n                  \"sale_tax\": null,\n                  \"tender_closing_date\": null,\n                  \"current_lease_expiry\": null,\n                  \"outgoings\": null,\n                  \"lease_price\": null,\n                  \"lease_price_show\": 0,\n                  \"lease_period\": null,\n                  \"lease_tax\": null,\n                  \"external_id\": null,\n                  \"video_url\": null,\n                  \"slug\": null,\n                  \"distance\": \"\",\n                  \"propertyTypename\": \"Industrial/Warehouse\",\n                  \"auctionDetails\": null,\n                  \"gallery\": [],\n                  \"auction\": [\n                      {\n                          \"id\": 2,\n                          \"listing_id\": 2,\n                          \"auction_type\": null,\n                          \"estate_agent_id\": 0,\n                          \"agency_id\": 0,\n                          \"date\": null,\n                          \"time\": null,\n                          \"datetime_start\": \"2018-05-21 06:55:57\",\n                          \"datetime_expire\": 1526972180,\n                          \"starting_price\": null,\n                          \"home_phone_number\": null,\n                          \"estate_agent\": null,\n                          \"picture_of_listing\": null,\n                          \"duration\": null,\n                          \"real_estate_active_himself\": null,\n                          \"real_estate_active_bidders\": null,\n                          \"reference_relevant_electronic\": null,\n                          \"reference_relevant_bidder\": null,\n                          \"created_at\": 0,\n                          \"auction_status\": 2,\n                          \"streaming_url\": null,\n                          \"completed_streaming_url\": \"\",\n                          \"streaming_status\": null,\n                          \"templateId\": \"\",\n                          \"envelopeId\": \"\",\n                          \"documentId\": \"\",\n                          \"document\": \"\",\n                          \"recipientId\": \"\",\n                          \"bidding_status\": 2,\n                          \"updated_at\": \"2018-04-27\",\n                          \"created_by\": 0,\n                          \"updated_by\": 0,\n                          \"agent\": null,\n                          \"agency\": null\n                      }\n                  ],\n                  \"saveauction\": null,\n                  \"sold\": \"0\",\n                  \"calendar\": 0\n              }\n          ],\n          \"_links\": {\n              \"self\": {\n                  \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/bidding/auction-details?page=1\"\n              }\n          },\n          \"_meta\": {\n              \"totalCount\": 1,\n              \"pageCount\": 1,\n              \"currentPage\": 1,\n              \"perPage\": 20\n          }\n      },\n      \"status\": 200\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"name\": \"Unauthorized\",\n    \"message\": \"Your request was made with invalid credentials.\",\n    \"code\": 0,\n    \"status\": 401,\n    \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/BiddingController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "get",
    "url": "api/web/v1/bidding/auction-current-bid",
    "title": "Current Bid for an Auction",
    "name": "Current_Bid_for_an_Auction",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>Current Bid for an Auction</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>listing id to get the detail of current bid.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 26,\n    \"auction_id\": 3,\n    \"bidder_id\": 351,\n    \"is_vendor\": 0,\n    \"amount\": \"2202.50\",\n    \"bidder_amount\": \"0.00\",\n    \"bidder_currency_id\": 0,\n    \"status\": 3,\n    \"bid_won\": 0,\n    \"complete_status\": 0,\n    \"turn_on_off\": 0,\n    \"play_pause\": 0,\n    \"play_status\": 0,\n    \"lastbid\": \"0.00\",\n    \"cancel\": 0,\n    \"property_in_market\": 0,\n    \"created_at\": \"1970-01-01 00:33:38\",\n    \"updated_at\": \"2018-06-04 08:34:54\",\n    \"created_by\": 351,\n    \"updated_by\": 351,\n    \"statusText\": \"Current Bid\",\n    \"bidderDetail\": {\n      \"id\": 351,\n      \"username\": \"gurpreet.gct\",\n      \"email\": \"gurpreet@graycelltech.com\",\n      \"user_type\": 3,\n      \"app_id\": \"2\",\n      \"auth_key\": \"oNoGgQNodoIRbAZlZqZgTTWDt5zb-ApK\",\n      \"firstName\": \"GPreet\",\n      \"lastName\": \"Singh\",\n      \"fullName\": \"GPreet Singh\",\n      \"orgName\": null,\n      \"fbidentifier\": null,\n      \"linkedin_identifier\": \"123qweasdcxz\",\n      \"google_identifier\": null,\n      \"role_id\": null,\n      \"password_hash\": \"$2y$13$nHZiuXex00yoh9dFlJQg3.vfX7UpAP3.pU3UmN8dl0G0kkRt6oDKm\",\n      \"password_reset_token\": \"ejGpGRiP1S-i1RfMjNa4iXIZpIlo4rWu_1526643425\",\n      \"phone\": null,\n      \"image\": null,\n      \"sex\": null,\n      \"dob\": \"1970-01-01\",\n      \"ageGroup_id\": 1,\n      \"bdayOffer\": 0,\n      \"walletPinCode\": null,\n      \"loyaltyPin\": null,\n      \"advertister_id\": null,\n      \"activationKey\": null,\n      \"confirmationKey\": null,\n      \"access_token\": null,\n      \"hashKey\": null,\n      \"status\": 10,\n      \"device\": \"android\",\n      \"device_token\": \"\",\n      \"lastLogin\": 1528089507,\n      \"latitude\": null,\n      \"longitude\": null,\n      \"timezone\": null,\n      \"timezone_offset\": null,\n      \"created_at\": 1524141463,\n      \"updated_at\": 1528089507,\n      \"storeid\": null,\n      \"promise_uid\": \"3085abb78c79ebb4\",\n      \"promise_acid\": null,\n      \"user_code\": \"AW-gurpreet.gct\",\n      \"referralCode\": null,\n      \"referral_percentage\": null,\n      \"agency_id\": null\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error No Record Found",
          "content": "HTTP/1.1 422 Unprocessable entity\t  \t \n{\n  \"message\": \"No Record Found\",\n  \"data\": {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgentController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "get",
    "url": "api/web/v1/bidding/auction-current-bid",
    "title": "Current Bid for an Auction",
    "name": "Current_Bid_for_an_Auction",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>Current Bid for an Auction</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>listing id to get the detail of current bid.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 26,\n    \"auction_id\": 3,\n    \"bidder_id\": 351,\n    \"is_vendor\": 0,\n    \"amount\": \"2202.50\",\n    \"bidder_amount\": \"0.00\",\n    \"bidder_currency_id\": 0,\n    \"status\": 3,\n    \"bid_won\": 0,\n    \"complete_status\": 0,\n    \"turn_on_off\": 0,\n    \"play_pause\": 0,\n    \"play_status\": 0,\n    \"lastbid\": \"0.00\",\n    \"cancel\": 0,\n    \"property_in_market\": 0,\n    \"created_at\": \"1970-01-01 00:33:38\",\n    \"updated_at\": \"2018-06-04 08:34:54\",\n    \"created_by\": 351,\n    \"updated_by\": 351,\n    \"statusText\": \"Current Bid\",\n    \"bidderDetail\": {\n      \"id\": 351,\n      \"username\": \"gurpreet.gct\",\n      \"email\": \"gurpreet@graycelltech.com\",\n      \"user_type\": 3,\n      \"app_id\": \"2\",\n      \"auth_key\": \"oNoGgQNodoIRbAZlZqZgTTWDt5zb-ApK\",\n      \"firstName\": \"GPreet\",\n      \"lastName\": \"Singh\",\n      \"fullName\": \"GPreet Singh\",\n      \"orgName\": null,\n      \"fbidentifier\": null,\n      \"linkedin_identifier\": \"123qweasdcxz\",\n      \"google_identifier\": null,\n      \"role_id\": null,\n      \"password_hash\": \"$2y$13$nHZiuXex00yoh9dFlJQg3.vfX7UpAP3.pU3UmN8dl0G0kkRt6oDKm\",\n      \"password_reset_token\": \"ejGpGRiP1S-i1RfMjNa4iXIZpIlo4rWu_1526643425\",\n      \"phone\": null,\n      \"image\": null,\n      \"sex\": null,\n      \"dob\": \"1970-01-01\",\n      \"ageGroup_id\": 1,\n      \"bdayOffer\": 0,\n      \"walletPinCode\": null,\n      \"loyaltyPin\": null,\n      \"advertister_id\": null,\n      \"activationKey\": null,\n      \"confirmationKey\": null,\n      \"access_token\": null,\n      \"hashKey\": null,\n      \"status\": 10,\n      \"device\": \"android\",\n      \"device_token\": \"\",\n      \"lastLogin\": 1528089507,\n      \"latitude\": null,\n      \"longitude\": null,\n      \"timezone\": null,\n      \"timezone_offset\": null,\n      \"created_at\": 1524141463,\n      \"updated_at\": 1528089507,\n      \"storeid\": null,\n      \"promise_uid\": \"3085abb78c79ebb4\",\n      \"promise_acid\": null,\n      \"user_code\": \"AW-gurpreet.gct\",\n      \"referralCode\": null,\n      \"referral_percentage\": null,\n      \"agency_id\": null\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error No Record Found",
          "content": "HTTP/1.1 422 Unprocessable entity\t  \n{\n  \"message\": \"No Record Found\",\n  \"data\": {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/BiddingController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "get",
    "url": "api/web/v1/bidding/list-bidders",
    "title": "List Bidders for an Auction",
    "name": "List_Bidders_for_an_Auction",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>List Bidders for an Auction</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>'auction id' to get the information about the respective bidders.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n  \"message\": \"\",\n  \"data\": {\n    \"items\": [\n      {\n        \"id\": 9,\n        \"user_id\": 401,\n        \"auction_id\": 3,\n        \"mobile\": \"9569635775\",\n        \"otp\": \"12323\",\n        \"solicitor_firstname\": \"solicitor_firstname\",\n        \"solicitor_lastname\": \"solicitor_lastname\",\n        \"solicitor_email\": \"solicitor_email\",\n        \"solicitor_mobile\": \"7018029728\",\n        \"bidding_panel_number\": \"232332\",\n        \"bidding_colour\": 0,\n        \"guarantor_first_name\": null,\n        \"guarantor_lastname\": null,\n        \"guarantor_email\": null,\n        \"bidderDetail\": {\n          \"app_id\": \"2\",\n          \"id\": 401,\n          \"username\": \"bidder_user13\",\n          \"email\": \"bidder_user13@yopmail.com\",\n          \"auth_key\": \"2l1FEGn3g7_yz59VTuGWzZ_ysmKe7qO0\",\n          \"firstName\": \"Bidder\",\n          \"lastName\": \"User\",\n          \"fullName\": \"Bidder User\",\n          \"fbidentifier\": null,\n          \"linkedin_identifier\": null,\n          \"google_identifier\": null,\n          \"phone\": null,\n          \"image\": null,\n          \"sex\": null,\n          \"dob\": \"1999-05-25\",\n          \"ageGroup_id\": 1,\n          \"bdayOffer\": 0,\n          \"walletPinCode\": null,\n          \"loyaltyPin\": null,\n          \"advertister_id\": null,\n          \"device\": \"android\",\n          \"device_token\": \"\",\n          \"latitude\": null,\n          \"longitude\": null,\n          \"timezone\": null,\n          \"timezone_offset\": null,\n          \"storeid\": null,\n          \"promise_uid\": \"4015afe96577208d\",\n          \"promise_acid\": null,\n          \"user_code\": \"AW-bidder_user13\",\n          \"referralCode\": null,\n          \"referral_percentage\": null,\n          \"agency_id\": null\n        }\n      },\n      {\n        \"id\": 33,\n        \"user_id\": 383,\n        \"auction_id\": 3,\n        \"mobile\": \"9569635775\",\n        \"otp\": \"12323\",\n        \"solicitor_firstname\": \"solicitor_firstname\",\n        \"solicitor_lastname\": \"solicitor_lastname\",\n        \"solicitor_email\": \"solicitor_email\",\n        \"solicitor_mobile\": \"7018029728\",\n        \"bidding_panel_number\": \"232332\",\n        \"bidding_colour\": 12,\n        \"guarantor_first_name\": \"guarantor_first_name\",\n        \"guarantor_lastname\": \"guarantor_last_name\",\n        \"guarantor_email\": \"guarantor_email\",\n        \"bidderDetail\": {\n          \"app_id\": \"2\",\n          \"id\": 383,\n          \"username\": \"soodvivek\",\n          \"email\": \"viveks@graycelltech.com\",\n          \"auth_key\": \"bzeIhiWdQ6ctsvRFODNZBwSuJWCjEiHL\",\n          \"firstName\": \"Vivek\",\n          \"lastName\": \"Sood\",\n          \"fullName\": \"Vivek Sood\",\n          \"fbidentifier\": null,\n          \"linkedin_identifier\": null,\n          \"google_identifier\": null,\n          \"phone\": null,\n          \"image\": null,\n          \"sex\": null,\n          \"dob\": \"1992-05-09\",\n          \"ageGroup_id\": 1,\n          \"bdayOffer\": 0,\n          \"walletPinCode\": null,\n          \"loyaltyPin\": null,\n          \"advertister_id\": null,\n          \"device\": \"android\",\n          \"device_token\": \"\",\n          \"latitude\": null,\n          \"longitude\": null,\n          \"timezone\": null,\n          \"timezone_offset\": null,\n          \"storeid\": null,\n          \"promise_uid\": \"3835afd5d9d0e593\",\n          \"promise_acid\": null,\n          \"user_code\": \"AW-soodvivek\",\n          \"referralCode\": null,\n          \"referral_percentage\": null,\n          \"agency_id\": null\n        }\n      },\n      {\n        \"id\": 34,\n        \"user_id\": 351,\n        \"auction_id\": 3,\n        \"mobile\": null,\n        \"otp\": null,\n        \"solicitor_firstname\": null,\n        \"solicitor_lastname\": null,\n        \"solicitor_email\": null,\n        \"solicitor_mobile\": null,\n        \"bidding_panel_number\": \"ad123\",\n        \"bidding_colour\": 0,\n        \"guarantor_first_name\": null,\n        \"guarantor_lastname\": null,\n        \"guarantor_email\": null,\n        \"bidderDetail\": {\n          \"app_id\": \"2\",\n          \"id\": 351,\n          \"username\": \"gurpreet.gct\",\n          \"email\": \"gurpreet@graycelltech.com\",\n          \"auth_key\": \"1XQDPa2FFphutfDKT7RHLFvHwU7bVIVP\",\n          \"firstName\": \"GPreet\",\n          \"lastName\": \"Singh\",\n          \"fullName\": \"GPreet Singh\",\n          \"fbidentifier\": null,\n          \"linkedin_identifier\": \"123qweasdcxz\",\n          \"google_identifier\": null,\n          \"phone\": null,\n          \"image\": null,\n          \"sex\": null,\n          \"dob\": \"1970-01-01\",\n          \"ageGroup_id\": 1,\n          \"bdayOffer\": 0,\n          \"walletPinCode\": null,\n          \"loyaltyPin\": null,\n          \"advertister_id\": null,\n          \"device\": \"android\",\n          \"device_token\": \"\",\n          \"latitude\": null,\n          \"longitude\": null,\n          \"timezone\": null,\n          \"timezone_offset\": null,\n          \"storeid\": null,\n          \"promise_uid\": \"3085abb78c79ebb4\",\n          \"promise_acid\": null,\n          \"user_code\": \"AW-gurpreet.gct\",\n          \"referralCode\": null,\n          \"referral_percentage\": null,\n          \"agency_id\": null\n        }\n      }\n    ],\n    \"_links\": {\n      \"self\": {\n        \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/bidding/list-bidders?page=1\"\n      }\n    },\n    \"_meta\": {\n      \"totalCount\": 3,\n      \"pageCount\": 1,\n      \"currentPage\": 1,\n      \"perPage\": 20\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error No Record Found",
          "content": "HTTP/1.1 422 Unprocessable entity\t  \t \n{\n  \"message\": \"No Record Found\",\n  \"data\": {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/BiddingController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "get",
    "url": "api/web/v1/bidding/auction-bids",
    "title": "Bids Listing",
    "name": "List_bids_for_an_auction",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>List bids for an auction excluding bids with status rejected</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>listing id to get the information about the respective bidders.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n  \"message\": \"\",\n  \"data\": [\n     {\n       \"id\": 14,\n       \"auction_id\": 3,\n       \"bidder_id\": 401,\n       \"is_vendor\": 0,\n       \"amount\": \"1000.00\",\n       \"status\": 1,\n       \"bid_won\": 0,\n       \"complete_status\": 0,\n       \"turn_on_off\": 0,\n       \"play_pause\": 0,\n       \"play_status\": 0,\n       \"lastbid\": \"0.00\",\n       \"cancel\": 0,\n       \"property_in_market\": 0,\n       \"created_at\": \"2018-05-28 09:58:07\",\n       \"updated_at\": \"2018-05-28 09:58:07\",\n       \"created_by\": 401,\n       \"updated_by\": 401,\n       \"statusText\": \"Accepted\",\n       \"bidderDetail\": {\n         \"id\": 401,\n         \"username\": \"bidder_user13\",\n         \"email\": \"bidder_user13@yopmail.com\",\n         \"user_type\": 3,\n         \"app_id\": \"2\",\n         \"auth_key\": \"D7O68qobgVgkMsHMZ2fjZqizBE7SemaG\",\n         \"firstName\": \"Bidder\",\n         \"lastName\": \"User\",\n         \"fullName\": \"Bidder User\",\n         \"orgName\": null,\n         \"fbidentifier\": null,\n         \"linkedin_identifier\": null,\n         \"google_identifier\": null,\n         \"role_id\": null,\n         \"password_hash\": \"$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC\",\n         \"password_reset_token\": null,\n         \"phone\": null,\n         \"image\": null,\n         \"sex\": null,\n         \"dob\": \"1999-05-25\",\n         \"ageGroup_id\": 1,\n         \"bdayOffer\": 0,\n         \"walletPinCode\": null,\n         \"loyaltyPin\": null,\n         \"advertister_id\": null,\n         \"activationKey\": null,\n         \"confirmationKey\": null,\n         \"access_token\": null,\n         \"hashKey\": null,\n         \"status\": 10,\n         \"device\": \"android\",\n         \"device_token\": \"\",\n         \"lastLogin\": 1527762172,\n         \"latitude\": null,\n         \"longitude\": null,\n         \"timezone\": null,\n         \"timezone_offset\": null,\n         \"created_at\": 1526634067,\n         \"updated_at\": 1527762172,\n         \"storeid\": null,\n         \"promise_uid\": \"4015afe96577208d\",\n         \"promise_acid\": null,\n         \"user_code\": \"AW-bidder_user13\",\n         \"referralCode\": null,\n         \"referral_percentage\": null,\n         \"agency_id\": null\n       }\n     },\n     {\n       \"id\": 15,\n       \"auction_id\": 3,\n       \"bidder_id\": 401,\n       \"is_vendor\": 0,\n       \"amount\": \"1800.00\",\n       \"status\": 0,\n       \"bid_won\": 0,\n       \"complete_status\": 0,\n       \"turn_on_off\": 0,\n       \"play_pause\": 0,\n       \"play_status\": 0,\n       \"lastbid\": \"0.00\",\n       \"cancel\": 0,\n       \"property_in_market\": 0,\n       \"created_at\": \"2018-05-28 09:58:07\",\n       \"updated_at\": \"2018-05-28 09:58:07\",\n       \"created_by\": 401,\n       \"updated_by\": 401,\n       \"statusText\": \"Pending\",\n       \"bidderDetail\": {\n         \"id\": 401,\n         \"username\": \"bidder_user13\",\n         \"email\": \"bidder_user13@yopmail.com\",\n         \"user_type\": 3,\n         \"app_id\": \"2\",\n         \"auth_key\": \"D7O68qobgVgkMsHMZ2fjZqizBE7SemaG\",\n         \"firstName\": \"Bidder\",\n         \"lastName\": \"User\",\n         \"fullName\": \"Bidder User\",\n         \"orgName\": null,\n         \"fbidentifier\": null,\n         \"linkedin_identifier\": null,\n         \"google_identifier\": null,\n         \"role_id\": null,\n         \"password_hash\": \"$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC\",\n         \"password_reset_token\": null,\n         \"phone\": null,\n         \"image\": null,\n         \"sex\": null,\n         \"dob\": \"1999-05-25\",\n         \"ageGroup_id\": 1,\n         \"bdayOffer\": 0,\n         \"walletPinCode\": null,\n         \"loyaltyPin\": null,\n         \"advertister_id\": null,\n         \"activationKey\": null,\n         \"confirmationKey\": null,\n         \"access_token\": null,\n         \"hashKey\": null,\n         \"status\": 10,\n         \"device\": \"android\",\n         \"device_token\": \"\",\n         \"lastLogin\": 1527762172,\n         \"latitude\": null,\n         \"longitude\": null,\n         \"timezone\": null,\n         \"timezone_offset\": null,\n         \"created_at\": 1526634067,\n         \"updated_at\": 1527762172,\n         \"storeid\": null,\n         \"promise_uid\": \"4015afe96577208d\",\n         \"promise_acid\": null,\n         \"user_code\": \"AW-bidder_user13\",\n         \"referralCode\": null,\n         \"referral_percentage\": null,\n         \"agency_id\": null\n       }\n     }\n  ],\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error No Record Found",
          "content": "HTTP/1.1 422 Unprocessable entity\t  \t \n{\n  \"message\": \"No Record Found\",\n  \"data\": {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n\t\"message\": \"Your request was made with invalid credentials.\",\n\t\"code\": 0,\n\t\"status\": 401,\n\t\"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgentController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "get",
    "url": "api/web/v1/bidding/auction-bids",
    "title": "Bids Listing",
    "name": "List_bids_for_an_auction",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>List bids for an auction excluding bids with status rejected</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>listing id to get the information about the respective bidders.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n  \"message\": \"\",\n  \"data\": [\n     {\n       \"id\": 14,\n       \"auction_id\": 3,\n       \"bidder_id\": 401,\n       \"is_vendor\": 0,\n       \"amount\": \"1000.00\",\n       \"status\": 1,\n       \"bid_won\": 0,\n       \"complete_status\": 0,\n       \"turn_on_off\": 0,\n       \"play_pause\": 0,\n       \"play_status\": 0,\n       \"lastbid\": \"0.00\",\n       \"cancel\": 0,\n       \"property_in_market\": 0,\n       \"created_at\": \"2018-05-28 09:58:07\",\n       \"updated_at\": \"2018-05-28 09:58:07\",\n       \"created_by\": 401,\n       \"updated_by\": 401,\n       \"statusText\": \"Accepted\",\n       \"bidderDetail\": {\n         \"id\": 401,\n         \"username\": \"bidder_user13\",\n         \"email\": \"bidder_user13@yopmail.com\",\n         \"user_type\": 3,\n         \"app_id\": \"2\",\n         \"auth_key\": \"D7O68qobgVgkMsHMZ2fjZqizBE7SemaG\",\n         \"firstName\": \"Bidder\",\n         \"lastName\": \"User\",\n         \"fullName\": \"Bidder User\",\n         \"orgName\": null,\n         \"fbidentifier\": null,\n         \"linkedin_identifier\": null,\n         \"google_identifier\": null,\n         \"role_id\": null,\n         \"password_hash\": \"$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC\",\n         \"password_reset_token\": null,\n         \"phone\": null,\n         \"image\": null,\n         \"sex\": null,\n         \"dob\": \"1999-05-25\",\n         \"ageGroup_id\": 1,\n         \"bdayOffer\": 0,\n         \"walletPinCode\": null,\n         \"loyaltyPin\": null,\n         \"advertister_id\": null,\n         \"activationKey\": null,\n         \"confirmationKey\": null,\n         \"access_token\": null,\n         \"hashKey\": null,\n         \"status\": 10,\n         \"device\": \"android\",\n         \"device_token\": \"\",\n         \"lastLogin\": 1527762172,\n         \"latitude\": null,\n         \"longitude\": null,\n         \"timezone\": null,\n         \"timezone_offset\": null,\n         \"created_at\": 1526634067,\n         \"updated_at\": 1527762172,\n         \"storeid\": null,\n         \"promise_uid\": \"4015afe96577208d\",\n         \"promise_acid\": null,\n         \"user_code\": \"AW-bidder_user13\",\n         \"referralCode\": null,\n         \"referral_percentage\": null,\n         \"agency_id\": null\n       }\n     },\n     {\n       \"id\": 15,\n       \"auction_id\": 3,\n       \"bidder_id\": 401,\n       \"is_vendor\": 0,\n       \"amount\": \"1800.00\",\n       \"status\": 0,\n       \"bid_won\": 0,\n       \"complete_status\": 0,\n       \"turn_on_off\": 0,\n       \"play_pause\": 0,\n       \"play_status\": 0,\n       \"lastbid\": \"0.00\",\n       \"cancel\": 0,\n       \"property_in_market\": 0,\n       \"created_at\": \"2018-05-28 09:58:07\",\n       \"updated_at\": \"2018-05-28 09:58:07\",\n       \"created_by\": 401,\n       \"updated_by\": 401,\n       \"statusText\": \"Pending\",\n       \"bidderDetail\": {\n         \"id\": 401,\n         \"username\": \"bidder_user13\",\n         \"email\": \"bidder_user13@yopmail.com\",\n         \"user_type\": 3,\n         \"app_id\": \"2\",\n         \"auth_key\": \"D7O68qobgVgkMsHMZ2fjZqizBE7SemaG\",\n         \"firstName\": \"Bidder\",\n         \"lastName\": \"User\",\n         \"fullName\": \"Bidder User\",\n         \"orgName\": null,\n         \"fbidentifier\": null,\n         \"linkedin_identifier\": null,\n         \"google_identifier\": null,\n         \"role_id\": null,\n         \"password_hash\": \"$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC\",\n         \"password_reset_token\": null,\n         \"phone\": null,\n         \"image\": null,\n         \"sex\": null,\n         \"dob\": \"1999-05-25\",\n         \"ageGroup_id\": 1,\n         \"bdayOffer\": 0,\n         \"walletPinCode\": null,\n         \"loyaltyPin\": null,\n         \"advertister_id\": null,\n         \"activationKey\": null,\n         \"confirmationKey\": null,\n         \"access_token\": null,\n         \"hashKey\": null,\n         \"status\": 10,\n         \"device\": \"android\",\n         \"device_token\": \"\",\n         \"lastLogin\": 1527762172,\n         \"latitude\": null,\n         \"longitude\": null,\n         \"timezone\": null,\n         \"timezone_offset\": null,\n         \"created_at\": 1526634067,\n         \"updated_at\": 1527762172,\n         \"storeid\": null,\n         \"promise_uid\": \"4015afe96577208d\",\n         \"promise_acid\": null,\n         \"user_code\": \"AW-bidder_user13\",\n         \"referralCode\": null,\n         \"referral_percentage\": null,\n         \"agency_id\": null\n       }\n     }\n  ],\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error No Record Found",
          "content": "HTTP/1.1 422 Unprocessable entity\t  \t \n{\n  \"message\": \"No Record Found\",\n  \"data\": {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n\t\"message\": \"Your request was made with invalid credentials.\",\n\t\"code\": 0,\n\t\"status\": 401,\n\t\"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/BiddingController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "post",
    "url": "/api/web/v1/bidding/submitbid",
    "title": "Submit bid",
    "name": "Submitbid",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>To Submit bid</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>(bid amount).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>Auction Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n {\n      \"message\": \"\",\n      \"data\": {\n          \"amount\": \"1000\",\n          \"auction_id\": \"3\",\n          \"status\": \"1\",\n          \"bidder_id\": 401,\n          \"updated_at\": 1527500831,\n          \"updated_by\": 401,\n          \"created_at\": 1527500831,\n          \"created_by\": 401,\n          \"id\": 13,\n          \"bidder\": {\n              \"id\": 401,\n              \"username\": \"bidder_user13\",\n              \"email\": \"bidder_user13@yopmail.com\",\n              \"user_type\": 3,\n              \"app_id\": \"2\",\n              \"auth_key\": \"4EdhQ2YlhGwgbrPMzqTNfdgQupPgY8OP\",\n              \"firstName\": \"Bidder\",\n              \"lastName\": \"User\",\n              \"fullName\": \"Bidder User\",\n              \"orgName\": null,\n              \"fbidentifier\": null,\n              \"linkedin_identifier\": null,\n              \"google_identifier\": null,\n              \"role_id\": null,\n              \"password_hash\": \"$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC\",\n              \"password_reset_token\": null,\n              \"phone\": null,\n              \"image\": null,\n              \"sex\": null,\n              \"dob\": \"1999-05-25\",\n              \"ageGroup_id\": 1,\n              \"bdayOffer\": 0,\n              \"walletPinCode\": null,\n              \"loyaltyPin\": null,\n              \"advertister_id\": null,\n              \"activationKey\": null,\n              \"confirmationKey\": null,\n              \"access_token\": null,\n              \"hashKey\": null,\n              \"status\": 10,\n              \"device\": \"android\",\n              \"device_token\": \"\",\n              \"lastLogin\": 1527485100,\n              \"latitude\": null,\n              \"longitude\": null,\n              \"timezone\": null,\n              \"timezone_offset\": null,\n              \"created_at\": 1526634067,\n              \"updated_at\": 1527485100,\n              \"storeid\": null,\n              \"promise_uid\": \"4015afe96577208d\",\n              \"promise_acid\": null,\n              \"user_code\": \"AW-bidder_user13\",\n              \"referralCode\": null,\n              \"referral_percentage\": null,\n              \"agency_id\": null\n          }\n      },\n      \"status\": 200\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"name\": \"Unauthorized\",\n    \"message\": \"Your request was made with invalid credentials.\",\n    \"code\": 0,\n    \"status\": 401,\n    \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgentController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "post",
    "url": "/api/web/v1/bidding/submitbid",
    "title": "Submit bid",
    "name": "Submitbid",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>To Submit bid</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>(bid amount).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>Auction Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n {\n      \"message\": \"\",\n      \"data\": {\n          \"amount\": \"1000\",\n          \"auction_id\": \"3\",\n          \"status\": \"1\",\n          \"bidder_id\": 401,\n          \"updated_at\": 1527500831,\n          \"updated_by\": 401,\n          \"created_at\": 1527500831,\n          \"created_by\": 401,\n          \"id\": 13,\n          \"bidder\": {\n              \"id\": 401,\n              \"username\": \"bidder_user13\",\n              \"email\": \"bidder_user13@yopmail.com\",\n              \"user_type\": 3,\n              \"app_id\": \"2\",\n              \"auth_key\": \"4EdhQ2YlhGwgbrPMzqTNfdgQupPgY8OP\",\n              \"firstName\": \"Bidder\",\n              \"lastName\": \"User\",\n              \"fullName\": \"Bidder User\",\n              \"orgName\": null,\n              \"fbidentifier\": null,\n              \"linkedin_identifier\": null,\n              \"google_identifier\": null,\n              \"role_id\": null,\n              \"password_hash\": \"$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC\",\n              \"password_reset_token\": null,\n              \"phone\": null,\n              \"image\": null,\n              \"sex\": null,\n              \"dob\": \"1999-05-25\",\n              \"ageGroup_id\": 1,\n              \"bdayOffer\": 0,\n              \"walletPinCode\": null,\n              \"loyaltyPin\": null,\n              \"advertister_id\": null,\n              \"activationKey\": null,\n              \"confirmationKey\": null,\n              \"access_token\": null,\n              \"hashKey\": null,\n              \"status\": 10,\n              \"device\": \"android\",\n              \"device_token\": \"\",\n              \"lastLogin\": 1527485100,\n              \"latitude\": null,\n              \"longitude\": null,\n              \"timezone\": null,\n              \"timezone_offset\": null,\n              \"created_at\": 1526634067,\n              \"updated_at\": 1527485100,\n              \"storeid\": null,\n              \"promise_uid\": \"4015afe96577208d\",\n              \"promise_acid\": null,\n              \"user_code\": \"AW-bidder_user13\",\n              \"referralCode\": null,\n              \"referral_percentage\": null,\n              \"agency_id\": null\n          }\n      },\n      \"status\": 200\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"name\": \"Unauthorized\",\n    \"message\": \"Your request was made with invalid credentials.\",\n    \"code\": 0,\n    \"status\": 401,\n    \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/BiddingController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "post",
    "url": "/api/web/v1/bidding/place-bid",
    "title": "Place a Bid",
    "name": "To_Place_Bid",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>To Place bid</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>(bid amount).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>Auction Id</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bidder_id",
            "description": "<p>Bidder Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n  \"message\": \"\",\n  \"data\": {\n    \"auction_id\": \"3\",\n    \"amount\": \"2202.5\",\n    \"bidder_id\": 351,\n    \"updated_at\": 1527844460,\n    \"updated_by\": 351,\n    \"created_at\": 1527844460,\n    \"created_by\": 351,\n    \"id\": 26,\n    \"statusText\": \"Pending\",\n    \"bidderDetail\": {\n      \"id\": 351,\n      \"username\": \"gurpreet.gct\",\n      \"email\": \"gurpreet@graycelltech.com\",\n      \"user_type\": 3,\n      \"app_id\": \"2\",\n      \"auth_key\": \"kyzgIhegtl5ZoO83tFGmjjWTxElDcBgA\",\n      \"firstName\": \"GPreet\",\n      \"lastName\": \"Singh\",\n      \"fullName\": \"GPreet Singh\",\n      \"orgName\": null,\n      \"fbidentifier\": null,\n      \"linkedin_identifier\": \"123qweasdcxz\",\n      \"google_identifier\": null,\n      \"role_id\": null,\n      \"password_hash\": \"$2y$13$nHZiuXex00yoh9dFlJQg3.vfX7UpAP3.pU3UmN8dl0G0kkRt6oDKm\",\n      \"password_reset_token\": \"ejGpGRiP1S-i1RfMjNa4iXIZpIlo4rWu_1526643425\",\n      \"phone\": null,\n      \"image\": null,\n      \"sex\": null,\n      \"dob\": \"1970-01-01\",\n      \"ageGroup_id\": 1,\n      \"bdayOffer\": 0,\n      \"walletPinCode\": null,\n      \"loyaltyPin\": null,\n      \"advertister_id\": null,\n      \"activationKey\": null,\n      \"confirmationKey\": null,\n      \"access_token\": null,\n      \"hashKey\": null,\n      \"status\": 10,\n      \"device\": \"android\",\n      \"device_token\": \"\",\n      \"lastLogin\": 1527841527,\n      \"latitude\": null,\n      \"longitude\": null,\n      \"timezone\": null,\n      \"timezone_offset\": null,\n      \"created_at\": 1524141463,\n      \"updated_at\": 1527841527,\n      \"storeid\": null,\n      \"promise_uid\": \"3085abb78c79ebb4\",\n      \"promise_acid\": null,\n      \"user_code\": \"AW-gurpreet.gct\",\n      \"referralCode\": null,\n      \"referral_percentage\": null,\n      \"agency_id\": null\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"amount\",\n      \"message\": \"The minimum bid should be greater than $2,202.00\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"message\": \"\",\n  \"data\": [\n    \"You are not verified for this bidding.\"\n  ],\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/BiddingController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "post",
    "url": "/api/web/v1/bidding/place-bid",
    "title": "Place a Bid",
    "name": "To_Place_Bid",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>To Place bid</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>(bid amount).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>Auction Id</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bidder_id",
            "description": "<p>Bidder Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n  \"message\": \"\",\n  \"data\": {\n    \"auction_id\": \"3\",\n    \"amount\": \"2202.5\",\n    \"bidder_id\": 351,\n    \"updated_at\": 1527844460,\n    \"updated_by\": 351,\n    \"created_at\": 1527844460,\n    \"created_by\": 351,\n    \"id\": 26,\n    \"statusText\": \"Pending\",\n    \"bidderDetail\": {\n      \"id\": 351,\n      \"username\": \"gurpreet.gct\",\n      \"email\": \"gurpreet@graycelltech.com\",\n      \"user_type\": 3,\n      \"app_id\": \"2\",\n      \"auth_key\": \"kyzgIhegtl5ZoO83tFGmjjWTxElDcBgA\",\n      \"firstName\": \"GPreet\",\n      \"lastName\": \"Singh\",\n      \"fullName\": \"GPreet Singh\",\n      \"orgName\": null,\n      \"fbidentifier\": null,\n      \"linkedin_identifier\": \"123qweasdcxz\",\n      \"google_identifier\": null,\n      \"role_id\": null,\n      \"password_hash\": \"$2y$13$nHZiuXex00yoh9dFlJQg3.vfX7UpAP3.pU3UmN8dl0G0kkRt6oDKm\",\n      \"password_reset_token\": \"ejGpGRiP1S-i1RfMjNa4iXIZpIlo4rWu_1526643425\",\n      \"phone\": null,\n      \"image\": null,\n      \"sex\": null,\n      \"dob\": \"1970-01-01\",\n      \"ageGroup_id\": 1,\n      \"bdayOffer\": 0,\n      \"walletPinCode\": null,\n      \"loyaltyPin\": null,\n      \"advertister_id\": null,\n      \"activationKey\": null,\n      \"confirmationKey\": null,\n      \"access_token\": null,\n      \"hashKey\": null,\n      \"status\": 10,\n      \"device\": \"android\",\n      \"device_token\": \"\",\n      \"lastLogin\": 1527841527,\n      \"latitude\": null,\n      \"longitude\": null,\n      \"timezone\": null,\n      \"timezone_offset\": null,\n      \"created_at\": 1524141463,\n      \"updated_at\": 1527841527,\n      \"storeid\": null,\n      \"promise_uid\": \"3085abb78c79ebb4\",\n      \"promise_acid\": null,\n      \"user_code\": \"AW-gurpreet.gct\",\n      \"referralCode\": null,\n      \"referral_percentage\": null,\n      \"agency_id\": null\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"amount\",\n      \"message\": \"The minimum bid should be greater than $2,202.00\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"message\": \"\",\n  \"data\": [\n    \"You are not verified for this bidding.\"\n  ],\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgentController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "post",
    "url": "/api/web/v1/bidding/update-bid-status",
    "title": "Update Bid Status",
    "name": "Update_Bid_Status",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>Update Bid Status, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bid_id",
            "description": "<p>().</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bid_status",
            "description": "<p>(1 = 'Accepted', 2 =&gt; 'Rejected', 3 =&gt; 'Current Bid', 3 =&gt; 'Awarded')</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 2,\n    \"auction_id\": 2,\n    \"bidder_id\": 383,\n    \"is_vendor\": 0,\n    \"amount\": \"1200.00\",\n    \"status\": \"1\",\n    \"bid_won\": 0,\n    \"complete_status\": 0,\n    \"turn_on_off\": 0,\n    \"play_pause\": 0,\n    \"play_status\": 0,\n    \"lastbid\": \"0.00\",\n    \"cancel\": 0,\n    \"property_in_market\": 0,\n    \"created_at\": \"1970-01-01 00:33:38\",\n    \"updated_at\": 1527845138,\n    \"created_by\": 383,\n    \"updated_by\": 383,\n    \"statusText\": \"Accepted\",\n    \"bidderDetail\": {\n      \"id\": 383,\n      \"username\": \"soodvivek\",\n      \"email\": \"viveks@graycelltech.com\",\n      \"user_type\": 3,\n      \"app_id\": \"2\",\n      \"auth_key\": \"bzeIhiWdQ6ctsvRFODNZBwSuJWCjEiHL\",\n      \"firstName\": \"Vivek\",\n      \"lastName\": \"Sood\",\n      \"fullName\": \"Vivek Sood\",\n      \"orgName\": null,\n      \"fbidentifier\": null,\n      \"linkedin_identifier\": null,\n      \"google_identifier\": null,\n      \"role_id\": null,\n      \"password_hash\": \"$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC\",\n      \"password_reset_token\": null,\n      \"phone\": null,\n      \"image\": null,\n      \"sex\": null,\n      \"dob\": \"1992-05-09\",\n      \"ageGroup_id\": 1,\n      \"bdayOffer\": 0,\n      \"walletPinCode\": null,\n      \"loyaltyPin\": null,\n      \"advertister_id\": null,\n      \"activationKey\": null,\n      \"confirmationKey\": null,\n      \"access_token\": null,\n      \"hashKey\": null,\n      \"status\": 10,\n      \"device\": \"android\",\n      \"device_token\": \"\",\n      \"lastLogin\": 1527841441,\n      \"latitude\": null,\n      \"longitude\": null,\n      \"timezone\": null,\n      \"timezone_offset\": null,\n      \"created_at\": 1526554007,\n      \"updated_at\": 1527841441,\n      \"storeid\": null,\n      \"promise_uid\": \"3835afd5d9d0e593\",\n      \"promise_acid\": null,\n      \"user_code\": \"AW-soodvivek\",\n      \"referralCode\": null,\n      \"referral_percentage\": null,\n      \"agency_id\": null\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error No Record Found",
          "content": "HTTP/1.1 422 Unprocessable entity\t  \t \n{\n  \"message\": \"No Record Found\",\n  \"data\": {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200\n{\n  \"message\": \"\",\n  \"data\": [\n    \"Your request was made with invalid credentials.\"\n  ],\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"message\": \"\",\n  \"data\": [\n    \"You don't have an authority for this action\"\n  ],\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AgentController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "post",
    "url": "/api/web/v1/bidding/update-bid-status",
    "title": "Update Bid Status",
    "name": "Update_Bid_Status",
    "version": "0.1.1",
    "group": "Bidding",
    "description": "<p>Update Bid Status, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bid_id",
            "description": "<p>().</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bid_status",
            "description": "<p>(1 = 'Accepted', 2 =&gt; 'Rejected', 3 =&gt; 'Current Bid', 3 =&gt; 'Awarded')</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 2,\n    \"auction_id\": 2,\n    \"bidder_id\": 383,\n    \"is_vendor\": 0,\n    \"amount\": \"1200.00\",\n    \"status\": \"1\",\n    \"bid_won\": 0,\n    \"complete_status\": 0,\n    \"turn_on_off\": 0,\n    \"play_pause\": 0,\n    \"play_status\": 0,\n    \"lastbid\": \"0.00\",\n    \"cancel\": 0,\n    \"property_in_market\": 0,\n    \"created_at\": \"1970-01-01 00:33:38\",\n    \"updated_at\": 1527845138,\n    \"created_by\": 383,\n    \"updated_by\": 383,\n    \"statusText\": \"Accepted\",\n    \"bidderDetail\": {\n      \"id\": 383,\n      \"username\": \"soodvivek\",\n      \"email\": \"viveks@graycelltech.com\",\n      \"user_type\": 3,\n      \"app_id\": \"2\",\n      \"auth_key\": \"bzeIhiWdQ6ctsvRFODNZBwSuJWCjEiHL\",\n      \"firstName\": \"Vivek\",\n      \"lastName\": \"Sood\",\n      \"fullName\": \"Vivek Sood\",\n      \"orgName\": null,\n      \"fbidentifier\": null,\n      \"linkedin_identifier\": null,\n      \"google_identifier\": null,\n      \"role_id\": null,\n      \"password_hash\": \"$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC\",\n      \"password_reset_token\": null,\n      \"phone\": null,\n      \"image\": null,\n      \"sex\": null,\n      \"dob\": \"1992-05-09\",\n      \"ageGroup_id\": 1,\n      \"bdayOffer\": 0,\n      \"walletPinCode\": null,\n      \"loyaltyPin\": null,\n      \"advertister_id\": null,\n      \"activationKey\": null,\n      \"confirmationKey\": null,\n      \"access_token\": null,\n      \"hashKey\": null,\n      \"status\": 10,\n      \"device\": \"android\",\n      \"device_token\": \"\",\n      \"lastLogin\": 1527841441,\n      \"latitude\": null,\n      \"longitude\": null,\n      \"timezone\": null,\n      \"timezone_offset\": null,\n      \"created_at\": 1526554007,\n      \"updated_at\": 1527841441,\n      \"storeid\": null,\n      \"promise_uid\": \"3835afd5d9d0e593\",\n      \"promise_acid\": null,\n      \"user_code\": \"AW-soodvivek\",\n      \"referralCode\": null,\n      \"referral_percentage\": null,\n      \"agency_id\": null\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error No Record Found",
          "content": "HTTP/1.1 422 Unprocessable entity\t  \t  \n{\n  \"message\": \"No Record Found\",\n  \"data\": {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200\n{\n  \"message\": \"\",\n  \"data\": [\n    \"Your request was made with invalid credentials.\"\n  ],\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"message\": \"\",\n  \"data\": [\n    \"You don't have an authority for this action\"\n  ],\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/BiddingController.php",
    "groupTitle": "Bidding"
  },
  {
    "type": "post",
    "url": "/api/web/v1/currency/add-update-currency",
    "title": "AddUpdateCurrency",
    "name": "AddUpdateCurrency",
    "version": "0.1.1",
    "group": "Currencies",
    "description": "<p>To Add Update Currency</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>(For an example: allowed_currencies_for_bids).</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "value",
            "description": "<p>(Currency ids in json format, for an example: [&quot;6&quot;,&quot;22&quot;,&quot;25&quot;,&quot;38&quot;,&quot;85&quot;,&quot;99&quot;,&quot;101&quot;,&quot;123&quot;])</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 \n\t{\n\t    \"message\": \"\",\n\t    \"data\": {\n\t        \"id\": 2,\n\t        \"key\": \"allowed_currencies_for_bids\",\n\t        \"value\": \"[\\\"6\\\",\\\"22\\\",\\\"25\\\",\\\"38\\\",\\\"85\\\",\\\"99\\\",\\\"101\\\",\\\"123\\\"]\",\n\t        \"status\": 1\n\t    },\n\t    \"status\": 200\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"name\": \"Unauthorized\",\n    \"message\": \"Your request was made with invalid credentials.\",\n    \"code\": 0,\n    \"status\": 401,\n    \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/CurrencyController.php",
    "groupTitle": "Currencies"
  },
  {
    "type": "post",
    "url": "/api/web/v1/currency/currency-exchange",
    "title": "CurrencyExchange",
    "name": "CurrencyExchange",
    "version": "0.1.1",
    "group": "Currencies",
    "description": "<p>To Currency Exchange</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>(amount).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "from_currency",
            "description": "<p>(converting from currency)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "to_currency",
            "description": "<p>(converting to currency)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 \n\t{\n\t    \"message\": \"\",\n\t    \"data\": {\n\t        \"currency\": \"INR\",\n\t        \"amount\": 6742\n\t    },\n\t    \"status\": 200\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"name\": \"Unauthorized\",\n    \"message\": \"Your request was made with invalid credentials.\",\n    \"code\": 0,\n    \"status\": 401,\n    \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/CurrencyController.php",
    "groupTitle": "Currencies"
  },
  {
    "type": "get",
    "url": "api/web/v1/currency/get-currency?id=246",
    "title": "Get Currency details by id",
    "name": "Get_Currency_by_id",
    "version": "0.1.1",
    "group": "Currencies",
    "description": "<p>To get a currency by id.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id of the currency.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": [\n        {\n            \"id\": 2,\n            \"country\": \"America\",\n            \"currency\": \"Dollars\",\n        \t \"code\": \"USD\",\n    \t\t \"symbol\": \"$\",\n    \t\t \"status\": 1\n        },\n    ],\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/CurrencyController.php",
    "groupTitle": "Currencies"
  },
  {
    "type": "post",
    "url": "/api/web/v1/currency/remove-currency",
    "title": "RemoveCurrency",
    "name": "RemoveCurrency",
    "version": "0.1.1",
    "group": "Currencies",
    "description": "<p>To Remove Currency</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>(For an example: allowed_currencies_for_bids).</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "value",
            "description": "<p>(Currency ids in json format, for an example: [&quot;22&quot;,&quot;25&quot;])</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 \n\t{\n\t    \"message\": \"\",\n\t    \"data\": {\n\t        \"id\": 2,\n\t        \"key\": \"allowed_currencies_for_bids\",\n\t        \"value\": \"[\\\"6\\\",\\\"38\\\",\\\"85\\\",\\\"99\\\",\\\"101\\\",\\\"123\\\"]\",\n\t        \"status\": 1\n\t    },\n\t    \"status\": 200\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"name\": \"Unauthorized\",\n    \"message\": \"Your request was made with invalid credentials.\",\n    \"code\": 0,\n    \"status\": 401,\n    \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/CurrencyController.php",
    "groupTitle": "Currencies"
  },
  {
    "type": "get",
    "url": "api/web/v1/currency/all-currency",
    "title": "Show All Currencies",
    "name": "Show_All_Currencies",
    "version": "0.1.1",
    "group": "Currencies",
    "description": "<p>To show all currencies.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n   {\n       \"message\": \"\",\n       \"data\": [\n\t        {\n\t            \"id\": 2,\n\t            \"country\": \"America\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"USD\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 3,\n\t            \"country\": \"Afghanistan\",\n\t            \"currency\": \"Afghanis\",\n\t            \"code\": \"AFN\",\n\t            \"symbol\": \"؋\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 4,\n\t            \"country\": \"Argentina\",\n\t            \"currency\": \"Pesos\",\n\t            \"code\": \"ARS\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 5,\n\t            \"country\": \"Aruba\",\n\t            \"currency\": \"Guilders\",\n\t            \"code\": \"AWG\",\n\t            \"symbol\": \"ƒ\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 6,\n\t            \"country\": \"Australia\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"AUD\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 7,\n\t            \"country\": \"Azerbaijan\",\n\t            \"currency\": \"New Manats\",\n\t            \"code\": \"AZN\",\n\t            \"symbol\": \"ман\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 8,\n\t            \"country\": \"Bahamas\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"BSD\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 9,\n\t            \"country\": \"Barbados\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"BBD\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 10,\n\t            \"country\": \"Belarus\",\n\t            \"currency\": \"Rubles\",\n\t            \"code\": \"BYR\",\n\t            \"symbol\": \"p.\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 11,\n\t            \"country\": \"Belgium\",\n\t            \"currency\": \"Euro\",\n\t            \"code\": \"EUR\",\n\t            \"symbol\": \"€\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 12,\n\t            \"country\": \"Beliz\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"BZD\",\n\t            \"symbol\": \"BZ$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 13,\n\t            \"country\": \"Bermuda\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"BMD\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 14,\n\t            \"country\": \"Bolivia\",\n\t            \"currency\": \"Bolivianos\",\n\t            \"code\": \"BOB\",\n\t            \"symbol\": \"$b\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 15,\n\t            \"country\": \"Bosnia and Herzegovina\",\n\t            \"currency\": \"Convertible Marka\",\n\t            \"code\": \"BAM\",\n\t            \"symbol\": \"KM\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 16,\n\t            \"country\": \"Botswana\",\n\t            \"currency\": \"Pula\",\n\t            \"code\": \"BWP\",\n\t            \"symbol\": \"P\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 17,\n\t            \"country\": \"Bulgaria\",\n\t            \"currency\": \"Leva\",\n\t            \"code\": \"BGN\",\n\t            \"symbol\": \"лв\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 18,\n\t            \"country\": \"Brazil\",\n\t            \"currency\": \"Reais\",\n\t            \"code\": \"BRL\",\n\t            \"symbol\": \"R$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 19,\n\t            \"country\": \"Britain (United Kingdom)\",\n\t            \"currency\": \"Pounds\",\n\t            \"code\": \"GBP\",\n\t            \"symbol\": \"£\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 20,\n\t            \"country\": \"Brunei Darussalam\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"BND\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 21,\n\t            \"country\": \"Cambodia\",\n\t            \"currency\": \"Riels\",\n\t            \"code\": \"KHR\",\n\t            \"symbol\": \"៛\",\n\t            \"status\": 1\n\t        }\n       ],\n       \"status\": 200\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/CurrencyController.php",
    "groupTitle": "Currencies"
  },
  {
    "type": "get",
    "url": "api/web/v1/currency/allowed-currency",
    "title": "Show Allowed Currencies",
    "name": "Show_Allowed_Currencies",
    "version": "0.1.1",
    "group": "Currencies",
    "description": "<p>To show allowed currencies.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n   {\n       \"message\": \"\",\n       \"data\": [\n\t        {\n\t            \"id\": 2,\n\t            \"country\": \"America\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"USD\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 3,\n\t            \"country\": \"Afghanistan\",\n\t            \"currency\": \"Afghanis\",\n\t            \"code\": \"AFN\",\n\t            \"symbol\": \"؋\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 4,\n\t            \"country\": \"Argentina\",\n\t            \"currency\": \"Pesos\",\n\t            \"code\": \"ARS\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 5,\n\t            \"country\": \"Aruba\",\n\t            \"currency\": \"Guilders\",\n\t            \"code\": \"AWG\",\n\t            \"symbol\": \"ƒ\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 6,\n\t            \"country\": \"Australia\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"AUD\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 7,\n\t            \"country\": \"Azerbaijan\",\n\t            \"currency\": \"New Manats\",\n\t            \"code\": \"AZN\",\n\t            \"symbol\": \"ман\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 8,\n\t            \"country\": \"Bahamas\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"BSD\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 9,\n\t            \"country\": \"Barbados\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"BBD\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 10,\n\t            \"country\": \"Belarus\",\n\t            \"currency\": \"Rubles\",\n\t            \"code\": \"BYR\",\n\t            \"symbol\": \"p.\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 11,\n\t            \"country\": \"Belgium\",\n\t            \"currency\": \"Euro\",\n\t            \"code\": \"EUR\",\n\t            \"symbol\": \"€\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 12,\n\t            \"country\": \"Beliz\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"BZD\",\n\t            \"symbol\": \"BZ$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 13,\n\t            \"country\": \"Bermuda\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"BMD\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 14,\n\t            \"country\": \"Bolivia\",\n\t            \"currency\": \"Bolivianos\",\n\t            \"code\": \"BOB\",\n\t            \"symbol\": \"$b\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 15,\n\t            \"country\": \"Bosnia and Herzegovina\",\n\t            \"currency\": \"Convertible Marka\",\n\t            \"code\": \"BAM\",\n\t            \"symbol\": \"KM\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 16,\n\t            \"country\": \"Botswana\",\n\t            \"currency\": \"Pula\",\n\t            \"code\": \"BWP\",\n\t            \"symbol\": \"P\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 17,\n\t            \"country\": \"Bulgaria\",\n\t            \"currency\": \"Leva\",\n\t            \"code\": \"BGN\",\n\t            \"symbol\": \"лв\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 18,\n\t            \"country\": \"Brazil\",\n\t            \"currency\": \"Reais\",\n\t            \"code\": \"BRL\",\n\t            \"symbol\": \"R$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 19,\n\t            \"country\": \"Britain (United Kingdom)\",\n\t            \"currency\": \"Pounds\",\n\t            \"code\": \"GBP\",\n\t            \"symbol\": \"£\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 20,\n\t            \"country\": \"Brunei Darussalam\",\n\t            \"currency\": \"Dollars\",\n\t            \"code\": \"BND\",\n\t            \"symbol\": \"$\",\n\t            \"status\": 1\n\t        },\n\t        {\n\t            \"id\": 21,\n\t            \"country\": \"Cambodia\",\n\t            \"currency\": \"Riels\",\n\t            \"code\": \"KHR\",\n\t            \"symbol\": \"៛\",\n\t            \"status\": 1\n\t        }\n       ],\n       \"status\": 200\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/CurrencyController.php",
    "groupTitle": "Currencies"
  },
  {
    "type": "post",
    "url": "/api/web/v1/webhooks/property/sync-properties",
    "title": "Properties Syncing",
    "name": "PropertySyncing",
    "version": "0.1.1",
    "group": "Data_Syncing_Webhooks",
    "description": "<p>To Sync Properties, Form-Data must be of raw type. Data will be posted in the below format by the cview team to sync with our database</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{  \n   \"type\":\"listings\",\n   \"data\":[  \n      {  \n         \"id\":261275,\n         \"attributes\":{  \n            \"title\":\"Final land release in Crestmead Industrial Estate\",\n            \"status\":\"current\",\n            \"price_view\":\"Contact agent\",\n            \"description\":\"The estate borders Crestmead, Browns Plains and Marsden - the fastest growing residential sector in the Logan region. It is a mixed industrial estate in a serviced industrial area designed to cater for the manufacturing industry.\\n\\nProvides access to interstate and overseas markets via the Gateway Arterial road and the Port of Brisbane.\\n\\nVisit www.industrial.edq.com.au for prices and availability.\",\n            \"sale_price\":451660,\n            \"listing_number\":\"11261275\",\n            \"address\":\"Magnesium Drive QLD 4132\",\n            \"business_email\":null,\n            \"website\":null,\n            \"valuation\":null,\n            \"slug\":\"crestmead-4132-qld-land-development-c170ff27-4c73-45cf-a61a-fd15aac3080e\",\n            \"sale_or_lease\":\"sale\",\n            \"exclusivity\":\"exclusive\",\n            \"floor_area\":null,\n            \"floor_area_unit\":null,\n            \"land_area\":2249,\n            \"land_area_unit\":\"squareMeter\",\n            \"total_car_space\":null,\n            \"tenancy\":null,\n            \"zoning\":null,\n            \"authority\":null,\n            \"sale_tax\":\"unknown\",\n            \"tender_closing_date\":null,\n            \"current_lease_expiry\":null,\n            \"outgoings\":null,\n            \"lease_price\":null,\n            \"lease_price_show\":null,\n            \"lease_period\":null,\n            \"lease_tax\":\"unknown\",\n            \"external_id\":\"CL17301272\",\n            \"video_url\":\"\"\n         },\n         \"location\":{  \n            \"latitude\":-37.899,\n            \"longitude\":145.183\n         },\n         \"relationships\":{  \n            \"listing_documents\":[],\n            \"agents\":[  \n               {  \n                  \"id\":6,\n                  \"attributes\":{  \n                     \"email\":\"office_admin@commercialview.com.au\",\n                     \"encrypted_password\":\"$2a$10$C/.SwrwhHHdcroc3nQkwCekv31CC9xTmHF.XN0xYepqODDUbLw7Ty\",\n                     \"first_name\":\"George\",\n                     \"last_name\":\"Clooney\",\n                     \"mobile\":\"0400999888\",\n                     \"phone\":null,\n                     \"enquiry_email\":null,\n                     \"agency_id\":14,\n                     \"preferred_contact_number\":\"0400999888\"\n                  },\n                  \"relationships\":{  \n                     \"agency\":{  \n                        \"id\":14,\n                        \"attributes\":{  \n                           \"name\":\"Fitzroys - Melbourne\",\n                           \"phone\":\"0392757777\",\n                           \"fax\":null,\n                           \"email\":\"email@fitzroys.com.au\",\n                           \"web\":null,\n                           \"twitter\":null,\n                           \"facebook\":null,\n                           \"logo_file_url\":null,\n                           \"agency_id\":\"CV-5B1B4D\",\n                           \"abn\":null,\n                           \"slug\":\"fitzroys-melbourne\"\n                        }\n                     },\n                     \"agent_personal_info\":{}\n                  }\n               }\n            ],\n            \"agency\":{  \n               \"id\":9,\n               \"attributes\":{  \n                  \"name\":\"Biz Aus Pty Pld\",\n                  \"phone\":\"0417113473\",\n                  \"fax\":null,\n                  \"email\":\"phil@bizaus.com.au\",\n                  \"web\":null,\n                  \"twitter\":null,\n                  \"facebook\":null,\n                  \"logo_file_url\":null,\n                  \"agency_id\":\"CV-CF1462\",\n                  \"abn\":null,\n                  \"slug\":\"biz-aus-pty-pld\"\n               }\n            },\n            \"images\":[]\n         }\n      }\n   ]\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "{\n  \"message\": \"Properties list sync up succesfully\",\n  \"data\": {\n      \"status\": \"success\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response in case of invalid / expired Access token:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/PropertyController.php",
    "groupTitle": "Data_Syncing_Webhooks"
  },
  {
    "type": "get",
    "url": "api/web/v1/webhooks/agency/sync-agency-data",
    "title": "Agencies Syncing",
    "name": "SyncAgencyData",
    "version": "0.1.1",
    "group": "Data_Syncing_Webhooks",
    "description": "<p>To Sync Up Agencies, Form-Data must be of raw type. Data will be posted in the below format by the cview team to sync with our database.</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"type\": \"agencies\",\n  \"data\": [\n     {\n       \"id\": 9,\n       \"attributes\": {\n         \"name\": \"Biz Aus Pty Pld\",\n         \"phone\": \"0417113473\",\n         \"fax\": null,\n         \"email\": \"phil@bizaus.com.au\",\n         \"web\": null,\n         \"twitter\": null,\n         \"facebook\": null,\n         \"logo_file_url\": null,\n         \"agency_id\": \"CV-CF1462\",\n         \"abn\": null,\n         \"slug\": \"biz-aus-pty-pld\",\n         \"primary_color\": null,\n         \"secondary_color\": null,\n         \"text_color\": null,\n         \"office_description\": null          \n       },\n       \"relationships\": {\n         \"mailing_address\": {\n           \"id\": 18,\n           \"attributes\": {\n             \"street_address\": \" Level 12, 440 Collins Street\",\n             \"full_address\": \" Level 12, 440 Collins Street<br/>Melbourne, VIC 3000\",\n             \"address_type\": \"mailing_address\",\n             \"suburb_id\": 3\n           },\n           \"location\": {\n             \"latitude\": -37.8163397,\n             \"longitude\": 144.9597121\n           }\n         },\n         \"office_address\": {\n           \"id\": 17,\n           \"attributes\": {\n             \"street_address\": \" Level 12, 440 Collins Street\",\n             \"full_address\": \" Level 12, 440 Collins Street<br/>Melbourne, VIC 3000\",\n             \"address_type\": \"office_address\",\n             \"suburb_id\": 3\n           },\n           \"location\": {\n             \"latitude\": -37.8163397,\n             \"longitude\": 144.9597121\n           }\n         },\n         \"servicing_suburbs\": [],\n         \"documents\": []\n       }\n     },\n     {\n       \"id\": 2,\n       \"attributes\": {\n         \"name\": \"CBRE - Hotels\",\n         \"phone\": \"0293333333\",\n         \"fax\": null,\n         \"email\": \"kelly.smith@cbre.com.au\",\n         \"web\": null,\n         \"twitter\": null,\n         \"facebook\": null,\n         \"logo_file_url\": null,\n         \"agency_id\": \"CV-D74235\",\n         \"abn\": null,\n         \"slug\": \"cbre-hotels\",\n         \"primary_color\": null,\n         \"secondary_color\": null,\n         \"text_color\": null,\n         \"office_description\": null\n       },\n       \"relationships\": {\n         \"mailing_address\": {\n           \"id\": 4,\n           \"attributes\": {\n             \"street_address\": \"Level 26, 363 George Street\",\n             \"full_address\": \"Level 26, 363 George Street<br/>Sydney, NSW 2000\",\n             \"address_type\": \"mailing_address\",\n             \"suburb_id\": 2\n           },\n           \"location\": {\n             \"latitude\": -33.8683539,\n             \"longitude\": 151.206534\n           }\n         },\n         \"office_address\": {\n           \"id\": 3,\n           \"attributes\": {\n             \"street_address\": \"Level 26, 363 George Street\",\n             \"full_address\": \"Level 26, 363 George Street<br/>Sydney, NSW 2000\",\n             \"address_type\": \"office_address\",\n             \"suburb_id\": 2\n           },\n           \"location\": {\n             \"latitude\": -33.8683539,\n             \"longitude\": 151.206534\n           }\n         },\n         \"servicing_suburbs\": [],\n         \"documents\": []\n       }\n     },\n     {\n       \"id\": 12,\n       \"attributes\": {\n         \"name\": \"Crest Business Brokers\",\n         \"phone\": \"0399995488\",\n         \"fax\": null,\n         \"email\": \"mail@crestcorporate.com\",\n         \"web\": null,\n         \"twitter\": null,\n         \"facebook\": null,\n         \"logo_file_url\": null,\n         \"agency_id\": \"CV-8BDF1A\",\n         \"abn\": null,\n         \"slug\": \"crest-business-brokers\",\n         \"primary_color\": null,\n         \"secondary_color\": null,\n         \"text_color\": null,\n         \"office_description\": null\n       },\n       \"relationships\": {\n         \"mailing_address\": {\n           \"id\": 24,\n           \"attributes\": {\n             \"street_address\": \" Level 8, 423 Bourke Street\",\n             \"full_address\": \" Level 8, 423 Bourke Street<br/>Melbourne, VIC 3000\",\n             \"address_type\": \"mailing_address\",\n             \"suburb_id\": 3\n           },\n           \"location\": {\n             \"latitude\": -37.8140595,\n             \"longitude\": 144.9599904\n           }\n         },\n         \"office_address\": {\n           \"id\": 23,\n           \"attributes\": {\n             \"street_address\": \" Level 8, 423 Bourke Street\",\n             \"full_address\": \" Level 8, 423 Bourke Street<br/>Melbourne, VIC 3000\",\n             \"address_type\": \"office_address\",\n             \"suburb_id\": 3\n           },\n           \"location\": {\n             \"latitude\": -37.8140595,\n             \"longitude\": 144.9599904\n           }\n         },\n         \"servicing_suburbs\": [],\n         \"documents\": []\n       }\n     }\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "{\n  \"message\": \"Agencies list sync up succesfully\",\n  \"data\": {\n      \"status\": \"success\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response in case of invalid / expired Access token:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AgencyController.php",
    "groupTitle": "Data_Syncing_Webhooks"
  },
  {
    "type": "get",
    "url": "api/web/v1/webhooks/user/sync-agent-data",
    "title": "Agents data Syncing",
    "name": "SyncAgentData",
    "version": "0.1.1",
    "group": "Data_Syncing_Webhooks",
    "description": "<p>To Sync Up Agent data, Form-Data must be of raw type. Data will be posted in the below format by the cview team to sync with our database.</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{  \n  \"type\":\"agents\",\n  \"data\":[  \n    {  \n      \"id\":1,\n      \"attributes\":{  \n        \"email\":\"admin@commercialview.com.au\",\n        \"encrypted_password\":\"$2a$10$2zpMns14VZxxi.EjILSRcuMWkQ0.OJyHEVILL6a7FBYYaGaBmJl62\",\n        \"first_name\":\"Donald\",\n        \"last_name\":\"Trump\",\n        \"mobile\":\"0400123456\",\n        \"phone\":null,\n        \"enquiry_email\":null,\n        \"agency_id\":14,\n        \"preferred_contact_number\":\"0400123456\"\n      },\n      \"relationships\":{  \n        \"agency\":{  \n           \"id\":14,\n           \"attributes\":{  \n              \"name\":\"Fitzroys - Melbourne\",\n              \"phone\":\"0392757777\",\n              \"fax\":null,\n              \"email\":\"email@fitzroys.com.au\",\n              \"web\":null,\n              \"twitter\":null,\n              \"facebook\":null,\n              \"logo_file_url\":null,\n              \"agency_id\":\"CV-5B1B4D\",\n              \"abn\":null,\n              \"slug\":\"fitzroys-melbourne\"\n           }\n        },\n        \"agent_personal_info\":{}\n      }\n    },\n    {  \n      \"id\":2,\n      \"attributes\":{  \n        \"email\":\"bill@commercialview.com.au\",\n        \"encrypted_password\":\"$2a$10$tTi4PrYjfC5Ufwj.ioaTQOdvWxiiJhmM9TnF5WAVgEylOYO6ITqtu\",\n        \"first_name\":\"Bill\",\n        \"last_name\":\"Gates\",\n        \"mobile\":\"0400111222\",\n        \"phone\":null,\n        \"enquiry_email\":null,\n        \"agency_id\":14,\n        \"preferred_contact_number\":\"0400111222\"\n      },\n      \"relationships\":{  \n        \"agency\":{  \n           \"id\":14,\n           \"attributes\":{  \n              \"name\":\"Fitzroys - Melbourne\",\n              \"phone\":\"0392757777\",\n              \"fax\":null,\n              \"email\":\"email@fitzroys.com.au\",\n              \"web\":null,\n              \"twitter\":null,\n              \"facebook\":null,\n              \"logo_file_url\":null,\n              \"agency_id\":\"CV-5B1B4D\",\n              \"abn\":null,\n              \"slug\":\"fitzroys-melbourne\"\n           }\n        },\n        \"agent_personal_info\":{}\n      }\n    },\n    {  \n      \"id\":3,\n      \"attributes\":{  \n        \"email\":\"warren@commercialview.com.au\",\n        \"encrypted_password\":\"$2a$10$67pXuIaiMt0oN0pQ4LMfcOUl9E.PDIXyWlurvEadu.g5VEyWqjZYO\",\n        \"first_name\":\"Warren\",\n        \"last_name\":\"Buffett\",\n        \"mobile\":\"0400999888\",\n        \"phone\":null,\n        \"enquiry_email\":null,\n        \"agency_id\":2,\n        \"preferred_contact_number\":\"0400999888\"\n      },\n      \"relationships\":{  \n        \"agency\":{  \n           \"id\":2,\n           \"attributes\":{  \n              \"name\":\"CBRE - Hotels\",\n              \"phone\":\"0293333333\",\n              \"fax\":null,\n              \"email\":\"kelly.smith@cbre.com.au\",\n              \"web\":null,\n              \"twitter\":null,\n              \"facebook\":null,\n              \"logo_file_url\":null,\n              \"agency_id\":\"CV-D74235\",\n              \"abn\":null,\n              \"slug\":\"cbre-hotels\"\n           }\n        },\n        \"agent_personal_info\":{}\n      }\n    },\n    {  \n      \"id\":4,\n      \"attributes\":{  \n        \"email\":\"barack@commercialview.com.au\",\n        \"encrypted_password\":\"$2a$10$25Ie1G6iwM5Wb5atL9T5OOidXfCeOu.An2/ny/mFtfo5SGNrBlirK\",\n        \"first_name\":\"Barack\",\n        \"last_name\":\"Obama\",\n        \"mobile\":\"0400999888\",\n        \"phone\":null,\n        \"enquiry_email\":null,\n        \"agency_id\":2,\n        \"preferred_contact_number\":\"0400999888\"\n      },\n      \"relationships\":{  \n        \"agency\":{  \n           \"id\":2,\n           \"attributes\":{  \n              \"name\":\"CBRE - Hotels\",\n              \"phone\":\"0293333333\",\n              \"fax\":null,\n              \"email\":\"kelly.smith@cbre.com.au\",\n              \"web\":null,\n              \"twitter\":null,\n              \"facebook\":null,\n              \"logo_file_url\":null,\n              \"agency_id\":\"CV-D74235\",\n              \"abn\":null,\n              \"slug\":\"cbre-hotels\"\n           }\n        },\n        \"agent_personal_info\":{}\n      }\n    },\n    {  \n      \"id\":6,\n      \"attributes\":{  \n        \"email\":\"office_admin@commercialview.com.au\",\n        \"encrypted_password\":\"$2a$10$C/.SwrwhHHdcroc3nQkwCekv31CC9xTmHF.XN0xYepqODDUbLw7Ty\",\n        \"first_name\":\"George\",\n        \"last_name\":\"Clooney\",\n        \"mobile\":\"0400999888\",\n        \"phone\":null,\n        \"enquiry_email\":null,\n        \"agency_id\":14,\n        \"preferred_contact_number\":\"0400999888\"\n      },\n      \"relationships\":{  \n        \"agency\":{  \n           \"id\":14,\n           \"attributes\":{  \n              \"name\":\"Fitzroys - Melbourne\",\n              \"phone\":\"0392757777\",\n              \"fax\":null,\n              \"email\":\"email@fitzroys.com.au\",\n              \"web\":null,\n              \"twitter\":null,\n              \"facebook\":null,\n              \"logo_file_url\":null,\n              \"agency_id\":\"CV-5B1B4D\",\n              \"abn\":null,\n              \"slug\":\"fitzroys-melbourne\"\n           }\n        },\n        \"agent_personal_info\":{}\n      }\n    }\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "{\n  \"message\": \"Agents list sync up succesfully\",\n  \"data\": {\n      \"status\": \"success\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response in case of invalid / expired Access token:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/UserController.php",
    "groupTitle": "Data_Syncing_Webhooks"
  },
  {
    "type": "get",
    "url": "api/web/v1/webhooks/user/sync-bidders-data",
    "title": "Bidders data Syncing",
    "name": "SyncBiddersData",
    "version": "0.1.1",
    "group": "Data_Syncing_Webhooks",
    "description": "<p>To Sync Up Bidders data, Form-Data must be of raw type. Data will be posted in the below format by the cview team to sync with our database.</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"type\": \"bidders\",\n  \"data\": [\n    {\n      \"id\": 5,\n      \"attributes\": {\n        \"email\": \"consumer@commercialview.com.au\",\n        \"encrypted_password\": \"$2a$10$LuD2lC.POomM9YQtTZcZZu0VsZpF8q1o/DCOmJZD1hyp8J.V2wpKO\",\n        \"first_name\": \"George\",\n        \"last_name\": \"Clooney\",\n        \"mobile\": \"0400999888\",\n        \"phone\": null,\n        \"preferred_contact_number\": \"0400999888\"\n      },\n      \"relationships\": {}\n    }\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "{\n  \"message\": \"Bidders list sync up succesfully\",\n  \"data\": {\n      \"status\": \"success\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response in case of invalid / expired Access token:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/UserController.php",
    "groupTitle": "Data_Syncing_Webhooks"
  },
  {
    "type": "get",
    "url": "api/web/v1/country/index",
    "title": "GeoCountry",
    "name": "GetCountries",
    "version": "0.1.1",
    "group": "Geo",
    "description": "<p>Returns all of the countries available in the DB.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 Created\n{\n    \"message\": \"\",\n    \"data\": [\n        {\n            \"id_countries\": 1,\n            \"sortname\": \"AF\",\n            \"name\": \"Afghanistan\",\n            \"iso_code\": \"AFG\"\n        },\n        {\n            \"id_countries\": 2,\n            \"sortname\": \"AL\",\n            \"name\": \"Albania\",\n            \"iso_code\": \"ALB\"\n        },\n    ],\n    \"status\": 201\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/CountryController.php",
    "groupTitle": "Geo"
  },
  {
    "type": "get",
    "url": "api/web/v1/state/get-state?id=246",
    "title": "Get State details by id",
    "name": "Get_State_by_id",
    "version": "0.1.1",
    "group": "Geo",
    "description": "<p>To get a state by id.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": [\n        {\n            \"id\": 1,\n            \"name\": \"Australian Capital Territory\",\n            \"country_id\": 13,\n        },\n    ],\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/StateController.php",
    "groupTitle": "Geo"
  },
  {
    "type": "get",
    "url": "api/web/v1/state/all-state",
    "title": "Show All States",
    "name": "Show_All_States",
    "version": "0.1.1",
    "group": "Geo",
    "description": "<p>To show all states.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": [\n        {\n            \"id\": 1,\n            \"name\": \"Victoria\"\n        },\n        {\n            \"id\": 2,\n            \"name\": \"New South Wales\"\n        },\n        {\n            \"id\": 3,\n            \"name\": \"Queensland\"\n        },\n        {\n            \"id\": 4,\n            \"name\": \"South Australia\"\n        },\n        {\n            \"id\": 5,\n            \"name\": \"Tasmania\"\n        },\n        {\n            \"id\": 6,\n            \"name\": \"Western Australia\"\n        },\n        {\n            \"id\": 7,\n            \"name\": \"Australian Capital Territory\"\n        },\n        {\n            \"id\": 8,\n            \"name\": \"Northern Territory\"\n        }\n    ],\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/StateController.php",
    "groupTitle": "Geo"
  },
  {
    "type": "get",
    "url": "api/web/v1/state?id=13",
    "title": "Show All States of a particular country",
    "name": "Show_All_States_of_a_particular_country",
    "version": "0.1.1",
    "group": "Geo",
    "description": "<p>To show all states of a particular country.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": [\n        {\n            \"id\": 246,\n            \"name\": \"Australian Capital Territory\",\n            \"country_id\": 13\n        },\n        {\n            \"id\": 266,\n            \"name\": \"New South Wales\",\n            \"country_id\": 13\n        },\n        {\n            \"id\": 267,\n            \"name\": \"Northern Territory\",\n            \"country_id\": 13\n        },\n        {\n            \"id\": 269,\n            \"name\": \"Queensland\",\n            \"country_id\": 13\n        },\n        {\n            \"id\": 273,\n            \"name\": \"Victoria\",\n            \"country_id\": 13\n        },\n        {\n            \"id\": 275,\n            \"name\": \"Western Australia\",\n            \"country_id\": 13\n        }\n    ],\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/StateController.php",
    "groupTitle": "Geo"
  },
  {
    "type": "post",
    "url": "api/web/v1/property/create",
    "title": "Create",
    "name": "CreateProperty",
    "version": "0.1.1",
    "group": "Property",
    "description": "<p>To create property, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>title of property.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>(Optional) A description of the property.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "propertyType",
            "description": "<p>You will get it from PropertyTypes - All Active PropertyTypes API, Please refer the same.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cost",
            "description": "<p>(Optional) eg. '1222.23', the Cost of the property</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "landArea",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "floorArea",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "latitude",
            "description": "<p>(Optional) The property latitude value, eg. 37.8136</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "longitude",
            "description": "<p>(Optional) The property longitude, eg. 144.9631</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "streetName",
            "description": "<p>(Optional) The property street name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "suburb",
            "description": "<p>(Optional) The suburb of the property</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>(Optional) The city where the property is located</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "state",
            "description": "<p>(Optional) The state/county where the property is located</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country",
            "description": "<p>(Optional) Represents the country code, check for  tbl_countries, eg. 101 for India</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "parking",
            "description": "<p>(number) (1|0) 1 =&gt; 'available', eg 1</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "media[]",
            "description": "<p>(Optional) Set of images(base64) files for property carousel</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "video[0]",
            "description": "<p>(Optional) Link to video, eg. youtube link (TBD)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "videoImage[0]",
            "description": "<p>(Optional) Link to video thumbnail</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "document[0]",
            "description": "<p>(Optional) Property document url.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "auction_document[0]",
            "description": "<p>(Optional) Auction prescribed document url</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "school",
            "description": "<p>(1|0), 1 =&gt; 'available', eg 1</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "communityCentre",
            "description": "<p>(1|0), 1=&gt; 'available', eg 1</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "parkLand",
            "description": "<p>(1|0), 1=&gt; 'available'</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "publicTransport",
            "description": "<p>(1|0), 1=&gt; 'available'</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "shopping",
            "description": "<p>(1|0), 1=&gt; 'available'</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bedrooms",
            "description": "<p>(Optional) eg. 5</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bathrooms",
            "description": "<p>(Optional) eg. 3</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "features",
            "description": "<p>(Optional) Text representing the description about the feaures of the property</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tags",
            "description": "<p>(Optional) Set of strings separated by comma(,) egs, countryside, newly built, fully-furnished</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>(Optional) TBD</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "created_by",
            "description": "<p>(Optional) TBD</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 201 Created\n    {\n   \t\"message\": \"\",\n   \t\"data\": [\n   \t\t{\n\t\t\t\t\"id\": 1,\n\t\t\t\t\"title\": \"Brand New Office/Warehouse Development\",\n\t\t\t\t\"description\": \"Cameron is pleased to offer this brand new Office/ Warehouse complex that consists of 23 units ranging between 240sqm to 609sqm. Offering easy access to major arterial roads including Frankston Dandenong Road, Abbotts Road, South Gippsland Freeway and Eastlink via Fox Drive and Discovery Road.\\n\\nEdison Park, being Dandnenong South's newest Industrial Park development; is situated on the corner of Taylors Road and Edison Road and will provide easy dual drive-thru access. Construction is expected to commence in June 2018.\\n\\nEach warehouse include the following:\\n- Office with carpet tiles, air-conditioning, power points, lighting and load-bearing ceiling\\n- Kitchenette\\n- Toilet amenities with shower\\n- Container height roller shutter door\\n- 3 Phase power\",\n\t\t\t\t\"status\": 1,\n\t\t\t\t\"propertyType\": 4,\n\t\t\t\t\"price\": 100,\n\t\t\t\t\"landArea\": 456,\n\t\t\t\t\"floorArea\": 742,\n\t\t\t\t\"conditions\": 0,\n\t\t\t\t\"address\": \"4/18 Luisa Avenue, DANDENONG VIC, 3175 \",\n\t\t\t\t\"latitude\": \"37.9810\",\n\t\t\t\t\"longitude\": \"145.2150\",\n\t\t\t\t\"unitNumber\": 0,\n\t\t\t\t\"streetNumber\": 0,\n\t\t\t\t\"streetName\": null,\n\t\t\t\t\"suburb\": null,\n\t\t\t\t\"city\": \"Mohali\",\n\t\t\t\t\"state\": \"1\",\n\t\t\t\t\"postcode\": null,\n\t\t\t\t\"country\": \"13\",\n\t\t\t\t\"parking\": 1,\n\t\t\t\t\"parkingList\": null,\n\t\t\t\t\"parkingOther\": null,\n\t\t\t\t\"floorplan\": null,\n\t\t\t\t\"media\": null,\n\t\t\t\t\"video\": \"[\\\"https://youtu.be/YXSXe0LVFV8\\\"]\",\n\t\t\t\t\"videoImage\": \"[\\\"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\\\"]\",\n\t\t\t\t\"school\": 1,\n\t\t\t\t\"communityCentre\": 1,\n\t\t\t\t\"parkLand\": 1,\n\t\t\t\t\"publicTransport\": 0,\n\t\t\t\t\"shopping\": 1,\n\t\t\t\t\"bedrooms\": 3,\n\t\t\t\t\"bathrooms\": 4,\n\t\t\t\t\"features\": null,\n\t\t\t\t\"tags\": null,\n\t\t\t\t\"estate_agent_id\": 502,\n\t\t\t\t\"second_agent_id\": null,\n\t\t\t\t\"agency_id\": 16,\n\t\t\t\t\"created_by\": null,\n\t\t\t\t\"propertyStatus\": 0,\n\t\t\t\t\"updated_by\": null,\n\t\t\t\t\"created_at\": \"2018-04-18 14:02:04\",\n\t\t\t\t\"updated_at\": \"2018-04-18 14:02:04\",\n\t\t\t\t\"inspection1\": null,\n\t\t\t\t\"inspection2\": null,\n\t\t\t\t\"inspection3\": null,\n\t\t\t\t\"inspection4\": null,\n\t\t\t\t\"inspection5\": null,\n\t\t\t\t\"inspection6\": null,\n\t\t\t\t\"inspection7\": null,\n\t\t\t\t\"inspection8\": null,\n\t\t\t\t\"is_featured\": 0,\n\t\t\t\t\"distance\": \"\",\n\t\t\t\t\"propertyTypename\": \"Industrial/Warehouse\",\n\t\t\t\t\"auctionDetails\": {\n\t\t\t\t\t\"live\": false,\n\t\t\t\t\t\"past\": false,\n\t\t\t\t\t\"upcoming\": true,\n\t\t\t\t\t\"seconds\": 0\n\t\t\t\t},\n\t\t\t\t\"gallery\": [\n\t\t\t\t\t{\n\t\t\t\t\t\t\"id\": 18,\n\t\t\t\t\t\t\"tbl_pk\": 1,\n\t\t\t\t\t\t\"url\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image903.jpg\",\n\t\t\t\t\t\t\"type\": \"image/jpg\",\n\t\t\t\t\t\t\"tbl_name\": \"tbl_properties\"\n\t\t\t\t\t}\n\t\t\t\t],\n\t\t\t\t\"auction\": {\n\t\t\t\t\t\"id\": 1,\n\t\t\t\t\t\"listing_id\": 1,\n\t\t\t\t\t\"auction_type\": null,\n\t\t\t\t\t\"date\": \"2018-04-14\",\n\t\t\t\t\t\"time\": \"13:00:00\",\n\t\t\t\t\t\"datetime_start\": \"2018-05-25 07:27:20\",\n\t\t\t\t\t\"datetime_expire\": 1523769931,\n\t\t\t\t\t\"starting_price\": null,\n\t\t\t\t\t\"home_phone_number\": null,\n\t\t\t\t\t\"estate_agent\": null,\n\t\t\t\t\t\"picture_of_listing\": null,\n\t\t\t\t\t\"duration\": null,\n\t\t\t\t\t\"real_estate_active_himself\": null,\n\t\t\t\t\t\"real_estate_active_bidders\": null,\n\t\t\t\t\t\"reference_relevant_electronic\": null,\n\t\t\t\t\t\"reference_relevant_bidder\": null,\n\t\t\t\t\t\"created_at\": 1523856263,\n\t\t\t\t\t\"auction_status\": 2,\n\t\t\t\t\t\"streaming_url\": null,\n\t\t\t\t\t\"streaming_status\": null,\n\t\t\t\t\t\"templateId\": \"\",\n\t\t\t\t\t\"envelopeId\": \"\",\n\t\t\t\t\t\"documentId\": \"\",\n\t\t\t\t\t\"document\": \"\",\n\t\t\t\t\t\"recipientId\": \"\",\n\t\t\t\t\t\"bidding_status\": 2,\n\t\t\t\t\t\"updated_at\": \"2018-04-27\",\n\t\t\t\t\t\"created_by\": 0,\n\t\t\t\t\t\"updated_by\": 0\n\t\t\t\t},\n\t\t\t\t\"agent\": {\n\t\t\t\t\t\"status\": 10,\n\t\t\t\t\t\"user_type\": 10,\n\t\t\t\t\t\"app_id\": \"2\",\n\t\t\t\t\t\"id\": 502,\n\t\t\t\t\t\"username\": \"gurpreet.gct\",\n\t\t\t\t\t\"auth_key\": \"ELuV7Ve9BccyKlqHU9bmPYtv3W63o7ZB\",\n\t\t\t\t\t\"fullName\": \"gurpreet singh\",\n\t\t\t\t\t\"email\": \"gurpreet@graycelltech.com\",\n\t\t\t\t\t\"orgName\": null,\n\t\t\t\t\t\"firstName\": \"gurpreet\",\n\t\t\t\t\t\"lastName\": \"singh\",\n\t\t\t\t\t\"fbidentifier\": null,\n\t\t\t\t\t\"linkedin_identifier\": null,\n\t\t\t\t\t\"google_identifier\": null,\n\t\t\t\t\t\"role_id\": null,\n\t\t\t\t\t\"password_hash\": \"$2y$13$2otlmYGEqDlkC2AIO0u3M.Bt/Z3WNNN/pxq4IjGvFcTWIrIVAPNbS\",\n\t\t\t\t\t\"password_reset_token\": \"GpZ_v64WJKpO47GxYOH17l3AvzTsdROA_1526644554\",\n\t\t\t\t\t\"phone\": null,\n\t\t\t\t\t\"image\": null,\n\t\t\t\t\t\"sex\": null,\n\t\t\t\t\t\"dob\": \"1993-08-03\",\n\t\t\t\t\t\"ageGroup_id\": 1,\n\t\t\t\t\t\"bdayOffer\": 0,\n\t\t\t\t\t\"walletPinCode\": null,\n\t\t\t\t\t\"loyaltyPin\": null,\n\t\t\t\t\t\"advertister_id\": null,\n\t\t\t\t\t\"activationKey\": null,\n\t\t\t\t\t\"confirmationKey\": null,\n\t\t\t\t\t\"access_token\": null,\n\t\t\t\t\t\"hashKey\": null,\n\t\t\t\t\t\"device\": \"android\",\n\t\t\t\t\t\"device_token\": \"12113123113\",\n\t\t\t\t\t\"lastLogin\": 1526890835,\n\t\t\t\t\t\"latitude\": null,\n\t\t\t\t\t\"longitude\": null,\n\t\t\t\t\t\"timezone\": null,\n\t\t\t\t\t\"timezone_offset\": null,\n\t\t\t\t\t\"created_at\": 1526624964,\n\t\t\t\t\t\"updated_at\": 1526890835,\n\t\t\t\t\t\"storeid\": null,\n\t\t\t\t\t\"promise_uid\": \"3085abb78c79ebb4\",\n\t\t\t\t\t\"promise_acid\": null,\n\t\t\t\t\t\"user_code\": \"AW-gurpreet.gct\",\n\t\t\t\t\t\"referralCode\": null,\n\t\t\t\t\t\"referral_percentage\": null,\n\t\t\t\t\t\"agency_id\": null,\n\t\t\t\t\t\"logo\": \"\"\n\t\t\t\t},\n\t\t\t\t\"agency\": {\n\t\t\t\t\t\"status\": 10,\n\t\t\t\t\t\"id\": 16,\n\t\t\t\t\t\"name\": \"Biggin\",\n\t\t\t\t\t\"email\": \"biggin@gmail.cpm\",\n\t\t\t\t\t\"mobile\": 919876543215,\n\t\t\t\t\t\"logo\": \"http://cview.civitastech.com/frontend/web/agencylogo/aastha248.jpg\",\n\t\t\t\t\t\"backgroungImage\": \"http://cview.civitastech.com/frontend/web/agencylogo/aastha248.jpg\",\n\t\t\t\t\t\"address\": \"sco 206 - 207, sector 34 a, chandigarh - 160022\",\n\t\t\t\t\t\"latitude\": \"30.72589\",\n\t\t\t\t\t\"longitude\": \"76.75787\",\n\t\t\t\t\t\"about_us\": \" <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\",\n\t\t\t\t\t\"created_at\": 1524059536,\n\t\t\t\t\t\"updated_at\": 1524059536,\n\t\t\t\t\t\"is_favourite\": \"\",\n\t\t\t\t\t\"facebook\": \"https://www.facebook.com/\",\n\t\t\t\t\t\"twitter\": \"https://twitter.com/\",\n\t\t\t\t\t\"instagram\": \"https://www.instagram.com/\"\n\t\t\t\t},\n\t\t\t\t\"saveauction\": null,\n\t\t\t\t\"sold\": \"0\",\n\t\t\t\t\"calendar\": 0\n\t\t\t},\n   \t],\n   \t\"status\": 200\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 422 Validation Error\n    {\n        \"message\": \"\",\n        \"data\": [\n            {\n                \"field\": \"some filedname\",\n                \"message\": \"Some message\"\n            }\n        ],\n        \"status\": 422\n    }\nOr\n    HTTP/1.1 422 Validation Error\n    {\n        \"message\": \"\",\n\t\t  \"data\": {\n\t\t      \"title\": \"First property\",\n\t\t\t  \"description\": \"A lovely property at the country side!\",\n            \"id\": 2\n        },\n        \"status\": 201\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/PropertyController.php",
    "groupTitle": "Property"
  },
  {
    "type": "get",
    "url": "api/web/v1/properties/get-agent-properties",
    "title": "Get logged-in agent properties",
    "name": "Get_Agent_Properties",
    "version": "0.1.1",
    "group": "Property",
    "description": "<p>To lsit properties of loggedin agent.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n\"message\": \"\",\n\"data\": {\n\t\"items\": [\n\t\t{\n\t\t\t\"id\": 5,\n\t\t\t\"title\": \"SUPERB FREEHOLD OPPORTUNITY\",\n\t\t\t\"description\": \"Teska Carson is pleased to offer 922 Riversdale Road, Surrey Hills for sale by public auction on Wednesday 23rd May.\",\n\t\t\t\"status\": 1,\n\t\t\t\"propertyType\": 7,\n\t\t\t\"price\": 4531.89,\n\t\t\t\"landArea\": null,\n\t\t\t\"floorArea\": 180,\n\t\t\t\"conditions\": 0,\n\t\t\t\"address\": \"Grayce Ct SE, Salem, OR, United States\",\n\t\t\t\"latitude\": \"44.92396550\",\n\t\t\t\"longitude\": \"-122.97581490\",\n\t\t\t\"unitNumber\": 0,\n\t\t\t\"streetNumber\": 0,\n\t\t\t\"streetName\": null,\n\t\t\t\"suburb\": null,\n\t\t\t\"city\": null,\n\t\t\t\"state\": null,\n\t\t\t\"postcode\": null,\n\t\t\t\"country\": null,\n\t\t\t\"parking\": 0,\n\t\t\t\"parkingList\": null,\n\t\t\t\"parkingOther\": null,\n\t\t\t\"floorplan\": null,\n\t\t\t\"media\": null,\n\t\t\t\"video\": null,\n\t\t\t\"videoImage\": null,\n\t\t\t\"school\": 0,\n\t\t\t\"communityCentre\": 0,\n\t\t\t\"parkLand\": 0,\n\t\t\t\"publicTransport\": 0,\n\t\t\t\"shopping\": 0,\n\t\t\t\"bedrooms\": 0,\n\t\t\t\"bathrooms\": 0,\n\t\t\t\"features\": null,\n\t\t\t\"tags\": null,\n\t\t\t\"estate_agent_id\": 351,\n\t\t\t\"second_agent_id\": null,\n\t\t\t\"agency_id\": 15,\n\t\t\t\"created_by\": null,\n\t\t\t\"propertyStatus\": 0,\n\t\t\t\"updated_by\": null,\n\t\t\t\"created_at\": \"2018-04-19 06:10:59\",\n\t\t\t\"updated_at\": \"2018-04-19 06:10:59\",\n\t\t\t\"inspection1\": null,\n\t\t\t\"inspection2\": null,\n\t\t\t\"inspection3\": null,\n\t\t\t\"inspection4\": null,\n\t\t\t\"inspection5\": null,\n\t\t\t\"inspection6\": null,\n\t\t\t\"inspection7\": null,\n\t\t\t\"inspection8\": null,\n\t\t\t\"is_featured\": 0,\n\t\t\t\"distance\": \"\",\n\t\t\t\"propertyTypename\": \"Retail\",\n\t\t\t\"auctionDetails\": {\n\t\t\t\t\"live\": false,\n\t\t\t\t\"past\": false,\n\t\t\t\t\"upcoming\": true,\n\t\t\t\t\"seconds\": 0\n\t\t\t},\n\t\t\t\"gallery\": [],\n\t\t\t\"auction\": {\n\t\t\t\t\"id\": 23,\n\t\t\t\t\"listing_id\": 5,\n\t\t\t\t\"auction_type\": null,\n\t\t\t\t\"date\": null,\n\t\t\t\t\"time\": null,\n\t\t\t\t\"datetime_start\": \"2018-05-20 02:58:11\",\n\t\t\t\t\"datetime_expire\": 1526792091,\n\t\t\t\t\"starting_price\": null,\n\t\t\t\t\"home_phone_number\": null,\n\t\t\t\t\"estate_agent\": null,\n\t\t\t\t\"picture_of_listing\": null,\n\t\t\t\t\"duration\": null,\n\t\t\t\t\"real_estate_active_himself\": null,\n\t\t\t\t\"real_estate_active_bidders\": null,\n\t\t\t\t\"reference_relevant_electronic\": null,\n\t\t\t\t\"reference_relevant_bidder\": null,\n\t\t\t\t\"created_at\": 0,\n\t\t\t\t\"auction_status\": 1,\n\t\t\t\t\"streaming_url\": null,\n\t\t\t\t\"streaming_status\": null,\n\t\t\t\t\"templateId\": \"\",\n\t\t\t\t\"envelopeId\": \"\",\n\t\t\t\t\"documentId\": \"\",\n\t\t\t\t\"document\": \"\",\n\t\t\t\t\"recipientId\": \"\",\n\t\t\t\t\"bidding_status\": 2,\n\t\t\t\t\"updated_at\": \"2018-04-27\",\n\t\t\t\t\"created_by\": 0,\n\t\t\t\t\"updated_by\": 0\n\t\t\t},\n\t\t\t\"agent\": {\n\t\t\t\t\"status\": 10,\n\t\t\t\t\"user_type\": 10,\n\t\t\t\t\"app_id\": \"2\",\n\t\t\t\t\"id\": 351,\n\t\t\t\t\"username\": \"gurpreet.gct\",\n\t\t\t\t\"email\": \"gurpreet@graycelltech.com\",\n\t\t\t\t\"auth_key\": \"YR7IbtgZ0YNCMXW7LqLqCiI6ULIGW9dh\",\n\t\t\t\t\"firstName\": \"GPreet\",\n\t\t\t\t\"lastName\": \"Singh\",\n\t\t\t\t\"fullName\": \"GPreet Singh\",\n\t\t\t\t\"orgName\": null,\n\t\t\t\t\"fbidentifier\": null,\n\t\t\t\t\"linkedin_identifier\": \"123qweasdcxz\",\n\t\t\t\t\"google_identifier\": null,\n\t\t\t\t\"role_id\": null,\n\t\t\t\t\"password_hash\": \"$2y$13$nHZiuXex00yoh9dFlJQg3.vfX7UpAP3.pU3UmN8dl0G0kkRt6oDKm\",\n\t\t\t\t\"password_reset_token\": null,\n\t\t\t\t\"phone\": null,\n\t\t\t\t\"image\": null,\n\t\t\t\t\"sex\": null,\n\t\t\t\t\"dob\": \"1970-01-01\",\n\t\t\t\t\"ageGroup_id\": 1,\n\t\t\t\t\"bdayOffer\": 0,\n\t\t\t\t\"walletPinCode\": null,\n\t\t\t\t\"loyaltyPin\": null,\n\t\t\t\t\"advertister_id\": null,\n\t\t\t\t\"activationKey\": null,\n\t\t\t\t\"confirmationKey\": null,\n\t\t\t\t\"access_token\": null,\n\t\t\t\t\"hashKey\": null,\n\t\t\t\t\"device\": \"android\",\n\t\t\t\t\"device_token\": \"\",\n\t\t\t\t\"lastLogin\": 1526628472,\n\t\t\t\t\"latitude\": null,\n\t\t\t\t\"longitude\": null,\n\t\t\t\t\"timezone\": null,\n\t\t\t\t\"timezone_offset\": null,\n\t\t\t\t\"created_at\": 1524141463,\n\t\t\t\t\"updated_at\": 1526628472,\n\t\t\t\t\"storeid\": null,\n\t\t\t\t\"promise_uid\": \"3085abb78c79ebb4\",\n\t\t\t\t\"promise_acid\": null,\n\t\t\t\t\"user_code\": \"AW-gurpreet.gct\",\n\t\t\t\t\"referralCode\": null,\n\t\t\t\t\"referral_percentage\": null,\n\t\t\t\t\"agency_id\": null,\n\t\t\t\t\"logo\": \"\"\n\t\t\t},\n\t\t\t\"agency\": {\n\t\t\t\t\"status\": 10,\n\t\t\t\t\"id\": 15,\n\t\t\t\t\"name\": \"Abercromby’s145\",\n\t\t\t\t\"email\": \"aastha14@gmail.com\",\n\t\t\t\t\"mobile\": 1234569874,\n\t\t\t\t\"logo\": \"http://localhost/AlphaWallet/html/frontend/web/agencylogo/Abercromby’s145181.jpg\",\n\t\t\t\t\"backgroungImage\": null,\n\t\t\t\t\"address\": \"[\\\" sco 206 - 207, sector 34 a, chandigarh - 160022\\\"]\",\n\t\t\t\t\"latitude\": \"[\\\"30.72589\\\"]\",\n\t\t\t\t\"longitude\": \"[\\\"76.75787\\\"]\",\n\t\t\t\t\"about_us\": \"Gross Waddell Pty Ltd is a\\ncommercial real estate agency,\\noperating for over 20 years in the\\nretail, office, industrial and\\ndevelopment property sectors. We\\nprovide Sales and Leasing, Asset\\nManagement\\nand\\nCorporate\\nServices to Vendors, Landlords and\\nInvestors.\",\n\t\t\t\t\"created_at\": 1524048846,\n\t\t\t\t\"updated_at\": 1524048846,\n\t\t\t\t\"is_favourite\": \"\",\n\t\t\t\t\"facebook\": \"https://www.facebook.com/\",\n\t\t\t\t\"twitter\": \"https://twitter.com/\",\n\t\t\t\t\"instagram\": \"https://www.instagram.com/\"\n\t\t\t},\n\t\t\t\"saveauction\": null,\n\t\t\t\"sold\": \"0\",\n\t\t\t\"calendar\": 0\n\t\t},\n\t],\n\t\"_links\": {\n\t\t\"self\": {\n\t\t\t\"href\": \"http://localhost/AlphaWallet/html/api/web/v1/properties/get-agent-properties?page=1\"\n\t\t}\n\t},\n\t\"_meta\": {\n\t\t\"totalCount\": 4,\n\t\t\"pageCount\": 1,\n\t\t\"currentPage\": 1,\n\t\t\"perPage\": 20\n\t}\n},\n\"status\": 200",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Validation Error\n{\n    \"message\": \"\",\n    \"data\": [],\n    \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/PropertyController.php",
    "groupTitle": "Property"
  },
  {
    "type": "get",
    "url": "api/web/v1/property/search",
    "title": "Property Searching & Sorting",
    "name": "SearchProperty",
    "version": "0.1.1",
    "group": "Property",
    "description": "<p>To search or Sort Property.</p> <h3>Supported keys for sorting:</h3> <ul> <li><em>price</em> | ascending=&gt;'price', 'descending' =&gt; '-price'</li> <li><em>date</em> | ascending=&gt;'date', 'descending' =&gt; '-date'</li> <li><em>distance</em> | ascending=&gt;'distance', 'descending' =&gt; '-distance'</li> </ul> <h3><code>Note: Append (-) for sorting in descending order</code></h3> <h3>Examples for reference:</h3> <h4><code>/api/web/v1/property/search?sort=price,-date</code></h4> <h4><code>/api/web/v1/property/search?property_type=Commercial Farming, Land/Development</code> | <code>property_type=Commercial Farming</code></h4> <h4><code>/api/web/v1/property/search?from_date=2018-04-01</code> | <code>/api/web/v1/property/search?to_date=2018-04-20</code> | <code>/api/web/v1/property/search?from_date=2018-04-01&amp;to_date=2018-04-20</code></h4> <h4><code>/api/web/v1/property/search?sale_from=2000</code> | <code>/api/web/v1/property/search?sale_to=5000</code> | <code>/api/web/v1/property/search?sale_from=2000&amp;sale_to=5000</code></h4> <h4><code>/api/web/v1/property/search?min_land=244</code> | <code>/api/web/v1/property/search?max_land=1200</code> | <code>/api/web/v1/property/search?min_land=230&amp;max_land=2200</code></h4> <h4><code>/api/web/v1/property/search?min_floor=244</code> | <code>/api/web/v1/property/search?max_floor=1200</code> | <code>/api/web/v1/property/search?min_floor=230&amp;max_floor=2200</code></h4> <h4><code>/api/web/v1/property/search?auction_only=1</code> To display only auctions</h4> <h4><code>/api/web/v1/property/search?auction_all=1</code> (Optional) To display all auctions</h4> <h4><code>http://cview.civitastech.com/api/web/v1/property/search?suburb=&amp;property_type=Commercial Farming, Land/Development, Hotel/Leisure, Offices, Showrooms/Bulky Good&amp;from_date=2018-04-01&amp;to_date=2018-04-20&amp;sale_from=2000&amp;sale_to=5000&amp;min_land=500&amp;max_land=1000&amp;min_floor=200&amp;max_floor=500&amp;auction_only=1&amp;auction_all=0</code></h4> <h4>For location based searching: <code>http://cview.civitastech.com/api/web/v1/property/search?lat=30.741482&amp;long=76.768066&amp;sort=distance</code>, and for descending <code>http://cview.civitastech.com/api/web/v1/property/search?lat=30.741482&amp;long=76.768066&amp;sort=distance</code></h4> <h2>Response</h2> <ul> <li>Key =&gt; Value</li> <li><em>data.items.propertyType</em> =&gt; PropertyType Id (Represents types like Commercial Farming, Land/Development, Hotel/Leisure, Industrial/Warehouse, etc.)</li> <li><em>data.items.state</em> =&gt; The corresponding state id from the saved states in DB. Refer to GeoState for getting the state id and names</li> <li><em>data.items.country</em> =&gt; The corresponding country id from the saved countries in DB. Refer to GeoCountry for getting the state id and names</li> <li><em>data.items.auctionDetails.live</em> =&gt; True if the auction is currently live</li> <li><em>data.items.auctionDetails.past</em> =&gt; True if the auction has taken place already.</li> <li><em>data.items.auctionDetails.upcoming</em> =&gt; True is the auction will take place in the future.</li> <li><em>data.items.auctionDetails.seconds</em> =&gt; Integer representing number of seconds left for the auction to start, 0 incase of the auction has already taken place or is live right now.</li> <li><em>data.items.auction.datetime_start</em> =&gt; The auction start date</li> <li><em>data.items.auction.auction_status</em> =&gt; Integer representing the type of auction. i.e. 1 =&gt; auction, 2 =&gt; Streaming, 3 =&gt; Offline</li> <li><em>data.items.saveauction</em> =&gt; Will consist a list of users that have saved that auction.</li> </ul>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "suburb",
            "description": "<p>To Filter by city/state/suburb/postcode</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "property_type",
            "description": "<p>Comma separated set of property types</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sale_from",
            "description": "<p>Min Cost of property</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sale_to",
            "description": "<p>Max Cost of property</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "from_date",
            "description": "<p>Auction Start date, format YYYY-M-D eg. 2018-01-23</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "to_date",
            "description": "<p>Auction End date, format YYYY-M-D eg. 2018-01-23</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "min_land",
            "description": "<p>eg. 2032</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "max_land",
            "description": "<p>eg. 23321</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "min_floor",
            "description": "<p>eg. 323</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "max_floor",
            "description": "<p>eg. 2938</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "auction_only",
            "description": "<p>1=&gt;Display only auction types</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "auction_all",
            "description": "<p>1=&gt; Display all auction types i.e. auction|streaming|offline</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "list",
            "description": "<p>1=&gt;Past Property, 2=&gt;Upcoming Property</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>1- For Sale, 2- For Rent. 3 - For Lease</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>(Optional) Comma separated list of sort keys, append (-) for descending order sort.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lat",
            "description": "<p>User's lat, or lat to be used for location filtering</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "long",
            "description": "<p>User's long, or long to be used for location filtering</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "state",
            "description": "<p>state id(from the state api)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{  \n  \"message\":\"\",\n  \"data\":{  \n    \"items\":[  \n      {  \n         \"id\":39,\n         \"title\":\"MULITPLE-TENANCY INVESTMENT WITH UPSIDE DOWN\",\n         \"description\":\"For Sale by Expressions of Interest closing Wednesday 23 May 2018 at 3pm.\\n\\nSubstantial retail/office opportunity with upside.\\n\\n- Land Area: 532m2 approx.\\n- Build Area: 1,190m2 approx.\\n- Combined Income: $351,390 pa plus GST\\n- 4 tenancies\\n- Significant development potential (14 levels - STCA)\\n- Rear access\",\n         \"status\":1,\n         \"propertyType\":4,\n         \"price\":200,\n         \"landArea\":532,\n         \"land_area_unit\":null,\n         \"floorArea\":1190,\n         \"floor_area_unit\":null,\n         \"conditions\":0,\n         \"address\":\"37-41 Hall Street, MOONEE PONDS VIC, 3039 \",\n         \"latitude\":\"-37.6893144\",\n         \"longitude\":\"145.0417777\",\n         \"unitNumber\":0,\n         \"streetNumber\":0,\n         \"streetName\":null,\n         \"suburb\":null,\n         \"city\":\"Richmond\",\n         \"state\":\"Victoria\",\n         \"postcode\":null,\n         \"country\":\"13\",\n         \"parking\":1,\n         \"parkingList\":null,\n         \"parkingOther\":null,\n         \"floorplan\":null,\n         \"media\":null,\n         \"video\":\"https://youtu.be/YXSXe0LVFV8\",\n         \"videoImage\":\"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n         \"document\":\"[{\\\"title\\\":\\\"Commercial view property for sale details\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\\\"},{\\\"title\\\":\\\"Commercial view property for sale details #2\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\\\"}]\",\n         \"school\":1,\n         \"communityCentre\":0,\n         \"parkLand\":0,\n         \"publicTransport\":0,\n         \"shopping\":0,\n         \"bedrooms\":3,\n         \"bathrooms\":4,\n         \"features\":null,\n         \"tags\":null,\n         \"created_by\":null,\n         \"propertyStatus\":0,\n         \"updated_by\":null,\n         \"created_at\":\"2018-05-09 07:43:50\",\n         \"updated_at\":\"2018-05-09 07:43:50\",\n         \"is_featured\":0,\n         \"cview_listing_id\":\"\",\n         \"is_deleted\":0,\n         \"business_email\":null,\n         \"website\":null,\n         \"valuation\":null,\n         \"exclusivity\":null,\n         \"tenancy\":null,\n         \"zoning\":null,\n         \"authority\":null,\n         \"sale_tax\":null,\n         \"tender_closing_date\":null,\n         \"current_lease_expiry\":null,\n         \"outgoings\":null,\n         \"lease_price\":null,\n         \"lease_price_show\":0,\n         \"lease_period\":null,\n         \"lease_tax\":null,\n         \"external_id\":null,\n         \"video_url\":null,\n         \"slug\":null,\n         \"distance\":\"\",\n         \"propertyTypename\":\"Industrial/Warehouse\",\n         \"auctionDetails\":{  \n           \"live\":false,\n           \"past\":false,\n           \"upcoming\":true,\n           \"seconds\":0\n         },\n         \"gallery\":[  \n            {  \n               \"id\":58,\n               \"tbl_pk\":39,\n               \"url\":\"https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727732/d5q2gsgglkg5hsut8qpr.jpg\",\n               \"thumbnail\":null,\n               \"type\":\"image\",\n               \"file_name\":null,\n               \"tbl_name\":\"tbl_properties\"\n            }\n         ],\n         \"auction\":  \n            {  \n               \"id\":58,\n               \"listing_id\":39,\n               \"auction_type\":\"auction\",\n               \"estate_agent_id\":546,\n               \"agency_id\":30,\n               \"date\":\"2018-05-31\",\n               \"time\":\"14:00:00\",\n               \"datetime_start\":\"2018-05-31 14:00:00\",\n               \"datetime_expire\":null,\n               \"starting_price\":null,\n               \"home_phone_number\":null,\n               \"estate_agent\":null,\n               \"picture_of_listing\":null,\n               \"duration\":null,\n               \"real_estate_active_himself\":null,\n               \"real_estate_active_bidders\":null,\n               \"reference_relevant_electronic\":null,\n               \"reference_relevant_bidder\":null,\n               \"created_at\":1525852138,\n               \"auction_status\":1,\n               \"streaming_url\":null,\n               \"completed_streaming_url\":\"\",\n               \"streaming_status\":null,\n               \"templateId\":\"\",\n               \"envelopeId\":\"\",\n               \"documentId\":\"\",\n               \"document\":\"[{\\\"title\\\":\\\"Commercial view property for sale details\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\\\"},{\\\"title\\\":\\\"Commercial view property for sale details #2\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\\\"}]\",\n               \"recipientId\":\"\",\n               \"bidding_status\":2,\n               \"updated_at\":\"2018-05-09\",\n               \"created_by\":0,\n               \"updated_by\":0,\n               \"agent\":{  \n                  \"status\":10,\n                  \"user_type\":10,\n                  \"app_id\":\"2\",\n                  \"id\":546,\n                  \"username\":\"paul_968\",\n                  \"auth_key\":\"EAMP8qC_sdB3lZZcOAvQBLcMqRjgEwKo\",\n                  \"fullName\":\"Paul Bell\",\n                  \"email\":\"eb160e46@commercialview.com.au\",\n                  \"orgName\":null,\n                  \"firstName\":\"Paul\",\n                  \"lastName\":\"Bell\",\n                  \"fbidentifier\":null,\n                  \"linkedin_identifier\":null,\n                  \"google_identifier\":null,\n                  \"role_id\":null,\n                  \"password_hash\":\"\",\n                  \"password_reset_token\":null,\n                  \"phone\":448772355,\n                  \"image\":null,\n                  \"sex\":null,\n                  \"dob\":\"1970-01-01\",\n                  \"ageGroup_id\":1,\n                  \"bdayOffer\":0,\n                  \"walletPinCode\":null,\n                  \"loyaltyPin\":null,\n                  \"advertister_id\":null,\n                  \"activationKey\":null,\n                  \"confirmationKey\":null,\n                  \"access_token\":null,\n                  \"hashKey\":null,\n                  \"device\":\"web\",\n                  \"device_token\":\"\",\n                  \"lastLogin\":null,\n                  \"latitude\":null,\n                  \"longitude\":null,\n                  \"timezone\":null,\n                  \"timezone_offset\":null,\n                  \"created_at\":1527656528,\n                  \"updated_at\":1527656528,\n                  \"storeid\":null,\n                  \"promise_uid\":null,\n                  \"promise_acid\":null,\n                  \"user_code\":\"AW-paul_968\",\n                  \"referralCode\":null,\n                  \"referral_percentage\":null,\n                  \"agency_id\":30,\n                  \"logo\":\"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png\"\n               },\n               \"agency\":{  \n                  \"status\":10,\n                  \"id\":30,\n                  \"cv_agency_id\":\"CV-B4CB59\",\n                  \"name\":\"Paul Bell Real Estate \",\n                  \"email\":\"paul@paulbellrealestate.com.au\",\n                  \"mobile\":448772355,\n                  \"fax\":\"\",\n                  \"logo\":\"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png\",\n                  \"backgroungImage\":null,\n                  \"address\":null,\n                  \"street_address\":null,\n                  \"full_address\":null,\n                  \"suburb_id\":null,\n                  \"latitude\":null,\n                  \"longitude\":null,\n                  \"about_us\":null,\n                  \"slug\":\"paul-bell-real-estate-highton\",\n                  \"abn\":\"\",\n                  \"created_at\":1527656528,\n                  \"updated_at\":1527656528,\n                  \"web\":null,\n                  \"twitter\":\"https://twitter.com/\",\n                  \"facebook\":\"https://www.facebook.com/\",\n                  \"is_favourite\":\"\",\n                  \"instagram\":\"https://www.instagram.com/\",\n                  \"agency_agents\":[  \n                     {  \n                        \"status\":10,\n                        \"user_type\":10,\n                        \"app_id\":\"2\",\n                        \"id\":546,\n                        \"username\":\"paul_968\",\n                        \"auth_key\":\"EAMP8qC_sdB3lZZcOAvQBLcMqRjgEwKo\",\n                        \"fullName\":\"Paul Bell\",\n                        \"email\":\"eb160e46@commercialview.com.au\",\n                        \"orgName\":null,\n                        \"firstName\":\"Paul\",\n                        \"lastName\":\"Bell\",\n                        \"fbidentifier\":null,\n                        \"linkedin_identifier\":null,\n                        \"google_identifier\":null,\n                        \"role_id\":null,\n                        \"password_hash\":\"\",\n                        \"password_reset_token\":null,\n                        \"phone\":448772355,\n                        \"image\":null,\n                        \"sex\":null,\n                        \"dob\":\"1970-01-01\",\n                        \"ageGroup_id\":1,\n                        \"bdayOffer\":0,\n                        \"walletPinCode\":null,\n                        \"loyaltyPin\":null,\n                        \"advertister_id\":null,\n                        \"activationKey\":null,\n                        \"confirmationKey\":null,\n                        \"access_token\":null,\n                        \"hashKey\":null,\n                        \"device\":\"web\",\n                        \"device_token\":\"\",\n                        \"lastLogin\":null,\n                        \"latitude\":null,\n                        \"longitude\":null,\n                        \"timezone\":null,\n                        \"timezone_offset\":null,\n                        \"created_at\":1527656528,\n                        \"updated_at\":1527656528,\n                        \"storeid\":null,\n                        \"promise_uid\":null,\n                        \"promise_acid\":null,\n                        \"user_code\":\"AW-paul_968\",\n                        \"referralCode\":null,\n                        \"referral_percentage\":null,\n                        \"agency_id\":30,\n                        \"logo\":\"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png\"\n                     }\n                  ]\n               }\n            }\n         ],\n         \"saveauction\":null,\n         \"sold\":\"0\",\n         \"calendar\":0\n       },\n       {  \n         \"id\":50,\n         \"title\":\"Looking for a \\\"Juicy\\\" investment?\",\n         \"description\":\"The building is nearing completion and what’s better than already having an excellent tenant from day one! With a 6.5% return locked in for the first three years, plus GST, plus outgoings. (3 x3 lease) $162,500 per annum plus GST/outgoings. I know where I would rather put my money as the financial institutions are paying around 2.5-3% on term deposits, here you will earn double that! Plus!\\r\\n•\\t2842m2 titled land\\r\\n•\\tBrand new purpose-built warehouse with initial 3 x 3 year terms\\r\\n•\\tBrand new building with great depreciation potential. Quantity Survey Available from Agent.\\r\\n•\\tFirst 3 years locked in at 6.5% return on $2.5million = $162,500 plus GST & outgoings paid by tenant\\r\\n•\\tLease will be in place so GST on sale is not applicable\\r\\n•\\tWould suit family super fund, financial investors, financial institutions, etc….\\r\\n•\\tBeing only a segment of an existing business there may be other opportunities for the purchaser to pursue – open to discussion\\r\\n•\\tInspection by appointment only.\\r\\n\",\n         \"status\":0,\n         \"propertyType\":1,\n         \"price\":0,\n         \"landArea\":null,\n         \"land_area_unit\":\"squareMeter\",\n         \"floorArea\":null,\n         \"floor_area_unit\":\"squareMeter\",\n         \"conditions\":0,\n         \"address\":null,\n         \"latitude\":\"-38.182\",\n         \"longitude\":\"144.375\",\n         \"unitNumber\":0,\n         \"streetNumber\":0,\n         \"streetName\":null,\n         \"suburb\":null,\n         \"city\":null,\n         \"state\":null,\n         \"postcode\":null,\n         \"country\":null,\n         \"parking\":0,\n         \"parkingList\":null,\n         \"parkingOther\":null,\n         \"floorplan\":null,\n         \"media\":null,\n         \"video\":null,\n         \"videoImage\":null,\n         \"document\":null,\n         \"school\":0,\n         \"communityCentre\":0,\n         \"parkLand\":0,\n         \"publicTransport\":0,\n         \"shopping\":0,\n         \"bedrooms\":0,\n         \"bathrooms\":0,\n         \"features\":null,\n         \"tags\":null,\n         \"created_by\":538,\n         \"propertyStatus\":0,\n         \"updated_by\":538,\n         \"created_at\":\"2018-05-30 05:02:08\",\n         \"updated_at\":\"2018-05-30 05:02:08\",\n         \"is_featured\":0,\n         \"cview_listing_id\":\"HDS235845S\",\n         \"is_deleted\":0,\n         \"business_email\":null,\n         \"website\":null,\n         \"valuation\":null,\n         \"exclusivity\":\"exclusive\",\n         \"tenancy\":\"tenanted\",\n         \"zoning\":\"Industrial Zone 1\",\n         \"authority\":null,\n         \"sale_tax\":\"unknown\",\n         \"tender_closing_date\":null,\n         \"current_lease_expiry\":null,\n         \"outgoings\":null,\n         \"lease_price\":null,\n         \"lease_price_show\":0,\n         \"lease_period\":\"annual\",\n         \"lease_tax\":\"\",\n         \"external_id\":\"b18c8652-72bf-47cb-ac06-d271ca0b0d8d\",\n         \"video_url\":\"https://youtu.be/YXSXe0LVFV8\",\n         \"slug\":\"breakwater-3219-vic-industrial-warehouse-showrooms-bulky-goods-offices-11294048\",\n         \"distance\":\"\",\n         \"propertyTypename\":\"Commercial Farming\",\n         \"auctionDetails\":null,\n         \"gallery\":[  \n           {  \n             \"id\":71,\n             \"tbl_pk\":50,\n             \"url\":\"https://cview-upload.s3.amazonaws.com/development/uploads/294048/FrontElevation.pdf\",\n             \"thumbnail\":null,\n             \"type\":\"document\",\n             \"file_name\":\"FrontElevation.pdf\",\n             \"tbl_name\":\"tbl_properties\"\n           },\n           {  \n             \"id\":72,\n             \"tbl_pk\":50,\n             \"url\":\"https://youtu.be/YXSXe0LVFV8\",\n             \"thumbnail\":null,\n             \"type\":\"video\",\n             \"file_name\":null,\n             \"tbl_name\":\"tbl_properties\"\n           },\n           {  \n             \"id\":58,\n             \"tbl_pk\":39,\n             \"url\":\"https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727732/d5q2gsgglkg5hsut8qpr.jpg\",\n             \"thumbnail\":null,\n             \"type\":\"image\",\n             \"file_name\":null,\n             \"tbl_name\":\"tbl_properties\"\n           }\n         ],\n         \"auction\":{  \n           \"id\":58,\n           \"listing_id\":39,\n           \"auction_type\":\"auction\",\n           \"estate_agent_id\":546,\n           \"agency_id\":30,\n           \"date\":\"2018-05-31\",\n           \"time\":\"14:00:00\",\n           \"datetime_start\":\"2018-05-31 14:00:00\",\n           \"datetime_expire\":null,\n           \"starting_price\":null,\n           \"home_phone_number\":null,\n           \"estate_agent\":null,\n           \"picture_of_listing\":null,\n           \"duration\":null,\n           \"real_estate_active_himself\":null,\n           \"real_estate_active_bidders\":null,\n           \"reference_relevant_electronic\":null,\n           \"reference_relevant_bidder\":null,\n           \"created_at\":1525852138,\n           \"auction_status\":1,\n           \"streaming_url\":null,\n           \"completed_streaming_url\":\"\",\n           \"streaming_status\":null,\n           \"templateId\":\"\",\n           \"envelopeId\":\"\",\n           \"documentId\":\"\",\n           \"document\":\"[{\\\"title\\\":\\\"Commercial view property for sale details\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\\\"},{\\\"title\\\":\\\"Commercial view property for sale details #2\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\\\"}]\",\n           \"recipientId\":\"\",\n           \"bidding_status\":2,\n           \"updated_at\":\"2018-05-09\",\n           \"created_by\":0,\n           \"updated_by\":0,\n           \"agent\":{  \n              \"status\":10,\n              \"user_type\":10,\n              \"app_id\":\"2\",\n              \"id\":546,\n              \"username\":\"paul_968\",\n              \"auth_key\":\"EAMP8qC_sdB3lZZcOAvQBLcMqRjgEwKo\",\n              \"fullName\":\"Paul Bell\",\n              \"email\":\"eb160e46@commercialview.com.au\",\n              \"orgName\":null,\n              \"firstName\":\"Paul\",\n              \"lastName\":\"Bell\",\n              \"fbidentifier\":null,\n              \"linkedin_identifier\":null,\n              \"google_identifier\":null,\n              \"role_id\":null,\n              \"password_hash\":\"\",\n              \"password_reset_token\":null,\n              \"phone\":448772355,\n              \"image\":null,\n              \"sex\":null,\n              \"dob\":\"1970-01-01\",\n              \"ageGroup_id\":1,\n              \"bdayOffer\":0,\n              \"walletPinCode\":null,\n              \"loyaltyPin\":null,\n              \"advertister_id\":null,\n              \"activationKey\":null,\n              \"confirmationKey\":null,\n              \"access_token\":null,\n              \"hashKey\":null,\n              \"device\":\"web\",\n              \"device_token\":\"\",\n              \"lastLogin\":null,\n              \"latitude\":null,\n              \"longitude\":null,\n              \"timezone\":null,\n              \"timezone_offset\":null,\n              \"created_at\":1527656528,\n              \"updated_at\":1527656528,\n              \"storeid\":null,\n              \"promise_uid\":null,\n              \"promise_acid\":null,\n              \"user_code\":\"AW-paul_968\",\n              \"referralCode\":null,\n              \"referral_percentage\":null,\n              \"agency_id\":30,\n              \"logo\":\"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png\"\n           },\n           \"agency\":{  \n             \"status\":10,\n             \"id\":30,\n             \"cv_agency_id\":\"CV-B4CB59\",\n             \"name\":\"Paul Bell Real Estate \",\n             \"email\":\"paul@paulbellrealestate.com.au\",\n             \"mobile\":448772355,\n             \"fax\":\"\",\n             \"logo\":\"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png\",\n             \"backgroungImage\":null,\n             \"address\":null,\n             \"street_address\":null,\n             \"full_address\":null,\n             \"suburb_id\":null,\n             \"latitude\":null,\n             \"longitude\":null,\n             \"about_us\":null,\n             \"slug\":\"paul-bell-real-estate-highton\",\n             \"abn\":\"\",\n             \"created_at\":1527656528,\n             \"updated_at\":1527656528,\n             \"web\":null,\n             \"twitter\":\"https://twitter.com/\",\n             \"facebook\":\"https://www.facebook.com/\",\n             \"is_favourite\":\"\",\n             \"instagram\":\"https://www.instagram.com/\",\n             \"agency_agents\":[  \n               {  \n                 \"status\":10,\n                 \"user_type\":10,\n                 \"app_id\":\"2\",\n                 \"id\":546,\n                 \"username\":\"paul_968\",\n                 \"auth_key\":\"EAMP8qC_sdB3lZZcOAvQBLcMqRjgEwKo\",\n                 \"fullName\":\"Paul Bell\",\n                 \"email\":\"eb160e46@commercialview.com.au\",\n                 \"orgName\":null,\n                 \"firstName\":\"Paul\",\n                 \"lastName\":\"Bell\",\n                 \"fbidentifier\":null,\n                 \"linkedin_identifier\":null,\n                 \"google_identifier\":null,\n                 \"role_id\":null,\n                 \"password_hash\":\"\",\n                 \"password_reset_token\":null,\n                 \"phone\":448772355,\n                 \"image\":null,\n                 \"sex\":null,\n                 \"dob\":\"1970-01-01\",\n                 \"ageGroup_id\":1,\n                 \"bdayOffer\":0,\n                 \"walletPinCode\":null,\n                 \"loyaltyPin\":null,\n                 \"advertister_id\":null,\n                 \"activationKey\":null,\n                 \"confirmationKey\":null,\n                 \"access_token\":null,\n                 \"hashKey\":null,\n                 \"device\":\"web\",\n                 \"device_token\":\"\",\n                 \"lastLogin\":null,\n                 \"latitude\":null,\n                 \"longitude\":null,\n                 \"timezone\":null,\n                 \"timezone_offset\":null,\n                 \"created_at\":1527656528,\n                 \"updated_at\":1527656528,\n                 \"storeid\":null,\n                 \"promise_uid\":null,\n                 \"promise_acid\":null,\n                 \"user_code\":\"AW-paul_968\",\n                 \"referralCode\":null,\n                 \"referral_percentage\":null,\n                 \"agency_id\":30,\n                 \"logo\":\"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png\"\n               }\n             ]\n           }\n         },\n         \"saveauction\":null,\n         \"sold\":\"0\",\n         \"calendar\":0\n      },\n      {  \n        \"id\":50,\n        \"title\":\"Looking for a \\\"Juicy\\\" investment?\",\n        \"description\":\"The building is nearing completion and what’s better than already having an excellent tenant from day one! With a 6.5% return locked in for the first three years, plus GST, plus outgoings. (3 x3 lease) $162,500 per annum plus GST/outgoings. I know where I would rather put my money as the financial institutions are paying around 2.5-3% on term deposits, here you will earn double that! Plus!\\r\\n•\\t2842m2 titled land\\r\\n•\\tBrand new purpose-built warehouse with initial 3 x 3 year terms\\r\\n•\\tBrand new building with great depreciation potential. Quantity Survey Available from Agent.\\r\\n•\\tFirst 3 years locked in at 6.5% return on $2.5million = $162,500 plus GST & outgoings paid by tenant\\r\\n•\\tLease will be in place so GST on sale is not applicable\\r\\n•\\tWould suit family super fund, financial investors, financial institutions, etc….\\r\\n•\\tBeing only a segment of an existing business there may be other opportunities for the purchaser to pursue – open to discussion\\r\\n•\\tInspection by appointment only.\\r\\n\",\n        \"status\":0,\n        \"propertyType\":1,\n        \"price\":0,\n        \"landArea\":null,\n        \"land_area_unit\":\"squareMeter\",\n        \"floorArea\":null,\n        \"floor_area_unit\":\"squareMeter\",\n        \"conditions\":0,\n        \"address\":null,\n        \"latitude\":\"-38.182\",\n        \"longitude\":\"144.375\",\n        \"unitNumber\":0,\n        \"streetNumber\":0,\n        \"streetName\":null,\n        \"suburb\":null,\n        \"city\":null,\n        \"state\":null,\n        \"postcode\":null,\n        \"country\":null,\n        \"parking\":0,\n        \"parkingList\":null,\n        \"parkingOther\":null,\n        \"floorplan\":null,\n        \"media\":null,\n        \"video\":null,\n        \"videoImage\":null,\n        \"document\":null,\n        \"school\":0,\n        \"communityCentre\":0,\n        \"parkLand\":0,\n        \"publicTransport\":0,\n        \"shopping\":0,\n        \"bedrooms\":0,\n        \"bathrooms\":0,\n        \"features\":null,\n        \"tags\":null,\n        \"created_by\":538,\n        \"propertyStatus\":0,\n        \"updated_by\":538,\n        \"created_at\":\"2018-05-30 05:02:08\",\n        \"updated_at\":\"2018-05-30 05:02:08\",\n        \"is_featured\":0,\n        \"cview_listing_id\":\"HDS235845S\",\n        \"is_deleted\":0,\n        \"business_email\":null,\n        \"website\":null,\n        \"valuation\":null,\n        \"exclusivity\":\"exclusive\",\n        \"tenancy\":\"tenanted\",\n        \"zoning\":\"Industrial Zone 1\",\n        \"authority\":null,\n        \"sale_tax\":\"unknown\",\n        \"tender_closing_date\":null,\n        \"current_lease_expiry\":null,\n        \"outgoings\":null,\n        \"lease_price\":null,\n        \"lease_price_show\":0,\n        \"lease_period\":\"annual\",\n        \"lease_tax\":\"\",\n        \"external_id\":\"b18c8652-72bf-47cb-ac06-d271ca0b0d8d\",\n        \"video_url\":\"https://youtu.be/YXSXe0LVFV8\",\n        \"slug\":\"breakwater-3219-vic-industrial-warehouse-showrooms-bulky-goods-offices-11294048\",\n        \"distance\":\"\",\n        \"propertyTypename\":\"Commercial Farming\",\n        \"auctionDetails\":null,\n        \"gallery\":[  \n          {  \n            \"id\":71,\n            \"tbl_pk\":50,\n            \"url\":\"https://cview-upload.s3.amazonaws.com/development/uploads/294048/FrontElevation.pdf\",\n            \"thumbnail\":null,\n            \"type\":\"document\",\n            \"file_name\":\"FrontElevation.pdf\",\n            \"tbl_name\":\"tbl_properties\"\n          },\n          {  \n            \"id\":72,\n            \"tbl_pk\":50,\n            \"url\":\"https://youtu.be/YXSXe0LVFV8\",\n            \"thumbnail\":null,\n            \"type\":\"video\",\n            \"file_name\":null,\n            \"tbl_name\":\"tbl_properties\"\n          },\n          {  \n            \"id\":73,\n            \"tbl_pk\":50,\n            \"url\":\"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1523514445/d4wlukvypczhex4jjm0w.jpg\",\n            \"thumbnail\":null,\n            \"type\":\"image\",\n            \"file_name\":null,\n            \"tbl_name\":\"tbl_properties\"\n          },\n          {  \n            \"id\":74,\n            \"tbl_pk\":50,\n            \"url\":\"https://dduaaywsz-res-4.cloudinary.com/image/upload/v1523514435/v2kjd0f14jzwkej5lyax.jpg\",\n            \"thumbnail\":null,\n            \"type\":\"image\",\n            \"file_name\":null,\n            \"tbl_name\":\"tbl_properties\"\n          }\n        ],\n        \"auction\":null,\n        \"saveauction\":null,\n        \"sold\":\"0\",\n        \"calendar\":0\n      }\n    ],\n    \"_links\":{  \n      \"self\":{  \n        \"href\":\"http://cview.civitastech.com/api/web/v1/properties/search?page=2\"\n      },\n      \"first\":{  \n        \"href\":\"http://cview.civitastech.com/api/web/v1/properties/search?page=1\"\n      },\n      \"prev\":{  \n        \"href\":\"http://cview.civitastech.com/api/web/v1/properties/search?page=1\"\n      }\n    },\n    \"_meta\":{  \n      \"totalCount\":25,\n      \"pageCount\":2,\n      \"currentPage\":2,\n      \"perPage\":20\n    }\n  },\n  \"status\":200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"some filedname\",\n      \"message\": \"Some message\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/PropertyController.php",
    "groupTitle": "Property"
  },
  {
    "type": "get",
    "url": "/api/web/v1/property-type/list",
    "title": "All Active PropertyTypes",
    "name": "All_Active_PropertyTypes",
    "version": "0.1.1",
    "group": "PropertyTypes",
    "description": "<p>Fetch the all active PropertyTypes</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"id\": 1,\n      \"name\": \"Commercial Farming\"\n    },\n    {\n      \"id\": 2,\n      \"name\": \"Land/Development\"\n    },\n    {\n      \"id\": 3,\n      \"name\": \"Hotel/Leisure\"\n    },\n    {\n      \"id\": 4,\n      \"name\": \"Industrial/Warehouse\"\n    },\n    {\n      \"id\": 5,\n      \"name\": \"Medical/Consulting\"\n    },\n    {\n      \"id\": 6,\n      \"name\": \"Offices\"\n    },\n    {\n      \"id\": 7,\n      \"name\": \"Retail\"\n    },\n    {\n      \"id\": 8,\n      \"name\": \"Showrooms/Bulky Goods\"\n    },\n    {\n      \"id\": 9,\n      \"name\": \"Other\"\n    }\n  ],\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/PropertyTypeController.php",
    "groupTitle": "PropertyTypes"
  },
  {
    "type": "post",
    "url": "/api/web/v1/property-type/create",
    "title": "Create Property Type",
    "name": "CreateProperty_type",
    "version": "0.1.1",
    "group": "PropertyTypes",
    "description": "<p>To create new property type, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>property type name.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>1:Enable, 0:disable</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n       \"name\": \"other1\", \n       \"status\": \"1\",\n       \"id\": 10\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 422 Validation Error\n    {\n      \"message\": \"\",\n      \"data\": [\n        {\n          \"field\": \"name\",\n          \"message\": \"Name \\\"Doe\\\" has already been taken. \"\n        },\n      ],\n      \"status\": 422\n    }\nOr\n   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n            {\n\t\"field\": \"name\",\n\t\"message\": \"Name cannot be blank.\"\n\t},\n\t{\n\t\"field\": \"status\",\n\t\"message\": \"Status cannot be blank.\"\n\t}\n      ],\n      \"status\": 422\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/PropertyTypeController.php",
    "groupTitle": "PropertyTypes"
  },
  {
    "type": "get",
    "url": "api/web/v1/property/view?id=39",
    "title": "View Property Detail",
    "name": "View_Property_Details",
    "version": "0.1.1",
    "group": "Property",
    "description": "<p>To view property detail, Form-Data must be x-www-form-urlencoded</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n{\n   \"message\": \"\",\n   \"data\": {\n     \"id\": 39,\n     \"title\": \"MULITPLE-TENANCY INVESTMENT WITH UPSIDE DOWN\",\n     \"description\": \"For Sale by Expressions of Interest closing Wednesday 23 May 2018 at 3pm.\\n\\nSubstantial retail/office opportunity with upside.\\n\\n- Land Area: 532m2 approx.\\n- Build Area: 1,190m2 approx.\\n- Combined Income: $351,390 pa plus GST\\n- 4 tenancies\\n- Significant development potential (14 levels - STCA)\\n- Rear access\",\n     \"status\": 1,\n     \"propertyType\": 4,\n     \"price\": 200,\n     \"landArea\": 532,\n     \"land_area_unit\": null,\n     \"floorArea\": 1190,\n     \"floor_area_unit\": null,\n     \"conditions\": 0,\n     \"address\": \"37-41 Hall Street, MOONEE PONDS VIC, 3039 \",\n     \"latitude\": \"-37.6893144\",\n     \"longitude\": \"145.0417777\",\n     \"unitNumber\": 0,\n     \"streetNumber\": 0,\n     \"streetName\": null,\n     \"suburb\": null,\n     \"city\": \"Richmond\",\n     \"state\": \"Victoria\",\n     \"postcode\": null,\n     \"country\": \"13\",\n     \"parking\": 1,\n     \"parkingList\": null,\n     \"parkingOther\": null,\n     \"floorplan\": null,\n     \"media\": null,\n     \"video\": \"https://youtu.be/YXSXe0LVFV8\",\n     \"videoImage\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n     \"document\": \"[{\\\"title\\\":\\\"Commercial view property for sale details\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\\\"},{\\\"title\\\":\\\"Commercial view property for sale details #2\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\\\"}]\",\n     \"school\": 1,\n     \"communityCentre\": 0,\n     \"parkLand\": 0,\n     \"publicTransport\": 0,\n     \"shopping\": 0,\n     \"bedrooms\": 3,\n     \"bathrooms\": 4,\n     \"features\": null,\n     \"tags\": null,\n     \"created_by\": null,\n     \"propertyStatus\": 0,\n     \"updated_by\": null,\n     \"created_at\": \"2018-05-09 07:43:50\",\n     \"updated_at\": \"2018-05-09 07:43:50\",\n     \"is_featured\": 0,\n     \"cview_listing_id\": \"\",\n     \"is_deleted\": 0,\n     \"business_email\": null,\n     \"website\": null,\n     \"valuation\": null,\n     \"exclusivity\": null,\n     \"tenancy\": null,\n     \"zoning\": null,\n     \"authority\": null,\n     \"sale_tax\": null,\n     \"tender_closing_date\": null,\n     \"current_lease_expiry\": null,\n     \"outgoings\": null,\n     \"lease_price\": null,\n     \"lease_price_show\": 0,\n     \"lease_period\": null,\n     \"lease_tax\": null,\n     \"external_id\": null,\n     \"video_url\": null,\n     \"slug\": null,\n     \"distance\": \"\",\n     \"propertyTypename\": \"Industrial/Warehouse\",\n     \"auctionDetails\": {\n       \"live\": false,\n       \"past\": false,\n       \"upcoming\": false,\n       \"seconds\": 0\n     },\n     \"gallery\": [\n       {\n         \"id\": 58,\n         \"tbl_pk\": 39,\n         \"url\": \"https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727732/d5q2gsgglkg5hsut8qpr.jpg\",\n         \"thumbnail\": null,\n         \"type\": \"image\",\n         \"file_name\": null,\n         \"tbl_name\": \"tbl_properties\"\n       }\n     ],\n     \"auction\": {\n       \"id\": 58,\n       \"listing_id\": 39,\n       \"auction_type\": \"auction\",\n       \"estate_agent_id\": 546,\n       \"agency_id\": 30,\n       \"date\": \"2018-05-31\",\n       \"time\": \"14:00:00\",\n       \"datetime_start\": \"2018-05-31 14:00:00\",\n       \"datetime_expire\": null,\n       \"starting_price\": null,\n       \"home_phone_number\": null,\n       \"estate_agent\": null,\n       \"picture_of_listing\": null,\n       \"duration\": null,\n       \"real_estate_active_himself\": null,\n       \"real_estate_active_bidders\": null,\n       \"reference_relevant_electronic\": null,\n       \"reference_relevant_bidder\": null,\n       \"created_at\": 1525852138,\n       \"auction_status\": 1,\n       \"streaming_url\": null,\n       \"completed_streaming_url\": \"\",\n       \"streaming_status\": null,\n       \"templateId\": \"\",\n       \"envelopeId\": \"\",\n       \"documentId\": \"\",\n       \"document\": \"[{\\\"title\\\":\\\"Commercial view property for sale details\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\\\"},{\\\"title\\\":\\\"Commercial view property for sale details #2\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\\\"}]\",\n       \"recipientId\": \"\",\n       \"bidding_status\": 2,\n       \"updated_at\": \"2018-05-09\",\n       \"created_by\": 0,\n       \"updated_by\": 0,\n       \"agent\": null,\n       \"agency\": null\n     },\n     \"saveauction\": null,\n     \"sold\": \"0\",\n     \"calendar\": 0\n   },\n   \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"No Record Found\",\n  \"data\":  {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/PropertyController.php",
    "groupTitle": "Property"
  },
  {
    "type": "post",
    "url": "api/web/v1/webhooks/property/create-property",
    "title": "Create New Property",
    "name": "CreateProperty",
    "version": "0.1.1",
    "group": "Property_Webhooks",
    "description": "<p>To Create Property (Webhook), Form-Data must be raw type</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"title\": \"New Property as per the testing\",\n    \"description\": \"The building is nearing completion and what’s better than already having an excellent tenant from day one! With a 6.5% return locked in for the first three years, plus GST, plus outgoings. (3 x3 lease) $162,500 per annum plus GST/outgoings.\",\n    \"status\": 1,\n    \"propertyType\": 7,\n    \"price\": 2500000,\n    \"landArea\": 2842,\n    \"land_area_unit\": \"squareMeter\",\n    \"floorArea\": 2600,\n    \"floor_area_unit\": \"squareMeter\",\n    \"conditions\": 0,\n    \"address\": \"Doltone House Hyde Park, 181 Elizabeth Street, Sydney\",\n    \"latitude\": \"-38.182\",\n    \"longitude\": \"144.375\",\n    \"unitNumber\": 0,\n    \"streetNumber\": 0,\n    \"streetName\": null,\n    \"suburb\": null,\n    \"city\": \"Sydney\",\n    \"state\": \"Sydney\",\n    \"postcode\": 135420,\n    \"country\": \"Australia\",\n    \"parking\": 0,\n    \"parkingList\": null,\n    \"parkingOther\": null,\n    \"floorplan\": null,\n    \"media\": null,\n    \"video\": null,\n    \"videoImage\": null,\n    \"document\": null,\n    \"school\": 0,\n    \"communityCentre\": 0,\n    \"parkLand\": 0,\n    \"publicTransport\": 0,\n    \"shopping\": 0,\n    \"bedrooms\": 3,\n    \"bathrooms\": 3,\n    \"features\": \"Adjoining Shopping Center\",\n    \"tags\": null,\n    \"created_by\": null,\n    \"propertyStatus\": 0,\n    \"updated_by\": null,\n    \"created_at\": \"2018-05-24 11:22:55\",\n    \"updated_at\": \"2018-05-24 11:22:55\",\n    \"is_featured\": 0,\n    \"cview_listing_id\": \"NEW250518-1\",\n    \"is_deleted\": 0,\n    \"business_email\": \"property@yopmail.com\",\n    \"website\": \"www.property.com\",\n    \"valuation\": null,\n    \"exclusivity\": \"exclusive\",\n    \"tenancy\": \"tenanted\",\n    \"zoning\": \"Industrial Zone 1\",\n    \"authority\": null,\n    \"sale_tax\": \"unknown\",\n    \"tender_closing_date\": null,\n    \"outgoings\": null,\n    \"lease_price\": null,\n    \"lease_price_show\": 1,\n    \"lease_period\": \"anual\",\n    \"lease_tax\": null,\n    \"external_id\": \"b18c8652-72bf-47cb-ac06-d271ca0b0d8d\",\n    \"video_url\": null,\n    \"distance\": \"\",\n    \"propertyTypename\": \"Retail\",\n    \"auctionDetails\": null,\n    \"gallery\": [\n      {\n        \"url\": \"http://localhost/AlphaWallet/html/frontend/web/images/property/Property_image935.jpg\",\n        \"type\": \"image/png\",\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"url\": \"http://localhost/AlphaWallet/html/frontend/web/images/property/Property_image838.jpg\",\n        \"type\": \"image/png\",\n        \"tbl_name\": \"tbl_properties\"\n      }\n    ],\n    \"auction\": []\n  }\n}",
          "type": "json"
        },
        {
          "title": "Success Response:",
          "content": "{\n  \"message\": \"\",\n  \"data\": {\n    \"title\": \"New Property as per the testing\",\n    \"description\": \"The building is nearing completion and what’s better than already having an excellent tenant from day one! With a 6.5% return locked in for the first three years, plus GST, plus outgoings. (3 x3 lease) $162,500 per annum plus GST/outgoings.\",\n    \"status\": 1,\n    \"propertyType\": 7,\n    \"price\": 2500000,\n    \"landArea\": 2842,\n    \"land_area_unit\": \"squareMeter\",\n    \"floorArea\": 2600,\n    \"floor_area_unit\": \"squareMeter\",\n    \"address\": \"Doltone House Hyde Park, 181 Elizabeth Street, Sydney\",\n    \"latitude\": \"-38.182\",\n    \"longitude\": \"144.375\",\n    \"city\": \"Sydney\",\n    \"state\": \"Sydney\",\n    \"country\": \"Australia\",\n    \"parking\": 0,\n    \"video\": null,\n    \"videoImage\": null,\n    \"school\": 0,\n    \"communityCentre\": 0,\n    \"parkLand\": 0,\n    \"shopping\": 0,\n    \"bedrooms\": 3,\n    \"bathrooms\": 3,\n    \"created_by\": null,\n    \"propertyStatus\": 0,\n    \"updated_by\": null,\n    \"created_at\": 1527233752,\n    \"updated_at\": 1527233752,\n    \"cview_listing_id\": \"NEW250518-1\",\n    \"is_deleted\": 0,\n    \"business_email\": \"property@yopmail.com\",\n    \"website\": \"www.property.com\",\n    \"valuation\": null,\n    \"exclusivity\": \"exclusive\",\n    \"tenancy\": \"tenanted\",\n    \"zoning\": \"Industrial Zone 1\",\n    \"authority\": null,\n    \"sale_tax\": \"unknown\",\n    \"tender_closing_date\": null,\n    \"outgoings\": null,\n    \"lease_price\": null,\n    \"lease_price_show\": 1,\n    \"lease_period\": \"anual\",\n    \"lease_tax\": null,\n    \"external_id\": \"b18c8652-72bf-47cb-ac06-d271ca0b0d8d\",\n    \"video_url\": null,\n    \"id\": 161,\n    \"distance\": \"\",\n    \"propertyTypename\": \"Retail\",\n    \"auctionDetails\": null,\n    \"gallery\": [\n      {\n        \"id\": 144,\n        \"tbl_pk\": 161,\n        \"url\": \"http://localhost/AlphaWallet/html/frontend/web/images/property/Property_image935.jpg\",\n        \"type\": \"image/png\",\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"id\": 145,\n        \"tbl_pk\": 161,\n        \"url\": \"http://localhost/AlphaWallet/html/frontend/web/images/property/Property_image838.jpg\",\n        \"type\": \"image/png\",\n        \"tbl_name\": \"tbl_properties\"\n      }\n    ],\n    \"auction\": [],\n    \"saveauction\": null,\n    \"sold\": \"0\",\n    \"calendar\": 0\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/PropertyController.php",
    "groupTitle": "Property_Webhooks"
  },
  {
    "type": "post",
    "url": "api/web/v1/webhooks/property/delete-property",
    "title": "Remove Property",
    "name": "DeleteProperty",
    "version": "0.1.1",
    "group": "Property_Webhooks",
    "description": "<p>To Remove property (Webhook), Form-Data must be raw type</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"id\": 153\n  }\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 422 Unprocessable entity\n    {\n      \"message\": \"No Record Found\",\n      \"data\":  {\n        \"status\": \"error\"\n\t\t },\n      \"status\": 422\n    }",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"Property has been deleted successfully\",\n  \"data\":  [],\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/PropertyController.php",
    "groupTitle": "Property_Webhooks"
  },
  {
    "type": "get",
    "url": "api/web/v1/webhooks/property/all",
    "title": "Property Listing",
    "name": "PropertyListing",
    "version": "0.1.1",
    "group": "Property_Webhooks",
    "description": "<p>List all Property, Result Set is in Pagination of 10 records in each page. To fetch resultset in pagination url would like this: api/web/v1/webhooks/property/all?page=2, Where page is the page number.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"message\": \"\",\n  \"data\": {\n    \"items\": [\n      {\n        \"id\": 178,\n        \"title\": \"Final land release in Crestmead Industrial Estate\",\n        \"description\": \"The estate borders Crestmead, Browns Plains and Marsden - the fastest growing residential sector in the Logan region. It is a mixed industrial estate in a serviced industrial area designed to cater for the manufacturing industry.\\n\\nProvides access to interstate and overseas markets via the Gateway Arterial road and the Port of Brisbane.\\n\\nVisit www.industrial.edq.com.au for prices and availability.\",\n        \"status\": 0,\n        \"propertyType\": 1,\n        \"price\": 0,\n        \"landArea\": null,\n        \"land_area_unit\": \"squareMeter\",\n        \"floorArea\": null,\n        \"floor_area_unit\": null,\n        \"conditions\": 0,\n        \"address\": \"Magnesium Drive QLD 4132\",\n        \"latitude\": \"-37.899\",\n        \"longitude\": \"145.183\",\n        \"unitNumber\": 0,\n        \"streetNumber\": 0,\n        \"streetName\": null,\n        \"suburb\": null,\n        \"city\": null,\n        \"state\": null,\n        \"postcode\": null,\n        \"country\": null,\n        \"parking\": 0,\n        \"parkingList\": null,\n        \"parkingOther\": null,\n        \"floorplan\": null,\n        \"media\": null,\n        \"video\": null,\n        \"videoImage\": null,\n        \"document\": null,\n        \"school\": 0,\n        \"communityCentre\": 0,\n        \"parkLand\": 0,\n        \"publicTransport\": 0,\n        \"shopping\": 0,\n        \"bedrooms\": 0,\n        \"bathrooms\": 0,\n        \"features\": null,\n        \"tags\": null,\n        \"created_by\": 383,\n        \"propertyStatus\": 0,\n        \"updated_by\": 383,\n        \"created_at\": \"2018-05-31 06:53:52\",\n        \"updated_at\": \"2018-05-31 06:53:52\",\n        \"is_featured\": 0,\n        \"cview_listing_id\": \"11261275\",\n        \"is_deleted\": 0,\n        \"business_email\": null,\n        \"website\": null,\n        \"valuation\": null,\n        \"exclusivity\": \"exclusive\",\n        \"tenancy\": null,\n        \"zoning\": null,\n        \"authority\": null,\n        \"sale_tax\": \"unknown\",\n        \"tender_closing_date\": null,\n        \"current_lease_expiry\": null,\n        \"outgoings\": null,\n        \"lease_price\": null,\n        \"lease_price_show\": 0,\n        \"lease_period\": null,\n        \"lease_tax\": \"unknown\",\n        \"external_id\": \"CL17301272\",\n        \"video_url\": \"\",\n        \"slug\": \"crestmead-4132-qld-land-development-c170ff27-4c73-45cf-a61a-fd15aac3080e\",\n        \"distance\": \"\",\n        \"propertyTypename\": \"Commercial Farming\",\n        \"auctionDetails\": {\n          \"live\": false,\n          \"past\": true,\n          \"upcoming\": false,\n          \"seconds\": 0\n        },           \n        \"gallery\": [],\n        \"auction\": null,\n        \"saveauction\": null,\n        \"sold\": \"0\",\n        \"calendar\": 0\n      }\n    ],\n    \"_links\": {\n      \"self\": {\n        \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/webhooks/property/all?page=1\"\n      },\n      \"next\": {\n        \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/webhooks/property/all?page=2\"\n      },\n      \"last\": {\n        \"href\": \"http://localhost/AlphaWallet/html/api/web/v1/webhooks/property/all?page=4\"\n      }\n    },\n    \"_meta\": {\n      \"totalCount\": 80,\n      \"pageCount\": 4,\n      \"currentPage\": 1,\n      \"perPage\": 20\n    }\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/PropertyController.php",
    "groupTitle": "Property_Webhooks"
  },
  {
    "type": "post",
    "url": "api/web/v1/property/update-property",
    "title": "Update Property",
    "name": "UpdateProperty",
    "version": "0.1.1",
    "group": "Property_Webhooks",
    "description": "<p>To Update Property (Webhook), Form-Data must be raw type</p>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 161,\n    \"title\": \"Property Edited as per the testing\",\n    \"description\": \"The building is nearing completion and what’s better than already having an excellent tenant from day one! With a 6.5% return locked in for the first three years, plus GST, plus outgoings. (3 x3 lease) $162,500 per annum plus GST/outgoings.\",\n    \"status\": 1,\n    \"propertyType\": 7,\n    \"price\": 2500000,\n    \"landArea\": 2842,\n    \"land_area_unit\": \"squareMeter\",\n    \"floorArea\": 2600,\n    \"floor_area_unit\": \"squareMeter\",\n    \"conditions\": 0,\n    \"address\": \"Doltone House Hyde Park, 181 Elizabeth Street, Sydney\",\n    \"latitude\": \"-38.182\",\n    \"longitude\": \"144.375\",\n    \"unitNumber\": 0,\n    \"streetNumber\": 0,\n    \"streetName\": null,\n    \"suburb\": null,\n    \"city\": \"Sydney\",\n    \"state\": \"Sydney\",\n    \"postcode\": null,\n    \"country\": 13,\n    \"parking\": 0,\n    \"parkingList\": null,\n    \"parkingOther\": null,\n    \"floorplan\": null,\n    \"media\": null,\n    \"video\": null,\n    \"videoImage\": null,\n    \"document\": null,\n    \"school\": 0,\n    \"communityCentre\": 0,\n    \"parkLand\": 0,\n    \"publicTransport\": 0,\n    \"shopping\": 0,\n    \"bedrooms\": 3,\n    \"bathrooms\": 3,\n    \"features\": null,\n    \"tags\": null,\n    \"created_by\": null,\n    \"propertyStatus\": 0,\n    \"updated_by\": null,\n    \"created_at\": \"2018-05-25 07:35:52\",\n    \"updated_at\": \"2018-05-25 07:35:52\",\n    \"is_featured\": 0,\n    \"cview_listing_id\": \"NEW250518-1\",\n    \"is_deleted\": 0,\n    \"business_email\": \"property@yopmail.com\",\n    \"website\": \"www.property.com\",\n    \"valuation\": null,\n    \"exclusivity\": \"exclusive\",\n    \"tenancy\": \"tenanted\",\n    \"zoning\": \"Industrial Zone 1\",\n    \"authority\": null,\n    \"sale_tax\": \"unknown\",\n    \"tender_closing_date\": null,\n    \"outgoings\": null,\n    \"lease_price\": null,\n    \"lease_price_show\": 1,\n    \"lease_period\": \"anual\",\n    \"lease_tax\": null,\n    \"external_id\": \"b18c8652-72bf-47cb-ac06-d271ca0b0d8d\",\n    \"video_url\": null,\n    \"distance\": \"\",\n    \"propertyTypename\": \"Retail\",\n    \"auctionDetails\": null,\n    \"gallery\": [\n      {\n        \"id\": 144,\n        \"tbl_pk\": 161,\n        \"url\": \"http://localhost/AlphaWallet/html/frontend/web/images/property/Property_image935.jpg\",\n        \"type\": \"image/png\",\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"id\": 145,\n        \"tbl_pk\": 161,\n        \"url\": \"http://localhost/AlphaWallet/html/frontend/web/images/property/Property_image838.jpg\",\n        \"type\": \"image/png\",\n        \"tbl_name\": \"tbl_properties\"\n      }\n    ],\n    \"auction\": [],\n    \"saveauction\": null,\n    \"sold\": \"0\",\n    \"calendar\": 0\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error Property not found",
          "content": "{\n  \"message\": \"No Record Found\",\n  \"data\": {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success Response:",
          "content": "{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 161,\n    \"title\": \"Property Edited as per the testing\",\n    \"description\": \"The building is nearing completion and what’s better than already having an excellent tenant from day one! With a 6.5% return locked in for the first three years, plus GST, plus outgoings. (3 x3 lease) $162,500 per annum plus GST/outgoings.\",\n    \"status\": 1,\n    \"propertyType\": 7,\n    \"price\": 2500000,\n    \"landArea\": 2842,\n    \"land_area_unit\": \"squareMeter\",\n    \"floorArea\": 2600,\n    \"floor_area_unit\": \"squareMeter\",\n    \"conditions\": 0,\n    \"address\": \"Doltone House Hyde Park, 181 Elizabeth Street, Sydney\",\n    \"latitude\": \"-38.182\",\n    \"longitude\": \"144.375\",\n    \"unitNumber\": 0,\n    \"streetNumber\": 0,\n    \"streetName\": null,\n    \"suburb\": null,\n    \"city\": \"Sydney\",\n    \"state\": \"Sydney\",\n    \"postcode\": null,\n    \"country\": 13,\n    \"parking\": 0,\n    \"parkingList\": null,\n    \"parkingOther\": null,\n    \"floorplan\": null,\n    \"media\": null,\n    \"video\": null,\n    \"videoImage\": null,\n    \"document\": null,\n    \"school\": 0,\n    \"communityCentre\": 0,\n    \"parkLand\": 0,\n    \"publicTransport\": 0,\n    \"shopping\": 0,\n    \"bedrooms\": 3,\n    \"bathrooms\": 3,\n    \"features\": null,\n    \"tags\": null,\n    \"created_by\": null,\n    \"propertyStatus\": 0,\n    \"updated_by\": null,\n    \"created_at\": \"2018-05-25 07:35:52\",\n    \"updated_at\": \"2018-05-25 07:35:52\",\n    \"is_featured\": 0,\n    \"cview_listing_id\": \"NEW250518-1\",\n    \"is_deleted\": 0,\n    \"business_email\": \"property@yopmail.com\",\n    \"website\": \"www.property.com\",\n    \"valuation\": null,\n    \"exclusivity\": \"exclusive\",\n    \"tenancy\": \"tenanted\",\n    \"zoning\": \"Industrial Zone 1\",\n    \"authority\": null,\n    \"sale_tax\": \"unknown\",\n    \"tender_closing_date\": null,\n    \"outgoings\": null,\n    \"lease_price\": null,\n    \"lease_price_show\": 1,\n    \"lease_period\": \"anual\",\n    \"lease_tax\": null,\n    \"external_id\": \"b18c8652-72bf-47cb-ac06-d271ca0b0d8d\",\n    \"video_url\": null,\n    \"distance\": \"\",\n    \"propertyTypename\": \"Retail\",\n    \"auctionDetails\": null,\n    \"gallery\": [\n      {\n        \"id\": 144,\n        \"tbl_pk\": 161,\n        \"url\": \"http://localhost/AlphaWallet/html/frontend/web/images/property/Property_image935.jpg\",\n        \"type\": \"image/png\",\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"id\": 145,\n        \"tbl_pk\": 161,\n        \"url\": \"http://localhost/AlphaWallet/html/frontend/web/images/property/Property_image838.jpg\",\n        \"type\": \"image/png\",\n        \"tbl_name\": \"tbl_properties\"\n      }\n    ],\n    \"auction\": [],\n    \"saveauction\": null,\n    \"sold\": \"0\",\n    \"calendar\": 0\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/PropertyController.php",
    "groupTitle": "Property_Webhooks"
  },
  {
    "type": "post",
    "url": "/api/web/v1/auction/rating",
    "title": "Rating",
    "name": "Rating",
    "version": "0.1.1",
    "group": "Rating",
    "description": "<p>To Rating, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>User's Id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "auction_id",
            "description": "<p>Auction Id (Auction Id)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Used For rate to property(in between 1 to 5)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": {\n        \"auction_id\": \"1\",\n        \"is_favourite\": \"0\",\n        \"rating\": \"1\",\n        \"user_id\": \"351\",\n        \"id\": 16,\n        \"user_name\": \"gurpreet.gct\"\n    },\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to use submit correct data to save an rating</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"rating\",\n      \"message\": \"Must be one star\"\n    },\n    {\n      \"field\": \"user_id\",\n    \"message\": \"The user with user_id \\\"351\\\" does not exist\"\n    },\n    {\n       \"field\": \"auction_id\",\n    \"message\": \"The auction with auction \\\"1\\\" and user_id \\\"351\\\" already saved\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/AuctionController.php",
    "groupTitle": "Rating"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/facebooklogin",
    "title": "Facebook Login",
    "name": "Facebook_Login",
    "version": "0.1.1",
    "group": "Social_Login",
    "description": "<p>To login a user or bidder, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Facebook token access.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device",
            "description": "<p>'ios|android|web'.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "app_id",
            "description": "<p>(Optional) '2' to force user login as bidder, if not specified the login will be forced upon site users and bidders won't be able to login.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Lastname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Data set consisting of the loggedin User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n      \"id\": 313,\n      \"username\": \"john\",\n      \"email\": \"john_doe@mail.com\",\n      \"user_sub_type\": 10,\n      \"auth_key\": \"-3_xNTB7hyuR7UaJ0P1HHXgDhb8km-ao\",\n      \"firstName\": \"John\",\n      \"lastName\": \"Doe\",\n      \"fullName\": \"John Doe\",\n      \"fbidentifier\": null,\n      \"phone\": null,\n      \"image\": null,\n      \"sex\": null,\n      \"dob\": \"1990-01-01\",\n      \"ageGroup_id\": 1,\n      \"bdayOffer\": 0,\n      \"walletPinCode\": null,\n      \"loyaltyPin\": null,\n      \"advertister_id\": null,\n      \"device\": \"android\",\n      \"device_token\": \"\",\n      \"latitude\": null,\n      \"longitude\": null,\n      \"timezone\": null,\n      \"timezone_offset\": null,\n      \"storeid\": null,\n      \"promise_uid\": \"3085abb78c79ebb4\",\n      \"promise_acid\": null,\n      \"user_code\": \"AW-john_doe\",\n      \"referralCode\": null,\n      \"referral_percentage\": null\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n           {\n               \"field\": \"password\",\n               \"message\": \"Please enter a registered email address and  password.\"\n           }\n       ],\n       \"status\": 422\n   }\nOr\n   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n           {\n               \"notice\": \"Invalid login credentials\",\n               \"user_data\": {\n                   \"username\": \"gurpreet.gct\",\n                   \"password\": \"password123\",\n                   \"rememberMe\": true,\n                   \"isMobile\": true,\n                   \"device\": \"android\",\n                   \"resolution\": null\n               }\n          }\n      ],\n      \"status\": 422\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "Social_Login"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/googlelogin",
    "title": "Google Login",
    "name": "Google_Login",
    "version": "0.1.1",
    "group": "Social_Login",
    "description": "<p>To login a user or bidder using Google, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Google access token.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device",
            "description": "<p>'ios|android|web'.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "app_id",
            "description": "<p>(Optional) '2' to force user login as bidder, if not specified the login will be forced upon site users and bidders won't be able to login.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"message\": \"\",\n        \"data\": [\n            {\n                \"field\": \"apiData\",\n                \"message\": {\n                \"azp\": \"123432232233-asknjdaasdasd6aadlo37s.apps.googleusercontent.com\",\n                \"aud\": \"123432232233-asdnkja788l777mco9jkqjkat2mrbdsk.apps.googleusercontent.com\",\n                \"sub\": \"107474798829959527628\",\n                \"email\": \"testgtect@gmail.com\",\n                \"email_verified\": \"true\",\n                \"exp\": \"1523950468\",\n                \"iss\": \"https://accounts.google.com\",\n                \"iat\": \"1523946868\",\n                \"name\": \"gtect sharma\",\n                \"picture\": \"https://lh3.googleusercontent.com/-jGYhN2K3DaU/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWDYkBVrYGA7wPTLexos_AKAUUTdpw/s96-c/photo.jpg\",\n                \"given_name\": \"gtect\",\n                \"family_name\": \"sharma\",\n                \"locale\": \"en\",\n                \"alg\": \"RS256\",\n                \"kid\": \"5425bb84616ebf973ae80bc62ac688d2a72715ad\"\n            }\n        }\n    ],\n    \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n           {\n               \"field\": \"token\",\n               \"message\": \"Invalid token\"\n           }\n       ],\n       \"status\": 422\n   }\nOr\n   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n           {\n               \"field\": \"token\",\n               \"message\": \"Token cannot be blank.\",\n          }\n      ],\n      \"status\": 422\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "Social_Login"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/linkedinlogin",
    "title": "LinkedIn Login",
    "name": "LinkedIn_Login",
    "version": "0.1.1",
    "group": "Social_Login",
    "description": "<p>To login a user or bidder, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Linkedin unique user id, this can be obtained by accessing the linkedin user profile using access_token.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device",
            "description": "<p>'ios|android|web'.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "app_id",
            "description": "<p>(Optional) '2' to force user login as bidder, if not specified the login will be forced upon site users and bidders won't be able to login.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Lastname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Data set consisting of the Linkedin data</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n   {\n\t \t\"message\": \"\",\n\t \t\"data\": {\n\t\t\t\"id\": 351,\n\t\t\t\"username\": \"gurpreet.gct\",\n\t\t\t\"email\": \"gurpreet@graycelltech.com\",\n\t\t\t\"app_id\": \"2\",\n\t\t\t\"auth_key\": \"p9q3q5d4SJqBKpvrybILG7oJeePkylQW\",\n\t\t\t\"firstName\": \"GPreet\",\n\t\t\t\"lastName\": \"singh\",\n\t\t\t\"fullName\": \"GPreet singh\",\n\t\t\t\"fbidentifier\": null,\n\t\t\t\"linkedin_identifier\": \"123qweasdcxz\",\n\t\t\t\"google_identifier\": null,\n\t\t\t\"phone\": null,\n\t\t\t\"image\": null,\n\t\t\t\"sex\": null,\n\t\t\t\"dob\": \"1970-01-01\",\n\t\t\t\"ageGroup_id\": 1,\n\t\t\t\"bdayOffer\": 0,\n\t\t\t\"walletPinCode\": null,\n\t\t\t\"loyaltyPin\": null,\n\t\t\t\"advertister_id\": null,\n\t\t\t\"device\": \"android\",\n\t\t\t\"device_token\": \"\",\n\t\t\t\"latitude\": null,\n\t\t\t\"longitude\": null,\n\t\t\t\"timezone\": null,\n\t\t\t\"timezone_offset\": null,\n\t\t\t\"storeid\": null,\n\t\t\t\"promise_uid\": \"3085abb78c79ebb4\",\n\t\t\t\"promise_acid\": null,\n\t\t\t\"user_code\": \"AW-gurpreet.gct\",\n\t\t\t\"referralCode\": null,\n\t\t\t\"referral_percentage\": null,\n\t\t\t\"agency_id\": null\n\t\t},\n\t\t\"status\": 200\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "   HTTP/1.1 422 Unprocessable entity\n   {\n\t \t\"message\": \"\",\n\t\t\"data\": [\n\t\t\t{\n\t\t\t\t\"field\": \"token\",\n\t\t\t\t\"message\": \"Invalid token\"\n\t\t\t}\n\t\t],\n\t\t\"status\": 422\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "Social_Login"
  },
  {
    "type": "get",
    "url": "/api/web/v1/streaming/server-info",
    "title": "View Streaming server configurations",
    "name": "Streaming_Server_Configurations",
    "version": "0.1.1",
    "group": "Streaming",
    "description": "<p>To view streaming server configurations</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": {\n        \"host\": \"XXX.XXX.XXX.XXX\",\n        \"port\": \"1935\",\n        \"application\": \"my_app\",\n        \"source\": {\n            \"username\": \"john\",\n            \"password\": \"some-cool-password\"\n        }\n    },\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"name\": \"Unauthorized\",\n    \"message\": \"Your request was made with invalid credentials.\",\n    \"code\": 0,\n    \"status\": 401,\n    \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/StreamingController.php",
    "groupTitle": "Streaming"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/changepassword",
    "title": "Change password",
    "name": "Change_Password",
    "version": "0.1.1",
    "group": "User",
    "description": "<p>To change a user's or bidder's password, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Existing password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "newPassword",
            "description": "<p>new password.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   \n   HTTP/1.1 422 Unprocessable entity\n   {\n       \"message\": \"\",\n       \"data\": [\n           {\n               \"field\": \"newPassword\",\n               \"message\": \"New Password cannot be blank.\"\n           },\n           {\n               \"field\": \"password\",\n               \"message\": \"Password cannot be blank.\"\n           }\n       ],\n       \"status\": 422\n   }\nOr\n   HTTP/1.1 422 Unprocessable entity\n   {\n       \"message\": \"\",\n       \"data\": [\n           {\n               \"field\": \"password\",\n               \"message\": \"Current password is not matched.\"\n           }\n       ],\n       \"status\": 422\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n           {\n               \"field\": \"password\",\n               \"message\": \"Please enter a registered email address and  password.\"\n           }\n       ],\n       \"status\": 422\n   }\nOr\n   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n           {\n               \"notice\": \"Invalid login credentials\",\n               \"user_data\": {\n                   \"username\": \"gurpreet.gct\",\n                   \"password\": \"password123\",\n                   \"rememberMe\": true,\n                   \"isMobile\": true,\n                   \"device\": \"android\",\n                   \"resolution\": null\n               }\n          }\n      ],\n      \"status\": 422\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/checkemail",
    "title": "Check Email",
    "name": "Check_Email",
    "version": "0.1.1",
    "group": "User",
    "description": "<p>To check if an email is available, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email to valdiate.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Json obj consisting of key pairs field and message</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>422</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "If email is available\nHTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": [\n        {\n            \"field\": \"username\",\n            \"message\": \"Username in here!! cannot be blank.\"\n        }\n    ],\n    \"status\": 422\n}\n\nIf email not available\nHTTP/1.1 422 Unprocessable entity\n{\n    \"message\": \"\",\n    \"data\": [\n        {\n            \"field\": \"username\",\n            \"message\": \"Username in here!! cannot be blank.\"\n        },\n        {\n            \"field\": \"email\",\n            \"message\": \"That email address has already been registered. Please enter a valid email address.\"\n        }\n    ],\n    \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n    \"message\": \"\",\n    \"data\": [\n        {\n            \"field\": \"username\",\n           \"message\": \"Username in here!! cannot be blank.\"\n        },\n        {\n            \"field\": \"email\",\n            \"message\": \"Email cannot be blank.\"\n        }\n    ],\n    \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/checkusername",
    "title": "Check Username",
    "name": "Check_Username",
    "version": "0.1.1",
    "group": "User",
    "description": "<p>To check if a username and email is available, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's Username to validate.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email to valdiate.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Json obj consisting of key pairs field and message</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "If username is not available\nHTTP/1.1 422 Unprocessable entity\n{\n    \"message\": \"\",\n    \"data\": [\n         {\n             \"field\": \"username\",\n             \"message\": \"The username \\\"testuser1\\\" has already been taken. The following usernames are available:               \\\"testuser1_224, testuser1.162, testuser1_783, testuser1.testuser1, testuser1testuser1\\\".\"\n         },\n         {\n             \"field\": \"email\",\n             \"message\": \"That email address has already been registered. Please enter a valid email address.\"\n         }\n     ],\n     \"status\": 422\n}\n\nIf username is available\nHTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": [\n         {\n             \"field\": \"email\",\n             \"message\": \"That email address has already been registered. Please enter a valid email address.\"\n         }\n     ],\n     \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>422</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n    \"message\": \"\",\n    \"data\": [\n        {\n            \"field\": \"username\",\n           \"message\": \"Username in here!! cannot be blank.\"\n        },\n        {\n            \"field\": \"email\",\n            \"message\": \"Email cannot be blank.\"\n        }\n    ],\n    \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/web/v1/user/create",
    "title": "Create User",
    "name": "Create_User",
    "version": "0.1.1",
    "group": "User",
    "description": "<p>To create new users/bidders, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstName",
            "description": "<p>User's firstname.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastName",
            "description": "<p>User's lastname.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's username.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_hash",
            "description": "<p>User's password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dob",
            "description": "<p>Date of Birth (date must in YYYY-MM-DD format).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "app_id",
            "description": "<p>(Optional) '2' to force user registration as bidder</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fbidentifier",
            "description": "<p>(Optional) Access token received after FB login</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "linkedin_identifier",
            "description": "<p>(Optional) Access token received after Linkedin login</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "google_identifier",
            "description": "<p>(Optional) Access token received after Google Login</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Lastname of the User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\t\tHTTP/1.1 200 OK\n\t\t{\n\t\t\t\"message\": \"\",\n\t\t \t\"data\": {\n\t\t   \t\t\"app_id\": \"2\",\n         \t\"firstName\": \"test\",\n         \t\"lastName\": \"user\",\n         \t\"email\": \"testuser1@yopmail.com\",\n         \t\"username\": \"testuser1\",\n         \t\"dob\": 981331200,\n         \t\"auth_key\": \"H9jxjdaX6Q0q6N2X9TQnc5Z025U_KcWM\",\n         \t\"fullName\": \"test user\",\n         \t\"user_code\": \"AW-testuser1\",\n         \t\"id\": 367,\n         \t\"promise_uid\": \"3675af2ec527bf32\"\n\t\t\t},\n\t\t\t\"status\": 200\n\t\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n\t \"message\": \"\",\n\t \"data\": [\n\t   {\n\t\t \"field\": \"username\",\n\t\t \"message\": \"The username \\\"testuser1\\\" has already been taken. The following usernames are available: \\\"test_277, user.471, testuser1_446, test.user, testuser\\\". \"\n\t   }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/forgot",
    "title": "Forgot password",
    "name": "Forgot_Password",
    "version": "0.1.1",
    "group": "User",
    "description": "<p>To recover forgotten password of a user or bidder, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "app_id",
            "description": "<p>'2' for CVIEW</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message.",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n           {\n           }\n       ],\n       \"status\": 422\n   }\nOr\n   HTTP/1.1 422 Validation Error\n   {\n       \"message\": \"\",\n       \"data\": [\n      ],\n      \"status\": 422\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/login",
    "title": "Login",
    "name": "Login_User",
    "version": "0.1.1",
    "group": "User",
    "description": "<p>To login a user or bidder, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's username.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device",
            "description": "<p>'ios|android|web'.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_token",
            "description": "<p>Random String token 'assss2mlkamladsasd289'.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "app_id",
            "description": "<p>(Optional) '2' to force user login as bidder, if not specified the login will be forced upon site users and bidders won't be able to login.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Lastname of the User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\tHTTP/1.1 200 OK\n\t{\n\t\"message\": \"\",\n  \"data\": {\n\t\t\"id\": 367,\n   \t\"username\": \"testuser1\",\n   \t\"email\": \"testuser1@yopmail.com\",\n   \t\"app_id\": \"2\",\n   \t\"auth_key\": \"3_ggKqBVuyzFG-3U-0_9FRumPmhIFXka\",\n   \t\"firstName\": \"test\",\n   \t\"lastName\": \"user\",\n   \t\"fullName\": \"test user\",\n   \t\"fbidentifier\": null,\n   \t\"linkedin_identifier\": null,\n   \t\"google_identifier\": null,\n   \t\"phone\": null,\n   \t\"image\": null,\n   \t\"sex\": null,\n   \t\"dob\": \"2001-02-05\",\n   \t\"ageGroup_id\": 1,\n   \t\"bdayOffer\": 0,\n   \t\"walletPinCode\": null,\n   \t\"loyaltyPin\": null,\n   \t\"advertister_id\": null,\n   \t\"device\": \"android\",\n   \t\"device_token\": \"\",\n   \t\"latitude\": null,\n   \t\"longitude\": null,\n   \t\"timezone\": null,\n   \t\"timezone_offset\": null,\n   \t\"storeid\": null,\n   \t\"promise_uid\": \"3675af2ec527bf32\",\n   \t\"promise_acid\": null,\n   \t\"user_code\": \"AW-testuser1\",\n   \t\"referralCode\": null,\n   \t\"referral_percentage\": null,\n   \t\"agency_id\": null\n   },\n   \"status\": 200\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "Or\n\tHTTP/1.1 422 Unprocessable entity\n\t{\n\t\t\"message\": \"\",\n\t\t\"data\": [\n\t\t\t{\n\t\t\t\t\"field\": \"password\",\n\t\t\t\t\"message\": \"That  Pin does not match the registered email address. Please try again or click on Forgot  Pin? to reset it.\"\n\t\t\t}\n\t\t],\n\t\t\"status\": 422\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/logout",
    "title": "Logout",
    "name": "Logout",
    "version": "0.1.1",
    "group": "User",
    "description": "<p>To logout a user or bidder, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>(required) The access-token of the logged-in user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Text response</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "If invalid/expired access token\nHTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": [\n        \"Please provide an validate user token\"\n    ],\n    \"status\": 422\n}\n\nIf valid access-token\nHTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": \"Logout Successfully\",\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Validation Error\n{\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "put,patch",
    "url": "api/web/v1/users/update?id=264",
    "title": "Update profile",
    "name": "Update_User",
    "version": "0.1.1",
    "group": "User",
    "description": "<p>To update profile of a user or bidder. Requires login|access token to perform the update, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstName",
            "description": "<p>Firstname</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastName",
            "description": "<p>Lastname</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fullName",
            "description": "<p>Fullname</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>Base64 Image</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sex",
            "description": "<p>Male =&gt; 1| Female =&gt; 0</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dob",
            "description": "<p>Date of Birth (date must in YYYY-MM-DD format).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fbidentifier",
            "description": "<p>(Optional) Access token received after FB login</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "linkedin_identifier",
            "description": "<p>(Optional) Access token received after Linkedin login</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "google_identifier",
            "description": "<p>(Optional) Access token received after Google Login</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ageGroup_id",
            "description": "<p>1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "latitude",
            "description": "<p>1234.43</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "longitude",
            "description": "<p>3214.43</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "timezone",
            "description": "<p>Asia/Kolkata</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "timezone_offset",
            "description": "<p>+05:30</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Lastname of the User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n      \"id\": 313,\n      \"username\": \"john\",\n      \"email\": \"john_doe@mail.com\",\n      \"auth_key\": \"-3_xNTB7hyuR7UaJ0P1HHXgDhb8km-ao\",\n      \"firstName\": \"John\",\n      \"lastName\": \"Doe\",\n      \"fullName\": \"John Doe\",\n      \"fbidentifier\": null,\n      \"phone\": null,\n      \"image\": null,\n      \"sex\": null,\n      \"dob\": \"1990-01-01\",\n      \"ageGroup_id\": 1,\n      \"bdayOffer\": 0,\n      \"walletPinCode\": null,\n      \"loyaltyPin\": null,\n      \"advertister_id\": null,\n      \"device\": \"android\",\n      \"device_token\": \"\",\n      \"latitude\": null,\n      \"longitude\": null,\n      \"timezone\": null,\n      \"timezone_offset\": null,\n      \"storeid\": null,\n      \"promise_uid\": \"3085abb78c79ebb4\",\n      \"promise_acid\": null,\n      \"user_code\": \"AW-john_doe\",\n      \"referralCode\": null,\n      \"referral_percentage\": null\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "api/web/v1/users/validateuser",
    "title": "Validate User",
    "name": "Validate_User",
    "version": "0.1.1",
    "group": "User",
    "description": "<p>To check if a username and email is available, Form-Data must be x-www-form-urlencoded</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's Username to validate.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email to valdiate.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Json obj consisting of key pairs field and message</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>422</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "If user is not validated\nHTTP/1.1 422 Unprocessable entity\n{\n    \"message\": \"\",\n    \"data\": [\n        {\n            \"field\": \"username\",\n            \"message\": \"The username \\\"lorem.ipsum\\\" has already been taken. The following usernames are available: \\\"lorem.ipsum, lorem.ipsum.341, lorem.ipsum, lorem.ipsum.lorem.ipsum, lorem.ipsum.ipsum\\\". \"\n        },\n        {\n            \"field\": \"email\",\n            \"message\": \"That email address has already been registered. Please enter a valid email address.\"\n        }\n    ],\n    \"status\": 422\n}\n\nIf user email and password is validated\nHTTP/1.1 200 OK\n{\n    \"message\": \"\",\n    \"data\": {\n        \"username\": \"lorem.as\",\n        \"email\": \"lorem_ipsum@email.com\"\n    },\n    \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to fix the listed issues to create a successful user account.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n\t\"message\": \"\",\n\t\"data\": [\n\t\t{\n\t\t\t\"field\": \"username\",\n\t\t\t\"message\": \"Username in here!! cannot be blank.\"\n\t\t},\n\t\t{\n\t\t\t\"field\": \"email\",\n\t\t\t\"message\": \"Email cannot be blank.\"\n\t\t}\n\t],\n\t\"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n {\n     \"message\": \"\",\n      \"data\": {\n          \"id\": 35,\n            \"user_id\": \"351\",\n            \"agency_id_id\": \"2\",\n            \"is_favourite\": 0,\n            \"created_at\": 1525929504,\n            \"created_by\": 351,\n            \"updated_at\": 1525930676,\n            \"updated_by\": 351\n       },\n  \"status\": 200\n  }",
          "type": "json"
        }
      ]
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "api/modules/v1/controllers/AgencyController.php",
    "group": "_var_www_html_AlphaWallet_html_api_modules_v1_controllers_AgencyController_php",
    "groupTitle": "_var_www_html_AlphaWallet_html_api_modules_v1_controllers_AgencyController_php",
    "name": ""
  },
  {
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 \n {\n     \"message\": \"\",\n      \"data\": {\n            \"user_id\": \"351\",\n            \"listing_id\": \"2\",\n            \"is_favourite\": 0,\n            \"id\": 35,\n            \"user_name\": \"gurpreet.gct\"\n       },\n  \"status\": 200\n  }",
          "type": "json"
        }
      ]
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "api/modules/v1/controllers/AuctionController.php",
    "group": "_var_www_html_AlphaWallet_html_api_modules_v1_controllers_AuctionController_php",
    "groupTitle": "_var_www_html_AlphaWallet_html_api_modules_v1_controllers_AuctionController_php",
    "name": ""
  },
  {
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 \n   {\n       \"message\": \"\",\n       \"data\": [\n           {\n               \"id\": 21,\n               \"title\": \"PRIME DEVELOPMENT OPPORTUNITY (STCA)\",\n               \"description\": \"Rare opportunity to acquire this substantial development site, situated in the heart of one of Melbourne's most sought after inner city locations.\",\n  \t\t\t  \"status\": 1,\n               \"propertyType\": 2,\n  \t\t\t  \"price\": 0,\n  \t\t\t  \"landArea\": null,\n  \t\t\t  \"floorArea\": null,\n  \t\t\t  \"conditions\": 0,\n  \t\t\t  \"address\": \"516 Church Street, Richmond, VIC 3121\",\n  \t\t\t  \"latitude\": \"-37.828015\",\n  \t\t\t  \"longitude\": \"144.997316\",\n  \t\t\t  \"unitNumber\": 0,\n  \t\t\t  \"streetNumber\": 0,\n  \t\t\t  \"streetName\": null,\n  \t\t\t  \"suburb\": null,\n  \t\t\t  \"city\": null,\n  \t\t\t  \"state\": null,\n  \t\t\t  \"postcode\": null,\n  \t\t\t  \"country\": null,\n  \t\t\t  \"parking\": 0,\n  \t\t\t  \"parkingList\": null,\n  \t\t\t  \"parkingOther\": null,\n  \t\t\t  \"floorplan\": null,\n  \t\t\t  \"media\": null,\n  \t\t\t  \"video\": null,\n  \t\t\t  \"school\": 0,\n  \t\t\t  \"communityCentre\": 0,\n  \t\t\t  \"parkLand\": 0,\n  \t\t\t  \"publicTransport\": 0,\n  \t\t\t  \"shopping\": 0,\n  \t\t\t  \"bedrooms\": 0,\n  \t\t\t  \"bathrooms\": 0,\n  \t\t\t  \"features\": null,\n  \t\t\t  \"tags\": null,\n  \t\t\t  \"estate_agent_id\": 344,\n  \t\t\t  \"second_agent_id\": null,\n  \t\t\t  \"agency_id\": 10,\n  \t\t\t  \"created_by\": null,\n  \t\t\t  \"propertyStatus\": 0,\n  \t\t\t  \"updated_by\": null,\n  \t\t\t  \"created_at\": \"2018-04-19 08:39:00\",\n  \t\t\t  \"updated_at\": \"2018-04-19 08:39:00\",\n  \t\t\t  \"inspection1\": null,\n  \t\t\t  \"inspection2\": null,\n  \t\t\t  \"inspection3\": null,\n  \t\t\t  \"inspection4\": null,\n  \t\t\t  \"inspection5\": null,\n  \t\t\t  \"inspection6\": null,\n  \t\t\t  \"inspection7\": null,\n  \t\t\t  \"inspection8\": null,\n  \t\t\t  \"is_featured\": 0,\n  \t\t\t  \"distance\": \"\",\n  \t\t\t  \"propertyTypename\": \"Land/Development\",\n  \t\t\t  \"auctionDetails\": {\n    \t\t\t      \"live\": false,\n    \t\t\t\t  \"past\": false,\n    \t\t\t\t  \"upcoming\": true,\n    \t\t\t\t  \"seconds\": 0\n               },\n  \t\t\t  \"gallery\": [\n  \t              {\n\t\t                  \"id\": 20,\n\t\t                  \"tbl_pk\": 29,\n\t\t                  \"url\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image585.jpg\",\n\t\t                  \"type\": \"images/jpg\",\n\t\t                  \"tbl_name\": \"tbl_properties\"\n\t                  },\n\t                  {\n\t\t                  \"id\": 21,\n\t\t                  \"tbl_pk\": 29,\n\t\t                  \"url\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n\t\t                  \"type\": \"images/jpg\",\n\t\t                  \"tbl_name\": \"tbl_properties\"\n\t   \t\t\t\t  }\n  \t\t\t  ],\n  \t\t\t  \"auction\": {\n   \t      \t      \"id\": 41,\n    \t\t\t      \"listing_id\": 21,\n    \t\t\t\t  \"auction_type\": null,\n    \t\t\t\t  \"date\": \"2018-05-23\",\n    \t\t\t\t  \"time\": \"13:00:00\",\n    \t\t\t\t  \"datetime_start\": \"2018-05-23 13:00:00\",\n    \t\t\t\t  \"datetime_expire\": null,\n    \t\t\t\t  \"starting_price\": null,\n    \t\t\t\t  \"home_phone_number\": null,\n    \t\t\t\t  \"estate_agent\": null,\n    \t\t\t\t  \"picture_of_listing\": null,\n    \t\t\t\t  \"duration\": null,\n    \t\t\t\t  \"real_estate_active_himself\": null,\n    \t\t\t\t  \"real_estate_active_bidders\": null,\n    \t\t\t\t  \"reference_relevant_electronic\": null,\n    \t\t\t\t  \"reference_relevant_bidder\": null,\n    \t\t\t\t  \"auction_status\": 1,\n    \t\t\t\t  \"streaming_url\": null,\n    \t\t\t\t  \"streaming_status\": null,\n    \t\t\t\t  \"templateId\": \"\",\n    \t\t\t\t  \"envelopeId\": \"\",\n    \t\t\t\t  \"documentId\": \"\",\n    \t\t\t\t  \"document\": \"\",\n    \t\t\t\t  \"recipientId\": \"\",\n    \t\t\t\t  \"bidding_status\": 2\n               },\n  \t\t\t  \"agent\": {\n                   \"status\": 10,\n                   \"user_type\": 10,\n                   \"app_id\": \"2\",\n                   \"id\": 344,\n                   \"username\": \"aastha1\",\n                   \"email\": \"aastha1@graycelltech.com\",\n                   \"auth_key\": \"41oBK-OHkauNIq5b8jyQlF4jcZPTsXT_\",\n                   \"firstName\": \"aastha\",\n                   \"lastName\": \"chawla\",\n                   \"fullName\": \"aastha chawla\",\n                   \"orgName\": null,\n                   \"fbidentifier\": null,\n                   \"linkedin_identifier\": null,\n                   \"google_identifier\": null,\n                   \"role_id\": null,\n                   \"password_hash\": \"\",\n                   \"password_reset_token\": null,\n                   \"phone\": null,\n                   \"image\": null,\n                   \"sex\": null,\n                   \"dob\": \"1970-01-01\",\n                   \"ageGroup_id\": 1,\n                   \"bdayOffer\": 0,\n                   \"walletPinCode\": null,\n                   \"loyaltyPin\": null,\n                   \"advertister_id\": null,\n                   \"activationKey\": null,\n                   \"confirmationKey\": null,\n                   \"access_token\": null,\n                   \"hashKey\": null,\n                   \"device\": \"web\",\n                   \"device_token\": \"\",\n                   \"lastLogin\": null,\n                   \"latitude\": null,\n                   \"longitude\": null,\n                   \"timezone\": null,\n                   \"timezone_offset\": null,\n                   \"created_at\": 1523881212,\n                   \"updated_at\": 1523881212,\n                   \"storeid\": null,\n                   \"promise_uid\": null,\n                   \"promise_acid\": null,\n                   \"user_code\": \"AW-aastha1\",\n                   \"referralCode\": null,\n                   \"referral_percentage\": null,\n                   \"agency_id\": 10,\n                   \"agency\": \"http://localhost/AlphaWallet/html/frontend/web/agencylogo/Abercromby’s819.jpg\"\n               },\n  \t\t\t  \"agency\": {\n    \t\t          \"status\": 10,\n    \t\t\t\t  \"id\": 10,\n    \t\t\t\t  \"name\": \"Abercromby’s\",\n    \t\t\t\t  \"email\": \"aastha@gmail.com\",\n    \t\t\t\t  \"mobile\": 1234569874,\n    \t\t\t\t  \"logo\": \"http://localhost/AlphaWallet/html/frontend/web/agencylogo/Abercromby’s819.jpg\",\n    \t\t\t\t  \"address\": \"[\\\" sco 206 - 207, sector 34 a, chandigarh - 160022\\\",\\\"sco 72 a & 73 a sector 26 grain market chandigarh pin-160019\\\"]\",\n    \t\t\t\t  \"latitude\": \"[\\\"30.72589\\\",\\\"30.72267\\\"]\",\n    \t\t\t\t  \"longitude\": \"[\\\"76.75787\\\",\\\"76.79825\\\"]\",\n    \t\t\t\t  \"created_at\": 1524034419,\n    \t\t\t\t  \"updated_at\": 1524034419\n  \t\t\t  },\n  \t\t\t  \"saveauction\": \"\",\n\t\t\t\t  \"sold\": \"0\"\n\t\t\t\t  or\n\t\t\t\t  \"sold\": \"1\"\n           },\n       ],\n       \"status\": 200,\n   }",
          "type": "json"
        }
      ]
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "api/modules/v1/controllers/PropertyController.php",
    "group": "_var_www_html_AlphaWallet_html_api_modules_v1_controllers_PropertyController_php",
    "groupTitle": "_var_www_html_AlphaWallet_html_api_modules_v1_controllers_PropertyController_php",
    "name": ""
  },
  {
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "app_id",
            "description": "<p>(Optional) <em>2=For CVIEW app</em></p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "mobile",
            "description": "<p>User's Mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "otp",
            "description": "<p>OTP sent on mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address_1",
            "description": "<p>User's Address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address_2",
            "description": "<p>User's Address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "state_id",
            "description": "<p>The state/county where the property is located</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "postcode",
            "description": "<p>User's Postcode</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country_id",
            "description": "<p>User's Country</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>Gender of user 1=Male /2=Female</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "passport_number",
            "description": "<p>User's Passport</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "driving_licence_number",
            "description": "<p>User's Licence</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mediacare_number",
            "description": "<p>Image in base64 encoded Format</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "recent_utility_bill",
            "description": "<p>Image in base64 encoded Format</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "solicitor_firstname",
            "description": "<p>Solicitor's First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "solicitor_lastname",
            "description": "<p>Solicitor's Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "solicitor_email",
            "description": "<p>Solicitor's Email</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "solicitor_mobile",
            "description": "<p>Solicitor's Mobile</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "property_id",
            "description": "<p>Bidding Property</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bidding_panel_number",
            "description": "<p>Bidding Panel Number</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "bidding_colour",
            "description": "<p>Represents the bidder at auction</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guarantor_first_name",
            "description": "<p>Guarantor First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guarantor_lastname",
            "description": "<p>Guarantor Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "guarantor_email",
            "description": "<p>Guarantor Email</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n        \"message\": \"\",\n\t       \"data\": {\n            \"id\": 15,\n            \"user_id\": 362,\n            \"mobile\": \"324536546\",\n            \"otp\": null,\n            \"address_1\": \"address 1\",\n            \"address_2\": \"address 2\",\n            \"state_id\": 0,\n            \"postcode\": null,\n            \"country_id\": 0,\n            \"gender\": \"3\",\n            \"passport_number\": \"2312432dfds\",\n            \"driving_licence_number\": \"cdsf34324\",\n            \"medicare_file\": \"mediacare_number910.jpg\",\n            \"utilitybill_file\": \"UtilityBill_Number_362_371.jpg\",\n            \"solicitor_firstname\": \"test\",\n            \"solicitor_lastname\": \"test2\",\n            \"solicitor_email\": \"test@solicitor.com\",\n            \"solicitor_mobile\": \"5885155842\",\n            \"property_id\": \"1\",\n            \"bidding_panel_number\": \"243265475673\",\n            \"bidding_colour\": \"10\",\n            \"guarantor_first_name\": \"gurentor1\",\n            \"guarantor_lastname\": \"gurentor last\",\n            \"guarantor_email\": \"gui@gui.com\",\n        },\n        \"status\": 200\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Validation",
            "description": "<p>Error Make sure to use submit correct data to save an bid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"user_id\",\n      \"message\": \"User Id must be an integer.\"\n    },\n    {\n      \"field\": \"property_id\",\n      \"message\": \"Property Id must be an integer.\"\n    },\n    {\n      \"field\": \"is_favourite\",\n      \"message\": \"Is Favourite must be an integer.\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "api/modules/v1/controllers/RegisterBidController.php",
    "group": "_var_www_html_AlphaWallet_html_api_modules_v1_controllers_RegisterBidController_php",
    "groupTitle": "_var_www_html_AlphaWallet_html_api_modules_v1_controllers_RegisterBidController_php",
    "name": ""
  }
] });
