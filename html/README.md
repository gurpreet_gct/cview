Yii 2 Advanced Project Template
===============================


INTRODUCTION
------------

Yii 2 Advanced Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
developing complex Web applications with multiple tiers.

The template includes three tiers: front end, back end, and console, each of which
is a separate Yii application.

The template is designed to work in a team development environment. It supports
deploying the application in different environments.

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-advanced/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-advanced/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-advanced.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-advanced)


DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
tests                    contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
```

CVIEW  API'S
============


Documentation generation
------------------------

- Major site APIs are under [![](api/modules/v1/)]

- Guide to support elegant documentation generation
[![Official link](http://apidocjs.com/#params)]

- API-Documentation
[![Plugin](apidocjs.com/)]

- Quick update for documentation
[![Cli command, run from project root, i.e. /AlphaWallet/html](apidoc -i api/ -o apidoc/)]

- Url to access API-Documentation
[![local](http://localhost/AlphaWallet/html/apidoc/)]
[![prod](http:://siteroot/html/apidoc/)]
http://192.168.100.15/AlphaWallet/html/apidoc

Internalization i18n
--------------------

- To generate, refresh the languages use the below command.
- For API specific: `./yii message/extract ./api/config/i18n.php`
- For Global: `./yii message/extract ./common/config/i18n.php`

USER TYPES
----------

1. Bidders:
  a) Bidders are stored originally as customers, i.e. with user_type as 3 to allow login to the system.
  b) To distinguish a bidder from a normal user/customer, column user_sub_type has been added to the tbl_users. The values corresponds to the id of tbl_usertype, defaults to null to make sure the existing flow of the app doesn't effect.

TEST CREDS
----------

1. Customer
 - *Username*: `johndoe`
 - *Email*: `johndoe@email.com`
 - *Password*: `password123`

2. Bidder
 - *Username*: `johndoe_bidder`
 - *Email*: `johndoe_bidder@email.com`
 - *Password*: `password123`

 - *Username*: `gurpreet.gct`
 - *Email*: `gurpreet@graycelltech.com` 
 - *Password*: `password123`


WOWZA
-----

1. Php API-Doc wrapper examples: `https://www.wowza.com/docs/application-management-query-examples`

##LOCAL API URL
-------------

`http://192.168.0.117/AlphaWallet/html/apidoc/`


---------------------------------------------------------------------------------
# MODULES

1. Property
 1.1
 
ALTER TABLE `tbl_properties`
  DROP `estate_agent_id`,
  DROP `second_agent_id`,
  DROP `agency_id`,
  DROP `inspection1`,
  DROP `inspection2`,
  DROP `inspection3`,
  DROP `inspection4`,
  DROP `inspection5`,
  DROP `inspection6`,
  DROP `inspection7`,
  DROP `inspection8`; 



2. Auction

ALTER TABLE `tbl_auction_event` ADD `estate_agent_id` INT NOT NULL COMMENT 'Agent ID' AFTER `auction_type`, ADD `agency_id` INT NOT NULL COMMENT 'Under Which Agency Agent Adding Auction' AFTER `estate_agent_id`;

3. Agent

4. Agency

ALTER TABLE `tbl_agencies` ADD `cv_agency_id` VARCHAR(15) NULL DEFAULT NULL COMMENT 'Provided by CView Team' AFTER `id`;
ALTER TABLE `tbl_agencies` ADD `slug` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;
ALTER TABLE `tbl_agencies` ADD `abn` VARCHAR(255) NULL AFTER `slug`;
ALTER TABLE `tbl_agencies` ADD `street_address` MEDIUMTEXT NULL DEFAULT NULL AFTER `address`, ADD `full_address` MEDIUMTEXT NULL DEFAULT NULL AFTER `street_address`;
ALTER TABLE `tbl_agencies` ADD `suburb_id` INT NULL DEFAULT NULL AFTER `full_address`;
ALTER TABLE `tbl_agencies` ADD `web` MEDIUMTEXT NULL AFTER `updated_at`, ADD `twitter` MEDIUMTEXT NULL AFTER `web`, ADD `facebook` MEDIUMTEXT NULL DEFAULT NULL AFTER `twitter`;
ALTER TABLE `tbl_agencies` ADD `fax` VARCHAR(15) NULL DEFAULT NULL AFTER `mobile`;

ALTER TABLE `tbl_agencies_offices` ADD `street_address` MEDIUMTEXT NULL DEFAULT NULL AFTER `address`, ADD `full_address` MEDIUMTEXT NULL DEFAULT NULL AFTER `street_address`, ADD `suburb_id` INT NULL DEFAULT NULL AFTER `full_address`;

5. Bidding

# DB UPDATES to be added to live server
22 May 2018
07:28 pm

ALTER TABLE `tbl_register_bid`
  DROP `address_1`,
  DROP `address_2`,
  DROP `state_id`,
  DROP `postcode`,
  DROP `country_id`,
  DROP `gender`,
  DROP `passport_number`,
  DROP `driving_licence_number`,
  DROP `medicare_file`,
  DROP `utilitybill_file`;

  property_id => auction_id

ALTER TABLE `tbl_register_bid`
  DROP `approved_status`;


ALTER TABLE `tbl_apply_bid`
  DROP `bid_approved`;

ALTER TABLE 'tbl_auction_event'
  SET auction_id => id 

ALTER TABLE `tbl_account_verification`
  DROP `user_id`,
  DROP `listing_id`;

# 23 May 2018

CREATE TABLE `tbl_currencies` (
  `id` int(11) NOT NULL,
  `country` varchar(200) NOT NULL,
  `currency` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `symbol` varchar(100) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# As on 23 May 2018

ALTER TABLE `tbl_properties` ADD `cview_listing_id` VARCHAR(55) NOT NULL COMMENT 'Provided by CView Team' AFTER `is_featured`;
ALTER TABLE `tbl_properties` ADD `is_deleted` TINYINT(2) NOT NULL DEFAULT '0' COMMENT '1 = "Yes", 0 = "No"' AFTER `cview_listing_id`;


ALTER TABLE `tbl_bidding` CHANGE `listing_id` `auction_id` INT(11) NOT NULL COMMENT 'auction id';
ALTER TABLE `tbl_bidding` CHANGE `status` `status` INT(11) NULL DEFAULT NULL COMMENT '0=pending,1=accepted,2=rejected';

ALTER TABLE `tbl_properties` ADD `land_area_unit` VARCHAR(255) NULL DEFAULT NULL AFTER `landArea`;
ALTER TABLE `tbl_properties` ADD `floor_area_unit` VARCHAR(255) NULL DEFAULT NULL AFTER `floorArea`;
ALTER TABLE `tbl_properties`  ADD `business_email` VARCHAR(255) NULL DEFAULT NULL  AFTER `is_deleted`,  ADD `website` VARCHAR(255) NULL DEFAULT NULL  AFTER `business_email`,  ADD `valuation` VARCHAR(255) NULL DEFAULT NULL  AFTER `website`,  ADD `exclusivity` VARCHAR(255) NULL DEFAULT NULL  AFTER `valuation`,  ADD `tenancy` VARCHAR(255) NULL DEFAULT NULL  AFTER `exclusivity`,  ADD `zoning` VARCHAR(255) NULL DEFAULT NULL  AFTER `tenancy`,  ADD `authority` VARCHAR(255) NULL DEFAULT NULL  AFTER `zoning`,  ADD `sale_tax` VARCHAR(55) NULL DEFAULT NULL  AFTER `authority`,  ADD `tender_closing_date` VARCHAR(25) NULL DEFAULT NULL  AFTER `sale_tax`,  ADD `outgoings` VARCHAR(55) NULL DEFAULT NULL  AFTER `tender_closing_date`,  ADD `lease_price` VARCHAR(15) NULL DEFAULT NULL  AFTER `outgoings`,  ADD `lease_price_show` TINYINT(3) NOT NULL DEFAULT '0' COMMENT '0 : False, 1 : True '  AFTER `lease_price`,  ADD `lease_period` VARCHAR(25) NULL DEFAULT NULL  AFTER `lease_price_show`,  ADD `lease_tax` VARCHAR(15) NULL DEFAULT NULL  AFTER `lease_period`,  ADD `external_id` VARCHAR(100) NULL DEFAULT NULL  AFTER `lease_tax`,  ADD `video_url` VARCHAR(255) NULL DEFAULT NULL  AFTER `external_id`;
ALTER TABLE `tbl_properties` ADD `current_lease_expiry` VARCHAR(55) NULL DEFAULT NULL AFTER `tender_closing_date`;
ALTER TABLE `tbl_properties` ADD `slug` VARCHAR(255) NULL DEFAULT NULL AFTER `video_url`;

// User Profile Regarding Agents

ALTER TABLE `tbl_profile` ADD `preferred_contact_number` VARCHAR(15) NOT NULL AFTER `subscriptionPay`, ADD `about` TEXT NULL DEFAULT NULL COMMENT 'Personal Info' AFTER `preferred_contact_number`, ADD `licence_number` VARCHAR(15) NULL DEFAULT NULL COMMENT 'Agent License Number' AFTER `about`, ADD `awards` TINYTEXT NOT NULL AFTER `licence_number`, ADD `specialties` TINYTEXT NOT NULL AFTER `awards`;

ALTER TABLE `tbl_profile` ADD `members` MEDIUMTEXT NULL DEFAULT NULL AFTER `specialties`, ADD `accreditation` VARCHAR(255) NULL DEFAULT NULL AFTER `members`, ADD `languages` VARCHAR(255) NULL DEFAULT NULL AFTER `accreditation`;

ALTER TABLE `tbl_profile` ADD `enquiry_email` VARCHAR(255) NULL DEFAULT NULL AFTER `preferred_contact_number`;
ALTER TABLE `tbl_profile` ADD `socialmediaLinkedin` VARCHAR(255) NULL DEFAULT NULL AFTER `socialmediaTwitter`;

// Gallery

ALTER TABLE `tbl_gallery` ADD `file_name` VARCHAR(55) NULL DEFAULT NULL AFTER `type`;

Now we are saving property documents and video_url in gallery table not in property table.
=======
#28-05-2018

ALTER TABLE `tbl_auction_event` ADD `completed_streaming_url` VARCHAR(255) NOT NULL AFTER `streaming_url`; 

#29-05-2018
DB synced
=======================================================================================
ALTER TABLE `tbl_auction_event` ADD `completed_streaming_url` VARCHAR(255) NOT NULL AFTER `streaming_url`;


#30-05-2018

ALTER TABLE `tbl_apply_bid` CHANGE `listing_id` `auction_id` INT(11) NOT NULL;

Data format as per the doc named "Commercial View APIs 25May2018.docx" as on 29-May2018
=======

In Property Listing Data 

    - property_type is missing // I added id 1 for now

In Agency Listing 

    - backgroungImage   is missing

In Agent Listing 

    - password_hash   is missing, but in case of bidders listing password is coming, we are saving it as a string.


#PasswordHash for webhooks
Regarding password_hash we need it to confirm from the cview Team about the password encryption techniques 

#CViewConfig Module
1. AuctionStatus-Pending Data-Structure
[
  'pause' => [
    [
     1 => ['name' => 'Seeking vendor advice', 'status' => 1],
     2 => ['name' => 'Weather conditions', 'status' => 1],
     3 => ['name' => 'Other', 'status' => 1],
    ]
  ]
]


#Auction Module
1. Auction status
    0 => applied, 
    1 => Started, 
    2 => Paused, 
    3 => Finished

#AuctionEvent Module
1. Auction_bidding_status
    1 => No Online Bidding
    2 => Online Bidding
    3 => Offsite bidding

31-05-2018
ALTER TABLE `tbl_bidding` CHANGE `status` `status` TINYINT(3) NULL DEFAULT '0' COMMENT '0 = Pending, 1 = Accepted, 2 = Rejected, 3 = Awarded';

#01-06-2018
ALTER TABLE `tbl_bidding` ADD `bidder_amount` DECIMAL(16,2) NOT NULL AFTER `amount`, ADD `bidder_currency_id` INT NOT NULL AFTER `bidder_amount`;
ALTER TABLE `tbl_auction_event` ADD `auction_currency_id` INT NOT NULL AFTER `starting_price`;

#04-06-2018
ALTER TABLE `tbl_auction_event` ADD `auction_bidding_status` TINYINT(3) NOT NULL COMMENT '1 = No Online Bidding, 2 = Online Bidding, 3 = Offsite bidding.' AFTER `bidding_pause_status`;

ALTER TABLE `tbl_bidding` CHANGE `status` `status` TINYINT(3) NULL DEFAULT '0' COMMENT '0 = Pending, 1 = Accepted, 2 = Rejected, 3 = Current Bid, 4 = Awarded';

AS on 04-06-2018


CREATE TABLE `tbl_auction_address` (
  `id` int(11) NOT NULL,
  `auction_event_id` int(11) NOT NULL,
  `address_line_1` mediumtext,
  `address_line_2` mediumtext,
  `city` varchar(55) DEFAULT NULL,
  `state` varchar(55) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `auction_start_time` int(11) NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `tbl_auction_address`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_auction_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
  
  06-06-2018
  
  ALTER TABLE `tbl_agencies` ADD `primary_color` VARCHAR(255) NULL DEFAULT NULL AFTER `abn`, ADD `secondary_color` VARCHAR(255) NULL DEFAULT NULL AFTER `primary_color`, ADD `text_color` VARCHAR(255) NULL DEFAULT NULL AFTER `secondary_color`;
  
  ALTER TABLE `tbl_auction_event` ADD `auction_type` TINYINT(2) NOT NULL DEFAULT '1' COMMENT '\'1 = For Sale, 2 = For Rent, 3 = For Lease\'' AFTER `listing_id`;
