#### Instructions:
	1. Base Url is `http://cview.civitastech.com/`
    2. Authentication: For requests that require authorization,  
    	2.1 "auth_key" Bearer Token Authorization
    	2.2 To get a valid "auth_key", Search for "Get An Auth Key" in search bar (Left top)
    3. Authorization headers: `Authorization: Bearer XXXXXXXXXXXXXXXXXXXXXX` 