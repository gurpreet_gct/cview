#26 July, 2016
ALTER TABLE `tbl_product_option`  ADD `is_included` TINYINT(2) NULL DEFAULT 0  AFTER `price`;


drop table if exists tbl_paymentMethods;
CREATE TABLE `tbl_paymentMethods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,  
  `user_id` int(11) DEFAULT NULL,  
  `bin` int(6) DEFAULT NULL COMMENT 'card first 6 digit',
  `last4` int(4) DEFAULT NULL COMMENT 'cart last 4 digit',
  `cardType` varchar(70) DEFAULT NULL,
  `expirationMonth` int(2) DEFAULT NULL COMMENT 'expiry month',
  `expirationYear` int(4) DEFAULT NULL COMMENT 'expiry year',
  `customerLocation` varchar(10) DEFAULT NULL,
  `cardholderName` varchar(100) DEFAULT NULL,
  `imageUrl` varchar(500) DEFAULT NULL,
  `prepaid` varchar(100) DEFAULT NULL,
  `healthcare` varchar(100) DEFAULT NULL,
  `debit` varchar(100) DEFAULT NULL,
  `durbinRegulated` varchar(100) DEFAULT NULL,
  `commercial` varchar(100) DEFAULT NULL,
  `payroll` varchar(100) DEFAULT NULL,
  `issuingBank` varchar(100) DEFAULT NULL,
  `countryOfIssuance` varchar(100) DEFAULT NULL,
  `productId` varchar(100) DEFAULT NULL,
  `uniqueNumberIdentifier` varchar(80) DEFAULT NULL,
  `venmoSdk` varchar(100) DEFAULT NULL,
  `expirationDate` varchar(8) DEFAULT NULL,
  `maskedNumber` varchar(20) DEFAULT NULL,
  token varchar(40) default null,
  is_delete tinyint(2) default 0,
  `systemToken` varchar(40) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
