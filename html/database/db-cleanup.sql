truncate tbl_account;
truncate tbl_address;
truncate tbl_bank_accounts;
truncate tbl_beacon_detect_logs;
truncate tbl_beacon_manager;
truncate tbl_cart;
truncate tbl_deal;
truncate tbl_deal_bookmark;
truncate tbl_deal_store;
truncate tbl_loginLogs;
truncate tbl_loyalty;
truncate tbl_loyaltyCards;
truncate tbl_loyaltypoints;
truncate tbl_offerTrack;
truncate tbl_ordercomment;
truncate tbl_order_item;
truncate tbl_order_loyalty;
truncate tbl_order_tax;
truncate tbl_order_sub_item;
truncate tbl_passes;
truncate tbl_paymentMethod;
truncate tbl_product;
truncate tbl_qrkit;
truncate tbl_shipper;
truncate tbl_stampcardqr;
truncate tbl_store;
truncate tbl_storeCategory;
truncate tbl_store_beacons;
truncate tbl_user_activity_logs;
truncate tbl_userCategory;
truncate tbl_user_loyalty;
truncate tbl_userPsa;
truncate tbl_userstore;
truncate tbl_walletids;
truncate tbl_walletpcards;
truncate tbl_workorderCategory;
drop table if exists tbl_workordercategory_old;
truncate tbl_workorder_popup;
truncate tbl_workorders;
drop table if exists tbl_categoriesold;
truncate tbl_users;

insert into tbl_users  (firstName, lastName, fullName, orgName, email, fbidentifier, role_id, user_type, username, password_hash, password_reset_token, phone, image, sex, dob, walletPinCode, auth_key, activationKey, confirmationKey, access_token, hashKey, status, device, device_token, lastLogin, latitude, longitude, created_at, updated_at)
values ('Paras', 'Kumar', 'Paras Kumar','' , 'paras@graycelltech.com','' , 0, 1, 'paras',
 '$2y$13$pde4j1qStBV7yWJCvO50ZOR51huB3WSDdhcJ0WgiBCDndRmq0Kvka', 'OHZH9EmxTh6FJUq3QhlfEwcZWLqTtWQM_1441712929', '1234567896', 'parasbd2c6f7c2fc41fb.png',
 1, 0, 1234, 'HOjikty37TXX0cQ2zpvqxeMXz--UiSTD','' ,'' , 'test-100', '', 10, 'web','' , 1460467860,'' ,'' ,
 1439191897, 1460467860);
