/* 08 MAY 2016 */
drop table if exists tbl_zone_notification_stats;
CREATE TABLE IF NOT EXISTS `tbl_zone_notification_stats` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `deal_id` int(11) DEFAULT NULL,
  `notification_limit` int(11) DEFAULT NULL,
  `used_limit` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
);

/* 08 MAY 2016 */
drop table if exists tbl_polygons_region;
CREATE TABLE IF NOT EXISTS `tbl_polygons_region` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `deal_id` int(11) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `polygon_shape` polygon NOT NULL,
  `message_limit` int(11) NOT NULL DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `tbl_polygons_region`
  ADD SPATIAL KEY `polygon_shape` (`polygon_shape`);
  
/* 08 MAY 2016 */
drop table if exists tbl_geo_zone;
CREATE TABLE IF NOT EXISTS `tbl_geo_zone` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `area_name` varchar(150) DEFAULT NULL,
  `latitude` decimal(12,6) DEFAULT NULL,
  `longitude` decimal(12,6) DEFAULT NULL,
  `deal_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
);


/* 
	24-06-2016
*/

ALTER TABLE `tbl_zone_notification_stats` ADD `detect_type` INT NOT NULL AFTER `deal_id`;


/* 
	04-07-2016
*/

ALTER TABLE `tbl_geo_zone` ADD `region_id` INT NOT NULL AFTER `id`;
ALTER TABLE tbl_polygons_region DROP zone_id;