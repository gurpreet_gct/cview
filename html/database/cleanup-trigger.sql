delimiter $$
drop trigger if exists tri_useloyalty$$

create trigger tri_useloyalty  after insert on tbl_order_loyalty
FOR EACH ROW 
BEGIN
update tbl_user_loyalty set  
points=points-((points/value)*NEW.value),
value=value - (NEW.value)
 where user_id=NEW.user_id
and store_id=NEW.store_id;
END$$


delimiter $$
drop trigger if exists tri_workorder_unid$$
create trigger tri_workorder_unid before insert on tbl_workorders
for each row
begin
  declare aid int default 0;
  if (new.uniqueID is null or new.uniqueID="") then
  select auto_increment into aid from information_schema.tables  where table_name ='tbl_workorders' and table_schema = database();
  set new.uniqueID=concat(date_format(now(),"%Y"),"-",LPAD(new.advertiserID,4,0),"-",LPAD(aid,6,0));
  end if;
end$$



delimiter $$
drop trigger if exists tri_addloyalty$$
create trigger tri_addloyalty  after insert on tbl_loyaltypoints 
FOR EACH ROW 
BEGIN
insert into tbl_user_loyalty (user_id,store_id,value,created_at,updated_at,loyaltyID,points) values
(new.user_id,new.store_id,ifnull((NEW.rValueOnDay * NEW.loyaltypoints),0),new.updated_at,new.updated_at,
concat(new.user_id,"-",new.store_id),New.loyaltypoints)
on duplicate key 
update  
value=value + (NEW.rValueOnDay * NEW.loyaltypoints), 
points=points + NEW.loyaltypoints, 
updated_at=NEW.updated_at;
#where user_id=NEW.user_id
#and store_id=NEW.store_id;

END$$
