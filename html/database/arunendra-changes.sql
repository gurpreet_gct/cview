5/07/2017
alter table tbl_setting add column minimum_cashout decimal(10,2); 
DELIMITER $$
CREATE TRIGGER add_referral_amount AFTER INSERT ON tbl_referral_earnings
FOR EACH ROW 
BEGIN
	if new.status = 0 then
		insert into tbl_referral_cashamount(user_id,lifetime_commission,unused_commission,created_at)
		values(new.refer_by,new.commission_amount,new.commission_amount,new.created_at)
		on duplicate key
		update
		lifetime_commission = lifetime_commission+new.commission_amount,
		unused_commission = unused_commission+new.commission_amount,
		updated_at=UNIX_TIMESTAMP();
	END IF;
END;


create table tbl_referral_cashamount(
user_id int(11) auto_increment,
lifetime_commission decimal(10,2) default 0.00,
unused_commission decimal(10,2) default 0.00,
used_commission decimal(10,2) default 0.00,
created_at int(11),
updated_at int(11),
updated_by int(11),
primary key (user_id)
);


4/07/2017
alter table tbl_users add column referral_percentage decimal(2,2);

create table tbl_referral_earnings(
id int(11) auto_increment primary key,
user_id int(11),
refer_by int(11),
commission_amount decimal(10,2),
commission_percentage tinyint(2),
order_id int(11),
order_total decimal(10,2),
status tinyint(1) comment "0 = pending,1=paid",
created_at int(11),
created_by int(11)
);

3/07/2017
create table if not exists tbl_setting(
id int(11) auto_increment primary key,
partner_referral decimal(10,2),
customer_referral decimal(10,2),
subscription_fees decimal(10,2),
subscription_day tinyint(2),
created_at int(11),
created_by int(11),
updated_at int(11),
updated_by int(11)
);

30-06-2017
alter table tbl_profile add column subscriptionFees decimal(10,2),
add column subscriptionDueDate tinyint(2);

alter table tbl_users add column referralCode varchar(50);
alter table tbl_users add column user_code varchar(50);

24/06/2017
alter table tbl_bank_accounts add column disbursement tinyint(1);

DROP TABLE IF EXISTS `tbl_subscription_charges`;
CREATE TABLE `tbl_subscription_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) NOT NULL,
  `amount` decimal(10,2) DEFAULT '0.00',
  `payment_status` tinyint(1) DEFAULT NULL COMMENT '1=paid,0=pending',
  `transaction_id` varchar(50) DEFAULT NULL,
  `account_id` varchar(50) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


Remove this module form the admin

DROP TABLE IF EXISTS `tbl_subscription_fees`;
CREATE TABLE `tbl_subscription_fees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(10,2) DEFAULT NULL,
  `due_date` tinyint(2) DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `prefered_acc` varchar(50) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `partner_id` (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


19/06/2017

ALTER table `tbl_orders` add column transaction_id varchar(50)

19/05/2017

alter
17/05/2017
alter table tbl_countries add column iso_code varchar(4);
alter table tbl_bank_accounts add column promise_acid varchar(50);
alter table tbl_bank_accounts add column country varchar(5);
alter table tbl_bank_accounts add column status tinyint(1);

15-05-2017

alter table tbl_partner_fees add column general_1 tinyint(1),add column general_2 tinyint(1),add column general_3 tinyint(1),add column general_4 tinyint(1),add column general_5 tinyint(1),add column general_6 tinyint(1),add column general_7 tinyint(1);


alter table tbl_users add column promise_uid varchar(100),add column promise_acid varchar(100);

12/05/2017
alter table tbl_partner_transactions_fees add column charge_type tinyint(2);
alter table  alpha.tbl_transactions_fees add column paid_within tinyint(2);

alter table tbl_partner_fees add column paid_day_1 tinyint(2);
alter table tbl_partner_fees add column paid_day_2 tinyint(2);
alter table tbl_partner_fees add column paid_day_3 tinyint(2);
alter table tbl_partner_fees add column paid_day_4 tinyint(2);
alter table tbl_partner_fees add column paid_day_5 tinyint(2);
alter table tbl_partner_fees add column paid_day_6 tinyint(2);
alter table tbl_partner_fees add column paid_day_7 tinyint(2);


10/-05-2017
create table tbl_partner_transactions_fees(
	id int(11) not null auto_increment primary key,
	partner_id int(11) not null,
	store_id int(11),
	order_id int(11),
	orderAmount decimal(10,2),
	transactionFees decimal(10,2),
	workorder_id int(11),
	is_deducted tinyint(1) default 0,
	created_at int(11),
	updated_at int(11),
	created_by int(11),
	updated_by int(11)
);

alter table tbl_order_item add column transactionFees decimal(10,2);

alter table alpha.tbl_profile add column tempid varchar(50);
alter table alpha.tbl_profile add column transactionrule tinyint(1);

08/-05-2017

create table tbl_transactions_fees(
id int (11) not null auto_increment primary key,
title varchar(150),
charge_type tinyint(2),
amount decimal(10,2),
created_at int(11),
updated_at int(11),
created_by int(11),
updated_by int(11)
);
alter table tbl_users add column promise_uid varchar(100) DEFAULT NULL,
 add column promise_acid varchar(100) DEFAULT NULL;

create table tbl_partner_fees(
	id int (11) not null auto_increment primary key,
	partner_id int(11),
	trans_id_1 tinyint(2),
	charge_type_1 tinyint(2),
	amount_1 decimal(10,2),
	trans_id_2 tinyint(2),
	charge_type_2 tinyint(2),
	amount_2 decimal(10,2),
	trans_id_3  tinyint(2),
	charge_type_3 tinyint(2),
	amount_3 decimal(10,2),
	trans_id_4 tinyint(2),
	charge_type_4 tinyint(2),
	amount_4 decimal(10,2),
	trans_id_5 tinyint(2),
	charge_type_5 tinyint(2),
	amount_5 decimal(10,2),
	trans_id_6 tinyint(2),
	charge_type_6 tinyint(2),
	amount_6 decimal(10,2),
	trans_id_7 tinyint(2),
	charge_type_7 tinyint(2),
	amount_7 decimal(10,2),
	created_at int(11),
	updated_at int(11),
	created_by int(11),
	updated_by int(11)
);
