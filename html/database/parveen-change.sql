/* Product database */
create table tbl_product(
id bigint primary key auto_increment,
name varchar(100),
sku varchar(20) unique,
image varchar(100),
description text,
urlkey varchar(200),
price double(10,2),
currency_id int,
currency_code varchar(20),
status tinyint(2) default 0,
is_online tinyint(2) default 1,
store_id int default 0,
tax_class int default 0,
created_at int,
updated_at int,
created_by int,
updated_by int
);

/* Image */
create table tbl_productImage(
id int primary key auto_increment,
imagename varchar(100),
product_id int,
status tinyint(2) default 0,
isDefault tinyint(2) default 0
);


/*Order*/






/* 9 Dec., 2015 */
/* Beacon view PSA */
drop table if exists tbl_beaconview;
drop table if exists tbl_userPsa;
CREATE TABLE `tbl_userPsa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `workorder_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  created_at int default 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;


/* 21 Dec. 2015 */
drop table if exists tbl_qrkit;
create table tbl_qrkit(
 id int(11) primary key auto_increment,
 data varchar(255),
 qrkey varchar(255) unique comment 'will be used in url',
 type tinyint(2) default 0 comment '0: qrcode, 1: barcode',
 pin varchar(10),
 status tinyint(2) default 1,
 created_at int,
 updated_at int,
 created_by int,
 updated_by int
);


create table tbl_shipper(
id int primary key auto_increment,
name varchar(100),
status tinyint(2) default 0,
 created_at int,
 updated_at int,
 created_by int,
 updated_by int
);

/* will need to update once shipper account info provided*/
create table tbl_shipper_setting
(
id int primary key auto_increment,
shipper_id int,

status tinyint(2) default 0,
 created_at int,
 updated_at int,
 created_by int,
 updated_by int
);

create table tbl_shipper_country(
id int primary key auto_increment,
shipper_id int,
country_id int,
 created_at int,
 updated_at int,
 created_by int,
 updated_by int
);
drop table if exists tbl_orders;
CREATE TABLE `tbl_orders` (
  `id` int primary key NOT NULL AUTO_INCREMENT ,  
  `status` tinyint(5) DEFAULT 1,
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',  
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
   shipper_id int comment 'Shipping via (Fedex, usp etc)',
  `is_offline` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT ' Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Offline Refunded',
  `total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Online Refunded',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty Ordered',  
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  `can_ship_partially` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Ship Partially',   
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',  
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',    
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `forced_shipment_with_invoice` smallint(5) unsigned DEFAULT NULL COMMENT 'Forced Do Shipment With Invoice',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `edit_increment` int(11) DEFAULT 0 COMMENT 'Edit Increment',
  created_at int,
  updated_at int,
created_by int,
updated_by int
  
  );
drop table if exists tbl_order_item;
create table tbl_order_item(
id int primary key auto_increment,
product_id int,
deal_id int,
deal_count int,
isCombo tinyint(2) default 0,
qty double(12,4),
order_id int,
user_id int,
store_id int,
store_location_id int,
product_price double(12,4),
product_currency_code varchar(10),
product_currency_id int,
payment_currency_code varchar(10),
payment_currency_id int,
exchange_rate double(12,4),
product_exchange_price double(12,4),
tax double(12,4),
shipping_charges double(12,4),
discount double(12,4),
total double(12,4),
status tinyint default 1,
created_at int,
updated_at int,
created_by int,
updated_by int
);

drop table if exists tbl_order_sub_item;
create table tbl_order_sub_item(
id int primary key auto_increment,
product_id int,
deal_id int,
qty double(12,4),
order_id int,
user_id int,
store_id int,
store_location_id int,
product_price double(12,4),
product_currency_code varchar(10),
product_currency_id int,
payment_currency_code varchar(10),
payment_currency_id int,
exchange_rate double(12,4),
product_exchange_price double(12,4),
tax double(12,4),
shipping_charges double(12,4),
discount double(12,4),
total double(12,4),
status tinyint default 1,
created_at int,
updated_at int,
created_by int,
updated_by int

);

create table tbl_order_status(
id int primary key auto_increment,
status varchar(100),
created_at int,
updated_at int,
created_by int,
updated_by int

);
drop table if exists tbl_address_type;
create table tbl_addresstype(
id int primary key auto_increment,
type varchar(100),
created_at int,
updated_at int,
created_by int,
updated_by int
);
INSERT INTO `sweetspotv2`.`tbl_addresstype` (`type`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES ('Home', '0', '0', '0', '0');
INSERT INTO `sweetspotv2`.`tbl_addresstype` (`type`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES ('Commerical', '0', '0', '0', '0');

drop table if exists tbl_address;
create table tbl_address(
id int primary key auto_increment,
fullname varchar(255),
addressLine1 varchar(255),
addressLine2 varchar(255),
city varchar(100),
state varchar(100),
pincode varchar(50),
country_id int,
mobile varchar(20),
landmark varchar(100),
address_type tinyint(3),
is_shipping tinyint(2) default 0,
is_billing tinyint(2) default 0,
user_id int,
created_at int,
updated_at int,
created_by int,
updated_by int
);

create table tbl_taxclass(
id varchar(50) primary key,
taxClass varchar(30),
created_at int,
updated_at int,
created_by int,
updated_by int
);

create table tbl_tax(
id varchar(50) primary key,
country_id int,
state_id int,
postcoderange tinyint(2) default 0 comment 'in case of range postcode,postcodeto should be int', 
postcode varchar(30) comment "'*' - matches any; 'xyz*' - matches any that begins on 'xyz' and not longer than 10.",
postcodeTo varchar(30), 
rate double(12,4) comment 'tax rate in percent',
created_at int,
updated_at int,
created_by int,
updated_by int
);

create table tbl_taxrule(
id varchar(50) primary key,
productclass varchar(30),
country_id int,
priority_id tinyint(4) default 0,
sort_id tinyint(4) default 0,
created_at int,
updated_at int,
created_by int,
updated_by int
);

create table tbl_taxrule_item(
id varchar(50) primary key,
tax_id int,
is_delete tinyint(2) default 0,
created_at int,
updated_at int,
created_by int,
updated_by int
);

create table tbl_order_tax(
id int primary key auto_increment,
order_id int,
code varchar(100),
percent decimal(10,4),
amount decimal(10,4),
created_at int,
updated_at int,
created_by int,
updated_by int

);

create table tbl_account(
id int primary key auto_increment,
user_id int,
bt_id int,
status tinyint(2) default 0,
created_at int,
updated_at int,
created_by int,
updated_by int
);

drop table if exists tbl_transcaction_detail;
create table tbl_transaction_detail(
id int primary key auto_increment,
tid varchar(20) unique,        
order_id int,
user_id int,
status varchar(100),
type varchar(50),
currencyIsoCode varchar(10),
amount double(12,4),
merchantAccountId varchar(100),
created_at int
);

drop table tbl_paymentMethod;
create table tbl_paymentMethod(
id int primary key auto_increment,
order_id int,
user_id int,
bin int(6) comment 'card first 6 digit',
last4 int(4) comment 'cart last 4 digit',
cardType varchar(70),
expirationMonth int(2) comment 'expiry month',
expirationYear int(4) comment 'expiry year',
customerLocation varchar(10),
cardholderName varchar(100),
imageUrl varchar(500),
prepaid varchar(100),
healthcare varchar(100),
debit varchar(100),
durbinRegulated varchar(100),
commercial varchar(100),
payroll varchar(100),
issuingBank varchar(100),
countryOfIssuance varchar(100),
productId varchar(100),
uniqueNumberIdentifier varchar(80),
venmoSdk varchar(100),
expirationDate varchar(8),
maskedNumber varchar(20),
token varchar(40),
created_at int ,
updated_at int
);


create table tbl_ordercomment(
id int auto_increment primary key,
comment text,
status tinyint(2) default 0,
onWeb tinyint(2) default 0,
author_id int,
order_id int,
email_to varchar(200),
created_at int,
updated_at int,
created_by int,
updated_by int
);


drop table if exists tbl_cart;
create table tbl_cart(
session_key varchar(255),
user_id int,
deal_id int,
dealCount tinyint default 1,
status tinyint default 1,
created_at int
);


alter table tbl_workorders add column usedPasses int default 0 after noPasses;
alter table tbl_address add column user_id int;


#4 Jan 2016--Done
alter table tbl_transaction_detail add column updated_at int after created_at;
alter table tbl_addresstype add column is_delete tinyint(2) default 0 after type;

#6 Jan 2016
alter table tbl_order_item add column payable double(12,4) default 0 after total;
update tbl_order_item set payable= total-discount+tax+shipping_charges where id!=0;


# loyalty
drop table tbl_loyalty ;
create table tbl_loyalty(
id int primary key auto_increment,
loyalty double(8,2) comment "1 Dollor = X loyalty point",
dolor double(8,2) default 1,
pointvalue double(8,4),
user_id int comment "partner/store id",
loyalty_pin int(4),
status tinyint(2) default 0,
created_at int,
updated_at int,
created_by int,
updated_by int
);

drop table tbl_loyaltypoints;
create table tbl_loyaltypoints(
id int primary key auto_increment,
loyaltypoints double(8,2),
valueOnDay double(8,2) comment "loyalty point value on the day",
order_id int,
user_id int,
store_id int,
method tinyint(3) default 0 ,
status tinyint(3) default 0,
created_at int,
updated_at int,
created_by int,
updated_by int
);


#15 jan


create table tbl_user_loyalty(
id int primary key auto_increment,
user_id int,
store_id int,
value double(12,4),
created_at int,
updated_at int
);

create table tbl_order_loyalty(
id int primary key auto_increment,
order_id int,
user_id int,
store_id int,
payable double(12,4),
value double(8,2)
);

delimiter $$
create trigger tri_addloyalty  after insert on tbl_loyaltypoints 
FOR EACH ROW 
BEGIN
update tbl_user_loyalty set value=value + (NEW.valueOnDay * NEW.loyaltypoints), updated_at=NEW.updated_at where user_id=NEW.user_id
and store_id=NEW.store_id;

END$$





create trigger tri_useloyalty  after insert on tbl_order_loyalty
FOR EACH ROW 
BEGIN
update tbl_user_loyalty set value=value - (NEW.value) where user_id=NEW.user_id
and store_id=NEW.store_id;
END$$

select * from tbl_order_loyalty $$
select unix_timestamp();

27 Jan., 2016
==========================================================

delimiter $$
drop trigger if exists tri_addloyalty$$
create trigger tri_addloyalty  after insert on tbl_loyaltypoints 
FOR EACH ROW 
BEGIN
insert into tbl_user_loyalty (user_id,store_id,value,created_at,updated_at,loyaltyID) values
(new.user_id,new.store_id,ifnull((NEW.valueOnDay * NEW.loyaltypoints),0),new.updated_at,new.updated_at,concat(new.user_id,"-",new.store_id))
on duplicate key 
update  
value=value + (NEW.valueOnDay * NEW.loyaltypoints), 
updated_at=NEW.updated_at;
#where user_id=NEW.user_id
#and store_id=NEW.store_id;

END$$

delimiter ;

alter table  tbl_user_loyalty add column loyaltyID varchar(50) first;

update tbl_user_loyalty set loyaltyID=concat(user_id, "-", store_id) ;
select * from tbl_user_loyalty;


#======================================================================================================


/* Date 29-01-2016 (workorder passes ) */

ALTER TABLE `tbl_workorder_popup` ADD `ticketinfo` VARCHAR(100) NULL DEFAULT NULL AFTER `eventDate`;

UPDATE `tbl_workorders` SET type=4 WHERE type= 'Event ticket'


ALTER TABLE `tbl_workorders` CHANGE `type` `type` INT(12) NULL DEFAULT NULL;
ALTER TABLE `tbl_workorder_popup` ADD `eventVenue` VARCHAR(250) NULL DEFAULT NULL AFTER `eventDate`;

/* Redeem Loyalty */
ALTER TABLE tbl_loyalty  ADD  redeem_loyalty double(8,2) NOT NULL DEFAULT '0.00' after user_id;
ALTER TABLE tbl_loyalty ADD  redeem_dolor double(8,2) NOT NULL DEFAULT '0.00' after redeem_loyalty;
ALTER TABLE tbl_loyalty ADD  redeem_pointvalue double(8,2) NOT NULL DEFAULT '0.00' comment "1 point =x dolor" after redeem_loyalty;
ALTER TABLE tbl_loyaltypoints ADD  rValueOnDay double(8,2) NOT NULL DEFAULT '0.00' comment "1 point =x dolor" after valueOnDay;

#4 Feb
delimiter $$
drop trigger if exists tri_addloyalty$$
create trigger tri_addloyalty  after insert on tbl_loyaltypoints 
FOR EACH ROW 
BEGIN
insert into tbl_user_loyalty (user_id,store_id,value,created_at,updated_at,loyaltyID,points) values
(new.user_id,new.store_id,ifnull((NEW.rValueOnDay * NEW.loyaltypoints),0),new.updated_at,new.updated_at,
concat(new.user_id,"-",new.store_id),New.loyaltypoints)
on duplicate key 
update  
value=value + (NEW.rValueOnDay * NEW.loyaltypoints), 
points=points + NEW.loyaltypoints, 
updated_at=NEW.updated_at;
#where user_id=NEW.user_id
#and store_id=NEW.store_id;

END$$

delimiter ;



#==============Harsh==================================
ALTER TABLE `tbl_passes` ADD `offerTitle` VARCHAR(300) NOT NULL;
ALTER TABLE `tbl_passes` ADD `offerText` VARCHAR(300) NOT NULL;
ALTER TABLE `tbl_passes` ADD `offerwas` VARCHAR(100) NOT NULL;
ALTER TABLE `tbl_passes` ADD `offernow` VARCHAR(100) NOT NULL;
ALTER TABLE `tbl_passes` ADD `readTerms` LONGTEXT NOT NULL;
ALTER TABLE `tbl_passes` ADD `sspMoreinfo` TEXT NOT NULL;


/* 8 Feb. other loyalty cards */
create table tbl_loyaltyCards(
id int primary key auto_increment,
brand varchar(255),
image varchar(255),
data text,
type tinyint(2) default 0,
created_at int default 0,
updated_at int default 0,
created_by int,
updated_by int 
);

update tbl_user_loyalty t set t.points=t.value* ifnull((select ts.redeem_pointvalue from tbl_loyalty ts 
where ts.user_id=t.user_id and status=1 order by id desc limit 1),1 ) ;

alter table tbl_user_loyalty add column points double(12,4) default 0 after value;




alter table tbl_passes add column isUsed tinyint(2) default 0 after workorder_id;
alter table tbl_workorders add column uniqueID varchar(30) default '' after buynow; 
delimiter $$
drop trigger if exists tri_workorder_unid$$
create trigger tri_workorder_unid before insert on tbl_workorders
for each row
begin
  declare aid int default 0;
  if (new.uniqueID is null or new.uniqueID="") then
  select auto_increment into aid from information_schema.tables  where table_name ='tbl_workorders' and table_schema = database();
  set new.uniqueID=concat(date_format(now(),"%Y"),"-",LPAD(new.advertiserID,4,0),"-",LPAD(aid,6,0));
  end if;
end$$

delimiter ;

ALTER TABLE `tbl_loyaltypoints` 
CHANGE COLUMN `transaction_id` `transaction_id` VARCHAR(50) NULL DEFAULT NULL ;

# need api changes
alter table tbl_loyaltypoints add column workorderid int default 0 after transaction_id; 

# digital stamp track
create table tbl_offerTrack(
id int primary key auto_increment,
user_id int,
workorder_id int,
usedNo int default 0,
pin varchar(10),
created_at int default 0,
created_by int default 0,
updated_at int default 0,
updated_by int default 0
);



drop table if exists tbl_loyaltyCards;
CREATE TABLE `tbl_loyaltyCards` (
  `id` int(11) NOT NULL AUTO_INCREMENT primary key,
  `brand` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nameOnCard` varchar(20) DEFAULT NULL,
  `cardNumber` varchar(100) DEFAULT NULL,
  `expiry` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `data` varchar(250) DEFAULT NULL,
  `actualData` varchar(250) DEFAULT NULL,
  `type` tinyint(2) DEFAULT '0',
  `loyaltystatus` int(2) DEFAULT NULL,
  `created_at` int(11) DEFAULT '0',
  `updated_at` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL  
);



/************* add new field in workorder table 25-02-2016*******************************************/
ALTER TABLE `tbl_workorders` ADD `allowpasstouser` INT(11) NULL DEFAULT NULL AFTER `usedPasses`;
ALTER TABLE `tbl_passes` ADD `count_passes` INT(11) NULL DEFAULT 0 ;
/************ end here *********************************************************/
#10 March
 alter table tbl_workorders add column qtyPerPass int(3) default 0 after usedPasses;
 ALTER TABLE `tbl_offerTrack` ADD `qty` INT NULL DEFAULT NULL AFTER `usedNo`;

delimiter $$
drop trigger if exists tri_useloyalty$$

create trigger tri_useloyalty  after insert on tbl_order_loyalty
FOR EACH ROW 
BEGIN
update tbl_user_loyalty set  
points=points-((points/value)*NEW.value),
value=value - (NEW.value)
 where user_id=NEW.user_id
and store_id=NEW.store_id;
END$$

##7 April
alter table tbl_users add column device_token varchar(200) default '' after device;
ALTER TABLE `SweetSpotV2`.`tbl_order_loyalty` 
CHANGE COLUMN `value` `value` DOUBLE(12,4) NULL DEFAULT NULL ;

alter table tbl_workorder_popup change uploadTypes uploadTypes tinyint(2) default 1;


create table tbl_agegroup
(
id int primary key auto_increment,
name varchar(100),
minAge int default 15,
maxAge int default 18,
status tinyint(2) default 0
);insert into tbl_agegroup (name,minAge,maxAge,status)
values('Between 15-18 years',15,18,1),
('Between 19-25 years',19,25,1),
('Between 26-35 years',26,35,1),
('Between 36-45 years',36,45,1),
('Between 46-60 years',46,60,1),
('More than 61 years',61,999,1);
alter table tbl_users add column ageGroup_id int default 1 after dob;

update tbl_users set ageGroup_id=(select t.id from tbl_agegroup t where 
 (TIMESTAMPDIFF(YEAR, from_unixtime(dob), CURDATE())) >= t.minAge
and  (TIMESTAMPDIFF(YEAR, from_unixtime(dob), CURDATE()))  <= t.maxAge)
where dob > 0 
and  (TIMESTAMPDIFF(YEAR, from_unixtime(dob), CURDATE())) >=15 and id!=0;

drop table tbl_user_access_log;
create table tbl_user_access_log(
id int primary key auto_increment,
user_id int unique,
lastvisit int

);


#=============================================Done above 8 April================================
ALTER TABLE tbl_deal ADD dealvideokey VARCHAR(255) NULL DEFAULT '' ;
ALTER TABLE tbl_deal ADD dealvideoTypes tinyint(2) NULL DEFAULT 0 after dealvideokey;



#24 april
INSERT INTO `tbl_usertype` (`id`, `usertype`, `status`) VALUES ('9', 'Employee', '1');
alter table tbl_userpsa add constraint unique key (user_id,workorder_id);
alter table tbl_userpsa add column updated_at int default 0 after created_at;

#29 April
alter table tbl_user_loyalty add column accuredpoints double(12,4) default 0 after points;
update tbl_user_loyalty t set accuredpoints=
(select sum(loyaltypoints) from tbl_loyaltypoints tp where tp.user_id=t.user_id 
and tp.store_id=t.store_id and loyaltypoints>0) ;




delimiter $$
drop trigger if exists tri_addloyalty$$
create trigger tri_addloyalty  after insert on tbl_loyaltypoints 
FOR EACH ROW 
BEGIN
insert into tbl_user_loyalty (user_id,store_id,value,created_at,updated_at,loyaltyID,points,accuredpoints) values
(new.user_id,new.store_id,ifnull((NEW.rValueOnDay * NEW.loyaltypoints),0),new.updated_at,new.updated_at,
concat(new.user_id,"-",new.store_id),New.loyaltypoints,new.loyaltypoints)
on duplicate key 
update  
value=value + (NEW.rValueOnDay * NEW.loyaltypoints), 
points=points + NEW.loyaltypoints, 
accuredpoints=accuredpoints + NEW.loyaltypoints, 
updated_at=NEW.updated_at;
#where user_id=NEW.user_id
#and store_id=NEW.store_id;

END$$

#3 May
alter table tbl_users add column loyaltyPin int  after walletPinCode;


#=============================================Done above 8 May================================
delimiter ;
alter table tbl_users add column bdayOffer tinyint(2) default 0 after ageGroup_id;
alter table tbl_users add column timezone_offset varchar(20) after longitude;
alter table tbl_users add column timezone varchar(20) after longitude;




create table tbl_notifications(
id int primary key auto_increment,
user_id int,
msg text,
type tinyint(2) default 0 comment "0:bday, 1:broadcast, 2:special",
is_read tinyint(2) default 0,
is_delete tinyint(2) default 0,
created_at int default 0,
updated_at int default 0
);
 
  
=============== 24 May ==============
create table tbl_notification_setting(
id int primary key auto_increment,
user_id int,
is_on tinyint(2) default 1,
notification_allow int default 0
);
alter table tbl_users add column advertister_id int after loyaltyPin;

 
select * from tbl_users where user_type=3; 
select unix_timestamp(date(from_unixtime(dob))), curdate() from tbl_users;
update tbl_users set timezone_offset='+10:30' where id=108;
select * from tbl_users where username='parasg';
 
select timezone_offset,convert_TZ(DATE_ADD(date(from_unixtime(dob)),INTERVAL 10 Hour),'SYSTEM',timezone_offset),id
,CONVERT_TZ(now(), 'SYSTEM', timezone_offset),
hour(CONVERT_TZ(now(), 'SYSTEM', timezone_offset))
,convert_TZ(date_add(date(from_unixtime(dob)), interval 7 day),'SYSTEM',timezone_offset) 
from tbl_users where user_type=3 and dob>0
#where (CONVERT_TZ(now(), 'SYSTEM', timezone_offset))=10
#and unix_timestamp() >= unix_timestamp(convert_TZ(date(from_unixtime(dob)),'SYSTEM',timezone_offset)) 
#and unix_timestamp() < unix_timestamp(convert_TZ(date_add(date(from_unixtime(dob)), interval 7 day),'SYSTEM',timezone_offset)) 
and dayofmonth(from_unixtime(dob))=dayofmonth(CONVERT_TZ(now(), 'SYSTEM', timezone_offset)) 
and month(from_unixtime(dob)) = month(CONVERT_TZ(now(), 'SYSTEM', timezone_offset))
and hour(CONVERT_TZ(now(), 'SYSTEM', timezone_offset))=10
;
 
select device_token,device from tbl_users where user_type=3 and dob>0
and dayofmonth(from_unixtime(dob))=dayofmonth(CONVERT_TZ(now(), 'SYSTEM', timezone_offset)) 
and month(from_unixtime(dob)) = month(CONVERT_TZ(now(), 'SYSTEM', timezone_offset))
and hour(CONVERT_TZ(now(), 'SYSTEM', timezone_offset))=21 
and device_token is not null and device_token!=""
;
 
 
SELECT `device`, `device_token` FROM `tbl_users` WHERE (((((`dob` > 0) 
AND (dayofmonth(from_unixtime(dob))=dayofmonth(CONVERT_TZ(now(), 'SYSTEM', timezone_offset)))) 
AND (month(from_unixtime(dob)) = month(CONVERT_TZ(now(), 'SYSTEM', timezone_offset)))) 
AND ( hour(CONVERT_TZ(now(), 'SYSTEM', timezone_offset))=21)) AND (device_token is not null)) AND (device_token != '')
;
 
UPDATE `sweetspotv2demo`.`tbl_users` SET `dob`=unix_timestamp("2002-05-19") WHERE `id`='108';
select dayofmonth("2016-03-01"),month("2016-03-01");
 
 

/* 7 June 2016*/
CREATE TABLE tbl_zone_notification_stats(
 id int primary key auto_increment,
 user_id int,
 deal_id varchar(255),
 notification_limit int,
 remaining_limit int,
 created_at int,
 updated_at int
);




/*
#23 feb payment cards
alter table tbl_walletpcards drop column cvv;
alter table tbl_walletpcards add column cardTypeText varchar(30) default '' after cardType;
alter table tbl_walletpcards add column imageUrl varchar(255) default '' after cardTypeText;
alter table tbl_walletpcards add column unid varchar(255) default '' after expiry;



alter table tbl_walletpcards add unique(user_id,unid);
alter table tbl_paymentmethod add column isSave tinyint(2) default 0;
alter table tbl_paymentmethod add column cardType int(5) default 0;
delimiter $$
drop trigger if exists tri_payment_method_cards$$
create trigger tri_payment_method_cards after insert on tbl_paymentmethod
for each row
begin
  if (new.isSave =1) then  
INSERT INTO tbl_walletpcards (`cardType`,`cardTypeText`,`imageUrl`,`user_id`,`nameOnCard`,
`cardNumber`,`expiry`,`unid`,`created_at`,`updated_at`)
VALUES
(
new.cardType,new.cardType,new.imageUrl,new.user_id,new.cardHolderName, new.maskedNumber,
unix_timestamp(last_day(concat(expirationYear,"-",expirationMonth,"-1")))+86400,
new.uniqueNumberIdentifier,
new.created_at,
new.updated_at)
on duplicate key
update updated_at=new.updated_at

;

  end if;
end$$
delimiter ;





delimiter $$

CREATE EVENT event_campaignComplete
    ON SCHEDULE  EVERY 1 DAY 
    STARTS '2016-04-27 00:15:00' ON COMPLETION PRESERVE ENABLE 
    DO
      Begin
      update tbl_workorders set status=4 where dateTo +86400 > unix_timestamp() and status=1; 
      end $$


*/
/* 16 June 2016  */
alter table tbl_beacon_group add column type tinyint(4) default 0 after status;
alter table tbl_beacon_group add column parent_id tinyint(4) default 0 after type;
alter table tbl_beacon_group add column grandparent_id tinyint(4) default 0 after parent_id;

/* 29 June*/
show triggers;
delimiter $$
drop trigger if exists tri_useloyalty$$
create trigger tri_useloyalty  after insert on tbl_order_loyalty
FOR EACH ROW 
BEGIN
update tbl_user_loyalty set  points=ifnull(
points-((points/value)*NEW.value),0),
value=value - (NEW.value) where user_id=NEW.user_id and store_id=NEW.store_id;
END$$


-------------------------------------------------------------------------------------
All above done
---------------------------------------------------
INSERT INTO `tbl_auth_item` (`name`, `type`, `description`,created_at,updated_at) VALUES 
('workorders/getzone', '2', 'workorders getzone',unix_timestamp(),unix_timestamp()),
('workorders/deletezone', '2', 'workorders Delete zone',unix_timestamp(),unix_timestamp());


