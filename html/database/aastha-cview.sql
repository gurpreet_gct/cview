#17-04-2018

ALTER TABLE `tbl_agencies` add `logo` varchar(255) null default null after `name`;
ALTER TABLE `tbl_agencies` add `address` varchar(255) null default null after `logo`;

alter table `tbl_users` add `agency_id` int(11) null default null after `referral_percentage`;

#18-04-2018

alter table `tbl_agencies` add `latitude` varchar(255) null default null after `address`;
alter table `tbl_agencies` add `longitude` varchar(255) null default null after `latitude`;
alter table `tbl_agencies` add `email` varchar(100) null default null after `name`;
alter table `tbl_agencies` add `mobile` bigint(20) null default null after `email`;

create table `tbl_agencies_offices`(
	`id` int(11) NOT NULL auto_increment,
	`agency_id` int(11) default null,
	`address` varchar(255) default null,
	`latitude` decimal(20,15) default null,
	`longitude` decimal(20,15) default null,
	`created_at` int(11) DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_at` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL,
	primary key ( `id` )
);

#19-04-2018

create table if not exists `tbl_gallery`(
	`id` int(11) NOT NULL auto_increment,
	`listing_id` int(11) default null,
	`image` varchar(255) default null,
	primary key ( `id` )
);


#25-04-2018

ALTER TABLE `tbl_properties` CHANGE `user_id` `user_id` INT(11) NULL DEFAULT NULL COMMENT 'agency_id';
ALTER TABLE `tbl_properties` CHANGE `user_id` `agency_id` INT(11) NULL DEFAULT NULL COMMENT 'agency_id';

#26-04-2018

ALTER TABLE `tbl_auction_event` CHANGE `auction_status` `auction_status` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0, (1:auction,2:streaming,3:offline)';

#27-04-2018

create table if not exists `tbl_cview_states`(
	`id` int(11) NOT NULL auto_increment,
	`name` varchar(255) default null,
	primary key ( `id` )
);

#30-05-2018

alter table `tbl_apply_bid` add `rating` float null default null after `status`;

#02-05-2018

ALTER TABLE `tbl_properties` CHANGE `status` `status` TINYINT(3) NULL DEFAULT NULL COMMENT '1- For Sale, 2- For Rent. 3 - For Lease';


#07-05-2018

	create table if not exists `tbl_verification`(
	`id` int(11) not null auto_increment,
	`user_id` int(11) default null,
	`verified_by` int(11) default null,
	`listing_id` int(11) default null comment 'property_id',
	`type` tinyint(2) default null comment '1-mannual,2-system',
	`data` text default null,
	`complete` int(11) not null default 0,
	`created_at` int(11) DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_at` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL,
	primary key ( `id` )
	);

alter table `tbl_apply_bid` add `bid_approved` tinyint(1) null default null after `rating`;

#08-05-2018

alter table `tbl_agencies` add `about_us` text default null after `longitude`;

#09-05-2018

ALTER TABLE `tbl_auction_event` CHANGE `document` `document` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '';

ALTER TABLE `tbl_properties` ADD `videoImage` varchar(300) null default null after `video`;	

alter table `tbl_agencies` add `backgroungImage` varchar(300) null default null after `logo`;

#10-05-2018

create table if not exists `tbl_agency_favourite`(
	`id` int(11) not null auto_increment,
	`agency_id` int(11) null default null,
	`user_id` int(11) null default null,
	`is_favourite` tinyint(2) not null default 0 comment '0:unfavorite,1:favourite',
	`created_at` int(11) DEFAULT NULL,
	`created_by` int(11) DEFAULT NULL,
	`updated_at` int(11) DEFAULT NULL,
	`updated_by` int(11) DEFAULT NULL,
	primary key(`id`)
);

#11-05-2018

alter table `tbl_apply_bid` add `add_to_calendar` tinyint(2) null default null comment '1:added to calendar';

#15-05-2018

alter table `tbl_agencies_offices` add `name` varchar(100) null default null after `agency_id`; 
alter table `tbl_agencies_offices` add `email` varchar(100) null default null after `name`; 
alter table `tbl_agencies_offices` add `mobile` bigint(20) null default null after `email`; 
