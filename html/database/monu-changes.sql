

#------------18-05-2016--------------------- in order loyalty for apply deal condition 

ALTER TABLE `tbl_order_loyalty` ADD `created_at` INT NULL DEFAULT NULL AFTER `value`;

#----- 25-05-2016 ---------------- beacon manager  
ALTER TABLE `tbl_beacon_manager` CHANGE `manufacturerSerial` `manufacturerSerial` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

#-----06-06-2016--------------profile table
ALTER TABLE `tbl_profile` ADD `food Establishment` TINYINT NOT NULL DEFAULT '0' AFTER `addcolors`;
#-------09-06-2016 -  walat payment card  for brain Tree
ALTER TABLE `tbl_walletpcards` ADD `maskedNumber` VARCHAR(20) NULL DEFAULT NULL AFTER `expiry`;
ALTER TABLE `tbl_walletpcards` ADD `token` VARCHAR(40) NULL DEFAULT NULL AFTER `cvv`;
ALTER TABLE `tbl_walletpcards` ADD `customtoken` VARCHAR(40) NULL DEFAULT NULL AFTER `token`;
ALTER TABLE `tbl_walletpcards` ADD `katon` VARCHAR(100) NULL DEFAULT NULL AFTER `token`;

#-----   14-06-2016 --- beacon manager 

ALTER TABLE `tbl_beacon_manager` DROP `businessType`, DROP `businessWebAddress`, DROP `beaconSoundTitle`, DROP `passID`, DROP `passUrl`, DROP `locationBrockerID`, DROP `imgBeaconLoc`;

ALTER TABLE `tbl_beacon_manager` CHANGE `identifier` `notesBox` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `tbl_beacon_manager` CHANGE `notesBox` `notesBox` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `tbl_beacon_manager` CHANGE `minor` `minor` BIGINT(11) NOT NULL;
ALTER TABLE `tbl_beacon_manager` CHANGE `major` `major` BIGINT(11) NOT NULL;

#------- 20-06-2016 --- Beacon Group
ALTER TABLE `tbl_beacon_group` ADD `city` VARCHAR(150) NULL DEFAULT NULL AFTER `parent_id`;

#----23-06-2016 ---
ALTER TABLE `tbl_workorders` ADD `geoProximity` INT NULL DEFAULT 2 AFTER `workorderType`;


#-----28-06-2016-
#--
ALTER TABLE `tbl_orders` ADD `order_type` INT NOT NULL DEFAULT '0' AFTER `user_id` ;

--------------------------------------------------------------------------------------------------------------------------------------------

------01-07-2016--------------------------------------------------
ALTER TABLE `tbl_userpsa` ADD `type` INT NULL DEFAULT NULL AFTER `workorder_id`;
