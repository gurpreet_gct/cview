#Report Page 7
#Average Transaction Value
select sum(amount)/count(id),sum(amount), count(id) from tbl_transaction_detail where created_at > unix_timestamp("2016-04-10");

#Bar Chart Payment via
select count(if(grand_total-discount_amount>total_paid,1,null)) as loyaltyandpay, count(if(grand_total-discount_amount=total_paid,1,null)) as onlypay,count(if(total_paid=0,1,null)) as loyalty from tbl_orders where is_offline=0;
# user to update the old records
#update tbl_orders set total_paid=(select sum(payable) from tbl_order_loyalty where order_id=tbl_orders.id) where id!=0;

#PIE Chart which credit card is used for each transaction
select count(id),cardType from tbl_paymentMethod where order_id is not null group by cardType;

#PIE chart how many cart are abadomen
select  (select count(session_key) from tbl_cart)*100 / (select count(id) from tbl_orders) ;

#BAR CHART:TRANSACTIONS BY GENDER / AGE / UNITS

#BAR CHART:
#TRANSACTIONS BY GENDER / AGE / UNITS
#How many transactions broken down by gender by age and also how many units per transaction

select tag.id,name,count(if(sex=1,tos.id,null)) as MaleTransaction,
count(if(sex=0,tos.id,null)) as femaleTransaction,
ifnull(sum(if(sex=1,deal_count,0)),0) as maleUnit,
ifnull(sum(if(sex=0,deal_count,0)),0) as femaleUnit
from tbl_agegroup tag
left join tbl_users t on tag.id=t.ageGroup_id and user_type=3  
left join tbl_orders tos on t.id=tos.user_id
left join tbl_order_item toi on toi.order_id=tos.id 
group by tag.id;





###############################################################
######################Report 6-Loyalty ########################
###############################################################


#GRAPH OFFLINE TRANSACTIONS How many offline loyalty transactions are there?
select count(id),store_id from tbl_loyaltypoints where order_id=0 group by store_id;


#BAR CHART: TOTAL POINTS ACCRUED / REDEEMED How many points accrued (by age and gender) and redeemed 
select tag.id,name,sex,
ifnull(sum(if(sex=1,tul.accuredpoints,0)),0) as maleAccured, 
ifnull(sum(if(sex=0,tul.accuredpoints,0)),0) as femaleAccured, 
ifnull(sum(if(sex=1,tul.accuredpoints,0)),0)-ifnull(sum(if(sex=1,tul.points,0)),0) as maleRedeem,  
ifnull(sum(if(sex=0,tul.accuredpoints,0)),0)-ifnull(sum(if(sex=0,tul.points,0)),0) as femaleRedeem  
from tbl_agegroup tag
left join tbl_users t on tag.id=t.ageGroup_id and user_type=3  
left join tbl_user_loyalty tul on tul.user_id=t.id
group by tag.id;


#BAR CHART: TOTAL POINTS ACCRUED How many points accrued (by age and gender)?
select tag.id,name,
ifnull(sum(if(sex=1,tul.accuredpoints,0)),0) as maleAccured, 
ifnull(sum(if(sex=0,tul.accuredpoints,0)),0) as femaleAccured 
from tbl_agegroup tag
left join tbl_users t on tag.id=t.ageGroup_id and user_type=3  
left join tbl_user_loyalty tul on tul.user_id=t.id
group by tag.id;

#BUTTON: POINTS LIABILITY  Total dollar value accrues by users available for redemption
select sum(value) from tbl_order_loyalty;

#BUTTON: AVERAGE TRANSACTION VALUE What is the average transaction value of loyal customers?
select sum(value)/count(Distinct user_id) from tbl_order_loyalty;

#BUTTON: ACTIVE LOYAL CUSTOMERS How many active loyal customers are there?
select count(distinct user_id) from tbl_user_loyalty where value > 0;



###############################################################
######################Report 5-Campaign########################
###############################################################

#BUTTON: TOTAL CAMPAIGNS RUNNING How many total campaigns running?
select count(id) from tbl_workorders where status=1 and type!=5;


#PIE CHART: TOP PERFORMING CAMPAIGNS Top 5 performing campaigns
select id,(usedPasses*100/noPasses)/(unix_timestamp()-dateFrom)/86400 as factor, from_unixtime(dateFrom),from_unixtime(dateTo)  from tbl_workorders where status=1 and type!=5 order by factor desc;

#GRAPH CAMPAIGNS REDEMPTION How many campaigns were redeemed in-store versus in-app?
select sum(if(is_offline=0,deal_count,null)) as ol, sum(if(is_offline=1,deal_count,null)) as of 
from tbl_orders t 
join tbl_order_item on order_id=t.id ;

#BAR CHART: CAMPAIGNS BY CATEGORY What are the top and bottom performing campaigns by category?
#in progress
select id,(usedPasses*100/noPasses)/(unix_timestamp()-dateFrom)/86400 as factor, from_unixtime(dateFrom),from_unixtime(dateTo)  from tbl_workorders where status=1 and type!=5 order by factor desc;

###############################################################
######################Report 4-Beacons ########################
###############################################################



###############################################################
####################Report 3- Deals/Passes#####################
###############################################################

#GRAPH:
#PASSES (SAVED VS. REDEEMED)
#How many passes have been saved to wallet versus how many passes redeemed vs monthly minimum quota (how many left)
select sum(isUsed) as saved,sum(passesTrack) as redeemed from tbl_passes;


#BAR GRAPH:
#PASSES PER CATEGORY
#How many passes have been saved / redeemed per category?
select tc.id, tc.name, sum(isUsed) as saved,sum(passesTrack) as redeemed 
from tbl_categories tc
join tbl_workorderCategory twc on twc.category_id=tc.id and lvl=0 
join tbl_passes t on t.workorder_id=twc.workorder_id and twc.status=1
where tc.lvl=0
group by tc.id;




#BUTTON:
#MONTHLY QUOTA - PASSES
#How many passes have been saved vs how many still available to be sent?
select usedPasses,noPasses from tbl_workorders where status=1 and type!=5; 

#GRAPH:
#NOTIFICATIONS (SENT VS. OPENED)
#How many notifications were sent versus how many are opened?
select count(if(status=0,1,null)) as 'notOpened',count(if(status=1,1,null)) as 'opended' from tbl_userpsa ;

#GRAPH:
#POP-UPS (BY CATEGORY)
#How many beacon pop-ups were bookmarked by category?



###############################################################
######################Report 2- Users #########################
###############################################################

#CATEGORIES (TOP / BOTTOM 3)
#What are the top 3 categories selected and bottom 3 categories selected?
select tc.id,tc.name, count(t.id) as count from tbl_userCategory t 
join tbl_categories tc on t.category_id=tc.id and tc.lvl=0 
where  t.status=1 group by category_id order by count desc ;


#BUTTON:
#RIGHT NOW THERE ARE XX ACTIVE USERS ON THE APP
select count(tual.id) as activeUser
from  tbl_user_access_log tual where unix_timestamp()-lastvisit<=900;


#Report 1:



#BAR CHART:
#ACTIVE USERS (BY GENDER / AGE)
#How many active users (user that has opened up the app) – represented by gender and age

select tag.id,name,sex,
count(if(sex=1,tual.id,null)) as maleActiveUser,
count(if(sex=0,tual.id,null)) as femaleActiveUser
from tbl_agegroup tag
left join tbl_users t on tag.id=t.ageGroup_id and user_type=3  
left join tbl_user_access_log tual on tual.user_id=t.id and unix_timestamp()-lastvisit<=3600 and tual.id is not null

group by tag.id;
 
 
 

#GRAPH:
#NOTIFICATIONS (SENT VS. OPENED)
#How many notifications were sent versus how many are opened?
select count(if(status=0,1,null)) as 'notOpened',count(if(status=1,1,null)) as 'opended' from tbl_userpsa ;