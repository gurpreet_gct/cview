13-4-2018

ALTER TABLE `tbl_auction_event` CHANGE `type` `auction_type` TINYINT(2) NULL DEFAULT NULL COMMENT '1 = Sell, 2 = Rent';

ALTER TABLE `tbl_auction_event` CHANGE `status` `auction_status` TINYINT(4) NOT NULL DEFAULT '0';
