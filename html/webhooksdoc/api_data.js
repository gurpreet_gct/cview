define({ "api": [
  {
    "type": "POST",
    "url": "/api/web/v1/webhooks/agencies",
    "title": "Create an Agency",
    "name": "AddAgency",
    "version": "0.1.1",
    "group": "Agency_Webhooks",
    "description": "<p>To create an Agency (Webhook)</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> <li><code>Form-Data must be of raw type</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "RAW Data\n{\n  \"data\": {\n    \"cv_agency_id\": \"CV-82AA85\",\n    \"name\": \"Offsice_titled\",\n    \"address\": \" sco 206 - 207, sector 34 a, chandigarh - 160022\",\n    \"logo\": \"logo_192x168.jpg\",\n    \"latitude\": \"30.72589\",\n    \"longitude\": \"76.75787\",\n    \"email\": \"usesr_testingd@yopmail.com\",\n    \"mobile\": \"9468594658\",\n    \"about_us\": \"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": \"35\",\n    \"cv_agency_id\": \"CV-82AA85\",\n    \"name\": \"Office_name\",\n    \"address\": \" sco 206 - 207, sector 34 a, chandigarh - 160022\",\n    \"logo\": \"logo_192x1x68.jpg\",\n    \"latitude\": \"30.72589\",\n    \"longitude\": \"76.75787\",\n    \"email\": \"usesr_123@yopmail.com\",\n    \"mobile\": \"9468594658\",\n    \"about_us\": \"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\",\n    \"created_at\": \"2018-05-21 11:14:14\",\n    \"updated_at\": \"2018-05-21 11:20:36\"*         \n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n\t\"message\": \"You are requesting with an invalid credentials.\",\n\t\"code\": 0,\n\t\"status\": 401,\n\t\"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}\nOr\t  \nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"name\",\n      \"message\": \"Name \\\"Test office 1\\\" has already been taken.\"\n    },\n    {\n      \"field\": \"email\",\n      \"message\": \"Email \\\"testuser@yopmail.com\\\" has already been taken.\"\n    }       \n  ],\n  \"status\": 422\n}\nOR\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"name\",\n      \"message\": \"Name cannot be blank.\"\n    },\n    {\n      \"field\": \"mobile\",\n      \"message\": \"Mobile cannot be blank.\"\n    },\n    {\n      \"field\": \"email\",\n      \"message\": \"Email cannot be blank.\"\n    },\n    {\n      \"field\": \"cv_agency_id\",\n      \"message\": \"cv_agency_id cannot be blank.\"\n    },\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AgencyController.php",
    "groupTitle": "Agency_Webhooks"
  },
  {
    "type": "DELETE",
    "url": "api/web/v1/webhooks/agencies/{id}",
    "title": "Delete an Agency",
    "name": "DeleteAgency",
    "version": "0.1.1",
    "group": "Agency_Webhooks",
    "description": "<p>Delete an Agency (Webhook). One must requires login|access token to perform the action</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"Agency has been deleted successfully\",\n  \"data\":{\n    \"status\": \"success\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Invalid credentials\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}\nOr\nHTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"No Record Found\",\n  \"data\":{\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}\nOr\nHTTP/1.1 401 Unauthorized\n{\n  \"message\": \"No Access given to delete all records\",\n  \"data\":{\n    \"status\": \"error\"\n  },\n  \"status\": 401\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AgencyController.php",
    "groupTitle": "Agency_Webhooks"
  },
  {
    "type": "PUT",
    "url": "/api/web/v1/webhooks/agencies/{id}",
    "title": "Update an Agency",
    "name": "UpdateAgency",
    "version": "0.1.1",
    "group": "Agency_Webhooks",
    "description": "<p>To uodate an Agency (Webhook)</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> <li><code>Form-Data must be of raw type</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"cv_agency_id\": \"CV-82AA85\",\n    \"name\": \"New Name\",\n    \"address\": \"Sco 115-118, sector 17 A, chandigarh - 160022\",\n    \"logo\": \"logo_192x168.jpg\",\n    \"latitude\": \"30.72589\",\n    \"longitude\": \"76.75787\",\n    \"email\": \"usesr_testingd@yopmail.com\",\n    \"mobile\": \"9468594658\",\n    \"about_us\": \"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"name\": \"New Name\",\n    \"address\": \"Sco 115-118, sector 17 A, chandigarh - 160022\",\n    \"logo\": \"logo_192x1x68.jpg\",\n    \"latitude\": \"30.72589\",\n    \"longitude\": \"76.75787\",\n    \"email\": \"usesr_123@yopmail.com\",\n    \"mobile\": \"9468594658\",\n    \"about_us\": \"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\",\n    \"created_at\": \"2018-05-21 11:14:14\",\n    \"updated_at\": \"2018-05-21 11:20:36\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n\t\"message\": \"You are requesting with an invalid credentials.\",\n\t\"code\": 0,\n\t\"status\": 401,\n\t\"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}\nOr\t  \nHTTP/1.1 401 Unauthorized\n{\n  \"message\": \"No Access given to update all records\",\n  \"data\":{\n    \"status\": \"error\"\n  },\n  \"status\": 401\n}\nOr\t  \nHTTP/1.1 200 OK\n{\n  \"message\": \"Empty payload data\",\n  \"data\":{\n    \"status\": \"false\"\n  },\n  \"status\": 200\n}\nOr\t  \nHTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"No Record Found\",\n  \"data\":{\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AgencyController.php",
    "groupTitle": "Agency_Webhooks"
  },
  {
    "type": "post",
    "url": "/api/web/v1/webhooks/agents",
    "title": "Add an Agent",
    "name": "AddAgent",
    "version": "0.1.1",
    "group": "AgentWebhooks",
    "description": "<p>To add an Agent (Webhook)</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> <li><code>Form-Data must be of raw type</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"firstName\": \"test_fname\",\n    \"lastName\": \"test_lname\",\n    \"username\": \"test_username\",\n    \"password_hash\": \"generated password in hash format\",\n    \"email\": \"abc@yourdomain.com\",\n    \"dob\": \"1995-05-15\",\n    \"passport_number\": \"UPN_15896542\",\n    \"driver_license_number\": \"DLN-15896/2004-05\",\n    \"companyName\" : \"companyNameasd\",\n    \"about\" : \"We are thought leaders in the franchise industry shaping the processes, the systems and the people that have developed a large number of the leading franchise networks in Australia, many of which we’ve taken to the world.\\r\\n\\r\\nThe foundations of DC Strategy were laid in 1983 with the purpose of helping entrepreneurs from start-ups to medium enterprises to large corporations, grow their businesses through franchising.\",\n    \"preferred_contact_number\" : \"9569635775\",\n    \"licence_number\" : \"CV-13234\",\n    \"awards\" : \"Arwards Description\",\n    \"specialties\" : \"specialties Description\",\n    \"members\" : \"['CIAB']\",\n    \"accreditation\" : \"accreditation Text\",\n    \"languages\" : \"English, French, Spanish, Turkey, Dutch, Russian\"    \n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 405,\n    \"username\": \"test_username\",\n    \"email\": \"abc@yourdomain.com\",\n    \"firstName\": null,\n    \"lastName\": null,\n    \"fullName\": \"\",\n    \"dob\": \"1989-05-25\",\n    \"user_tpe\": 10,\n    \"app_id\": 2,\n    \"status\": 10,\n    \"agency_id\": null,\n    \"passport_number\": \"UPN_15896542\",\n    \"driver_license_number\": \"DLN-15896/2004-05\",\n    \"companyName\": \"companyNameasd\",\n    \"preferred_contact_number\": \"9569635775\",\n    \"about\": \"We are thought leaders in the franchise industry shaping the processes, the systems and the people that have developed a large number of the leading franchise networks in Australia, many of which we’ve taken to the world.\\r\\n\\r\\nThe foundations of DC Strategy were laid in 1983 with the purpose of helping entrepreneurs from start-ups to medium enterprises to large corporations, grow their businesses through franchising.\",\n    \"licence_number\": \"CV-13234\",\n    \"awards\": \"Arwards Description\",\n    \"specialties\": \"specialties Description\",\n    \"members\": \"['CIAB']\",\n    \"accreditation\": \"English, French, Spanish, Turkey, Dutch, Russian\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n\t\"message\": \"You are requesting with an invalid credentials.\",\n\t\"code\": 0,\n\t\"status\": 401,\n\t\"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}\nOr\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"username\",\n      \"message\": \"The username \\\"test_username\\\" has already been taken. The following usernames are available: \\\"test_fname_141, test_lname.929, test_username_786, test_fname.test_lname, test_fnametest_lname\\\". \"\n    },\n    {\n      \"field\": \"email\",\n      \"message\": \"Email abc@yourdomain.com has already been registered. Please enter a valid email address.\"\n    }\n  ],\n \"status\": 422\n}\nOr\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n       \"field\": \"username\",\n       \"message\": \"Username in here!! cannot be blank.\"\n    },\n    {\n       \"field\": \"email\",\n       \"message\": \"Email cannot be blank.\"\n    }\n  ],\n  \"status\": 422\n}\nOr\t  \nHTTP/1.1 200 OK\n{\n  \"message\": \"Empty payload data\",\n  \"data\":{\n    \"status\": \"false\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AgentController.php",
    "groupTitle": "AgentWebhooks"
  },
  {
    "type": "delete",
    "url": "api/web/v1/webhooks/agents/{id}",
    "title": "Delete an Agent",
    "name": "DeleteAgent",
    "version": "0.1.1",
    "group": "AgentWebhooks",
    "description": "<p>Delete an Agent (Webhook)</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"Agent has been deleted successfully\",\n  \"data\":{\n    \"status\": \"success\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response in case of record not found:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"No Record Found\",\n  \"data\":{\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        },
        {
          "title": "Error-Response in case of invalid / expired Access token:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AgentController.php",
    "groupTitle": "AgentWebhooks"
  },
  {
    "type": "put",
    "url": "/api/web/v1/webhooks/agents/{id}",
    "title": "Update an Agent",
    "name": "UpdateAgent",
    "version": "0.1.1",
    "group": "AgentWebhooks",
    "description": "<p>To update an Agent (Webhook)</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> <li><code>Form-Data must be of raw type</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"id\" : 396,\n    \"firstName\": \"test_fname\",\n    \"lastName\": \"test_lname\",\n    \"username\": \"test_username\",\n    \"password_hash\": \"generated password in hash format\",\n    \"email\": \"abc@yourdomain.com\",\n    \"agency_id\": 11,\n    \"dob\": \"1995-05-15\",\n    \"passport_number\": \"UPN_15896542\",\n    \"driver_license_number\": \"DLN-15896/2004-05\",\n    \"companyName\" : \"companyNameasd\",\n    \"about\" : \"We are thought leaders in the franchise industry shaping the processes, the systems and the people that have developed a large number of the leading franchise networks in Australia, many of which we’ve taken to the world.\\r\\n\\r\\nThe foundations of DC Strategy were laid in 1983 with the purpose of helping entrepreneurs from start-ups to medium enterprises to large corporations, grow their businesses through franchising.\",\n    \"preferred_contact_number\" : \"9569635775\",\n    \"licence_number\" : \"CV-13234\",\n    \"awards\" : \"Arwards Description\",\n    \"specialties\" : \"specialties Description\",\n    \"members\" : \"['CIAB']\",\n    \"accreditation\" : \"accreditation Text\",\n    \"languages\" : \"English, French, Spanish, Turkey, Dutch, Russian\"    \n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 396,\n    \"username\": \"test_username\",\n    \"email\": \"abc@yourdomain.com\",\n    \"firstName\": null,\n    \"lastName\": null,\n    \"fullName\": \"\",\n    \"dob\": \"1989-05-25\",\n    \"user_tpe\": 10,\n    \"app_id\": \"2\",\n    \"status\": 10,\n    \"agency_id\": 10,\n    \"passport_number\": \"UPN_15896542\",\n    \"driver_license_number\": \"DLN-15896/2004-05\",\n    \"companyName\": \"companyNameasd\",\n    \"preferred_contact_number\": \"9569635775\",\n    \"about\": \"We are thought leaders in the franchise industry shaping the processes, the systems and the people that have developed a large number of the leading franchise networks in Australia, many of which we’ve taken to the world.\\r\\n\\r\\nThe foundations of DC Strategy were laid in 1983 with the purpose of helping entrepreneurs from start-ups to medium enterprises to large corporations, grow their businesses through franchising.\",\n    \"licence_number\": \"CV-13234\",\n    \"awards\": \"Arwards Description\",\n    \"specialties\": \"specialties Description\",\n    \"members\": \"['CIAB']\",\n    \"accreditation\": \"English, French, Spanish, Turkey, Dutch, Russian\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"id\",\n      \"message\": \"ID cannot be blank.\"\n    },      \n    {\n      \"field\": \"username\",\n      \"message\": \"Username in here!! cannot be blank.\"\n    },      \n  ],\n  \"status\": 422\n}\nOr\nHTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/AgentController.php",
    "groupTitle": "AgentWebhooks"
  },
  {
    "type": "post",
    "url": "/api/web/v1/webhooks/bidders",
    "title": "Add a Bidder",
    "name": "AddBidder",
    "version": "0.1.1",
    "group": "BidderWebhooks",
    "description": "<p>To add a Bidder (Webhook)</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> <li><code>Form-Data must be of raw type</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"firstName\": \"test_fname\",\n    \"lastName\": \"test_lname\",\n    \"username\": \"test_username\",\n    \"password_hash\": \"generated password in hash format\",\n    \"email\": \"abc@yourdomain.com\",\n    \"dob\": \"1995-05-15\",\n    \"passport_number\": \"UPN_15896542\",\n    \"driver_license_number\": \"DLN-15896/2004-05\",\n    \"companyName\" : \"companyNameasd\",\n    \"houseNumber\" : \"#123 A\",\n    \"city\" : \"cityname\",\n    \"state\" : \"stateName\",\n    \"country\" : \"India\",\n    \"postcode\" : 134202,\n    \"officePhone\" : \"0172-42014247\",\n    \"officeFax\" : \"0172-42011547\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 382,\n    \"username\": \"testusername\",\n    \"email\": \"axyz@yourdomain.com\",\n    \"firstName\": \"test_fnameu\",\n    \"lastName\": \"test_lname\",\n    \"fullName\": \"test_fnameu test_lname\",\n    \"dob\": \"1989-05-30\",\n    \"passport_number\": \"UPN_15896542_u\",\n    \"driver_license_number\": \"DLN-15896/2004-05u\",\n    \"companyName\": \"companyName_asd\",\n    \"houseNumber\": \"#123 U\",\n    \"city\": \"cityname_u\",\n    \"state\": \"stateName_u\",\n    \"country\": \"India\",\n    \"postcode\": 134202,\n    \"officePhone\": \"0172-42014200\",\n    \"officeFax\": \"0172-42011500\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"You are requesting with an invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}\nOr     \nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"username\",\n      \"message\": \"The username \\\"test_username\\\" has already been taken. The following usernames are available: \\\"test_fname_141, test_lname.929, test_username_786, test_fname.test_lname, test_fnametest_lname\\\". \"\n    },\n    {\n      \"field\": \"email\",\n      \"message\": \"Email abc@yourdomain.com has already been registered. Please enter a valid email address.\"\n    }\n  ],\n \"status\": 422\n}\nOr\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n       \"field\": \"username\",\n       \"message\": \"Username in here!! cannot be blank.\"\n    },\n    {\n       \"field\": \"email\",\n       \"message\": \"Email cannot be blank.\"\n    }        \n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/BidderController.php",
    "groupTitle": "BidderWebhooks"
  },
  {
    "type": "delete",
    "url": "api/web/v1/webhooks/bidders/{id}",
    "title": "Delete a Bidder",
    "name": "DeleteBidder",
    "version": "0.1.1",
    "group": "BidderWebhooks",
    "description": "<p>To Delete A Bidder (Webhook)</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> <li><code>Form-Data must be of raw type</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"Agent has been deleted successfully\",\n  \"data\":{\n    \"status\": \"success\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response in case of record not found:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"No Record Found\",\n  \"data\":{\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        },
        {
          "title": "Error-Response in case of invalid / expired Access token:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/BidderController.php",
    "groupTitle": "BidderWebhooks"
  },
  {
    "type": "put",
    "url": "/api/web/v1/webhooks/bidders/{id}",
    "title": "Update a Bidder",
    "name": "UpdateBidder",
    "version": "0.1.1",
    "group": "BidderWebhooks",
    "description": "<p>To update a Bidder (Webhook)</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> <li><code>Form-Data must be of raw type</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"id\" : \"Bidder Id\",\n    \"firstName\": \"fname_update\",\n    \"lastName\": \"lname_update\",\n    \"username\": \"test_username\",\n    \"email\": \"abc@yourdomain.com\",\n    \"dob\": \"1995-05-15\",\n    \"passport_number\": \"UPN_15896542_N\",\n    \"driver_license_number\": \"DLN-15896/2004-05_N\",\n    \"companyName\" : \"companyName_u\",\n    \"houseNumber\" : \"#123 A_u\",\n    \"city\" : \"cityname_u\",\n    \"state\" : \"stateName_u\",\n    \"country\" : \"India\",\n    \"postcode\" : 134202,\n    \"officePhone\" : \"0172-42014200\",\n    \"officeFax\" : \"0172-42011200\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 382,\n    \"username\": \"testusername\",\n    \"email\": \"axyz@yourdomain.com\",\n    \"firstName\": \"test_fnameu\",\n    \"lastName\": \"test_lname\",\n    \"fullName\": \"test_fnameu test_lname\",\n    \"dob\": \"1989-05-30\",\n    \"passport_number\": \"UPN_15896542_u\",\n    \"driver_license_number\": \"DLN-15896/2004-05u\",\n    \"companyName\": \"companyName_asd\",\n    \"houseNumber\": \"#123 U\",\n    \"city\": \"cityname_u\",\n    \"state\": \"stateName_u\",\n    \"country\": \"India\",\n    \"postcode\": 134202,\n    \"officePhone\": \"0172-42014200\",\n    \"officeFax\": \"0172-42011500\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 422 Validation Error\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"id\",\n      \"message\": \"ID cannot be blank.\"\n    },\n    {\n      \"field\": \"username\",\n      \"message\": \"Username in here!! cannot be blank.\"\n    }\n  ],\n  \"status\": 422\n}\nOr\nHTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n  \"message\": \"Your request was made with invalid credentials.\",\n  \"code\": 0,\n  \"status\": 401,\n  \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}\nOr\nHTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"No Record Found\",\n  \"data\":{\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/BidderController.php",
    "groupTitle": "BidderWebhooks"
  },
  {
    "type": "post",
    "url": "api/web/v1/webhooks/users/get-auth-key",
    "title": "Get An Auth Key",
    "name": "GetAnAuthKey",
    "version": "0.1.1",
    "group": "CommercialView",
    "description": "<p>To Get An Auth Key</p> <h3>Notes:</h3> <ul> <li><code>Form-Data must be x-www-form-urlencoded</code></li> </ul>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>john.doe</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>password123</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"\",\n  \"data\": {\n    \"username\": \"john.doe\",\n    \"email\": \"john_doe@example.com\",\n    \"auth_key\": \"fW11CiNa4E5IhUmFRcdQttEXdXB8ee6u\"\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"username\",\n      \"message\": \"Username in here!! cannot be blank.\"\n    },\n    {\n      \"field\": \"password\",\n      \"message\": \"Password cannot be blank.\"\n    },\n  ],\n  \"status\": 422\n}\nOr\nHTTP/1.1 422 Unprocessable entity\n{\n  \"message\": \"\",\n  \"data\": [\n    {\n      \"field\": \"password\",\n      \"message\": \"That Pin does not match the registered email address. Please try again or click on Forgot  Pin? to reset it.\"\n    }\n  ],\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/UserController.php",
    "groupTitle": "CommercialView"
  },
  {
    "type": "post",
    "url": "api/web/v1/webhooks/properties",
    "title": "Create a Property",
    "name": "CreateProperty",
    "version": "0.1.1",
    "group": "PropertyWebhooks",
    "description": "<p>To Create Property (Webhook)</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> <li><code>Form-Data must be of raw type</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "RAW Data      \n{\n  \"data\": {\n    \"title\": \"PRIME DEVELOPMENT OPPORTUNITY (STCA)\",\n    \"description\": \"Rare opportunity to acquire this substantial development site, situated in the heart of one of Melbourne's most sought after inner city locations.\",\n    \"price\": 13500000,\n    \"landArea\": 986,\n    \"land_area_unit\": \"squareMeter\",\n    \"floorArea\": 890,\n    \"floor_area_unit\": \"squareMeter\",\n    \"address\": \"516 Church Street, Richmond, VIC 3121\",\n    \"latitude\": \"-37.8280153\",\n    \"longitude\": \"144.9973155\",\n    \"state\": \"6\",\n    \"country\": \"13\",\n    \"media\": null,\n    \"cview_listing_id\": \"CVIEW-5123GHT\",\n    \"video\": \"https://youtu.be/YXSXe0LVFV8\",\n    \"videoImage\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n    \"document\": \"[{\\\"title\\\":\\\"Auction Address\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\\\"},{\\\"title\\\":\\\"Lease Agreement\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\\\"}]\",\n    \"tags\": \"Land Cheap Property\",\n    \"gallery\": [\n      {\n        \"url\": \"https://dduaaywsz-res-5.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1526883272/cxfjy5v0lkutrcuhjemg.jpg\",\n        \"type\": \"image\",\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"url\": \"https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727712/rp57edrrkcuwjqjx51fr.jpg\",\n        \"type\": \"image\",\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"url\": \"https://youtu.be/YXSXe0LVFV8\",\n        \"thumbnail\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n        \"type\": \"video\",\n        \"tbl_name\": \"tbl_properties\"\n      }\n    ]\n  }\n}",
          "type": "json"
        },
        {
          "title": "Success Response:",
          "content": "{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 187,\n    \"title\": \"PRIME DEVELOPMENT OPPORTUNITY (STCA)\",\n    \"description\": \"Rare opportunity to acquire this substantial development site, situated in the heart of one of Melbourne's most sought after inner city locations.\",\n    \"status\": null,\n    \"propertyType\": 0,\n    \"price\": 13500000,\n    \"landArea\": 986,\n    \"land_area_unit\": \"squareMeter\",\n    \"floorArea\": 890,\n    \"floor_area_unit\": \"squareMeter\",\n    \"conditions\": 0,\n    \"address\": \"516 Church Street, Richmond, VIC 3121\",\n    \"latitude\": \"-37.8280153\",\n    \"longitude\": \"144.9973155\",\n    \"unitNumber\": 0,\n    \"streetNumber\": 0,\n    \"streetName\": null,\n    \"suburb\": null,\n    \"city\": null,\n    \"state\": \"6\",\n    \"postcode\": null,\n    \"country\": \"13\",\n    \"parking\": 0,\n    \"parkingList\": null,\n    \"parkingOther\": null,\n    \"floorplan\": null,\n    \"media\": null,\n    \"video\": \"https://youtu.be/YXSXe0LVFV8\",\n    \"videoImage\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n    \"document\": null,\n    \"school\": 0,\n    \"communityCentre\": 0,\n    \"parkLand\": 0,\n    \"publicTransport\": 0,\n    \"shopping\": 0,\n    \"bedrooms\": 0,\n    \"bathrooms\": 0,\n    \"features\": null,\n    \"tags\": null,\n    \"created_by\": null,\n    \"propertyStatus\": 0,\n    \"updated_by\": null,\n    \"created_at\": \"2018-06-08 11:38:34\",\n    \"updated_at\": \"2018-06-08 11:38:34\",\n    \"is_featured\": 0,\n    \"cview_listing_id\": \"CVIEW-5123GHT\",\n    \"is_deleted\": 0,\n    \"business_email\": null,\n    \"website\": null,\n    \"valuation\": null,\n    \"exclusivity\": null,\n    \"tenancy\": null,\n    \"zoning\": null,\n    \"authority\": null,\n    \"sale_tax\": null,\n    \"tender_closing_date\": null,\n    \"current_lease_expiry\": null,\n    \"outgoings\": null,\n    \"lease_price\": null,\n    \"lease_price_show\": 0,\n    \"lease_period\": null,\n    \"lease_tax\": null,\n    \"external_id\": null,\n    \"video_url\": null,\n    \"slug\": null,\n    \"distance\": \"\",\n    \"propertyTypename\": \"\",\n    \"auctionDetails\": null,\n    \"gallery\": [\n      {\n        \"id\": 156,\n        \"tbl_pk\": 187,\n        \"url\": \"https://dduaaywsz-res-5.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1526883272/cxfjy5v0lkutrcuhjemg.jpg\",\n        \"thumbnail\": null,\n        \"type\": \"image\",\n        \"file_name\": null,\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"id\": 157,\n        \"tbl_pk\": 187,\n        \"url\": \"https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727712/rp57edrrkcuwjqjx51fr.jpg\",\n        \"thumbnail\": null,\n        \"type\": \"image\",\n        \"file_name\": null,\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"id\": 158,\n        \"tbl_pk\": 187,\n        \"url\": \"https://youtu.be/YXSXe0LVFV8\",\n        \"thumbnail\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n        \"type\": \"video\",\n        \"file_name\": null,\n        \"tbl_name\": \"tbl_properties\"\n      }\n    ],\n    \"auction\": null,\n    \"saveauction\": null,\n    \"sold\": \"0\",\n    \"calendar\": 0\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "\nHTTP/1.1 401 Unauthorized\n{\n  \"name\": \"Unauthorized\",\n\t\"message\": \"You are requesting with an invalid credentials.\",\n\t\"code\": 0,\n\t\"status\": 401,\n\t\"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/PropertyController.php",
    "groupTitle": "PropertyWebhooks"
  },
  {
    "type": "delete",
    "url": "api/web/v1/webhooks/properties/{id}",
    "title": "Delete a Property",
    "name": "DeleteProperty",
    "version": "0.1.1",
    "group": "PropertyWebhooks",
    "description": "<p>To Delete a property (Webhook)</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"Property has been deleted successfully\",\n  \"data\":  [],\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 422 Unprocessable entity\n    {\n      \"message\": \"No Record Found\",\n      \"data\":  {\n        \"status\": \"error\"\n\t\t },\n      \"status\": 422\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/PropertyController.php",
    "groupTitle": "PropertyWebhooks"
  },
  {
    "type": "put",
    "url": "api/web/v1/webhooks/properties/{id}",
    "title": "Update a Property",
    "name": "UpdateProperty",
    "version": "0.1.1",
    "group": "PropertyWebhooks",
    "description": "<p>To Update Property (Webhook)</p> <h3>Notes:</h3> <ul> <li><code>Login|access token required</code></li> <li><code>Form-Data must be of raw type</code></li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Request Data:",
          "content": "{\n  \"data\": {\n    \"title\": \"PRIME DEVELOPMENT OPPORTUNITY (STCA) Edited\",\n    \"description\": \"Rare opportunity to acquire this substantial development site, situated in the heart of one of Melbourne's most sought after inner city locations.\",\n    \"price\": 13500000,\n    \"landArea\": 986,\n    \"land_area_unit\": \"squareMeter\",\n    \"floorArea\": 890,\n    \"floor_area_unit\": \"squareMeter\",\n    \"address\": \"516 Church Street, Richmond, VIC 3121\",\n    \"latitude\": \"-37.8280153\",\n    \"longitude\": \"144.9973155\",\n    \"state\": \"6\",\n    \"country\": \"13\",\n    \"media\": null,\n    \"cview_listing_id\": \"CVIEW-5123GHT\",\n    \"video\": \"https://youtu.be/YXSXe0LVFV8\",\n    \"videoImage\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n    \"document\": \"[{\\\"title\\\":\\\"Auction Address\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\\\"},{\\\"title\\\":\\\"Lease Agreement\\\",\\\"url\\\":\\\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\\\"}]\",\n    \"tags\": \"Land Cheap Property Added New\",\n    \"gallery\": [\n      {\n        \"url\": \"https://dduaaywsz-res-5.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1526883272/cxfjy5v0lkutrcuhjemg.jpg\",\n        \"type\": \"image\",\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"url\": \"https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727712/rp57edrrkcuwjqjx51fr.jpg\",\n        \"type\": \"image\",\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"url\": \"https://youtu.be/YXSXe0LVFV8\",\n        \"thumbnail\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n        \"type\": \"video\",\n        \"tbl_name\": \"tbl_properties\"\n      }\n    ]\n  }\n}",
          "type": "json"
        },
        {
          "title": "Error Property not found",
          "content": "{\n  \"message\": \"No Record Found\",\n  \"data\": {\n    \"status\": \"error\"\n  },\n  \"status\": 422\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Success Response:",
          "content": "{\n  \"message\": \"\",\n  \"data\": {\n    \"id\": 187,\n    \"title\": \"PRIME DEVELOPMENT OPPORTUNITY (STCA) Edited\",\n    \"description\": \"Rare opportunity to acquire this substantial development site, situated in the heart of one of Melbourne's most sought after inner city locations.\",\n    \"status\": null,\n    \"propertyType\": 0,\n    \"price\": 13500000,\n    \"landArea\": 986,\n    \"land_area_unit\": \"squareMeter\",\n    \"floorArea\": 890,\n    \"floor_area_unit\": \"squareMeter\",\n    \"conditions\": 0,\n    \"address\": \"516 Church Street, Richmond, VIC 3121\",\n    \"latitude\": \"-37.8280153\",\n    \"longitude\": \"144.9973155\",\n    \"unitNumber\": 0,\n    \"streetNumber\": 0,\n    \"streetName\": null,\n    \"suburb\": null,\n    \"city\": null,\n    \"state\": \"6\",\n    \"postcode\": null,\n    \"country\": \"13\",\n    \"parking\": 0,\n    \"parkingList\": null,\n    \"parkingOther\": null,\n    \"floorplan\": null,\n    \"media\": null,\n    \"video\": \"https://youtu.be/YXSXe0LVFV8\",\n    \"videoImage\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n    \"document\": null,\n    \"school\": 0,\n    \"communityCentre\": 0,\n    \"parkLand\": 0,\n    \"publicTransport\": 0,\n    \"shopping\": 0,\n    \"bedrooms\": 0,\n    \"bathrooms\": 0,\n    \"features\": null,\n    \"tags\": null,\n    \"created_by\": null,\n    \"propertyStatus\": 0,\n    \"updated_by\": null,\n    \"created_at\": \"1970-01-01 00:33:38\",\n    \"updated_at\": 1528460700,\n    \"is_featured\": 0,\n    \"cview_listing_id\": \"CVIEW-5123GHT\",\n    \"is_deleted\": 0,\n    \"business_email\": null,\n    \"website\": null,\n    \"valuation\": null,\n    \"exclusivity\": null,\n    \"tenancy\": null,\n    \"zoning\": null,\n    \"authority\": null,\n    \"sale_tax\": null,\n    \"tender_closing_date\": null,\n    \"current_lease_expiry\": null,\n    \"outgoings\": null,\n    \"lease_price\": null,\n    \"lease_price_show\": 0,\n    \"lease_period\": null,\n    \"lease_tax\": null,\n    \"external_id\": null,\n    \"video_url\": null,\n    \"slug\": null,\n    \"distance\": \"\",\n    \"propertyTypename\": \"\",\n    \"auctionDetails\": null,\n    \"gallery\": [\n      {\n        \"id\": 156,\n        \"tbl_pk\": 187,\n        \"url\": \"https://dduaaywsz-res-5.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1526883272/cxfjy5v0lkutrcuhjemg.jpg\",\n        \"thumbnail\": null,\n        \"type\": \"image\",\n        \"file_name\": null,\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"id\": 157,\n        \"tbl_pk\": 187,\n        \"url\": \"https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727712/rp57edrrkcuwjqjx51fr.jpg\",\n        \"thumbnail\": null,\n        \"type\": \"image\",\n        \"file_name\": null,\n        \"tbl_name\": \"tbl_properties\"\n      },\n      {\n        \"id\": 158,\n        \"tbl_pk\": 187,\n        \"url\": \"https://youtu.be/YXSXe0LVFV8\",\n        \"thumbnail\": \"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\",\n        \"type\": \"video\",\n        \"file_name\": null,\n        \"tbl_name\": \"tbl_properties\"\n      }\n    ],\n    \"auction\": null,\n    \"saveauction\": null,\n    \"sold\": \"0\",\n    \"calendar\": 0\n  },\n  \"status\": 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/v1/controllers/webhooks/PropertyController.php",
    "groupTitle": "PropertyWebhooks"
  }
] });
