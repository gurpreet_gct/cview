define({
  "name": "CView",
  "version": "0.1.1",
  "description": "API-Doc for CView Online Auction System",
  "header": {
    "title": "Cview Api-Documentation: Instructions",
    "content": "<h4>Instructions:</h4>\n<pre><code>1. Base Url is `http://cview.civitastech.com/`\n2. Authentication: For requests that require authorization,  \n\t2.1 &quot;auth_key&quot; Bearer Token Authorization\n\t2.2 To get a valid &quot;auth_key&quot;, Search for &quot;Get An Auth Key&quot; in search bar (Left top)\n3. Authorization headers: `Authorization: Bearer XXXXXXXXXXXXXXXXXXXXXX` </code></pre>\n"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-06-08T14:36:04.554Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
