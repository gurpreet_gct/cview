<?php
namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
#use yii\web\Controller;
use api\components\Controller;
use common\models\LoginForm;
use common\models\Usertype;
use yii\filters\VerbFilter;
use yii\authclient\clients\Facebook;
use yii\authclient\OAuthToken;
use linslin\yii2\curl;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
     public $modelClass = 'api\modules\v1\models\Country';   
    public $layout="";
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error','index'],
                        'allow' => true,

                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\rest\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        
        
        
    /*    $this->layout='main';
        $token="CAAVLLjd1VYcBAAVETZCP2fzhqkSeEZBxjrJSuxv4L7Y5SJMDbSJM5ZA4wkKjci3pP0fChPtyhyIzDHyqs00AGoOHf8TWJccqXHL25FqvbSBG5ZAaPTrRZAEcPj0wtceWpIfvIaYLdZAXZALSXeJluEL2I2EOhycZBneJfjL6wKyyUI5NvBxCKGy9ZAav6ZBgDdyZBesAKNttJiU5BuK5ZCsHKa0IZCCGSXhf1KVwZDd";
        
        
          //Init curl
        $curl = new curl\Curl();
        $curl->setOption(CURLOPT_SSL_VERIFYPEER,false);    
        //get http://example.com/
        $response = $curl->get('https://graph.facebook.com/me?access_token=CAAVLLjd1VYcBAAVETZCP2fzhqkSeEZBxjrJSuxv4L7Y5SJMDbSJM5ZA4wkKjci3pP0fChPtyhyIzDHyqs00AGoOHf8TWJccqXHL25FqvbSBG5ZAaPTrRZAEcPj0wtceWpIfvIaYLdZAXZALSXeJluEL2I2EOhycZBneJfjL6wKyyUI5NvBxCKGy9ZAav6ZBgDdyZBesAKNttJiU5BuK5ZCsHKa0IZCCGSXhf1KVwZDd4');
        print_r($response);
        print_r($curl->responseCode);
        die("end");
        $ob=json_decode($response);
        print_r($ob->email);
        die;*/
    /*  $facebook= new Facebook();
       $facebook = new Facebook([
    'accessToken' => $token  
]);
//       $facebook->accessToken=$token;
       echo $facebook->api("me","GET");
       
     print_r($facebook);*/
 
        return $this->render('index');
    }


    
    
}
