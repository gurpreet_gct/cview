    <?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    ?>
    <div class="login-box">
          <div class="login-logo">
            <a href="<?= Yii::$app->homeUrl;?>"> <?= Html::img('@web/images/logo.png',array('alt'=>Yii::$app->name,'title'=>Yii::$app->name));?></b></a>
          </div><!-- /.login-logo -->
          <div class="login-box-body">
            <p class="login-box-msg">Sign in</p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
              <div class="form-group has-feedback">
                <?= $form->field($model, 'username')->textInput(['class'=>'form-control', 'placeholder' => 'Username']);?>
              </div>
              <div class="form-group has-feedback">
                <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control', 'placeholder' => 'Password']) ?>
              </div>
              <div class="row">
                <div class="col-xs-8">    
                  <div class="checkbox icheck">
                    <label>
                      <?= $form->field($model, 'rememberMe')->checkbox() ?>
                    </label>
                  </div>                        
                </div><!-- /.col -->
                <div class="col-xs-4">
                  <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                </div><!-- /.col -->
              </div>
            </form>

   <?php /*   <div class="social-auth-links text-center">
              <p>- OR -</p>
              <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
              <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
            </div><!-- /.social-auth-links -->
           */ ?>

            <?= Html::a('Register', ["/user/register"]) ?> | <?= Html::a('Forgot password', ["/user/forgot"]) ?>

          </div><!-- /.login-box-body -->
          <?php ActiveForm::end(); ?>
                <?php if (Yii::$app->get("authClientCollection", false)): ?>
                            <div class="col-lg-offset-2">
                                <?= yii\authclient\widgets\AuthChoice::widget([
                                    'baseAuthUrl' => ['/user/auth/login']
                                ]) ?>
                            </div>
                        <?php endif; ?>
        </div><!-- /.login-box --> 