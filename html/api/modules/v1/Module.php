<?php

namespace api\modules\v1;
use Yii;
use yii\db\Query;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\v1\controllers';

    public function init()
    {
        parent::init();
       \Yii::$app->user->enableSession = false;
        // custom initialization code goes here
    }
   /* 
    public function afterAction( $action, $result)
    {
		
		if(Yii::$app->user->id)
		{	
			Yii::$app->db->createCommand("insert into tbl_user_access_log set user_id=".Yii::$app->user->id.",lastvisit=unix_timestamp() 
on duplicate key update lastvisit=unix_timestamp();")->execute();
			
			if(in_array(Yii::$app->controller->id."/".Yii::$app->controller->action->id,["deal/index","user/login"]) && $_SERVER['REMOTE_ADDR']!="127.0.0.1")
			{
				try{
						$ip= $_SERVER['REMOTE_ADDR'];
						
						$detail= geoip_record_by_name($ip);    
						$tz= geoip_time_zone_by_country_and_region ($detail['country_code'],$detail['region']);     
						
						$target_time_zone = new \DateTimeZone($tz);
						
						$new_date_time = new \DateTime('now', $target_time_zone); 
						//get the exact GMT format
						$newOffset=$new_date_time->format('P');
						if($newOffset!=Yii::$app->user->identity->timezone_offset)
						{
							Yii::$app->db->createCommand(sprintf("update tbl_users set timezone='%s',timezone_offset='%s' where id=%s",$tz,$newOffset,Yii::$app->user->id))
							->execute();
						}
					}catch(Exception $e){}
			}
			
		}
		
	  return parent::afterAction( $action, $result);
	}
	 */
	
	
}
