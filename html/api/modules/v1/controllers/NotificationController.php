<?php

namespace api\modules\v1\controllers;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use backend\models\Notification;
use common\models\User;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;

class NotificationController extends ActiveController
{
	public $modelClass = 'backend\models\Notification';
	public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view'],$actions['custom']);

		return $actions;
	}
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	
	/*
	 * To get the all notifications of a specific user. 
	 * */
	public function actionIndex()
	{
		  return new ActiveDataProvider([ 
            'query' => Notification::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere('created_at >=unix_timestamp(curdate() - INTERVAL 15 day)')	
        ]);		 
	}
	public function actionIsdelete($id)
	{	
		$user_id = Yii::$app->user->id;
		$model=Notification::find()->where(['user_id' =>$user_id,'id' => $id])->one();
		if($model)
		{
			 $model->is_delete=1;
			 $model->save(false);
		}
	}
	
	public function actionRead($id)
	{
		$user_id = Yii::$app->user->id;
		$model=Notification::find()->where(['user_id' =>$user_id,'id' => $id])->one();
		
		if($model)
		{
			 $model->is_read=1;
			 $model->save(false);
		}
	}
}
