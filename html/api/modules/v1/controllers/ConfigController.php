<?php
namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use common\models\CviewConfig;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class ConfigController extends Controller
{
    public $modelClass = 'common\models\CviewConfig';   
    
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		/*$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];*/
		return $behaviors;
	}
	   
    
	public function actions()
	{
		$actions = parent::actions();
		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index']);    

		return $actions;
	}
 
  	
    /** 
     * Create a new config 
     */    
    public function actionCreate(){
		$model= new CviewConfig();			
		$model->load(Yii::$app->getRequest()->getBodyParams(), ''); 
		
		if($model->validate()){
			/* save data */
			$model->save();
		}

		return $model;
    }
    
    /* update address by address id */
    public function actionUpdate($id){		
        $user_id=Yii::$app->user->id;
        $model= CviewConfig::findOne(['id' => $id]);
				
		$model->load(Yii::$app->getRequest()->getBodyParams(), '');  
	 	if($model->validate()){
			$model->save();		
		}
		return $model;       
    }

    public function actionAll()
    {	
    	$configs = CviewConfig::all();
    	return $configs;
    }
	
}
