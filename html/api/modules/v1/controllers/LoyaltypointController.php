<?php
namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use common\models\Loyaltypoint;
use common\models\Loyalty;
use common\models\UserLoyalty;
use common\models\Profile;
use common\models\Workorders;
use common\models\Pass;
use common\models\Loyaltycard;
use common\models\User;
use common\models\Order;
use common\models\OrderItems;
use common\models\Transaction;
//use common\models\Cardtype;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
error_reporting(E_ALL & ~E_NOTICE);
class LoyaltypointController extends Controller
{
        public $modelClass = 'common\models\Loyalty';

        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}


  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['view']);

		return $actions;
	}


	/********* add loyalty point of user**************/

	public function actionCreate(){

	$user_id=Yii::$app->user->id;
    $model= new Loyaltypoint();

  //  $model->scenario ='create';
	$model->load(Yii::$app->getRequest()->getBodyParams(), '');
    $model->user_id= $user_id;
    if($model->validate())
    {
		$checkUserbday = User::findOne(['id'=>Yii::$app->user->id]);
		$checkStorebday = User::findOne(['id'=>$model->store_id]);

		$loyalty= Loyalty:: find()->select(
								[Loyalty::tableName().'.*'])
								->where([Loyalty::tableName().'.user_id' => $model->store_id,
								# Loyalty::tableName().'.loyalty_pin' => $model->pin,
								'status'=>1])
								->orderBy([Loyalty::tableName().'.id' => SORT_DESC])->asArray()->one();


			$checkEmployee = User::findOne(['advertister_id' =>$model->store_id, 'loyaltyPin' => $model->pin]);

			if(($checkEmployee != '') && ($loyalty != '') ){
				$loyaltypoints = 0;

				$totalSpend = $this->calculateSpend($model->store_id);
				if($totalSpend=='ok')
				{

					if(($checkUserbday->bdayOffer==1) && ($checkStorebday->bdayOffer==1) ){
						$loyaltypoints = $loyalty['pointvalue']* $model->amount*2;
						$model->method = 3;
					}else{
						$loyaltypoints = $loyalty['pointvalue']* $model->amount;
						$model->method = 2;
					}
					$model->valueOnDay = $loyalty['dolor']/$loyalty['loyalty'];
					$model->rValueOnDay = $loyalty['redeem_pointvalue'];
					$model->order_id = 0;
					$model->loyaltypoints = $loyaltypoints;
				}else{
					$model->valueOnDay = 0;
					$model->rValueOnDay = 0;
					$model->order_id = 0;
					$model->loyaltypoints = $loyaltypoints;
				}

			$model->status = 1;
			$model->save();
			if($loyaltypoints!=0){
					return ["message"=>$loyaltypoints.' loyalty points have been added to your account.'];
				}else{
					return ["message"=>'Your pass has been redeemed.'];
				}
			}else{

				 return ["message"=>"That PIN does not match your registered 6-digit PIN. Please try again.","statusCode"=>422];
			}
	}else{

		 $errors = $model->errors;

		 if( array_key_exists('transaction_id',$errors))
		 {
			return ["message"=>$errors['transaction_id'][0],$model,"statusCode"=>422];
		 }
		 else
		 return $model;

		}
	}
	// check minimum spend by user on this store for get points
	public function calculateSpend($storeId){
			$user_id=Yii::$app->user->id;
				$lo = Loyaltypoint::tableName();
				$pro = Profile::tableName();
				$ord = OrderItems::tableName();
				$offline = Loyaltypoint::find()
									->select(["sum($lo.amount) as totalSpend"])
									->where("$lo.store_id=".$storeId)
									->andWhere("$lo.user_id=".$user_id)
									->asArray()
									->one();
				$online = OrderItems::find()
									->select(["sum($ord.payable) as total"])
									->where("$ord.store_id=".$storeId)
									->andWhere("$ord.user_id=".$user_id)
									->asArray()
									->one();
				$minimum = Profile::find()
									->select(["$pro.minimumSpend"])
									->where("$pro.user_id=".$storeId)
									->asArray()
									->one();
				$offline1 = $offline['totalSpend']!=''?$offline['totalSpend']:0;
				$online1  = $online['total']!=''?$online['total']:0;
				$totalSpend = $offline1+$online1;
				if($totalSpend >= $minimum['minimumSpend']){
					return 'ok';
				}else{
					return 'no';
				}

	}

	/*********  end here***************************************/
	public function actionStorelist(){
	$user_id=Yii::$app->user->id;
	$model= new User();
	$model= User:: find()->select(
								[User::tableName().'.orgName',User::tableName().'.id'])
							     ->where([User::tableName().'.user_type' => 7,User::tableName().'.status' => 10])
								->orderBy([User::tableName().'.id' => SORT_DESC])->asArray()->all();
	return $model;
	 }

	public function actionUserloyaltypoint(){

	$user_id=Yii::$app->user->id;
	if(isset(Yii::$app->request->post()['workorderid']) && Yii::$app->request->post()['workorderid']!='' && isset(Yii::$app->request->post()['pin']) && Yii::$app->request->post()['pin']!=''){
		$model= new Workorders();
		$loyalty= new Loyalty();
		$Loyaltypoint= new Loyaltypoint();
	   $model= Workorders:: find()->select(
								[Workorders::tableName().'.advertiserID',Loyalty::tableName().'.loyalty',Loyalty::tableName().'.dolor',Loyalty::tableName().'.pointvalue',Loyalty::tableName().'.user_id',Loyalty::tableName().'.loyalty_pin'])
								->leftJoin(Loyalty::tableName(), Loyalty::tableName().'.user_id = '.Workorders::tableName(). '.advertiserID')
								->where([Workorders::tableName().'.id' => Yii::$app->request->post()['workorderid'],Loyalty::tableName().'.loyalty_pin' => Yii::$app->request->post()['pin'],Loyalty::tableName().'.status' =>1])
								->orderBy([Workorders::tableName().'.id' => SORT_DESC])->asArray()->one();

	                   if(count($model) > 0){
					    $Loyaltypoint->updated_at= time();
						$Loyaltypoint->created_at= time();
						$Loyaltypoint->valueOnDay = $model['pointvalue'];
						$Loyaltypoint->order_id = 0;
						$Loyaltypoint->loyaltypoints = $model['pointvalue'];
						$Loyaltypoint->user_id = $user_id;
						$Loyaltypoint->store_id = $model['user_id'];
						$Loyaltypoint->method = 1;
						$Loyaltypoint->status = 1;
						$Loyaltypoint->created_by = $user_id;
						$Loyaltypoint->updated_by = $user_id;
						$Loyaltypoint->save();
						return ["message"=>"Your loyalty points have been added."];
					    }else{
							return ["message"=>"Please enter the correct data to add the loyalty points for the store.","statusCode"=>422];
						}
	    }else{
							return ["message"=>"Please enter the correct data to add the loyalty points for the store.","statusCode"=>422];
						}
	 }


  	 /* To get all Loyaltypoint*/

  	 public function actionIndex2(){

	 		$UserLoyalty =  UserLoyalty:: find()
											->joinWith("profile")
											->where([UserLoyalty::tableName().'.user_id' => Yii::$app->user->id])
											->andWhere(UserLoyalty::tableName().'.value is not null')
											->asArray()->all();

			$Loyaltycard =  Loyaltycard:: find()
											->where([Loyaltycard::tableName().'.user_id' => Yii::$app->user->id])
										->asArray()->all();
			$arr =array_merge($UserLoyalty,$Loyaltycard);
			$arr_result = array();

			foreach($arr as $key=>$modelinfo){

			$result = array();
			$result['user_id'] = (!empty($modelinfo['user_id']))?intval($modelinfo['user_id']):0;
			$result['store_id']= (!empty($modelinfo['store_id']))?intval($modelinfo['store_id']):'';
			$result['loyaltypoints'] = (!empty($modelinfo['points']))?floatval($modelinfo['points']):'';
			$result['loyaltyvalue']= (!empty($modelinfo['value']))?floatval($modelinfo['value']):'';

			$result['loyaltyID'] = (!empty($modelinfo['loyaltyID']))?Yii::$app->urlManagerApi->createAbsoluteUrl("/v1/barcodes?id=".$modelinfo['loyaltyID']):'';
			$result['brandName'] = (!empty($modelinfo['profile']['brandName']))?$modelinfo['profile']['brandName']:'';
			$result['brandLogo'] = (!empty($modelinfo['profile']['brandLogo']))?Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$modelinfo['profile']['brandLogo']):'';
			$result['backgroundHexColour'] = (!empty($modelinfo['profile']['backgroundHexColour']))?$modelinfo['profile']['backgroundHexColour']:'';

			$result['foregroundHexColour'] = (!empty($modelinfo['profile']['foregroundHexColour']))?$modelinfo['profile']['foregroundHexColour']:'';
			$result['id']= (!empty($modelinfo['id']))?$modelinfo['id']:'';
			$result['loyaltycardbrand']= (!empty($modelinfo['brand']))?$modelinfo['brand']:'';
			$result['nameOnCard']= (!empty($modelinfo['nameOnCard']))?$modelinfo['nameOnCard']:'';
			$result['cardNumber']= (!empty($modelinfo['cardNumber']))?$modelinfo['cardNumber']:'';
			$result['expiry'] = (!empty($modelinfo['expiry']))?date("Y-m",$modelinfo['expiry']):'';
			$result['nameOnCard'] = (!empty($modelinfo['nameOnCard']))?$modelinfo['nameOnCard']:'';

			$result['loyaltystatus'] = (!empty($modelinfo['loyaltystatus']))?intval($modelinfo['loyaltystatus']):1;
			$result['data'] = (!empty($modelinfo['data']))?Yii::$app->urlManagerLoyaltyKit->createAbsoluteUrl($modelinfo['data']):'';
			$result['image'] = (!empty($modelinfo['image']))?Yii::$app->urlManagerLoyaltyCardFrontEnd->createAbsoluteUrl("loyaltycardImage/".$modelinfo['image']):'';
			$arr_result[] = $result;

			}
		return $arr_result;
	}



   public function actionIndex(){

	   $loyalty= Loyaltycard::find()
	   ->select(["id","user_id",new Expression("0 as store_id"),new Expression("0 as value"),new Expression("0 as points"),
	   new Expression("brand as brandName"),
	   'nameOnCard','cardNumber','image',new Expression('date_format(from_unixtime(expiry),"%Y-%m")'),'data','type','loyaltystatus'
	   ])
	   ->where(["user_id"=>yii::$app->user->id]);
	/* show pending loyalty points */
	$online = Loyaltypoint::find()
									->select([
										 'id','user_id','store_id',
										new Expression("0 as value"),
										'sum(loyaltypoints) as points',
										new Expression("'' as brandName"),
										new Expression("'' as nameOnCard"),
										new Expression("'' as cardNumber"),
										new Expression("'' as image"),
										new Expression("0 as expiry"),
										new Expression("0 as data"),
										new Expression("'-1' as type"),
										new Expression("'pending' as loyaltystatus"),
									])
									->where('status=2')
									->andWhere('user_id='.Yii::$app->user->id);
	/* show pending loyalty points end */
	return   UserLoyalty::find()
	   ->select(["loyaltyID","user_id","store_id","value","points",new Expression("'' as brandName"),
	   new Expression("'' as nameOnCard"),
	   new Expression("'' as cardNumber"),
	   new Expression("'' as image"),
	   new Expression("0 as expiry"),
	   new Expression("0 as data"),
	   new Expression("'-1' as type"),
	   new Expression("'-1' as loyaltystatus"),
	   ])
	   ->where(["user_id"=>yii::$app->user->id])
	   ->union($loyalty)
	  // ->union($online)
	   ->all();
   }

  	 /* To get all Loyaltypoint*/

  	 public function actionView($id){
		$UserLoyalty=  UserLoyalty:: find()->select(
					[UserLoyalty::tableName().'.loyaltyID',UserLoyalty::tableName().'.user_id',UserLoyalty::tableName().'.store_id','ifnull(value,0) as value', UserLoyalty::tableName().'.points'])
					->joinWith("profile")
					->where([UserLoyalty::tableName().'.user_id' => Yii::$app->user->id])
					->andWhere([UserLoyalty::tableName().'.store_id' => $id])
					->one();



		$workorder= Workorders::find()
					->select([Workorders::tableName().'.id'])
					->join("LEFT JOIN",Pass::tableName(),
					Pass::tableName().'.workorder_id='.Workorders::tableName().'.id and '.Pass::tableName().'.user_id='.Yii::$app->user->id)
					->Where(['workorderpartner'=>$id])
					->andWhere(sprintf('dateFrom <= %s and dateTo > %s and noPasses > usedPasses ' ,time(date("Y-m-d")),time(date("Y-m-d"))+86400))
					->andWhere(['status' => 1, 'type' => 2])
					->andWhere(Pass::tableName().".id is null")
					->orderBy(Workorders::tableName().'.id')
					->one();
		/* show pending loyalty points */
		$online = Loyaltypoint::find()
									->select([
											'sum(loyaltypoints) as pendingLoyalty'
										 	])
									->where('status=2')
									->andWhere('user_id='.Yii::$app->user->id)
									->andWhere('store_id='.$id)
									->asArray()
									->one();
	/* show pending loyalty points end */

		if($UserLoyalty){

			//return [$UserLoyalty,"workorder_id"=>$workorder->id];
			return ["message"=>"","statusCode"=>200,"loyaltypoints"=>$UserLoyalty->points,"workorder_id"=>$workorder->id,
			"pendingPoints"=>$online['pendingLoyalty'],
			"loyaltyID"=>Yii::$app->urlManagerApi->createAbsoluteUrl("/v1/barcodes?id=".$UserLoyalty->loyaltyID)];
		}
		else
		{
			return ["message"=>"loyalty points vlaue is zero","statusCode"=>200,"loyaltypoints"=>0,"workorder_id"=>$workorder->id,
			"pendingPoints"=>$online['pendingLoyalty'],
			"loyaltyID"=>Yii::$app->urlManagerApi->createAbsoluteUrl("/v1/barcodes?id=".Yii::$app->user->id. "-". $id)];
		}
	}






}
