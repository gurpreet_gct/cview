<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\ApplyBid;
use common\models\User;
use api\modules\v1\models\AuctionEvent;
use common\models\Property;
use api\modules\v1\models\Customer;
use api\modules\v1\models\ApplyBidSearch;
use api\components\Controller;

/**
 * AuctionController
 *
 * Save|Update Auction management methods for APIs are defined here.
 */

class AuctionController extends Controller
{
    public $modelClass = 'api\modules\v1\models\ApplyBid';
    

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'except' => ['subfees'],
            'only'=>[
                'saveauction',
                'search',
                'unfavourite',
                'rating',
                'add-to-calendar'
            ],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "update" actions
        unset($actions['delete'], $actions['index'], $actions['update'],$actions['create']);

        return $actions;
    }
	
    /**
    * To favourite or unfavourite an auction
    *
    * @api {post} /api/web/v1/auction/saveauction To favourite or unfavourite an auction
    * @apiName Saveauction
    * @apiVersion 0.1.1
    * @apiGroup Auction
    * @apiDescription To save auction, Form-Data must be x-www-form-urlencoded
    *
    * @apiParam {Number} listing_id Listing Id (Auction Id)
    * @apiParam {Number} is_favourite Used For Favorite Listing, 1 => set as favourite, 0 => unfavourite 
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "message": "",
    *       "data": {
    *          "id": 3
    *          "user_id": "1",
    *          "auction_id": "1",
    *          "is_favourite": "1",
    *          "status": 1,
    *          "rating": null,
    *          "user_name": "john_doe"
    *       },
    *       "status": 200
    *     }
    *
    * @apiError Validation Error Make sure to use submit correct data to save an auction
    *
    *
    * @apiErrorExample Error-Response:
    *     HTTP/1.1 422 Validation Error
    *     {
    *       "message": "",
    *       "data": [
    *         {
    *           "field": "auction_id",
    *           "message": "Auction Id must be an integer."
    *         },
    *         {
    *           "field": "is_favourite",
    *           "message": "Is Favourite must be an integer."
    *         }
    *       ],
    *       "status": 422
    *     }
    * 
    * @return Object|User-model
    */
	public function actionSaveauction()
    {   
        $authToken =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        $user = User::findIdentityByAccessToken($authToken);
        if (!$user || empty($authToken)) {
            return ['Invalid request, Login first'];
        }

        $formData = Yii::$app->getRequest()->getBodyParams();
        $formData['user_id'] = $user->id;
        
        if (empty($formData['auction_id'])) {
            return ['Required auction_id'];
        }

        /**
         * This is a temp fix.
         * Once the listing is shown relative to auction and not PROPERTY, this shall be adjusted
         */
        $property_id_temp_fix = AuctionEvent::findOne($formData['auction_id']);
        $formData['auction_id'] = $property_id_temp_fix->listing_id;

        $model = ApplyBid::findByUserAuction($formData);
        if ($model == null) {
            $model = new ApplyBid();
            $model->load($formData, '');
            if($model->validate()) {
                $model->save();
            }
        } else {
            $model->load($formData, '');
            $model->save();
        }

        return $model;
    }
    
    /**
    * To list of Save Auction
    * 
    * @api {get} /api/web/v1/auction/search List of Save Auction
    * @apiName ListofSaveAuction
    * @apiVersion 0.1.1
    * @apiGroup Auction
    * @apiDescription To search or sort for saved Auction
    * 
    * ### Supported keys for sorting:
    * - *price* | ascending=>'price', 'descending' => '-price' 
    * - *date* | ascending=>'date', 'descending' => '-date' 
    * - *distance* | ascending=>'distance', 'descending' => '-distance' 
    * ### `Note: Append (-) for sorting in descending order` 
    *
    *   ### Examples for reference:
    *   #### `/api/web/v1/auction/search?sort=price,-date`
    *   #### `/api/web/v1/auction/search?property_type=Commercial Farming, Land/Development` | `property_type=Commercial Farming`
    *   #### `/api/web/v1/auction/search?from_date=2018-04-01` | `/api/web/v1/property/search?to_date=2018-04-20` | `/api/web/v1/property/search?from_date=2018-04-01&to_date=2018-04-20`
    *   #### `/api/web/v1/auction/search?sale_from=2000` | `/api/web/v1/property/search?sale_to=5000` | `/api/web/v1/property/search?sale_from=2000&sale_to=5000`
    *   #### `/api/web/v1/auction/search?min_land=244` | `/api/web/v1/property/search?max_land=1200` | `/api/web/v1/property/search?min_land=230&max_land=2200`
    *   #### `/api/web/v1/auction/search?min_floor=244` | `/api/web/v1/property/search?max_floor=1200` | `/api/web/v1/property/search?min_floor=230&max_floor=2200`
    *   #### `/api/web/v1/auction/search?auction_only=1` To display only auctions
    *   #### `/api/web/v1/auction/search?auction_all=1` (Optional) To display all auctions
    *
    *   #### `http://cview.civitastech.com/api/web/v1/auction/search?suburb=&property_type=Commercial Farming, Land/Development, Hotel/Leisure, Offices, Showrooms/Bulky Good&from_date=2018-04-01&to_date=2018-04-20&sale_from=2000&sale_to=5000&min_land=500&max_land=1000&min_floor=200&max_floor=500&auction_only=1&auction_all=0`
    *  #### For location based searching: `http://cview.civitastech.com/api/web/v1/auction/search?lat=30.741482&long=76.768066&sort=distance`, and for descending `http://cview.civitastech.com/api/web/v1/auction/search?lat=30.741482&long=76.768066&sort=distance` 
    *
    *  ## Response 
    *  - Key => Value
    *  - *data.items.propertyType* => PropertyType Id (Represents types like Commercial Farming, Land/Development, Hotel/Leisure, Industrial/Warehouse, etc.)
    *  - *data.items.state* => The corresponding state id from the saved states in DB. Refer to GeoState for getting the state id and names
    *  - *data.items.country* => The corresponding country id from the saved countries in DB. Refer to GeoCountry for getting the state id and names
    *  - *data.items.auctionDetails.live* => True if the auction is currently live
    *  - *data.items.auctionDetails.past* => True if the auction has taken place already.
    *  - *data.items.auctionDetails.upcoming* => True is the auction will take place in the future.
    *  - *data.items.auctionDetails.seconds* => Integer representing number of seconds left for the auction to start, 0 incase of the auction has already taken place or is live right now.
    *  - *data.items.auction.datetime_start* => The auction start date
    *  - *data.items.auction.auction_status* => Integer representing the type of auction. i.e. 1 => auction, 2 => Streaming, 3 => Offline
    *  - *data.items.saveauction* => Will consist a list of users that have saved that auction.
    *
    * @apiParam {String} suburb To Filter by city/state/suburb/postcode
    * @apiParam {String} property_type Comma separated set of property types
    * @apiParam {String} sale_from Min Cost of property
    * @apiParam {String} sale_to Max Cost of property
    * @apiParam {String} from_date Auction Start date, format YYYY-M-D eg. 2018-01-23
    * @apiParam {String} to_date Auction End date, format YYYY-M-D eg. 2018-01-23
    * @apiParam {String} min_land eg. 2032
    * @apiParam {String} max_land eg. 23321
    * @apiParam {String} min_floor eg. 323
    * @apiParam {String} max_floor eg. 2938 
    * @apiParam {String} auction_only 1=>Display only auction types
    * @apiParam {String} auction_all 1=> Display all auction types i.e. auction|streaming|offline
    * @apiParam {String} list 1=>Past Property, 2=>Upcoming Property
    * @apiParam {String} status 1- For Sale, 2- For Rent. 3 - For Lease
    * @apiParam {String} sort (Optional) Comma separated list of sort keys, append (-) for descending order sort.
    * @apiParam {String} lat User's lat, or lat to be used for location filtering
    * @apiParam {String} long User's long, or long to be used for location filtering 
    *
    * 
    * @apiSuccessExample Success-Response:
    *    HTTP/1.1 200 
    *    {
    *        "message": "",
    *        "data": {
    *        "items": [
    *          {
    *           "id": 1,
    *           "title": "Brand New Office/Warehouse Development",
    *           "description": "Cameron is pleased to offer this brand new Office/ Warehouse complex that consists of 23 units ranging between 240sqm to 609sqm. Offering easy access to major arterial roads including Frankston Dandenong Road, Abbotts Road, South Gippsland Freeway and Eastlink via Fox Drive and Discovery Road.\n\nEdison Park, being Dandnenong South's newest Industrial Park development; is situated on the corner of Taylors Road and Edison Road and will provide easy dual drive-thru access. Construction is expected to commence in June 2018.\n\nEach warehouse include the following:\n- Office with carpet tiles, air-conditioning, power points, lighting and load-bearing ceiling\n- Kitchenette\n- Toilet amenities with shower\n- Container height roller shutter door\n- 3 Phase power",
    *           "status": null,
    *           "propertyType": 4,
    *           "price": 480000,
    *           "landArea": null,
    *           "floorArea": 742,
    *           "conditions": 0,
    *           "address": "4/18 Luisa Avenue, DANDENONG VIC, 3175 ",
    *           "latitude": "37.9810",
    *           "longitude": "145.2150",
    *           "unitNumber": 0,
    *           "streetNumber": 0,
    *           "streetName": null,
    *           "suburb": null,
    *           "city": "Mohali",
    *           "state": "32",
    *           "postcode": "123456",
    *           "country": "13",
    *           "parking": 1,
    *           "parkingList": null,
    *           "parkingOther": null,
    *           "floorplan": null,
    *           "media": null,
    *           "video": null,
    *           "school": 1,
    *           "communityCentre": 1,
    *           "parkLand": 1,
    *           "publicTransport": 0,
    *           "shopping": 1,
    *           "bedrooms": 3,
    *           "bathrooms": 4,
    *           "features": null,
    *           "tags": null,
    *           "estate_agent_id": null,
    *           "second_agent_id": null,
    *           "agency_id": null,
    *           "created_by": null,
    *           "propertyStatus": 0,
    *           "updated_by": null,
    *           "created_at": "2018-04-18 14:02:04",
    *           "updated_at": "2018-04-18 14:02:04",
    *           "inspection1": null,
    *           "inspection2": null,
    *           "inspection3": null,
    *           "inspection4": null,
    *           "inspection5": null,
    *           "inspection6": null,
    *           "inspection7": null,
    *           "inspection8": null,
    *           "is_featured": 0,
    *           "distance": "",
    *           "propertyTypename": "Industrial/Warehouse",
    *           "auctionDetails": 
    *           {
    *                "live": false,
    *                "past": true,
    *                "upcoming": false,
    *                "seconds": 0
    *           },
    *           "gallery": [],
    *           "auction": {
    *                "id": 1,
    *                "listing_id": 1,
    *                "auction_type": null,
    *                "date": null,
    *                "time": null,
    *                "datetime_start": "2018-04-14 05:25:31",
    *                "datetime_expire": 1523769931,
    *                "starting_price": null,
    *                "home_phone_number": null,
    *                "estate_agent": null,
    *                "picture_of_listing": null,
    *                "duration": null,
    *                "real_estate_active_himself": null,
    *                "real_estate_active_bidders": null,
    *                "reference_relevant_electronic": null,
    *                "reference_relevant_bidder": null,
    *                "auction_status": 1,
    *                "streaming_url": null,
    *                "streaming_status": null,
    *                "templateId": "",
    *                "envelopeId": "",
    *                "documentId": "",
    *                "document": "",
    *                "recipientId": "",
    *                "bidding_status": 2
    *           },
    *           "agent": null,
    *           "agency": null,
    *           "saveauction": {
    *                "id": 33,
    *                "user_id": 351,
    *                "listing_id": 1,
    *                "is_favourite": 1,
    *                "status": 0,
    *                "rating": null,
    *                "user_name": "gurpreet.gct"
    *            }
    *            },
    *        ],
    *        "_links": {
    *            "self": {
    *                "href": "http://cview.civitastech.com/api/web/v1/properties/search?list=1&auction_only=1&page=1"
    *            }
    *        },
    *        "_meta": {
    *            "totalCount": 7,
    *            "pageCount": 1,
    *            "currentPage": 1,
    *            "perPage": 20
    *        }
    *    },
    *    "status": 200
    *}
    * */
    public function actionSearch(){
		$auctionsearchModel = new ApplyBidSearch();
		$searchByAttr['ApplyBidSearch'] = Yii::$app->request->queryParams;
          return $auctionsearchModel->search($searchByAttr);
	}
	
	
    /**
    * To Unfavourite property
    * 
    * @ api {post} /api/web/v1/auction/unfavourite?listing_id=2 Un Favourite Auction
    * @ apiName UnfavouriteAuction
    * @ apiVersion 0.1.1
    * @ apiGroup Auction
    * @ apiDescription To Un Favourite Auction
    * 
    * @ apiParam {Number} user_id User's Id(login user id).
    * @ apiParam {Number} listing_id Listing Id (Property Id)
    * @ apiParam {Number} is_favourite Used For un favourite to property(i.e 0)
    * 
    * @apiSuccessExample Success-Response:
    *  HTTP/1.1 200 
    *   {
    *       "message": "",
    *        "data": {
    *              "user_id": "351",
    *              "listing_id": "2",
    *              "is_favourite": 0,
    *              "id": 35,
    *              "user_name": "gurpreet.gct"
    *         },
    *    "status": 200
    *    }
    */
	public function actionUnfavourite($listing_id){
		if(Yii::$app->user->id)
		if(!empty($listing_id)){
			$find = ApplyBid::findOne(['listing_id'=>$listing_id,'user_id'=>Yii::$app->user->id,'status'=>1]);
            if(!empty($create)){
			$find->scenario = 'create';
            $find->load(Yii::$app->request->getBodyParams(),'');
			$find->is_favourite = 0;
			$find->save(false);
            }else{
                $find = new ApplyBid();
                $find->scenario = 'create';
                $find->load(Yii::$app->request->getBodyParams(),'');
                if($find->validate()){
                    $find->is_favourite = 0;
                    $find->save(false);
                }
            }
			return $find;
		}
	}
	
	/**
     * To Rating for property
     *
     * @api {post} /api/web/v1/auction/rating Rating
     * @apiName Rating
     * @apiVersion 0.1.1
     * @apiGroup Rating
     * @apiDescription To Rating, Form-Data must be x-www-form-urlencoded
     *
     * @apiParam {Number} user_id User's Id.
     * @apiParam {Number} auction_id Auction Id (Auction Id)
     * @apiParam {Number} rating Used For rate to property(in between 1 to 5)
     *
     *
     *
     * @apiSuccessExample Success-Response:
     *    HTTP/1.1 200 OK
     *    {
     *        "message": "",
     *        "data": {
     *            "auction_id": "1",
     *            "is_favourite": "0",
     *            "rating": "1",
     *            "user_id": "351",
     *            "id": 16,
     *            "user_name": "gurpreet.gct"
     *        },
     *        "status": 200
     *    }
     *
     * @apiError Validation Error Make sure to use submit correct data to save an rating
     *
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Validation Error
     *     {
     *       "message": "",
     *       "data": [
     *         {
     *           "field": "rating",
     *           "message": "Must be one star"
     *         },
     *         {
     *           "field": "user_id",
        "message": "The user with user_id \"351\" does not exist"
     *         },
     *         {
     *            "field": "auction_id",
        "message": "The auction with auction \"1\" and user_id \"351\" already saved"
     *         }
     *       ],
     *       "status": 422
     *     }
     * 
     * @return Object|User-model
     */	
	public function actionRating(){
		$data = Yii::$app->request->post();
        $auction = ApplyBid::findByUserAuction($data);
        if(!empty($auction)){
            $auction = ApplyBid::findOne($auction->id);
			$auction->scenario = 'update-ratings';
			$auction->load($data,'');
			if ($auction->validate()) {
			    $auction->save(false);
		    }
            return $auction;
		}else{
            $auction = new ApplyBid();
            $auction->scenario      = 'ratings';
            $auction->load($data,'');
			if($auction->validate()){
				if($auction->save(false))
				return $auction;
			}else{
				return $auction;
			}
		}
	}
	
	/**
     * To add a property to calendar
     *
     * @api {post} /api/web/v1/auction/add-to-calendar Rating
     * @apiName AddToCalendar
     * @apiVersion 0.1.1
     * @apiGroup Auction
     * @apiDescription Add a listing to calendar, Form-Data must be x-www-form-urlencoded
     *
     * @apiParam {Number} user_id User's Id.
     * @apiParam {Number} auction_id Auction Id (Auction Id)
     * @apiParam {Number} rating Used For rate to property(in between 1 to 5)
     *
     * @apiSuccessExample Success-Response:
     *    HTTP/1.1 200 OK
     *    {
     *        "message": "",
     *        "data": {
     *            "auction_id": "1",
     *            "is_favourite": "0",
     *            "rating": "1",
     *            "user_id": "351",
     *            "id": 16,
     *            "user_name": "gurpreet.gct"
     *        },
     *        "status": 200
     *    }
     *
     * @apiError Validation Error Make sure to use submit correct data to save an rating
     *
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Validation Error
     *     {
     *       "message": "",
     *       "data": [
     *         {
     *           "field": "rating",
     *           "message": "Must be one star"
     *         },
     *         {
     *           "field": "user_id",
        "message": "The user with user_id \"351\" does not exist"
     *         },
     *         {
     *            "field": "auction_id",
        "message": "The auction with auction \"1\" and user_id \"351\" already saved"
     *         }
     *       ],
     *       "status": 422
     *     }
     * 
     * @return Object|User-model
     */
	public function actionAddToCalendar(){
		
        // listing_id param is actually auction_id
        // Will implement in next version
        $data = Yii::$app->request->getBodyParams();
        if(Yii::$app->user->id){
			$model = ApplyBid::findOne(['auction_id'=>$data['listing_id'],'user_id'=>Yii::$app->user->id]);
			if(!empty($model)){
                if ($model->add_to_calendar == 1) {
				    return ['message'=>'Auction already added to calendar','statusCode'=>422];
                } else {
    				$model->scenario = 'calendar';
                    $data['auction_id'] = $data['listing_id'];
                    unset($data['listing_id']);
                    $model->load($data,'');
    				
    				if($model->validate()){
    					$model->save(false);
    				}
				}
				return $model;
			}else{
                $model = new ApplyBid();
				$model->scenario = 'calendar';
				$model->is_favourite = 0;
                $model->user_id = Yii::$app->user->id;
                $data = Yii::$app->request->getBodyParams();
                $data['auction_id'] = $data['listing_id'];
                unset($data['listing_id']);
                $model->load($data,'');
				if($model->validate()){
					$model->save(false);
				}
					return $model;
				
			}
		}
	}
    
}
