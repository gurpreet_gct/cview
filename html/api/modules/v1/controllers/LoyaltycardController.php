<?php
namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use common\models\Loyaltycard;
use common\models\Qr;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use dosamigos\qrcode\QrCode;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class LoyaltycardController extends Controller
{    
        public $modelClass = 'common\models\Loyaltycard';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['view'],$actions['delete']);                    

		return $actions;
	}

	
	
	/********* add loyalty card of user**************/
	public function actionCreate(){
	$user_id=Yii::$app->user->id;  
	$model= new Loyaltycard();	
	$model->scenario ='create';
	$model->load(Yii::$app->getRequest()->getBodyParams(), '');
	$model->user_id=$user_id;
	if($model->validate()){
		
		$find=Loyaltycard::findOne(['nameOnCard'=>$model->nameOnCard,'user_id'=>$user_id,'cardNumber'=>$model->cardNumber]);
                if($find)
                {
                    $model=$find; 
                    $model->load(Yii::$app->getRequest()->getBodyParams(), '');  
                }
		
		if($model->image!=''){
			$loyaltyimage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '',$model->image));
			$imageNamenew=md5($model->nameOnCard.rand(10,1000).'front'). ".png";
			 $imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['loyaltycardImage']."/".$imageNamenew);
			file_put_contents($imagePath, $loyaltyimage);
			$model->image=$imageNamenew; 
			}
		
		 if($model->data!="")
		 {
			
			$loyaltydataimageName=md5($model->data.'_'.rand());
			if($model->type){
				Yii::$app->barcode->drawBars($model->data,'/web/loyaltycard/'.$loyaltydataimageName.".png");
			}		
			else{
			
			$data=QrCode::png($model->data,Yii::$app->basePath.'/web/loyaltycard/'.$loyaltydataimageName.".png");	
			}
			$model->actualData = $model->data;
			$model->data= $loyaltydataimageName; 
			 
		}
			$model->brand=$model->nameOnCard; 
			$model->loyaltystatus=2;
			
			$model->save();
			$arr_result = array();	
			if(isset($model->data)){
				$arr_result['cardUrl'] = Yii::$app->urlManagerLoyaltyKit->createAbsoluteUrl($model->data);
			}
			$arr_result['image'] = Yii::$app->urlManagerLoyaltyCardFrontEnd->createAbsoluteUrl("loyaltycardImage/".$model->image);
		return ["message"=>"You have successfully added a loyalty card",$arr_result];
	}else{
		return $model;
		}
	}
	/************ end here **************************/
	public function actionIndex(){	 
	 	//echo "saSAa";
	}
	
	
	
	 public function actionUpdate($id)
    {
        $user_id=Yii::$app->user->id;
		$model= Loyaltycard::findOne($id);		 
		$oldfrontimage=$model->image; 		
		$oldfrontdata=$model->actualData;		
		$oldqrurl =$model->data;		
		$model->load(Yii::$app->getRequest()->getBodyParams(), ''); 
        $model->user_id=$user_id;
        
       if($model->validate())
        {
			
			
			 /* for update front image */ 
            if($model->image!="" && (!strstr($model->image,".png")) )
            {	
			
                /******************* add and update front image ****************/
            	$loyaltyimage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $model->image));
            	preg_match('/.*\/(.*?)\./',trim($oldfrontimage),$match);				
				if(count($match))
					$imageName=$match[1]. ".png";
				else 
					$imageName= md5($model->brand.rand(10,1000).'front'). ".png";
				
				$imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['loyaltycardImage']."/".$imageName);
				file_put_contents($imagePath, $loyaltyimage);
				$model->image=$imageName;
				/******************* end here *********************************************/ 							
				}else{
					
					if(strstr($oldfrontimage,'.png')){
						$model->image = $oldfrontimage;
					}
				}
            
                /* make qr and barcode */
				if(!empty($model->data)){
					
            	$loyaltydataimageName=md5($model->data.'_'.rand());
            	
            	$model->actualData = $model->data; 
            	#$model->data=$loyaltydataimageName; 
				}else{
				$model->data = $oldfrontdata;
				$loyaltydataimageName= $oldqrurl;				
				}
			
               if($model->type){
					Yii::$app->barcode->drawBars($model->data,'/web/loyaltycard/'.$loyaltydataimageName.".png");			
					}		
				else{
					$data=QrCode::png($model->data,Yii::$app->basePath.'/web/loyaltycard/'.$loyaltydataimageName.".png");	
				}
				
				$model->data=$loyaltydataimageName; 
				$model->brand=$model->nameOnCard; 
				               	 
				if (($model->save() === false) && (!$model->hasErrors())) {
					throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
					}
				else
					{
						return $model;	
					}
					
			}
			
		return $model;	
    }
     
     public function actionView($id){
		 
			$Loyaltycard =  Loyaltycard:: findOne(['user_id' => Yii::$app->user->id,'id'=>$id]);
							
			return $Loyaltycard;
					
					
	}
	 /* delete */
   public function actionDelete($id)
    {
        $data = Loyaltycard::findOne(['id' => $id])->delete();
		return ["message"=>"Your loyalty card has been deleted."];
    }
   
	
	
	
}
