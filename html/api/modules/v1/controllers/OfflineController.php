<?php
namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use api\modules\v1\models\Cart;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use common\models\Loyaltypoint;
use common\models\Loyalty;
use common\models\UserLoyalty;
use common\models\Profile;
use common\models\Product;
use common\models\Pass;
use common\models\Order;
use common\models\Workorderpopup;
use common\models\Workorders;
use common\models\User;
use common\models\OrderItems;
use common\models\OrderSubItems;
use common\models\Transaction;
//use common\models\Cardtype;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use api\modules\v1\models\Foodordersapi;
use common\models\FoodordersItem;
use common\models\FoodordersSubItem;
use common\models\Store;

class OfflineController extends Controller
{
        public $modelClass = 'common\models\Loyalty';

        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}


  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['view']);

		return $actions;
	}

	public function actionCreate(){

	$user_id				= Yii::$app->user->id;
    $model					= new Loyaltypoint();
	$postData 				= Yii::$app->getRequest()->post();
    $model->user_id			= $user_id;
    $model->foodorder 		= !empty($postData['foodorder'])   ? $postData['foodorder'] : 0;
    $foodorderID       		= !empty($postData['foodorderID']) ? $postData['foodorderID'] : 0;
    $model->foodorder_id 	= $foodorderID;
    $model->load(Yii::$app->getRequest()->getBodyParams(), '');
    $checktype = 0;
	if(isset($foodorderID) &&  $foodorderID !=0){
		$checktype =1;
	}

    if($model->validate())
    {

	  /* start save order ----------------------------------------------------------------*/
		 if(!empty($postData['cart']))
		 {
				$resPdata = $this->processedData($postData['cart']);
				$cartData=json_decode($postData['cart']);
				$created_at = time();
				$order =new Order();
				$order->is_offline=1;
				$order->user_id=Yii::$app->user->id;
				$order->order_type=$checktype;
				$order->discount_amount=$resPdata['discount'];
				$order->grand_total=$resPdata['total'];
				$order->shipping_amount=0;
				$order->shipping_tax_amount=0;
				$order->subtotal=$resPdata['total'] - $resPdata['discount'];
				$order->tax_amount=0;
				$order->total_offline_refunded=0;
				$order->total_online_refunded=0;
				$order->total_paid=round($resPdata['total'] - $resPdata['discount'],2);
				$order->qty_ordered=$resPdata['qty'];
				$order->total_refunded=0;
				$order->can_ship_partially=0;
				$order->customer_note_notify=0;
				$order->billing_address_id=0;
				$order->billing_address="";
				$order->shipping_address_id=0;
				$order->shipping_address="";
				$order->forced_shipment_with_invoice=0;
				$order->weight=0;
				$order->email_sent=0;
				$order->edit_increment=0;
				$order->status=4;
				$order->food_oid=$foodorderID;
				$order->save(false);
				/* Get the orderitems for batch insert */
				$orderItems=array();
				$orderSubItems=array();
				$pass=array();
				//$orderLoyalty=array();
				//$orderLoyalty=array();
				$result=array();

				/* Prepare data if order type is food Order */
				if($model->foodorder == 1 && $foodorderID!='')
				{
					$orderFood = Foodordersapi::findOne($foodorderID);
					if($orderFood)
						{
						$orderFood->status = 4;
						$orderFood->save(false);
						/* send notification to kitchen/resturent */
						if(!empty($postData['lat']) && !empty($postData['lang'])){
						 $this->sendnotification($orderFood->store_id,$postData['lat'],$postData['lang'],$orderFood->id,$order->id);
						}
						/* get food order item from order id */
						$storeId = $orderFood->store_id;
						$forderItem = FoodordersItem::find()->where('order_id='.$foodorderID)->asArray()->all();
						foreach($forderItem as $_forderItem)
							{
							 array_push($orderItems,[
								'product_id'=>$_forderItem['product_id'],
								'deal_id'=>0,
								'deal_count'=>0,
								'isCombo'=>0,
								'qty'=>$_forderItem['qty'],
								'order_id'=>$order->id,
								'user_id'=>Yii::$app->user->id,
								'store_id'=>$storeId,
								'store_location_id'=>0,
								'product_price'=>$_forderItem['price'],
								'product_currency_code'=>0,
								'product_currency_id'=>0,
								'payment_currency_code'=>0,
								'payment_currency_id'=>0,
								'exchange_rate'=>0,
								'product_exchange_price'=>0,
								'tax'=>0,
								'shipping_charges'=>0,
								'discount'=>0,
								'total'=>$_forderItem['grand_total'],
								'payable'=>$_forderItem['grand_total'],
								'status'=>4,
								'created_at'=>$created_at,
								'updated_at'=>$created_at,
								'created_by'=>Yii::$app->user->identity->id,
								'updated_by'=>Yii::$app->user->identity->id,
							]);
							/* save order sub item */
							if($_forderItem['have_child'] == 1)
							{
								$subItem = FoodordersSubItem::find()->where('order_id='.$foodorderID)->asArray()->all();
							foreach($subItem as $_subItem)
							{
								array_push($orderSubItems,[
									'product_id'=>$_subItem['product_id'],
									'deal_id'=>0,
									'qty'=>$_subItem['qty'],
									'order_id'=>$order->id,
									'user_id'=>Yii::$app->user->id,
									'store_id'=>$storeId,
									'store_location_id'=>0,
									'product_price'=>$_subItem['price'],
									'product_currency_code'=>0,
									'product_currency_id'=>0,
									'payment_currency_code'=>0,
									'payment_currency_id'=>0,
									'exchange_rate'=>0,
									'product_exchange_price'=>0,
									'tax'=>0,
									'shipping_charges'=>0,
									'discount'=>0,
									'total'=>$_subItem['qty']*$_subItem['price'],
									'status'=>4,
									'created_at'=>$created_at,
									'updated_at'=>$created_at,
									'created_by'=>Yii::$app->user->identity->id,
									'updated_by'=>Yii::$app->user->identity->id,
							 ]);
							} //end foreach sub item
							}
						} //foreach
					}

				}
				else
				{
					 foreach($cartData as $key=>$value)
							 {
							  foreach($value as $item)
							  {
							 array_push($orderItems,[
							'product_id'=>$item->productID,

							'deal_id'=>$item->workOrderID,
							'deal_count'=>$item->dealCount,
							'isCombo'=>$item->isCombo,
							'qty'=>$item->quantity,
							'order_id'=>$order->id,
							'user_id'=>Yii::$app->user->id,
							'store_id'=>$item->storeID,
							'store_location_id'=>0,
							'product_price'=>$item->productPrice,
							'product_currency_code'=>0,
							'product_currency_id'=>0,
							'payment_currency_code'=>0,
							'payment_currency_id'=>0,
							'exchange_rate'=>0,
							'product_exchange_price'=>0,
							'tax'=>0,
							'shipping_charges'=>0,
							'discount'=>$item->discount,
							'total'=>$item->total,
							'payable'=>$item->total-$item->discount,
							'status'=>4,
							'created_at'=>$created_at,
							'updated_at'=>$created_at,
							'created_by'=>Yii::$app->user->id,
							'updated_by'=>Yii::$app->user->id,
							]);


						if($item->isCombo)
							{

								array_push($orderSubItems,[
									'product_id'=>$item->comboData->productID,
									'deal_id'=>$item->comboData->workOrderID,
									'qty'=>$item->comboData->quantity,
									'order_id'=>$order->id,
									'user_id'=>Yii::$app->user->id,
									'store_id'=>$item->comboData->storeID,
									'store_location_id'=>0,
									'product_price'=>$item->comboData->productPrice,
									'product_currency_code'=>0,
									'product_currency_id'=>0,
									'payment_currency_code'=>0,
									'payment_currency_id'=>0,
									'exchange_rate'=>0,
									'product_exchange_price'=>0,
									'tax'=>0,
									'shipping_charges'=>0,
									'discount'=>$item->comboData->discount,
									'total'=>$item->comboData->total,
									'status'=>4,
									'created_at'=>$created_at,
									'updated_at'=>$created_at,
									'created_by'=>Yii::$app->user->id,
									'updated_by'=>Yii::$app->user->id,
								]);

							}


							}

					}

				}

				if(count($orderItems))
				{
					$OrderItems=new OrderItems();
					$column=array_keys($orderItems[0]);
					Yii::$app->db->createCommand()->batchInsert(OrderItems::tableName(), $column, $orderItems)->execute();

					if(count($orderSubItems))
					{
						$OrderSubItems=new OrderSubItems();
						$column=array_keys($orderSubItems[0]);
						Yii::$app->db->createCommand()->batchInsert(OrderSubItems::tableName(), $column, $orderSubItems)->execute();
					}
				}


	     } // if(isset($postData['cart']))
	   /* end save order ----------------------------------------------------------------*/
		$checkUserbday = User::findOne(['id'=>Yii::$app->user->id]);
		$checkStorebday = User::findOne(['id'=>$model->store_id]);

		$loyalty= Loyalty:: find()->select(
								[Loyalty::tableName().'.*'])
								->where([Loyalty::tableName().'.user_id' => $model->store_id,
								'status'=>1])
								->orderBy([Loyalty::tableName().'.id' => SORT_DESC])->asArray()->one();



			$checkEmployee = User::findOne(['advertister_id' =>$model->store_id, 'loyaltyPin' => $model->pin]);

			if(($checkEmployee != '') && ($loyalty != '') )
			{
				$loyaltypoints = 0;
				$totalSpend = $this->calculateSpend($model->store_id);
				if($totalSpend=='ok')
				{
						if(($checkUserbday->bdayOffer==1) && ($checkStorebday->bdayOffer==1) ){
							$loyaltypoints = $loyalty['pointvalue']* $model->amount*2;
							$model->method = 3;
						}else{
							$loyaltypoints = $loyalty['pointvalue']* $model->amount;
							$model->method = 2;
						}
							$model->valueOnDay = $loyalty['dolor']/$loyalty['loyalty'];
							$model->rValueOnDay = $loyalty['redeem_pointvalue'];
							$model->order_id = 0;
							$model->loyaltypoints = $loyaltypoints;
				}else{
					$model->valueOnDay = 0;
					$model->rValueOnDay = 0;
					$model->order_id = 0;
					$model->loyaltypoints = $loyaltypoints;
				}

			if($model->workorderid)
			{
				$model->transaction_id= $model->transaction_id;

				/************** get isUsed pass value from pass table ********************/
				$pass_count = Pass:: find()->select([
														Pass::tableName().'.id,user_id,workorder_id,store_id,isUsed,count_passes'
													])
										->where([
													Pass::tableName().'.workorder_id' => $model->workorderid,Pass::tableName().'.user_id' => $user_id
												])
										->orderBy([Pass::tableName().'.id' => SORT_DESC])
										->asArray()->one();

					if($pass_count['isUsed']==0){
						$isUsed = 1;
					}else{
						 $isUsed = $pass_count['isUsed'] + 1;
						}

				/************** end here **********************************/
					$condition = 'workorder_id="'.$model->workorderid.'" AND user_id="'.Yii::$app->user->id.'"';
					Yii::$app->db->createCommand()->update(Pass::tableName(),
									['isUsed' => $isUsed], $condition)
									->execute();
			}
			if($model->foodorder==1){
				$model->transaction_id= $model->transaction_id;
			}
			$model->status = 1;
			$model->save();
			  /* change food order status */
			  if($model->foodorder == 1 && $foodorderID!=''){
				$orderFood = Foodordersapi::findOne($foodorderID);
				if($orderFood){
					$orderFood->status = 2;
					$orderFood->save(false);
				}
			  }
			  // save referral commission 
				$modelData = (object) ['subtotal'=>$model->amount,'id'=>0];
				/*$saveCommision = new	\common\models\Order();
				$saveCommision->saveReferral($modelData);		
				*/		
				if($loyaltypoints!=0){
					return ["message"=>'Your pass has been redeemed and '.$loyaltypoints.'  loyalty points added.',
					'orderID'=>!empty($order->id) ? $order->id : '',
					];
				}else{
					return ["message"=>'Your pass has been redeemed.',
							'orderID'=>!empty($order->id) ? $order->id : '',
					];
				}

			}else{

				 return ["message"=>"That PIN does not match your registered 6-digit PIN. Please try again.","statusCode"=>422];
			}
	}else{
		 $errors = $model->errors;

		 if( array_key_exists('transaction_id',$errors))
		 {
			return ["message"=>$errors['transaction_id'][0],$model,"statusCode"=>422];
		 }
		 else
		 return $model;

		}
	}
// $this->sendnotification($orderFood->store_id,$postData['lat'],$postData['lang']);
public function sendnotification($store_id,$lat,$lang,$foid,$oid){

	$store = Store::find()->select('storename,latitude,longitude,geo_radius,id')
							->where('owner='.$store_id)->asArray()->all();
	 $storeDis = array();
	foreach($store as $_store)  {
		 $theta = $_store['longitude'] - $lang;
		 $dist = sin(deg2rad($_store['latitude'])) * sin(deg2rad($lat)) +  cos(deg2rad($_store['latitude'])) * cos(deg2rad($lat)) * cos(deg2rad($theta));
		 $dist = acos($dist);
		 $dist = rad2deg($dist);
		 $miles = $dist * 60 * 1.1515;
		// $unit = strtoupper($unit);
		 $storeData = array();
		 $storeData['distancekm']= ($miles * 1.609344);
		 $storeData['storename']= $_store['storename'];
		 $storeData['storeID']= $_store['id'];
		 $storeData['geo_radius']= $_store['geo_radius'];
		 $storeDis[] = $storeData;
	}
	usort($storeDis, function($a, $b) {
		return $a['distancekm'] - $b['distancekm'];
	});
	$res = isset($storeDis[0]) ? $storeDis[0]:'';
	$radious = isset($res['geo_radius']) ? $res['geo_radius'] : $this->partnerradious($res['storeID']);
	$res['geo_radius'] = $radious;
	$res['distancekm'] = !empty($res['distancekm']) ? $res['distancekm']:1;
	if($res['distancekm'] <= $res['geo_radius'])
	{
			$user = User::find()->where('storeid='.$res['storeID'])
							->one();
			if(isset($user->device_token)) {
				$userDevice[$user->device_token] = $user->device;
				 yii::$app->pushNotification->send($userDevice,["msg"=>"New order #$oid has been paid for. Please open the order and make it active. Please tell the kitchen to begin cooking.+$foid-$oid"],0);
			}


	}
}
public function partnerradious($id)
{
		$user = Profile::find()->select('geo_radius')->where('user_id='.$id)->asArray()->one();
		return isset($user['geo_radius']) ? $user['geo_radius'] : 1;
}
/* processed data */
	public function processedData($data){
		 $cart=json_decode($data);
		 #$store_ids=array();
		 $created_at = time();
		 $carData = array('total'=>0,'discount'=>0,'qty'=>0);
		 foreach($cart as $key=>$value)
           {
               foreach($value as $item)
               {
					# array_push($store_ids,$item->storeID);
					$carData['total']+=$item->total;
					$carData['discount']+=$item->discount;
					$carData['qty']+=$item->quantity;
			   }
		   }
		return   $carData;
	}
		// check minimum spend by user on this store for get points
	public function calculateSpend($storeId){
			$user_id=Yii::$app->user->id;
				$lo = Loyaltypoint::tableName();
				$pro = Profile::tableName();
				$ord = OrderItems::tableName();
				$offline = Loyaltypoint::find()
									->select(["sum($lo.amount) as totalSpend"])
									->where("$lo.store_id=".$storeId)
									->andWhere("$lo.user_id=".$user_id)
									->asArray()
									->one();
				$online = OrderItems::find()
									->select(["sum($ord.payable) as total"])
									->where("$ord.store_id=".$storeId)
									->andWhere("$ord.user_id=".$user_id)
									->asArray()
									->one();
				$minimum = Profile::find()
									->select(["$pro.minimumSpend"])
									->where("$pro.user_id=".$storeId)
									->asArray()
									->one();
				$offline1 = $offline['totalSpend']!=''?$offline['totalSpend']:0;
				$online1  = $online['total']!=''?$online['total']:0;
				$totalSpend = $offline1+$online1;
				if($totalSpend >= $minimum['minimumSpend']){
					return 'ok';
				}else{
					return 'no';
				}

	}


}
