<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use common\models\Paymentcard;
use common\models\Paymentcardtype;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\PaymentAccount;
use api\modules\v1\models\Customer;
class PaymentcardController extends ActiveController
{
        public $modelClass = 'common\models\Paymentcard';
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}


  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['view'],$actions['update'],$actions['index'],$actions['delete']);

		return $actions;
	}

	public function actionToken()
	{
		$braintree = Yii::$app->braintree;
           $response = $braintree->call('ClientToken', 'generate' ,[]);
           return $response;
	}

   /* save new Paymentcard information */
    public function actionCreate()
    {
         $braintree = Yii::$app->braintree;
         $model=new Paymentcard();
         $data=Yii::$app->getRequest()->getBodyParams();
         if($model->load($data,'')&& $model->validate()){

		 /* find the account of customer for braintree */
		   $paymentAccount=PaymentAccount::find()->where(['user_id'=>Yii::$app->user->id,'status'=>1])->one();
                   if(!$paymentAccount)
                   {
                       /* register customer to braintree */
                       $user=Customer::findOne(Yii::$app->user->id);
                       $response = $braintree->call('Customer', 'create', [
                           'firstName' => $user->firstName,
                               'lastName' => $user->lastName,
                               //'company' => 'graycell',
                               'email' => $user->email,
                               'phone' => $user->phone,
                               //'website' => ''
                           ]);

                           /* Register braintree account detail to database */
                           $paymentAccount=new PaymentAccount();
                           $paymentAccount->user_id=Yii::$app->user->id;
                           $paymentAccount->bt_id=$response->customer->id;
                           $paymentAccount->status=1;
                           $paymentAccount->save();

                   }

                   /* generate the payment method */
                   $response = $braintree->call('PaymentMethod','create',[
                       'customerId' => $paymentAccount->bt_id,
                       'paymentMethodNonce' => $data['nonce'],
                        'options' => [
						  //'makeDefault' => true,
						 // 'failOnDuplicatePaymentMethod' => true,
						   'verifyCard' => true
						]
                   ]);

                   if(!$response->success)
                   {

                       return [
                           "message"=> $response->message,
                           'nonce'=>$data['nonce'],
                           "statusCode"=> 404
                           ];
                   }
                   else
                   {

               //    $payment_method->load($response->paymentMethod->attributes());
				   $model->user_id=Yii::$app->user->id;
                   $model->bin=$response->paymentMethod->bin;
                   $model->last4=$response->paymentMethod->last4;
                   $model->cardType=$response->paymentMethod->cardType;
                   $model->expirationMonth=$response->paymentMethod->expirationMonth;
                   $model->expirationYear=$response->paymentMethod->expirationYear;
                   if($response->paymentMethod->cardholderName!="" && $response->paymentMethod->cardholderName!=null)
                   $model->cardholderName=$response->paymentMethod->cardholderName;
                   $model->customerLocation=$response->paymentMethod->customerLocation;
                   $model->imageUrl=$response->paymentMethod->imageUrl;
                   $model->prepaid=$response->paymentMethod->prepaid;
                   $model->healthcare=$response->paymentMethod->healthcare;
                   $model->debit=$response->paymentMethod->debit;
                   $model->durbinRegulated=$response->paymentMethod->durbinRegulated;
                   $model->commercial=$response->paymentMethod->commercial;
                   $model->payroll=$response->paymentMethod->payroll;
                   $model->issuingBank=$response->paymentMethod->issuingBank;
                   $model->countryOfIssuance=$response->paymentMethod->countryOfIssuance;
               //    $payment_method->productId=$response->paymentMethod->productId;
                   $model->token=$response->paymentMethod->token;
                   $model->uniqueNumberIdentifier=$response->paymentMethod->uniqueNumberIdentifier;
                   $model->maskedNumber=$response->paymentMethod->maskedNumber;
                   $model->venmoSdk=$response->paymentMethod->venmoSdk;
                   $model->expirationDate=$response->paymentMethod->expirationDate;
                   $model->is_delete=0;
                   $model->save(false);

					   return [
					   "message"=> Yii::t("app","Card Successfully Saved."),

                           "statusCode"=> 200
					   ];

					}



		}



    }


    /* To get all cards*/
   public function actionIndex()
   {
        $user_id=Yii::$app->user->id;
       return new ActiveDataProvider([
                            'query' => Paymentcard::find()
                       ->where(['user_id'=>$user_id,'is_delete'=>0]),
                       'pagination' => [
						'pageSize' => 50
						],



                        ]);

   }

	public function actionDelete($id)
	{

		$model=Paymentcard::find()
					->where(['is_delete'=>0,'systemToken'=>$id,'user_id'=>Yii::$app->user->id])
					->one();

		if($model)
		{
			try{

			$braintree = Yii::$app->braintree;
			 $response = $braintree->call('PaymentMethod','delete',$model->token);
              if($response->success)
              {
				  $model->is_delete=1;
				  $model->save(false);
				  return[
					"message"=> Yii::t("app","Card succesfully removed."),
					"statusCode"=> 204
					];

			  }
			else
			{
				return[
				 "message"=> $response->message,
				"statusCode"=> 404
				];
				}
			}catch(Exception $e)
			{
				return[
				 "message"=> $e->message,
				"statusCode"=> 404
				];

				}

		}
		else
		{
			return[
			 "message"=> Yii::t("app","Card is already removed."),
			"statusCode"=> 204
			];
		}

	}

    public function actionView($id=0)
    {
		 $paymentAccount=PaymentAccount::find()->where(['user_id'=>Yii::$app->user->id,'status'=>1])->one();

		$braintree = Yii::$app->braintree;
			 $response = $braintree->call('Customer','find',$paymentAccount->bt_id);
			 print_r($response);die;
		}

  // add credit card into promise pay
	public function actionAddCreditCard(){
		$attributes=['card_id'];
		$model=new \yii\base\DynamicModel($attributes);
		$model->addRule($attributes,'required');
		$data = Yii::$app->request->getBodyParams();
		if($model->load($data,'') && $model->validate()) {
			$cards = Yii::$app->promisePay->getCardDetails($model->card_id);
			if(is_array($cards['card'])){
				$cardModel = Paymentcard::findOne(['uniqueNumberIdentifier'=>$model->card_id]);
				if(!$cardModel){
					$cardModel=new Paymentcard();
				}
				 $cardModel->user_id=Yii::$app->user->identity->id;
				 $cardModel->cardholderName=$cards['card']['full_name'];
				 $cardModel->maskedNumber=$cards['card']['number'];
				 $cardModel->expirationMonth=$cards['card']['expiry_month'];
				 $cardModel->expirationYear=$cards['card']['expiry_year'];
				 $cardModel->cardType=$cards['card']['type'];
				 $cardModel->uniqueNumberIdentifier=$cards['id'];
				 $cardModel->issuingBank='promisePay';
				 $cardModel->save(false);
				return $cards;
			}
			return[
			 "message"=> Yii::t("app","Unable to save the card."),
			 "statusCode"=> 204
			];
		} else {
			return $model;
		}
	}
  public function actionRemoveCard()
  {

    $post = Yii::$app->getRequest()->getBodyParams();
    $model=Paymentcard::find()
          ->where(['is_delete'=>0,'uniqueNumberIdentifier'=>$post['card_id'],'user_id'=>Yii::$app->user->id])
          ->one();
    if($model)
    {
          $model->is_delete=1;
          $model->save(false);
          return[
          "message"=> Yii::t("app","Card succesfully removed."),
          "statusCode"=> 200
          ];

    }
    else
    {
      return[
       "message"=> Yii::t("app","Card is already removed."),
      "statusCode"=> 422
      ];
    }

  }
	// get all saved card on promise pay
	public function actionCreditCardList(){

		$payUserId = Yii::$app->user->identity->promise_uid;
		if($payUserId)
		{
			$table = Paymentcard::tableName();
			$userID = Yii::$app->user->identity->id;
			$sql = "select cardholderName,systemToken,maskedNumber,uniqueNumberIdentifier,expirationYear,cardType,expirationMonth from $table where user_id=$userID and issuingBank='promisePay' and  uniqueNumberIdentifier is not null and is_delete=0";
			$result = Yii::$app->db->createCommand($sql)->queryAll();
			if($result){
				return  $result;
			}
		    return [
					   "message"=> Yii::t("app","You have not save any card yet."),

                           "statusCode"=> 422
			   ];
		}else{
			 return [
					   "message"=> Yii::t("app","You are not authorized on the payment gateway."),

                           "statusCode"=> 422
			   ];
		}
	}
	//delete credit card on promise pay
	public function actionRemoveCreditCard(){

		$attribute=['card_id'];
		$model=new \yii\base\DynamicModel($attribute);
		$model->addRule($attribute,'required');
		$data = Yii::$app->request->getBodyParams();
		if($model->load($data,'') && $model->validate()) {

			return Yii::$app->promisePay->removeCard($model->card_id);

		} else {
			return $model;
		}
	}

	// genereate token promise pay
	public function actionGenerateToken(){
		$payUserId = Yii::$app->user->identity->promise_uid;
		if($payUserId)
		{
			return Yii::$app->promisePay->generateToken($payUserId);
		}else{
			 return [
					   "message"=> Yii::t("app","You are not authorized on the payment gateway."),

                           "statusCode"=> 422
			   ];
		}
	}
}
