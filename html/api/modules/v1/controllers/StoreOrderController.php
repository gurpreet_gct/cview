<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use api\components\Controller;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use common\models\Order;
use common\models\OrderItems;
use common\models\StoreOrders;
use common\models\OrderSubItems;

class StoreOrderController extends Controller
{    
        public $modelClass = 'common\models\StoreOrders';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
    public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['delete'],$actions['view'],$actions['index']);                    

		return $actions;
	}

	/* order */
	public function actionIndex()
	{		
		   return new ActiveDataProvider([ 
            'query' => StoreOrders::find()
            ->select(['store_id','sum(payable) as payable',StoreOrders::tableName().'.order_id',StoreOrders::tableName().'.created_at'])
			->where([StoreOrders::tableName().'.user_id'=>Yii::$app->user->id])
			->andWhere(StoreOrders::tableName().'.status=2')
			
		->joinWith('store')		
	//	->joinWith('payment')		
		->groupBy(['store_id',StoreOrders::tableName().'.order_id'])		
		->orderBy(StoreOrders::tableName().".id desc")
       			
        ]);
		 
	}

  	   /* To get all cards*/
   public function actionView($id)
   {
        $user_id=Yii::$app->user->id;
		
        if(isset(Yii::$app->request->get()['order_id']))
        {
        $order_id=Yii::$app->request->get()['order_id'];
        
         return new ActiveDataProvider([ 
            'query' => OrderItems::find()
			->where([OrderItems::tableName().'.user_id'=>Yii::$app->user->id])
			->andWhere([OrderItems::tableName().'.store_id'=>$id])			
			->andWhere([OrderItems::tableName().'.order_id'=>$order_id])			
		
		->joinWith('ordersubitems')
		->joinWith('order')
		 ->orderBy("id desc")
       			
        ]);
		}
		else
		{
			return[
			"statusCode"=>422,
			"message"=>"Order id is required."
			];
			
		}
            
   }
  
   
}
