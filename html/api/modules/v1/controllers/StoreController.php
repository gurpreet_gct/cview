<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Store;
use common\models\Country;
use common\models\StoreCategory;
use api\modules\v1\models\Profiles;
use api\modules\v1\models\UserStore;
use api\modules\v1\models\Manufacturer;
use api\modules\v1\models\Category;
use api\modules\v1\models\Storelocator;
use common\models\User;
use common\models\WorkorderCategory;
use backend\models\Dealstore;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
class StoreController extends ActiveController
{    
        public $modelClass = 'common\models\UserStore';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view'],$actions['searchcategories'],$actions['searchkeyword']);                    

		return $actions;
	}

  	
  
    public function actionIndex()
    {  
		
	  $user_id=Yii::$app->user->id; 
	  	   
       $model = new Manufacturer();
	   $user_id=Yii::$app->user->id;   
	   $result=$model->getManufacturer($user_id);
	
	   return $result;
		

    }
    
    public function checkselect($id){
	
	return array('isselect' => 0);
	}
    
	
	public function actionView($id)
	{   
			$user_id=Yii::$app->user->id; 
			$model= Manufacturer::find()
					->select(["tbl_users.*",'ifnull(if(store_id!=0,1,0),0) as isSelected'])	->joinWith([
				'userStore'=>function($query) use ($user_id) {
																$query->andOnCondition(['=', 'user_id', $user_id]);
															},
				],false,"LEFT JOIN")
				 ->innerJoinWith('profile', false)->where([Manufacturer::tableName().'.id'=>$id,'user_type'=>Manufacturer::ROLE_Advertiser])->one();  
		 
		/* $model=Manufacturer::findOne(['id'=>$id,'user_type'=>Manufacturer::ROLE_Advertiser]);
	     
		 $check=$this->checkselect($id); */
	  
		return $model;
		
	}
	

    /* To add Brand in favourite list */
    public function actionUpdate($id)
	{
		
		$user_id=Yii::$app->user->id;        
		$userStore=UserStore::findOne(['user_id'=>$user_id,'store_id'=>$id]);
		if($userStore)
		{
			$userStore->status=1;
			$userStore->save();
		}
		else
		{
			$userStore=new UserStore();
			$userStore->user_id=$user_id;
			$userStore->store_id=$id;
			$userStore->status=1;
			$userStore->save();
		}
		return ["message"=>"Store added successfully to your favourite list."];
	}
	/* To remove an store from favourite list */
    public function actionDelete($id)
    {        
        $user_id=Yii::$app->user->id;
		$userStore=UserStore::findOne(['user_id'=>$user_id,'store_id'=>$id,'status'=>1]);
		if($userStore)
		{
			$userStore->status=0;
			$userStore->save();
			return ["message"=>"Store removed successfully from your favourite list.","statusCode"=>"204"];
		}
		else
			return ["message"=>"Store already removed from your favourite list.","statusCode"=>"204"];
		
    }
	
	public function actionStorelocator($id)
	{	
    $model=new Storelocator();
    $model->load(Yii::$app->request->getQueryParams(),'');   

    if($model->validate())    
    {
        $lng=$model->lng;
        $lat=$model->lat;
        $countryTable=Country::tableName();
            $query = new Query;
          
            $exp=sprintf('(6371 * acos (cos ( radians(%s) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(%s) ) + sin ( radians(%s) ) * sin( radians( latitude ) ) )) as distance',$lat,$lng,$lat);
            $expression = new Expression($exp);           

            // compose the query
        $query->select('*,'.$expression)
            ->from(Store::tableName())
            ->join("INNER JOIN", $countryTable,sprintf('%s.id_countries=%s.country',$countryTable,Store::tableName()))
            ->where('owner='.$id)
            ->andWhere(['status'=> 1])
            ->orderBy('distance');

        //	$query= Store::find(['owner'=>$id]);//with('countryname',false);
            
             return new ActiveDataProvider([
                'query' => $query,
            ]);
    }else
        return $model;
    }
    
    
    public function actionSearchcategories()
    {
		 $user_id=Yii::$app->user->id; 				
      
		if(isset(Yii::$app->request->post()['categories']))
        {	
			
		$rs=json_decode(Yii::$app->request->post()['categories'],true);  
		
        $category=(array_filter($rs['categories'], function($var){ return ($var['status']==1);}));
        if(!empty($category)) {
		$category_ids=implode(",",ArrayHelper::map($category, 'category_id', 'category_id'));
		}else{ $category_ids=0;}
        $getstore = StoreCategory::find()->select('store_id')->where('category_id in ( '.$category_ids.')')->all();
        
        $location_id=implode(",",ArrayHelper::map($getstore, 'store_id', 'store_id'));
        if(!empty($location_id)) {
         
		/*$getlocationstore = Store::find()->select('owner')->where('id in ( '.$store_id.')')->all();
				
		$location_id=implode(",",ArrayHelper::map($getlocationstore, 'owner', 'owner'));*/
		    
             $result = Manufacturer::find()
					->select(["tbl_profile.brandLogo","tbl_profile.foregroundHexColour","tbl_profile.backgroundHexColour","tbl_users.id","tbl_users.orgName",'ifnull(if(store_id!=0,1,0),0) as isSelected'])
					->joinWith([
				'userStore'=>function($query) use ($user_id) {
																$query->andOnCondition(['=', 'user_id', $user_id]);
															},
				],false,"LEFT JOIN")
				 ->innerJoinWith('profile', false)
					//->leftJoin('userStore', 'store_id = `id` and status=1 and user_id='.$user_id)
					->where('tbl_users.id in ( '.$location_id.')')->andWhere(['user_type'=> 7 ]) 		
					->groupBy('orgName')        
					->orderBy(['orgName'=>SORT_ASC])
					->asArray()
					->All(); 
					
					$datam = array();
					foreach($result as $data){
						$val = array();
						if($data['brandLogo']){
							$val['brandLogo'] = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$data['brandLogo']);
						}
						else{
						$val['brandLogo'] = $data['brandLogo'];						
						}
						$val['foregroundHexColour'] = $data['foregroundHexColour'];
						$val['backgroundHexColour'] = $data['backgroundHexColour'];
						$val['id']					 = $data['id'];
						$val['orgName'] 			= $data['orgName'];
						$val['isSelected']			 = $data['isSelected'];
						$datam [] = $val;
					}
						
						return($datam);
					
        
	   }else{
		       return array(); 
		   }
       
        }
      
	}
	
	public function actionSearchkeyword()
	{
	 	  $user_id=Yii::$app->user->id; 	
	 	 
			if(isset(Yii::$app->request->post()['keyword']))
			{
				
			$keyword = trim($_POST['keyword']);
			
			$category = Category::find()->select('id')->where(['name'=> $keyword])->all();
			
			
			if(!empty($category)) {				
				
			$category_ids=implode(",",ArrayHelper::map($category, 'id', 'id'));
			         
			$getstore = StoreCategory::find()->select('store_id')->where('category_id in ( '. $category_ids.')')->all();
			    
			$user_ids=implode(",",ArrayHelper::map($getstore, 'store_id', 'store_id'));
        
            if(!empty($user_ids)) {
			/*$getlocationstore = Store::find()->select('owner')->where('id in ( '.$store_id.')')->all();
		  		
			$user_ids=implode(",",ArrayHelper::map($getlocationstore, 'owner', 'owner'));*/
		    				 
			
			 $result = Manufacturer::find()
					->select(["tbl_profile.brandLogo","tbl_profile.foregroundHexColour","tbl_profile.backgroundHexColour","tbl_users.id","tbl_users.orgName",'ifnull(if(store_id!=0,1,0),0) as isSelected'])
					->joinWith([
				'userStore'=>function($query) use ($user_id) {
																$query->andOnCondition(['=', 'user_id', $user_id]);
															},
				],false,"LEFT JOIN")
			    ->innerJoinWith('profile', false)
					//->leftJoin('userStore', 'store_id = `id` and status=1 and user_id='.$user_id)
					->where('tbl_users.id in ( '.$user_ids.')')->andWhere(['user_type'=> 7 ]) 		
					->groupBy('orgName')        
					->orderBy(['orgName'=>SORT_ASC])
					->asArray()
					->All(); 
					$datam = array();
					foreach($result as $data){
						$val = array();
						if($data['brandLogo']){
							$val['brandLogo'] = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$data['brandLogo']);
						}
						else{
						$val['brandLogo'] = $data['brandLogo'];						
						}
						$val['foregroundHexColour'] = $data['foregroundHexColour'];
						$val['backgroundHexColour'] = $data['backgroundHexColour'];
						$val['id']					 = $data['id'];
						$val['orgName'] 			= $data['orgName'];
						$val['isSelected']			 = $data['isSelected'];
						$datam [] = $val;
					}
						
						return($datam);	
				}else{
					
				 return array(); 
				}		
		 }else{ 
			 
			
				 $result = Manufacturer::find()
					->select(["tbl_profile.brandLogo","tbl_profile.foregroundHexColour","tbl_profile.backgroundHexColour","tbl_users.id","tbl_users.orgName",'ifnull(if(store_id!=0,1,0),0) as isSelected'])
					->joinWith([
				'userStore'=>function($query) use ($user_id) {
																$query->andOnCondition(['=', 'user_id', $user_id]);
															},
				],false,"LEFT JOIN")
				 ->innerJoinWith('profile', false)
					//->leftJoin('userStore', 'store_id = `id` and status=1 and user_id='.$user_id)
					->where(['LIKE', 'orgName', $keyword])->andWhere(['user_type'=> 7 ]) 				    
					->orderBy(['orgName'=>SORT_DESC])
					->groupBy('orgName')  
					->asArray()
					->All(); 
					
					$datam = array();
					foreach($result as $data){
						$val = array();
						if($data['brandLogo']){
							$val['brandLogo'] = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$data['brandLogo']);
						}
						else{
						$val['brandLogo'] = $data['brandLogo'];						
						}
						$val['foregroundHexColour'] = $data['foregroundHexColour'];
						$val['backgroundHexColour'] = $data['backgroundHexColour'];
						$val['id']					 = $data['id'];
						$val['orgName'] 			= $data['orgName'];
						$val['isSelected']			 = $data['isSelected'];
						$datam [] = $val;
					}
						
						return($datam);		
					
			
		}
			
		 }						
			
	}
	
	
	
}
