<?php

namespace api\modules\v1\controllers;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;


use common\models\User;
use common\models\Deal;
use common\models\PolygonsRegion;
use common\models\Workorderpopup;
use common\models\Workorders;
use common\models\ZoneNotificationStats;
use common\components\PushNotification;
use backend\models\Notificationsetting;
use common\models\Foodnotification;

use common\models\Beaconmanager;
use common\models\Pass;
use common\models\UserPsa;
use common\models\Profile;
use api\modules\v1\models\UserCategory;
use api\modules\v1\models\Foodordersapi;
use common\models\Store;
use api\modules\v1\models\Category;
use api\modules\v1\models\Manufacturer;
use api\modules\v1\models\Storelocator;

use yii\db\Expression;
use yii\helpers\ArrayHelper;

class GeolocationController extends ActiveController
{
	public $modelClass = 'common\models\User';
	public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	public function actions(){
		
		$actions = parent::actions();

		// disable the "create" and "update" actions
		
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);                     

		return $actions;
		
	}
	
	public function behaviors(){
		
		$behaviors = parent::behaviors();
				
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			//'except' => ['sendnote'],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		
		return $behaviors;
		
	}
	
	/*
	 * Update lat/long of user based on geo location
	 * 
	*/
	 
	public function actionCreate(){
		$user = [];
		$user					= Yii::$app->user->identity;  
		$user_id 				= $user->id;  
		$psaReturn 				= false;	
		$User 					= new User();
		$PolygonsRegion 		= new PolygonsRegion();
		$Workorderpopup 		= new Workorderpopup();
		$PushNotification 		= new PushNotification();
		$notificationset 		= new Notificationsetting();
		
		
		
		if($User->load(Yii::$app->request->getBodyParams(),'')){
			
			$userData 		= Yii::$app->request->getBodyParams();		
			// check user exists
			
			if($user){			
				$LastUpdated_on 		= $user->updated_at ;
				$user->latitude 		= $userData['latitude'];
				$user->longitude 		= $userData['longitude'];
				$user->updated_at 		= strtotime(date('Y-m-d H:i:s'));
				$user->save();				// update user lat/lng
				
				
				$getUserData 			= $user;
				
				/* send notification for enable gps and bluetoth if user have food order */
				$now = time();	 
				$diiffTime =  round(abs($now - $LastUpdated_on) / 60,2); 
				if(isset($userData['bluetooth']) && isset($userData['gps']) && $userData['bluetooth']==0 && $userData['gps'] ==0){
					 $IfUserHavefoodOrder = $this->getFoodOrder($user_id,0);				
					
					if($IfUserHavefoodOrder=='ok'){
								$userDevice[$getUserData->device_token] = $getUserData->device;
								
								return yii::$app->pushNotification->send($userDevice,["msg"=>'You have a pending food order. Turning off Bluetooth or location services will prevent us from completing this order.'],0);
					}
				}
				else{
					$IfUserHavefoodOrder = $this->getFoodOrder($user_id,1);		
				}
			// get user data
				
			if(!empty($getUserData->latitude) && !empty($getUserData->longitude)){			
				$latitude 	= $getUserData->latitude;
				$longitude 	= $getUserData->longitude;				
				// get all workorder category
				$category_ids	=	implode(",",ArrayHelper::map( UserCategory::find()
										->select('category_id')
										->where(['user_id'=>$user_id,'status'=>1])
										->all(), 'category_id', 'category_id'));					
								
				$provider =  new ActiveDataProvider(['query' =>Deal::find()->select([UserPsa::tableName().'.user_id',Workorderpopup::tableName().'.iphoneNotificationText',Workorderpopup::tableName().'.workorder_id',Workorders::tableName().'.geoProximity',PolygonsRegion::tableName().'.*'])										  
					->joinWith('workorderpopup')
					->joinWith(['workorders'=> function ($q)  {						
						$q->where(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))));	
						}
					],true,'INNER JOIN')
					->joinWith(['workordercategory'=> function ($q) use ($category_ids) {
                       if($category_ids=="")
                           $category_ids='0';
							$q->where('category_id in ( '.$category_ids.')');
                      }
                   ],false,'INNER JOIN')
					->joinWith(['userpsa'=>function($q) use ($user_id) {
                                  $q->onCondition(UserPsa::tableName().'.user_id= '. $user_id);
                                  $q->andOnCondition(UserPsa::tableName().'.status!= 0');                                
                                  }],false,'LEFT JOIN')  
					->joinWith(['polygonregion'],true,'LEFT JOIN')					
					->andWhere([Workorders::tableName().'.status' =>1])
					->andWhere('MBRWithin(POINT('.$latitude.','.$longitude.'),polygon_shape)')
					->andWhere("(CASE rulecondition WHEN 1 THEN IF(Loyalty1 & Loyalty2,(CASE Loyalty3 WHEN 1 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= UNIX_TIMESTAMP(curdate() - INTERVAL Loyalty2 day) AND user_id = $user_id ) WHEN 2 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= UNIX_TIMESTAMP(curdate() - INTERVAL Loyalty2 week) AND user_id = $user_id) WHEN 3 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= UNIX_TIMESTAMP(curdate() - INTERVAL Loyalty2 month) AND user_id = $user_id) WHEN 4 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= UNIX_TIMESTAMP(curdate() - INTERVAL Loyalty2 year) AND user_id = $user_id) END),false) ELSE rulecondition = 0 END)") 
		
					->andWhere("(CASE rulecondition WHEN 1 THEN IF(userenter,userenter = 1,IF(usereturned, usereturned = 1, usereturned = 0 )) ELSE rulecondition = 0 END)") 
								
					->andWhere("(CASE dayparting WHEN 1 THEN dayparting = 1 WHEN 3 THEN IF(daypartingcustom3 & daypartingcustom2,(daypartingcustom3 <= '".date('H', time())."' and daypartingcustom3end >= '".date('H', time())."') || (daypartingcustom2 <= '".date('H', time())."' and daypartingcustom2end >= '".date('H', time())."') || daypartingcustom1 <= '".date('H', time())."' and daypartingcustom1end >= '".date('H', time())."',IF(daypartingcustom2 & daypartingcustom1,(daypartingcustom2 <= '".date('H', time())."' and daypartingcustom2end >= '".date('H', time())."') || daypartingcustom1 <= '".date('H', time())."' and daypartingcustom1end >= '".date('H', time())."',daypartingcustom1 <= '".date('H', time())."' and daypartingcustom1end >= '".date('H', time())."')) ELSE rulecondition = 0 END)") 
								
					->andWhere("(CASE temperature WHEN 1 THEN temperature = 1 WHEN 2 THEN IF(temperaturestart & temperaturend,temperaturestart = 9 and temperaturend >= 15, true) ELSE rulecondition = 0 END)")
								
					->andWhere("(CASE rulecondition WHEN 1 THEN IF(behaviourDwell & behaviourDwellMinutes, (CASE behaviourDwellMinutes WHEN 1 THEN (SELECT (max(UNIX_TIMESTAMP(detect_at))-min(UNIX_TIMESTAMP(detect_at)))/60 >= behaviourDwell FROM tbl_beacon_detect_logs) WHEN 2 THEN (SELECT (max(UNIX_TIMESTAMP(detect_at))-min(UNIX_TIMESTAMP(detect_at))) >= behaviourDwell FROM tbl_beacon_detect_logs)END), false) ELSE rulecondition = 0 END)") 
								
					->andWhere("(CASE rulecondition WHEN 1 THEN IF(behaviourvisit1 & behaviourvisit2 & behaviourvisit3, (CASE behaviourvisit3 WHEN 1 THEN (SELECT count(*) >= behaviourvisit1 FROM ".UserPsa::tableName()." WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 DAY ) AND user_id = $user_id AND status= 0 ) WHEN 2 THEN (SELECT count(*) >= behaviourvisit1 FROM ".UserPsa::tableName()." WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 WEEK) AND user_id = $user_id AND status= 0) WHEN 3 THEN (SELECT count(*) >= behaviourvisit1 FROM ".UserPsa::tableName()." WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 MONTH) AND user_id = $user_id AND status= 0) END) ,false) ELSE rulecondition = 0 END)") 
					->groupBy('id')
					->having(UserPsa::tableName().'.user_id is null')
					->orderBy(new Expression('rand()')),          
				    'pagination' => [
						'pageSize' => 1,
					]]);
					
					// return $provider;
					// die;
					
					if($provider->totalCount>0){
						
						$WorkordData 	= $provider->getModels();
						
						
						$geoProximity 	= $WorkordData[0]->workorders->geoProximity;
						
						$workorder_id 	= $WorkordData[0]->workorders->id;
						$message_limit 	= $WorkordData[0]->polygonregion->message_limit;
						$iphoneNotificationText 	= $WorkordData[0]->workorderpopup->iphoneNotificationText;
						
						$startUnix 		= strtotime(date('Y-m-d 00:00:00'));
						$endUnix 		= strtotime(date('Y-m-d 23:59:59'));
							
						$geoProximity 	= isset($geoProximity)?$geoProximity:null;
							
						
						// check proximity type is both=1,Proximity=2
							
						if(!empty($geoProximity) || $geoProximity==1 || $geoProximity==2){
								
							// check from user psa is notification sent or not
							$userPsa = new UserPsa();
							$userpsa = $userPsa::find()
											->where(['user_id'=>$user_id,
													'workorder_id'=>$workorder_id,
													'status'=>1
											])
											->orderBy('id DESC')
											->asArray()
											->one();
									
									// not send notification 
							if($userpsa){
								
								$psaReturn = false;
								
							}else{
								// send notification 
								$psaReturn = true;
								
							}
								
						}elseif(!empty($geoProximity) || $geoProximity==3){
								
								$psaReturn = true;
						}
							
						// $message['msg'] = "test";
						// $userDevice['2b13a9d55510ffc1733fc1af32341374ef7f86087e367e4541885db52806f68c'] = 'ios';
							
							
						// check user is eligible to get notification
						
						
						if($psaReturn){
							
							$ZoneNotificationStats 	= new ZoneNotificationStats();
							
							$notifStats = $ZoneNotificationStats::find()
											->where(['user_id'=>$user_id])
											->andWhere(['deal_id'=>$workorder_id])
											->andWhere(['between','created_at',$startUnix,$endUnix])
											->orderBy(['id'=>SORT_DESC])
											->asArray()
											->one();
							
							
							// check for geo location or proximity
							
							
							$notificationsetData 		= $notificationset::find()
										->where(['user_id'=>$user_id])
										->asArray()
										->one();
										
							
							if(!empty($notifStats) && !empty($notificationsetData)){
								
								// on/off active for user message sent before
								
								$notifc = $ZoneNotificationStats::find()
											->where(['user_id'=>$user_id])
											->andWhere(['deal_id'=>$workorder_id])
											->andWhere(['between','used_limit',1,$message_limit])
											->andWhere(['between','created_at',$startUnix,$endUnix])
											->orderBy(['id'=>SORT_DESC])
											->one();
											
								$notification_allow = $notificationsetData['notification_allow']==0?true:$notifc['used_limit']<$notificationsetData['notification_allow'];			
								// $notification_allow = $notificationsetData['notification_allow']==0?true:($notificationsetData['notification_allow']>0?$notifc['used_limit']<$notificationsetData['notification_allow']);	
								
								if(!empty($notifc) && $notifc['used_limit']<=$message_limit && ($notifc['detect_type'] !=1 && $notifc['detect_type'] !=2) && ( $notification_allow && $notificationsetData['is_on']==1)){
																		
									$usedLimit = $notifc['used_limit']+1;
									
									$notifStats = $ZoneNotificationStats;
									
									$notifStats->user_id = $user_id;
									$notifStats->deal_id = $workorder_id;
									$notifStats->detect_type = $geoProximity;
									$notifStats->notification_limit = $message_limit;
									$notifStats->used_limit = $usedLimit;
									$notifStats->created_at = strtotime(date('Y-m-d H:i:s'));
									$notifStats->updated_at = strtotime(date('Y-m-d H:i:s'));
									
									if($notifc['used_limit']==$message_limit){
										
										$return = false;
										
										
									}else{
										
										$notifStats->save(false);
									
										$return = true;
									}
									
								}else{
									
									$return = false;
									
								}
								
							}elseif(!empty($notifStats) && empty($notificationsetData)){
								
								// on/off not active for user message sent
								
								
								$notifc = $ZoneNotificationStats::find()
											->where(['user_id'=>$user_id])
											->andWhere(['deal_id'=>$workorder_id])
											->andWhere(['between','used_limit',1,$message_limit])
											->andWhere(['between','created_at',$startUnix,$endUnix])
											->orderBy(['id'=>SORT_DESC])
											->one();
											
								
								if(!empty($notifc) && $notifc['used_limit']<$message_limit && ($notifc['detect_type'] !=1 && $notifc['detect_type'] !=2)){
																		
									$usedLimit = $notifc['used_limit']+1;
									
									$notifStats = $ZoneNotificationStats;
									
									$notifStats->user_id = $user_id;
									$notifStats->deal_id = $workorder_id;
									$notifStats->detect_type = $geoProximity;
									$notifStats->notification_limit = $message_limit;
									$notifStats->used_limit = $usedLimit;
									$notifStats->created_at = strtotime(date('Y-m-d H:i:s'));
									$notifStats->updated_at = strtotime(date('Y-m-d H:i:s'));
									
									if($notifc['used_limit']==$message_limit){
										
										$return = false;
										
										
									}else{
										
										$notifStats->save(false);
									
										$return = true;
									}
									
								}else{
									
									$return = false;
									
								}								
								
							}elseif(empty($notifStats) && !empty($notificationsetData)){
								
								// on/off active for user message not sent
								// check $message_limit
								
								if($message_limit>0 && $notificationsetData['is_on']==1){
									
									$notifStats = $ZoneNotificationStats;
								
									$notifStats->user_id 	= $user_id;
									$notifStats->deal_id 	= $workorder_id;
									$notifStats->detect_type = $geoProximity;
									$notifStats->notification_limit = $message_limit;
									$notifStats->used_limit = 1;
									$notifStats->created_at = strtotime(date('Y-m-d H:i:s'));
									$notifStats->updated_at = strtotime(date('Y-m-d H:i:s'));
									
									$notifStats->save(false);
									
									$return = true;
									
								}else{
									
									$return = false;
								}
								
							}else{
								
								// on/off not active for user message not sent
								
								// insert into zone_notification_stats for 1st notification
								
								$notifStats = $ZoneNotificationStats;
								
								$notifStats->user_id = $user_id;
								$notifStats->deal_id = $workorder_id;
								$notifStats->detect_type = $geoProximity;
								$notifStats->notification_limit = $message_limit;
								$notifStats->used_limit = 1;
								$notifStats->created_at = strtotime(date('Y-m-d H:i:s'));
								$notifStats->updated_at = strtotime(date('Y-m-d H:i:s'));
								
								$notifStats->save(false);
								
								$return = true;
								
							}
							
							// check user is eligible to get notification end
							
							if($return){
								
								$userDevice[$getUserData->device_token] = $getUserData->device;
								
								return yii::$app->pushNotification->send($userDevice,["msg"=>$iphoneNotificationText],$workorder_id);
								
							} // send notification end
							
							
							
						} // check geo or proximity ends
							
							
							
					}
					
					return $provider;
					
				}
				
			}
			
			
		} 
		
			// return $user;
			
		die;
		
	}
	
	public function getFoodOrder($id,$status){
		$foodOrder = Foodordersapi::find()->where(['user_id'=>$id,'status'=>1])->asArray()->one();
		if(isset($foodOrder['id'])){
			$check = Foodnotification::findOne(['user_id'=>$id]);
			if(isset($check->id)){
				$check->updated_at = time();
				$check->status =$status;
				$check->save(false);
			}
			else{
				$modle = new Foodnotification();
				$modle->user_id = $id;
				$modle->updated_at = time();
				$check->status = $status;
				$modle->save(false);
			}
			return 'ok';
		}else{
			return 'no';
		}
		
	}
	/*
	public function ActionSendnote(){
		echo 'ss'; die;
		$foodOrder = Foodordersapi::find()->where(['status'=>1])->asArray()->all();
		foreach($foodOrder as $_foodOrder){
		
		
		}
	
	}*/
}
