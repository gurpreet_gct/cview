<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\RegisterBid;
use api\modules\v1\models\Profiles;
use api\modules\v1\models\AccountVerification;
use api\modules\v1\models\ApplyBid;
use common\models\User;
use common\models\Property;
use common\models\Auction;
use common\models\CviewConfig;
use api\components\Controller;

/**
 * RegisterBidController
 *
 * Save|Update Register Bid management methods for APIs are defined here.
 */
class RegisterBidController extends Controller
{
    public $modelClass = 'api\modules\v1\models\RegisterBid';
    
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "update" actions
        unset($actions['delete'], $actions['index'], $actions['update'],$actions['create']);

        return $actions;
    }
	
	/**
	 * To Register Bid
	 *
	 * @ api {post} /api/web/v1/bid/savebid Save Bid
	 * @ apiName Savebid
	 * @ apiVersion 0.1.1
	 * @ apiGroup Bid
	 * @ apiDescription To save bid, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} app_id (Optional) *2=For CVIEW app*
	 * @apiParam {Number} mobile User's Mobile
	 * @apiParam {Number} otp OTP sent on mobile
	 * @apiParam {String} address_1 User's Address
	 * @apiParam {String} address_2 User's Address
	 * @apiParam {String} state_id The state/county where the property is located
	 * @apiParam {String} postcode User's Postcode
	 * @apiParam {Number} country_id User's Country
	 * @apiParam {Number} gender Gender of user 1=Male /2=Female
	 * @apiParam {String} passport_number User's Passport
	 * @apiParam {String} driving_licence_number User's Licence
	 * @apiParam {String} mediacare_number Image in base64 encoded Format 
	 * @apiParam {String} recent_utility_bill Image in base64 encoded Format 
	 * @apiParam {String} solicitor_firstname Solicitor's First Name
	 * @apiParam {String} solicitor_lastname Solicitor's Last Name
	 * @apiParam {String} solicitor_email Solicitor's Email
	 * @apiParam {Number} solicitor_mobile Solicitor's Mobile
	 * @apiParam {Number} property_id Bidding Property
	 * @apiParam {Number} bidding_panel_number Bidding Panel Number
	 * @apiParam {Number} bidding_colour Represents the bidder at auction
	 * @apiParam {String} guarantor_first_name Guarantor First Name
	 * @apiParam {String} guarantor_lastname Guarantor Last Name
	 * @apiParam {String} guarantor_email Guarantor Email
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
     *         "message": "",
     *	       "data": {
     *             "id": 15,
     *             "user_id": 362,
     *             "mobile": "324536546",
     *             "otp": null,
     *             "address_1": "address 1",
     *             "address_2": "address 2",
     *             "state_id": 0,
     *             "postcode": null,
     *             "country_id": 0,
     *             "gender": "3",
     *             "passport_number": "2312432dfds",
     *             "driving_licence_number": "cdsf34324",
     *             "medicare_file": "mediacare_number910.jpg",
     *             "utilitybill_file": "UtilityBill_Number_362_371.jpg",
     *             "solicitor_firstname": "test",
     *             "solicitor_lastname": "test2",
     *             "solicitor_email": "test@solicitor.com",
     *             "solicitor_mobile": "5885155842",
     *             "property_id": "1",
     *             "bidding_panel_number": "243265475673",
     *             "bidding_colour": "10",
     *             "guarantor_first_name": "gurentor1",
     *             "guarantor_lastname": "gurentor last",
     *             "guarantor_email": "gui@gui.com",
     *         },
     *         "status": 200
     *     }
	 *
	 * @apiError Validation Error Make sure to use submit correct data to save an bid
	 *
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 422 Validation Error
	 *     {
	 *       "message": "",
	 *       "data": [
	 *         {
	 *           "field": "user_id",
	 *           "message": "User Id must be an integer."
	 *         },
	 *         {
	 *           "field": "property_id",
	 *           "message": "Property Id must be an integer."
	 *         },
	 *         {
	 *           "field": "is_favourite",
	 *           "message": "Is Favourite must be an integer."
	 *         }
	 *       ],
	 *       "status": 422
	 *     }
	 * 
	 * @return Object|User-model
	 */
	public function actionSavebid()
    {   
		$authToken =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        $user = User::findIdentityByAccessToken($authToken);
        if (!$user || empty($authToken)) {
            return ['Invalid request, Login first'];
        }
        $formData = Yii::$app->getRequest()->getBodyParams();
        $formData['user_id'] = $user->id;
        $model = new RegisterBid(['scenario' => 'Savebid']);
		$model->load($formData, '');
		
		if(!empty(Yii::$app->request->post()['medicare_file'])){
           	$medicare_doc = Yii::$app->request->post('medicare_file');
					
			$data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $medicare_doc));
			$imageName='mediacare_number'.rand(10,1000). ".jpg";
			$imagePath1=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['profileUploadDocPath'].$imageName);
			$model->medicare_file=$imageName;
		}
		
		if(!empty(Yii::$app->request->post()['utilitybill_file'])){
           	$utilitybill_doc = Yii::$app->request->post('utilitybill_file');
					
			$data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $utilitybill_doc));
			$imageName='UtilityBill_Number'.rand(10,1000). ".jpg";
			$imagePath2=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['profileUploadDocPath'].$imageName);
	        $model->utilitybill_file=$imageName;
		}
		if ($model->validate()) {        
            // $model->save();
		 	if (!empty($data1)) {
		 		file_put_contents($imagePath1, $data1);
		 	}
	     	if (!empty($data2)) {
		 		file_put_contents($imagePath2, $data2);
	     	}
		}
        return $model;
    }
	
    /**
	 * To Update Bid
	 *
	 * @api {post} /api/web/v1/register-bid/updatebid Update Bid
	 * @apiName Updatebid
	 * @apiVersion 0.1.1
	 * @apiGroup Bid
	 * @apiDescription To Update bid, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {Number} user_id User's Id.
	 * @apiParam {Number} mobile User's Mobile
	 * @apiParam {Number} otp OTP sent on mobile
	 * @apiParam {String} address_1 User's Address
	 * @apiParam {String} address_2 User's Address
	 * @apiParam {String} state_id The state/county where the property is located
	 * @apiParam {String} postcode User's Postcode
	 * @apiParam {Number} country_id User's Country
	 * @apiParam {Number} gender Gender of user 1=Male /2=Female
	 * @apiParam {String} passport_number User's Passport
	 * @apiParam {String} driving_licence_number User's Licence
	 * @apiParam {String} mediacare_number Image in base64 encoded Format 
	 * @apiParam {String} recent_utility_bill Image in base64 encoded Format 
	 * @apiParam {String} solicitor_firstname Solicitor's First Name
	 * @apiParam {String} solicitor_lastname Solicitor's Last Name
	 * @apiParam {String} solicitor_email Solicitor's Email
	 * @apiParam {Number} solicitor_mobile Solicitor's Mobile
	 * @apiParam {Number} property_id Bidding Property
	 * @apiParam {Number} bidding_panel_number Bidding Panel Number
	 * @apiParam {Number} bidding_colour Represents the bidder at auction
	 * @apiParam {String} guarantor_first_name Guarantor First Name
	 * @apiParam {String} guarantor_last_name Guarantor Last Name
	 * @apiParam {String} guarantor_email Guarantor Email
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
     *      "message": "",
     *	   "data": {
     *      "id": 15,
     *      "user_id": 362,
     *      "mobile": "324536546",
     *      "otp": null,
     *      "address_1": "address 1",
     *      "address_2": "address 2",
     *      "state_id": 0,
     *      "postcode": null,
     *      "country_id": 0,
     *      "gender": "3",
     *      "passport_number": "2312432dfds",
     *      "driving_licence_number": "cdsf34324",
     *      "medicare_file": "mediacare_number910.jpg",
     *      "utilitybill_file": "UtilityBill_Number_362_371.jpg",
     *      "solicitor_firstname": "test",
     *      "solicitor_lastname": "test2",
     *      "solicitor_email": "test@solicitor.com",
     *      "solicitor_mobile": "5885155842",
     *      "property_id": "1",
     *      "bidding_panel_number": "243265475673",
     *      "bidding_colour": "10",
     *      "guarantor_first_name": "gurentor1",
     *      "guarantor_lastname": "gurentor last",
     *      "guarantor_email": "gui@gui.com",
     *      },
     *      "status": 200
     *      }
	 *
	 * @apiError Validation Error Make sure to use submit correct data to save an bid
	 *
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 422 Validation Error
	 *     {
	 *       "message": "",
	 *       "data": [
	 *         {
	 *           "field": "user_id",
	 *           "message": "User Id must be an integer."
	 *         },
	 *         {
	 *           "field": "property_id",
	 *           "message": "Property Id must be an integer."
	 *         },
	 *         {
	 *           "field": "is_favourite",
	 *           "message": "Is Favourite must be an integer."
	 *         }
	 *       ],
	 *       "status": 422
	 *     }
	 * 
	 * @return Object|User-model
	 */
	public function actionUpdatebid()
	{
		$authToken =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        $user = User::findIdentityByAccessToken($authToken);
        if (!$user || empty($authToken)) {
            return ['Invalid request, Login first'];
        }
        $formData = Yii::$app->getRequest()->getBodyParams();
		
        $formData['user_id'] = $user->id;
		$model = RegisterBid::findOne(['user_id' => $formData['user_id'], 'auction_id' => $formData['auction_id']]);
		$model->load($formData, '');
		
		
		if(!empty(Yii::$app->request->post()['medicare_file'])){
           $medicare_doc = Yii::$app->request->post('medicare_file');
					
		$data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $medicare_doc));
		$imageName='mediacare_number'.rand(10,1000). ".jpg";
		$imagePath1=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['profileUploadDocPath'].$imageName);
		$model->medicare_file=$imageName;
		}
		
		if(!empty(Yii::$app->request->post()['utilitybill_file'])){
           $utilitybill_doc = Yii::$app->request->post('utilitybill_file');
					
		$data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $utilitybill_doc));
		$imageName='UtilityBill_Number_'.$user->id."_".rand(10,1000). ".jpg";
		$imagePath2=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['profileUploadDocPath'].$imageName);
        $model->utilitybill_file=$imageName;
		}	
		if($model->validate()){
			//echo '<pre>'; print_r($model); die('here');
            $model->save();
		 if(!empty($data1))
		 file_put_contents($imagePath1, $data1);
	     if(!empty($data2))
		 file_put_contents($imagePath2, $data2);
		}
		
        return $model;
	}
	

	/**
	 * To Register Bid
	 *
	 * @api {post} /api/web/v1/register-bid/create Register to Bid
	 * @apiName Register bid
	 * @apiVersion 0.1.1
	 * @apiGroup Bid
	 * @apiDescription To save bid, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {Number} auction_id Auction Id 
	 * @apiParam {Number} mobile User's Mobile
	 * @apiParam {String} address_1 User's Address
	 * @apiParam {String} address_2 User's Address
	 * @apiParam {Number} state The state/county where the property is located
	 * @apiParam {String} postcode User's Postcode
	 * @apiParam {Number} country_id User's Country
	 * @apiParam {Number} gender Gender of user 1=Male /2=Female
	 * @apiParam {String} passport_number User's Passport 
	 * @apiParam {String} driving_licence_number User's Licence
	 * @apiParam {String} mediacare_number Image in base64 encoded Format 
	 * @apiParam {String} recent_utility_bill Image in base64 encoded Format 
	 * @apiParam {String} solicitor_firstname Solicitor's First Name
	 * @apiParam {String} solicitor_lastname Solicitor's Last Name
	 * @apiParam {String} solicitor_email Solicitor's Email
	 * @apiParam {Number} solicitor_mobile Solicitor's Mobile
	 * @apiParam {Number} bidding_panel_number Bidding Panel Number
	 * @apiParam {Number} bidding_colour Represents the bidder at auction
	 * @apiParam {String} guarantor_first_name Guarantor First Name
	 * @apiParam {String} guarantor_lastname Guarantor Last Name
	 * @apiParam {String} guarantor_email Guarantor Email
	 * @apiParam {String} callback_url Callback url to post the data on success 
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 OK
	 *    {
     *        "message": "",
     *	      "data": {
     *            "property_id": "1",
     *            "user_id": 351,
     *    		  "bidding_colour": "red",
     *   		  "bidding_panel_number": "12",
     *    		  "id": 1,
     *    		  "profile": {
     *        	      "id": 732,
     *        	      "companyName": null,
     *        		  "houseNumber": "",
     *        		  "street": "",
     *        		  "city": "",
     *        		  "state": "",
     *        		  "country": "",
     *        		  "postcode": "",
     *        		  "brandName": "",
     *        		  "brandLogo": "",
     *        		  "brandAddress": "",
     *        		  "brandDescription": "",
     *        		  "addLoyalty": null,
     *        		  "shopNow": null,
     *        		  "foodEstablishment": 0,
     *        		  "minimumSpend": null,
     *        		  "socialmediaFacebook": "",
     *        		  "socialmediaTwitter": "",
     *        		  "socialmediaGoogle": "",
     *        		  "socialmediaPinterest": "",
     *        		  "socialmediaInstagram": "",
     *        		  "backgroundHexColour": "",
     *        		  "foregroundHexColour": "",
     *        		  "socialcheck": null,
     *        		  "addcolors": null,
     *        		  "geo_radius": null,
     *        		  "tempid": "",
     *        		  "transactionrule": null,
     *        		  "subscriptionFees": null,
     *        		  "passport_number": "123",
     *        		  "driver_license_number": "212",
     *        		  "mediacare_number": "http://localhost/AlphaWallet/html/frontend/web/profile/docs/mediacare_number776.jpg",
     *        		  "recent_utility_bill": "http://localhost/AlphaWallet/html/frontend/web/profile/docs/UtilityBill_Number550.jpg",
     *        		  "subscriptionDueDate": null,
     *       		  "subscriptionPay": null
     *   		  }
     *		  },
     *        "status": 200
     *    }
     *
     * @apiError Validation Error Make sure to use submit correct data to save an bid
	 *
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Unprocessable entity
	 *    {
     *        "message": "",
     *        "data": [
     *            {
     *                "field": "user_id",
     *                "message": "The bid with property_id \"1\" and user_id \"351\" already saved"
     *            },
     *            {
     *                "field": "property_id",
     *                "message": "The bid with property_id \"1\" and user_id \"351\" already saved"
     *            }
     *        ],
     *        "status": 422
     *    }
     *    Or
     *    {
	 *        "message": "",
	 *        "data": [
	 *            "Unable to place a bid, time exceeded."
	 *        ],
	 *        "status": 200
	 *    }
     *
	 * @return Object
	 */
	public function actionCreate()
	{	
		$authToken =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        $user = User::findIdentityByAccessToken($authToken);
        if (!$user || empty($authToken)) {
            return ['Invalid request, Login first'];
        }
        $formData = Yii::$app->getRequest()->getBodyParams();
        // Checking if user can bid, min seconds set before start if auction
       	/*if (!$this->canBid($formData['property_id'])) {
       		return ['Unable to place a bid, time exceeded.'];
       	}*/

        $formData['user_id'] = $user->id;
       // echo '<pre>'; print_r($formData); die('her');
        $model = new RegisterBid(['scenario' => 'Savebid']);
        $model->load($formData, '');
        $profile = Profiles::findOne(['user_id'=>$user->id]);
        if (empty($profile)) {
        	$profile = new Profiles();
        }
        $profile->scenario = 'savebid';
        $profile->load($formData, '');
       	//print_r($profile);die;
        if ($model->validate() && $profile->validate()) {
        	if (!empty(Yii::$app->request->post()['mediacare_number'])) {
	           	$medicare_doc = Yii::$app->request->post('mediacare_number');		
				$data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $medicare_doc));
				$imageName='mediacare_number'.rand(10,1000). ".jpg";
				$imagePath1=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['profileUploadDocPath'].$imageName);
				$profile->mediacare_number=$imageName;
				if (!empty($data1)) {
			 		file_put_contents($imagePath1, $data1);
				}
			}
		
			if(!empty(Yii::$app->request->post()['recent_utility_bill'])){
	            $utilitybill_doc = Yii::$app->request->post('recent_utility_bill');		
				$data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $utilitybill_doc));
				$imageName='UtilityBill_Number'.rand(10,1000). ".jpg";
				$imagePath2=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['profileUploadDocPath'].$imageName);
		        $profile->recent_utility_bill=$imageName;
		        if (!empty($data2)) {
			 	 	file_put_contents($imagePath2, $data2);
				}
			}
			$data_verify = array('passport_number'=>0,'driver_license_number'=>0,'mediacare_number'=>0,'recent_utility_bill'=>0);
			if ($model->save(false)) {
				if ($profile->save(false)) {
					$verification = new AccountVerification();
					$verification->user_id = $model->user_id;
					$verification->listing_id = $model->property_id;
					$verification->type = 1;
					$verification->data = json_encode($data_verify);
					$verification->save(false);
					$applyBid = ApplyBid::findOne(['user_id'=>$user->id,'listing_id'=>$model->property_id]);
			        if (empty($applyBid)) {
			        	$applyBid = new ApplyBid();
			        	$applyBid->is_favourite = 0;
			        	$applyBid->status = 0;
			        }
			        $applyBid->user_id = $user->id;
			        $applyBid->listing_id = $model->property_id;
			        $applyBid->bid_approved = 0;
			        $applyBid->save(false);
			        
			        parent::processCallback($formData);

			        return $model;
				}
			}
        } else {
        	if (!$model->validate()) {
        		return $model;
        	}
        	if (!$profile->validate()) {
        		return $profile;
        	}
        }
	}

	public function canBid($listing_id)
	{
		$config = new CviewConfig();
		$config_obj = $config->getConfigObj($config->configs['bid_min_time']);
		$seconds_before_bid = 0;
		if ($config_obj->value) {
			$seconds_before_bid = $config_obj->value;
		}
		$auction = Auction::findOne(['listing_id' => $listing_id]);
		$current_time = time();
		if (strtotime($auction->datetime_start) - $current_time < intval($seconds_before_bid)) {
			return false;
		}
		return true;
	}	
}


