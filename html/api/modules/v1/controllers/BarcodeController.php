<?php

namespace api\modules\v1\controllers;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\components\Controller;
use common\models\Qr;
use dosamigos\qrcode\QrCode;
//use api\components\Controller;
/**
 * Qr Controller API
 *
 
 */
class BarcodeController extends Controller
{
    public $modelClass = 'common\models\Qr';    

   /*
public function behaviors()
{
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
        'class' => CompositeAuth::className(),
        'authMethods' => [
            HttpBasicAuth::className(),
            HttpBearerAuth::className(),
            QueryParamAuth::className(),
        ],
    ];
    return $behaviors;
}*/
 
    public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'],$actions['index'], $actions['update'],$actions['search'],$actions['delete'],$actions['view']);                    

		return $actions;
	}
    
    /* Create Bar Code image */
    
    public function actionIndex($id=null)
    {
		
		if($id)
		{	
			Yii::$app->barcode->drawBars($id);							
			die;				
		}
	 
    }
 }


