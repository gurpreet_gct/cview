<?php

namespace api\modules\v1\controllers;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;

use common\models\User;
use common\models\Deal;
use common\models\PolygonsRegion;
use common\models\Workorderpopup;
use common\models\Workorders;
use common\components\PushNotification;

use yii\db\Expression;
use yii\helpers\ArrayHelper;

class DealnotificationController extends ActiveController
{
	public $modelClass = 'backend\models\PolygonsRegion';
	
	public function actions(){
		
		$actions = parent::actions();

		// disable the "create" and "update" actions
		
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);                     

		return $actions;
		
	}
	
	public function behaviors(){
		
		$behaviors = parent::behaviors();
				
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		
		return $behaviors;
		
	}
	
	/*
	 * Create and Update notification Setting for users
	 * 
	*/
	 
	public function actionCreate(){
		
		$user_id 			= Yii::$app->user->id;  	
		
		$PolygonsRegion 	= new PolygonsRegion();
		$Workorderpopup 	= new Workorderpopup();
		$PushNotification 	= new PushNotification();
		
		if($PolygonsRegion->load(Yii::$app->request->getBodyParams(),'')){
			
			$postedData 		= Yii::$app->request->getBodyParams();
			
			$latitude 		= isset($postedData['latitude'])?$postedData['latitude']:'';
			$longitude 		= isset($postedData['longitude'])?$postedData['longitude']:'';
			
			if(!empty($latitude) && !empty($longitude)){
				
				// check in which rectangle point is found
				
				$reslt = $PolygonsRegion::find()
							->select(['deal_id'])
							->where('contains(polygon_shape, POINT('.$latitude.','.$longitude.'))')
							->asArray()
							->one();
				
								
				if(!empty($reslt)){
					
					// get deal notificaion message
						
					$Workordpopup = $Workorderpopup::find()
										->select([Workorderpopup::tableName().'.iphoneNotificationText'])
										->leftJoin('{{%workorders}}', Workorders::tableName().'.id = '.Workorderpopup::tableName().'.workorder_id')
										->where(Workorders::tableName().'.status=1')
										->andWhere(['workorder_id'=>$reslt['deal_id']])
										->asArray()
										->one();
										
					
					$PushNotification->send();
					
					// return 	$Workordpopup;
					
					
				}else{
					
					$this->send('No notification message found.');
					
					// return ['iphoneNotificationText'=>'No notification message found.'];
				}			
				
			}else{
				
				$this->send('Not a valid lat/long.');
				
				// return ['iphoneNotificationText'=>'Not a valid lat/long.'];
				
			}
			
			
		}
		
	}
	
	
	
	
	
}