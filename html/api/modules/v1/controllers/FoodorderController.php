<?php
 
namespace api\modules\v1\controllers;
 
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;
use yii\db\Query;
use yii\db\Expression;
use api\modules\v1\models\Foodordersapi;
use common\models\FoodordersItem;
use common\models\FoodordersSubItem;
use common\models\Product;
use common\models\User;
use common\models\FoodorderFinal;
use common\models\Store;
use common\models\Profile;
use common\models\Order;
use common\models\OrderItem;
use common\models\FoodorderOptions;
use dosamigos\qrcode\QrCode;
use common\models\ProductOption;
 
class FoodorderController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Foodordersapi';    
 
   
       
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
   
   public function behaviors()
   {
       $behaviors = parent::behaviors();
       $behaviors['authenticator'] = [
           'class' => CompositeAuth::className(),
           'authMethods' => [
               HttpBasicAuth::className(),
               HttpBearerAuth::className(),
               QueryParamAuth::className(),
           ],
       ];
       return $behaviors;
   } 
   
     public function actions()
   {
       $actions = parent::actions();
 
       // disable the "delete" and "update" actions
       unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);                    
 
       return $actions;
   }
 /* get user order detail by user id or order id */        
public function actionIndex(){	
		$userId = Yii::$app->user->identity->id;
		$brand = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/profile/').'/';
		$image = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/uploads/catalog/').'/';
		$thumb = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/uploads/catalog/thumb').'/';
		$curl  = Yii::$app->urlManagerApi->createAbsoluteUrl("/v1/barcodes?id=");
		$ord   = Foodordersapi::tableName();
		$pro   = Profile::tableName();
		$fi    = FoodordersItem::tableName();
		$p     = Product::tableName();
		$po    = ProductOption::tableName();
		$fdo   = FoodorderOptions::tableName();
           if(!empty($_REQUEST['orderid'])){            
           /* get main products order */
			$orderItem = Foodordersapi::find()->select(["$ord.id as order_id","$ord.store_id","$ord.qty","$ord.grand_total","$ord.tax_amount","$ord.discount_amount as discount","$ord.status","FROM_UNIXTIME('$ord.created_at') as created_at","$pro.companyName as brand","CONCAT('$brand',$pro.brandLogo) as logo","CONCAT('$brand',$pro.backgroundHexColour) as backgroundHexColour","CONCAT('$curl',$ord.id,'-',$ord.grand_total) as qrurl"])
			->join('INNER JOIN',$pro,"$pro.user_id=$ord.store_id")
			->where("$ord.id=".$_REQUEST['orderid'])
			->asArray()
			->one();			
			 if($orderItem) {		
				    /* order item details */
			$product = FoodordersItem::find()
						   ->select(["$fi.product_id","$fi.pname as name","$fi.qty","$fi.grand_total","$fi.item_total_price","$fi.sub_item_total_price","$fi.have_child","CONCAT('$image',$p.image) as image","CONCAT('$thumb',$p.image) as thumb","$p.description","$p.related_product","$p.price","$p.p_type","$p.has_option","$p.sku","$pro.companyName as brandName","CONCAT('brand',$pro.brandLogo) as logo","CONCAT('$brand',$pro.backgroundHexColour) as backgroundHexColour","$pro.brandDescription"])
						   ->join('INNER JOIN',$p,"$fi.product_id=$p.id")
						   ->join('INNER JOIN',$pro,"$p.store_id=$pro.user_id")
						   ->where("$fi.order_id=".$orderItem['order_id'])
						   ->asArray()
						   ->all();
			
			if($product){
		   $productArr = array();		
			foreach($product as $_product){				
			  /* show sub items */ 			
			  $childWrap = array();
			  if(isset($product['related_product'])){
					$ids = explode(',',$_product['related_product']);
					$subProduct = Product::find()->select(["$p.id as product_id","$p.p_type","$p.name","$p.sku","$p.store_id","$p.description","$p.urlkey","$p.price","$p.status","$p.is_online","CONCAT('$image',$p.image) as image","CONCAT('$thumb',$p.image) as thumb","$p.related_product",new Expression('0 as is_added'),"$p.qty"])
					->where(['IN', 'id',$ids])
					->asArray()
					->all();				
					 foreach($subProduct as $_childProduct){
						$added = FoodordersSubItem::find()->select('product_id')->where('product_id='.$_childProduct['product_id'].' AND order_id='.$_REQUEST['orderid'])->one();    
						$isAdde = !empty($added->product_id)?1:0;
						unset($_childProduct['is_added']);
						$_childProduct['is_added'] = $isAdde;
						$childWrap[]	= 	$_childProduct;			
												
			   } //	endforeach($subProduct as $_childProduct)
					
			} //	end if(!isset($product['related_product']))
			$pOptions = array();
			if($_product['has_option']==1){
				$pOptions2 = ProductOption::find()
					->select(["$po.id","$po.product_id","$po.title","$po.price","$po.is_included as is_added","$po.is_included",new Expression('0 as qty')])
					->where("$po.product_id=".$_product['product_id'])
					->orderBy('is_included DESC')
					->asArray()
					->all();	
				foreach($pOptions2 as $_pOptions){
						$added = FoodorderOptions::find()->select('product_id,qty')->where('product_id='.$_pOptions['id'].' AND order_id='.$_REQUEST['orderid'])->one(); 
						$isAdde = !empty($added->product_id)?1:0;
						$isAddeqty = !empty($added->qty)?$added->qty:0;
						unset($_pOptions['is_added']);
						unset($_pOptions['qty']);
						$_pOptions['is_added'] = $isAdde;
						$_pOptions['qty'] = $isAddeqty ;
						$pOptions[]	= 	$_pOptions;	
										
				} 
			} //end	if($product['has_option']==1)	
			$_product['child'] = $childWrap;
			$_product['options']=$pOptions;	
			$productArr[] = $_product;
		} // endforeach($product as $_product){		
		} // end  if($product){	
			$orderItem['item'] = $productArr;				
		    return $orderItem;
		} // end if order 
		else{
			 return [
                   'statusCode'=>'200',
                   'data'=>'',
                   'message'=>'Order not found',
               ];
		}
       }
       else if(!empty($userId) && empty($_REQUEST['orderid']) && !empty($_REQUEST['store_id'])){		  
			$status =  isset($_REQUEST['status'])?$_REQUEST['status']:0;
			 
		    /* get main  order */
			if($status==2){
             $orderItem = sprintf("SELECT o.id as order_id,o.store_id,o.qty,o.grand_total,o.tax_amount,o.discount_amount,o.status,FROM_UNIXTIME(o.created_at) as created_at from %s o where o.user_id =%s and o.store_id =%s and o.status=%s;",Foodordersapi::tableName(),$userId,$_REQUEST['store_id'],$status); 
			}else{
				 $orderItem = sprintf("SELECT o.id as order_id,o.store_id,o.qty,o.grand_total,o.tax_amount,o.discount_amount,o.status,FROM_UNIXTIME(o.created_at) as created_at from %s o where o.user_id =%s and o.store_id =%s and o.status IN (0,1);",Foodordersapi::tableName(),$userId,$_REQUEST['store_id']); 
			}
             $order =Yii::$app->db->createCommand($orderItem)->queryAll();
             $orderData = array(); 
			 $itemsname2 = '';			 
			 $subItemsname2 = '';			 
			 if($order) {	
			  $oData = array();
               foreach($order as $_order)
			   {  
				  $items =FoodordersItem::find()->select('pname')->where('order_id ='.$_order['order_id'])->asArray()->all(); 				 			   
				if($items){
					$itemsname = array();
					 foreach($items as $_items){
						$itemsname[] = $_items['pname'];
					 }
					$itemsname2 = implode(',',$itemsname);
				}
				 $oData['order_id']=$_order['order_id'];
				 $oData['store_id']=$_order['store_id'];
				 $oData['qty']=$_order['qty'];
				 $oData['tax_amount']=$_order['tax_amount'];
				 $oData['grand_total']=$_order['grand_total'];
				 $oData['discount']=$_order['discount_amount'];
				 $oData['status']=$_order['status'];				
				 $oData['menuitem'] = $itemsname2 ;
				 $oData['created_at']=$_order['created_at'];
				 $orderData[] = $oData;
             } 
		    return array('item'=>$orderData);
		} // end if order 
		else{
			 return [
                    'statusCode'=>'200',
                    'data'=>'',
                   'message'=>'Order not found',
               ];
		}
           
       }
   }  
/* create a new order */    
   public function actionCreate()
   {    
	   $model = new Foodordersapi();    
	   $model->scenario = 'create';
	   $createdAt = time();
	   $postData = Yii::$app->getRequest()->getBodyParams();
	   $model->load($postData, '');  	  
		   if ($model->validate() )  {    
			$userId = Yii::$app->user->identity->id;			
			$data = json_decode($postData['item'],true); 		 		   
			$obj =  $data['data'];
			$objectArray = $obj['item'];
			$mainProduct=[];
			$childProduct = array();
			$productOption = array();
			$logo = '';
			$brandName = '';
			if(is_array($objectArray))
			{
			 /* save the order */
				$model->user_id                 = $userId;
				$model->grand_total           	= $postData['grand_total'];
				$model->tax_amount              = $postData['tax_amount'];            
				$model->status                  = 0;
				$model->sub_item_total_price    = 0;
				$model->item_total_price        = 0;
				$model->is_offline              = 0;            
				$model->qty                     = $postData['qty'];
				$model->discount_amount         = $postData['discount'];
				$model->store_id                = $postData['store_id'];
				$model->created_at              = $createdAt;
				$model->updated_at              = $createdAt;
			    $model->save(false);
			 /* end save order code */
			 	foreach($objectArray as $_data)
				{
					
					$haveChild = 0;
					if(isset($_data['child'])){				
						foreach($_data['child'] as $_child)
						{
							if($_child['is_added']==1)
							{
								array_push($childProduct,array(
									"order_id"=>$model->id,
									"product_id"=>$_child['product_id'],					
									"child_pname"=>$_child['name'],
									"parent_id"=>$_data['product_id'],
									"price"=>$_child['price'],
									"qty"=>$_child['qty'],
									"created_at"=>$createdAt,
									"updated_at"=>$createdAt
								));
									$haveChild = 1;
							}
						}				  
					}
					$haveOption = 0;
					if(isset($_data['options'])){				
						foreach($_data['options'] as $_options)
						{
							
							if($_options['is_added']==1)
							{
								array_push($productOption,array(
									"order_id"=>$model->id,
									"product_id"=>$_options['id'],					
									"title"=>$_options['title'],
									"price"=>$_options['price'],						
									"qty"=>$_options['qty']								
								));
									$haveOption = 1;
							}
						}				  
					}
					array_push($mainProduct,array(
						"order_id"=>$model->id,
						"product_id"=>$_data['product_id'],					
						"pname"=>$_data['name'],
						"have_child"=>$haveChild,
						"have_option"=>$haveOption,
						"price"=>$_data['price'],
						"qty"=>$_data['qty'],
						"p_type"=>$_data['p_type'],
						"grand_total"=>$_data['grand_total'],
						"sub_item_total_price"=>$_data['sub_item_total_price'],
						"item_total_price"=>$_data['item_total_price'],						
						"created_at"=>$createdAt,
						"updated_at"=>$createdAt
					));
					$logo = isset($_data['logo']) ? $_data['logo']:'';
					$brandName = isset($_data['brand'])?$_data['brand']:'';
				}	 // end foreach 
			}// end if
			
		    /* save order items ****************/
			if(count($mainProduct))
		   {
			   	Yii::$app->db->createCommand()->batchInsert(FoodordersItem::tableName(),array_keys($mainProduct[0]), $mainProduct)->execute();
		   }
		   /* save order sub items ****************/
		   if(count($childProduct))
		   {
			   	Yii::$app->db->createCommand()->batchInsert(FoodordersSubItem::tableName(),array_keys($childProduct[0]), $childProduct)->execute();
		   } 
		   /* save items options ****************/
		   if(count($productOption))
		   {
			   	Yii::$app->db->createCommand()->batchInsert(FoodorderOptions::tableName(),array_keys($productOption[0]), $productOption)->execute();
		   }
		    if($model->id)
           {
			    return [
					   'statusCode'=>'200',
					   'order_id'=>$model->id,
					   'store_id'=>$model->store_id,
					   'grand_total'=>$model->grand_total,
					   'tax_amount'=>$model->tax_amount,
					   'discount'=>$model->discount_amount,
					   'qty'=>$model->qty,
					   'logo'=>$logo,
					   'brandName'=>$brandName,
					   'qrurl'=>Yii::$app->urlManagerApi->createAbsoluteUrl("/v1/barcodes?id=".$model->id. "-".$model->grand_total),
					   'item'=>$objectArray,
					   'message'=>'Your order saved successfully.',
				   ];
			   }else
			   {
				   return [
					   'statusCode'=>'500',
					   'message'=>'Your order not saved',
				   ];
           }
       }else{
               return $model;
       }
       
   }

/* update order by order id */            
   public function actionUpdate($id){
	   $formData = Yii::$app->getRequest()->post();
       $model = new Foodordersapi();    
	   $model->scenario = 'update';
       $model->load($formData, '');  
      if ($model->validate())  {    
           /* update product item */
			$updatedAt = time();
			$order = $model::findOne($id);
			$order->grand_total           	= $formData['grand_total'];
			$order->tax_amount              = $formData['tax_amount']; 			
			$order->sub_item_total_price    = 0;
			$order->item_total_price        = 0;
			$order->qty                     = $formData['qty'];
			$order->discount_amount         = $formData['discount'];			
			$order->updated_at              = $updatedAt;
			if($order->save(false))
		   {				
			$data = json_decode($formData['item'],true); 		 		   
			$obj =  $data['data'];
			$objectArray = $obj['item'];
			$mainProduct=[];
			$childProduct = array();
			$productOption = array();
			$logo = '';
			$brandName = '';
			if(is_array($objectArray))
			{
			/* end update order code */
				foreach($objectArray as $_data)
				{
					#if($_data['is_added']==1)	{
						$haveChild = 0;
						if(isset($_data['child'])){				
							foreach($_data['child'] as $_child)
								{
								if($_child['is_added']==1)
								{
									array_push($childProduct,array(
									"order_id"=>$order->id,
									"product_id"=>$_child['product_id'],					
									"child_pname"=>$_child['name'],
									"parent_id"=>$_data['product_id'],
									"price"=>$_child['price'],
									"qty"=>$_child['qty'],
									"created_at"=>$order->created_at,
									"updated_at"=>$updatedAt
									));
										$haveChild = 1;
									}
								}				  
							}
						$haveOption = 0;
						if(isset($_data['options'])){				
							foreach($_data['options'] as $_options)
							{
								
								if($_options['is_added']==1)
								{
									array_push($productOption,array(
										"order_id"=>$order->id,
										"product_id"=>$_options['id'],					
										"title"=>$_options['title'],
										"price"=>$_options['price'],						
										"qty"=>$_options['qty']								
									));
										$haveOption = 1;
								}
							}				  
						}	
						array_push($mainProduct,array(
						"order_id"=>$order->id,
						"product_id"=>$_data['product_id'],					
						"pname"=>$_data['name'],
						"have_child"=>$haveChild,
						"have_option"=>$haveOption,
						"price"=>$_data['price'],
						"qty"=>$_data['qty'],
						"p_type"=>$_data['p_type'],
						"grand_total"=>$_data['grand_total'],
						"sub_item_total_price"=>$_data['sub_item_total_price'],
						"item_total_price"=>$_data['item_total_price'],			
						"created_at"=>$order->created_at,
						"updated_at"=>$updatedAt
					));
							$logo = isset($_data['logo']) ? $_data['logo']:'';
							$brandName = isset($_data['brand'])?$_data['brand']:'';
						#} // if is added 
					}	 // end foreach 
				}// end if				
				/* update order items ****************/
				if(count($mainProduct))
				{
					FoodordersItem::deleteAll('order_id ='.$order->id);	 
					FoodordersSubItem::deleteAll('order_id ='.$order->id);
					FoodorderOptions::deleteAll('order_id ='.$order->id);
					Yii::$app->db->createCommand()->batchInsert(FoodordersItem::tableName(),array_keys($mainProduct[0]), $mainProduct)->execute();
				}
				/* update order sub items ****************/
				if(count($childProduct))
				{
					
					Yii::$app->db->createCommand()->batchInsert(FoodordersSubItem::tableName(),array_keys($childProduct[0]), $childProduct)->execute();
				}
			/* update items options ****************/
			   if(count($productOption))
			   {
					Yii::$app->db->createCommand()->batchInsert(FoodorderOptions::tableName(),array_keys($productOption[0]), $productOption)->execute();
			   }				
				return [
				   'statusCode'=>'200',
				   'order_id'=>$order->id,
				   'store_id'=>$order->store_id,
				   'grand_total'=>$order->grand_total,
				   'tax_amount'=>$order->tax_amount,
				   'discount'=>$order->discount_amount,
				   'qty'=>$order->qty,
				   'logo'=>$logo,
				   'brandName'=>$brandName,
				   'qrurl'=>Yii::$app->urlManagerApi->createAbsoluteUrl("/v1/barcodes?id=".$order->id. "-".$order->grand_total),
				   'item'=>$objectArray,
				   'message'=>'Your order has been updated successfully.',
			   ];
		   }
		   else{
               return [
                   'statusCode'=>'500',
                   'message'=>'Your order not updated',
               ];
           }
       }
       else{
               return $model;
       }
   }
/* finalize order for payment */    
   public function actionFinalize(){
       $model = new Foodordersapi();    
	   $model->scenario = 'finalize';
	   $model->user_id = Yii::$app->user->identity->id;
       $model->load(Yii::$app->getRequest()->getBodyParams(), '');  
	      if ($model->validate())  {  
           $userId = Yii::$app->user->identity->id;
           $dataArray     = Yii::$app->getRequest()->getBodyParams();
           $orderId     = $dataArray['order_id'];
           $order = Foodordersapi::find()->select('store_id')->where('id='.$orderId)->one();
           if(isset($order->store_id)){
			$orderFood = Foodordersapi::findOne($orderId); 
			$orderFood->status = 1;
			$orderFood->save(false);						
			$store = Store::find()->select('storename,latitude,longitude,geo_radius')
							->where('owner='.$order->store_id)->asArray()->all();
			$valid = 0;
			
		    $res = $this->distance($store, $dataArray['lat'], $dataArray['lang'],"K");
		    $radious = isset($res['geo_radius']) ? $res['geo_radius'] : $this->partnerradious($order->store_id);
		    $res['geo_radius'] = $radious ;
			if($res && isset($res['distancekm']) && isset($res['geo_radius']) &&$res['distancekm'] <= $res['geo_radius'])
			{
				
				$valid = 1;
			}
            if($valid==1){
				$distance =  $res['geo_radius'];
				$name =  $res['storename'];
               return [
                   'statusCode'=>200,
                   'message'=>"You are within $distance km of $name. Would you like to pay and send your order to the kitchen?"
               ];
           }else{
           
               return [
                   'statusCode'=>422,
                   'message'=>'Thanks for your order. You will get a message when you are close to the restaurant. You can then pay for your order and the kitchen will make your food.',
                   ];
                   
               }
           }
         }
         else{
             return $model;
         }
   }
   /* get ditance between two lat and lang */
   public function distance($store, $lat2, $lon2, $unit) {
	 $storeDis = array();  
	foreach($store as $_store)  {
		 $theta = $_store['longitude'] - $lon2;
		 $dist = sin(deg2rad($_store['latitude'])) * sin(deg2rad($lat2)) +  cos(deg2rad($_store['latitude'])) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		 $dist = acos($dist);
		 $dist = rad2deg($dist);
		 $miles = $dist * 60 * 1.1515;
		 $unit = strtoupper($unit);
		 
		 $storeData = array();
		 $storeData['distancekm']= ($miles * 1.609344);
		 $storeData['storename']= $_store['storename'];
		 $storeData['geo_radius']= $_store['geo_radius'];
		 $storeDis[] = $storeData;
	}
	usort($storeDis, function($a, $b) {
		return $a['distancekm'] - $b['distancekm'];
	});
	$data = isset($storeDis[0]) ? $storeDis[0]:'';
	return  $data;
   /*  if ($unit == "K") {
       return ($miles * 1.609344);
     }else if($unit == "M"){
           return ($miles * 1.609344*1000);
     }
     else if ($unit == "N") {
         return ($miles * 0.8684);
       } else {
           return $miles;
         } */
   }
public function partnerradious($id)
{
		$user = Profile::find()->select('geo_radius')->where('user_id='.$id)->asArray()->one();
		return $user['geo_radius'];
}

  public function actionAllorder(){	   
			$fo  = Foodordersapi::tableName();
			$pro = Profile::tableName();
			$ord = Order::tableName();
			$oi  = OrderItem::tableName();
			$p   = Product::tableName();
			$foi   = FoodordersItem::tableName();
			$userId = Yii::$app->user->identity->id;
		/* pending food order */		
			$brand = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/profile').'/';		
			$order = Foodordersapi::find()
						 ->select(["$fo.id  as food_order_id","$fo.store_id as storeID","$fo.qty","$fo.grand_total as totalPrice","$fo.tax_amount","$fo.discount_amount as discount","$fo.status","FROM_UNIXTIME($fo.created_at) as created_at","$pro.companyName as storeName","CONCAT('$brand',$pro.brandLogo) as storeImage",new Expression('1 as order_type'),"$foi.pname as menuitem"])
						->join('INNER JOIN',$foi,"$fo.id=$foi.order_id")
						 ->join('INNER JOIN',$pro,"$fo.store_id=$pro.user_id")
						 ->where("$fo.user_id=".$userId)
						 ->andWhere(['IN',"$fo.status",[0,1,3]])
						 ->orderBy("$fo.id DESC")
						 ->groupBy("$fo.id")
						 ->asArray()
						 ->all();
			/*Order  To be Collected */				
			$orderItem2 = Order::find()
						->select(["$ord.id as order_id","$ord.qty_ordered as qty","$ord.subtotal as totalPrice","$ord.order_type","$ord.tax_amount","$ord.status","FROM_UNIXTIME($ord.created_at) as created_at","$oi.product_id","$oi.store_id as storeID","$p.name,$pro.companyName as storeName","CONCAT('$brand',$pro.brandLogo) as storeImage","$ord.food_oid as food_order_id"])
						 ->join('INNER JOIN',$oi,"$ord.id=$oi.order_id")
						 ->join('INNER JOIN',$p,"$oi.product_id=$p.id")
						 ->join('INNER JOIN',$pro,"$oi.store_id=$pro.user_id")
						 ->where("$ord.user_id=".$userId)
						->andWhere(['IN',"$ord.status",[2,6,7]])
						 ->orderBy("$ord.id DESC")
						 ->groupBy("$ord.id")
						 ->asArray()
						 ->all();
		/* Order History  */				 
		 $orderItem3 = Order::find()
						->select(["$ord.id as order_id","$ord.qty_ordered as qty","$ord.subtotal as totalPrice","$ord.order_type","$ord.tax_amount","$ord.status","FROM_UNIXTIME($ord.created_at) as created_at","$oi.product_id","$oi.store_id as storeID","$p.name,$pro.companyName as storeName","CONCAT('$brand',$pro.brandLogo) as storeImage","$ord.food_oid as food_order_id"])
						 ->join('INNER JOIN',$oi,"$ord.id=$oi.order_id")
						 ->join('INNER JOIN',$p,"$oi.product_id=$p.id")
						 ->join('INNER JOIN',$pro,"$oi.store_id=$pro.user_id")
						 ->where("$ord.user_id=".$userId)
						 ->andWhere(["$ord.status"=>4])
						 ->orderBy("$ord.id DESC")
						 ->groupBy("$ord.id")
						 ->asArray()
						 ->all();	
		
		/* end *************/	            
		 if(isset($order) || isset($orderItem2) || isset($orderItem3)) {	
			  		 
			return array('item'=>array('pendingFoodOrder'=>$order,'OrderTobeCollected'=>$orderItem2,'completedOrder'=> $orderItem3));
		} // end if order 
		else{
			 return [
                    'statusCode'=>'200',
                    'data'=>'',
                   'message'=>'Order not found',
               ];
		}
   }
   public function actionFoodwallet(){
			$fo  = Foodordersapi::tableName();
			$pro = Profile::tableName();
			$p   = Product::tableName();
			$userId = Yii::$app->user->identity->id;
			/* pending food order */		
			$brand = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/profile').'/';		
			$order = Foodordersapi::find()
						 ->select(["$fo.id as order_id","$fo.store_id as storeID","$fo.qty","$fo.grand_total","$fo.tax_amount","$fo.discount_amount as discount","$fo.status","FROM_UNIXTIME($fo.created_at) as created_at","$pro.companyName as storeName","CONCAT('$brand',$pro.brandLogo) as storeImage",new Expression('1 as order_type')])
						 ->joinWith('profile',false,'INNER JOIN')
						 ->where("$fo.user_id=".$userId)
						 ->andWhere("$fo.status=3")
						 ->asArray()
						 ->all();
			if(isset($order)){
				 $orderData = array(); 
				foreach($order as $_order)
				{  
					$items =FoodordersItem::find()->select('pname')->where('order_id ='.$_order['order_id'])->asArray()->all(); 				 			   
					if($items){
					$itemsname = array();
					foreach($items as $_items){
						$itemsname[] = $_items['pname'];
					}
						$itemsname2 = implode(',',$itemsname);
					}				 				
					$_order['menuitem'] = $itemsname2 ;				
					$orderData[] = $_order;
				}
				return array('item'=>array('foodorder'=>$orderData));
			}
			else{
			 return [
                    'statusCode'=>'200',
                    'data'=>'',
                   'message'=>'Order not found',
               ];
			}			
   }
    public function actionDelete($id)
   {
	
	 $foodorder = Foodordersapi::findOne($id);
	 if($foodorder){
		$foodorder->delete();
		FoodordersItem::deleteAll('order_id ='.$id);
		FoodordersSubItem::deleteAll('order_id ='.$id);
		FoodorderOptions::deleteAll('order_id ='.$id);
		
		return ['message'=> 'success', 'statusCode' => 204];
	
	 }else{
		return ['message'=> 'No result found', 'statusCode' => 404]; 
	 }
	 
   }
  /* get resturent orders for employee users */
   
   public function actionResturentOrder(){	   
			$fo  = Foodordersapi::tableName();
			$pro = Profile::tableName();
			$usr = User::tableName();
			$ord = Order::tableName();
			$oi  = OrderItem::tableName();
			$p   = Product::tableName();
			$foi   = FoodordersItem::tableName();
			$storeId = Yii::$app->user->identity->advertister_id;
			if(Yii::$app->user->identity->user_type!=9){
				 return [
                    'statusCode'=>'404',
                    'data'=>'',
                   'message'=>'Not a valid user',
				];
				}
			/* pending food order */		
			$brand = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/profile').'/';		
			$order = Order::find()
						->select(["$ord.id as order_id","$usr.username","$usr.id as userId","$ord.qty_ordered as qty","$ord.subtotal as totalPrice","$ord.order_type","$ord.tax_amount","$ord.discount_amount as discount","$ord.status","FROM_UNIXTIME($ord.created_at) as created_at","$oi.product_id","$oi.store_id as storeID","$p.name,$pro.companyName as storeName","CONCAT('$brand',$pro.brandLogo) as storeImage","$ord.food_oid as food_order_id"])
						 ->join('INNER JOIN',$oi,"$ord.id=$oi.order_id")
						 ->join('INNER JOIN',$p,"$oi.product_id=$p.id")
						 ->join('INNER JOIN',$pro,"$oi.store_id=$pro.user_id")
						 ->join('INNER JOIN',$usr,"$ord.user_id=$usr.id")
						 ->where("$oi.store_id=".$storeId)
						 ->andWhere(["$ord.status"=>2])
						 ->andWhere(["$ord.order_type"=>1])
						 ->orderBy("$ord.id DESC")
						 ->groupBy("$ord.id")
						 ->asArray()
						 ->all();	
			/*Order  To be Collected */				
			$orderItem2 = Order::find()
						->select(["$ord.id as order_id","$usr.username","$usr.id as userId","$ord.qty_ordered as qty","$ord.subtotal as totalPrice","$ord.order_type","$ord.tax_amount","$ord.status","$ord.discount_amount as discount","FROM_UNIXTIME($ord.created_at) as created_at","$oi.product_id","$oi.store_id as storeID","$p.name,$pro.companyName as storeName","CONCAT('$brand',$pro.brandLogo) as storeImage","$ord.food_oid as food_order_id"])
						 ->join('INNER JOIN',$oi,"$ord.id=$oi.order_id")
						 ->join('INNER JOIN',$p,"$oi.product_id=$p.id")
						 ->join('INNER JOIN',$pro,"$oi.store_id=$pro.user_id")
						 ->join('INNER JOIN',$usr,"$ord.user_id=$usr.id")
						 ->where("$oi.store_id=".$storeId)
						 ->andWhere(["$ord.status"=>6])
						 ->andWhere(["$ord.order_type"=>1])
						 ->orderBy("$ord.id DESC")
						 ->groupBy("$ord.id")
						 ->asArray()
						 ->all();
		/* Order History  */				 
		 $orderItem3 = Order::find()
						->select(["$ord.id as order_id","$usr.username","$usr.id as userId","$ord.qty_ordered as qty","$ord.subtotal as totalPrice","$ord.order_type","$ord.tax_amount","$ord.status","$ord.discount_amount as discount","FROM_UNIXTIME($ord.created_at) as created_at","$oi.product_id","$oi.store_id as storeID","$p.name,$pro.companyName as storeName","CONCAT('$brand',$pro.brandLogo) as storeImage","$ord.food_oid as food_order_id"])
						 ->join('INNER JOIN',$oi,"$ord.id=$oi.order_id")
						 ->join('INNER JOIN',$p,"$oi.product_id=$p.id")
						 ->join('INNER JOIN',$pro,"$oi.store_id=$pro.user_id")
						 ->join('INNER JOIN',$usr,"$ord.user_id=$usr.id")
						 ->where("$oi.store_id=".$storeId)
						 ->andWhere(["$ord.status"=>4])
						 ->andWhere(["$ord.order_type"=>1])
						 ->orderBy("$ord.id DESC")
						 ->groupBy("$ord.id")
						 ->asArray()
						 ->all();	
		
		/* end *************/	            
		 if(isset($order) || isset($orderItem2) || isset($orderItem3)) {	
			  		 
			return array('item'=>array('ordertobeconfirmed'=>$order,'activeorder'=>$orderItem2,'completedorder'=> $orderItem3));
		} // end if order 
		else{
			 return [
                    'statusCode'=>'200',
                    'data'=>'',
                   'message'=>'Order not found',
               ];
		}
   }
}

