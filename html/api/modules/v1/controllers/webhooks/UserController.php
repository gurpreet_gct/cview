<?php
namespace api\modules\v1\controllers\webhooks;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\LoginLogs;
use common\models\User;
use common\models\Usertype;
use common\models\ChangePassword;
use common\models\Foodnotification;
use common\models\Property;
use api\modules\v1\models\Customer;
use api\modules\v1\models\Bidder;
use api\modules\v1\models\Agent;
use api\modules\v1\models\Profiles;
use api\modules\v1\models\ForgotPassword;
use api\modules\v1\models\FacebookLogin;
use api\modules\v1\models\LinkedinLogin;
use api\modules\v1\models\GoogleLogin;
use api\modules\v1\models\Login;
use api\components\Controller;
use api\modules\v1\models\Foodordersapi;
use api\modules\v1\models\Agency;
use api\modules\v1\controllers\EmailController;
use common\models\CommonModel;

/**
 * UserController
 *
 * Login|Registration and other user management methods for APIs are defined here.
 */
class UserController extends Controller
{
    const USER_TYPE_BIDDER = '2';

    public $modelClass = 'api\modules\v1\models\Customer';
    //public $modelClass = 'backend\models\Usermanagement';

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['subfees'],
            'only'=>[
                'add-agent',
                'update-agent',
                'delete-agent',
                'add-bidder',
                'update-bidder',
                'delete-bidder',
                'sync-agent-data',
                'sync-bidders-data',
            ],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "update" actions
        unset($actions['delete'], $actions['index'], $actions['update'],$actions['create']);

        return $actions;
    }
    
	/**
	 *
	 * @api {post} api/web/v1/webhooks/users/get-auth-key Get An Auth Key
	 * @apiName GetAnAuthKey
	 * @apiVersion 0.1.1
	 * @apiGroup CommercialView
	 * @apiDescription To Get An Auth Key
     * ### Notes:
     *  - `Form-Data must be x-www-form-urlencoded`
	 *
	 * @apiParam {String} username john.doe
	 * @apiParam {String} password password123
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
     *     {
     *       "message": "",
     *       "data": {
     *         "username": "john.doe",
     *         "email": "john_doe@example.com",
     *         "auth_key": "fW11CiNa4E5IhUmFRcdQttEXdXB8ee6u"
     *       },
     *       "status": 200
     *     }
     *     
	 * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable entity
     *     {
     *       "message": "",
     *       "data": [
     *         {
     *           "field": "username",
     *           "message": "Username in here!! cannot be blank."
     *         },
     *         {
     *           "field": "password",
     *           "message": "Password cannot be blank."
     *         },
     *       ],
     *       "status": 422
     *     }
     *     Or
     *     HTTP/1.1 422 Unprocessable entity
     *     {
     *       "message": "",
     *       "data": [
     *         {
     *           "field": "password",
     *           "message": "That Pin does not match the registered email address. Please try again or click on Forgot  Pin? to reset it."
     *         }
     *       ],
     *       "status": 422
     *     }
	 *   
	 */
    public function actionGetAuthKey()
    {
        $model = new Login();
        $model->isMobile=true;
        $posted = Yii::$app->request->post();
        $posted['device'] = 'android';
        $posted['app_id'] = 2;
        $data = array('Login' => $posted);
        $model->load($data);
        if(!$model->validate()){
            return $model;
        }
        if($model->login()) {
            $userModel = Customer::findOne(Yii::$app->user->identity->id);
            $userModel = $this->postLoginCheck($userModel, $data);
            if (!$userModel) {
                $model->validate(null,false);
                $userModel = $model;
                return [['notice' => 'Invalid credentials', 'user_data' => $userModel], 'statusCode'=>422];  
            }
        } else { //die('eh');
            $model->validate(null,false);
            $userModel = $model;
        }
        $data = [
            'username' => $userModel->username,
            'email' => $userModel->email,
            'auth_key' => $userModel->auth_key,
        ];
        return $data;
    }
    
    /**
     * Post login check for user type
     * Checks for the user type post login authentication.
     * @return User OBJ True|If login requested belonged to the adequate Bidder
     */
    public function postLoginCheck($authenticatedUser, $dataPassed)
    {   
        // Return the default authenticated user, when @param type not sent in POST.
        // $user_sub_type = Usertype::getBidderType();
        // Assuming normal customer login requested
        if (empty($dataPassed['Login']['app_id']) || (strtolower($dataPassed['Login']['app_id']) != self::USER_TYPE_BIDDER)) {
            // Check for the authenticated customer as non bidder, block login if user_type not bidder
            if ($authenticatedUser->app_id == self::USER_TYPE_BIDDER) {
                return false;
            }

            return $authenticatedUser;
        }
        // Check for the authenticated customer as bidder, allow login only if true
        if ($authenticatedUser->app_id !== self::USER_TYPE_BIDDER) {
            return false;
        }
        return $authenticatedUser;
    }    
    
    /* ** 
	 * 
	 *
	 * @api {post} api/web/v1/webhooks/user/all-agents Agent Listing
	 *
	 * @apiName ShowAgents
	 * @apiVersion 0.1.1
	 * @apiGroup AgentWebhooks
	 * @apiDescription To show agents, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 OK
     *    {
     *      "data": {
     *        "items": [
     *          {
     *            "agent_fname": "aastha",
     *            "agent_lname": "chawla",
     *            "agent_email": "aastha1@graycelltech.com",
     *            "agent_fullname": "aastha chawla",
     *            "auth_key": "41oBK-OHkauNIq5b8jyQlF4jcZPTsXT_",
     *            "agency_id": "10",
     *            "id": "10",
     *            "name": "Abercromby’s",
     *            "email": "aastha@gmail.com",
     *            "mobile": "1234569874",
     *            "logo": "Abercromby’s819.jpg",
     *            "backgroungImage": null,
     *            "address": "[\" sco 206 - 207, sector 34 a, chandigarh - 160022\",\"sco 72 a & 73 a sector 26 grain market chandigarh pin-160019\"]",
     *            "latitude": "[\"30.72589\",\"30.72267\"]",
     *            "longitude": "[\"76.75787\",\"76.79825\"]",
     *            "about_us": "Gross Waddell Pty Ltd is a\ncommercial real estate agency,\noperating for over 20 years in the\nretail, office, industrial and\ndevelopment property sectors. We\nprovide Sales and Leasing, Asset\nManagement\nand\nCorporate\nServices to Vendors, Landlords and\nInvestors.",
     *            "status": "10",
     *            "created_at": "1524034419",
     *            "updated_at": "1524034419"
     *          },
     *          {
     *            "agent_fname": "aastha",
     *            "agent_lname": "chawla",
     *            "agent_email": "aastha3@graycelltech.com",
     *            "agent_fullname": "aastha chawla",
     *            "auth_key": "ZWuX9J6HhhArfvQYWlNAdsUp678h87gN",
     *            "agency_id": "10",
     *            "id": "10",
     *            "name": "Abercromby’s",
     *            "email": "aastha@gmail.com",
     *            "mobile": "1234569874",
     *            "logo": "Abercromby’s819.jpg",
     *            "backgroungImage": null,
     *            "address": "[\" sco 206 - 207, sector 34 a, chandigarh - 160022\",\"sco 72 a & 73 a sector 26 grain market chandigarh pin-160019\"]",
     *            "latitude": "[\"30.72589\",\"30.72267\"]",
     *            "longitude": "[\"76.75787\",\"76.79825\"]",
     *            "about_us": "Gross Waddell Pty Ltd is a\ncommercial real estate agency,\noperating for over 20 years in the\nretail, office, industrial and\ndevelopment property sectors. We\nprovide Sales and Leasing, Asset\nManagement\nand\nCorporate\nServices to Vendors, Landlords and\nInvestors.",
     *            "status": "10",
     *            "created_at": "1524034419",
     *            "updated_at": "1524034419"
     *          },
     *          {
     *            "agent_fname": "aastha",
     *            "agent_lname": "chawla",
     *            "agent_email": "aastha341@graycelltech.com",
     *            "agent_fullname": "aastha chawla",
     *            "auth_key": "YB0wG3_xbkKIjnwy4-WU0C7tPAIubXSd",
     *            "agency_id": "23",
     *            "id": "23",
     *            "name": "aastha1",
     *            "email": "aastha1@gmail.cpm",
     *            "mobile": "919876543215",
     *            "logo": "aastha1601.jpg",
     *            "backgroungImage": null,
     *            "address": "[\"sco 206 - 207, sector 34 a, chandigarh - 160022\"]",
     *            "latitude": "[\"30.72589\"]",
     *            "longitude": "[\"76.75787\"]",
     *            "about_us": "Gross Waddell Pty Ltd is a\ncommercial real estate agency,\noperating for over 20 years in the\nretail, office, industrial and\ndevelopment property sectors. We\nprovide Sales and Leasing, Asset\nManagement\nand\nCorporate\nServices to Vendors, Landlords and\nInvestors.",
     *            "status": "10",
     *            "created_at": "1524059912",
     *            "updated_at": "1524059912"
     *          }
     *        ],
     *        "_links": {
     *            "self": {
     *                "href": "http://localhost/AlphaWallet/html/api/web/v1/webhooks/user/all-agents?page=1"
     *            }
     *        },
     *        "_meta": {
     *            "totalCount": 3,
     *            "pageCount": 1,
     *            "currentPage": 1,
     *            "perPage": 20
     *        }
     *      },
     *      "status": 200
     *    }
	 */
    public function actionAllAgents(){
		$query = new Query;
		//$model = Agent::find()->select([Agent::tableName().'.id','firstName','lastName',Agent::tableName().'.email',Agent::tableName().'.phone','fullName','auth_key','agency_id','tbl_agencies.logo'])->joinWith('agency')->where(['user_type'=>10,'app_id'=>2])->andWhere(['agency_id'=>$agency_id]);
		$res = $query->select(['firstName as agent_fname','lastName as agent_lname', Agent::tableName().'.email as agent_email','fullName as agent_fullname','auth_key','agency_id','tbl_agencies.*'])->from(Agent::tableName())->join("INNER JOIN", Agency::tableName(), sprintf('%s.id=%s.agency_id', Agency::tableName(),Agent::tableName()))->where(['user_type'=>10,'app_id'=>2]);
		$result = new ActiveDataProvider([
            'query' => $res,
        ]);
        return $result;
	}

	/* *
	 *
	 * Sync up Agents data
	 *
	 * @api {get} api/web/v1/webhooks/user/sync-agent-data Agents data Syncing
	 * @apiName SyncAgentData
	 * @apiVersion 0.1.1
	 * @apiGroup DataSyncingWebhooks
	 * @apiDescription Sync Up Agent data.
     * @apiDescription To Sync Up Agent data, Form-Data must be of raw type. Data will be posted in the below format by the cview team to sync with our database.
	 *
	 * @apiSuccessExample Request Data:
     *    {  
     *      "type":"agents",
     *      "data":[  
     *        {  
     *          "id":1,
     *          "attributes":{  
     *            "email":"admin@commercialview.com.au",
     *            "encrypted_password":"$2a$10$2zpMns14VZxxi.EjILSRcuMWkQ0.OJyHEVILL6a7FBYYaGaBmJl62",
     *            "first_name":"Donald",
     *            "last_name":"Trump",
     *            "mobile":"0400123456",
     *            "phone":null,
     *            "enquiry_email":null,
     *            "agency_id":14,
     *            "preferred_contact_number":"0400123456"
     *          },
     *          "relationships":{  
     *            "agency":{  
     *               "id":14,
     *               "attributes":{  
     *                  "name":"Fitzroys - Melbourne",
     *                  "phone":"0392757777",
     *                  "fax":null,
     *                  "email":"email@fitzroys.com.au",
     *                  "web":null,
     *                  "twitter":null,
     *                  "facebook":null,
     *                  "logo_file_url":null,
     *                  "agency_id":"CV-5B1B4D",
     *                  "abn":null,
     *                  "slug":"fitzroys-melbourne"
     *               }
     *            },
     *            "agent_personal_info":{}
     *          }
     *        },
     *        {  
     *          "id":2,
     *          "attributes":{  
     *            "email":"bill@commercialview.com.au",
     *            "encrypted_password":"$2a$10$tTi4PrYjfC5Ufwj.ioaTQOdvWxiiJhmM9TnF5WAVgEylOYO6ITqtu",
     *            "first_name":"Bill",
     *            "last_name":"Gates",
     *            "mobile":"0400111222",
     *            "phone":null,
     *            "enquiry_email":null,
     *            "agency_id":14,
     *            "preferred_contact_number":"0400111222"
     *          },
     *          "relationships":{  
     *            "agency":{  
     *               "id":14,
     *               "attributes":{  
     *                  "name":"Fitzroys - Melbourne",
     *                  "phone":"0392757777",
     *                  "fax":null,
     *                  "email":"email@fitzroys.com.au",
     *                  "web":null,
     *                  "twitter":null,
     *                  "facebook":null,
     *                  "logo_file_url":null,
     *                  "agency_id":"CV-5B1B4D",
     *                  "abn":null,
     *                  "slug":"fitzroys-melbourne"
     *               }
     *            },
     *            "agent_personal_info":{}
     *          }
     *        },
     *        {  
     *          "id":3,
     *          "attributes":{  
     *            "email":"warren@commercialview.com.au",
     *            "encrypted_password":"$2a$10$67pXuIaiMt0oN0pQ4LMfcOUl9E.PDIXyWlurvEadu.g5VEyWqjZYO",
     *            "first_name":"Warren",
     *            "last_name":"Buffett",
     *            "mobile":"0400999888",
     *            "phone":null,
     *            "enquiry_email":null,
     *            "agency_id":2,
     *            "preferred_contact_number":"0400999888"
     *          },
     *          "relationships":{  
     *            "agency":{  
     *               "id":2,
     *               "attributes":{  
     *                  "name":"CBRE - Hotels",
     *                  "phone":"0293333333",
     *                  "fax":null,
     *                  "email":"kelly.smith@cbre.com.au",
     *                  "web":null,
     *                  "twitter":null,
     *                  "facebook":null,
     *                  "logo_file_url":null,
     *                  "agency_id":"CV-D74235",
     *                  "abn":null,
     *                  "slug":"cbre-hotels"
     *               }
     *            },
     *            "agent_personal_info":{}
     *          }
     *        },
     *        {  
     *          "id":4,
     *          "attributes":{  
     *            "email":"barack@commercialview.com.au",
     *            "encrypted_password":"$2a$10$25Ie1G6iwM5Wb5atL9T5OOidXfCeOu.An2/ny/mFtfo5SGNrBlirK",
     *            "first_name":"Barack",
     *            "last_name":"Obama",
     *            "mobile":"0400999888",
     *            "phone":null,
     *            "enquiry_email":null,
     *            "agency_id":2,
     *            "preferred_contact_number":"0400999888"
     *          },
     *          "relationships":{  
     *            "agency":{  
     *               "id":2,
     *               "attributes":{  
     *                  "name":"CBRE - Hotels",
     *                  "phone":"0293333333",
     *                  "fax":null,
     *                  "email":"kelly.smith@cbre.com.au",
     *                  "web":null,
     *                  "twitter":null,
     *                  "facebook":null,
     *                  "logo_file_url":null,
     *                  "agency_id":"CV-D74235",
     *                  "abn":null,
     *                  "slug":"cbre-hotels"
     *               }
     *            },
     *            "agent_personal_info":{}
     *          }
     *        },
     *        {  
     *          "id":6,
     *          "attributes":{  
     *            "email":"office_admin@commercialview.com.au",
     *            "encrypted_password":"$2a$10$C/.SwrwhHHdcroc3nQkwCekv31CC9xTmHF.XN0xYepqODDUbLw7Ty",
     *            "first_name":"George",
     *            "last_name":"Clooney",
     *            "mobile":"0400999888",
     *            "phone":null,
     *            "enquiry_email":null,
     *            "agency_id":14,
     *            "preferred_contact_number":"0400999888"
     *          },
     *          "relationships":{  
     *            "agency":{  
     *               "id":14,
     *               "attributes":{  
     *                  "name":"Fitzroys - Melbourne",
     *                  "phone":"0392757777",
     *                  "fax":null,
     *                  "email":"email@fitzroys.com.au",
     *                  "web":null,
     *                  "twitter":null,
     *                  "facebook":null,
     *                  "logo_file_url":null,
     *                  "agency_id":"CV-5B1B4D",
     *                  "abn":null,
     *                  "slug":"fitzroys-melbourne"
     *               }
     *            },
     *            "agent_personal_info":{}
     *          }
     *        }
     *      ]
     *    }
     *    
     * @apiSuccessExample Success-Response:
     *    {
     *      "message": "Agents list sync up succesfully",
     *      "data": {
     *          "status": "success"
     *      },
     *      "status": 200
     *    }
     *    
	 * @apiErrorExample Error-Response in case of invalid / expired Access token:
	 *     HTTP/1.1 401 Unauthorized
	 *     {
	 *       "name": "Unauthorized",
     *       "message": "Your request was made with invalid credentials.",
	 *       "code": 0,
	 *       "status": 401,
	 *       "type": "yii\\web\\UnauthorizedHttpException"
	 *     }
	 *     
     */
    public function actionSyncAgentData(){
        $data = file_get_contents("php://input");		
        $event = json_decode($data, true);
        $galleryArr = $auctionArr = [];
        if($event){
            $objectArray = $event['data'];
            if(is_array($objectArray)){
                foreach($objectArray as $details){
                    $agentModel = Agent::findOne(['email'=>$details['attributes']['email'], 'user_type'=>10, 'app_id'=>2]);
                    if(empty($agentModel->id)){
                        $agentModel = new Agent();
                        $agentModel->setAttributes($details['attributes']);
                        $agentModel->firstName = $details['attributes']['first_name'];
                        $agentModel->lastName = $details['attributes']['last_name'];
                        $agentModel->phone = $details['attributes']['mobile'];
                        if($details['attributes']['encrypted_password']) $agentModel->password_hash = $details['attributes']['encrypted_password'];                        
                        if($agentModel->save(false)){
                            $agentModel->user_code = 'AW-'.$agentModel->username;
                                $profileModel = new Profiles();
                                $profileModel->setAttributes($details['attributes']);
                                $profileModel->user_id = $agentModel->id;
                                $profileModel->officePhone = $details['attributes']['phone'];
                                $profileModel->officeFax = $details['attributes']['phone'];
                                $profileModel->enquiry_email = $details['attributes']['enquiry_email'];
                                $profileModel->preferred_contact_number = $details['attributes']['preferred_contact_number'];
                                if($details['relationships']['agent_personal_info']){
                                    $profileModel->about = $details['relationships']['agent_personal_info']['attributes']['about'];
                                    $profileModel->licence_number = $details['relationships']['agent_personal_info']['attributes']['licence_number'];
                                    $profileModel->awards = $details['relationships']['agent_personal_info']['attributes']['awards'];
                                    $profileModel->specialties = $details['relationships']['agent_personal_info']['attributes']['specialties'];
                                    $profileModel->socialmediaFacebook = $details['relationships']['agent_personal_info']['attributes']['facebook_url'];
                                    $profileModel->socialmediaTwitter = $details['relationships']['agent_personal_info']['attributes']['twitter_url'];
                                    $profileModel->socialmediaLinkedin = $details['relationships']['agent_personal_info']['attributes']['linkedin_url'];
                                    $profileModel->accreditation = $details['relationships']['agent_personal_info']['attributes']['accreditation'];
                                    $memVal = $details['relationships']['agent_personal_info']['attributes']['members'];
                                    if(is_array($memVal)) $memVal = json_encode($memVal);
                                    $profileModel->members = $memVal;
                                    $profileModel->languages = $details['relationships']['agent_personal_info']['attributes']['languages'];
                                }
                            $profileModel->save(false);
                        }
                    }
                    if($details['relationships']['agency']['attributes']){
                        $agencyModel = Agency::findOne(['cv_agency_id' => $details['relationships']['agency']['attributes']['agency_id']]);
                        if(empty($agencyModel)){
                            $agencyModel = new Agency();
                            $agencyModel->cv_agency_id = $details['relationships']['agency']['attributes']['agency_id'];
                        }
                        $agencyModel->scenario = "create"; 
                        $agencyModel->setAttributes($details['relationships']['agency']['attributes']); // set posted attributes to the model Agency
                        $agencyModel->mobile = $details['relationships']['agency']['attributes']['phone'];
                        $agencyModel->fax = $details['relationships']['agency']['attributes']['fax'];
                        $agencyModel->logo = $details['relationships']['agency']['attributes']['logo_file_url'];
                        if(!$agencyModel->validate()){
                            return $agencyModel;
                        }else{
                            //echo '<pre>'; print_r($agencyModel); die('before save');
                            $agencyModel->save(false);
                            $agentModel->agency_id = $agencyModel->id;
                            $agentModel->user_code = 'AW-'.$agentModel->username;
                            $agentModel->save(false);
                        }
                    }
                }
                $msg = CommonModel::textGlobalization('app', 'agent_sync_successfull');
                return ['status' => 'success', 'message' => $msg, 'statusCode' => 200];
            }
        }else{
            $msg = CommonModel::textGlobalization('app', 'invalid_data');
            return ['status' => 'error', 'message' => $msg, 'statusCode' => 422]; 
        }
    }

    /* **
	 *
     * Sync up Bidders data
	 *
	 * @api {get} api/web/v1/webhooks/user/sync-bidders-data Bidders data Syncing
	 * @apiName SyncBiddersData
	 * @apiVersion 0.1.1
	 * @apiGroup DataSyncingWebhooks
     * @apiDescription To Sync Up Bidders data, Form-Data must be of raw type. Data will be posted in the below format by the cview team to sync with our database.
	 *
	 * @apiSuccessExample Request Data:
     *    {
     *      "type": "bidders",
     *      "data": [
     *        {
     *          "id": 5,
     *          "attributes": {
     *            "email": "consumer@commercialview.com.au",
     *            "encrypted_password": "$2a$10$LuD2lC.POomM9YQtTZcZZu0VsZpF8q1o/DCOmJZD1hyp8J.V2wpKO",
     *            "first_name": "George",
     *            "last_name": "Clooney",
     *            "mobile": "0400999888",
     *            "phone": null,
     *            "preferred_contact_number": "0400999888"
     *          },
     *          "relationships": {}
     *        }
     *      ]
     *    }
     *    
     * @apiSuccessExample Success-Response:
     *    {
     *      "message": "Bidders list sync up succesfully",
     *      "data": {
     *          "status": "success"
     *      },
     *      "status": 200
     *    }
     *    
	 * @apiErrorExample Error-Response in case of invalid / expired Access token:
	 *     HTTP/1.1 401 Unauthorized
	 *     {
	 *       "name": "Unauthorized",
     *       "message": "Your request was made with invalid credentials.",
	 *       "code": 0,
	 *       "status": 401,
	 *       "type": "yii\\web\\UnauthorizedHttpException"
	 *     }
	 *              
     */
    public function actionSyncBiddersData(){
        $data = file_get_contents("php://input");		
        $event = json_decode($data, true);
        $galleryArr = $auctionArr = [];
        if($event){
            $objectArray = $event['data'];
            if(is_array($objectArray)){
                foreach($objectArray as $details){
                    $bidderModel = Bidder::findOne(['email'=>$details['attributes']['email'], 'user_type'=>3, 'app_id'=>2]);
                    if(empty($bidderModel->id)){
                        $bidderModel = new Bidder();
                        $bidderModel->setAttributes($details['attributes']);
                        if($details['attributes']['encrypted_password']) $bidderModel->password_hash = $details['attributes']['encrypted_password'];
                        $bidderModel->firstName = $details['attributes']['first_name'];
                        $bidderModel->lastName = $details['attributes']['last_name'];
                        $bidderModel->phone = $details['attributes']['mobile'];
                        if($bidderModel->save(false)){
                            $bidderModel->user_code = 'AW-'.$bidderModel->username;
                            $bidderModel->save(false);
                            $profileModel = new Profiles();
                            $profileModel->setAttributes($details['attributes']);
                            $profileModel->user_id = $bidderModel->id;
                            $profileModel->officePhone = $details['attributes']['phone'];
                            $profileModel->officeFax = $details['attributes']['phone'];
                            $profileModel->preferred_contact_number = empty($details['attributes']['preferred_contact_number']) ? $details['attributes']['mobile'] : $details['attributes']['preferred_contact_number'];
                            $profileModel->save(false);
                        }
                    }
                }
                $msg = CommonModel::textGlobalization('app', 'bidders_sync_successfull');
                return ['status' => 'success', 'message' => $msg, 'statusCode' => 200];
            }
        }else{
            $msg = CommonModel::textGlobalization('app', 'invalid_data');
            return ['status' => 'error', 'message' => $msg, 'statusCode' => 422]; 
        }
    }    

}