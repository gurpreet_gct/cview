<?php
namespace api\modules\v1\controllers\webhooks;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\User;
use api\components\Controller;
use common\models\PropertyType;
use api\modules\v1\models\AgencyOffices;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;
use api\modules\v1\models\Storelocator;
class PropertyTypeController extends Controller{
    public $modelClass = 'common\models\PropertyType';
    
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors(){
		$behaviors = parent::behaviors();
		// $behaviors['authenticator'] = [
		// 	'class' => CompositeAuth::className(),
		// 	'authMethods' => [
		// 		HttpBasicAuth::className(),
		// 		HttpBearerAuth::className(),
		// 		QueryParamAuth::className(),
		// 	],
		// ];
		return $behaviors;
	}
	   
    
  	public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		 unset($actions['create'], $actions['update'],$actions['index'],$actions['search'],$actions['searchkeyword'],$actions['delete'],$actions['view']);                    

		return $actions;
	}
    
	/* ***
	* To create Property Type
	*
	* 
	*
	* @api {post} /api/web/v1/webhooks/property-type/create Create Property Type
	* @apiName CreateProperty-type
	* @apiVersion 0.1.1
	* @apiGroup PropertyTypes
	* @apiDescription To create new property type, Form-Data must be x-www-form-urlencoded
	*
	* @apiParam {String} name property type name.  
	* @apiParam {Number} status 1:Enable, 0:disable
	*
	*
	* @apiSuccessExample Success-Response:
	*     HTTP/1.1 200 OK
	*     {
	*       "message": "",
	*       "data": {
	*            "name": "other1", 
	*            "status": "1",
	*            "id": 10
	*       },
	*       "status": 200
	*     }
	*
	* @apiError Validation Error Make sure to fix the listed issues to create 
	* a successful user account.
	*
	* @apiErrorExample Error-Response:
	*     HTTP/1.1 422 Validation Error
	*     {
	*       "message": "",
	*       "data": [
	*         {
	*           "field": "name",
	*           "message": "Name \"Doe\" has already been taken. "
	*         },
	*       ],
	*       "status": 422
	*     }
	* Or
	*    HTTP/1.1 422 Validation Error
	*    {
	*        "message": "",
	*        "data": [
	*             {
	"field": "name",
	"message": "Name cannot be blank."
	},
	{
	"field": "status",
	"message": "Status cannot be blank."
	}
	*       ],
	*       "status": 422
	*    }
	* @return Object|User-model
	*/
  	public function actionCreate()
    {
		$model= new PropertyType();
		$model->load(Yii::$app->getRequest()->getBodyParams(), ''); 
		 if (!$model->validate()) {
            return $model;
        }
		if (($model->save() === false) && (!$model->hasErrors())) {
			throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }
       // print_r(count(json_decode($model->address)));die;
        
        return $model; 
    }
    

	/* **
	* All Active PropertyTypes
	*
	* @api {get} /api/web/v1/webhooks/property-type/list All Active PropertyTypes
	* @apiName All Active PropertyTypes
	* @apiVersion 0.1.1
	* @apiGroup PropertyTypes
	* @apiDescription Fetch the all active PropertyTypes
	*
	* @apiSuccessExample Success-Response:
	*    HTTP/1.1 200 OK
    *    {
    *      "message": "",
    *      "data": [
    *        {
    *          "id": 1,
    *          "name": "Commercial Farming"
    *        },
    *        {
    *          "id": 2,
    *          "name": "Land/Development"
    *        },
    *        {
    *          "id": 3,
    *          "name": "Hotel/Leisure"
    *        },
    *        {
    *          "id": 4,
    *          "name": "Industrial/Warehouse"
    *        },
    *        {
    *          "id": 5,
    *          "name": "Medical/Consulting"
    *        },
    *        {
    *          "id": 6,
    *          "name": "Offices"
    *        },
    *        {
    *          "id": 7,
    *          "name": "Retail"
    *        },
    *        {
    *          "id": 8,
    *          "name": "Showrooms/Bulky Goods"
    *        },
    *        {
    *          "id": 9,
    *          "name": "Other"
    *        }
    *      ],
    *      "status": 200
    *    }
	* @return Object| PropertyType
	*/
  	public function actionList(){
		$model = PropertyType::find()->select(['id', 'name'])->where([ 'status' => 1 ])->all();
        return $model; 
    }    
    
	
    public function actionTest()
    {
    	return "Testing agency routes!!!";
    }

}
