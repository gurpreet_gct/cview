<?php
namespace api\modules\v1\controllers\webhooks;

use Yii;
use yii\db\Query;
use common\models\User;
use common\models\CommonModel;
use yii\rest\ActiveController;
use api\components\Controller;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\Agency;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\AgencySearch;
use api\modules\v1\models\AgencyOffices;
use api\modules\v1\models\AgencyFavourite;

class AgencyController extends Controller
{
    public $modelClass = 'api\modules\v1\models\Agency';
    
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
    {
        $behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'except' => ['subfees'],
            'only'=>[
                'add-agency',
                'update-agency',
                'delete-agency',
                'sync-agency-data',
                'create',
                'view',
                'update',
                'delete',
            ],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
    }
	   
  	public function actions(){
        $actions = parent::actions();
        // disable the "delete" and "update" actions
        unset($actions['create'], $actions['update'], $actions['index'], $actions['search'], $actions['searchkeyword'], $actions['delete'], $actions['view']);
        return $actions;
	}
    
    public function actionIndex(){
        $agencysearchModel = new AgencySearch();
        $searchByAttr['AgencySearch'] = Yii::$app->request->queryParams;
        return $agencysearchModel->search($searchByAttr);
    }
    
    /*public function actionView(){
        echo '<pre>'; print_r($_GET); die('view');
    }*/
    
	/**
	 * Update an Agency
	 *                                        
	 * @api {PUT} /api/web/v1/webhooks/agencies/{id} Update an Agency
	 * @apiName UpdateAgency
	 * @apiVersion 0.1.1
	 * @apiGroup AgencyWebhooks
	 * @apiDescription To uodate an Agency (Webhook)
     * ### Notes:
     *  - `Login|access token required`
     *  - `Form-Data must be of raw type`   	  
	 *
	 * @apiSuccessExample Request Data:
     *     {
     *       "data": {
     *         "cv_agency_id": "CV-82AA85",
     *         "name": "New Name",
     *         "address": "Sco 115-118, sector 17 A, chandigarh - 160022",
     *         "logo": "logo_192x168.jpg",
     *         "latitude": "30.72589",
     *         "longitude": "76.75787",
     *         "email": "usesr_testingd@yopmail.com",
     *         "mobile": "9468594658",
     *         "about_us": "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>"
     *       }
     *     }
	 *
	 * @apiErrorExample Success-Response:
	 *     HTTP/1.1 200 OK
     *     {
     *       "message": "",
     *       "data": {
     *         "name": "New Name",
     *         "address": "Sco 115-118, sector 17 A, chandigarh - 160022",
     *         "logo": "logo_192x1x68.jpg",
     *         "latitude": "30.72589",
     *         "longitude": "76.75787",
     *         "email": "usesr_123@yopmail.com",
     *         "mobile": "9468594658",
     *         "about_us": "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
     *         "created_at": "2018-05-21 11:14:14",
     *         "updated_at": "2018-05-21 11:20:36"
     *       },
     *       "status": 200
     *     }
	 *
	 * @apiErrorExample Error-Response:
	 * 
	 *    HTTP/1.1 401 Unauthorized
	 *    {
     *      "name": "Unauthorized",
     *    	"message": "You are requesting with an invalid credentials.",
     *    	"code": 0,
     *    	"status": 401,
     *    	"type": "yii\\web\\UnauthorizedHttpException"
     *    }
     *    Or	  
	 *    HTTP/1.1 401 Unauthorized
	 *    {
	 *      "message": "No Access given to update all records",
	 *      "data":{
     *        "status": "error"
     *      },
	 *      "status": 401
	 *    }
     *    Or	  
	 *    HTTP/1.1 200 OK
	 *    {
	 *      "message": "Empty payload data",
	 *      "data":{
     *        "status": "false"
     *      },
	 *      "status": 200
	 *    }
     *    Or	  
	 *    HTTP/1.1 422 Unprocessable entity
	 *    {
	 *      "message": "No Record Found",
	 *      "data":{
     *        "status": "error"
     *      },
	 *      "status": 422
	 *    }
	 *    
	 * @return Object|Agency-model
	 */         
    public function actionUpdate($id = NULL){
        $data = file_get_contents("php://input");
        $inputValidity = parent::validateWebhooksInput($data);
        if (!$inputValidity['status']) {
            return $inputValidity;
        }
        $event = json_decode($data, true);
        $model = new Agency();
        if(!$id) return ['status' => 'error', 'message' => 'No Access given to update all records', 'statusCode' => 401];
        if($event){
            $obj = $event['data'];
            if(is_array($obj)){
                $model = Agency::findOne(['id'=>$id]);
                if(empty($model)){
                    $msg = CommonModel::textGlobalization('app', 'no_record');
                    return ['message' => $msg, 'statusCode' => 422];                        
                }
                $model->setAttributes($obj); // set posted attributes to the model Agency
                $model->save(false);
                $data = [
                    'id' => $model->id,
                    'name' => $model->name,
                    'email' => $model->email,
                    'mobile' => $model->mobile,
                    'logo' => $model->logo,
                    'address' => $model->address,
                    'latitude' => $model->latitude,
                    'longitude' => $model->longitude,
                    'about_us' => $model->about_us,
                    'status' => $model->status,
                    'created_at' => date("Y-m-d H:i:s", $model->created_at),
                    'updated_at' => date("Y-m-d H:i:s", $model->updated_at)
                ];
                return $data;           
			}
        }else{
            if(!$model->validate()) return $model;
        }
    }
    
	/*** 
	 * 
	 * Delete an Agency
	 *
	 * @api {DELETE} api/web/v1/webhooks/agencies/{id} Delete an Agency
	 * @apiName DeleteAgency
	 * @apiVersion 0.1.1
	 * @apiGroup AgencyWebhooks
     * @apiDescription Delete an Agency (Webhook). One must requires login|access token to perform the action
     * ### Notes:
     *  - `Login|access token required`
	 *     
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "message": "Agency has been deleted successfully",
	 *       "data":{
     *         "status": "success"
     *       },
	 *       "status": 200
	 *     }
	 *     
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 401 Invalid credentials
	 *    {
	 *      "name": "Unauthorized",
     *      "message": "Your request was made with invalid credentials.",
	 *      "code": 0,
	 *      "status": 401,
	 *      "type": "yii\\web\\UnauthorizedHttpException"
	 *    }
	 *    Or
	 *    HTTP/1.1 422 Unprocessable entity
	 *    {
	 *      "message": "No Record Found",
	 *      "data":{
     *        "status": "error"
     *      },
	 *      "status": 422
	 *    }
	 *    Or
	 *    HTTP/1.1 401 Unauthorized
	 *    {
	 *      "message": "No Access given to delete all records",
	 *      "data":{
     *        "status": "error"
     *      },
	 *      "status": 401
	 *    }	      
	 *
     */    
    public function actionDelete($id=null){
        if($id){
            $model = Agency::findOne(['id' => $id]);
            if($model){
                $model->delete();
                AgencyOffices::DeleteAll(['agency_id' => $id ]);
                AgencyFavourite::DeleteAll(['agency_id' => $id ]);
                $msg = CommonModel::textGlobalization('app', 'agency_del_msg');
                return ['status' => 'success', 'message' => $msg, 'statusCode' => 200];
            }else{
                $msg = CommonModel::textGlobalization('app', 'no_record');
                return ['status' => 'error', 'message' => $msg, 'statusCode' => 422];                
            }
		}else{
            return ['status' => 'error', 'message' => 'No Access given to delete all records', 'statusCode' => 401];
        }
    }
    
	/**
	 * Create an Agency
	 *                                        
	 * @api {POST} /api/web/v1/webhooks/agencies Create an Agency
	 * @apiName AddAgency
	 * @apiVersion 0.1.1
	 * @apiGroup AgencyWebhooks
	 * @apiDescription To create an Agency (Webhook)
     * ### Notes:
     *  - `Login|access token required`
     *  - `Form-Data must be of raw type`
     *  
	 * @apiSuccessExample Request Data:
	 *     RAW Data
     *     {
     *       "data": {
     *         "cv_agency_id": "CV-82AA85",
     *         "name": "Offsice_titled",
     *         "address": " sco 206 - 207, sector 34 a, chandigarh - 160022",
     *         "logo": "logo_192x168.jpg",
     *         "latitude": "30.72589",
     *         "longitude": "76.75787",
     *         "email": "usesr_testingd@yopmail.com",
     *         "mobile": "9468594658",
     *         "about_us": "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>"
     *       }
     *     }
	 *
	 * @apiErrorExample Success-Response:
	 *     HTTP/1.1 200 OK
     *     {
     *       "message": "",
     *       "data": {
     *         "id": "35",
     *         "cv_agency_id": "CV-82AA85",
     *         "name": "Office_name",
     *         "address": " sco 206 - 207, sector 34 a, chandigarh - 160022",
     *         "logo": "logo_192x1x68.jpg",
     *         "latitude": "30.72589",
     *         "longitude": "76.75787",
     *         "email": "usesr_123@yopmail.com",
     *         "mobile": "9468594658",
     *         "about_us": "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
     *         "created_at": "2018-05-21 11:14:14",
     *         "updated_at": "2018-05-21 11:20:36"*         
     *       },
     *       "status": 200
     *     }
	 *
	 * @apiErrorExample Error-Response:
	 * 
	 *    HTTP/1.1 401 Unauthorized
	 *    {
     *      "name": "Unauthorized",
     *    	"message": "You are requesting with an invalid credentials.",
     *    	"code": 0,
     *    	"status": 401,
     *    	"type": "yii\\web\\UnauthorizedHttpException"
     *    }
     *    Or	  
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *      "message": "",
	 *      "data": [
     *        {
     *          "field": "name",
     *          "message": "Name \"Test office 1\" has already been taken."
     *        },
     *        {
     *          "field": "email",
     *          "message": "Email \"testuser@yopmail.com\" has already been taken."
     *        }       
	 *      ],
	 *      "status": 422
	 *    }
	 *    OR
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *      "message": "",
	 *      "data": [
	 *        {
     *          "field": "name",
     *          "message": "Name cannot be blank."
     *        },
     *        {
     *          "field": "mobile",
     *          "message": "Mobile cannot be blank."
     *        },
     *        {
     *          "field": "email",
     *          "message": "Email cannot be blank."
     *        },
     *        {
     *          "field": "cv_agency_id",
     *          "message": "cv_agency_id cannot be blank."
     *        },
	 *      ],
	 *      "status": 422
	 *    }
	 *    
	 * @return Object|Agency-model
	 */
    public function actionCreate(){
        $data = file_get_contents("php://input");
        $inputValidity = parent::validateWebhooksInput($data);
        if (!$inputValidity['status']) {
            return $inputValidity;
        }        
        $enc_data = json_decode($data, true);
        $model = new Agency();
        $model->scenario = "create";
        if(empty($enc_data)){
            if(!$model->validate()) return $model;
        }
        $model = Agency::findOne(['cv_agency_id' => $enc_data['data']['cv_agency_id']]);
        if(empty($model)){
            $model = new Agency();
            $model->scenario = "create";            
            $obj = $enc_data['data'];
            if(is_array($obj)){
				$model->setAttributes($obj); // set posted attributes to the model Agency
                if(!$model->validate()){
                    return $model;
                }else{
                    $model->save(false);
                }
			}
        }
        $data = [
            'id' => $model->id,
            'cv_agency_id' => $model->cv_agency_id,
            'name' => $model->name,
            'email' => $model->email,
            'mobile' => $model->mobile,
            'logo' => $model->logo,
            'address' => $model->address,
            'latitude' => $model->latitude,
            'longitude' => $model->longitude,
            'about_us' => $model->about_us,
            'status' => $model->status,
            'created_at' => date("Y-m-d H:i:s", $model->created_at),
            'updated_at' => date("Y-m-d H:i:s", $model->updated_at)
        ];
        return $data;        
    }
	
	/* **
	 *
	 * List Agencies
	 *
	 * @api {get} api/web/v1/webhooks/agency/all Agency Listing
	 * @apiName AgencyListing
	 * @apiVersion 0.1.1
	 * @apiGroup AgencyWebhooks
	 * @apiDescription List all Agency, Result Set is in Pagination of 10 records in each page. To fetch resultset in pagination url would like this: api/web/v1/webhooks/agency/all?page=2, Where page is the page number.
	 *
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 OK
     *    {
     *      "data": {
     *        "items": [
     *          {
     *            "status": 10,
     *            "id": 33,
     *            "name": "Offsice_dtitled",
     *            "email": "usesr_testdingd@yopmail.com",
     *            "mobile": 9468594658,
     *            "logo": "http://localhost/AlphaWallet/html/frontend/web/agencylogo/logo_192x168.jpg",
     *            "backgroungImage": null,
     *            "address": " sco 206 - 207, sector 34 a, chandigarh - 160022",
     *            "latitude": "30.72589",
     *            "longitude": "76.75787",
     *            "about_us": "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
     *            "created_at": 1526899067,
     *            "updated_at": 1526899067,
     *            "is_favourite": "",
     *            "facebook": "https://www.facebook.com/",
     *            "twitter": "https://twitter.com/",
     *            "instagram": "https://www.instagram.com/"
     *          },
     *          {
     *            "status": 10,
     *            "id": 34,
     *            "name": "Offsice_sdtitled",
     *            "email": "usesr_testsdingd@yopmail.com",
     *            "mobile": 9468594658,
     *            "logo": "http://localhost/AlphaWallet/html/frontend/web/agencylogo/logo_192x168.jpg",
     *            "backgroungImage": null,
     *            "address": " sco 206 - 207, sector 34 a, chandigarh - 160022",
     *            "latitude": "30.72589",
     *            "longitude": "76.75787",
     *            "about_us": "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
     *            "created_at": 1526899107,
     *            "updated_at": 1526899107,
     *            "is_favourite": "",
     *            "facebook": "https://www.facebook.com/",
     *            "twitter": "https://twitter.com/",
     *            "instagram": "https://www.instagram.com/"
     *          },
     *        ],  
     *        "_links": {
     *          "self": {
     *            "href": "http://localhost/AlphaWallet/html/api/web/v1/webhooks/agency/all?page=2&per-page=10"
     *          },
     *          "first": {
     *            "href": "http://localhost/AlphaWallet/html/api/web/v1/webhooks/agency/all?page=1&per-page=10"
     *          },
     *          "prev": {
     *            "href": "http://localhost/AlphaWallet/html/api/web/v1/webhooks/agency/all?page=1&per-page=10"
     *          }
     *        },
     *        "_meta": {
     *          "totalCount": 17,
     *          "pageCount": 2,
     *          "currentPage": 2,
     *          "perPage": 10
     *        }
     *      },
     *      "status": 200
     *    }
	 */
	public function actionAll(){
		//~ $agencies = Agency::find()->where(['status'=>10])->orderBy(['name'=>'SORT_DESC'])->all();
		//~ return $agencies;
        $agencysearchModel = new AgencySearch();
        $searchByAttr['AgencySearch'] = Yii::$app->request->queryParams;
        return $agencysearchModel->search($searchByAttr);
	}
    
	/* **
	 *
	 * Sync up Agencies data
	 *
	 * @api {get} api/web/v1/webhooks/agency/sync-agency-data Agencies Syncing
	 * @apiName SyncAgencyData
	 * @apiVersion 0.1.1
	 * @apiGroup DataSyncingWebhooks
	 * @apiDescription Sync Up Agencies data.
     * @apiDescription To Sync Up Agencies, Form-Data must be of raw type. Data will be posted in the below format by the cview team to sync with our database.
	 *
	 * @apiSuccessExample Request Data:
     *    {
     *      "type": "agencies",
     *      "data": [
     *         {
     *           "id": 9,
     *           "attributes": {
     *             "name": "Biz Aus Pty Pld",
     *             "phone": "0417113473",
     *             "fax": null,
     *             "email": "phil@bizaus.com.au",
     *             "web": null,
     *             "twitter": null,
     *             "facebook": null,
     *             "logo_file_url": null,
     *             "agency_id": "CV-CF1462",
     *             "abn": null,
     *             "slug": "biz-aus-pty-pld",
     *             "primary_color": null,
     *             "secondary_color": null,
     *             "text_color": null,
     *             "office_description": null          
     *           },
     *           "relationships": {
     *             "mailing_address": {
     *               "id": 18,
     *               "attributes": {
     *                 "street_address": " Level 12, 440 Collins Street",
     *                 "full_address": " Level 12, 440 Collins Street<br/>Melbourne, VIC 3000",
     *                 "address_type": "mailing_address",
     *                 "suburb_id": 3
     *               },
     *               "location": {
     *                 "latitude": -37.8163397,
     *                 "longitude": 144.9597121
     *               }
     *             },
     *             "office_address": {
     *               "id": 17,
     *               "attributes": {
     *                 "street_address": " Level 12, 440 Collins Street",
     *                 "full_address": " Level 12, 440 Collins Street<br/>Melbourne, VIC 3000",
     *                 "address_type": "office_address",
     *                 "suburb_id": 3
     *               },
     *               "location": {
     *                 "latitude": -37.8163397,
     *                 "longitude": 144.9597121
     *               }
     *             },
     *             "servicing_suburbs": [],
     *             "documents": []
     *           }
     *         },
     *         {
     *           "id": 2,
     *           "attributes": {
     *             "name": "CBRE - Hotels",
     *             "phone": "0293333333",
     *             "fax": null,
     *             "email": "kelly.smith@cbre.com.au",
     *             "web": null,
     *             "twitter": null,
     *             "facebook": null,
     *             "logo_file_url": null,
     *             "agency_id": "CV-D74235",
     *             "abn": null,
     *             "slug": "cbre-hotels",
     *             "primary_color": null,
     *             "secondary_color": null,
     *             "text_color": null,
     *             "office_description": null
     *           },
     *           "relationships": {
     *             "mailing_address": {
     *               "id": 4,
     *               "attributes": {
     *                 "street_address": "Level 26, 363 George Street",
     *                 "full_address": "Level 26, 363 George Street<br/>Sydney, NSW 2000",
     *                 "address_type": "mailing_address",
     *                 "suburb_id": 2
     *               },
     *               "location": {
     *                 "latitude": -33.8683539,
     *                 "longitude": 151.206534
     *               }
     *             },
     *             "office_address": {
     *               "id": 3,
     *               "attributes": {
     *                 "street_address": "Level 26, 363 George Street",
     *                 "full_address": "Level 26, 363 George Street<br/>Sydney, NSW 2000",
     *                 "address_type": "office_address",
     *                 "suburb_id": 2
     *               },
     *               "location": {
     *                 "latitude": -33.8683539,
     *                 "longitude": 151.206534
     *               }
     *             },
     *             "servicing_suburbs": [],
     *             "documents": []
     *           }
     *         },
     *         {
     *           "id": 12,
     *           "attributes": {
     *             "name": "Crest Business Brokers",
     *             "phone": "0399995488",
     *             "fax": null,
     *             "email": "mail@crestcorporate.com",
     *             "web": null,
     *             "twitter": null,
     *             "facebook": null,
     *             "logo_file_url": null,
     *             "agency_id": "CV-8BDF1A",
     *             "abn": null,
     *             "slug": "crest-business-brokers",
     *             "primary_color": null,
     *             "secondary_color": null,
     *             "text_color": null,
     *             "office_description": null
     *           },
     *           "relationships": {
     *             "mailing_address": {
     *               "id": 24,
     *               "attributes": {
     *                 "street_address": " Level 8, 423 Bourke Street",
     *                 "full_address": " Level 8, 423 Bourke Street<br/>Melbourne, VIC 3000",
     *                 "address_type": "mailing_address",
     *                 "suburb_id": 3
     *               },
     *               "location": {
     *                 "latitude": -37.8140595,
     *                 "longitude": 144.9599904
     *               }
     *             },
     *             "office_address": {
     *               "id": 23,
     *               "attributes": {
     *                 "street_address": " Level 8, 423 Bourke Street",
     *                 "full_address": " Level 8, 423 Bourke Street<br/>Melbourne, VIC 3000",
     *                 "address_type": "office_address",
     *                 "suburb_id": 3
     *               },
     *               "location": {
     *                 "latitude": -37.8140595,
     *                 "longitude": 144.9599904
     *               }
     *             },
     *             "servicing_suburbs": [],
     *             "documents": []
     *           }
     *         }
     *      ]
     *    }
     *    
     * @apiSuccessExample Success-Response:
     *    {
     *      "message": "Agencies list sync up succesfully",
     *      "data": {
     *          "status": "success"
     *      },
     *      "status": 200
     *    }
     *    
	 * @apiErrorExample Error-Response in case of invalid / expired Access token:
	 *     HTTP/1.1 401 Unauthorized
	 *     {
	 *       "name": "Unauthorized",
     *       "message": "Your request was made with invalid credentials.",
	 *       "code": 0,
	 *       "status": 401,
	 *       "type": "yii\\web\\UnauthorizedHttpException"
	 *     }
	 *             
     */
    public function actionSyncAgencyData(){
        $data = file_get_contents("php://input");		
        $event = json_decode($data, true);
        $galleryArr = $auctionArr = [];
        if($event){
            $objectArray = $event['data'];
            if(is_array($objectArray)){
                foreach($objectArray as $details){
                    $agencyModel = Agency::findOne(['cv_agency_id' => $details['attributes']['agency_id']]);
                    $newRec = false; //removes quote
                    if(empty($agencyModel)){
                        $agencyModel = new Agency();
                        $newRec = true;
                        $agencyModel->cv_agency_id = $details['attributes']['agency_id'];
                    }
                    $agencyModel->scenario = "create"; 
                    $agencyModel->setAttributes($details['attributes']); // set posted attributes to the model Agency
                    $agencyModel->mobile = $details['attributes']['phone'];
                    $agencyModel->logo = $details['attributes']['logo_file_url'];
                    $agencyModel->slug = $details['attributes']['slug'];
                    $agencyModel->abn = $details['attributes']['abn'];
                    $agencyModel->about_us = $details['attributes']['office_description'];
                    if($details['relationships']['mailing_address']['attributes']){
                        $agencyModel->address = $details['relationships']['mailing_address']['attributes']['full_address'];
                        $agencyModel->street_address = $details['relationships']['mailing_address']['attributes']['street_address'];
                        $agencyModel->full_address = $details['relationships']['mailing_address']['attributes']['full_address'];
                        $agencyModel->suburb_id = $details['relationships']['mailing_address']['attributes']['suburb_id'];
                        $agencyModel->latitude = $details['relationships']['mailing_address']['location']['latitude'];
                        $agencyModel->longitude = $details['relationships']['mailing_address']['location']['longitude'];
                    }
                    if(!$agencyModel->validate()){
                        return $agencyModel;
                    }else{
                        $agencyModel->save(false);
                        if($newRec){ // if agency is created for the first time.
                            $agencyOffcModel = new AgencyOffices();
                            $agencyOffcModel->setAttributes($details['attributes']); // set posted attributes to the model AgencyOffices
                            if($details['relationships']['office_address']['attributes']){
                                $agencyOffcModel->street_address = $details['relationships']['office_address']['attributes']['street_address'];
                                $agencyOffcModel->full_address = $details['relationships']['office_address']['attributes']['full_address'];
                                $agencyOffcModel->suburb_id = $details['relationships']['office_address']['attributes']['suburb_id'];
                                $agencyOffcModel->latitude = $details['relationships']['office_address']['location']['latitude'];
                                $agencyOffcModel->longitude = $details['relationships']['office_address']['location']['longitude'];
                                $agencyOffcModel->agency_id = $agencyModel->id;
                                $agencyOffcModel->save(false);
                            }
                        }
                    }
                }
                $msg = CommonModel::textGlobalization('app', 'agency_sync_successfull');
                return ['status' => 'success', 'message' => $msg, 'statusCode' => 200];
            }
        }else{
            $msg = CommonModel::textGlobalization('app', 'invalid_data');
            return ['status' => 'error', 'message' => $msg, 'statusCode' => 422]; 
        }
    }
    
}