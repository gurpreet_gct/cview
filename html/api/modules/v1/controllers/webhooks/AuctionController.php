<?php
namespace api\modules\v1\controllers\webhooks;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\ApplyBid;
use common\models\User;
use common\models\Property;
use api\components\Controller;

/**
 * AuctionController
 *
 * Save|Update Auction management methods for APIs are defined here.
 */

class AuctionController extends Controller
{
    public $modelClass = 'api\modules\v1\models\ApplyBid';
    

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'except' => ['subfees'],
            'only'=>[
                'saveauction',
            ],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "update" actions
        unset($actions['delete'], $actions['index'], $actions['update'],$actions['create']);

        return $actions;
    }
	
    /* **
    *
    * @api {post} /api/web/v1/webhooks/auction/saveauction To save an auction
    * @apiName Saveauction
    * @apiVersion 0.1.1
    * @apiGroup Auction
    * @apiDescription To save auction, Form-Data must be x-www-form-urlencoded
    *
    * @apiParam {Number} auction_id auction_id
    * @apiParam {Number} is_favourite Used For Favorite Auction, 1 => set as favourite, 0 => unfavourite
    * @apiParam {String} callback_url Callback url to post the data on success
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "message": "",
    *       "data": {
    *          "id": 3
    *          "user_id": "1",
    *          "auction_id": "1",
    *          "is_favourite": "1",
    *          "status": 1,
    *          "rating": null,
    *          "user_name": "john_doe"
    *       },
    *       "status": 200
    *     }
    *
    *
    * @apiErrorExample Error-Response:
    *     HTTP/1.1 422 Validation Error
    *     {
    *       "message": "",
    *       "data": [
    *         {
    *           "field": "auction_id",
    *           "message": "Auction Id must be an integer."
    *         },
    *         {
    *           "field": "is_favourite",
    *           "message": "Is Favourite must be an integer."
    *         }
    *       ],
    *       "status": 422
    *     }
    * 
    * @return Object|User-model
    */
	public function actionSaveauction()
    {   
        $authToken =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        $user = User::findIdentityByAccessToken($authToken);
        if(!$user || empty($authToken)) {
            return ['Invalid request, Login first'];
        }
        $formData = Yii::$app->getRequest()->getBodyParams();
        $formData['user_id'] = $user->id;
        $model = ApplyBid::findOne(['user_id' => $formData['user_id'], 'listing_id' => $formData['auction_id']]);
        if ($model == null) {
            $model = new ApplyBid();
            $formData['listing_id'] = $formData['auction_id'];
            $model->load($formData, '');
            // echo '<pre>'; print_r($model); die('here');
            if($model->validate()) {
                // $model->tbl_name = 'tbl_auction_event';
                $model->save();
            }
        } else {
            $model->load($formData, '');
            $model->save();
        }
        parent::processCallback($formData);
        return $model;
    }

}