<?php
namespace api\modules\v1\controllers\webhooks;

use Yii;
use yii\rest\ActiveController;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use common\models\Pass;
use common\models\Property;
use common\models\Workorderpopup; 
use common\models\Workorders;
use common\models\User;
use common\models\Profile;
use common\models\Qr;
use common\models\Offertrack;
use api\modules\v1\models\Agent;
use api\modules\v1\models\Agency;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\PropertySearch;
use common\models\Auction;
use common\models\Gallery;
use api\modules\v1\models\Profiles;
use common\models\CommonModel;

/**
 * PropertyController
 *
 * Property management methods for APIs are defined here.
 * Form-Data must be x-www-form-urlencoded
 */
class PropertyController extends Controller
{    
    public $modelClass = 'common\models\Property';
    
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['subfees'],
            'only'=>[
                'create',
                'update',
                'view',
                'delete',
                'create-property',
                'sync-properties',
            ],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }    
    
  	public function actions()
	{
		$actions = parent::actions();
		// disable the "delete" and "update" actions
		unset($actions['delete'], $actions['index'], $actions['update'],$actions['create']); 
		return $actions;
	}
    
	/**
	 * Create New Property
	 * 
	 * @api {post} api/web/v1/webhooks/properties Create a Property
	 * @apiName CreateProperty
	 * @apiVersion 0.1.1
	 * @apiGroup PropertyWebhooks
	 * @apiDescription To Create Property (Webhook)
	 * ### Notes:
     *  - `Login|access token required`
     *  - `Form-Data must be of raw type`  
	 *
     * @apiSuccessExample Request Data:
	 *    RAW Data      
     *    {
     *      "data": {
     *        "title": "PRIME DEVELOPMENT OPPORTUNITY (STCA)",
     *        "description": "Rare opportunity to acquire this substantial development site, situated in the heart of one of Melbourne's most sought after inner city locations.",
     *        "price": 13500000,
     *        "landArea": 986,
     *        "land_area_unit": "squareMeter",
     *        "floorArea": 890,
     *        "floor_area_unit": "squareMeter",
     *        "address": "516 Church Street, Richmond, VIC 3121",
     *        "latitude": "-37.8280153",
     *        "longitude": "144.9973155",
     *        "state": "6",
     *        "country": "13",
     *        "media": null,
     *        "cview_listing_id": "CVIEW-5123GHT",
     *        "video": "https://youtu.be/YXSXe0LVFV8",
     *        "videoImage": "http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
     *        "document": "[{\"title\":\"Auction Address\",\"url\":\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\"},{\"title\":\"Lease Agreement\",\"url\":\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\"}]",
     *        "tags": "Land Cheap Property",
     *        "gallery": [
     *          {
     *            "url": "https://dduaaywsz-res-5.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1526883272/cxfjy5v0lkutrcuhjemg.jpg",
     *            "type": "image",
     *            "tbl_name": "tbl_properties"
     *          },
     *          {
     *            "url": "https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727712/rp57edrrkcuwjqjx51fr.jpg",
     *            "type": "image",
     *            "tbl_name": "tbl_properties"
     *          },
     *          {
     *            "url": "https://youtu.be/YXSXe0LVFV8",
     *            "thumbnail": "http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
     *            "type": "video",
     *            "tbl_name": "tbl_properties"
     *          }
     *        ]
     *      }
     *    }
     *    
     * @apiSuccessExample Success Response:
     *    {
     *      "message": "",
     *      "data": {
     *        "id": 187,
     *        "title": "PRIME DEVELOPMENT OPPORTUNITY (STCA)",
     *        "description": "Rare opportunity to acquire this substantial development site, situated in the heart of one of Melbourne's most sought after inner city locations.",
     *        "status": null,
     *        "propertyType": 0,
     *        "price": 13500000,
     *        "landArea": 986,
     *        "land_area_unit": "squareMeter",
     *        "floorArea": 890,
     *        "floor_area_unit": "squareMeter",
     *        "conditions": 0,
     *        "address": "516 Church Street, Richmond, VIC 3121",
     *        "latitude": "-37.8280153",
     *        "longitude": "144.9973155",
     *        "unitNumber": 0,
     *        "streetNumber": 0,
     *        "streetName": null,
     *        "suburb": null,
     *        "city": null,
     *        "state": "6",
     *        "postcode": null,
     *        "country": "13",
     *        "parking": 0,
     *        "parkingList": null,
     *        "parkingOther": null,
     *        "floorplan": null,
     *        "media": null,
     *        "video": "https://youtu.be/YXSXe0LVFV8",
     *        "videoImage": "http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
     *        "document": null,
     *        "school": 0,
     *        "communityCentre": 0,
     *        "parkLand": 0,
     *        "publicTransport": 0,
     *        "shopping": 0,
     *        "bedrooms": 0,
     *        "bathrooms": 0,
     *        "features": null,
     *        "tags": null,
     *        "created_by": null,
     *        "propertyStatus": 0,
     *        "updated_by": null,
     *        "created_at": "2018-06-08 11:38:34",
     *        "updated_at": "2018-06-08 11:38:34",
     *        "is_featured": 0,
     *        "cview_listing_id": "CVIEW-5123GHT",
     *        "is_deleted": 0,
     *        "business_email": null,
     *        "website": null,
     *        "valuation": null,
     *        "exclusivity": null,
     *        "tenancy": null,
     *        "zoning": null,
     *        "authority": null,
     *        "sale_tax": null,
     *        "tender_closing_date": null,
     *        "current_lease_expiry": null,
     *        "outgoings": null,
     *        "lease_price": null,
     *        "lease_price_show": 0,
     *        "lease_period": null,
     *        "lease_tax": null,
     *        "external_id": null,
     *        "video_url": null,
     *        "slug": null,
     *        "distance": "",
     *        "propertyTypename": "",
     *        "auctionDetails": null,
     *        "gallery": [
     *          {
     *            "id": 156,
     *            "tbl_pk": 187,
     *            "url": "https://dduaaywsz-res-5.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1526883272/cxfjy5v0lkutrcuhjemg.jpg",
     *            "thumbnail": null,
     *            "type": "image",
     *            "file_name": null,
     *            "tbl_name": "tbl_properties"
     *          },
     *          {
     *            "id": 157,
     *            "tbl_pk": 187,
     *            "url": "https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727712/rp57edrrkcuwjqjx51fr.jpg",
     *            "thumbnail": null,
     *            "type": "image",
     *            "file_name": null,
     *            "tbl_name": "tbl_properties"
     *          },
     *          {
     *            "id": 158,
     *            "tbl_pk": 187,
     *            "url": "https://youtu.be/YXSXe0LVFV8",
     *            "thumbnail": "http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
     *            "type": "video",
     *            "file_name": null,
     *            "tbl_name": "tbl_properties"
     *          }
     *        ],
     *        "auction": null,
     *        "saveauction": null,
     *        "sold": "0",
     *        "calendar": 0
     *      },
     *      "status": 200
     *    }
     *    
	 * @apiErrorExample Error-Response:
	 * 
	 *    HTTP/1.1 401 Unauthorized
	 *    {
     *      "name": "Unauthorized",
     *    	"message": "You are requesting with an invalid credentials.",
     *    	"code": 0,
     *    	"status": 401,
     *    	"type": "yii\\web\\UnauthorizedHttpException"
     *    }         
	 * 
	 */
    public function actionCreate(){
        $data = file_get_contents("php://input");
        $inputValidity = parent::validateWebhooksInput($data);
        if (!$inputValidity['status']) {
            return $inputValidity;
        }
        $event = json_decode($data, true);
        $model = new Property();
        $model->scenario = "webhook_create";
        if($event){        
            $enc_data = $event['data'];
            $model = Property::findOne(['cview_listing_id' => $enc_data['cview_listing_id']]);
            if(empty($model)){
                $model = new Property();
                $model->setAttributes($enc_data);
                if($model->save(false)){
                    if($enc_data['gallery']){
                        foreach($enc_data['gallery'] as $galleryData){
                            $gallery = new Gallery(); 
                            $gallery->setAttributes($galleryData);
                            $gallery->tbl_pk = $model->id;
                            $gallery->save(false);
                            $galleryArr[] = $gallery; // saving results into an array
                        }
                    }
                }
            }
        }else{
            if(!$model->validate()) return $model;
        }        
        return $model;
	}

	/** 
	 * 
	 * Update Property
	 * 
	 * @api {put} api/web/v1/webhooks/properties/{id} Update a Property
	 * @apiName UpdateProperty
	 * @apiVersion 0.1.1
	 * @apiGroup PropertyWebhooks
	 * @apiDescription To Update Property (Webhook)
	 * ### Notes:
     *  - `Login|access token required`
     *  - `Form-Data must be of raw type` 
     *
	 * @apiSuccessExample Request Data:
     *    {
     *      "data": {
     *        "title": "PRIME DEVELOPMENT OPPORTUNITY (STCA) Edited",
     *        "description": "Rare opportunity to acquire this substantial development site, situated in the heart of one of Melbourne's most sought after inner city locations.",
     *        "price": 13500000,
     *        "landArea": 986,
     *        "land_area_unit": "squareMeter",
     *        "floorArea": 890,
     *        "floor_area_unit": "squareMeter",
     *        "address": "516 Church Street, Richmond, VIC 3121",
     *        "latitude": "-37.8280153",
     *        "longitude": "144.9973155",
     *        "state": "6",
     *        "country": "13",
     *        "media": null,
     *        "cview_listing_id": "CVIEW-5123GHT",
     *        "video": "https://youtu.be/YXSXe0LVFV8",
     *        "videoImage": "http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
     *        "document": "[{\"title\":\"Auction Address\",\"url\":\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\"},{\"title\":\"Lease Agreement\",\"url\":\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\"}]",
     *        "tags": "Land Cheap Property Added New",
     *        "gallery": [
     *          {
     *            "url": "https://dduaaywsz-res-5.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1526883272/cxfjy5v0lkutrcuhjemg.jpg",
     *            "type": "image",
     *            "tbl_name": "tbl_properties"
     *          },
     *          {
     *            "url": "https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727712/rp57edrrkcuwjqjx51fr.jpg",
     *            "type": "image",
     *            "tbl_name": "tbl_properties"
     *          },
     *          {
     *            "url": "https://youtu.be/YXSXe0LVFV8",
     *            "thumbnail": "http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
     *            "type": "video",
     *            "tbl_name": "tbl_properties"
     *          }
     *        ]
     *      }
     *    }
	 * 
	 * @apiErrorExample Success Response:
     *    {
     *      "message": "",
     *      "data": {
     *        "id": 187,
     *        "title": "PRIME DEVELOPMENT OPPORTUNITY (STCA) Edited",
     *        "description": "Rare opportunity to acquire this substantial development site, situated in the heart of one of Melbourne's most sought after inner city locations.",
     *        "status": null,
     *        "propertyType": 0,
     *        "price": 13500000,
     *        "landArea": 986,
     *        "land_area_unit": "squareMeter",
     *        "floorArea": 890,
     *        "floor_area_unit": "squareMeter",
     *        "conditions": 0,
     *        "address": "516 Church Street, Richmond, VIC 3121",
     *        "latitude": "-37.8280153",
     *        "longitude": "144.9973155",
     *        "unitNumber": 0,
     *        "streetNumber": 0,
     *        "streetName": null,
     *        "suburb": null,
     *        "city": null,
     *        "state": "6",
     *        "postcode": null,
     *        "country": "13",
     *        "parking": 0,
     *        "parkingList": null,
     *        "parkingOther": null,
     *        "floorplan": null,
     *        "media": null,
     *        "video": "https://youtu.be/YXSXe0LVFV8",
     *        "videoImage": "http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
     *        "document": null,
     *        "school": 0,
     *        "communityCentre": 0,
     *        "parkLand": 0,
     *        "publicTransport": 0,
     *        "shopping": 0,
     *        "bedrooms": 0,
     *        "bathrooms": 0,
     *        "features": null,
     *        "tags": null,
     *        "created_by": null,
     *        "propertyStatus": 0,
     *        "updated_by": null,
     *        "created_at": "1970-01-01 00:33:38",
     *        "updated_at": 1528460700,
     *        "is_featured": 0,
     *        "cview_listing_id": "CVIEW-5123GHT",
     *        "is_deleted": 0,
     *        "business_email": null,
     *        "website": null,
     *        "valuation": null,
     *        "exclusivity": null,
     *        "tenancy": null,
     *        "zoning": null,
     *        "authority": null,
     *        "sale_tax": null,
     *        "tender_closing_date": null,
     *        "current_lease_expiry": null,
     *        "outgoings": null,
     *        "lease_price": null,
     *        "lease_price_show": 0,
     *        "lease_period": null,
     *        "lease_tax": null,
     *        "external_id": null,
     *        "video_url": null,
     *        "slug": null,
     *        "distance": "",
     *        "propertyTypename": "",
     *        "auctionDetails": null,
     *        "gallery": [
     *          {
     *            "id": 156,
     *            "tbl_pk": 187,
     *            "url": "https://dduaaywsz-res-5.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1526883272/cxfjy5v0lkutrcuhjemg.jpg",
     *            "thumbnail": null,
     *            "type": "image",
     *            "file_name": null,
     *            "tbl_name": "tbl_properties"
     *          },
     *          {
     *            "id": 157,
     *            "tbl_pk": 187,
     *            "url": "https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727712/rp57edrrkcuwjqjx51fr.jpg",
     *            "thumbnail": null,
     *            "type": "image",
     *            "file_name": null,
     *            "tbl_name": "tbl_properties"
     *          },
     *          {
     *            "id": 158,
     *            "tbl_pk": 187,
     *            "url": "https://youtu.be/YXSXe0LVFV8",
     *            "thumbnail": "http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
     *            "type": "video",
     *            "file_name": null,
     *            "tbl_name": "tbl_properties"
     *          }
     *        ],
     *        "auction": null,
     *        "saveauction": null,
     *        "sold": "0",
     *        "calendar": 0
     *      },
     *      "status": 200
     *    }
	 *     
	 * @apiSuccessExample Error Property not found
	 *    {
	 *      "message": "No Record Found",
	 *      "data": {
	 *        "status": "error"
	 *      },
	 *      "status": 422
	 *    }
	 *    
	 */
	public function actionUpdate($id = null){
        $data = file_get_contents("php://input");
        $inputValidity = parent::validateWebhooksInput($data);
        if (!$inputValidity['status']) {
            return $inputValidity;
        }
        $event = json_decode($data, true); 
        if(!$id) return ['status' => 'error', 'message' => 'No Access given to update all records', 'statusCode' => 401];        
        if($event){
            $obj =  $event['data'];
            if(is_array($obj)){
                $model = $this->findModel($id); // find properties
                if(!empty($model)){
                    $model->setAttributes($obj);
                    $property_changed_attributes = array_diff_assoc($obj, $model->oldAttributes); 
                    if(!$model->validate()){
                        return $model;
                    }else{
                        $model->save(false);
                        return $model;
                    }
                }else{
                    return ['status' => 'error', 'message' => 'No Record Found', 'statusCode' => 422];
                }
            }else{
                return ['status' => 'error', 'message' => 'No Record Found', 'statusCode' => 422];
            }
        }
	}
    
	/**
	 * 
	 * Delete Property
	 * 
	 * @api {delete} api/web/v1/webhooks/properties/{id} Delete a Property
	 * @apiName DeleteProperty
	 * @apiVersion 0.1.1
	 * @apiGroup PropertyWebhooks
	 * @apiDescription To Delete a property (Webhook)
	 * ### Notes:
     *  - `Login|access token required`
     * 
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "message": "Property has been deleted successfully",
	 *       "data":  [],
	 *       "status": 200
	 *     }
	 * 
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 422 Unprocessable entity
	 *     {
	 *       "message": "No Record Found",
	 *       "data":  {
	 *         "status": "error"
	 *		 },
	 *       "status": 422
	 *     }
	 *     
	 */
    public function actionDelete($id = NULL){
        if($id){
			$model = $this->findModel($id); // find properties
			if($model->id){
                $model->delete();
                Auction::DeleteAll(['listing_id' => $id]);
                Gallery::DeleteAll(['tbl_pk' => $id, 'tbl_name' => 'tbl_properties']);
                $msg = CommonModel::textGlobalization('app', 'property_del_msg');
                return ['status' => 'success', 'message' => $msg, 'statusCode' => 200];
			}else{
                $msg = CommonModel::textGlobalization('app', 'no_record');
                return ['status' => 'error', 'message' => $msg, 'statusCode' => 422];
            }
		}else{
            return ['status' => 'error', 'message' => 'No Access given to delete all records', 'statusCode' => 401];
        }
	}  
    
    /** 
     * To get all passes api
     */
	public function actionIndex() {	
		return ["Cool"];
	}
     
    /*save property data into */
    public function actionSaveListProperties_aastha(){
        $data = file_get_contents("php://input");		
        $event = json_decode($data, true);
        if($event){
            $objectArray = $event['data']['items'];
            if(is_array($objectArray)){
                foreach($objectArray as $details){
                    $model = new Property();
                    $model->setAttributes($details);
                    if($model->save(false)){
                        $propertyArr[] = $model; // saving results into an array
                        $items['properties'] = $propertyArr;
                        $auctionmodel = new Auction();
                        $auctionmodel->setAttributes($details['auction']);
                        $auctionmodel->listing_id = $model->id;
                        $auctionmodel->save(false);
                        $auctionArr[] = $auctionmodel; // saving results into an array
                        $items['auctions'] = $auctionArr;
                        if(count($details['gallery'])){
                            foreach($details['gallery'] as $galleryData){
                                $gallery = new Gallery(); 
                                $gallery->setAttributes($galleryData);
                                $gallery->tbl_pk = $model->id;
                                $gallery->save(false);
                                $galleryArr[] = $gallery; // saving results into an array
                                $items['galleries'] = $galleryArr;
                            }
                        }
                    }
                }
                return $items;
               // return['properties' => $propertyArr, 'auctions' => $auctionArr, 'galleries' => $galleryArr];
                // return ['status' => 'success', 'message' => 'Property added successfully'];
            }
        }else{
            return $response;
        }
    }
    
	/* **
	 * Property Listing
	 *                                        
	 * @api {post} /api/web/v1/webhooks/property/sync-properties Properties Syncing
	 * @apiName PropertySyncing
	 * @apiVersion 0.1.1
	 * @apiGroup DataSyncingWebhooks
	 * @apiDescription To Sync Properties, Form-Data must be of raw type. Data will be posted in the below format by the cview team to sync with our database
	 *
	 * @apiSuccessExample Request Data:
     *    {  
     *       "type":"listings",
     *       "data":[  
     *          {  
     *             "id":261275,
     *             "attributes":{  
     *                "title":"Final land release in Crestmead Industrial Estate",
     *                "status":"current",
     *                "price_view":"Contact agent",
     *                "description":"The estate borders Crestmead, Browns Plains and Marsden - the fastest growing residential sector in the Logan region. It is a mixed industrial estate in a serviced industrial area designed to cater for the manufacturing industry.\n\nProvides access to interstate and overseas markets via the Gateway Arterial road and the Port of Brisbane.\n\nVisit www.industrial.edq.com.au for prices and availability.",
     *                "sale_price":451660,
     *                "listing_number":"11261275",
     *                "address":"Magnesium Drive QLD 4132",
     *                "business_email":null,
     *                "website":null,
     *                "valuation":null,
     *                "slug":"crestmead-4132-qld-land-development-c170ff27-4c73-45cf-a61a-fd15aac3080e",
     *                "sale_or_lease":"sale",
     *                "exclusivity":"exclusive",
     *                "floor_area":null,
     *                "floor_area_unit":null,
     *                "land_area":2249,
     *                "land_area_unit":"squareMeter",
     *                "total_car_space":null,
     *                "tenancy":null,
     *                "zoning":null,
     *                "authority":null,
     *                "sale_tax":"unknown",
     *                "tender_closing_date":null,
     *                "current_lease_expiry":null,
     *                "outgoings":null,
     *                "lease_price":null,
     *                "lease_price_show":null,
     *                "lease_period":null,
     *                "lease_tax":"unknown",
     *                "external_id":"CL17301272",
     *                "video_url":""
     *             },
     *             "location":{  
     *                "latitude":-37.899,
     *                "longitude":145.183
     *             },
     *             "relationships":{  
     *                "listing_documents":[],
     *                "agents":[  
     *                   {  
     *                      "id":6,
     *                      "attributes":{  
     *                         "email":"office_admin@commercialview.com.au",
     *                         "encrypted_password":"$2a$10$C/.SwrwhHHdcroc3nQkwCekv31CC9xTmHF.XN0xYepqODDUbLw7Ty",
     *                         "first_name":"George",
     *                         "last_name":"Clooney",
     *                         "mobile":"0400999888",
     *                         "phone":null,
     *                         "enquiry_email":null,
     *                         "agency_id":14,
     *                         "preferred_contact_number":"0400999888"
     *                      },
     *                      "relationships":{  
     *                         "agency":{  
     *                            "id":14,
     *                            "attributes":{  
     *                               "name":"Fitzroys - Melbourne",
     *                               "phone":"0392757777",
     *                               "fax":null,
     *                               "email":"email@fitzroys.com.au",
     *                               "web":null,
     *                               "twitter":null,
     *                               "facebook":null,
     *                               "logo_file_url":null,
     *                               "agency_id":"CV-5B1B4D",
     *                               "abn":null,
     *                               "slug":"fitzroys-melbourne"
     *                            }
     *                         },
     *                         "agent_personal_info":{}
     *                      }
     *                   }
     *                ],
     *                "agency":{  
     *                   "id":9,
     *                   "attributes":{  
     *                      "name":"Biz Aus Pty Pld",
     *                      "phone":"0417113473",
     *                      "fax":null,
     *                      "email":"phil@bizaus.com.au",
     *                      "web":null,
     *                      "twitter":null,
     *                      "facebook":null,
     *                      "logo_file_url":null,
     *                      "agency_id":"CV-CF1462",
     *                      "abn":null,
     *                      "slug":"biz-aus-pty-pld"
     *                   }
     *                },
     *                "images":[]
     *             }
     *          }
     *       ]
     *    }
     *    
     * @apiSuccessExample Success-Response:
     *    {
     *      "message": "Properties list sync up succesfully",
     *      "data": {
     *          "status": "success"
     *      },
     *      "status": 200
     *    }
     *    
     *    
	 * @apiErrorExample Error-Response in case of invalid / expired Access token:
	 *     HTTP/1.1 401 Unauthorized
	 *     {
	 *       "name": "Unauthorized",
     *       "message": "Your request was made with invalid credentials.",
	 *       "code": 0,
	 *       "status": 401,
	 *       "type": "yii\\web\\UnauthorizedHttpException"
	 *     }
	 *       
     */
    public function actionSyncProperties(){
        $data = file_get_contents("php://input");		
        $event = json_decode($data, true);
        $galleryArr = $auctionArr = [];
        if($event){
            $objectArray = $event['data'];
            if(is_array($objectArray)){
                foreach($objectArray as $details){
                    $model = Property::findOne(['cview_listing_id' => $details['attributes']['listing_number']]);
                    if(empty($model)){
                        $model = new Property();
                        $model->setAttributes($details['attributes']);
                        $model->title = $details['attributes']['title'];
                        $model->price = $details['attributes']['sale_price'];
                        $model->latitude = $details['location']['latitude'];
                        $model->longitude = $details['location']['longitude'];
                        $model->lease_price_show = empty($details['attributes']['lease_price_show']) ? 0 : $details['attributes']['lease_price_show'];
                        $model->propertyType = 1; // default set
                        $model->video_url = $details['attributes']['video_url'];
                        $model->cview_listing_id = $details['attributes']['listing_number'];
                        $model->created_by = 0; // Because they are syncing so update it with 0
                        $model->updated_by = 0; // Because they are syncing so update it with 0
                        if($model->save(false)){
                            // for listing_documents
                            foreach($details['relationships']['listing_documents'] as $document){
                                $docGallery = new Gallery();
                                $docGallery->tbl_pk = $model->id;
                                $docGallery->type = 'document';
                                $docGallery->tbl_name = 'tbl_properties';
                                $docGallery->file_name = $document['Attributes']['file_name'];
                                $docGallery->url = $document['Attributes']['File_url'];
                                $docGallery->save(false);
                            }
                            // for Property's video_url
                            if($details['attributes']['video_url']){
                                $videoGallery = new Gallery();
                                $videoGallery->tbl_pk = $model->id;
                                $videoGallery->type = 'video';
                                $videoGallery->tbl_name = 'tbl_properties';
                                $videoGallery->url = $details['attributes']['video_url'];
                                $videoGallery->save(false);
                            }
                            // for Property's Gallery
                            foreach($details['relationships']['images'] as $data){
                                $imageGallery = new Gallery();
                                $imageGallery->tbl_pk = $model->id;
                                $imageGallery->type = 'image';
                                $imageGallery->tbl_name = 'tbl_properties';
                                $imageGallery->url = $data['attributes']['file_url'];
                                $imageGallery->save(false);                                
                            }
                        }
                        if($details['relationships']['agency']['attributes']){
                            $agencyModel = Agency::findOne(['cv_agency_id' => $details['relationships']['agency']['attributes']['agency_id']]);
                            if(empty($agencyModel)){
                                $agencyModel = new Agency();
                                $agencyModel->scenario = "create"; 
                                $agencyModel->setAttributes($details['relationships']['agency']['attributes']); // set posted attributes to the model Agency
                                $agencyModel->mobile = $details['relationships']['agency']['attributes']['phone'];
                                $agencyModel->logo = $details['relationships']['agency']['attributes']['logo_file_url'];
                                $agencyModel->slug = $details['relationships']['agency']['attributes']['slug'];
                                $agencyModel->abn = $details['relationships']['agency']['attributes']['abn'];
                                $agencyModel->cv_agency_id = $details['relationships']['agency']['attributes']['agency_id'];
                                if(!$agencyModel->validate()){
                                    return $agencyModel;
                                }else{
                                    $agencyModel->save(false);
                                }
                            }else{
                                $agencyModel->setAttributes($details['relationships']['agency']['attributes']); // set posted attributes to the model Agency
                                $agencyModel->mobile = $details['relationships']['agency']['attributes']['phone'];
                                $agencyModel->logo = $details['relationships']['agency']['attributes']['logo_file_url'];
                                $agencyModel->slug = $details['relationships']['agency']['attributes']['slug'];
                                $agencyModel->abn = $details['relationships']['agency']['attributes']['abn'];
                                $agencyModel->save(false);
                            }                          
                        }
                        if($details['relationships']['agents']){
                            foreach($details['relationships']['agents'] as $agentData){
                                $agentModel = Agent::findOne(['email'=>$agentData['attributes']['email'], 'user_type'=>10, 'app_id'=>2]);
                                if(empty($agentModel->id)){
                                    $agentModel = new Agent();
                                    $agentModel->setAttributes($agentData['attributes']);
                                    $agentModel->firstName = $agentData['attributes']['first_name'];
                                    $agentModel->lastName = $agentData['attributes']['last_name'];
                                    $agentModel->phone = $agentData['attributes']['mobile'];
                                    if($agentData['attributes']['encrypted_password']) $agentModel->password_hash = $agentData['attributes']['encrypted_password'];
                                    $agentModel->agency_id = $agencyModel->id;
                                    if($agentModel->save(false)){
                                        $agentModel->user_code = 'AW-'.$agentModel->username;
                                        $agentModel->save('false');
                                        $profileModel = new Profiles();
                                        $profileModel->user_id = $agentModel->id;
                                        $profileModel->officePhone = $agentData['attributes']['phone'];
                                        $profileModel->officeFax = $agentData['attributes']['phone'];
                                        $profileModel->officeFax = $agentData['attributes']['phone'];
                                        $profileModel->enquiry_email = $agentData['attributes']['enquiry_email'];
                                        $profileModel->preferred_contact_number = $agentData['attributes']['preferred_contact_number'];
                                        $profileModel->save(false);
                                    }
                                }
                            }
                        }
                    }
                }
                $msg = CommonModel::textGlobalization('app', 'prop_sync_successfull');
                return ['status' => 'success', 'message' => $msg, 'statusCode' => 200];
            }
        }else{
            $msg = CommonModel::textGlobalization('app', 'invalid_data');
            return ['status' => 'error', 'message' => $msg, 'statusCode' => 422]; 
        }
    }
    
    protected function findAuctionModel($id)
    {
        if (($model = Auction::findOne(['listing_id'=>$id])) !== null) {
            return $model;
        } else {
            return new Auction();
        }
    }
    
	protected function findModel($id)
    {
        if (($model = Property::findOne($id)) !== null) {
            return $model;
        } else {
            return new Property();
        }
    }
    
	/* ** 
	 * 
	 * Fetch the list of all properties in the system
     * @api {get} api/web/v1/webhooks/property/all Property Listing
	 * @apiName PropertyListing
	 * @apiVersion 0.1.1
	 * @apiGroup PropertyWebhooks
	 * @apiDescription List all Property, Result Set is in Pagination of 10 records in each page. To fetch resultset in pagination url would like this: api/web/v1/webhooks/property/all?page=2, Where page is the page number.
     * @apiSuccessExample Success-Response:
     *    HTTP/1.1 200
     *    {
     *      "message": "",
     *      "data": {
     *        "items": [
     *          {
     *            "id": 178,
     *            "title": "Final land release in Crestmead Industrial Estate",
     *            "description": "The estate borders Crestmead, Browns Plains and Marsden - the fastest growing residential sector in the Logan region. It is a mixed industrial estate in a serviced industrial area designed to cater for the manufacturing industry.\n\nProvides access to interstate and overseas markets via the Gateway Arterial road and the Port of Brisbane.\n\nVisit www.industrial.edq.com.au for prices and availability.",
     *            "status": 0,
     *            "propertyType": 1,
     *            "price": 0,
     *            "landArea": null,
     *            "land_area_unit": "squareMeter",
     *            "floorArea": null,
     *            "floor_area_unit": null,
     *            "conditions": 0,
     *            "address": "Magnesium Drive QLD 4132",
     *            "latitude": "-37.899",
     *            "longitude": "145.183",
     *            "unitNumber": 0,
     *            "streetNumber": 0,
     *            "streetName": null,
     *            "suburb": null,
     *            "city": null,
     *            "state": null,
     *            "postcode": null,
     *            "country": null,
     *            "parking": 0,
     *            "parkingList": null,
     *            "parkingOther": null,
     *            "floorplan": null,
     *            "media": null,
     *            "video": null,
     *            "videoImage": null,
     *            "document": null,
     *            "school": 0,
     *            "communityCentre": 0,
     *            "parkLand": 0,
     *            "publicTransport": 0,
     *            "shopping": 0,
     *            "bedrooms": 0,
     *            "bathrooms": 0,
     *            "features": null,
     *            "tags": null,
     *            "created_by": 383,
     *            "propertyStatus": 0,
     *            "updated_by": 383,
     *            "created_at": "2018-05-31 06:53:52",
     *            "updated_at": "2018-05-31 06:53:52",
     *            "is_featured": 0,
     *            "cview_listing_id": "11261275",
     *            "is_deleted": 0,
     *            "business_email": null,
     *            "website": null,
     *            "valuation": null,
     *            "exclusivity": "exclusive",
     *            "tenancy": null,
     *            "zoning": null,
     *            "authority": null,
     *            "sale_tax": "unknown",
     *            "tender_closing_date": null,
     *            "current_lease_expiry": null,
     *            "outgoings": null,
     *            "lease_price": null,
     *            "lease_price_show": 0,
     *            "lease_period": null,
     *            "lease_tax": "unknown",
     *            "external_id": "CL17301272",
     *            "video_url": "",
     *            "slug": "crestmead-4132-qld-land-development-c170ff27-4c73-45cf-a61a-fd15aac3080e",
     *            "distance": "",
     *            "propertyTypename": "Commercial Farming",
     *            "auctionDetails": {
     *              "live": false,
     *              "past": true,
     *              "upcoming": false,
     *              "seconds": 0
     *            },           
     *            "gallery": [],
     *            "auction": null,
     *            "saveauction": null,
     *            "sold": "0",
     *            "calendar": 0
     *          }
     *        ],
     *        "_links": {
     *          "self": {
     *            "href": "http://localhost/AlphaWallet/html/api/web/v1/webhooks/property/all?page=1"
     *          },
     *          "next": {
     *            "href": "http://localhost/AlphaWallet/html/api/web/v1/webhooks/property/all?page=2"
     *          },
     *          "last": {
     *            "href": "http://localhost/AlphaWallet/html/api/web/v1/webhooks/property/all?page=4"
     *          }
     *        },
     *        "_meta": {
     *          "totalCount": 80,
     *          "pageCount": 4,
     *          "currentPage": 1,
     *          "perPage": 20
     *        }
     *      },
     *      "status": 200
     *    }
	 *
	 *
	 * @return Object(common\models\Property)
	 */
    public function actionAll(){
       // $propertymodel = Property::find()->joinWith('agent')->joinWith('agency')->joinWith('auction')->joinWith('gallery')->joinWith('bidding')->orderBy('id desc');
        $propertymodel = Property::find()->joinWith('auction')->joinWith('gallery')->joinWith('bidding')->orderBy('id desc');
        $result = new ActiveDataProvider([
            'query' => $propertymodel,
        ]);
        return $result;
    }

}