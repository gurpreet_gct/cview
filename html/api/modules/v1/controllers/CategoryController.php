<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use api\modules\v1\models\UserCategory;
use api\modules\v1\models\Category;
use common\models\Categories;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
class CategoryController extends ActiveController
{    
        public $modelClass = 'api\modules\v1\models\UserCategory';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);                    

		return $actions;
	}

  	
  
    public function actionIndex()
    {        
/*	$model=new Categories();
	$model->updateCategoryPath();	
	die;*/
       /*  $result = Category::find(['active'=>1])
					->select(['name','lvl','root','lft','ifnull(category_id,0) as isSelected'])
					->joinWith('userCategory', false, 'LEFT JOIN',['user_id'=>2])
					
					->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC])
					->asArray()
					->All();   */
					
					
					
		$requiredResult=array();
	  $id=Yii::$app->user->id;

		
		$query = new Query;
		$query	->select(['tbl_categories.id as id','name','icon','imageurl', 'lvl','root','lft','ifnull(status,0) as isSelected'])  
				->from('tbl_categories')
				->leftJoin('tbl_userCategory', 'tbl_userCategory.category_id = tbl_categories.id and tbl_userCategory.user_id='.$id)
				->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC]); 	
		$command = $query->createCommand();
		$data = $command->queryAll();
			$requiredResult=array();
		$oldLevel=0;
		$levelKeys=array();
     
		foreach($data as $key=>$value)
		{
			//$value['path']="root/";
			$key=$value['id'];
            $value['name']=htmlspecialchars_decode($value['name']);
           
			if($value['lvl']==0)
			{
		//	$value['path']=$value['path'].$value['name'];
			$requiredResult[$key]=$value;
				
			}
			else
			{	
				$array_ptr=&$requiredResult;
				
				for($i=0; $i<$value['lvl']; $i++)
				{
					//$value['path']= $array_ptr[ strval($levelKeys[$i])]['path'];						 
					 $array_ptr = &$array_ptr[ strval($levelKeys[$i]) ]['subCategory'];
					 
				}
			//	$value['path']=$value['path']."/".$value['name'];
				$array_ptr[$key]=$value;
			//print_r($requiredResult);die;
			}            
    
			$levelKeys[$value['lvl']]=$value['id'];			
		}	
      // recursiveRemoval($array, 'subCategory');
        
       $category=new Category();
      $category->recursiveRemoval($requiredResult,'subCategory');
      $requiredResult=is_array($requiredResult)?array_values($requiredResult):$requiredResult;
   //   DFS($requiredResult);
     //   die;
     
     
   
     
        return ($requiredResult);
    }
    
   function recursiveRemoval(&$array, $val)
{
    if(is_array($array))
    {
        foreach($array as $key=>&$arrayElement)
        {
            if(is_array($arrayElement))
            {
                recursiveRemoval($arrayElement, $val);
            }
            else
            {
                if($arrayElement == $val)
                {
                    reset($array[$key]);
                }
            }
        }
    }
}
    
    public function actionUpdate($id)
	{
		
		$user_id=Yii::$app->user->id;
		$userCategory=UserCategory::findOne(['user_id'=>$user_id,'category_id'=>$id]);
		if($userCategory)
		{
			$userCategory->status=1;
			$userCategory->save();
		}
		else
		{
			$userCategory=new UserCategory();
			$userCategory->user_id=$user_id;
			$userCategory->category_id=$id;
			$userCategory->status=1;
			$userCategory->save();
		}
		return ["message"=>"Category added successfully."];
	}
    public function actionDelete($id)
    {
        
        $user_id=Yii::$app->user->id;
		$userCategory=UserCategory::findOne(['user_id'=>$user_id,'category_id'=>$id,'status'=>1]);
		if($userCategory)
		{
			$userCategory->status=0;
			$userCategory->save();
			return ["message"=>"Category removed successfully."];
		}
		else
			return ["message"=>"Category already removed."];
		
    }
    
    public function actionUpdatebatch()
    {
        if(isset(Yii::$app->request->post()['categories']))
        {
        $user_id=Yii::$app->user->id;
        $userCategory=UserCategory::find()->where(['user_id'=>$user_id])->select('category_id as id,status as isSelected')->asArray()->all();
        $rs=(json_decode(Yii::$app->request->post()['categories'],true));      
         $newCategory=ArrayHelper::map($rs, 'id', 'isSelected');
         $oldCategory=ArrayHelper::map($userCategory, 'id', 'isSelected');
         /* All element having value 0 for isSelected*/
         $newZero=array_keys($newCategory,0);
         $newZero=(array_fill_keys(array_values($newZero),0));
         /* All element having value 1 for isSelected*/
         $newOne=array_keys($newCategory,1);
         $newOne=(array_fill_keys(array_values($newOne),1));
         
         /* All element having value 0 for isSelected*/
         $oldZero=array_keys($oldCategory,0);
         $oldZero=(array_fill_keys(array_values($oldZero),0));
         /* All element having value 1 for isSelected*/
         $oldOne=array_keys($oldCategory,1);
         $oldOne=(array_fill_keys(array_values($oldOne),1));
         
         /* All category need to be set zero in db */
         $catCommon=array_intersect_key($oldCategory,$newZero);         
         $catToBeZero=array_diff_key($catCommon,$oldZero);
         
         /* All category need to be insert with 1 in db */
         $catToBeAdd=array_diff_key($newOne,$oldCategory);
         
         /* All category need to be update with 1 in db */
         $catToBeOne=array_intersect_key($newOne,$oldZero);
        
       if(count($catToBeZero)){  
           $updateZero=sprintf("update %s set status=0, updated_at=%s where user_id=%s and category_id in (%s)",UserCategory::getTableSchema()->fullName,time(),$user_id,implode(',',array_keys($catToBeZero)));
            Yii::$app->db->createCommand($updateZero)->execute();
       }
        if(count($catToBeOne)){
            $updateOne=sprintf("update %s set status=1, updated_at=%s where user_id=%s and category_id in (%s)",UserCategory::getTableSchema()->fullName,time(),$user_id,implode(',',array_keys($catToBeOne)));
            Yii::$app->db->createCommand($updateOne)->execute();
        }
        array_walk($catToBeAdd, function(&$key, $value,$user_id) { $key = ['category_id'=>$value,'status'=>1,'user_id'=>$user_id,'created_at'=>time(),'updated_at'=>time()]; },$user_id) ;
        $catToBeAdd=(array_values($catToBeAdd));
        if(count($catToBeAdd))
            Yii::$app->db->createCommand()->batchInsert(UserCategory::tableName(),array_keys($catToBeAdd[0]), $catToBeAdd)->execute();

        return['message'=>'Categories updated successfully.'];
        }
        else
            return['message'=>'categories can not be blank.',['field'=>'categories','message'=>'categories can not be blank.'],'statusCode'=>422];
    }
}