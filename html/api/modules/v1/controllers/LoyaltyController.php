<?php
namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use common\models\Loyaltypoint;
use common\models\Profile;
//use common\models\Cardtype;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class LoyaltyController extends Controller
{    
        public $modelClass = 'common\models\Loyaltypoint';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index']);                    

		return $actions;
	}

  	
   /* save new Id information */    
    /*public function actionCreate(){
		$user_id=Yii::$app->user->id;  
		$model= new Address();
		$model->load(Yii::$app->getRequest()->getBodyParams(), ''); 
		$model->created_by=$user_id;
		$model->updated_by=$user_id;
        if($model->validate()){
			$model->save();
		}
         
		return $model;
  }*/
    
  /*  public function actionUpdate($id){
		
       //$user_id=Yii::$app->user->id;
		
		$model= new Address();
		$model= Address::findOne(['id' => $id]);
		
		$model->load(Yii::$app->getRequest()->getBodyParams(), '');  
		
		 //$model->user_id=$user_id;
		  if($model->validate()){
			$model->save();
			
		}
		return $model;
         
		
    
    }*/
    
    /* To get all Loyaltypoint*/
   public function actionIndex(){
	
	  // $model=Loyaltypoint::find()->all();
	   $query = new Query;
		$query->select([Loyaltypoint::tableName().'.user_id',Loyaltypoint::tableName().'.store_id',Loyaltypoint::tableName().'.loyaltypoints',Profile::tableName().'.brandName',Profile::tableName().'.brandLogo',Profile::tableName().'.backgroundHexColour',Profile::tableName().'.foregroundHexColour',Profile::tableName().'.foregroundHexColour'])  
		->from(Loyaltypoint::tableName())
		->leftJoin(Profile::tableName(), Loyaltypoint::tableName().'.store_id = '.Profile::tableName().'.user_id');
		$command = $query->createCommand();
		$model = $command->queryAll();
		return $model;
	
            
   }
   
	
}
