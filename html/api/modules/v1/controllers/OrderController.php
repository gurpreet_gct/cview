<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use api\components\Controller;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use common\models\Order;
use common\models\User;
use common\models\OrderItem;
use common\models\Workorders;
use common\models\OrderSubItems;
use api\modules\v1\models\OrderApi;
use api\modules\v1\models\Foodordersapi;
use common\models\Profile;
use common\models\Product;
use common\models\FoodordersItem;
use yii\db\Expression;

class OrderController extends Controller
{    
        public $modelClass = 'common\models\Order';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
    public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['delete'],$actions['view'],$actions['index']);                    

		return $actions;
	}

	/* order */
	public function actionIndex()
	{		
		   return new ActiveDataProvider([ 
            'query' => Order::find()
			->where([Order::tableName().'.user_id'=>Yii::$app->user->id])			
			->joinWith('orderitems')
			->joinWith('orderitems.ordersubitems')
			 ->orderBy("id asc")       			
        ]);
		 
	}

  	   /* To get all cards*/
   public function actionView($id)
   {
        $user_id=Yii::$app->user->id;
         return new ActiveDataProvider([ 
            'query' => Order::find()
			->where([Order::tableName().'.user_id'=>Yii::$app->user->id])
			->where([Order::tableName().'.id'=>$id])
			
		->joinWith('orderitems')
		->joinWith('orderitems.ordersubitems')
		 ->orderBy("id desc")
       			
        ]);
            
   }
   /* order collected api */
   public function actionFinalize(){	
	
	$model = new OrderApi ();
	$model->scenario = 'finalize';
	$postData = Yii::$app->getRequest()->getBodyParams();
	$model->load($postData, '');  	
	if($model->validate()){		
		$Order = OrderItem::findOne(['order_id'=>$model->id]);
		$mainOrder = Order::findOne($model->id);
		if($mainOrder!=''){
			$Order->status = 4;
			$Order->save(false);
			$mainOrder->status = 4;
			$mainOrder->save(false);
			return [
				'statusCode'=>200,
				'message'=>'Thanks for collecting your order.'
			];
		}else{
		 return ["message"=>"Order not Found","statusCode"=>422];
		}
		
	}else{
		return $model;
	
	}
		  
  }
  
   /* order is active for kitchen api */
   public function actionActiveOrder(){	
	
	$model = new OrderApi ();	
	$model->scenario = 'active-order';
	$postData = Yii::$app->getRequest()->getBodyParams();
	$model->load($postData, '');  	
	if($model->validate()){		
		$Order = OrderItem::findOne(['order_id'=>$model->id]);
		$mainOrder = Order::findOne($model->id);
		if($mainOrder->food_oid !=0 && $mainOrder->food_oid!=''){
			$ord   = FoodordersItem::tableName();
			$pro   = Profile::tableName();
			$ford   = Foodordersapi::tableName();
			$brand = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/profile/').'/';
			$orderItem = FoodordersItem::find()->select(["$ord.pname as name","$ord.product_id","$ord.order_id as food_order_id","FROM_UNIXTIME('$ord.created_at') as created_at","$pro.companyName as storeName","CONCAT('$brand',$pro.brandLogo) as storeImage",new Expression("$mainOrder->status as status"),new Expression("$Order->store_id as storeID"),new Expression("$mainOrder->subtotal as totalPrice"),new Expression("$mainOrder->tax_amount as tax_amount"),new Expression("$mainOrder->qty_ordered as qty"),new Expression("$mainOrder->id as order_id"),new Expression("$mainOrder->order_type as order_type"),new Expression("$mainOrder->status as status")])
			->join('INNER JOIN',$ford,"$ford.id=$ord.order_id")
			->join('INNER JOIN',$pro,"$pro.user_id=$ford.store_id")
			->where("$ford.id=".$mainOrder->food_oid)
			->asArray()
			->one();
			$Order->status = $postData['status'];
			$Order->save(false);
			$mainOrder->status = $postData['status'];
			$mainOrder->save(false);
			$user = User::find()->where('id='.$Order->user_id)
							->one();
		$userDevice[$user->device_token] = $user->device;			
		if($user->device_token){
			$message = array('6'=>'Thanks for order #'.$mainOrder->id.'. The kitchen has begun preparing your food. We will let you know when it is ready for collection.','7'=>'Your order #'.$mainOrder->id.' is ready for collection.');	
			yii::$app->pushNotification->send($userDevice,["msg"=>$message[$postData['status']]],0,$orderItem);
		}
		 return [
				'statusCode'=>200,
				'message'=>'Order active now',
				'food_order_id'=>$mainOrder->food_oid,
				'id'=>$mainOrder->id
		]; 
		}else{
		 return ["message"=>"Order not Found","statusCode"=>422];
		}
		
	}else{
		return $model;
	
	}
		  
  }
   public function actionDelete($id)
   {
	
	 $Order = Order::findOne($id);
	 if($Order){
		 $oi = OrderItem::tableName();
		 $wo = Workorders::tableName();
		 $orderItem = OrderItem::find()
					 ->select(["$oi.deal_id","$wo.returnWithin","FROM_UNIXTIME($oi.created_at) as createdON","$oi.created_at"])
					 ->join('INNER JOIN',$wo,"$wo.id=$oi.deal_id")
					 ->where('order_id='.$Order->id)
					 ->asArray()
					 ->one();
		 $now = time();	 
		 $datediff = $now - $orderItem['created_at'];
		 $days =  floor($datediff/(60*60*24));
		 if($orderItem['returnWithin']>=$days){
		 	$Order->status = 5; // order status 5 = deleted/cancelled order
			$Order->save(false); 
			$OrderI = OrderItem::findOne(['order_id'=>$id]);
			$OrderI->status = 5;
			$OrderI->save(false); 
			return ['message'=> 'Order cancelled successfully.', 'statusCode' => 204];
						
		 }else{
			return ['message'=> 'Return Period end.', 'statusCode' => 404];  
		 }	
		
	 }else{
		return ['message'=> 'No result found.', 'statusCode' => 404]; 
	 }
	 
   }
}
