<?php
namespace api\modules\v1\controllers;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;

use common\models\User;
use common\models\Beaconmanager;
use common\models\Workorders;
use common\models\Pass;
use common\models\Workorderpopup;
use common\models\Deal;
use common\models\UserPsa;
use common\models\Profile;
use api\modules\v1\models\UserCategory;
use yii\helpers\ArrayHelper;
use yii\db\Expression;


use common\models\Store;
use api\modules\v1\models\Category;
use api\modules\v1\models\Manufacturer;
use api\modules\v1\models\Storelocator;

/*
* Used to show full message of notification
*
*/

class PayloadmessageController extends ActiveController
{
	public $modelClass = 'common\models\User';
	
	
	public function behaviors(){
		
		$behaviors = parent::behaviors();
			
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		
		return $behaviors;
		
	}
	
	public function actions(){
		
		$actions = parent::actions();

		// disable the "create" and "update" actions
		
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);                     

		return $actions;
		
	}
	
	
	/*
	 * Update lat/long of user based on geo location
	 * 
	*/
	
	
	public function actionView(){
		
		// $user_id			= 	448;
		
		$user_id			= 	Yii::$app->user->id;
          
		$rq					=	Yii::$app->getRequest()->get();
		
		$resultcode 		= 	array();
		   
		if($rq){
				 	
			$workorderId 	=  	$rq['id'];
			
			$dataProvider =                     
				  new ActiveDataProvider([ 
					'query' => Deal::find()->select([UserPsa::tableName().'.user_id',Workorderpopup::tableName().'.*',Pass::tableName().'.isUsed'])
										  
					->joinWith('workorderpopup')
					->joinWith('pass')	
					->joinWith(['workorders'=> function ($q)  {						
						$q->where(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))));	
						}
					],false,'INNER JOIN')
					->joinWith(['userpsa'=>function($q) use ($user_id) {
                                  $q->onCondition(UserPsa::tableName().'.user_id= '. $user_id);
                                  $q->andOnCondition(UserPsa::tableName().'.status!= 0');                                
                                  }],false,'LEFT JOIN')
					->andWhere([Workorders::tableName().'.status' =>1])
					->andWhere([Workorders::tableName().'.id' =>$workorderId])
								
					->andWhere("(CASE rulecondition WHEN 1 THEN IF(Loyalty1 & Loyalty2,(CASE Loyalty3     
		WHEN 1 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 day) AND user_id = $user_id ) WHEN 2 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 week) AND user_id = $user_id) WHEN 3 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 month) AND user_id = $user_id) WHEN 4 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 year) AND user_id = $user_id) END),false) ELSE rulecondition = 0 END)") 
		
					->andWhere("(CASE rulecondition WHEN 1 THEN IF(userenter,userenter = 1,IF(usereturned, usereturned = 1, usereturned = 0 )) ELSE rulecondition = 0 END)") 
								
					->andWhere("(CASE dayparting WHEN 1 THEN dayparting = 1 WHEN 3 THEN IF(daypartingcustom3 & daypartingcustom2,(daypartingcustom3 <= '".date('H', time())."' and daypartingcustom3end >= '".date('H', time())."') || (daypartingcustom2 <= '".date('H', time())."' and daypartingcustom2end >= '".date('H', time())."') || daypartingcustom1 <= '".date('H', time())."' and daypartingcustom1end >= '".date('H', time())."',IF(daypartingcustom2 & daypartingcustom1,(daypartingcustom2 <= '".date('H', time())."' and daypartingcustom2end >= '".date('H', time())."') || daypartingcustom1 <= '".date('H', time())."' and daypartingcustom1end >= '".date('H', time())."',daypartingcustom1 <= '".date('H', time())."' and daypartingcustom1end >= '".date('H', time())."')) ELSE rulecondition = 0 END)") 
								
					->andWhere("(CASE temperature WHEN 1 THEN temperature = 1 WHEN 2 THEN IF(temperaturestart & temperaturend,temperaturestart = 9 and temperaturend >= 15, true) ELSE rulecondition = 0 END)")
								
					->andWhere("(CASE rulecondition WHEN 1 THEN IF(behaviourDwell & behaviourDwellMinutes, (CASE behaviourDwellMinutes WHEN 1 THEN (SELECT (max(unix_timestamp(detect_at))-min(unix_timestamp(detect_at)))/60 >= behaviourDwell FROM tbl_beacon_detect_logs) WHEN 2 THEN (SELECT (max(unix_timestamp(detect_at))-min(unix_timestamp(detect_at))) >= behaviourDwell FROM tbl_beacon_detect_logs)END), false) ELSE rulecondition = 0 END)") 
								
					->andWhere("(CASE rulecondition WHEN 1 THEN IF(behaviourvisit1 & behaviourvisit2 & behaviourvisit3, (CASE behaviourvisit3 WHEN 1 THEN (SELECT count(*) >= behaviourvisit1 FROM ".UserPsa::tableName()." WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 DAY ) AND user_id = $user_id AND status= 0 ) WHEN 2 THEN (SELECT count(*) >= behaviourvisit1 FROM ".UserPsa::tableName()." WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 WEEK) AND user_id = $user_id AND status= 0) WHEN 3 THEN (SELECT count(*) >= behaviourvisit1 FROM ".UserPsa::tableName()." WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 MONTH) AND user_id = $user_id AND status= 0) END) ,false) ELSE rulecondition = 0 END)")	 
								
					->groupBy('id')    
					->having(UserPsa::tableName().'.user_id is null')   
					->orderBy("id desc"),          
							  'pagination' => [
								   'pageSize' => 1,
								   ], 
				   
						]);
				
                
               						
				if($dataProvider->totalCount > 0) {                        
				  
					$data = $dataProvider->getModels();
				 
					
					if(count($data[0]->workorder_id)>0){
					  
						$query=sprintf("insert into %s (workorder_id,user_id,created_at,status,type) values(%s,%s,%s,0,1) on duplicate key update status=0",
                      UserPsa::tableName(),$data[0]->workorder_id,Yii::$app->user->id,time());
                      Yii::$app->db->createCommand($query)->execute(); 	  
					}
					
				  
					return $dataProvider;
								   
				}else{
					
				   $resultcode['message'] = 'No data available';                    
				   return $resultcode;
			   }
                   
            }else{
               
                   $resultcode['message'] = 'invalid data';
                   return $resultcode;
           }
            
	}
	
	
}
