<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use common\models\Pass;
use common\models\Property;
use common\models\Workorderpopup; 
use common\models\Workorders;
use common\models\User;
use common\models\Profile;
use common\models\Qr;
use common\models\Offertrack;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\PropertySearch;
use common\models\Auction;
use common\models\Gallery;
use common\models\CommonModel;

/**
 * PropertyController
 *
 * Property management methods for APIs are defined here.
 * Form-Data must be x-www-form-urlencoded
 */
class PropertyController extends Controller
{    
    public $modelClass = 'common\models\Property';
    
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		// $behaviors['authenticator'] = [
		// 	'class' => CompositeAuth::className(),
		// 	'authMethods' => [
		// 		HttpBasicAuth::className(),
		// 		HttpBearerAuth::className(),
		// 		QueryParamAuth::className(),
		// 	],
		// ];
		return $behaviors;
	}
    
  	public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['delete'], $actions['index'], $actions['update'],$actions['create'], $actions['view']); 

		return $actions;
	}
	
	/** 
	 * 
	 * Save new property information
	 * - URI: *api/web/v1/properties/create
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - title (string)|required 
	 *   - description (string)|required
	 *   - propertyType (number)| 1,2,3... (Represents ids from tbl_property_types)
	 *   - price (number) | eg. 1222.23 (The Cost of the property)
	 *   - landArea (string) | optional
	 *   - floorArea (string) | optional
	 *   - address (string) | optional
	 *	 - latitude (number) | The property latitude value, eg. 37.8136
	 *   - longitude (number) | The property longitude, eg. 144.9631
	 *	 - streetName (string) | The property street name
	 *   - suburb (string) | The suburb of the property
	 *   - city (string) | The city where the property is located
	 *   - state (string) | The state/county where the property is located
	 *   - country (number) | Represents the country code, check for tbl_countries, eg. 101 for India
	 *   - parking (number) | (1|0) 1 => 'available', eg 1
	 *   - parkingList (string) (Optional)
	 *   - media[] (files) | Set of images(base64) files for property carousel
	 *   - video[0] (string) | (Optional) Link to video, eg. youtube link (TBD) 
	 *   - videoImage[0] (string) | (Optional) Link to video thumbnail 
	 *   - school (number) | (1|0), 1 => 'available', eg 1
	 *   - communityCentre (number) | (1|0), 1=> 'available', eg 1
	 *   - parkLand (number) | (1|0), 1=> 'available'
	 *	 - publicTransport (number) | (1|0), 1=> 'available'
	 *   - shopping (number) | (1|0), 1=> 'available'
	 *   - bedrooms (number) (Optional) eg. 5
	 *   - bathrooms (number) (Optional) eg. 3
	 *   - features (string) (Optional) Text representing the description about the features of the property
	 *	 - tags (string) (Optional) Set of strings separated by comma(,) egs, countryside, newly built, fully-furnished 
	 * 	 - user_id (number) (Optional) TBD
	 *   - created_by (string) (Optional) TBD
	 *   - is_featured (number) (number) | (1|0), default 0
	 *
	 *  @api {post} api/web/v1/property/create Create
	 *  @apiName CreateProperty
	 *  @apiVersion 0.1.1
	 *  @apiGroup Property
	 *  @apiDescription To create property, Form-Data must be x-www-form-urlencoded
	 * 
	 *  @apiParam {String} title title of property.
	 *  @apiParam {String} description (Optional) A description of the property.
	 *  @apiParam {Number} propertyType   You will get it from PropertyTypes - All Active PropertyTypes API, Please refer the same. 
	 *  @apiParam {String} cost (Optional) eg. '1222.23', the Cost of the property   
	 *  @apiParam {String} landArea
	 *  @apiParam {String} floorArea
	 *  @apiParam {String} address
	 *  @apiParam {Number} latitude (Optional) The property latitude value, eg. 37.8136
	 *  @apiParam {Number} longitude (Optional) The property longitude, eg. 144.9631
	 *  @apiParam {String} streetName (Optional) The property street name
	 *  @apiParam {String} suburb (Optional) The suburb of the property
	 *  @apiParam {String} city (Optional) The city where the property is located
	 *  @apiParam {String} state (Optional) The state/county where the property is located
	 *  @apiParam {Number} country (Optional) Represents the country code, check for  tbl_countries, eg. 101 for India
	 *  @apiParam {Number} parking (number) (1|0) 1 => 'available', eg 1
	 *  @apiParam {Array}  media[] (Optional) Set of images(base64) files for property carousel
	 *  @apiParam {String} video[0] (Optional) Link to video, eg. youtube link (TBD)
	 *  @apiParam {String} videoImage[0] (Optional) Link to video thumbnail
     *  @apiParam {String} document[0] (Optional) Property document url.
     *  @apiParam {String} auction_document[0] (Optional) Auction prescribed document url
	 *  @apiParam {Number} school (1|0), 1 => 'available', eg 1
	 *  @apiParam {Number} communityCentre (1|0), 1=> 'available', eg 1
	 *  @apiParam {Number} parkLand (1|0), 1=> 'available'
	 *  @apiParam {Number} publicTransport (1|0), 1=> 'available'
	 *  @apiParam {Number} shopping (1|0), 1=> 'available'
	 *  @apiParam {Number} bedrooms (Optional) eg. 5
	 *  @apiParam {Number} bathrooms (Optional) eg. 3
	 *  @apiParam {String} features (Optional) Text representing the description about the feaures of the property
	 *  @apiParam {String} tags (Optional) Set of strings separated by comma(,) egs, countryside, newly built, fully-furnished 
	 *  @apiParam {Number} user_id (Optional) TBD
	 *  @apiParam {String} created_by (Optional) TBD
	 * 
	 * 
	 *  @apiSuccessExample Success-Response:
	 *     HTTP/1.1 201 Created
	 *     {
     *    	"message": "",
     *    	"data": [
     *    		{
     *				"id": 1,
     *				"title": "Brand New Office/Warehouse Development",
     *				"description": "Cameron is pleased to offer this brand new Office/ Warehouse complex that consists of 23 units ranging between 240sqm to 609sqm. Offering easy access to major arterial roads including Frankston Dandenong Road, Abbotts Road, South Gippsland Freeway and Eastlink via Fox Drive and Discovery Road.\n\nEdison Park, being Dandnenong South's newest Industrial Park development; is situated on the corner of Taylors Road and Edison Road and will provide easy dual drive-thru access. Construction is expected to commence in June 2018.\n\nEach warehouse include the following:\n- Office with carpet tiles, air-conditioning, power points, lighting and load-bearing ceiling\n- Kitchenette\n- Toilet amenities with shower\n- Container height roller shutter door\n- 3 Phase power",
     *				"status": 1,
     *				"propertyType": 4,
     *				"price": 100,
     *				"landArea": 456,
     *				"floorArea": 742,
     *				"conditions": 0,
     *				"address": "4/18 Luisa Avenue, DANDENONG VIC, 3175 ",
     *				"latitude": "37.9810",
     *				"longitude": "145.2150",
     *				"unitNumber": 0,
     *				"streetNumber": 0,
     *				"streetName": null,
     *				"suburb": null,
     *				"city": "Mohali",
     *				"state": "1",
     *				"postcode": null,
     *				"country": "13",
     *				"parking": 1,
     *				"parkingList": null,
     *				"parkingOther": null,
     *				"floorplan": null,
     *				"media": null,
     *				"video": "[\"https://youtu.be/YXSXe0LVFV8\"]",
     *				"videoImage": "[\"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg\"]",
     *				"school": 1,
     *				"communityCentre": 1,
     *				"parkLand": 1,
     *				"publicTransport": 0,
     *				"shopping": 1,
     *				"bedrooms": 3,
     *				"bathrooms": 4,
     *				"features": null,
     *				"tags": null,
     *				"estate_agent_id": 502,
     *				"second_agent_id": null,
     *				"agency_id": 16,
     *				"created_by": null,
     *				"propertyStatus": 0,
     *				"updated_by": null,
     *				"created_at": "2018-04-18 14:02:04",
     *				"updated_at": "2018-04-18 14:02:04",
     *				"inspection1": null,
     *				"inspection2": null,
     *				"inspection3": null,
     *				"inspection4": null,
     *				"inspection5": null,
     *				"inspection6": null,
     *				"inspection7": null,
     *				"inspection8": null,
     *				"is_featured": 0,
     *				"distance": "",
     *				"propertyTypename": "Industrial/Warehouse",
     *				"auctionDetails": {
     *					"live": false,
     *					"past": false,
     *					"upcoming": true,
     *					"seconds": 0
     *				},
     *				"gallery": [
     *					{
     *						"id": 18,
     *						"tbl_pk": 1,
     *						"url": "http://cview.civitastech.com/frontend/web/images/property/Property_image903.jpg",
     *						"type": "image/jpg",
     *						"tbl_name": "tbl_properties"
     *					}
     *				],
     *				"auction": {
     *					"id": 1,
     *					"listing_id": 1,
     *					"auction_type": null,
     *					"date": "2018-04-14",
     *					"time": "13:00:00",
     *					"datetime_start": "2018-05-25 07:27:20",
     *					"datetime_expire": 1523769931,
     *					"starting_price": null,
     *					"home_phone_number": null,
     *					"estate_agent": null,
     *					"picture_of_listing": null,
     *					"duration": null,
     *					"real_estate_active_himself": null,
     *					"real_estate_active_bidders": null,
     *					"reference_relevant_electronic": null,
     *					"reference_relevant_bidder": null,
     *					"created_at": 1523856263,
     *					"auction_status": 2,
     *					"streaming_url": null,
     *					"streaming_status": null,
     *					"templateId": "",
     *					"envelopeId": "",
     *					"documentId": "",
     *					"document": "",
     *					"recipientId": "",
     *					"bidding_status": 2,
     *					"updated_at": "2018-04-27",
     *					"created_by": 0,
     *					"updated_by": 0
     *				},
     *				"agent": {
     *					"status": 10,
     *					"user_type": 10,
     *					"app_id": "2",
     *					"id": 502,
     *					"username": "gurpreet.gct",
     *					"auth_key": "ELuV7Ve9BccyKlqHU9bmPYtv3W63o7ZB",
     *					"fullName": "gurpreet singh",
     *					"email": "gurpreet@graycelltech.com",
     *					"orgName": null,
     *					"firstName": "gurpreet",
     *					"lastName": "singh",
     *					"fbidentifier": null,
     *					"linkedin_identifier": null,
     *					"google_identifier": null,
     *					"role_id": null,
     *					"password_hash": "$2y$13$2otlmYGEqDlkC2AIO0u3M.Bt/Z3WNNN/pxq4IjGvFcTWIrIVAPNbS",
     *					"password_reset_token": "GpZ_v64WJKpO47GxYOH17l3AvzTsdROA_1526644554",
     *					"phone": null,
     *					"image": null,
     *					"sex": null,
     *					"dob": "1993-08-03",
     *					"ageGroup_id": 1,
     *					"bdayOffer": 0,
     *					"walletPinCode": null,
     *					"loyaltyPin": null,
     *					"advertister_id": null,
     *					"activationKey": null,
     *					"confirmationKey": null,
     *					"access_token": null,
     *					"hashKey": null,
     *					"device": "android",
     *					"device_token": "12113123113",
     *					"lastLogin": 1526890835,
     *					"latitude": null,
     *					"longitude": null,
     *					"timezone": null,
     *					"timezone_offset": null,
     *					"created_at": 1526624964,
     *					"updated_at": 1526890835,
     *					"storeid": null,
     *					"promise_uid": "3085abb78c79ebb4",
     *					"promise_acid": null,
     *					"user_code": "AW-gurpreet.gct",
     *					"referralCode": null,
     *					"referral_percentage": null,
     *					"agency_id": null,
     *					"logo": ""
     *				},
     *				"agency": {
     *					"status": 10,
     *					"id": 16,
     *					"name": "Biggin",
     *					"email": "biggin@gmail.cpm",
     *					"mobile": 919876543215,
     *					"logo": "http://cview.civitastech.com/frontend/web/agencylogo/aastha248.jpg",
     *					"backgroungImage": "http://cview.civitastech.com/frontend/web/agencylogo/aastha248.jpg",
     *					"address": "sco 206 - 207, sector 34 a, chandigarh - 160022",
     *					"latitude": "30.72589",
     *					"longitude": "76.75787",
     *					"about_us": " <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
     *					"created_at": 1524059536,
     *					"updated_at": 1524059536,
     *					"is_favourite": "",
     *					"facebook": "https://www.facebook.com/",
     *					"twitter": "https://twitter.com/",
     *					"instagram": "https://www.instagram.com/"
     *				},
     *				"saveauction": null,
     *				"sold": "0",
     *				"calendar": 0
     *			},
     *    	],
     *    	"status": 200
     *    }
	 * 
	 *  @apiErrorExample Error-Response:
	 *     HTTP/1.1 422 Validation Error
	 *     {
	 *         "message": "",
	 *         "data": [
	 *             {
	 *                 "field": "some filedname",
	 *                 "message": "Some message"
	 *             }
	 *         ],
	 *         "status": 422
	 *     }
	 * Or
	 *     HTTP/1.1 422 Validation Error
	 *     {
	 *         "message": "",
	 * 		  "data": {
	 * 		      "title": "First property",
	 * 			  "description": "A lovely property at the country side!",
	 *             "id": 2
	 *         },
	 *         "status": 201
	 *     }
	 * 
	 *  @return Object(common\models\Property)
	 */
    public function actionCreate()
    {
        $model= new Property();
		$model->load(Yii::$app->request->post(), '');
		$model->video = (!empty($model->video))?json_encode($model->video):'';
		$model->videoImage = (!empty($model->videoImage))?json_encode($model->videoImage):'';
          $property_document = Yii::$app->request->post()['document'];
          $model->document = (!empty($property_document))?json_encode($property_document):'';
		  if($model->save(false)) {
			$auctionmodel = new Auction();
		  	$auctionmodel->date=Yii::$app->request->post()['date'];
		  	$auctionmodel->time=Yii::$app->request->post()['time'];
		  	if (!empty(stristr($auctionmodel->date,"-"))) {
				$auctionmodel->datetime_start = strtotime($auctionmodel->date.' '.$auctionmodel->time);	
				$auctionmodel->date = strtotime($auctionmodel->date);
			}
		 	/*auction_status -> 1:auction,2:streaming,3:offline*/
		  	$auctionmodel->listing_id=$model->id;
			$auctionmodel->auction_status=!empty(Yii::$app->request->post()['auction_status'])?Yii::$app->request->post()['auction_status']:0;
			if(!empty(Yii::$app->request->post()['auction_document'])){
				$auction_document = Yii::$app->request->post()['auction_document'];
				//~ foreach($documents as $document){
					//~ $dataDoc = base64_decode($document);
					//~ $DocName = urlencode($model->title).rand(10,1000). ".pdf";
					//~ $DocPath =str_replace("/",DIRECTORY_SEPARATOR,Yii::getAlias('@frontend')."/" .'web/images/property/doc'."/".$DocName);
					//~ //print_r($DocName);die;
					//~ file_put_contents($DocPath,$dataDoc);
					//~ $documentname[] = $DocName;
				//~ }
				$auctionmodel->document = json_encode($auction_document);
			}
			$auctionmodel->save(false);
     		if(!empty(Yii::$app->request->post()['floorplan'])) {
				$images = Yii::$app->request->post()['floorplan'];
			  	foreach ($images as $propertyimage) {
					$gallery = new Gallery();
					$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $propertyimage));
					$imageName = urlencode($model->title).rand(10,1000). ".jpg";
					$imagePath = str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['PropertyImagePath'].$imageName);
					file_put_contents($imagePath, $data);
					$gallery->url = $imageName;
					$gallery->type = "image/jpg";
					$gallery->tbl_name = "tbl_properties";
					$gallery->tbl_pk = $model->id;
					$gallery->save(false);
			  	}
		  	}
	  	}
	  	$model = Property::find()->joinWith('gallery')->joinWith('auction')->where([Property::tableName().'.id'=>$model->id])->one();
		return [$model];
	}
    
    /** 
     * To get all passes api
     */
	public function actionIndex() {	
		return ["Cool"];
	}

	/** 
	 * 
	 * Save new property information
	 * - URI: *api/web/v1/properties/all
	 * - Data: required|x-www-form-urlencoded
	 *
	 * @ api {get} api/web/v1/property/all View All
	 * @ apiName ViewAllProperties
	 * @ apiVersion 0.1.1
	 * @ apiGroup Property
	 * @ apiDescription To view properties, Form-Data must be x-www-form-urlencoded
	 *
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "id": 21,
	 *                "title": "PRIME DEVELOPMENT OPPORTUNITY (STCA)",
	 *                "description": "Rare opportunity to acquire this substantial development site, situated in the heart of one of Melbourne's most sought after inner city locations.",
	 *   			  "status": 1,
	 *                "propertyType": 2,
	 *   			  "price": 0,
	 *   			  "landArea": null,
	 *   			  "floorArea": null,
	 *   			  "conditions": 0,
	 *   			  "address": "516 Church Street, Richmond, VIC 3121",
	 *   			  "latitude": "-37.828015",
	 *   			  "longitude": "144.997316",
	 *   			  "unitNumber": 0,
	 *   			  "streetNumber": 0,
	 *   			  "streetName": null,
	 *   			  "suburb": null,
	 *   			  "city": null,
	 *   			  "state": null,
	 *   			  "postcode": null,
	 *   			  "country": null,
	 *   			  "parking": 0,
	 *   			  "parkingList": null,
	 *   			  "parkingOther": null,
	 *   			  "floorplan": null,
	 *   			  "media": null,
	 *   			  "video": null,
	 *   			  "school": 0,
	 *   			  "communityCentre": 0,
	 *   			  "parkLand": 0,
	 *   			  "publicTransport": 0,
	 *   			  "shopping": 0,
	 *   			  "bedrooms": 0,
	 *   			  "bathrooms": 0,
	 *   			  "features": null,
	 *   			  "tags": null,
	 *   			  "estate_agent_id": 344,
	 *   			  "second_agent_id": null,
	 *   			  "agency_id": 10,
	 *   			  "created_by": null,
	 *   			  "propertyStatus": 0,
	 *   			  "updated_by": null,
	 *   			  "created_at": "2018-04-19 08:39:00",
	 *   			  "updated_at": "2018-04-19 08:39:00",
	 *   			  "inspection1": null,
	 *   			  "inspection2": null,
	 *   			  "inspection3": null,
	 *   			  "inspection4": null,
	 *   			  "inspection5": null,
	 *   			  "inspection6": null,
	 *   			  "inspection7": null,
	 *   			  "inspection8": null,
	 *   			  "is_featured": 0,
	 *   			  "distance": "",
	 *   			  "propertyTypename": "Land/Development",
	 *   			  "auctionDetails": {
	 *     			      "live": false,
	 *     				  "past": false,
	 *     				  "upcoming": true,
	 *     				  "seconds": 0
	 *                },
	 *   			  "gallery": [
	 *   	              {
	 *		                  "id": 20,
	 *		                  "tbl_pk": 29,
	 *		                  "url": "http://cview.civitastech.com/frontend/web/images/property/Property_image585.jpg",
	 *		                  "type": "images/jpg",
	 *		                  "tbl_name": "tbl_properties"
	 *	                  },
	 *	                  {
	 *		                  "id": 21,
	 *		                  "tbl_pk": 29,
	 *		                  "url": "http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
	 *		                  "type": "images/jpg",
	 *		                  "tbl_name": "tbl_properties"
	 *	   				  }
	 *   			  ],
	 *   			  "auction": {
	 *    	      	      "id": 41,
	 *     			      "listing_id": 21,
	 *     				  "auction_type": null,
	 *     				  "date": "2018-05-23",
	 *     				  "time": "13:00:00",
	 *     				  "datetime_start": "2018-05-23 13:00:00",
	 *     				  "datetime_expire": null,
	 *     				  "starting_price": null,
	 *     				  "home_phone_number": null,
	 *     				  "estate_agent": null,
	 *     				  "picture_of_listing": null,
	 *     				  "duration": null,
	 *     				  "real_estate_active_himself": null,
	 *     				  "real_estate_active_bidders": null,
	 *     				  "reference_relevant_electronic": null,
	 *     				  "reference_relevant_bidder": null,
	 *     				  "auction_status": 1,
	 *     				  "streaming_url": null,
	 *     				  "streaming_status": null,
	 *     				  "templateId": "",
	 *     				  "envelopeId": "",
	 *     				  "documentId": "",
	 *     				  "document": "",
	 *     				  "recipientId": "",
	 *     				  "bidding_status": 2
	 *                },
	 *   			  "agent": {
	 *                    "status": 10,
	 *                    "user_type": 10,
	 *                    "app_id": "2",
	 *                    "id": 344,
	 *                    "username": "aastha1",
	 *                    "email": "aastha1@graycelltech.com",
	 *                    "auth_key": "41oBK-OHkauNIq5b8jyQlF4jcZPTsXT_",
	 *                    "firstName": "aastha",
	 *                    "lastName": "chawla",
	 *                    "fullName": "aastha chawla",
	 *                    "orgName": null,
	 *                    "fbidentifier": null,
	 *                    "linkedin_identifier": null,
	 *                    "google_identifier": null,
	 *                    "role_id": null,
	 *                    "password_hash": "",
	 *                    "password_reset_token": null,
	 *                    "phone": null,
	 *                    "image": null,
	 *                    "sex": null,
	 *                    "dob": "1970-01-01",
	 *                    "ageGroup_id": 1,
	 *                    "bdayOffer": 0,
	 *                    "walletPinCode": null,
	 *                    "loyaltyPin": null,
	 *                    "advertister_id": null,
	 *                    "activationKey": null,
	 *                    "confirmationKey": null,
	 *                    "access_token": null,
	 *                    "hashKey": null,
	 *                    "device": "web",
	 *                    "device_token": "",
	 *                    "lastLogin": null,
	 *                    "latitude": null,
	 *                    "longitude": null,
	 *                    "timezone": null,
	 *                    "timezone_offset": null,
	 *                    "created_at": 1523881212,
	 *                    "updated_at": 1523881212,
	 *                    "storeid": null,
	 *                    "promise_uid": null,
	 *                    "promise_acid": null,
	 *                    "user_code": "AW-aastha1",
	 *                    "referralCode": null,
	 *                    "referral_percentage": null,
	 *                    "agency_id": 10,
	 *                    "agency": "http://localhost/AlphaWallet/html/frontend/web/agencylogo/Abercromby’s819.jpg"
	 *                },
	 *   			  "agency": {
	 *     		          "status": 10,
	 *     				  "id": 10,
	 *     				  "name": "Abercromby’s",
	 *     				  "email": "aastha@gmail.com",
	 *     				  "mobile": 1234569874,
	 *     				  "logo": "http://localhost/AlphaWallet/html/frontend/web/agencylogo/Abercromby’s819.jpg",
	 *     				  "address": "[\" sco 206 - 207, sector 34 a, chandigarh - 160022\",\"sco 72 a & 73 a sector 26 grain market chandigarh pin-160019\"]",
	 *     				  "latitude": "[\"30.72589\",\"30.72267\"]",
	 *     				  "longitude": "[\"76.75787\",\"76.79825\"]",
	 *     				  "created_at": 1524034419,
	 *     				  "updated_at": 1524034419
	 *   			  },
	 *   			  "saveauction": "",
	 * 				  "sold": "0"
	 * 				  or
	 * 				  "sold": "1"
	 *            },
	 *        ],
	 *        "status": 200,
	 *    }
	 *
	 *
	 * @return Object(common\models\Property)
	 */
    public function actionAll() {	

  		 $propertymodel=Property::find()->joinWith('agent')->joinWith('agency')->joinWith('auction')->joinWith('gallery')->joinWith('bidding')->orderBy('id desc');
  		 $result = new ActiveDataProvider([
					'query' => $propertymodel,
				]);
				return $result;
  	}

	/** 
	 * Search and Sort property information
	 * - URI: *api/web/v1/property/search
	 *
	 * @api {get} api/web/v1/property/search Property Searching & Sorting
	 * @apiName SearchProperty
	 * @apiVersion 0.1.1
	 * @apiGroup Property
	 * @apiDescription To search or Sort Property. 
	 *
	 * ### Supported keys for sorting:
	 * - *price* | ascending=>'price', 'descending' => '-price' 
	 * - *date* | ascending=>'date', 'descending' => '-date' 
	 * - *distance* | ascending=>'distance', 'descending' => '-distance' 
	 * ### `Note: Append (-) for sorting in descending order` 
	 *
	 *   ### Examples for reference:
	 *   #### `/api/web/v1/property/search?sort=price,-date`
	 *   #### `/api/web/v1/property/search?property_type=Commercial Farming, Land/Development` | `property_type=Commercial Farming`
	 *   #### `/api/web/v1/property/search?from_date=2018-04-01` | `/api/web/v1/property/search?to_date=2018-04-20` | `/api/web/v1/property/search?from_date=2018-04-01&to_date=2018-04-20`
	 *   #### `/api/web/v1/property/search?sale_from=2000` | `/api/web/v1/property/search?sale_to=5000` | `/api/web/v1/property/search?sale_from=2000&sale_to=5000`
	 *   #### `/api/web/v1/property/search?min_land=244` | `/api/web/v1/property/search?max_land=1200` | `/api/web/v1/property/search?min_land=230&max_land=2200`
	 *   #### `/api/web/v1/property/search?min_floor=244` | `/api/web/v1/property/search?max_floor=1200` | `/api/web/v1/property/search?min_floor=230&max_floor=2200`
	 *   #### `/api/web/v1/property/search?auction_only=1` To display only auctions
	 *   #### `/api/web/v1/property/search?auction_all=1` (Optional) To display all auctions
	 *
	 *   #### `http://cview.civitastech.com/api/web/v1/property/search?suburb=&property_type=Commercial Farming, Land/Development, Hotel/Leisure, Offices, Showrooms/Bulky Good&from_date=2018-04-01&to_date=2018-04-20&sale_from=2000&sale_to=5000&min_land=500&max_land=1000&min_floor=200&max_floor=500&auction_only=1&auction_all=0`
	 *  #### For location based searching: `http://cview.civitastech.com/api/web/v1/property/search?lat=30.741482&long=76.768066&sort=distance`, and for descending `http://cview.civitastech.com/api/web/v1/property/search?lat=30.741482&long=76.768066&sort=distance` 
	 *
	 *  ## Response 
	 *  - Key => Value
	 *  - *data.items.propertyType* => PropertyType Id (Represents types like Commercial Farming, Land/Development, Hotel/Leisure, Industrial/Warehouse, etc.)
	 *  - *data.items.state* => The corresponding state id from the saved states in DB. Refer to GeoState for getting the state id and names
	 *  - *data.items.country* => The corresponding country id from the saved countries in DB. Refer to GeoCountry for getting the state id and names
	 *  - *data.items.auctionDetails.live* => True if the auction is currently live
	 *  - *data.items.auctionDetails.past* => True if the auction has taken place already.
	 *  - *data.items.auctionDetails.upcoming* => True is the auction will take place in the future.
	 *  - *data.items.auctionDetails.seconds* => Integer representing number of seconds left for the auction to start, 0 incase of the auction has already taken place or is live right now.
	 *  - *data.items.auction.datetime_start* => The auction start date
	 *  - *data.items.auction.auction_status* => Integer representing the type of auction. i.e. 1 => auction, 2 => Streaming, 3 => Offline
	 *  - *data.items.saveauction* => Will consist a list of users that have saved that auction.
	 *
	 * @apiParam {String} suburb To Filter by city/state/suburb/postcode
	 * @apiParam {String} property_type Comma separated set of property types
	 * @apiParam {String} sale_from Min Cost of property
	 * @apiParam {String} sale_to Max Cost of property
	 * @apiParam {String} from_date Auction Start date, format YYYY-M-D eg. 2018-01-23
	 * @apiParam {String} to_date Auction End date, format YYYY-M-D eg. 2018-01-23
	 * @apiParam {String} min_land eg. 2032
	 * @apiParam {String} max_land eg. 23321
	 * @apiParam {String} min_floor eg. 323
	 * @apiParam {String} max_floor eg. 2938 
	 * @apiParam {String} auction_only 1=>Display only auction types
	 * @apiParam {String} auction_all 1=> Display all auction types i.e. auction|streaming|offline
	 * @apiParam {String} list 1=>Past Property, 2=>Upcoming Property
	 * @apiParam {String} status 1- For Sale, 2- For Rent. 3 - For Lease
	 * @apiParam {String} sort (Optional) Comma separated list of sort keys, append (-) for descending order sort.
	 * @apiParam {String} lat User's lat, or lat to be used for location filtering
	 * @apiParam {String} long User's long, or long to be used for location filtering 
	 * @apiParam {String} state state id(from the state api)
	 *
	 * 
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 
     *    {  
     *      "message":"",
     *      "data":{  
     *        "items":[  
     *          {  
     *             "id":39,
     *             "title":"MULITPLE-TENANCY INVESTMENT WITH UPSIDE DOWN",
     *             "description":"For Sale by Expressions of Interest closing Wednesday 23 May 2018 at 3pm.\n\nSubstantial retail/office opportunity with upside.\n\n- Land Area: 532m2 approx.\n- Build Area: 1,190m2 approx.\n- Combined Income: $351,390 pa plus GST\n- 4 tenancies\n- Significant development potential (14 levels - STCA)\n- Rear access",
     *             "status":1,
     *             "propertyType":4,
     *             "price":200,
     *             "landArea":532,
     *             "land_area_unit":null,
     *             "floorArea":1190,
     *             "floor_area_unit":null,
     *             "conditions":0,
     *             "address":"37-41 Hall Street, MOONEE PONDS VIC, 3039 ",
     *             "latitude":"-37.6893144",
     *             "longitude":"145.0417777",
     *             "unitNumber":0,
     *             "streetNumber":0,
     *             "streetName":null,
     *             "suburb":null,
     *             "city":"Richmond",
     *             "state":"Victoria",
     *             "postcode":null,
     *             "country":"13",
     *             "parking":1,
     *             "parkingList":null,
     *             "parkingOther":null,
     *             "floorplan":null,
     *             "media":null,
     *             "video":"https://youtu.be/YXSXe0LVFV8",
     *             "videoImage":"http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
     *             "document":"[{\"title\":\"Commercial view property for sale details\",\"url\":\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\"},{\"title\":\"Commercial view property for sale details #2\",\"url\":\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\"}]",
     *             "school":1,
     *             "communityCentre":0,
     *             "parkLand":0,
     *             "publicTransport":0,
     *             "shopping":0,
     *             "bedrooms":3,
     *             "bathrooms":4,
     *             "features":null,
     *             "tags":null,
     *             "created_by":null,
     *             "propertyStatus":0,
     *             "updated_by":null,
     *             "created_at":"2018-05-09 07:43:50",
     *             "updated_at":"2018-05-09 07:43:50",
     *             "is_featured":0,
     *             "cview_listing_id":"",
     *             "is_deleted":0,
     *             "business_email":null,
     *             "website":null,
     *             "valuation":null,
     *             "exclusivity":null,
     *             "tenancy":null,
     *             "zoning":null,
     *             "authority":null,
     *             "sale_tax":null,
     *             "tender_closing_date":null,
     *             "current_lease_expiry":null,
     *             "outgoings":null,
     *             "lease_price":null,
     *             "lease_price_show":0,
     *             "lease_period":null,
     *             "lease_tax":null,
     *             "external_id":null,
     *             "video_url":null,
     *             "slug":null,
     *             "distance":"",
     *             "propertyTypename":"Industrial/Warehouse",
     *             "auctionDetails":{  
     *               "live":false,
     *               "past":false,
     *               "upcoming":true,
     *               "seconds":0
     *             },
     *             "gallery":[  
     *                {  
     *                   "id":58,
     *                   "tbl_pk":39,
     *                   "url":"https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727732/d5q2gsgglkg5hsut8qpr.jpg",
     *                   "thumbnail":null,
     *                   "type":"image",
     *                   "file_name":null,
     *                   "tbl_name":"tbl_properties"
     *                }
     *             ],
     *             "auction":  
     *                {  
     *                   "id":58,
     *                   "listing_id":39,
     *                   "auction_type":"auction",
     *                   "estate_agent_id":546,
     *                   "agency_id":30,
     *                   "date":"2018-05-31",
     *                   "time":"14:00:00",
     *                   "datetime_start":"2018-05-31 14:00:00",
     *                   "datetime_expire":null,
     *                   "starting_price":null,
     *                   "home_phone_number":null,
     *                   "estate_agent":null,
     *                   "picture_of_listing":null,
     *                   "duration":null,
     *                   "real_estate_active_himself":null,
     *                   "real_estate_active_bidders":null,
     *                   "reference_relevant_electronic":null,
     *                   "reference_relevant_bidder":null,
     *                   "created_at":1525852138,
     *                   "auction_status":1,
     *                   "streaming_url":null,
     *                   "completed_streaming_url":"",
     *                   "streaming_status":null,
     *                   "templateId":"",
     *                   "envelopeId":"",
     *                   "documentId":"",
     *                   "document":"[{\"title\":\"Commercial view property for sale details\",\"url\":\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\"},{\"title\":\"Commercial view property for sale details #2\",\"url\":\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\"}]",
     *                   "recipientId":"",
     *                   "bidding_status":2,
     *                   "updated_at":"2018-05-09",
     *                   "created_by":0,
     *                   "updated_by":0,
     *                   "agent":{  
     *                      "status":10,
     *                      "user_type":10,
     *                      "app_id":"2",
     *                      "id":546,
     *                      "username":"paul_968",
     *                      "auth_key":"EAMP8qC_sdB3lZZcOAvQBLcMqRjgEwKo",
     *                      "fullName":"Paul Bell",
     *                      "email":"eb160e46@commercialview.com.au",
     *                      "orgName":null,
     *                      "firstName":"Paul",
     *                      "lastName":"Bell",
     *                      "fbidentifier":null,
     *                      "linkedin_identifier":null,
     *                      "google_identifier":null,
     *                      "role_id":null,
     *                      "password_hash":"",
     *                      "password_reset_token":null,
     *                      "phone":448772355,
     *                      "image":null,
     *                      "sex":null,
     *                      "dob":"1970-01-01",
     *                      "ageGroup_id":1,
     *                      "bdayOffer":0,
     *                      "walletPinCode":null,
     *                      "loyaltyPin":null,
     *                      "advertister_id":null,
     *                      "activationKey":null,
     *                      "confirmationKey":null,
     *                      "access_token":null,
     *                      "hashKey":null,
     *                      "device":"web",
     *                      "device_token":"",
     *                      "lastLogin":null,
     *                      "latitude":null,
     *                      "longitude":null,
     *                      "timezone":null,
     *                      "timezone_offset":null,
     *                      "created_at":1527656528,
     *                      "updated_at":1527656528,
     *                      "storeid":null,
     *                      "promise_uid":null,
     *                      "promise_acid":null,
     *                      "user_code":"AW-paul_968",
     *                      "referralCode":null,
     *                      "referral_percentage":null,
     *                      "agency_id":30,
     *                      "logo":"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png"
     *                   },
     *                   "agency":{  
     *                      "status":10,
     *                      "id":30,
     *                      "cv_agency_id":"CV-B4CB59",
     *                      "name":"Paul Bell Real Estate ",
     *                      "email":"paul@paulbellrealestate.com.au",
     *                      "mobile":448772355,
     *                      "fax":"",
     *                      "logo":"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png",
     *                      "backgroungImage":null,
     *                      "address":null,
     *                      "street_address":null,
     *                      "full_address":null,
     *                      "suburb_id":null,
     *                      "latitude":null,
     *                      "longitude":null,
     *                      "about_us":null,
     *                      "slug":"paul-bell-real-estate-highton",
     *                      "abn":"",
     *                      "created_at":1527656528,
     *                      "updated_at":1527656528,
     *                      "web":null,
     *                      "twitter":"https://twitter.com/",
     *                      "facebook":"https://www.facebook.com/",
     *                      "is_favourite":"",
     *                      "instagram":"https://www.instagram.com/",
     *                      "agency_agents":[  
     *                         {  
     *                            "status":10,
     *                            "user_type":10,
     *                            "app_id":"2",
     *                            "id":546,
     *                            "username":"paul_968",
     *                            "auth_key":"EAMP8qC_sdB3lZZcOAvQBLcMqRjgEwKo",
     *                            "fullName":"Paul Bell",
     *                            "email":"eb160e46@commercialview.com.au",
     *                            "orgName":null,
     *                            "firstName":"Paul",
     *                            "lastName":"Bell",
     *                            "fbidentifier":null,
     *                            "linkedin_identifier":null,
     *                            "google_identifier":null,
     *                            "role_id":null,
     *                            "password_hash":"",
     *                            "password_reset_token":null,
     *                            "phone":448772355,
     *                            "image":null,
     *                            "sex":null,
     *                            "dob":"1970-01-01",
     *                            "ageGroup_id":1,
     *                            "bdayOffer":0,
     *                            "walletPinCode":null,
     *                            "loyaltyPin":null,
     *                            "advertister_id":null,
     *                            "activationKey":null,
     *                            "confirmationKey":null,
     *                            "access_token":null,
     *                            "hashKey":null,
     *                            "device":"web",
     *                            "device_token":"",
     *                            "lastLogin":null,
     *                            "latitude":null,
     *                            "longitude":null,
     *                            "timezone":null,
     *                            "timezone_offset":null,
     *                            "created_at":1527656528,
     *                            "updated_at":1527656528,
     *                            "storeid":null,
     *                            "promise_uid":null,
     *                            "promise_acid":null,
     *                            "user_code":"AW-paul_968",
     *                            "referralCode":null,
     *                            "referral_percentage":null,
     *                            "agency_id":30,
     *                            "logo":"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png"
     *                         }
     *                      ]
     *                   }
     *                }
     *             ],
     *             "saveauction":null,
     *             "sold":"0",
     *             "calendar":0
     *           },
     *           {  
     *             "id":50,
     *             "title":"Looking for a \"Juicy\" investment?",
     *             "description":"The building is nearing completion and what’s better than already having an excellent tenant from day one! With a 6.5% return locked in for the first three years, plus GST, plus outgoings. (3 x3 lease) $162,500 per annum plus GST/outgoings. I know where I would rather put my money as the financial institutions are paying around 2.5-3% on term deposits, here you will earn double that! Plus!\r\n•\t2842m2 titled land\r\n•\tBrand new purpose-built warehouse with initial 3 x 3 year terms\r\n•\tBrand new building with great depreciation potential. Quantity Survey Available from Agent.\r\n•\tFirst 3 years locked in at 6.5% return on $2.5million = $162,500 plus GST & outgoings paid by tenant\r\n•\tLease will be in place so GST on sale is not applicable\r\n•\tWould suit family super fund, financial investors, financial institutions, etc….\r\n•\tBeing only a segment of an existing business there may be other opportunities for the purchaser to pursue – open to discussion\r\n•\tInspection by appointment only.\r\n",
     *             "status":0,
     *             "propertyType":1,
     *             "price":0,
     *             "landArea":null,
     *             "land_area_unit":"squareMeter",
     *             "floorArea":null,
     *             "floor_area_unit":"squareMeter",
     *             "conditions":0,
     *             "address":null,
     *             "latitude":"-38.182",
     *             "longitude":"144.375",
     *             "unitNumber":0,
     *             "streetNumber":0,
     *             "streetName":null,
     *             "suburb":null,
     *             "city":null,
     *             "state":null,
     *             "postcode":null,
     *             "country":null,
     *             "parking":0,
     *             "parkingList":null,
     *             "parkingOther":null,
     *             "floorplan":null,
     *             "media":null,
     *             "video":null,
     *             "videoImage":null,
     *             "document":null,
     *             "school":0,
     *             "communityCentre":0,
     *             "parkLand":0,
     *             "publicTransport":0,
     *             "shopping":0,
     *             "bedrooms":0,
     *             "bathrooms":0,
     *             "features":null,
     *             "tags":null,
     *             "created_by":538,
     *             "propertyStatus":0,
     *             "updated_by":538,
     *             "created_at":"2018-05-30 05:02:08",
     *             "updated_at":"2018-05-30 05:02:08",
     *             "is_featured":0,
     *             "cview_listing_id":"HDS235845S",
     *             "is_deleted":0,
     *             "business_email":null,
     *             "website":null,
     *             "valuation":null,
     *             "exclusivity":"exclusive",
     *             "tenancy":"tenanted",
     *             "zoning":"Industrial Zone 1",
     *             "authority":null,
     *             "sale_tax":"unknown",
     *             "tender_closing_date":null,
     *             "current_lease_expiry":null,
     *             "outgoings":null,
     *             "lease_price":null,
     *             "lease_price_show":0,
     *             "lease_period":"annual",
     *             "lease_tax":"",
     *             "external_id":"b18c8652-72bf-47cb-ac06-d271ca0b0d8d",
     *             "video_url":"https://youtu.be/YXSXe0LVFV8",
     *             "slug":"breakwater-3219-vic-industrial-warehouse-showrooms-bulky-goods-offices-11294048",
     *             "distance":"",
     *             "propertyTypename":"Commercial Farming",
     *             "auctionDetails":null,
     *             "gallery":[  
     *               {  
     *                 "id":71,
     *                 "tbl_pk":50,
     *                 "url":"https://cview-upload.s3.amazonaws.com/development/uploads/294048/FrontElevation.pdf",
     *                 "thumbnail":null,
     *                 "type":"document",
     *                 "file_name":"FrontElevation.pdf",
     *                 "tbl_name":"tbl_properties"
     *               },
     *               {  
     *                 "id":72,
     *                 "tbl_pk":50,
     *                 "url":"https://youtu.be/YXSXe0LVFV8",
     *                 "thumbnail":null,
     *                 "type":"video",
     *                 "file_name":null,
     *                 "tbl_name":"tbl_properties"
     *               },
     *               {  
     *                 "id":58,
     *                 "tbl_pk":39,
     *                 "url":"https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727732/d5q2gsgglkg5hsut8qpr.jpg",
     *                 "thumbnail":null,
     *                 "type":"image",
     *                 "file_name":null,
     *                 "tbl_name":"tbl_properties"
     *               }
     *             ],
     *             "auction":{  
     *               "id":58,
     *               "listing_id":39,
     *               "auction_type":"auction",
     *               "estate_agent_id":546,
     *               "agency_id":30,
     *               "date":"2018-05-31",
     *               "time":"14:00:00",
     *               "datetime_start":"2018-05-31 14:00:00",
     *               "datetime_expire":null,
     *               "starting_price":null,
     *               "home_phone_number":null,
     *               "estate_agent":null,
     *               "picture_of_listing":null,
     *               "duration":null,
     *               "real_estate_active_himself":null,
     *               "real_estate_active_bidders":null,
     *               "reference_relevant_electronic":null,
     *               "reference_relevant_bidder":null,
     *               "created_at":1525852138,
     *               "auction_status":1,
     *               "streaming_url":null,
     *               "completed_streaming_url":"",
     *               "streaming_status":null,
     *               "templateId":"",
     *               "envelopeId":"",
     *               "documentId":"",
     *               "document":"[{\"title\":\"Commercial view property for sale details\",\"url\":\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\"},{\"title\":\"Commercial view property for sale details #2\",\"url\":\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\"}]",
     *               "recipientId":"",
     *               "bidding_status":2,
     *               "updated_at":"2018-05-09",
     *               "created_by":0,
     *               "updated_by":0,
     *               "agent":{  
     *                  "status":10,
     *                  "user_type":10,
     *                  "app_id":"2",
     *                  "id":546,
     *                  "username":"paul_968",
     *                  "auth_key":"EAMP8qC_sdB3lZZcOAvQBLcMqRjgEwKo",
     *                  "fullName":"Paul Bell",
     *                  "email":"eb160e46@commercialview.com.au",
     *                  "orgName":null,
     *                  "firstName":"Paul",
     *                  "lastName":"Bell",
     *                  "fbidentifier":null,
     *                  "linkedin_identifier":null,
     *                  "google_identifier":null,
     *                  "role_id":null,
     *                  "password_hash":"",
     *                  "password_reset_token":null,
     *                  "phone":448772355,
     *                  "image":null,
     *                  "sex":null,
     *                  "dob":"1970-01-01",
     *                  "ageGroup_id":1,
     *                  "bdayOffer":0,
     *                  "walletPinCode":null,
     *                  "loyaltyPin":null,
     *                  "advertister_id":null,
     *                  "activationKey":null,
     *                  "confirmationKey":null,
     *                  "access_token":null,
     *                  "hashKey":null,
     *                  "device":"web",
     *                  "device_token":"",
     *                  "lastLogin":null,
     *                  "latitude":null,
     *                  "longitude":null,
     *                  "timezone":null,
     *                  "timezone_offset":null,
     *                  "created_at":1527656528,
     *                  "updated_at":1527656528,
     *                  "storeid":null,
     *                  "promise_uid":null,
     *                  "promise_acid":null,
     *                  "user_code":"AW-paul_968",
     *                  "referralCode":null,
     *                  "referral_percentage":null,
     *                  "agency_id":30,
     *                  "logo":"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png"
     *               },
     *               "agency":{  
     *                 "status":10,
     *                 "id":30,
     *                 "cv_agency_id":"CV-B4CB59",
     *                 "name":"Paul Bell Real Estate ",
     *                 "email":"paul@paulbellrealestate.com.au",
     *                 "mobile":448772355,
     *                 "fax":"",
     *                 "logo":"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png",
     *                 "backgroungImage":null,
     *                 "address":null,
     *                 "street_address":null,
     *                 "full_address":null,
     *                 "suburb_id":null,
     *                 "latitude":null,
     *                 "longitude":null,
     *                 "about_us":null,
     *                 "slug":"paul-bell-real-estate-highton",
     *                 "abn":"",
     *                 "created_at":1527656528,
     *                 "updated_at":1527656528,
     *                 "web":null,
     *                 "twitter":"https://twitter.com/",
     *                 "facebook":"https://www.facebook.com/",
     *                 "is_favourite":"",
     *                 "instagram":"https://www.instagram.com/",
     *                 "agency_agents":[  
     *                   {  
     *                     "status":10,
     *                     "user_type":10,
     *                     "app_id":"2",
     *                     "id":546,
     *                     "username":"paul_968",
     *                     "auth_key":"EAMP8qC_sdB3lZZcOAvQBLcMqRjgEwKo",
     *                     "fullName":"Paul Bell",
     *                     "email":"eb160e46@commercialview.com.au",
     *                     "orgName":null,
     *                     "firstName":"Paul",
     *                     "lastName":"Bell",
     *                     "fbidentifier":null,
     *                     "linkedin_identifier":null,
     *                     "google_identifier":null,
     *                     "role_id":null,
     *                     "password_hash":"",
     *                     "password_reset_token":null,
     *                     "phone":448772355,
     *                     "image":null,
     *                     "sex":null,
     *                     "dob":"1970-01-01",
     *                     "ageGroup_id":1,
     *                     "bdayOffer":0,
     *                     "walletPinCode":null,
     *                     "loyaltyPin":null,
     *                     "advertister_id":null,
     *                     "activationKey":null,
     *                     "confirmationKey":null,
     *                     "access_token":null,
     *                     "hashKey":null,
     *                     "device":"web",
     *                     "device_token":"",
     *                     "lastLogin":null,
     *                     "latitude":null,
     *                     "longitude":null,
     *                     "timezone":null,
     *                     "timezone_offset":null,
     *                     "created_at":1527656528,
     *                     "updated_at":1527656528,
     *                     "storeid":null,
     *                     "promise_uid":null,
     *                     "promise_acid":null,
     *                     "user_code":"AW-paul_968",
     *                     "referralCode":null,
     *                     "referral_percentage":null,
     *                     "agency_id":30,
     *                     "logo":"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1496367275/zz38juhhm9ttw4jv9tsl.png"
     *                   }
     *                 ]
     *               }
     *             },
     *             "saveauction":null,
     *             "sold":"0",
     *             "calendar":0
     *          },
     *          {  
     *            "id":50,
     *            "title":"Looking for a \"Juicy\" investment?",
     *            "description":"The building is nearing completion and what’s better than already having an excellent tenant from day one! With a 6.5% return locked in for the first three years, plus GST, plus outgoings. (3 x3 lease) $162,500 per annum plus GST/outgoings. I know where I would rather put my money as the financial institutions are paying around 2.5-3% on term deposits, here you will earn double that! Plus!\r\n•\t2842m2 titled land\r\n•\tBrand new purpose-built warehouse with initial 3 x 3 year terms\r\n•\tBrand new building with great depreciation potential. Quantity Survey Available from Agent.\r\n•\tFirst 3 years locked in at 6.5% return on $2.5million = $162,500 plus GST & outgoings paid by tenant\r\n•\tLease will be in place so GST on sale is not applicable\r\n•\tWould suit family super fund, financial investors, financial institutions, etc….\r\n•\tBeing only a segment of an existing business there may be other opportunities for the purchaser to pursue – open to discussion\r\n•\tInspection by appointment only.\r\n",
     *            "status":0,
     *            "propertyType":1,
     *            "price":0,
     *            "landArea":null,
     *            "land_area_unit":"squareMeter",
     *            "floorArea":null,
     *            "floor_area_unit":"squareMeter",
     *            "conditions":0,
     *            "address":null,
     *            "latitude":"-38.182",
     *            "longitude":"144.375",
     *            "unitNumber":0,
     *            "streetNumber":0,
     *            "streetName":null,
     *            "suburb":null,
     *            "city":null,
     *            "state":null,
     *            "postcode":null,
     *            "country":null,
     *            "parking":0,
     *            "parkingList":null,
     *            "parkingOther":null,
     *            "floorplan":null,
     *            "media":null,
     *            "video":null,
     *            "videoImage":null,
     *            "document":null,
     *            "school":0,
     *            "communityCentre":0,
     *            "parkLand":0,
     *            "publicTransport":0,
     *            "shopping":0,
     *            "bedrooms":0,
     *            "bathrooms":0,
     *            "features":null,
     *            "tags":null,
     *            "created_by":538,
     *            "propertyStatus":0,
     *            "updated_by":538,
     *            "created_at":"2018-05-30 05:02:08",
     *            "updated_at":"2018-05-30 05:02:08",
     *            "is_featured":0,
     *            "cview_listing_id":"HDS235845S",
     *            "is_deleted":0,
     *            "business_email":null,
     *            "website":null,
     *            "valuation":null,
     *            "exclusivity":"exclusive",
     *            "tenancy":"tenanted",
     *            "zoning":"Industrial Zone 1",
     *            "authority":null,
     *            "sale_tax":"unknown",
     *            "tender_closing_date":null,
     *            "current_lease_expiry":null,
     *            "outgoings":null,
     *            "lease_price":null,
     *            "lease_price_show":0,
     *            "lease_period":"annual",
     *            "lease_tax":"",
     *            "external_id":"b18c8652-72bf-47cb-ac06-d271ca0b0d8d",
     *            "video_url":"https://youtu.be/YXSXe0LVFV8",
     *            "slug":"breakwater-3219-vic-industrial-warehouse-showrooms-bulky-goods-offices-11294048",
     *            "distance":"",
     *            "propertyTypename":"Commercial Farming",
     *            "auctionDetails":null,
     *            "gallery":[  
     *              {  
     *                "id":71,
     *                "tbl_pk":50,
     *                "url":"https://cview-upload.s3.amazonaws.com/development/uploads/294048/FrontElevation.pdf",
     *                "thumbnail":null,
     *                "type":"document",
     *                "file_name":"FrontElevation.pdf",
     *                "tbl_name":"tbl_properties"
     *              },
     *              {  
     *                "id":72,
     *                "tbl_pk":50,
     *                "url":"https://youtu.be/YXSXe0LVFV8",
     *                "thumbnail":null,
     *                "type":"video",
     *                "file_name":null,
     *                "tbl_name":"tbl_properties"
     *              },
     *              {  
     *                "id":73,
     *                "tbl_pk":50,
     *                "url":"https://dduaaywsz-res-5.cloudinary.com/image/upload/v1523514445/d4wlukvypczhex4jjm0w.jpg",
     *                "thumbnail":null,
     *                "type":"image",
     *                "file_name":null,
     *                "tbl_name":"tbl_properties"
     *              },
     *              {  
     *                "id":74,
     *                "tbl_pk":50,
     *                "url":"https://dduaaywsz-res-4.cloudinary.com/image/upload/v1523514435/v2kjd0f14jzwkej5lyax.jpg",
     *                "thumbnail":null,
     *                "type":"image",
     *                "file_name":null,
     *                "tbl_name":"tbl_properties"
     *              }
     *            ],
     *            "auction":null,
     *            "saveauction":null,
     *            "sold":"0",
     *            "calendar":0
     *          }
     *        ],
     *        "_links":{  
     *          "self":{  
     *            "href":"http://cview.civitastech.com/api/web/v1/properties/search?page=2"
     *          },
     *          "first":{  
     *            "href":"http://cview.civitastech.com/api/web/v1/properties/search?page=1"
     *          },
     *          "prev":{  
     *            "href":"http://cview.civitastech.com/api/web/v1/properties/search?page=1"
     *          }
     *        },
     *        "_meta":{  
     *          "totalCount":25,
     *          "pageCount":2,
     *          "currentPage":2,
     *          "perPage":20
     *        }
     *      },
     *      "status":200
     *    }
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *      "message": "",
	 *      "data": [
	 *        {
	 *          "field": "some filedname",
	 *          "message": "Some message"
	 *        }
	 *      ],
	 *      "status": 422
	 *    }
	 *
	 * @return Object(common\models\Property)
	 */
	public function actionSearch() {  
        if(!empty(Yii::$app->getRequest()->getHeaders()->get('Authorization'))){
            $token = Yii::$app->getRequest()->getHeaders()->get('Authorization');
            $user_id = Yii::$app->MyConstant->UserID($token);
            if(!empty($user_id)){
                $findUser = User::findOne(['id'=>$user_id]);
                //print_r(Yii::$app->getRequest()->getHeaders()->get('Authorization'));die;
                if($findUser->auth_key!=str_replace("Bearer ", "", $token)){
                    $response['name']="Unauthorized";
                    $response['message'] = 'You are requesting with an invalid credential';
                    $response['code']=0;
                    $response['statusCode']=401;
                    $response['type']="yii\\web\\UnauthorizedHttpException";
                    return $response;	
                }
            }else{
                $response['name']="Unauthorized";
                $response['message'] = 'You are requesting with an invalid credential';
                $response['code']=0;
                $response['statusCode']=401;
                $response['type']="yii\\web\\UnauthorizedHttpException";
                return $response;	
            }
        }
        $propertysearchModel = new PropertySearch();
        $searchByAttr['PropertySearch'] = Yii::$app->request->queryParams;
        // echo '<pre>'; print_r($searchByAttr); die('ghe');
        return $propertysearchModel->search($searchByAttr);
    }
	  
	/** 
	 * 
	 * View Property Information
	 * - URI: *api/web/v1/properties/view?id=39
	 * - Data: required|x-www-form-urlencoded
	 *
	 * @api {get} api/web/v1/property/view?id=39 View Property Detail
	 * @apiName View Property Details
	 * @apiVersion 0.1.1
	 * @apiGroup Property
	 * @apiDescription To view property detail, Form-Data must be x-www-form-urlencoded
	 *
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 
     *    {
     *       "message": "",
     *       "data": {
     *         "id": 39,
     *         "title": "MULITPLE-TENANCY INVESTMENT WITH UPSIDE DOWN",
     *         "description": "For Sale by Expressions of Interest closing Wednesday 23 May 2018 at 3pm.\n\nSubstantial retail/office opportunity with upside.\n\n- Land Area: 532m2 approx.\n- Build Area: 1,190m2 approx.\n- Combined Income: $351,390 pa plus GST\n- 4 tenancies\n- Significant development potential (14 levels - STCA)\n- Rear access",
     *         "status": 1,
     *         "propertyType": 4,
     *         "price": 200,
     *         "landArea": 532,
     *         "land_area_unit": null,
     *         "floorArea": 1190,
     *         "floor_area_unit": null,
     *         "conditions": 0,
     *         "address": "37-41 Hall Street, MOONEE PONDS VIC, 3039 ",
     *         "latitude": "-37.6893144",
     *         "longitude": "145.0417777",
     *         "unitNumber": 0,
     *         "streetNumber": 0,
     *         "streetName": null,
     *         "suburb": null,
     *         "city": "Richmond",
     *         "state": "Victoria",
     *         "postcode": null,
     *         "country": "13",
     *         "parking": 1,
     *         "parkingList": null,
     *         "parkingOther": null,
     *         "floorplan": null,
     *         "media": null,
     *         "video": "https://youtu.be/YXSXe0LVFV8",
     *         "videoImage": "http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
     *         "document": "[{\"title\":\"Commercial view property for sale details\",\"url\":\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\"},{\"title\":\"Commercial view property for sale details #2\",\"url\":\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\"}]",
     *         "school": 1,
     *         "communityCentre": 0,
     *         "parkLand": 0,
     *         "publicTransport": 0,
     *         "shopping": 0,
     *         "bedrooms": 3,
     *         "bathrooms": 4,
     *         "features": null,
     *         "tags": null,
     *         "created_by": null,
     *         "propertyStatus": 0,
     *         "updated_by": null,
     *         "created_at": "2018-05-09 07:43:50",
     *         "updated_at": "2018-05-09 07:43:50",
     *         "is_featured": 0,
     *         "cview_listing_id": "",
     *         "is_deleted": 0,
     *         "business_email": null,
     *         "website": null,
     *         "valuation": null,
     *         "exclusivity": null,
     *         "tenancy": null,
     *         "zoning": null,
     *         "authority": null,
     *         "sale_tax": null,
     *         "tender_closing_date": null,
     *         "current_lease_expiry": null,
     *         "outgoings": null,
     *         "lease_price": null,
     *         "lease_price_show": 0,
     *         "lease_period": null,
     *         "lease_tax": null,
     *         "external_id": null,
     *         "video_url": null,
     *         "slug": null,
     *         "distance": "",
     *         "propertyTypename": "Industrial/Warehouse",
     *         "auctionDetails": {
     *           "live": false,
     *           "past": false,
     *           "upcoming": false,
     *           "seconds": 0
     *         },
     *         "gallery": [
     *           {
     *             "id": 58,
     *             "tbl_pk": 39,
     *             "url": "https://dduaaywsz-res-4.cloudinary.com/image/upload/a_ignore,c_fill,h_420,w_640/v1510727732/d5q2gsgglkg5hsut8qpr.jpg",
     *             "thumbnail": null,
     *             "type": "image",
     *             "file_name": null,
     *             "tbl_name": "tbl_properties"
     *           }
     *         ],
     *         "auction": {
     *           "id": 58,
     *           "listing_id": 39,
     *           "auction_type": "auction",
     *           "estate_agent_id": 546,
     *           "agency_id": 30,
     *           "date": "2018-05-31",
     *           "time": "14:00:00",
     *           "datetime_start": "2018-05-31 14:00:00",
     *           "datetime_expire": null,
     *           "starting_price": null,
     *           "home_phone_number": null,
     *           "estate_agent": null,
     *           "picture_of_listing": null,
     *           "duration": null,
     *           "real_estate_active_himself": null,
     *           "real_estate_active_bidders": null,
     *           "reference_relevant_electronic": null,
     *           "reference_relevant_bidder": null,
     *           "created_at": 1525852138,
     *           "auction_status": 1,
     *           "streaming_url": null,
     *           "completed_streaming_url": "",
     *           "streaming_status": null,
     *           "templateId": "",
     *           "envelopeId": "",
     *           "documentId": "",
     *           "document": "[{\"title\":\"Commercial view property for sale details\",\"url\":\"http://www.commercialview.com.au/659-sherwood-road-sherwood-qld/commercial-for-sale-details-11299294?format=pdf\"},{\"title\":\"Commercial view property for sale details #2\",\"url\":\"http://www.commercialview.com.au/8-131-147-walker-street-dandenong-vic/commercial-for-sale-details-11297781?format=pdf\"}]",
     *           "recipientId": "",
     *           "bidding_status": 2,
     *           "updated_at": "2018-05-09",
     *           "created_by": 0,
     *           "updated_by": 0,
     *           "agent": null,
     *           "agency": null
     *         },
     *         "saveauction": null,
     *         "sold": "0",
     *         "calendar": 0
     *       },
     *       "status": 200
     *    }
     *     
	 * @apiErrorExample Error-Response:
     *    HTTP/1.1 422 Unprocessable entity
	 *    {
	 *      "message": "No Record Found",
	 *      "data":  {
	 *        "status": "error"
	 *      },
	 *      "status": 422
	 *    }
	 *
	 * @return Object(common\models\Property)
	 */
    public function actionView($id){
		//return Property::find()->joinWith('agent')->joinWith('agency')->joinWith('auction')->joinWith('gallery')->where([Property::tableName().'.id' => $id])->one();
		//return Property::find()->joinWith('auction')->joinWith('gallery')->where([Property::tableName().'.id' => $id])->one();
		$res = Property::find()->joinWith('auction')->joinWith('gallery')->where([Property::tableName().'.id' => $id])->one();
        if($res) return $res;
        $msg = CommonModel::textGlobalization('app', 'no_record');
        return ['status' => 'error', 'message' => $msg, 'statusCode' => 422];
	}
	
    public function actionTest() {
        return Property::find()->all();
        return Property::find()->joinWith('auction')->joinWith('gallery')->orderBy('id desc')->where([Property::tableName().'.id'=>24])->all();
     }

	/** 
	 * Get logged-in agent properties
	 * - URI: *api/web/v1/properties/get-agent-properties
	 *
	 * @api {get} api/web/v1/properties/get-agent-properties Get logged-in agent properties
	 * @apiName Get Agent Properties
	 * @apiVersion 0.1.1
	 * @apiGroup Property
	 * @apiDescription To lsit properties of loggedin agent.
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 
     *    {
     *    "message": "",
     *    "data": {
     *    	"items": [
     *    		{
     *    			"id": 5,
     *    			"title": "SUPERB FREEHOLD OPPORTUNITY",
     *    			"description": "Teska Carson is pleased to offer 922 Riversdale Road, Surrey Hills for sale by public auction on Wednesday 23rd May.",
     *    			"status": 1,
     *    			"propertyType": 7,
     *    			"price": 4531.89,
     *    			"landArea": null,
     *    			"floorArea": 180,
     *    			"conditions": 0,
     *    			"address": "Grayce Ct SE, Salem, OR, United States",
     *    			"latitude": "44.92396550",
     *    			"longitude": "-122.97581490",
     *    			"unitNumber": 0,
     *    			"streetNumber": 0,
     *    			"streetName": null,
     *    			"suburb": null,
     *    			"city": null,
     *    			"state": null,
     *    			"postcode": null,
     *    			"country": null,
     *    			"parking": 0,
     *    			"parkingList": null,
     *    			"parkingOther": null,
     *    			"floorplan": null,
     *    			"media": null,
     *    			"video": null,
     *    			"videoImage": null,
     *    			"school": 0,
     *    			"communityCentre": 0,
     *    			"parkLand": 0,
     *    			"publicTransport": 0,
     *    			"shopping": 0,
     *    			"bedrooms": 0,
     *    			"bathrooms": 0,
     *    			"features": null,
     *    			"tags": null,
     *    			"estate_agent_id": 351,
     *    			"second_agent_id": null,
     *    			"agency_id": 15,
     *    			"created_by": null,
     *    			"propertyStatus": 0,
     *    			"updated_by": null,
     *    			"created_at": "2018-04-19 06:10:59",
     *    			"updated_at": "2018-04-19 06:10:59",
     *    			"inspection1": null,
     *    			"inspection2": null,
     *    			"inspection3": null,
     *    			"inspection4": null,
     *    			"inspection5": null,
     *    			"inspection6": null,
     *    			"inspection7": null,
     *    			"inspection8": null,
     *    			"is_featured": 0,
     *    			"distance": "",
     *    			"propertyTypename": "Retail",
     *    			"auctionDetails": {
     *    				"live": false,
     *    				"past": false,
     *    				"upcoming": true,
     *    				"seconds": 0
     *    			},
     *    			"gallery": [],
     *    			"auction": {
     *    				"id": 23,
     *    				"listing_id": 5,
     *    				"auction_type": null,
     *    				"date": null,
     *    				"time": null,
     *    				"datetime_start": "2018-05-20 02:58:11",
     *    				"datetime_expire": 1526792091,
     *    				"starting_price": null,
     *    				"home_phone_number": null,
     *    				"estate_agent": null,
     *    				"picture_of_listing": null,
     *    				"duration": null,
     *    				"real_estate_active_himself": null,
     *    				"real_estate_active_bidders": null,
     *    				"reference_relevant_electronic": null,
     *    				"reference_relevant_bidder": null,
     *    				"created_at": 0,
     *    				"auction_status": 1,
     *    				"streaming_url": null,
     *    				"streaming_status": null,
     *    				"templateId": "",
     *    				"envelopeId": "",
     *    				"documentId": "",
     *    				"document": "",
     *    				"recipientId": "",
     *    				"bidding_status": 2,
     *    				"updated_at": "2018-04-27",
     *    				"created_by": 0,
     *    				"updated_by": 0
     *    			},
     *    			"agent": {
     *    				"status": 10,
     *    				"user_type": 10,
     *    				"app_id": "2",
     *    				"id": 351,
     *    				"username": "gurpreet.gct",
     *    				"email": "gurpreet@graycelltech.com",
     *    				"auth_key": "YR7IbtgZ0YNCMXW7LqLqCiI6ULIGW9dh",
     *    				"firstName": "GPreet",
     *    				"lastName": "Singh",
     *    				"fullName": "GPreet Singh",
     *    				"orgName": null,
     *    				"fbidentifier": null,
     *    				"linkedin_identifier": "123qweasdcxz",
     *    				"google_identifier": null,
     *    				"role_id": null,
     *    				"password_hash": "$2y$13$nHZiuXex00yoh9dFlJQg3.vfX7UpAP3.pU3UmN8dl0G0kkRt6oDKm",
     *    				"password_reset_token": null,
     *    				"phone": null,
     *    				"image": null,
     *    				"sex": null,
     *    				"dob": "1970-01-01",
     *    				"ageGroup_id": 1,
     *    				"bdayOffer": 0,
     *    				"walletPinCode": null,
     *    				"loyaltyPin": null,
     *    				"advertister_id": null,
     *    				"activationKey": null,
     *    				"confirmationKey": null,
     *    				"access_token": null,
     *    				"hashKey": null,
     *    				"device": "android",
     *    				"device_token": "",
     *    				"lastLogin": 1526628472,
     *    				"latitude": null,
     *    				"longitude": null,
     *    				"timezone": null,
     *    				"timezone_offset": null,
     *    				"created_at": 1524141463,
     *    				"updated_at": 1526628472,
     *    				"storeid": null,
     *    				"promise_uid": "3085abb78c79ebb4",
     *    				"promise_acid": null,
     *    				"user_code": "AW-gurpreet.gct",
     *    				"referralCode": null,
     *    				"referral_percentage": null,
     *    				"agency_id": null,
     *    				"logo": ""
     *    			},
     *    			"agency": {
     *    				"status": 10,
     *    				"id": 15,
     *    				"name": "Abercromby’s145",
     *    				"email": "aastha14@gmail.com",
     *    				"mobile": 1234569874,
     *    				"logo": "http://localhost/AlphaWallet/html/frontend/web/agencylogo/Abercromby’s145181.jpg",
     *    				"backgroungImage": null,
     *    				"address": "[\" sco 206 - 207, sector 34 a, chandigarh - 160022\"]",
     *    				"latitude": "[\"30.72589\"]",
     *    				"longitude": "[\"76.75787\"]",
     *    				"about_us": "Gross Waddell Pty Ltd is a\ncommercial real estate agency,\noperating for over 20 years in the\nretail, office, industrial and\ndevelopment property sectors. We\nprovide Sales and Leasing, Asset\nManagement\nand\nCorporate\nServices to Vendors, Landlords and\nInvestors.",
     *    				"created_at": 1524048846,
     *    				"updated_at": 1524048846,
     *    				"is_favourite": "",
     *    				"facebook": "https://www.facebook.com/",
     *    				"twitter": "https://twitter.com/",
     *    				"instagram": "https://www.instagram.com/"
     *    			},
     *    			"saveauction": null,
     *    			"sold": "0",
     *    			"calendar": 0
     *    		},
     *    	],
     *    	"_links": {
     *    		"self": {
     *    			"href": "http://localhost/AlphaWallet/html/api/web/v1/properties/get-agent-properties?page=1"
     *    		}
     *    	},
     *    	"_meta": {
     *    		"totalCount": 4,
     *    		"pageCount": 1,
     *    		"currentPage": 1,
     *    		"perPage": 20
     *    	}
     *    },
     *    "status": 200
     *    
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [],
	 *        "status": 422
	 *    }
	 *
	 * @return Object(common\models\Property)
	 */
	public function actionGetAgentProperties() {
		if (!empty(Yii::$app->getRequest()->getHeaders()->get('Authorization'))) {
		   	$token = Yii::$app->getRequest()->getHeaders()->get('Authorization');
			$user_id = Yii::$app->MyConstant->AgentID($token);
			if (!empty($user_id)) {
				$findUser = User::findOne(['id'=>$user_id]);
			 	if ($findUser->auth_key!=str_replace("Bearer ", "", $token)) {
					$response['name']="Unauthorized";
					$response['message'] = 'You are requesting with an invalid credential';
					$response['code']=0;
					$response['statusCode']=401;
					$response['type']="yii\\web\\UnauthorizedHttpException";
					return $response;	
				}
			}else{
				$response['name']="Unauthorized";
				$response['message'] = 'You are requesting with an invalid credential';
				$response['code']=0;
				$response['statusCode']=401;
				$response['type']="yii\\web\\UnauthorizedHttpException";
				return $response;	
			}
		}

		$propertysearchModel = new PropertySearch();

		// $searchByAttr['PropertySearch'] = Yii::$app->request->queryParams;
		$searchByAttr['PropertySearch']['estate_agent_id'] = $user_id;
		$searchByAttr['PropertySearch']['sort'] = 'date';
		$searchByAttr['PropertySearch']['list'] = '2';
		$searchByAttr['nearfuture'] = '';

		return $propertysearchModel->search($searchByAttr);
	}
	
	protected function findAuctionModel($id)
    {
        if (($model = Auction::findOne(['listing_id'=>$id])) !== null) {
            return $model;
        } else {
            return new User();
        }
    }
    
	protected function findModel($id)
    {
        if (($model = Property::findOne($id)) !== null) {
            return $model;
        } else {
            return new User();
        }
    }
}