<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\LoginLogs;
use common\models\User;
use common\models\Usertype;
use common\models\ChangePassword;
use common\models\Foodnotification;
use common\models\Property;
use api\modules\v1\models\Customer;
use api\modules\v1\models\LoginCviewUser;
use api\modules\v1\models\Bidder;
use api\modules\v1\models\Agent;
use api\modules\v1\models\Profiles;
use api\modules\v1\models\ForgotPassword;
use api\modules\v1\models\FacebookLogin;
use api\modules\v1\models\LinkedinLogin;
use api\modules\v1\models\GoogleLogin;
use api\modules\v1\models\Login;
use api\components\Controller;
use api\modules\v1\models\Foodordersapi;
use api\modules\v1\models\Agency;
use api\modules\v1\controllers\EmailController;
use common\models\CommonModel;

/**
 * UserController
 *
 * Login|Registration and other user management methods for APIs are defined here.
 */
class UserController extends Controller
{
    const USER_TYPE_BIDDER = '2';

    public $modelClass = 'api\modules\v1\models\Customer';
    //public $modelClass = 'backend\models\Usermanagement';

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['subfees'],
            'only'=>[
                'update',
                'view',
                'changepassword',
                'referral',
                'referral-history',
                'referral-cashout',
                'delete-agent',
                'update-agent',
            ],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "update" actions
        unset($actions['delete'], $actions['index'], $actions['update'],$actions['create']);

        return $actions;
    }

	/**
	 * Custom Login api to allow user login into the system, user includes bidders
	 *  
	 * - URI: *api/web/v1/users/login
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - username (string)|required 
	 *   - password (string)|required
	 *   - device   (string)|'android' or 'iphone'
	 *
	 * @api {post} api/web/v1/users/login Login
	 * @apiName Login User
	 * @apiVersion 0.1.1
	 * @apiGroup User
	 * @apiDescription To login a user or bidder, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} username User's username.
	 * @apiParam {String} password User's password.
	 * @apiParam {String} device   'ios|android|web'.
	 * @apiParam {String} device_token   Random String token 'assss2mlkamladsasd289'.
	 * @apiParam {Number} app_id (Optional) '2' to force user login as bidder, if not specified the login will be forced upon site users and bidders won't be able to login.   
	 *
	 * @apiSuccess {String} firstname Firstname of the User.
	 * @apiSuccess {String} lastname  Lastname of the User.
	 *
	 * @apiSuccessExample Success-Response:
	 *	HTTP/1.1 200 OK
	 *	{
     *	"message": "",
     *   "data": {
     *		"id": 367,
     *    	"username": "testuser1",
     *    	"email": "testuser1@yopmail.com",
     *    	"app_id": "2",
     *    	"auth_key": "3_ggKqBVuyzFG-3U-0_9FRumPmhIFXka",
     *    	"firstName": "test",
     *    	"lastName": "user",
     *    	"fullName": "test user",
     *    	"fbidentifier": null,
     *    	"linkedin_identifier": null,
     *    	"google_identifier": null,
     *    	"phone": null,
     *    	"image": null,
     *    	"sex": null,
     *    	"dob": "2001-02-05",
     *    	"ageGroup_id": 1,
     *    	"bdayOffer": 0,
     *    	"walletPinCode": null,
     *    	"loyaltyPin": null,
     *    	"advertister_id": null,
     *    	"device": "android",
     *    	"device_token": "",
     *    	"latitude": null,
     *    	"longitude": null,
     *    	"timezone": null,
     *    	"timezone_offset": null,
     *    	"storeid": null,
     *    	"promise_uid": "3675af2ec527bf32",
     *    	"promise_acid": null,
     *    	"user_code": "AW-testuser1",
     *    	"referralCode": null,
     *    	"referral_percentage": null,
     *    	"agency_id": null
     *    },
     *    "status": 200
     *   }	
     *
     * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 * Or
	 *	HTTP/1.1 422 Unprocessable entity
	 *	{
	 *		"message": "",
	 *		"data": [
	 *			{
	 *				"field": "password",
	 *				"message": "That  Pin does not match the registered email address. Please try again or click on Forgot  Pin? to reset it."
	 *			}
	 *		],
	 *		"status": 422
	 *	}
	 *
	 *	@return Object(api\modules\v1\models\Customer)
	 */
    public function actionLogin()
    {
        $data = Yii::$app->getRequest()->getBodyParams();
        $requestForBidder = $this->isBidderRequested($data);
        if ($requestForBidder) {
            $model = new LoginCviewUser();
        } else {
            $model = new Login();
        }
        $model->isMobile=true;
        $data=array('Login'=>Yii::$app->getRequest()->getBodyParams());
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
    		$userModel = Customer::findOne(Yii::$app->user->identity->id);
            isset($data['Login']['latitude'])?$userModel->latitude=$data['Login']['latitude']:NULL;
            isset($data['Login']['longitude'])?$userModel->longitude=$data['Login']['longitude']:NULL;
            $userModel->device_token=isset($data['Login']['device_token'])?$data['Login']['device_token']:'';

            // check if user is registerd on promise pay
            $payUserId = $userModel->promise_uid;
            if ($payUserId=='') {
                $result=Yii::$app->promisePay->createUser($userModel);
                if(isset($result->id)){
                    $userModel->promise_uid=$result->id;
                }
            }
            // assign referral code
            if ($userModel->user_code=='') {
                $userModel->user_code = Yii::t('app', 'AW-').$userModel->username;
            }
            $userModel->save(false);
            if (!strstr($userModel->dob,"-")) {
                $userModel->dob=date("Y-m-d",$userModel->dob);
            }
            $userModel = $this->postLoginCheck($userModel, $data);
            if (!$userModel) {
                $model->validate(null,false);
                $userModel = $model;
                return [['notice' => 'Invalid login credentials', 'user_data' => $userModel],'statusCode'=>422];  
            }

        } else {
            $model->validate(null,false);
            $userModel = $model;
        }

        return $userModel;

    }

	/**
	 * Logout API for users and bidders
	 *  
	 * - URI: *api/web/v1/users/logout
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *
	 * @api {post} api/web/v1/users/logout Logout
	 * @apiName Logout
	 * @apiVersion 0.1.1
	 * @apiGroup User
	 * @apiDescription To logout a user or bidder, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} access-token (required) The access-token of the logged-in user   
	 *
	 * @apiSuccess {String} message
	 * @apiSuccess {String} data  Text response
	 * @apiSuccess {String} status 
	 *
	 * @apiSuccessExample Success-Response:
	 *    If invalid/expired access token
	 *    HTTP/1.1 200 OK
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            "Please provide an validate user token"
	 *        ],
	 *        "status": 422
	 *    }
	 *    
	 *    If valid access-token
	 *    HTTP/1.1 200 OK
	 *    {
	 *        "message": "",
	 *        "data": "Logout Successfully",
	 *        "status": 200
	 *    }
	 *
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *    }
	 *
	 * @return Object(api\modules\v1\models\Customer)
	 */
    public function actionLogout()
    {
        $model=new User();
        if((Yii::$app->request->post()) && ($model=$model->findIdentityByAccessToken(Yii::$app->request->post()['access-token']))) {
            try{
                $loginlog=LoginLogs::findOne(['sessionid'=>$model->auth_key]);
                if($loginlog){
                    $loginlog->logout_at=time();
                    $loginlog->save();
                }
            }
            catch(Exception $e){

            }
            $model->auth_key="";
            $model->device_token="";
            $model->save();
            return "Logout Successfully";
        } else {
            return ["Please provide an validate user token",'statusCode'=>422];
        }
    }

	/**
	 * To create new users/bidders
	 *
	 * @param app_id=2 To sign-up as a bidder.
	 *
	 * @api {post} /api/web/v1/user/create Create User
	 * @apiName Create User
	 * @apiVersion 0.1.1
	 * @apiGroup User
	 * @apiDescription To create new users/bidders, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} firstName User's firstname.
	 * @apiParam {String} lastName User's lastname.
	 * @apiParam {String} username User's username.
	 * @apiParam {String} email User's email.
	 * @apiParam {String} password_hash User's password.
	 * @apiParam {String} dob Date of Birth (date must in YYYY-MM-DD format).
	 * @apiParam {Number} app_id (Optional) '2' to force user registration as bidder   
	 * @apiParam {String} fbidentifier (Optional) Access token received after FB login
	 * @apiParam {String} linkedin_identifier (Optional) Access token received after Linkedin login
	 * @apiParam {String} google_identifier (Optional) Access token received after Google Login
	 *
	 * @apiSuccess {String} firstname Firstname of the User.
	 * @apiSuccess {String} lastname  Lastname of the User.
	 *
	 * @apiSuccessExample Success-Response:
	 *		HTTP/1.1 200 OK
	 *		{
	 *			"message": "",
	 *		 	"data": {
	 * 		   		"app_id": "2",
	 *          	"firstName": "test",
	 *          	"lastName": "user",
	 *          	"email": "testuser1@yopmail.com",
	 *          	"username": "testuser1",
   	 *          	"dob": 981331200,
	 *          	"auth_key": "H9jxjdaX6Q0q6N2X9TQnc5Z025U_KcWM",
	 *          	"fullName": "test user",
	 *          	"user_code": "AW-testuser1",
	 *          	"id": 367,
	 *          	"promise_uid": "3675af2ec527bf32"
	 *			},
	 *			"status": 200
	 *		}
	 *
	 * @apiError Validation Error Make sure to fix the listed issues to create a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 422 Unprocessable entity
	 *     {
	 *     	 "message": "",
	 *     	 "data": [
	 *     	   {
	 *     		 "field": "username",
	 *     		 "message": "The username \"testuser1\" has already been taken. The following usernames are available: \"test_277, user.471, testuser1_446, test.user, testuser\". "
	 *     	   }
	 *       ],
	 *       "status": 422
	 *     }	 
	 * 
	 * @return Object|User-model
	 */
    public function actionCreate()
    {
        $data = Yii::$app->getRequest()->getBodyParams();
        $requestForBidder = $this->isBidderRequested($data);
        if ($requestForBidder) {
            $model= new Bidder();
        } else {
            $model = new Customer();
        }

        $model->scenario = "create,update";
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        
        if (!$model->validate()) {
            return $model;
        }

        $profile=new Profiles();
        $profile->usertype_id=3;
        $profile->commercialStatus=3;

        foreach ($profile->attributes() as $key) {
            $profile->$key = "";
        }

        if (isset($data['profile'])) {
            $profile->load($data['profile'],'');
        }

        if (!empty($model->image!="")) {
            $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $model->image));
            $imageName=$model->username.rand(10,1000). ".jpg";
            $imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['profileUploadPath'].$imageName);
            file_put_contents($imagePath, $data);
            $model->image=$imageName;
        }

        if (!empty($data['mediacare_number'])) {
            $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['mediacare_number']));
            $fileAmalMedicareName = 'medicare_'.$model->username.'_'.rand(10,1000). ".jpg";
            $fileAmalMedicarePath = str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['amalUploadPath'].$fileAmalMedicareName);
            @file_put_contents($fileAmalMedicarePath, $file);
            $profile->mediacare_number = $fileAmalMedicareName;
        }

        if ( !empty($data['recent_utility_bill']) && $data['recent_utility_bill'] != "") {
            $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['recent_utility_bill']));
            $fileAmalRecentUtilityName = 'recent_utility_'.$model->username.'_'.rand(10,1000). ".jpg";
            $fileAmalMedicarePath = str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['amalUploadPath'].$fileAmalRecentUtilityName);
            file_put_contents($fileAmalMedicarePath, $file);
            $profile->recent_utility_bill = $fileAmalRecentUtilityName;
        }

        if (($model->save() === false ) && (!$model->hasErrors() )) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        } else {
            $profile->user_id=$model->id;
            $profile->foodEstablishment=!empty($data['foodEstablishment'])?$data['foodEstablishment']:0;
            $profile->save(false);
            $model->getImageUrl();
        }

        if ($requestForBidder) {
            $this->createBidder($model, $data);
        }

        if (!strstr($model->dob,"-")) {
            $model->dob=date("Y-m-d",$model->dob);
        }
        
        return $model;
    }

	/**
	 * Check username
	 *
	 * - URI: *api/web/v1/users/checkusername
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - username (string)|required
	 *   - email (string)|required
	 *
	 * @api {post} api/web/v1/users/checkusername Check Username
	 * @apiName Check Username
	 * @apiVersion 0.1.1
	 * @apiGroup User
	 * @apiDescription To check if a username and email is available, Form-Data must be x-www-form-urlencoded
     *
	 * @apiParam {String} username User's Username to validate.
	 * @apiParam {String} email Email to valdiate.
	 *
	 * @apiSuccess {String} message 
	 * @apiSuccess {String} data Json obj consisting of key pairs field and message
 	 * @apiError {String} status 422
	 *
	 * @apiSuccessExample Success-Response:
	 *    If username is not available
	 *    HTTP/1.1 422 Unprocessable entity
	 *    {
	 *        "message": "",
	 *        "data": [
	 *             {
	 *                 "field": "username",
	 *                 "message": "The username \"testuser1\" has already been taken. The following usernames are available:               \"testuser1_224, testuser1.162, testuser1_783, testuser1.testuser1, testuser1testuser1\"."
	 *             },
	 *             {
	 *                 "field": "email",
	 *                 "message": "That email address has already been registered. Please enter a valid email address."
	 *             }
	 *         ],
	 *         "status": 422
	 *    }
	 *
	 *    If username is available
	 *    HTTP/1.1 200 OK
	 *    {
	 *        "message": "",
	 *        "data": [
	 *             {
	 *                 "field": "email",
	 *                 "message": "That email address has already been registered. Please enter a valid email address."
	 *             }
	 *         ],
	 *         "status": 422
	 *    }
	 *
	 * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Unprocessable entity
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "field": "username",
	 *               "message": "Username in here!! cannot be blank."
	 *            },
	 *            {
	 *                "field": "email",
	 *                "message": "Email cannot be blank."
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 *
	 * @return Object(api\modules\v1\models\Customer)
     */
    public function actionCheckusername()
    {
        $model= new Customer();
        //$model->scenario= "checkusername";
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->validate()) {
            $model = 'success';
            return($model);
        } else {
            // validation failed: $errors is an array containing error messages
            //$errors = $model->errors;
            return($model);
        }
    }

	/**
	 * Check valid email
	 *
	 * - URI: *api/web/v1/users/checkemail
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - email (string)|required
	 *
	 * @api {post} api/web/v1/users/checkemail Check Email
	 * @apiName Check Email
	 * @apiVersion 0.1.1
	 * @apiGroup User
	 * @apiDescription To check if an email is available, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} email Email to valdiate.
	 *
	 * @apiSuccess {String} message 
	 * @apiSuccess {String} data Json obj consisting of key pairs field and message
	 * @apiSuccess {String} status 422
	 *
	 * @apiSuccessExample Success-Response:
	 *    If email is available
	 *    HTTP/1.1 200 OK
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "field": "username",
	 *                "message": "Username in here!! cannot be blank."
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 *    
	 *    If email not available
	 *    HTTP/1.1 422 Unprocessable entity
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "field": "username",
	 *                "message": "Username in here!! cannot be blank."
	 *            },
	 *            {
	 *                "field": "email",
	 *                "message": "That email address has already been registered. Please enter a valid email address."
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 *
	 * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Unprocessable entity
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "field": "username",
	 *               "message": "Username in here!! cannot be blank."
	 *            },
	 *            {
	 *                "field": "email",
	 *                "message": "Email cannot be blank."
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 *
	 * @return Object(api\modules\v1\models\Customer)
	 */
    public function actionCheckemail()
    {
        $model= new Customer();
        //$model->scenario= "checkemail";
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->validate()) {
            $model = 'success';
            return($model);
        } else {
            // validation failed: $errors is an array containing error messages
            return($model);
        }
    }


	/**
	 * Update user data
	 * 
	 * Sample call api/web/v1/users/update/?id=264
	 * Data should be x-www-form-urlencoded
	 * @param Id of the current loggedin user.
	 * 
	 * @api {put,patch} api/web/v1/users/update?id=264 Update profile
	 * @apiName Update User
	 * @apiVersion 0.1.1
	 * @apiGroup User
	 * @apiDescription To update profile of a user or bidder. Requires login|access token to perform the update, Form-Data must be x-www-form-urlencoded  
	 *
	 * @apiParam {String} username Username
	 * @apiParam {String} email Email
	 * @apiParam {String} firstName Firstname
	 * @apiParam {String} lastName Lastname
	 * @apiParam {String} fullName Fullname
	 * @apiParam {String} phone Phone
	 * @apiParam {String} image Base64 Image
	 * @apiParam {String} sex Male => 1| Female => 0
	 * @apiParam {String} dob Date of Birth (date must in YYYY-MM-DD format).
	 * @apiParam {String} fbidentifier (Optional) Access token received after FB login
	 * @apiParam {String} linkedin_identifier (Optional) Access token received after Linkedin login
	 * @apiParam {String} google_identifier (Optional) Access token received after Google Login
	 * @apiParam {String} ageGroup_id 1
	 * @apiParam {String} latitude 1234.43
	 * @apiParam {String} longitude 3214.43
	 * @apiParam {String} timezone Asia/Kolkata
	 * @apiParam {String} timezone_offset +05:30
	 *
	 * @apiSuccess {String} firstname Firstname of the User.
	 * @apiSuccess {String} lastname  Lastname of the User.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "message": "",
	 *       "data": {
	 *           "id": 313,
	 *           "username": "john",
	 *           "email": "john_doe@mail.com",
	 *           "auth_key": "-3_xNTB7hyuR7UaJ0P1HHXgDhb8km-ao",
	 *           "firstName": "John",
	 *           "lastName": "Doe",
	 *           "fullName": "John Doe",
	 *           "fbidentifier": null,
	 *           "phone": null,
	 *           "image": null,
	 *           "sex": null,
	 *           "dob": "1990-01-01",
	 *           "ageGroup_id": 1,
	 *           "bdayOffer": 0,
	 *           "walletPinCode": null,
	 *           "loyaltyPin": null,
	 *           "advertister_id": null,
	 *           "device": "android",
	 *           "device_token": "",
	 *           "latitude": null,
	 *           "longitude": null,
	 *           "timezone": null,
	 *           "timezone_offset": null,
	 *           "storeid": null,
	 *           "promise_uid": "3085abb78c79ebb4",
	 *           "promise_acid": null,
	 *           "user_code": "AW-john_doe",
	 *           "referralCode": null,
	 *           "referral_percentage": null
	 *       },
	 *       "status": 200
	 *     }
	 *
	 * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 401 Unauthorized
	 *     {
	 *       "name": "Unauthorized",
	 *       "message": "Your request was made with invalid credentials.",
	 *       "code": 0,
	 *       "status": 401,
	 *       "type": "yii\\web\\UnauthorizedHttpException"
	 *     }
	 *
	 * @return UserModel
	 */
    public function actionUpdate($id)
    {   
        $id=Yii::$app->user->id;
        $model= Customer::findOne($id);
        $model->scenario="create,update";
        $oldImageName=$model->image;
	    $oldUsername=$model->username;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->usernameChanged=$oldUsername==$model->username?false:true;
        $data=Yii::$app->getRequest()->getBodyParams();
        //print_r($data);die;
        // if (!empty($data['profile'])) {
            // $model->profile->load($data['profile'],'');
            // $model->profile = $this->saveProfile($model, $data['profile']);
            // $model->profile = $model->saveProfileData($model->profile, $data['profile']);
        // }
        if  (!empty($data['profile'])) {
             $model->profile->load($data['profile'],'');
            // foreach ($data['profile'] as $key=>$val) {
            //     $model->profile->{$key} = $val;
            // }
            $model->profile->usertype_id=3;
            $model->profile->commercialStatus=3;
        }

        if ($model->validate()) {
			//print_r();die;
            if ($model->image!="" && (!strstr($model->image,".jpg")) && (!strstr($model->image,"large"))) {

                $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $model->image));
				        preg_match('/.*\/(.*?)\./',$oldImageName,$match);
				        if (count($match)) {
                    $imageName=$match[1]. ".jpg";
                } else {
                    $imageName=$model->username.rand(10,1000). ".jpg";
                }

                $imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['profileUploadPath'].$imageName);

                file_put_contents($imagePath, $file);
				$model->image = $imageName;
            }

            if (!empty($data['profile']['mediacare_number']) && $data['profile']['mediacare_number'] != "") {
                $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['profile']['mediacare_number']));
                $fileAmalMedicareName = 'medicare_'.$model->username.'_'.rand(10,1000). ".jpg";
                $fileAmalMedicarePath = str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['amalUploadPath'].$fileAmalMedicareName);
                file_put_contents($fileAmalMedicarePath, $file);
                $model_profile->mediacare_number = $fileAmalMedicareName;
            }

            if ( !empty($data['profile']['recent_utility_bill']) && $data['profile']['recent_utility_bill'] != "") {
                $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['recent_utility_bill']));
                $fileAmalRecentUtilityName = 'recent_utility_'.$model->username.'_'.rand(10,1000). ".jpg";
                $fileAmalMedicarePath = str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['amalUploadPath'].$fileAmalRecentUtilityName);
                file_put_contents($fileAmalMedicarePath, $file);
                $model_profile->recent_utility_bill = $fileAmalRecentUtilityName;
            }

            if (($model->save(false) === false || ($model->profile->save(false)==false)) && (!$model->hasErrors() || !$model_profile->hasErrors())) {
				        throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
            } else {
                $model->getImageUrl();
                if(!strstr($model->dob,"-")) {
                    $model->dob=date("Y-m-d",$model->dob);
                }

                return $model;
	        }
        } else {
             $errors = $model->errors;
             $errors['profile'] = $model_profile->errors;

             return $model;
        }
    }

	/**
	 * Change Password for loggedin user
	 *  
	 * - URI: *api/web/v1/users/changepassword
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - username (string)|required 
	 *   - password (string)|required
	 *   - device   (string)|'android' or 'iphone'
	 *
	 * @api {post} api/web/v1/users/changepassword Change password
	 * @apiName Change Password
	 * @apiVersion 0.1.1
	 * @apiGroup User
	 * @apiDescription To change a user's or bidder's password, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} password Existing password.
	 * @apiParam {String} newPassword new password.
	 *
	 * @apiSuccessExample Success-Response:
	 *    
	 *    HTTP/1.1 422 Unprocessable entity
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "field": "newPassword",
	 *                "message": "New Password cannot be blank."
	 *            },
	 *            {
	 *                "field": "password",
	 *                "message": "Password cannot be blank."
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 * Or
	 *    HTTP/1.1 422 Unprocessable entity
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "field": "password",
	 *                "message": "Current password is not matched."
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 *
	 * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "field": "password",
	 *                "message": "Please enter a registered email address and  password."
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 * Or
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "notice": "Invalid login credentials",
	 *                "user_data": {
	 *                    "username": "gurpreet.gct",
	 *                    "password": "password123",
	 *                    "rememberMe": true,
	 *                    "isMobile": true,
	 *                    "device": "android",
	 *                    "resolution": null
	 *                }
	 *           }
	 *       ],
	 *       "status": 422
	 *    }
	 *
	 * @return Object(api\modules\v1\models\Customer)
	 */
    public function actionChangepassword()
    {
        $id=Yii::$app->user->id;
        $model=new ChangePassword;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $model->id=$id;
        $model->changePassword();
        return $model;
    }

	/**
	 * Forgot Password
	 *  
	 * - URI: *api/web/v1/users/forgot
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - email (string)|required 
	 *
	 * @api {post} api/web/v1/users/forgot Forgot password
	 * @apiName Forgot Password
	 * @apiVersion 0.1.1
	 * @apiGroup User
	 * @apiDescription To recover forgotten password of a user or bidder, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} email User's email.
	 * @apiParam {Number} app_id '2' for CVIEW
	 *
	 * @apiSuccess {String} message.
	 * @apiSuccess {String} data
	 * @apiSuccess {Number} status
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *     }
	 *
	 * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 * Or
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [
	 *       ],
	 *       "status": 422
	 *    }
	 *
	 * @return Object(api\modules\v1\models\Customer)
	 */
    public function actionForgot()
    {
        $model= new ForgotPassword();
        if (isset($_REQUEST['app_id']) && $_REQUEST['app_id'] == self::USER_TYPE_BIDDER) {
        	$model->scenario = ForgotPassword::SCENARIO_CVIEW_USER;
        } else {
        	$model->scenario = ForgotPassword::SCENARIO_ALPHA_USER;
        }
        
        $model->load(Yii::$app->request->getBodyParams(),'');
        if ($model->validate()) {
            $model = $model->getUserObject();
            if(!empty($model)){
            $validtoken=$model->generatePasswordResetToken();
            $model->save(false);
            $pasMessage ='password';
            
            if (isset($user->user_type) && $user->user_type==3) {
                $pasMessage ='Pin';
            }
            if ($model->isPasswordResetTokenValid($validtoken)) {
                $subject="AlphaWallet:Forgot Password";
                if ( isset($_REQUEST['app_id']) && $_REQUEST['app_id'] == self::USER_TYPE_BIDDER) {
                	$subject="CVIEW:Forgot Password";	
                }
                
                if (isset($_REQUEST['app_id']) && $_REQUEST['app_id'] == self::USER_TYPE_BIDDER) {
                	$sitepath = Yii::$app->urlManagerBackEnd->createAbsoluteUrl(['user/reset-cview', 'token' => $validtoken]);
                	$sitepath .= "&app_id=". $_REQUEST['app_id'];
                } else {
                	$sitepath = Yii::$app->urlManagerBackEnd->createAbsoluteUrl(['user/reset', 'token' => $validtoken]);
                }
            	$password_reset_template = 'passwordReset-mail-api';


                $message = sprintf("Dear %s,<br/>Please follow the below mentioned url to reset your $pasMessage.<br/><a href='%s?token=%s'>Click Here to Reset %s</a>",$model->fullName,$sitepath,$model->password_reset_token,$pasMessage);
                $emailData  = [
                    'Name'=> $model->fullName,
                    'validtoken'=> $validtoken,
                    'pasMessage'=>$pasMessage,
                    'resetLink' => $sitepath
                ];

                $emailsend =Yii::$app->mail->compose(['html' => $password_reset_template],$emailData)
                    ->setTo($model->email)
                    ->setFrom(Yii::$app->params["noReplyEmail"],'No Reply')
                    ->setSubject($subject)
                    //->setHtmlBody($message)
                    ->send();
                $response = array();
               // print_r(array('cuul', 'is'));
               // exit();
                if ($emailsend) {
                    $response['statusCode'] = 200;
                    $response['message'] = "We have sent a reset $pasMessage  link to your email. Please follow the steps.";
                    return $response;
                } else {
                    $response['statusCode'] = 201;
                    $response['message'] = 'Email server is not working, Please try later.';
                    return $response;
                }
            } else {
                $response['statusCode'] = 201;
                $response['message'] = 'Email server is not working, Please try later.';
                return $response;
            }
		}
        }else{
			return $model;
		}

    }

	/**
	 * Custom Login api to allow user login into the system, user includes bidders
	 *  
	 * - URI: *api/web/v1/users/facebooklogin
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - username (string)|required 
	 *   - password (string)|required
	 *   - device   (string)|'android' or 'iphone'
	 *
	 * @api {post} api/web/v1/users/facebooklogin Facebook Login
	 * @apiName Facebook Login
	 * @apiVersion 0.1.1
	 * @apiGroup Social Login
	 * @apiDescription To login a user or bidder, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} token Facebook token access.
	 * @apiParam {String} device 'ios|android|web'. 
	 * @apiParam {Number} app_id (Optional) '2' to force user login as bidder, if not specified the login will be forced upon site users and bidders won't be able to login.
	 *
	 * @apiSuccess {String} status Firstname of the User.
	 * @apiSuccess {String} message  Lastname of the User.
	 * @apiSuccess {String} data  Data set consisting of the loggedin User.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "message": "",
	 *       "data": {
	 *           "id": 313,
	 *           "username": "john",
	 *           "email": "john_doe@mail.com",
	 *           "user_sub_type": 10,
	 *           "auth_key": "-3_xNTB7hyuR7UaJ0P1HHXgDhb8km-ao",
	 *           "firstName": "John",
	 *           "lastName": "Doe",
	 *           "fullName": "John Doe",
	 *           "fbidentifier": null,
	 *           "phone": null,
	 *           "image": null,
	 *           "sex": null,
	 *           "dob": "1990-01-01",
	 *           "ageGroup_id": 1,
	 *           "bdayOffer": 0,
	 *           "walletPinCode": null,
	 *           "loyaltyPin": null,
	 *           "advertister_id": null,
	 *           "device": "android",
	 *           "device_token": "",
	 *           "latitude": null,
	 *           "longitude": null,
	 *           "timezone": null,
	 *           "timezone_offset": null,
	 *           "storeid": null,
	 *           "promise_uid": "3085abb78c79ebb4",
	 *           "promise_acid": null,
	 *           "user_code": "AW-john_doe",
	 *           "referralCode": null,
	 *           "referral_percentage": null
	 *       },
	 *       "status": 200
	 *     }
	 *
	 * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "field": "password",
	 *                "message": "Please enter a registered email address and  password."
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 *    Or
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "notice": "Invalid login credentials",
	 *                "user_data": {
	 *                    "username": "gurpreet.gct",
	 *                    "password": "password123",
	 *                    "rememberMe": true,
	 *                    "isMobile": true,
	 *                    "device": "android",
	 *                    "resolution": null
	 *                }
	 *           }
	 *       ],
	 *       "status": 422
	 *    }
	 *
	 * @return Object(api\modules\v1\models\Customer)
	 */
    public function actionFacebooklogin()
    {
        $model=new FacebookLogin();
        $model->load(Yii::$app->request->getBodyParams(),'');
        if (!$model->validate()) {
            return $model;
        }
        $user=$model->getUserObject();
        $rs=Customer::findOne($user->id);
        isset ($data['Login']['latitude']) ? $rs->latitude = $data['Login']['latitude'] : NULL;
        isset ($data['Login']['longitude']) ? $rs->longitude = $data['Login']['longitude'] : NULL;
        $rs->device_token = isset($data['Login']['device_token']) ? $data['Login']['device_token'] : '';
        $rs->save(false);
        return $user;
        
    }

	/**
	 * Login with linkedIn
	 *  
	 * - URI: *api/web/v1/users/linkedinlogin
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - username (string)|required 
	 *   - password (string)|required
	 *   - device   (string)|'android' or 'iphone'
	 *
	 * @api {post} api/web/v1/users/linkedinlogin LinkedIn Login
	 * @apiName LinkedIn Login
	 * @apiVersion 0.1.1
	 * @apiGroup Social Login
	 * @apiDescription To login a user or bidder, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} token Linkedin unique user id, this can be obtained by accessing the linkedin user profile using access_token.
	 * @apiParam {String} device 'ios|android|web'. 
	 * @apiParam {Number} app_id (Optional) '2' to force user login as bidder, if not specified the login will be forced upon site users and bidders won't be able to login.
	 *
	 * @apiSuccess {String} status Firstname of the User.
	 * @apiSuccess {String} message  Lastname of the User.
	 * @apiSuccess {String} data  Data set consisting of the Linkedin data
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 OK
     *    {
     *	 	"message": "",
     *	 	"data": {
     *			"id": 351,
     *			"username": "gurpreet.gct",
     *			"email": "gurpreet@graycelltech.com",
     *			"app_id": "2",
     *			"auth_key": "p9q3q5d4SJqBKpvrybILG7oJeePkylQW",
     *			"firstName": "GPreet",
     *			"lastName": "singh",
     *			"fullName": "GPreet singh",
     *			"fbidentifier": null,
     *			"linkedin_identifier": "123qweasdcxz",
     *			"google_identifier": null,
     *			"phone": null,
     *			"image": null,
     *			"sex": null,
     *			"dob": "1970-01-01",
     *			"ageGroup_id": 1,
     *			"bdayOffer": 0,
     *			"walletPinCode": null,
     *			"loyaltyPin": null,
     *			"advertister_id": null,
     *			"device": "android",
     *			"device_token": "",
     *			"latitude": null,
     *			"longitude": null,
     *			"timezone": null,
     *			"timezone_offset": null,
     *			"storeid": null,
     *			"promise_uid": "3085abb78c79ebb4",
     *			"promise_acid": null,
     *			"user_code": "AW-gurpreet.gct",
     *			"referralCode": null,
     *			"referral_percentage": null,
     *			"agency_id": null
     *		},
     *		"status": 200
     *	}
	 *
	 * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Unprocessable entity
	 *    {
     *	 	"message": "",
     *		"data": [
     *			{
     *				"field": "token",
     *				"message": "Invalid token"
     *			}
     *		],
     *		"status": 422
     *	}
	 *
	 * @return Object(api\modules\v1\models\Customer)
	 */
    public function actionLinkedinlogin()
    {
    	$model = new LinkedinLogin();
    	$data = Yii::$app->request->getBodyParams();
        
        $model->load(Yii::$app->request->getBodyParams(),'');
        if (!$model->validate()) {
            return $model;
        }
        // ['id' => $user->id, 'app_id' => self::APP
        $user = $model->getUser();
        // $rs = Customer::findOne(['id' => $user->id, 'app_id' => self::APP);
        return $user;
    }

	/**
	 * Login with Google
	 *  
	 * - URI: *api/web/v1/users/googlelogin
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - username (string)|required 
	 *   - password (string)|required
	 *   - device   (string)|'android' or 'iphone'
	 *
	 * @api {post} api/web/v1/users/googlelogin Google Login
	 * @apiName Google Login
	 * @apiVersion 0.1.1
	 * @apiGroup Social Login
	 * @apiDescription To login a user or bidder using Google, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} token Google access token.
	 * @apiParam {String} device 'ios|android|web'. 
	 * @apiParam {Number} app_id (Optional) '2' to force user login as bidder, if not specified the login will be forced upon site users and bidders won't be able to login.
	 *
	 * @apiSuccess {String} status
	 * @apiSuccess {String} message
	 * @apiSuccess {String} data
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 OK
	 *        {
	 *            "message": "",
	 *            "data": [
	 *                {
	 *                    "field": "apiData",
	 *                    "message": {
	 *                    "azp": "123432232233-asknjdaasdasd6aadlo37s.apps.googleusercontent.com",
	 *                    "aud": "123432232233-asdnkja788l777mco9jkqjkat2mrbdsk.apps.googleusercontent.com",
	 *                    "sub": "107474798829959527628",
	 *                    "email": "testgtect@gmail.com",
	 *                    "email_verified": "true",
	 *                    "exp": "1523950468",
	 *                    "iss": "https://accounts.google.com",
	 *                    "iat": "1523946868",
	 *                    "name": "gtect sharma",
	 *                    "picture": "https://lh3.googleusercontent.com/-jGYhN2K3DaU/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWDYkBVrYGA7wPTLexos_AKAUUTdpw/s96-c/photo.jpg",
	 *                    "given_name": "gtect",
	 *                    "family_name": "sharma",
	 *                    "locale": "en",
	 *                    "alg": "RS256",
	 *                    "kid": "5425bb84616ebf973ae80bc62ac688d2a72715ad"
	 *                }
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "field": "token",
	 *                "message": "Invalid token"
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 * Or
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "field": "token",
	 *                "message": "Token cannot be blank.",
	 *           }
	 *       ],
	 *       "status": 422
	 *    }
	 *
	 * @return Object(api\modules\v1\models\Customer)
	 */
    public function actionGooglelogin()
    {   
        $model = new GoogleLogin();
        $model->load(Yii::$app->request->getBodyParams(),'');
        if (!$model->validate()) {
            return $model;
        }
        $user=$model->getUserObject();
        $rs=Customer::findOne($user->id);
        isset ($data['Login']['latitude']) ? $rs->latitude = $data['Login']['latitude'] : NULL;
        isset ($data['Login']['longitude']) ? $rs->longitude = $data['Login']['longitude'] : NULL;
        $rs->device_token = isset($data['Login']['device_token']) ? $data['Login']['device_token'] : '';
        $rs->save(false);
        return $user;
    }

	/**
	 * Validate a user
	 *
	 * - URI: *api/web/v1/users/validateuser
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - username (string)|required 
	 *   - email (string)|required
	 *
	 * @api {post} api/web/v1/users/validateuser Validate User
	 * @apiName Validate User
	 * @apiVersion 0.1.1
	 * @apiGroup User
	 * @apiDescription To check if a username and email is available, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} username User's Username to validate.
	 * @apiParam {String} email Email to valdiate.
	 *
	 * @apiSuccess {String} message 
	 * @apiSuccess {String} data Json obj consisting of key pairs field and message
	 * @apiSuccess {String} status 422
	 *
	 * @apiSuccessExample Success-Response:
	 *    If user is not validated
	 *    HTTP/1.1 422 Unprocessable entity
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *                "field": "username",
	 *                "message": "The username \"lorem.ipsum\" has already been taken. The following usernames are available: \"lorem.ipsum, lorem.ipsum.341, lorem.ipsum, lorem.ipsum.lorem.ipsum, lorem.ipsum.ipsum\". "
	 *            },
	 *            {
	 *                "field": "email",
	 *                "message": "That email address has already been registered. Please enter a valid email address."
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 *
	 *    If user email and password is validated
	 *    HTTP/1.1 200 OK
	 *    {
	 *        "message": "",
	 *        "data": {
	 *            "username": "lorem.as",
	 *            "email": "lorem_ipsum@email.com"
	 *        },
	 *        "status": 200
	 *    }
	 *
	 * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 *	HTTP/1.1 422 Unprocessable entity
	 *	{
	 *		"message": "",
	 *		"data": [
	 *			{
	 *				"field": "username",
	 *				"message": "Username in here!! cannot be blank."
	 *			},
	 *			{
	 *				"field": "email",
	 *				"message": "Email cannot be blank."
	 *			}
	 *		],
	 *		"status": 422
	 *	}
	 *
	 *
	 * @return Object(api\modules\v1\models\Customer)
	 */
    public function actionValidateuser()
    {
        $model= new Customer();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        $data=Yii::$app->getRequest()->getBodyParams();
        if ($model->validate()) {
            //Yii::$app->getResponse()->setStatusCode(200);
            return $model;
        } else {
            //Yii::$app->getResponse()->setStatusCode(422);
            return $model;
        }
    }

    public function actionSendnote(){

        $foodOrder = Foodordersapi::find()->where(['status'=>1])->asArray()->all();

        foreach($foodOrder as $_foodOrder){

            $getUserData 			= User::find()
            ->where(['id'=>$_foodOrder['user_id'],'user_type'=>3])
            ->one();
            if (isset($foodOrder['id'])) {
                $check = Foodnotification::findOne(['user_id'=>$_foodOrder['user_id']]);
                if (isset($check->id)) {
                    $check->updated_at = time();
                    $check->status =0;
                    $check->save(false);
                } else {
                    $modle = new Foodnotification();
                    $modle->user_id = $_foodOrder['user_id'];
                    $modle->updated_at = time();
                    $check->status = 0;
                    $modle->save(false);
                }

            }
            /* send notification for enable gps and bluetoth if user have food order */
            $up = Foodnotification::find()
                ->where('user_id='.$_foodOrder['user_id'])
                ->andWhere('status=0')
                ->asArray()->one();
            $now = time();
            $diiffTime =  round(abs($now - $up['updated_at']) / 60,2);
            if ($diiffTime > 15) {
                $userDevice[$getUserData->device_token] = $getUserData->device;
                return yii::$app->pushNotification->send($userDevice,["msg"=>'You have a pending food order. Turning off Bluetooth or location services will prevent us from completing this order.'],0);
            }
        }

    }

    /**
     * Post login check for user type
     * Checks for the user type post login authentication.
     * @return User OBJ True|If login requested belonged to the adequate Bidder
     */
    public function postLoginCheck($authenticatedUser, $dataPassed)
    {   
        // Return the default authenticated user, when @param type not sent in POST.
        // $user_sub_type = Usertype::getBidderType();
        // Assuming normal customer login requested
        if (empty($dataPassed['Login']['app_id']) || (strtolower($dataPassed['Login']['app_id']) != self::USER_TYPE_BIDDER)) {
            // Check for the authenticated customer as non bidder, block login if user_type not bidder
            if ($authenticatedUser->app_id == self::USER_TYPE_BIDDER) {
                return false;
            }

            return $authenticatedUser;
        }
        // Check for the authenticated customer as bidder, allow login only if true
        if ($authenticatedUser->app_id !== self::USER_TYPE_BIDDER) {
            return false;
        }
        return $authenticatedUser;
    }

    /**
     * Get referral earning of user 
     */
    public function actionReferral()
    {
        $ref = new \common\models\ReferralCashamount();
        return $ref->getUserReferral(Yii::$app->user->identity->id);
    }

    /* referral commision history */
    public function actionReferralHistory()
    {
        $query =	\common\models\ReferralEarnings::find()
            ->where(['refer_by'=>Yii::$app->user->identity->id]);
        return new ActiveDataProvider([
            'query' => $query
        ]);
    }

    /* cash out the referral amount */
    public function actionReferralCashout()
    {
        $id = Yii::$app->user->identity->id;
        $model = \common\models\ReferralCashamount::findOne(['user_id'=>$id]);
        $minimumAmt = \common\models\Setting::getCashOutLimit();
        if (isset($model->unused_commission) && $minimumAmt>$model->unused_commission) {
            $response['statusCode'] = 201;
            $response['message'] = 'You have not enough amount for cashout.';
            return $response;
        }
        $creditCards=   \common\models\Paymentcard::find()->select(['cardholderName as account_name','uniqueNumberIdentifier as promise_acid'])
            ->where(['user_id'=>$id])
            ->andWhere('uniqueNumberIdentifier is not null')
            ->andWhere("issuingBank='promisePay'")
            ->asArray()
            ->all();

        if (count($creditCards)>0) {
            $itemId = md5(time().'aw');
            $itemName = Yii::$app->user->identity->username;
            $promiseUid = Yii::$app->user->identity->promise_uid;
            $item = Yii::$app->promisePay->createItemCashout($itemId,$model->unused_commission,$promiseUid,$itemName);
            if (isset($item['id'])) {
                $pay = Yii::$app->promisePay->referralCashOut($item['id']);
                if (isset($pay['id'])) {
                    $model->used_commission = $model->used_commission+$model->unused_commission;
                    $model->unused_commission = 0;
                    $model->save(false)	;
                    $response['statusCode'] = 200;
                    $response['message'] = 'Cash out success.';
                    return $model;
                } else {
                    $response['statusCode'] = 201;
                    $response['message'] = $pay;
                    return $response;
                }
            } else {
                $response['statusCode'] = 201;
                $response['message'] = $item;
                return $response;
            }
        } else {
            $response['statusCode'] = 201;
            $response['message'] = 'You have not added any account details please add this before cashout.';
            return $response;
        }
    }

    public function isBidderRequested($data)
    {
        // Check if bidder login requested
        if (empty($data['app_id']) || (strtolower($data['app_id']) !== self::USER_TYPE_BIDDER)) {
            return false;
        }
        return true;
    }

	/**
	 * Create a bidder
	 * A hook-in method to allow creating a bidder, if requested.
	 * Populates user_sub_type column with appropriate bidder id from tbl_usertype 
	 * @param User $model
	 */
    public function createBidder($model, $data) 
    {   
        // Not a bidder login requested
        if (empty($data['app_id']) || (strtolower($data['app_id']) !== self::USER_TYPE_BIDDER)) {
            return $model;
        }
        // Register user as bidder
        // $user_sub_type = Usertype::getBidderType();
        $model->app_id = $data['app_id'];
        $model->save(false);
        return $model;
    }

    /**
     * Test method for performing debugging and stuff
     */
    public function actionTest()
    {   
    	$password_hash = 'c669ed67690092b33cf1533092f95709585c918d0e77afa06f25dc39dfa458f440435488a611fa42287b9311af2b13c8c741a1dbea75962c51127e6b4a023418';
    	$pass = Yii::$app->request->post('password');
    	$enc_pass = crypt($pass, $password_hash);
        
        $site_enc_pass = \Yii::$app->security->generatePasswordHash($pass);
    	return ['enc_pass' => $enc_pass, 'site_enc_pass' => $site_enc_pass];
        // return [\Yii::$app->language, 200];
        return [Yii::t('app','api_response_test_success'), 200];
    }
    
	/**
	 * To create new agent
	 *
	 * @param user_type=agent To sign-up as a agent.
	 *
	 * @api {post} /api/web/v1/users/create-agent Create a new Agent
	 * @apiName CreateUser
	 * @apiVersion 0.1.1
	 * @apiGroup Agent
	 * @apiDescription To create new agents, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} firstName User's firstname.
	 * @apiParam {String} lastName User's lastname.
	 * @apiParam {String} username User's username.
	 * @apiParam {String} email User's email.
	 * @apiParam {Number} agency_id Agency Id get from the Agency Table.
	 * @apiParam {Number} app_id (Optional) '2' to force user registration as agent and for access from CVIEW APP  
	 * @apiParam {Number} user_type (Optional) '10' to force user registration as agent   
	 *
	 * @apiSuccess {String} firstname Firstname of the User.
	 * @apiSuccess {String} lastname  Lastname of the User.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "message": "",
	 *       "data": {
	 *         "firstName": "John",
	 *         "lastName": "Doe",
	 *         "email": "john_doe@mail.com",
	 *         "username": "john_hoe",
	 *         "agency_id": "1",
	 *         "auth_key": "AHSGAWERQWSAFRGEQw2Q21-A2W4G_13S",
	 *         "fullName": "john doe",
	 *         "user_code": "XX-john_doe",
	 *         "id": 12,
	 *         "promise_uid": "3145ac4b4251fd43"
	 *       },
	 *       "status": 200
	 *     }
	 *
	 * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 422 Validation Error
	 *     {
	 *       "message": "",
	 *       "data": [
	 *         {
	 *           "field": "username",
	 *           "message": "The username \'john_doe\' has already been taken. The following usernames are available: \'john_doe, john_doe.600, john_doe_110, john.doe, johnddoe\'. "
	 *         },
	 *         {
	 *           "field": "email",
	 *           "message": "Email john_doe@mail.com has already been registered. Please enter a valid email address."
	 *         }
	 *       ],
	 *       "status": 422
	 *     }
	 *   Or
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [
	 *         {
	 *            "field": "firstName",
	 *            "message": "First Name cannot be blank."
	 *         },
	 *         {
	 *            "field": "lastName",
	 *            "message": "Last Name cannot be blank."
	 *         },
	 *         {
	 *            "field": "agency_id",
	 *            "message": "Agency Id cannot be blank."
	 *         },
	 *         {
	 *            "field": "dob",
	 *            "message": "Date Of Birth cannot be blank."
	 *         },
	 *         {
	 *            "field": "username",
	 *            "message": "Username in here!! cannot be blank."
	 *         },
	 *         {
	 *            "field": "email",
	 *            "message": "Email cannot be blank."
	 *         }
	 *       ],
	 *       "status": 422
	 *    }
	 * @return Object|User-model
	 */
    public function actionCreateAgent()
    {
        $data = Yii::$app->getRequest()->getBodyParams();
        $model= new Agent();
        $model->scenario = "create";
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        
        if (!$model->validate()) {
            return $model;
        }
        $profile=new Profiles();
        $profile->usertype_id=3;
        $profile->commercialStatus=3;

        foreach ($profile->attributes() as $key) {
            $profile->$key = "";
        }

        if (isset($data['profile'])) {
            $profile->load($data['profile'],'');
        }

        if (!empty($model->image!="")) {
            $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $model->image));
            $imageName=$model->username.rand(10,1000). ".jpg";
            $imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['profileUploadPath'].$imageName);
            file_put_contents($imagePath, $data);
            $model->image=$imageName;
        }

        if (!empty($data['mediacare_number'])) {
            $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['mediacare_number']));
            $fileAmalMedicareName = 'medicare_'.$model->username.'_'.rand(10,1000). ".jpg";
            $fileAmalMedicarePath = str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['amalUploadPath'].$fileAmalMedicareName);
            file_put_contents($fileAmalMedicarePath, $file);
            $profile->mediacare_number = $fileAmalMedicareName;
        }

        if ( !empty($data['recent_utility_bill']) && $data['recent_utility_bill'] != "") {
            $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data['recent_utility_bill']));
            $fileAmalRecentUtilityName = 'recent_utility_'.$model->username.'_'.rand(10,1000). ".jpg";
            $fileAmalMedicarePath = str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['amalUploadPath'].$fileAmalRecentUtilityName);
            file_put_contents($fileAmalMedicarePath, $file);
            $profile->recent_utility_bill = $fileAmalRecentUtilityName;
        }

        if (($model->save() === false ) && (!$model->hasErrors() )) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        } else {
            $profile->user_id=$model->id;
            $profile->foodEstablishment=!empty($data['foodEstablishment'])?$data['foodEstablishment']:0;
            $profile->save(false);
            $model->getImageUrl();
        }

        if (($model->dob!='') && (!strstr($model->dob,"-"))) {
            $model->dob=date("Y-m-d",$model->dob);
        }
        
            $this->createBidder($model, $data);
        return $model;
    }
    
	/** 
	* 
	* Save new property information
	* - URI: *api/web/v1/users/agent-list
	* - Data: required|x-www-form-urlencoded
	*
	* @api {post} api/web/v1/users/agent-list?agency_id=1 Show list of Agent
	*
	* @apiName ShowAgents
	* @apiVersion 0.1.1
	* @apiGroup Agent
	* @apiDescription To show agents, Form-Data must be x-www-form-urlencoded
	*
	* @apiSuccessExample Success-Response:
	*    HTTP/1.1 200 OK
	*    {
	*      "message": "",
	*      "data":  {
	*        "items": [
	*          { 
	*            "firstName": "John",
	*            "lastName": "doe",
	*            "email": "john@gmail.com",
	*            "fullName": "John Doe",
	*            "auth_key": "wBhrAI7uD-y1Ow5xrmZ5EyFdJJwE4IOe",
	*            "agency_id": "10",
	*            "logo": "Abercromby’s819.jpg"
	*          },
	*          {
	*            "firstName": "John",
	*            "lastName": "doe1",
	*            "email": "john1@gmail.com",
	*            "fullName": "john doe1",
	*            "auth_key": "41oBK-OHkauNIq5b8jyQlF4jcZPTsXT_",
	*            "agency_id": "10",
	*            "logo": "Abercromby’s819.jpg"
	*          },
	*        ],
	*        "_links": {
	*          "self": {
	*            "href": "http://localhost/AlphaWallet/html/api/web/v1/user/agent-list?agency_id=10&page=1"
	*          }
	*        },
	*        "_meta": {
    *          "totalCount": 3,
	*          "pageCount": 1,
	*          "currentPage": 1,
	*          "perPage": 20
	*        }
	*      },
	*      "status": 200
	*    }
	*/
    public function actionAgentList($agency_id){
		$query = new Query;
		$model = Agent::find()->select([Agent::tableName().'.id','firstName','lastName',Agent::tableName().'.email',Agent::tableName().'.phone','fullName','auth_key','agency_id','tbl_agencies.logo'])->joinWith('agency')->where(['user_type'=>10,'app_id'=>2])->andWhere(['agency_id'=>$agency_id]);
		
		$query->select(['firstName','lastName',Agent::tableName().'.email','fullName','auth_key','agency_id','tbl_agencies.*'])
            ->from(Agent::tableName())
            ->join("INNER JOIN", Agency::tableName(),sprintf('%s.id=%s.agency_id',Agency::tableName(),Agent::tableName()))
            ->where(['user_type'=>10,'app_id'=>2])->andWhere(['agency_id'=>$agency_id]);
		$result = new ActiveDataProvider([
                'query' => $model,
            ]);
		return $result;
	}
	
	/** 
	 * 
	 * Email an agent
	 * - URI: *api/web/v1/users/email-agent
	 * - Data: required|x-www-form-urlencoded
	 *
	 * @api {post} api/web/v1/users/email-agent Email an agent
	 *
	 * @apiName EmailAgent
	 * @apiVersion 0.1.1
	 * @apiGroup Agent
	 * @apiDescription To email an agent, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {Number} property_id Unique property id.
	 * @apiParam {String} enquiry_name Sender's name
	 * @apiParam {String} enquiry_email Sender's email
	 * @apiParam {String} enquiry_phone Sender's phone
	 * @apiParam {String} enquiry_message Sender's message
	 * @apiParam {String} enquiry_i_would_like_to[] I would like selections
	 * @apiParam {String} enquiry_i_would_like_to[] I would like to have fun
	 * @ apiParam {String} to (Optional) Email to (For testing) Send the email address to test the email.
	 * @apiParam {Number} property_id Unique property id.
	 * @apiParam {Number} contact_me 1=>Yes, 0=>No
	 * 
	 *  @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "message": "",
	 *       "data":  {
	 *		},
	 *       "status": 200
	 *     }
	 */
    public function actionEmailAgent()
    {	
    	$formData = Yii::$app->request->post();
    	if (isset($formData['property_id'])) {
    	
    		$property = Property::find($formData['property_id']);
    		$email = new EmailController;

			$contact_me = false;
			if (!empty($formData['enquiry']['contact_me']) && $formData['enquiry']['contact_me'] == '1') {
				$contact_me = true;
			}

			$property = Property::find()->where([Property::tableName().'.id'=>$formData['property_id']])->joinWith('agent')->one();
			if (!$property) {
				return ['Invalid property'];
			}

			$property_agent_email = !empty($property->agent)?$property->agent->email:'';
			$to_email = $property_agent_email;

			// Remove later
			$to_email = "gurpreet@graycelltech.com";

			$formData['property_title'] = $property->title;
			$formData['property_address'] = $property->address;
			$formData['contact_me'] = $contact_me;
			$formData['enquiry_i_would_like_to'] = json_decode($formData['enquiry_i_would_like_to'], true);

			$rs = $email->actionSend($to_email, isset($formData['subject'])?$formData['subject']:'Contact Agent', $formData);
			return $rs; 
		} else {
			return ['message'=>'Property id cannot be blank.','statusCode'=>422]; 	
		}
	}

	/* search agent */
	public function actionAgentSearch()
	{
		$searchModel = new Agent();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	}


}