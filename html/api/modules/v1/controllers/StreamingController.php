<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\ApplyBid;
use common\models\User;
use common\models\CviewConfig;
use api\components\Controller;

/**
 * StreamingController
 *
 */

class StreamingController extends Controller
{
    public $modelClass = 'api\modules\v1\models\CviewConfig';
    

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'except' => [],
            'only'=>[],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "update" actions
        unset($actions['delete'], $actions['index'], $actions['update'],$actions['create']);

        return $actions;
    }
	
    /**
     * To View Streaming server configurations
     *
     * @api {get} /api/web/v1/streaming/server-info View Streaming server configurations 
     * @apiName Streaming Server Configurations
     * @apiVersion 0.1.1
     * @apiGroup Streaming
     * @apiDescription To view streaming server configurations
     *
     * @apiSuccessExample Success-Response:
     *    HTTP/1.1 200 OK
     *    {
     *        "message": "",
     *        "data": {
     *            "host": "XXX.XXX.XXX.XXX",
     *            "port": "1935",
     *            "application": "my_app",
     *            "source": {
     *                "username": "john",
     *                "password": "some-cool-password"
     *            }
     *        },
     *        "status": 200
     *    }
     *
     * @apiErrorExample Error-Response:
     *    HTTP/1.1 401 Unauthorized
     *    {
     *        "name": "Unauthorized",
     *        "message": "Your request was made with invalid credentials.",
     *        "code": 0,
     *        "status": 401,
     *        "type": "yii\\web\\UnauthorizedHttpException"
     *    }
     * 
     * @return Array|Streaming Object Data
     */
	public function actionServerInfo()
    {   
        return Yii::$app->params['wowza'];
    }
        
}