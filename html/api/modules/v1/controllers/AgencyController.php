<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\User;
use api\components\Controller;
use api\modules\v1\models\Agency;
use api\modules\v1\models\AgencySearch;
use api\modules\v1\models\AgencyOffices;
use api\modules\v1\models\AgencyFavourite;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;
use api\modules\v1\models\Storelocator;
use common\models\CommonModel;
class AgencyController extends Controller
{
    public $modelClass = 'api\modules\v1\models\Agency';
    
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
    {
        $behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'except' => ['subfees'],
            'only'=>[
                'favourite-agency',
            ],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
    }
	   
    
  	public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		 unset($actions['create'], $actions['update'],$actions['index'],$actions['search'],$actions['searchkeyword'],$actions['delete'],$actions['view']);                    

		return $actions;
	}
	/**
	* To create Agency
	*
	* @api {post} /api/web/v1/agency/create Create Agency
	* @apiName CreateUser
	* @apiVersion 0.1.1
	* @apiGroup Agency
	* @apiDescription To create new agency, Form-Data must be x-www-form-urlencoded
	*
	* @apiParam {String} name Agencies's name.
	* @apiParam {String} address[0] Agencies first Address
	* @apiParam {Number} latitude[0] for address[0] 
	* @apiParam {Number} longitude[0] for address[0] 
	* @apiParam {String} address[0] Agencies second Address and so on
	* @apiParam {Number} latitude[0] for address[1] and so on
	* @apiParam {Number} longitude[0] for address[1] and so on
	* @apiParam {text} about_us About Agency
	* @apiParam {base64} logo Agencies logo(path frontend/web/agencylogo).  
	* @apiParam {Number} status (Optional) '10' to force agencies create 
	*
	* @apiSuccess {String} name Name of Agency.
	* @apiSuccess {json} address  Address of the Agency.
	*
	* @apiSuccessExample Success-Response:
	*    HTTP/1.1 200 OK
	*     {
	*       "message": "",
	*       "data": {
	*          "status": 10,
	*          "name": "Doe",
	*          "address": "[\"3314, Sector 19D, Sector 19, Chandigarh, 160019\",\"House No. 3031, Sector 19-D, Chandigarh, 160019\"]",
	*         "logo": "http://localhost/AlphaWallet/html/frontend/web/agencylogo/imagename.jpeg",
	*         "latitude": "[\"30.72811\",\"30.719059\"]",
	*         "longitude": "[\"76.77065\",\"76.748704\"]",
	*         "about_us": "Gross Waddell Pty Ltd is a\ncommercial real estate agency,\noperating for over 20 years in the\nretail, office, industrial and\ndevelopment property sectors. We\nprovide Sales and Leasing, Asset\nManagement\nand\nCorporate\nServices to Vendors, Landlords and\nInvestors.",
	*         "email": "john_doe@gmail.com",
	*         "mobile": "+911234569874",
	*         "updated_at": "1523960975",
	*         "created_at": "1523960975",
	*         "id": 12,
	*       },
	*       "status": 200
	*     }
	*
	* @apiError Validation Error Make sure to fix the listed issues to create 
	* a successful user account.
	*
	* @apiErrorExample Error-Response:
	*     HTTP/1.1 422 Validation Error
	*     {
	*       "message": "",
	*       "data": [
	*         {
	*           "field": "name",
	*           "message": "Name \"Doe\" has already been taken. "
	*         },
	*         {
	*           "field": "mobile",
	*           "message": "Mobile must be an integer."
	*         },
	*         {
	*           "field": "email",
	*           "message": "Email \"john_doe@gmail.com\" has already been taken."
	*         }
	*       ],
	*       "status": 422
	*     }
	* Or
	*    HTTP/1.1 422 Validation Error
	*    {
	*        "message": "",
	*        "data": [
	*        {
	*          "field": "name",
	*          "message": "Name cannot be blank."
	*        },
	*        {
	*          "field": "mobile",
	*          "message": "Mobile cannot be blank."
	*        },
	*        {
	*          "field": "logo",
	*          "message": "Logo cannot be blank."
	*        },
	*        {
	*          "field": "address",
	*          "message": "Address cannot be blank."
	*        },
	*        {
	*          "field": "latitude",
	*          "message": "Latitude cannot be blank."
	*        },
	*        {
	*          "field": "longitude",
	*          "message": "Longitude cannot be blank."
	*        },
	*  		 {
	* 			"field": "about_us",
	* 			"message": "About Us cannot be blank."
	* 		 },
	*        {
	*          "field": "email",
	*          "message": "Email cannot be blank."
	*        }
	*       ],
	*       "status": 422
	*    }
	* @return Object|User-model
	*/
  	public function actionCreate()
    {
		$model= new Agency();
		$model->load(Yii::$app->getRequest()->getBodyParams(), ''); 
		 if (!$model->validate()) {
            return $model;
        }
		 if (!empty($model->logo!="")) {
            $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $model->logo));
            $imageName=$model->name.rand(10,1000). ".jpg";
            $imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['agencylogoUploadPath'].$imageName);
            file_put_contents($imagePath, $data);
            $model->logo=$imageName;
        }
		if (($model->save() === false) && (!$model->hasErrors())) {
			throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }
       
        return $model; 
    }
    
    /**
     * Delete an Agency
     * Removed an agency by its Primary Key i.e. id
     */
    public function actionDelete($id)
    {
		if (\Yii::$app->getRequest()->getMethod() === 'DELETE') {
			$model= Agency::find()->where(['deal_id'=>$id])->one();
			if ($model) {
				$model->delete();
				Yii::$app->getResponse()->setStatusCode(204);
			} else {
				Yii::$app->getResponse()->setStatusCode(404);
			}
		}
    }
    
    public function actionFindall()
    {   
	   	$agencies = User::findAll(['status' => 1]);
	   	return $agencies;
	   	// return $agencies;
	   	// return new ActiveDataProvider([
     //        'query' => Agency::find()
     //    		->groupBy('id')        
     //   			->orderBy("id desc")
     //    ]);
    }
    
	/**
	 *
	 * List Agencies with sort by favourite functionality
	 * - URI: *api/web/v1/agency/list-agencies
	 * - Data: required|x-www-form-urlencoded
	 *
	 * @api {get} api/web/v1/agency/list-agencies?sort=favourite Shows list of Agencies with sorting by favourite (if the user is logged-in)
	 * @apiName List Agencies
	 * @apiVersion 0.1.1
	 * @apiGroup Agency
	 * @apiDescription To show agencies, Form-Data must be x-www-form-urlencoded
	 * ### Supported keys for sorting:
	 * - *favourite* | ascending=>'favourite', 'descending' => '-favourite'
	 * ### `Note: Append (-) for sorting in descending order` 
	 *
	 *   ### Examples for reference:
	 *   #### `/api/web/v1/agency/list-agencies?sort=-favourite`	
	 *
	 * @apiParam {String} sort (optional) favourites or -favourites
	 * @apiParam {String} favourites (optional) 1=> show only favourited list 0=> show all
	 *
	 *@apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 OK
	 *    {
	 *        "message": "",
	 * 	      "data": {
	 *		      "items": [
	 *                {
	 *                    "status": 10,
	 *					  "id": 10,
	 *					  "name": "Abercromby’s",
	 *					  "email": "aastha@gmail.com",
	 *					  "mobile": 1234569874,
	 *					  "logo": "http://localhost/AlphaWallet/html/frontend/web/agencylogo/Abercromby’s819.jpg",
	 *					  "backgroungImage": null,
	 *					  "address": "[\" sco 206 - 207, sector 34 a, chandigarh - 160022\",\"sco 72 a & 73 a sector 26 grain market chandigarh pin-160019\"]",
	 *					  "latitude": "[\"30.72589\",\"30.72267\"]",
	 *					  "longitude": "[\"76.75787\",\"76.79825\"]",
	 *					  "about_us": "Gross Waddell Pty Ltd is a\ncommercial real estate agency,\noperating for over 20 years in the\nretail, office, industrial and\ndevelopment property sectors. We\nprovide Sales and Leasing, Asset\nManagement\nand\nCorporate\nServices to Vendors, Landlords and\nInvestors.",
	 *					  "created_at": 1524034419,
	 *					  "updated_at": 1524034419,
	 *					  "is_favourite": "",
	 *					  "facebook": "https://www.facebook.com/",
	 *					  "twitter": "https://twitter.com/",
	 *					  "instagram": "https://www.instagram.com/"
	 *				  }
	 *			  ],
	 *			  "_links": {
	 *		          "self": {
	 *		          "href": "http://localhost/AlphaWallet/html/api/web/v1/agencies/list-agencies?sort=-favourite&page=1"
	 *		         }
	 *           },
	 *	         "_meta": {
	 *		         "totalCount": 6,
	 *	             "pageCount": 1,
	 *		         "currentPage": 1,
	 *		         "perPage": 20
	 *	         }
	 *    	  },	
	 *        "status": 200
	 *    }
	 */
	public function actionListAgencies(){
		//~ $agencies = Agency::find()->where(['status'=>10])->orderBy(['name'=>'SORT_DESC'])->all();
		//~ return $agencies;
		$agencysearchModel = new AgencySearch();
		 $searchByAttr['AgencySearch'] = Yii::$app->request->queryParams;
          return $agencysearchModel->search($searchByAttr);
	}
	
	/** 
	* 
	* Save new agency information
	* - URI: *api/web/v1/agency/create
	* - Data: required|x-www-form-urlencoded
	*
	* @api {get} api/web/v1/agency/view?id=14&lat=30.741482&lng=76.768066 Show list of offices addresses 
	* @apiName ShowAddressAgencies
	* @apiVersion 0.1.1
	* @apiGroup Agency
	* @apiDescription To show agencies addresses with address and disatnce from the current location, Form-Data must be x-www-form-urlencoded
	*
	*
	*@apiSuccessExample Success-Response:
	*     HTTP/1.1 200 OK
	*     {
	*       "message": "",
	*       "data": {
	*          "items": [
	*              {
	*                  "id": "1",
	*                  "name": "Abercromby’s14",
	*                  "email": "aastha4@gmail.com",
	*                  "address": " sco 206 - 207, sector 34 a, chandigarh - 160022",
	*                  "latitude": "30.725890000000000",
	*                  "longitude": "76.757870000000000",
	*                  "distance": "1.988860491131707"
	*             },
	*             {
	*                  "id": "2",
	*                  "name": "Abercromby’s14",
	*                  "email": "aastha4@gmail.com",
	*                  "address": "sco 72 a & 73 a sector 26 grain market chandigarh pin-160019",
	*                  "latitude": "30.722670000000000",
	*                  "longitude": "76.798250000000000",
	*                  "distance": "3.5635191814253315"
	*             }
	*           ],
	*            "_links": {
	*                "self": {
	*                      "href": "http://localhost/AlphaWallet/html/api/web/v1/agency/view?id=14&lat=30.741482&lng=76.768066&page=1"
	*               }
	*            },
	*        "_meta": {
	*           "totalCount": 2,
	*           "pageCount": 1,
	*           "currentPage": 1,
	*           "perPage": 20
	*          }
	*       },
	*       "status": 200
	*     }
	*/
	public function actionView($id,$lat,$long){
		$model=new Storelocator();
		$model->load(Yii::$app->request->getQueryParams(),'');
		//~ if($model->validate())    
		//~ {
			//$lat = $model->lat;
			//$long = $long;
			$agencyTable = Agency::tableName();
			$agencyoffice = AgencyOffices::tableName();
			$exp=sprintf('(6371 * acos (cos ( radians(%s) ) * cos( radians( %s.latitude ) ) * cos( radians( %s.longitude ) - radians(%s) ) + sin ( radians(%s) ) * sin( radians( %s.latitude ) ) )) as distance',$lat,$agencyoffice,$agencyoffice ,$long,$lat,$agencyoffice);
            $expression = new Expression($exp);
             $query = new Query;
            $query->select([$agencyoffice.'.id',$agencyoffice.'.name',$agencyoffice.'.email',$agencyoffice.'.mobile',$agencyoffice.'.address',$agencyoffice.'.latitude',$agencyoffice.'.longitude',$expression,$agencyTable.'.logo'])
            ->from(AgencyOffices::tableName())
            ->join("INNER JOIN", $agencyTable,sprintf('%s.id=%s.agency_id',$agencyTable,AgencyOffices::tableName()))
            ->where(['agency_id'=>$id])
            ->orderBy('distance');
				$result = new ActiveDataProvider([
					'query' => $query,
				]);
				return $result;
			
		//}
	}
	
	/** 
	* 
	* Save new agency information
	* - URI: *api/web/v1/agency/create
	* - Data: required|x-www-form-urlencoded
	*
	* @api {get} api/web/v1/agency/view-detail?id=1 View detail for office address
	* @apiName ShowAddressdetail
	* @apiVersion 0.1.1
	* @apiGroup Agency
	* @apiDescription To show agencies addresses with address and disatnce from the current location, Form-Data must be x-www-form-urlencoded
	*
	*
	*@apiSuccessExample Success-Response:
	*     HTTP/1.1 200 OK
	*     {
	*       "message": "",
	*       "data": {
	*            "name": "Abercromby’s14",
	*            "logo": "http://localhost/AlphaWallet/html/frontend/web/agencylogo/Abercromby’s14882.jpg",
	*            "email": "aastha4@gmail.com",
	*            "mobile": 1234569874,
	*            "address": " sco 206 - 207, sector 34 a, chandigarh - 160022",
	*            "latitude": "30.725890000000000",
	*            "longitude": "76.757870000000000"
	*        },
	*       "status": 200
	*     }
	*/
	public function actionViewDetail($id){
		$agencyoffice = AgencyOffices::find()->joinWith('agency')->where([AgencyOffices::tableName().'.id'=>$id])->one();
		$email = $agencyoffice->email;
		$mobile = $agencyoffice->mobile;
		$name = $agencyoffice->name;
		$logo = $agencyoffice->agency->logo;
		return array('name'=>$name,'logo'=>$logo,'email'=>$email,'mobile'=>$mobile,'address'=>$agencyoffice->address,'latitude'=>$agencyoffice->latitude,'longitude'=>$agencyoffice->longitude);
	}
	
	/** 
	 * 
	 * To View the Agency Profile
	 * - URI: *api/web/v1/agency/agency-profile?id=8
	 * - Data: required|x-www-form-urlencoded
	 *
	 * @api {get} api/web/v1/agency/agency-profile?id=8 View Agency profile
	 * @apiName ShowAgencydetail
	 * @apiVersion 0.1.1
	 * @apiGroup Agency
	 * @apiDescription To show agencies details, Form-Data must be x-www-form-urlencoded
	 *
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 OK
	 *    {
	 *        "message": "",
	 *	      "data": {
	 *			  "status": 10,
	 *			  "id": 8,
	 *			  "name": "Biggin & Scott",
	 *			  "email": null,
	 *			  "mobile": null,
	 *			  "logo": "http://localhost/AlphaWallet/html/frontend/web/agencylogo/Biggin & Scott389.jpg",
	 *			  "backgroungImage": null,
	 *			  "address": "[\"sco no2917 18,sector 22 c,chandigarh 160022\",\"SCO- 1473, 1473, 43B, Sector 43, Chandigarh, 160047, India\"]",
	 *			  "latitude": "[\"30.72811\",\"30.719059\"]",
	 *			  "longitude": "[\"76.77065\",\"76.748704\"]",
	 *			  "about_us": "Gross Waddell Pty Ltd is a\ncommercial real estate agency,\noperating for over 20 years in the\nretail, office, industrial and\ndevelopment property sectors. We\nprovide Sales and Leasing, Asset\nManagement\nand\nCorporate\nServices to Vendors, Landlords and\nInvestors.",
	 *			  "created_at": 1524027072,
	 *			  "updated_at": 1524027072,
	 *			  "is_favourite": 1,
	 *			  "facebook": "https://www.facebook.com/",
	 *			  "twitter": "https://twitter.com/",
	 *			  "instagram": "https://www.instagram.com/"
	 *        },
	 *	      "status": 200
	 *    }
	 */
	public function actionAgencyProfile($id){
		$model = Agency::FindOne(['id'=>$id]);
		return $model;
	}
    public function actionTest()
    {
    	return "Testing agency routes!!!";
    }
    
    /**
    * To Favourite/Un-favourite Agency
    * 
    * @ api {post} /api/web/v1/agency/favourite-agency?agency_id=8 Favourite/Un-favourite Agency
    * @ apiName Favourite/UnfavouriteAgency
    * @ apiVersion 0.1.1
    * @ apiGroup Auction
    * @ apiDescription To Favourite/Un-favourite Agency
    * 
    * @ apiParam {Number} user_id User's Id(login user id).
    * @ apiParam {Number} listing_id Listing Id (Property Id)
    * @ apiParam {Number} is_favourite (0:unfavourite and 1:favourite)
    * 
    * @apiSuccessExample Success-Response:
    *  HTTP/1.1 200 
    *   {
    *       "message": "",
    *        "data": {
    *            "id": 35,
    *              "user_id": "351",
    *              "agency_id_id": "2",
    *              "is_favourite": 0,
    *              "created_at": 1525929504,
    *              "created_by": 351,
    *              "updated_at": 1525930676,
    *              "updated_by": 351
    *         },
    *    "status": 200
    *    }
    */
    public function actionFavouriteAgency($agency_id){
		if(Yii::$app->user->id)
		if(!empty($agency_id)){
			$model = AgencyFavourite::findOne(['agency_id'=>$agency_id,'user_id'=>Yii::$app->user->id]);
            if(!empty($model)){
            $model->load(Yii::$app->request->getBodyParams(),'');
			$model->save(false);
            }else{
                $model = new AgencyFavourite();
                $model->load(Yii::$app->request->getBodyParams(),'');
                $model->user_id = Yii::$app->user->id;
                $model->agency_id = $agency_id;
                if($model->validate()){
                $model->save(false);
                }
            }
			return $model;
		}
	}
	
	public function actionCreateList(){
		$data = file_get_contents("php://input");
		$enc_data = json_decode($data, true);
		print_r($enc_data);die('her');
		if($enc_data){
			//$data = json_decode($postData['item'],true);
			$obj =  $enc_data['data'];
			$objectArray = isset($obj['items'])?$obj['items']:$obj;
			if(is_array($objectArray)){
				$model = new Agency();
				$model->setAttributes($objectArray);
				if($model->validate()){
                $model->save(false);
                }
				return $model;
			}
		}
	}
	
	public function actionOfficeCreate(){
		$model = new AgencyOffices();
		$model->load(Yii::$app->getRequest()->getBodyParams(), ''); 
		 if (!$model->validate()) {
            return $model;
        }else{
			$model->save();
			return $model;
		}
	}
	
	public function actionUpdateAgency(){
		$data = file_get_contents("php://input");
		$enc_data = json_decode($data,true);
		//print_r($enc_data);die;
		if($enc_data){
			//$data = json_decode($postData['item'],true);
			$obj = $enc_data['data'];
			if(is_array($obj)){
				$model = Agency::findOne($obj['id']);
				$model->setAttributes($obj);
				$changed_attributes = array_diff_assoc($obj, $model->oldAttributes); 
				 if (!$model->validate()) {
					return $model;
				}else{
					$model->save();
					return ['Agency has been updated successfully',$model];
				}
			}
		}
	}
 
}