<?php

namespace api\modules\v1\controllers;
use Yii;
use yii\db\Query;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use backend\models\Notificationsetting;
use common\models\User;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class NotificationsettingController extends ActiveController
{
	public $modelClass = 'backend\models\Notificationsetting';
	
	public function actions(){
		
		$actions = parent::actions();

		// disable the "create" and "update" actions
		
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);                     

		return $actions;
		
	}
	
	public function behaviors(){
		
		$behaviors = parent::behaviors();
				
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		
		return $behaviors;
		
	}
	
	/*
	 * Create and Update notification Setting for users
	 * 
	*/
	 
	public function actionCreate(){
		
		
		
		$user_id 			= Yii::$app->user->id;  	
		
		
		$notificationset 	= new Notificationsetting();
		
		$postedData 		= Yii::$app->request->post();
		
		$notificationset->load(Yii::$app->request->getBodyParams(),'');
			
		if(!empty($postedData) && $notificationset->validate()){
			
			
			$notificationMsg['notific_status'] = ($postedData['is_on']==1)?'Notification is ON':'Notification is OFF';
			
			$notificationData 	= $notificationset::find()
									->where(['user_id'=>$user_id])								
									->one();
		
			if($notificationData){
				
				$notificationData->user_id 				= $user_id;
				$notificationData->is_on 				= $postedData['is_on'];
				
				if(isset($postedData['is_on']) && $postedData['is_on']==1 && !empty($postedData['notification_allow'])){
					
					$notificationAllow = $postedData['notification_allow'];
					
					$notificationMsg['max_allow'] = 'Allowed notification  is '.$postedData['notification_allow'];
					
				}elseif($postedData['is_on']==1 && empty($postedData['notification_allow'])){
					
					$notificationAllow = 0;
					
				}elseif($postedData['is_on']==0){
					$notificationAllow = 0;
					
				}
				
				$notificationData->notification_allow 	= $notificationAllow;
				
				
			}else{
				
				$notificationData	=	new Notificationsetting();	
				
				$notificationData->user_id 				= $user_id;
				$notificationData->is_on 				= $postedData['is_on'];
				
				if(isset($postedData['is_on']) && $postedData['is_on']==1 && !empty($postedData['notification_allow'])){
					
					$notificationAllow = $postedData['notification_allow'];
					
					$notificationMsg['max_allow'] = 'Allowed notification  is '.$postedData['notification_allow'];
					
				}elseif($postedData['is_on']==1 && empty($postedData['notification_allow'])){
					
					$notificationAllow = 0;
					
				}elseif($postedData['is_on']==0){
					$notificationAllow = 0;
				}
				
				$notificationData->notification_allow = $notificationAllow;
				
			}	
			
			$notificationData->save();
			
		}
		
		return $notificationset;
		
	}
	
	
}