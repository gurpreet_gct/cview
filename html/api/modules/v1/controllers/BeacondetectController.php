<?php
namespace api\modules\v1\controllers;
 
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use common\models\BeaconDetectLog;
use common\models\Beaconmanager;
use common\models\Workorders;
use common\models\Pass;
use common\models\User;
use common\models\Workorderpopup;
use common\models\Deal;
use common\models\UserPsa;
use api\modules\v1\models\UserCategory;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;  
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use api\modules\v1\models\Foodordersapi;
use common\components\PushNotification;
use common\models\Product;
use common\models\Profile;
use common\models\FoodordersItem;
use common\models\FoodordersSubItem;
use common\models\Foodorders;
use common\models\FoodorderOptions;
use backend\models\Beacongroup;
use common\models\ProductOption;

class BeacondetectController extends ActiveController
{    
        public $modelClass = 'common\models\BeaconDetectLog';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
   
   public function behaviors()
   {
       $behaviors = parent::behaviors();
       $behaviors['authenticator'] = [
           'class' => CompositeAuth::className(),
           'authMethods' => [
               HttpBasicAuth::className(),
               HttpBearerAuth::className(),
               QueryParamAuth::className(),
           ],
       ];
       return $behaviors;
   }
      
    
  public function actions()
   {
       $actions = parent::actions();
 
       // disable the "delete" and "update" actions
       unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);                    
 
       return $actions;
   }
 
      
   public function actionCreate()
   {
		
		$user_id = Yii::$app->user->id;            
		$model=new BeaconDetectLog();
		$model->customerId=$user_id;   
		$model->load(Yii::$app->getRequest()->getBodyParams(), '');        
     
		  if (($model->save() === false ) && (!$model->hasErrors() )) {
			  
			   throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
		   }else{			   
			$rq=Yii::$app->getRequest()->getBodyParams() ;           
			$beaconDeviceID=0;
			$rq=Yii::$app->getRequest()->getBodyParams();
			$beaconDeviceID=isset($rq['beaconDeviceID'])?$rq['beaconDeviceID']:0;
			$resultcode = array();
			$rs= Beaconmanager::find()->select(['id','groups','locationOwnerID'])
				->where(['major' => $model->majorValue])
				//->orWhere(['manufacturer' => $beaconDeviceID])
				->andWhere(['minor' => $model->minorValue])             
				->one();    
   
            if($rs)        
            {			
			   /* for food ordering start  */
                $groups=explode(",",$rs->groups);
        		$query = Beacongroup::find()->select('type')->where('id IN('.$rs->groups.')')->all();
				$type =array();
				foreach($query as $querydata){
					$type[] = $querydata['type'];
				}
				
				$FoodorderTag = in_array(4,$type)?1:0;				
                $groups=implode("|",$groups);  
               
                $category_ids=implode(",",ArrayHelper::map( UserCategory::find()
					->select('category_id')
					->where(['user_id'=>$user_id,'status'=>1])
					->all(), 'category_id', 'category_id'));
                      
               $foodOrder = Foodordersapi::find()->select('id as orderId,user_id,store_id')->where(['user_id'=>$user_id,'status'=>1,'store_id'=>$rs->locationOwnerID])->asArray()->one();

                if(($foodOrder) && ($FoodorderTag==1))
				{
					$brand = Yii::$app->urlManagerBackEnd->createAbsoluteUrl('/profile/');
					$image = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/uploads/catalog/');
					$thumb = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/uploads/catalog/thumb');
					$storeId = $foodOrder['store_id']; 				
					$foodQuery = Foodorders::find()->select(UserPsa::tableName().'.status,'.Foodorders::tableName().'.id')
						->joinWith(['userpsa'=>function($q) use ($user_id) {
						$q->onCondition([UserPsa::tableName().'.user_id' => $user_id,UserPsa::tableName().'.type' => 2]);
						$q->orOnCondition(UserPsa::tableName().'.status=0');                                
						$q->orOnCondition(UserPsa::tableName().'.status is null');                                
						}],false,'LEFT JOIN')
						->where(Foodorders::tableName().'.user_id='.$user_id)
						->andwhere(Foodorders::tableName().'.status=1')
						->groupBy('id');   
						$FooddataProvider =                     
						new ActiveDataProvider([ 
						'query' => $foodQuery,
						'pagination' => [
						'pageSize' => 1,
						], 
					]); 
					if ($FooddataProvider->totalCount > 0) {                        

						$data = $FooddataProvider->getModels();
						$productval = $this->getProductChild($data[0]->id);	
						return $productval;

					}
			}
				/* start workoder deal */				
                 $query =  Deal::find()->select([UserPsa::tableName().'.user_id' ,Workorderpopup::tableName().'.*',Pass::tableName().'.isUsed']);
                                       
                 $query->joinWith('workorderpopup')
					->joinWith('pass')      
					->joinWith(['workordercategory'=> function ($q) use ($category_ids) {
                       if($category_ids=="")
                           $category_ids='0';
							$q->where('category_id in ( '.$category_ids.')');
                      }
                   ],false,'INNER JOIN') 
                   
                     ->joinWith(['userpsa'=>function($q) use ($user_id) {
                                  $q->onCondition(UserPsa::tableName().'.user_id= '. $user_id);
                                  $q->andOnCondition(UserPsa::tableName().'.status!=0');                                
                                  }],false,'LEFT JOIN')       
                   
                   ->joinWith(['workorders'=> function ($q)  {                        
                       $q->where(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))));                        
                     }
 
                   ],false,'INNER JOIN')
                    ->where(["REGEXP","beaconGroup",'[[:<:]]'.$groups.'[[:>:]]'])
                               ->andWhere([Workorders::tableName().'.status' =>1])
                               
                               ->andWhere("(CASE rulecondition WHEN 1 THEN IF(Loyalty1 & Loyalty2,(CASE Loyalty3     
										WHEN 1 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 day) AND user_id = $user_id ) WHEN 2 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 week) AND user_id = $user_id) WHEN 3 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 month) AND user_id = $user_id) WHEN 4 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 year) AND user_id = $user_id) END),false) ELSE rulecondition = 0 END)") 
       
                               ->andWhere("(CASE rulecondition WHEN 1 THEN IF(userenter,userenter = 1,IF(usereturned, usereturned = 1, usereturned = 0 )) ELSE rulecondition = 0 END)") 
                               
                               ->andWhere("(CASE dayparting WHEN 1 THEN dayparting = 1 WHEN 3 THEN IF(daypartingcustom3 & daypartingcustom2,(daypartingcustom3 <= '".date('H', time())."' and daypartingcustom3end >= '".date('H', time())."') || (daypartingcustom2 <= '".date('H', time())."' and daypartingcustom2end >= '".date('H', time())."') || daypartingcustom1 <= '".date('H', time())."' and daypartingcustom1end >= '".date('H', time())."',IF(daypartingcustom2 & daypartingcustom1,(daypartingcustom2 <= '".date('H', time())."' and daypartingcustom2end >= '".date('H', time())."') || daypartingcustom1 <= '".date('H', time())."' and daypartingcustom1end >= '".date('H', time())."',daypartingcustom1 <= '".date('H', time())."' and daypartingcustom1end >= '".date('H', time())."')) ELSE rulecondition = 0 END)") 
                               
                               ->andWhere("(CASE temperature WHEN 1 THEN temperature = 1 WHEN 2 THEN IF(temperaturestart & temperaturend,temperaturestart = 9 and temperaturend >= 15, true) ELSE rulecondition = 0 END)")
                                                            
                               ->andWhere("(CASE rulecondition WHEN 1 THEN IF(behaviourDwell & behaviourDwellMinutes, (CASE behaviourDwellMinutes WHEN 1 THEN (SELECT (max(unix_timestamp(detect_at))-min(unix_timestamp(detect_at)))/60 >= behaviourDwell FROM tbl_beacon_detect_logs) WHEN 2 THEN (SELECT (max(unix_timestamp(detect_at))-min(unix_timestamp(detect_at))) >= behaviourDwell FROM tbl_beacon_detect_logs)END), false) ELSE rulecondition = 0 END)") 
                               
                                ->andWhere("(CASE rulecondition WHEN 1 THEN IF(behaviourvisit1 & behaviourvisit2 & behaviourvisit3, (CASE behaviourvisit3 WHEN 1 THEN (SELECT count(*) >= behaviourvisit1 FROM `tbl_userPsa` WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 DAY ) AND user_id = $user_id AND status= 0 ) WHEN 2 THEN (SELECT count(*) >= behaviourvisit1 FROM `tbl_userPsa` WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 WEEK) AND user_id = $user_id AND status= 0) WHEN 3 THEN (SELECT count(*) >= behaviourvisit1 FROM `tbl_userPsa` WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 MONTH) AND user_id = $user_id AND status= 0) END) ,false) ELSE rulecondition = 0 END)")    
                               
                               ->groupBy('id')    
                               ->having(UserPsa::tableName().'.user_id is null')   
                                ->orderBy("id desc");          
                            
                  
              $dataProvider =                     
                 new ActiveDataProvider([ 
                   'query' => $query,
                    'pagination' => [
                                  'pageSize' => 1,
                                  ], 
              ]);
                       
                
                                   
                    if ($dataProvider->totalCount > 0) {                        
                      
                      $data = $dataProvider->getModels();
                     
                      
                     if(count($data[0]->workorder_id)>0){
                     $query=sprintf("insert into %s (workorder_id,user_id,created_at,status,type) values(%s,%s,%s,0,1) on duplicate key update status=0",
                      UserPsa::tableName(),$data[0]->workorder_id,Yii::$app->user->id,time());
                      Yii::$app->db->createCommand($query)->execute();       
                     }
                             return $dataProvider;
                                      
                   }else{                            
                       $resultcode['message'] = 'No beacon data available';                    
                       return $resultcode;
                   }
                   
            }else{
               
                   $resultcode['message'] = 'invalid data';
                   return $resultcode;
           }
            
        }
   
        return $model;
   }
   
public function getProductChild($id){
	   
$brand = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/profile/');
$brandImg = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/profile//');
$image = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/uploads/catalog/');
$thumb = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/uploads/catalog/thumb');
$po    = ProductOption::tableName();
$fdo   = FoodorderOptions::tableName();
 /* get main products order */
           $orderItem = sprintf("SELECT o.id as order_id,o.store_id,o.qty,o.grand_total,o.tax_amount,o.discount_amount,o.status,FROM_UNIXTIME(o.created_at) as created_at from %s o where o.id=%s;",Foodordersapi::tableName(),$id); 
             $order =Yii::$app->db->createCommand($orderItem)->queryAll();
             $orderData = array(); 
			 $itemsname2 = 0;			 
			 $subItemsname2 = 0;	
			 if($order) {		
				/* order detail */
			     $id = $order[0]['order_id'];
				  /* update psa table */
				   if($id){
				   $query=sprintf("insert into %s (workorder_id,user_id,created_at,status,type) values(%s,%s,%s,0,2) on duplicate key update status=0",
				   UserPsa::tableName(),$id,Yii::$app->user->id,time());
				   Yii::$app->db->createCommand($query)->execute();
				   $ordrStatus = Foodordersapi::findOne($id);
				   $ordrStatus->status = 3;
				   $ordrStatus->save(false);
				   
                 }
				 $orderData['workorderType']=101;
				 $orderData['notificationText']='Welcome. Swipe here to pay and send your order to the kitchen';
				 $orderData['order_id']=$id;
				 $orderData['store_id']=$order[0]['store_id'];
				 $orderData['qty']=$order[0]['qty'];
				 $orderData['tax_amount']=$order[0]['tax_amount'];
				 $orderData['grand_total']=$order[0]['grand_total'];
				 $orderData['discount']=$order[0]['discount_amount'];
				 $orderData['status']=$order[0]['status'];	
				 $orderData['logo']='';	
				 $orderData['brandDescription']='';	
				 $orderData['companyName']='';	
				 $orderData['backgroundHexColour']='';	
				 $orderData['qrurl'] 	= Yii::$app->urlManagerApi->createAbsoluteUrl("/v1/barcodes?id=".$id. "-".$order[0]['grand_total']);		
			     /* order item details */
				 $orderItemp = sprintf("SELECT fi.product_id,fi.pname,fi.qty,fi.grand_total,fi.item_total_price,fi.sub_item_total_price,fi.have_child,CONCAT('%s/',p.image) as image,CONCAT('%s/',p.image) as thumb,p.description,p.has_option,p.related_product,p.price,p.p_type,p.sku,b.companyName,CONCAT('%s/',b.brandLogo) as brandLogo,CONCAT('%s/',b.backgroundHexColour) as backgroundHexColour,b.brandDescription from %s fi INNER JOIN %s p on fi.product_id=p.id INNER JOIN  %s b on p.store_id=b.user_id where fi.order_id =%s;",$image,$thumb,$brand,$brandImg,FoodordersItem::tableName(),Product::tableName(),Profile::tableName(),$id);
				$product=Yii::$app->db->createCommand($orderItemp)->queryAll();	
			 	if($product){
				$productArr = array();		
				 foreach($product as $_product){
					   $orderData['logo'] = $_product['brandLogo'];
					   $orderData['backgroundHexColour'] = $_product['backgroundHexColour'];
					   $orderData['brandName'] = $_product['companyName'];
					   $orderData['brandDescription']=strip_tags($_product['brandDescription']);
					   $proItem = array();
					   $proItem['product_id'] = $_product['product_id'];
					   $proItem['name'] = $_product['pname'];
					   $proItem['qty'] = $_product['qty'];
					   $proItem['p_type'] = $_product['p_type'];
					   $proItem['is_added'] = 1;
					   $proItem['grand_total'] = $_product['grand_total'];
					   $proItem['item_total_price'] = $_product['item_total_price'];
					   $proItem['sub_item_total_price'] = $_product['sub_item_total_price'];
					   $proItem['have_child'] = $_product['have_child'];
					   $proItem['image'] = $_product['image'];
					   $proItem['thumb'] = $_product['thumb'];
					   $proItem['description'] = $_product['description'];
					   $proItem['price'] = $_product['price'];
					   $proItem['sku'] = $_product['sku'];
					   $proItem['brandName'] = $_product['companyName'];
					   $proItem['logo'] = $_product['brandLogo'];
					   /* show sub items */ 
					$childWrap = array();
                   if(isset($_product['related_product'])){
                       $sqlProduct2=sprintf("SELECT id ,p_type,name,sku,description,urlkey,price,status,is_online,image,is_added,qty,CONCAT('%s/',image) as image,CONCAT('%s/',image) as thumb FROM %s  where id IN(%s)",$image,$thumb,Product::tableName(),$_product['related_product']);
                       $childProduct=Yii::$app->db->createCommand($sqlProduct2)->queryAll();
                       foreach($childProduct as $_childProduct){
                           $added = FoodordersSubItem::find()->select('product_id')->where('product_id='.$_childProduct['id'].' AND order_id='.$id)->one();    
							$isAdde = !empty($added->product_id)?1:0;
							$child = array();
							$child['product_id']   	 = $_childProduct['id'];
							$child['name']           = $_childProduct['name'];
							$child['qty']         	 = $_childProduct['qty'];
							$child['sku']            = $_childProduct['sku'];
							$child['image']          = $_childProduct['image'];
							$child['thumb']          = $_childProduct['thumb'];
							$child['p_type']         = $_childProduct['p_type'];
							$child['description']    = $_childProduct['description'];
							$child['urlkey']         = $_childProduct['urlkey'];
							$child['price']          = $_childProduct['price'];
							$child['status']         = $_childProduct['status'];
							$child['is_added']       = $isAdde;							
							$child['brandName']      =  $_product['companyName'];
							$child['logo']         	 = $_product['brandLogo'];
                           $childWrap[] = $child;   
                           
                       }
                   
                   }	
					$proItem['child'] = $childWrap;
					$pOptions = array();
			if($_product['has_option']==1){
				$pOptions2 = ProductOption::find()
					->select(["$po.id","$po.product_id","$po.title","$po.price",new Expression('0 as is_added'),new Expression('0 as qty')])
					->where("$po.product_id=".$_product['product_id'])
					->orderBy('is_included DESC')
					->asArray()
					->all();	
				foreach($pOptions2 as $_pOptions){
						$added = FoodorderOptions::find()->select('product_id,qty')->where('product_id='.$_pOptions['id'].' AND order_id='.$id)->one(); 
						$isAdde = !empty($added->product_id)?1:0;
						$isAddeqty = !empty($added->qty)?$added->qty:0;
						unset($_pOptions['is_added']);
						unset($_pOptions['qty']);
						$_pOptions['is_added'] = $isAdde;
						$_pOptions['qty'] = $isAddeqty ;
						$pOptions[]	= 	$_pOptions;	
										
				} 
			} //end	if($product['has_option']==1)	
					$proItem['options']=$pOptions;
					$productArr[] = $proItem;
				 }	
				 $orderData['item'] = $productArr;
				}        
				
		   return array('items'=>array($orderData));
				
	}
}
    
    /* Update the status that beacon popup is viewed for PSA*/
    public function actionView($id=null,$type=null)
    {
       if($id)
       {
		  
           $model=UserPsa::find()
           ->where(["workorder_id"=>$id])
           ->andWhere(["user_id"=>Yii::$app->user->id])
           ->andWhere(["status"=>0,"type"=>$type])
           ->one();   			
           if($model)
           {
              $model->updated_at=time();
              $model->status=1;
               $model->save(false);
               return $model;
          }
          else{
          return ["statusCode"=>403,"message"=>"Invalid log"];
		}
       }
       
   }
   
   
 
   
   
   
}
