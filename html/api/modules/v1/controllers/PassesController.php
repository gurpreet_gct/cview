<?php
namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use common\models\Pass;
use common\models\Workorderpopup;
use common\models\Workorders;
use common\models\User;
use common\models\Profile;
use common\models\Qr;
use common\models\Offertrack;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class PassesController extends Controller
{    
        public $modelClass = 'common\models\Pass';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['delete'],$actions['index'],$actions['view']);         

		return $actions;
	}
	/* save new passes information */    
    public function actionCreate(){
		$user_id 		= Yii::$app->user->id;  	
		$model			= new Pass();
		$Workorders		= new Workorders();
		$workorderpopup	= new Workorderpopup();
		$created_at		=	time();
		
	if(isset(Yii::$app->request->post()['workorderid']) && Yii::$app->request->post()['workorderid']!='' ){
		
		
		
		// workorderpartner = advertiserID
		$Workorders = Workorders:: find()->select([
													Workorders::tableName().'.workorderpartner,name,dateTo,noPasses,usedPasses,type,allowpasstouser',
													Workorderpopup::tableName().'.offerTitle,offerText,offerwas,offernow,readTerms,sspMoreinfo,passUrl,logo,photo',
													Workorderpopup::tableName().'.buynow',
													Profile::tableName().'.brandName,brandLogo,brandAddress'
												])
										->leftJoin(
													Workorderpopup::tableName(), Workorderpopup::tableName().'.workorder_id = '.Workorders::tableName(). '.id'
												 )
										->leftJoin(
													Profile::tableName(), Workorders::tableName().'.workorderpartner = '.Profile::tableName().'.user_id'
												 )
										->where([
													Workorders::tableName().'.id' => Yii::$app->request->post()['workorderid']
												])
										->asArray()->one();
								
		
		/*************** passes count **************************/
		$pass_count = Pass:: find()->select([
												Pass::tableName().'.id,user_id,workorder_id,store_id,count_passes,isUsed,passesTrack'
											])
										->where([
													Pass::tableName().'.workorder_id' => Yii::$app->request->post()['workorderid'],
													Pass::tableName().'.user_id' => $user_id
												])
										->orderBy([Pass::tableName().'.id' => SORT_DESC])->asArray()->one();
			
						
		/************ end here *******************************************************************/
		
		
		if($pass_count)
		{
			
			if($Workorders['type']==1){
				
				if($pass_count['isUsed'] >= $Workorders['allowpasstouser']){
						return ["message"=>"You have already redeemed that pass.","statusCode"=>422];						
				}elseif($pass_count['isUsed']!=$pass_count['passesTrack']){
						return ["message"=>"You have already added this pass to your wallet. It can be found in My Passes in My Wallet.","statusCode"=>422];
				}else{					
						$model =  Pass::findOne(['workorder_id' => Yii::$app->request->post()['workorderid'],'user_id' => $user_id ]);					
						$model->passesTrack = $model->passesTrack + 1;					
						$forPlus = $model->passesTrack - $model->isUsed;
						if($forPlus==1){					
							$model->save(false);
						}					
					
					return $query = Pass:: find()
									->joinWith("workorders")
									->joinWith("workorderpopup")
									->joinWith("profile")
									->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_id' => Yii::$app->request->post()['workorderid'],Workorders::tableName().'.status' =>1])
									->andWhere(sprintf('dateTo >='.strtotime(date("Y-m-d"))))->one();
									// ->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))))->one(); 
					
				
				}
				
			}else if($Workorders['type']==2){
				
				// get advertiser id
				
				$passData =  Pass::find()->where(['workorder_id'=>Yii::$app->request->post()['workorderid'],'store_id' =>$Workorders['workorderpartner'] ,'user_id' => $user_id])->andWhere('isUsed >0 ')->orderBy('id desc')->one();	
				
				if($Workorders['workorderpartner']==$pass_count['store_id'] && !$passData){
					
					return ["message"=>"You have already added a digital stamp card to your wallet. It can be found in My Stamp Cards in My Wallet.","statusCode"=>422];
					
				}else{
					
					
					
					if($passData){
						
						$model->passesTrack = $model->passesTrack + 1;
					
						$forPlus 	= $model->passesTrack - $model->isUsed;
						$usedtotal 	= Pass::find()
										->where(['workorder_id' => Yii::$app->request->post()['workorderid'],'user_id' => $user_id,'isUsed'=>0])->count();
										
						if($forPlus==1 && $usedtotal==0){
						
							$model->user_id	 = 		$user_id;				
							$model->workorder_id = 	Yii::$app->request->post()['workorderid'];
							$model->store_id = 		$Workorders['workorderpartner'];
							$model->offerTitle = 	$Workorders['offerTitle'];
							$model->offerText = 	$Workorders['offerText'];
							$model->offerwas = 		$Workorders['offerwas'];
							$model->offernow = 		$Workorders['offernow'];
							$model->readTerms = 	$Workorders['readTerms'];
							$model->sspMoreinfo = 	$Workorders['sspMoreinfo'];
							$model->count_passes = 	$model->passesTrack;
							$model->save(false);	
							
							$usedPasses = $Workorders['usedPasses'] + 1;
							Yii::$app->db->createCommand()->update(Workorders::tableName(), 
											['usedPasses' => $usedPasses],  
											['id' => Yii::$app->request->post()['workorderid']])
											->execute();
											
						
							if($model->id){
								
								return $query = Pass:: find()
												->joinWith("workorders")
												->joinWith("workorderpopup")
												->joinWith("profile")
												->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.id' => $model->id,Workorders::tableName().'.status' =>1])
												->andWhere(sprintf('dateTo >='.strtotime(date("Y-m-d"))))->one();
												// ->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))))->one();
							}else{
								 
								return ["message"=>"No data saved.","statusCode"=>422];
							 
							}
						
						}else{
							
							return ["message"=>"You have already added this digital stamp card to your wallet. It can be found in My Stamp Cards in My Wallet.","statusCode"=>422];
						}
						
					}else{
						
						return ["message"=>"You have already added this digital stamp card to your wallet. It can be found in My Stamp Cards in My Wallet.","statusCode"=>422];
						
					}
					
				}
			}else{
					
					 if($pass_count['isUsed'] >= $Workorders['allowpasstouser']){
						 return ["message"=>"You have already redeemed that event ticket.","statusCode"=>422];
						
					  }elseif($pass_count['isUsed']!=$pass_count['passesTrack']){
							return ["message"=>"You have already added this pass to your wallet. It can be found in My Passes in My Wallet.","statusCode"=>422];
					}else{
						$model =  Pass::findOne(['workorder_id' => Yii::$app->request->post()['workorderid'],'user_id' => $user_id ]);
					
					$model->passesTrack = $model->passesTrack + 1;
					
					$forPlus = $model->passesTrack - $model->isUsed;
					if($forPlus==1){
					
						$model->save(false);
					}
						return $query = Pass:: find()
									->joinWith("workorders")
									->joinWith("workorderpopup")
									->joinWith("profile")
									->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_id' => Yii::$app->request->post()['workorderid'],Workorders::tableName().'.status' =>1])
									->andWhere(sprintf('dateTo >='. strtotime(date("Y-m-d"))))->one();
									// ->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))))->one();
						
						}
					}
		}elseif(!$pass_count && $Workorders['type']==2){
			
			$storePassData =  Pass::find()->where(['workorder_id'=>Yii::$app->request->post()['workorderid'],'store_id' =>$Workorders['workorderpartner'],'user_id' => $user_id])->andWhere('isUsed = 0 ')->orderBy('id desc')->one();
			
			// var_dump($storePassData);
			// die;
			
			if($storePassData){
				
				return ["message"=>"You have already added a digital stamp card to your wallet. It can be found in My Stamp Cards in My Wallet.","statusCode"=>422];
				
			}else{
				
				$model->user_id	 = 		$user_id;				
				$model->workorder_id = 	Yii::$app->request->post()['workorderid'];
				$model->store_id = 		$Workorders['workorderpartner'];
				// $model->store_id = 		$Workorders['advertiserID'];
				$model->offerTitle = 	$Workorders['offerTitle'];
				$model->offerText = 	$Workorders['offerText'];
				$model->offerwas = 		$Workorders['offerwas'];
				$model->offernow = 		$Workorders['offernow'];
				$model->readTerms = 	$Workorders['readTerms'];
				$model->sspMoreinfo = 	$Workorders['sspMoreinfo'];
				$model->passesTrack   = 1; 
				$model->count_passes = 	1;
				$model->save();	
					
				$usedPasses = $Workorders['usedPasses'] + 1;
				
				Yii::$app->db->createCommand()->update(Workorders::tableName(), 
									['usedPasses' => $usedPasses],  
									['id' => Yii::$app->request->post()['workorderid']])
									->execute();
									
				// echo "<pre>";print_r($model);die('asdasd');	
				if($model->id){
				 	return $query = Pass:: find()
									->joinWith("workorders")
									->joinWith("workorderpopup")
									->joinWith("profile")
									->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.id' => $model->id,Workorders::tableName().'.status' =>1])
									->andWhere(sprintf('dateTo >='.strtotime(date("Y-m-d"))))->one();
									// ->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))))->one();
				}else{
					 
					return 'data not saved';
				 
				}
				
			}
		}
		else{
			
			
				if($Workorders['type']==4 || $Workorders['type']==2){
					$allowpasstouser = 0;
				}else{
					
					if($Workorders['noPasses'] > $Workorders['usedPasses']){
						$allowpasstouserLeft =  $Workorders['noPasses'] - $Workorders['usedPasses'];
						if($allowpasstouserLeft > $Workorders['allowpasstouser']) {
							$allowpasstouser = $Workorders['allowpasstouser'];
							}else{
								 $allowpasstouser = $allowpasstouserLeft;
							}
						}
					
				}
					
				$model->user_id	 = 		$user_id;				
				$model->workorder_id = 	Yii::$app->request->post()['workorderid'];
				$model->store_id = 		$Workorders['workorderpartner'];
				// $model->store_id = 		$Workorders['advertiserID'];
				$model->offerTitle = 	$Workorders['offerTitle'];
				$model->offerText = 	$Workorders['offerText'];
				$model->offerwas = 		$Workorders['offerwas'];
				$model->offernow = 		$Workorders['offernow'];
				$model->readTerms = 	$Workorders['readTerms'];
				$model->sspMoreinfo = 	$Workorders['sspMoreinfo'];
				$model->passesTrack   = 1; 
				$model->count_passes = 	1;
				$model->save();	
					
				$usedPasses = $Workorders['usedPasses'] + 1;
				Yii::$app->db->createCommand()->update(Workorders::tableName(), 
									['usedPasses' => $usedPasses],  
									['id' => Yii::$app->request->post()['workorderid']])
									->execute();
									
				// echo "<pre>";print_r($model);die('asdasd');	
				if($model->id){
				 	return $query = Pass:: find()
									->joinWith("workorders")
									->joinWith("workorderpopup")
									->joinWith("profile")
									->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.id' => $model->id,Workorders::tableName().'.status' =>1])
									->andWhere(sprintf('dateTo >='.strtotime(date("Y-m-d"))))->one();
									// ->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))))->one();
				}else{
					 
					return 'data not saved';
				 
				}
				
			
		}
		
		
	}else{
		return ["message"=>"workorderid cannot be blank."];
		}
  }

    /* To get all passes api*/
	public function actionIndex(){
		
		$user_id = Yii::$app->user->id;  	
		$pass_count= new Pass();
		$Workorders= new Workorders();
		$Workorderpopup= new Workorderpopup();
		return $dataProvider = new ActiveDataProvider([
				'query' => Pass:: find()
							->joinWith("workorders")
							->joinWith("workorderpopup")
							->joinWith("profile")
							->where([Pass::tableName().'.user_id' => $user_id,Workorders::tableName().'.workorderType' => 1,Workorders::tableName().'.status' =>1])							
							
							// #->andWhere(Pass::tableName().'.isUsed <'.Workorders::tableName().'.allowpasstouser')	
							->andWhere(Pass::tableName().'.isUsed !='.Pass::tableName().'.passesTrack' )
							
							->andWhere(Workorders::tableName().'.type!=2')					

							->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d")))),
				]);
				
            
            
       /* $pass_count =  Pass:: find()
						->joinWith("workorders")
						->joinWith("workorderpopup")
						->joinWith("profile")
						->where([Pass::tableName().'.user_id' => $user_id,Workorders::tableName().'.workorderType' => 1,Workorders::tableName().'.status' =>1])
						->andWhere(['or',[Workorders::tableName().'.type' => 4],[Workorders::tableName().'.type' => 1]])
						->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))))
						->asArray()->orderBy('id desc')->all();		
						
			
			$arr_result = array();
			$result = array();
			foreach($pass_count as $key=>$modelinfo){				
			$result['id'] = $modelinfo['id'];
			$result['user_id'] = $modelinfo['user_id'];
			$result['workorderType'] = $modelinfo['workorders']['workorderType'];
			$result['type'] = $modelinfo['workorders']['type'];
			$result['store_id'] = $modelinfo['store_id'];
			$result['workorder_id'] = $modelinfo['workorder_id'];
			$result['offerTitle'] = $modelinfo['offerTitle'];
			$result['offerText'] = $modelinfo['offerText'];
			$result['offerwas'] = $modelinfo['offerwas'];
			$result['offernow'] = $modelinfo['offernow'];
			$result['readTerms'] = $modelinfo['readTerms'];
			$result['sspMoreinfo'] = $modelinfo['sspMoreinfo'];
			$result['created_at'] = $modelinfo['created_at'];
			$result['isUsed'] = $modelinfo['isUsed'];
			$result['count_passes'] = $modelinfo['count_passes'];
			$result['workordername'] = $modelinfo['workorders']['name'];
			$result['expire date'] = date("Y-m-d",$modelinfo['workorders']['dateTo']);
			
			$result['passUrl'] = Yii::$app->urlManagerPassKit->createAbsoluteUrl($modelinfo['workorderpopup']['passUrl']);	
				$result['brandName'] = $modelinfo['profile']['brandName'];
				$result['brandAddress'] = $modelinfo['profile']['brandAddress'];
				$result['brandLogo'] = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$modelinfo['profile']['brandLogo']);
				$result['logo'] = Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/workorder/".$modelinfo['workorderpopup']['logo']);	
				$result['photo'] = Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/workorder/".$modelinfo['workorderpopup']['photo']);
				$result['buynow'] = $modelinfo['workorderpopup']['buynow'];
			 if(($modelinfo['isUsed'] < $modelinfo['count_passes']) && ($result['type'] ==1)){
				 $arr_pass[] = $result;
			 }elseif(($result['count_passes'] == 0) && ($result['type'] ==4)){
				 $arr_pass[] = $result;
			 }
			
			if(($modelinfo['isUsed'] > 0) && ($result['type'] ==1)){
				$arr_redeem[] = $result;
			}elseif(($modelinfo['count_passes'] > 0) && ($result['type'] ==4)){
				$arr_redeem[] = $result;
			}
			}
			if(isset($arr_pass)){
				
				$arr_result['passes'] = $arr_pass;
			}else{
				$arr_result['passes'] =array();
				}
			if(isset($arr_redeem)){
				$arr_result['reedem'] = $arr_redeem;
			}else{
				$arr_result['reedem'] =array();
				}
			
			return $arr_result; */
		
	
	}
	
	
	
	public function actionRedeem(){
		
		$user_id = Yii::$app->user->id;  	
		$pass_count= new Pass();
		$Workorders= new Workorders();
		$Workorderpopup= new Workorderpopup();
		
		return $dataProvider = new ActiveDataProvider([
								'query' => Pass:: find()
											->joinWith("workorders")
											->joinWith("workorderpopup")
											->joinWith("profile")
											->where([Pass::tableName().'.user_id' => $user_id,Workorders::tableName().'.workorderType' => 1,Workorders::tableName().'.status' =>1,Workorders::tableName().'.type' => 1])
											#->andWhere(['or',[Workorders::tableName().'.type' => 4],[Workorders::tableName().'.type' => 1]])
											->andWhere(Pass::tableName().'.isUsed > 0')
											->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")',(date("Y-m-d")))),
								]);
            
	
	}
	
	/********************* end here*************************/
   /*********** get view particular passes and digital stamp and event ticket***********/
	public function actionView($id){
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$workorderpopup= new Workorderpopup();
	return  Pass:: find()
				->joinWith("workorders")
				->joinWith("workorderpopup")
				->joinWith("profile")
				->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_id' => $id])
				->one();
	
	}
	
	/************ end here*****************************/
	/************* list digital stamp api******************/
	
	public function actionDigitalstampcard(){
		
		$user_id 		= Yii::$app->user->id; 
		$pass_count		= new Pass();
		$Workorders		= new Workorders();
		$Workorderpopup	= new Workorderpopup();
		
		//check total cards are stamped or not
	
		return $dataProvider = new ActiveDataProvider([
				'query' => Pass:: find()
							->joinWith("workorders")
							->joinWith("workorderpopup")
							->joinWith("profile")
							->where([Pass::tableName().'.user_id' => $user_id,Workorders::tableName().'.workorderType' => 1,Workorders::tableName().'.type' => 2,Workorders::tableName().'.status' =>1])
							->andWhere(Pass::tableName().'.isUsed !='.Pass::tableName().'.passesTrack' )
							->andWhere(Workorders::tableName().'.dateTo >='.strtotime(date("Y-m-d")))
							// ->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d")))),
						]);
							
	}
	
	/********** end here *******************************/
	
	/************* list redeem digital stamp api ******************/
	
	public function actionReedeemedstampcard(){
		
		$user_id 		= Yii::$app->user->id; 
		$pass_count		= new Pass();
		$Workorders		= new Workorders();
		$Workorderpopup	= new Workorderpopup();
		
		//check total cards are stamped or not
	
		return $dataProvider = new ActiveDataProvider([
				'query' => Pass:: find()
							->joinWith("workorders")
							->joinWith("workorderpopup")
							->joinWith("profile")
							->where([Pass::tableName().'.user_id' => $user_id,Workorders::tableName().'.workorderType' => 1,Workorders::tableName().'.type' => 2,Workorders::tableName().'.status' =>1])
							->andWhere([Pass::tableName().'.isUsed'=>1])
							->andWhere(Workorders::tableName().'.dateTo >='.strtotime(date("Y-m-d")))
							// ->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d")))),
						]);
							
	}
	
	/********** end here *******************************/
	
	
	/************* list event ticket******************/
	public function actionEventticket(){
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$Workorderpopup= new Workorderpopup();
	return $dataProvider = new ActiveDataProvider([ 
					 'query' =>	Pass:: find()
						->joinWith("workorders")
						->joinWith("workorderpopup")
						->joinWith("profile")
						->where([Pass::tableName().'.user_id' => $user_id,Workorders::tableName().'.workorderType' => 1,Workorders::tableName().'.type' => 4,Workorders::tableName().'.status' =>1])
						->andWhere("isUsed >0")
						
						->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d")))),
					]);
	}
	/********** end here *******************************/
	
	
	
}
