<?php

namespace api\modules\v1\controllers;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\components\Controller;
use api\modules\v1\models\Foodordersapi;
use common\models\Qr;
use dosamigos\qrcode\QrCode;
use yii;
//use api\components\Controller;
/**
 * Qr Controller API
 *

 */
class QrController extends Controller
{
    public $modelClass = 'common\models\Qr';

   /*
public function behaviors()
{
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
        'class' => CompositeAuth::className(),
        'authMethods' => [
            HttpBasicAuth::className(),
            HttpBearerAuth::className(),
            QueryParamAuth::className(),
        ],
    ];
    return $behaviors;
}*/

    public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['search'],$actions['delete'],$actions['view']);

		return $actions;
	}

    public function actionView($id=null)
    {
		$type=Yii::$app->request->getBodyParam('type')!=""?Yii::$app->request->getBodyParam('type'):0;
		 // $type
		if($id)
		{
			$model=Qr::find()
			->where(["qrkey"=>$id])
			->one();

			QrCode::png($model->data);
		}
	}

	 public function actionUpdate($id=null)
    {
		 return QrCode::base64('ghgh',"base64Image");
		$data=Yii::$app->getRequest()->getBodyParams()['data'];
		if($id==0)
		{

			return QrCode::base64($data,"base64Image");


		}
		else
		{
			//return Yii::$app->barcode->drawBars($data,"base64Image");
				return Yii::$app->urlManagerApi->createAbsoluteUrl(["v1/barcodes","id"=>$data]);

		}
	}
	 public function actionOrder(){
		$orderId = Yii::$app->getRequest()->post('orderId');
		if(!empty($orderId)){
           /* get main products order */
           $orderItem = sprintf("SELECT id,o.store_id,o.user_id,o.sub_item_total_price,o.item_total_price,o.qty,o.grand_total,o.tax_amount,o.discount_amount from %s o where o.id =%s;",Foodordersapi::tableName(),$orderId);
           $orderp =Yii::$app->db->createCommand($orderItem)->queryAll();
           $wrapAll = array();
               foreach($orderp as $_orderp){
                   $pitem = array();
                   $pitem['userID']                 =    $_orderp['user_id'];
                   $pitem['order_id']               =    $_orderp['id'];
                   $pitem['qty']                    =    $_orderp['qty'];
                   $pitem['grand_total']            =    $_orderp['grand_total'];
                   $wrapAll= $pitem;

               } //foreach
				if($wrapAll)
				{
					$dataQr = http_build_query($wrapAll);
					$qrkey = md5("swp@r".time()."$#%");
					$filename = \Yii::getAlias('@backend').'/web/qrcodes/'.$qrkey.'.png';
					$data = QrCode::png($dataQr,$filename);
					return $filename;
				}

       } // if id

	 }

}
