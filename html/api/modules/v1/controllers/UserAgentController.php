<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\LoginLogs;
use common\models\User;
use common\models\Usertype;
use common\models\ChangePassword;
use common\models\Foodnotification;
use common\models\Property;
use api\modules\v1\models\Customer;
use api\modules\v1\models\Bidder;
use api\modules\v1\models\Agent;
use api\modules\v1\models\Profiles;
use api\modules\v1\models\ForgotPassword;
use api\modules\v1\models\FacebookLogin;
use api\modules\v1\models\LinkedinLogin;
use api\modules\v1\models\GoogleLogin;
use api\modules\v1\models\Login;
use api\modules\v1\models\UserAgentLogin;
use api\components\Controller;
use api\modules\v1\models\Foodordersapi;
use api\modules\v1\models\Agency;
use api\modules\v1\controllers\EmailController;
use common\models\CommonModel;

/**
 * UserAgentController
 *
 * Login|Registration and other user management methods for APIs are defined here.
 */

class UserAgentController extends Controller
{
    const USER_TYPE_BIDDER = '2';
    const CVIEW_APP_ID = '2';

    public $modelClass = 'api\modules\v1\models\Agent';
    //public $modelClass = 'backend\models\Usermanagement';

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'except' => ['subfees'],
            'only'=>[
                'update',
                'view',
                'changepassword',
                'referral',
                'referral-history',
                'referral-cashout',
                'delete-agent'
            ],
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "update" actions
        unset($actions['delete'], $actions['index'], $actions['update'],$actions['create']);

        return $actions;
    }

	/**
	 * Custom Login api to allow Agent login into the system.
	 *  
	 * - URI: *api/web/v1/user-agent/login
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - username (string)|required 
	 *   - password (string)|required
	 *   - device   (string)|'android' or 'iphone'
	 *
	 * @api {post} api/web/v1/user-agent/login Agent Login
	 * @apiName Agent Login
	 * @apiVersion 0.1.1
	 * @apiGroup Agent
	 * @apiDescription To login an agent, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} username User's username.
	 * @apiParam {String} password User's password.
	 * @apiParam {String} device   'ios|android|web'.
	 * @apiParam {String} device_token   Random String token 'assss2mlkamladsasd289'.
	 * @apiParam {Number} app_id '2' to force user login as bidder
	 *
	 * @apiSuccess {String} firstname Firstname of the User.
	 * @apiSuccess {String} lastname  Lastname of the User.
	 *
	 * @apiSuccessExample Success-Response:
	 *	HTTP/1.1 200 OK
     *  {
     *      "message": "",
     *      "data": {
     *          "login": {
     *              "status": 10,
     *              "user_type": 10,
     *              "app_id": "2",
     *              "id": 449,
     *              "username": "john.doe.agent",
     *              "email": "john.doe.agent@graycelltech.com",
     *              "auth_key": "dld8JhMCAcFY2blg-IdlUxPLeoDqn82Z",
     *              "firstName": "john",
     *              "lastName": "doe.agent",
     *              "fullName": "john doe.agent",
     *              "orgName": null,
     *              "fbidentifier": null,
     *              "linkedin_identifier": null,
     *              "google_identifier": null,
     *              "role_id": null,
     *              "password_hash": "$2y$13$qkqQ63QkWshQaCHiFgp0L.oNm7MvaluzmcFgDVLVM/6CS7euDYDDy",
     *              "password_reset_token": null,
     *              "phone": null,
     *              "image": null,
     *              "sex": null,
     *              "dob": "1993-05-24",
     *              "ageGroup_id": 1,
     *              "bdayOffer": 0,
     *              "walletPinCode": null,
     *              "loyaltyPin": null,
     *              "advertister_id": null,
     *              "activationKey": null,
     *              "confirmationKey": null,
     *              "access_token": null,
     *              "hashKey": null,
     *              "device": "android",
     *              "device_token": "",
     *              "lastLogin": 1527766400,
     *              "latitude": null,
     *              "longitude": null,
     *              "timezone": null,
     *              "timezone_offset": null,
     *              "created_at": 1527765671,
     *              "updated_at": 1527766400,
     *              "storeid": null,
     *              "promise_uid": "4495b0fdaaf46a28",
     *              "promise_acid": null,
     *              "user_code": "AW-john.doe.agent",
     *              "referralCode": null,
     *              "referral_percentage": null,
     *              "agency_id": null,
     *              "logo": ""
     *          },
     *          "streaming_server_info": {
     *              "host": "XXXX.XXXX.XXXX.XXXX",
     *              "port": "XXXX",
     *              "application": "app_name",
     *              "source": {
     *                  "username": "username",
     *                  "password": "password"
     *              }
     *          }
     *      },
     *      "status": 200
     * }
     * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 * Or
	 *	HTTP/1.1 422 Unprocessable entity
	 *	{
	 *		"message": "",
	 *		"data": [
	 *			{
	 *				"field": "password",
	 *				"message": "That  Pin does not match the registered email address. Please try again or click on Forgot  Pin? to reset it."
	 *			}
	 *		],
	 *		"status": 422
	 *	}
	 *
	 *	@return Object(api\modules\v1\models\Customer)
	 */
    public function actionLogin()
    {
        $model = new UserAgentLogin();
        $model->isMobile=true;
        $data=array('UserAgentLogin'=>Yii::$app->request->post());
        if ($model->load($data) && $model->login()) {

            $userModel = Agent::findOne(Yii::$app->user->identity->id);

            isset($data['UserAgentLogin']['latitude'])?$userModel->latitude=$data['UserAgentLogin']['latitude']:NULL;
            isset($data['UserAgentLogin']['longitude'])?$userModel->longitude=$data['UserAgentLogin']['longitude']:NULL;
            $userModel->device_token=isset($data['UserAgentLogin']['device_token'])?$data['UserAgentLogin']['device_token']:'';

            // check if user is registerd on promise pay
            $payUserId = $userModel->promise_uid;
            if ($payUserId=='') {
                $result=Yii::$app->promisePay->createUser($userModel);
                if(isset($result->id)){
                    $userModel->promise_uid=$result->id;
                }
            }
            // assign referral code
            if ($userModel->user_code=='') {
                $userModel->user_code = Yii::t('app', 'AW-').$userModel->username;
            }
            $userModel->save(false);
            if (!strstr($userModel->dob,"-")) {
                $userModel->dob=date("Y-m-d",$userModel->dob);
            }
            $userModel = $this->postLoginCheck($userModel, $data);
            if (!$userModel) {
                $model->validate(null,false);
                $userModel = $model;
                return [['notice' => 'Invalid login credentials', 'user_data' => $userModel],'statusCode'=>422];  
            }

        } else {
            $model->validate(null,false);
            $userModel = $model;
        }
        return ['login' => $userModel, 'streaming_server_info' => Yii::$app->MyConstant->StreamingServerInfo()];

    }

	/**
	* Logout API for Agents
	*  
	* - URI: *api/web/v1/user-agent/logout
	* - Data: required|x-www-form-urlencoded
	* 
	* @api {post} api/web/v1/user-agent/logout Logout
	* @apiName Logout
	* @apiVersion 0.1.1
	* @apiGroup Agent
	* @apiDescription To logout an agent, Form-Data must be x-www-form-urlencoded
	*
	* @apiParam {String} access-token (required) The access-token of the logged-in agent
	*  //If valid access-token
	* @apiSuccessExample Success-Response:  
	*    HTTP/1.1 200 OK
	*    {
	*        "message": "",
	*        "data": "Logout Successfully",
	*        "status": 200
	*    }
	*    
	* @apiErrorExample Error-Response:
	*    HTTP/1.1 422 Unprocessable entity
    *    {
    *        "message": "",
    *        "data": [
    *            "Please provide a validate user token"
    *        ],
    *        "status": 422
    *    }
	*
	* @return Object(api\modules\v1\models\Agent)
	*/
    public function actionLogout()
    {
        $model=new Agent();
        if((Yii::$app->request->post()) && ($model=$model->findIdentityByAccessToken(Yii::$app->request->post()['access-token']))) {
            try{
                $loginlog=LoginLogs::findOne(['sessionid'=>$model->auth_key]);
                if($loginlog){
                    $loginlog->logout_at=time();
                    $loginlog->save();
                }
            }
            catch(Exception $e){

            }
            $model->auth_key="";
            $model->device_token="";
            $model->save();
            return "Logout Successfully";
        } else {
            return ["Please provide a valid user token",'statusCode'=>422];
        }
    }

	/**
	 * Agent Forgot Password
	 *  
	 * - URI: *api/web/v1/user-agent/forgot
	 * - Data: required|x-www-form-urlencoded
	 * - DataParams:
	 *   - email (string)|required 
	 *
	 * @api {post} api/web/v1/user-agent/forgot Agent Forgot Password
	 * @apiName Agent Forgot Password
	 * @apiVersion 0.1.1
	 * @apiGroup Agent
	 * @apiDescription To recover forgotten password of a user-agent, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {String} email UserAgent's email.
	 * @apiParam {Number} app_id '2' for CVIEW
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *     }
	 *
	 * @apiError Validation Error Make sure to fix the listed issues to create 
	 * a successful user account.
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [
	 *            {
	 *            }
	 *        ],
	 *        "status": 422
	 *    }
	 * Or
	 *    HTTP/1.1 422 Validation Error
	 *    {
	 *        "message": "",
	 *        "data": [
	 *       ],
	 *       "status": 422
	 *    }
	 *
	 * @return Object(api\modules\v1\models\Agent)
	 */
    public function actionForgot()
    {
        $model= new ForgotPassword();
        $model->scenario = ForgotPassword::SCENARIO_CVIEW_USER;
        $model->load(Yii::$app->request->getBodyParams(),'');
        if ($model->validate()) {
            $model = $model->getUserObject();
            
            if ($model->user_type != '10') {
                $response['statusCode'] = 201;
                $response['message'] = 'Invalid Agent email requested!';
                return $response;
            }

            
            if(!empty($model)){
            $validtoken=$model->generatePasswordResetToken();
            $model->save(false);
            $pasMessage ='password';
            
            if (isset($user->user_type) && $user->user_type==10) {
                $pasMessage ='Pin';
            }
            if ($model->isPasswordResetTokenValid($validtoken)) {
                $subject="AlphaWallet:Forgot Password";
                $sitepath = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl(['user/reset', 'token' => $validtoken]);
                if (isset($_REQUEST['app_id']) && $_REQUEST['app_id'] == self::USER_TYPE_BIDDER) {
                	$sitepath .= "&app_id=". $_REQUEST['app_id'];
                }
                $message = sprintf("Dear %s,<br/>Please follow the below mentioned url to reset your $pasMessage.<br/><a href='%s?token=%s'>Click Here to Reset %s</a>",$model->fullName,$sitepath,$model->password_reset_token,$pasMessage);
                $emailData  = [
                    'Name'=> $model->fullName,
                    'validtoken'=> $validtoken,
                    'pasMessage'=>$pasMessage
                ];
                $emailsend =Yii::$app->mail->compose(['html' => 'passwordReset-mail-api'],$emailData)
                    ->setTo($model->email)
                    ->setFrom(Yii::$app->params["noReplyEmail"],'No Reply')
                    ->setSubject($subject)
                    //->setHtmlBody($message)
                    ->send();
                $response = array();
               // print_r(array('cuul', 'is'));
               // exit();
                if ($emailsend) {
                    $response['statusCode'] = 200;
                    $response['message'] = "We have sent an reset $pasMessage  link to your email. Please follow the steps.";
                    return $response;
                } else {
                    $response['statusCode'] = 201;
                    $response['message'] = 'Email server is not working, Please try later.';
                    return $response;
                }
            } else {
                $response['statusCode'] = 201;
                $response['message'] = 'Email server is not working, Please try later.';
                return $response;
            }
		}
        }else{
			return $model;
		}

    }

    /**
     * Post login check for user type
     * Checks for the user type post login authentication.
     * @return User OBJ True|If login requested belonged to the adequate CVIEW app
     */
    public function postLoginCheck($authenticatedUser, $dataPassed)
    {   
        // Return the default authenticated user, when @param type not sent in POST.
        // $user_sub_type = Usertype::getBidderType();
        // Assuming normal customer login requested
        if (empty($dataPassed['UserAgentLogin']['app_id']) || (strtolower($dataPassed['UserAgentLogin']['app_id']) != self::CVIEW_APP_ID)) {
            // Check for the authenticated customer as non bidder, block login if user_type not bidder
            if ($authenticatedUser->app_id == self::CVIEW_APP_ID) {
                return false;
            }

            return $authenticatedUser;
        }

        // Check for the authenticated customer as bidder, allow login only if true
        if ($authenticatedUser->app_id !== self::CVIEW_APP_ID) {
            return false;
        }

        return $authenticatedUser;
    }

    /**
     * Test method for performing debugging and stuff
     */
    public function actionTest()
    {   
        \Yii::$app->language = 'en-US';
        // return [\Yii::$app->language, 200];
        return [Yii::t('app','api_response_test_success'), 200];
    }
    
}
