<?php
namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use common\models\Pass;
use common\models\Workorderpopup;
use common\models\Workorders;
use common\models\User;
use common\models\Profile;
use common\models\Qr;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class DigitalstampcardController extends Controller
{    
        public $modelClass = 'common\models\Pass';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
  	public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['delete'],$actions['index'],$actions['view']);         

		return $actions;
	}

  	
   /* save new Id information */    
    public function actionCreate(){
	$user_id = Yii::$app->user->id;
	$model= new Pass();
	$Workorders= new Workorders();
	$workorderpopup= new Workorderpopup();
	$pass_count= new Pass();
	$created_at=time();
	if(isset(Yii::$app->request->post()['workorderid']) && Yii::$app->request->post()['workorderid']!='' ){
		$Workorders= Workorders:: find()->select(
								[Workorders::tableName().'.*',Workorderpopup::tableName().'.*',Profile::tableName().'.*'])
								->leftJoin(Workorderpopup::tableName(), Workorderpopup::tableName().'.workorder_id = '.Workorders::tableName(). '.id')
								->leftJoin(Profile::tableName(), Workorders::tableName().'.advertiserID = '.Profile::tableName().'.user_id')
								->where([
										Workorders::tableName().'.id' => Yii::$app->request->post()['workorderid']])
								->orderBy([Workorders::tableName().'.id' => SORT_DESC])->asArray()->one();
			
		if($Workorders['noPasses'] > 0) {
	/*************** passes count **************************/
			$pass_count = Pass:: find()->select(
								[Pass::tableName().'.id,user_id,workorder_id,count_passes','store_id'])
							    ->where([
										Pass::tableName().'.workorder_id' => Yii::$app->request->post()['workorderid'],
										Pass::tableName().'.user_id' => $user_id
										])
								->orderBy([Pass::tableName().'.id' => SORT_DESC])->asArray()->one();
						
	/************ end here ******************************/
		if($pass_count)
		{
			$noPasses = $Workorders['noPasses'] - 1;
			Yii::$app->db->createCommand()->update(Workorders::tableName(), 
									['noPasses' => $noPasses], 
									['id' => Yii::$app->request->post()['workorderid'] ])
									->execute();	
			$countNewPass = $pass_count['count_passes'] + 1;
			$workorderType = $Workorders['workorderType'];
			$type = $Workorders['type'];
			Yii::$app->db->createCommand()->update(Pass::tableName(), 
												['count_passes' => $countNewPass,
												'workorder_type' => $workorderType,
												 'type' => $type], 
												['workorder_id' => Yii::$app->request->post()['workorderid'], 'user_id' => $user_id])
												->execute();
												
				$arr_result = array();	
				$arr_result['id'] = $pass_count['id'];
				$arr_result['user_id'] = $pass_count['user_id'];
				$arr_result['store_id'] = $pass_count['store_id'];
				$arr_result['workordername'] = $Workorders['name'];
				$arr_result['expire date'] = $Workorders['dateTo'];
				$arr_result['offerTitle'] = $Workorders['offerTitle'];
				$arr_result['offerText'] = $Workorders['offerText'];
				$arr_result['offerwas'] = $Workorders['offerwas'];
				$arr_result['offernow'] = $Workorders['offernow'];
				$arr_result['readTerms'] = $Workorders['readTerms'];
				$arr_result['sspMoreinfo'] = $Workorders['sspMoreinfo'];
				$arr_result['stampscardMoreinfo'] = $Workorders['stampscardMoreinfo'];
				$arr_result['passUrl'] = Yii::$app->urlManagerPassKit->createAbsoluteUrl($Workorders['passUrl']);	
				$arr_result['brandName'] = $Workorders['brandName'];
				$arr_result['brandLogo'] = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$Workorders['brandLogo']);	
				$arr_result['logo'] = Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/workorder/".$Workorders['logo']);	
				$arr_result['photo'] = Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/workorder/".$Workorders['photo']);							
				return ["message"=>"add successfully","statusCode"=>200,"items" =>$arr_result];$created_at=time();
		}else{
				$model->user_id = 	$user_id;		
				$model->store_id = 	$Workorders['advertiserID'];		
				$model->workorder_id = 	Yii::$app->request->post()['workorderid'];		
				$model->workorder_type = 	$Workorders['workorderType'];
				$model->type = 	$Workorders['type'];
				$model->count_passes = 	1;		
				$model->save();		
				$noPasses = $Workorders['noPasses'] - 1;
				Yii::$app->db->createCommand()->update(Workorders::tableName(), 
									['noPasses' => $noPasses], 
									['id' => Yii::$app->request->post()['workorderid']])
									->execute();
				$arr_result = array();	
				$arr_result['id'] = $model->id;
				$arr_result['user_id'] = $user_id;
				$arr_result['store_id'] = $Workorders['advertiserID'];
				$arr_result['workordername'] = $Workorders['name'];
				$arr_result['expire date'] = $Workorders['dateTo'];
				$arr_result['offerTitle'] = $Workorders['offerTitle'];
				$arr_result['offerText'] = $Workorders['offerText'];
				$arr_result['offerwas'] = $Workorders['offerwas'];
				$arr_result['offernow'] = $Workorders['offernow'];
				$arr_result['readTerms'] = $Workorders['readTerms'];
				$arr_result['sspMoreinfo'] = $Workorders['sspMoreinfo'];
				$arr_result['stampscardMoreinfo'] = $Workorders['stampscardMoreinfo'];
				$arr_result['passUrl'] = Yii::$app->urlManagerPassKit->createAbsoluteUrl($Workorders['passUrl']);	
				$arr_result['brandName'] = $Workorders['brandName'];
				$arr_result['brandLogo'] = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$Workorders['brandLogo']);
				$arr_result['logo'] = Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/workorder/".$Workorders['logo']);	
				$arr_result['photo'] = Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/workorder/".$Workorders['photo']);	
											
			return ["message"=>"add successfully","statusCode"=>200,"items" =>$arr_result];$created_at=time();
		}
	 }else{
		  return ["message"=>"passes are empty","statusCode"=>200];$created_at=time();
		 }
	}else{
		return ["message"=>"workorderid cannot be blank.","statusCode"=>422];$created_at=time();
		}
  }

      	  
    /* To get all digital stamp card */
   public function actionIndex(){
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$Workorderpopup= new Workorderpopup();
	$pass_count = Pass:: find()
						->joinWith("workorders")
						->joinWith("workorderpopup")
						->joinWith("profile")
						->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_type' => 1,Pass::tableName().'.type' => 2])
						->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d"))))
						->all();
	if($pass_count){
	return $pass_count;		
	}else{
		return ["message"=>"not found digital stamp card","statusCode"=>200];$created_at=time();	
		}
	
    }
   
	public function actionView($id){
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$workorderpopup= new Workorderpopup();
	$pass_count = Pass:: find()
							->joinWith("workorders")
							->joinWith("workorderpopup")
							->joinWith("profile")
							->where([Pass::tableName().'.user_id' => $user_id,Pass::tableName().'.workorder_id' => $id])
							->one();
	return $pass_count;
	}
	
	
}
