<?php
namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
// use common\models\Email;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class EmailController
{
	/** 
     * Send email wrapper
     */    
    public function actionSend($to, $subject, $messageSet, $from=null){
		// $sitepath = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl(['user/reset', 'token' => $validtoken]);

    	$emailsend = Yii::$app->mail->compose(['html' => 'contact-agent'],$messageSet)
                    ->setTo($to)
                    ->setFrom(Yii::$app->params["noReplyEmail"],'No Reply')
                    ->setSubject("Property Enquiry")
                    //->setHtmlBody($messageSet)
                    ->send();
        $response = array();
        
        if ($emailsend) {
            $response['statusCode'] = 200;
            $response['message'] = "Message sent to the agent.";
            return $response;
        }
        
        $response['statusCode'] = 201;
        $response['message'] = 'Email server is not working, Please try later.';
        return $response;

    }
	
}
