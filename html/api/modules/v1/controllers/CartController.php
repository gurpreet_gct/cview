<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use api\modules\v1\models\Cart;
use api\modules\v1\models\Customer;
use api\modules\v1\models\Foodordersapi;
use common\models\PaymentAccount;
use common\models\PaymentMethod;
use common\models\Transaction;
use common\models\UserLoyalty;
use common\models\User;
use common\models\Profile;
use common\models\Order;
use common\models\Store;
use common\models\Pass;
use common\models\OrderItems;
use common\models\Workorders;
use common\models\Loyaltypoint;
use common\models\Loyalty;
use common\models\OrderSubItems;
use common\models\OrderLoyalty;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\FoodordersItem;
use common\models\FoodordersSubItem;
use common\models\Paymentcard;
use common\models\PartnerTransactionsFees;

class CartController extends ActiveController
{
        public $modelClass = 'api\modules\v1\models\cart';

        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

   public function behaviors()
   {
       $behaviors = parent::behaviors();
       $behaviors['authenticator'] = [
           'class' => CompositeAuth::className(),
           'authMethods' => [
               HttpBasicAuth::className(),
               HttpBearerAuth::className(),
               QueryParamAuth::className(),
           ],
       ];
       return $behaviors;
   }


    public function actions()
   {
       $actions = parent::actions();

       // disable the "delete" and "update" actions
       unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);

       return $actions;
   }


      public function actionCreate(){

        $data=Yii::$app->getRequest()->getBodyParams();
        $cart=json_decode($data['cart']);
        $model=new Cart();
        $foodOrder = $data['foodorder'];
		$model->foodorder=$foodOrder;
        $model->data=$cart;
       if($model->validate())
        {

           Yii::$app->db->createCommand()->batchInsert(Cart::tableName(), $model->attributes(), $model->rows)->execute();
           $braintree = Yii::$app->braintree;
           $response = $braintree->call('ClientToken', 'generate' ,[]);

           return [
           "message"=> "",
           'session_key'=>$model->session_key,
           'clientToken'=>$response,
           'loyalty'=>$model->loyalty,
           "statusCode"=> 200
           ];
        }
       return $model;

   }
    /* create item in promise pay */
    public function actionCreateItem(){
		$data=Yii::$app->getRequest()->getBodyParams();
        $cart=json_decode($data['cart']);
        $model=new Cart();
        $foodOrder = $data['foodorder'];
		$model->foodorder=$foodOrder;
        $model->data=$cart;
       
       if($model->validate())
        {
			
			$workOrderID = '';
			foreach($model->data as $item){
				$workOrderID = array_map(function($e) {
					return is_object($e) ? $e->workOrderID : $e['workOrderID'];
				}, $item);
			}

			if(count($workOrderID)){
				$workOrderID = 'WorkorderId# '.implode('-',$workOrderID);
			}else{
				foreach($model->data as $item){
					$workOrderID = array_map(function($e) {
						return is_object($e) ? $e->productID : $e['productID'];
					}, $item);
				}
				$workOrderID = 'foodOrder#'.implode('-',$workOrderID);
			}
			
			$promiseUid = Yii::$app->user->identity->promise_uid;
			$totalAmt = round($model->payable,2);
			$itemId = $model->session_key;
			//$model->expirationDate=$model->expirationMonth.'/'.$model->expirationYear;
			Yii::$app->db->createCommand()->batchInsert(Cart::tableName(), $model->attributes(), $model->rows)->execute();
			
			
			if($totalAmt>0){
				// registered Item on promise pay
				$item=Yii::$app->promisePay->createItem($itemId,$totalAmt,$promiseUid,$workOrderID);
				if($item['id']!=''){
					$itemId = $item['id'];
					
			   }	
			}
			
			return [
				"message"=> "",
				'session_key'=>$itemId, 
				'loyalty'=>$model->loyalty,
				'clientToken'=>'',
				"statusCode"=> 200
			];
			
        }
       return $model;
	}
    /* make payment through promise pay */
    public function actionPayNow(){
		$data=Yii::$app->getRequest()->getBodyParams();
		$cart=json_decode($data['cart']);
		$model=new Cart();
		$foodOrder = $data['foodorder'];
		$foodorderID = $data['foodorderID'];
		$checktype = 0;
		$tempcard=$data['tempcard'];
		if(isset($data['foodorderID']) && $data['foodorderID']!=0){
			$checktype =1;
		}

		$model->foodorder=$foodOrder;
		$model->foodorder_id=$foodorderID;
		$model->data=$cart;
		$model->useloyalty=isset($data['useloyalty'])?$data['useloyalty']:0;
		$totalSpend	= 'no';
		/* validate the data/cart */
        if($model->validate())
        {
			$userId = Yii::$app->user->identity->id;
			$promiseUid = Yii::$app->user->identity->promise_uid;

			// check if order is eligiable for payment
			$model->payable=round($model->payable,2);
			if($model->payable >0)
			{
				// cardid =id of promise pay of credit card
				$CardId = $data['creditCardId'];
				
				// make payment , $data['nonce'] == item id
				$payment= Yii::$app->promisePay->makePayment($data['nonce'],$CardId);
				if(!is_array($payment)){
					return [
							   "message"=> $payment,
							   'cart'=>$data['cart'],
							   'session_key'=>$model->session_key,
							   "statusCode"=> 404
							   ];
				}

			} else {
				$payment['id'] = 'paid-via-loyalty'.substr($model->session_key,0,25);
			}

			/* Save the order */
			$order =new Order();
			$order->user_id=$userId;
			$order->order_type=$checktype;
			$order->discount_amount=$model->discount;
			$order->grand_total=$model->total;
			$order->subtotal=$model->total - $model->discount;
			$order->total_paid=round($model->payable,2);
			$order->qty_ordered=$model->qty;
			$order->status=2;
			$order->food_oid=$foodorderID;
			$order->created_at=time();
			$order->transaction_id = $payment['id'];
			$order->save(false);
			if($CardId){
				$paymentCard = Paymentcard::find()->where(['uniqueNumberIdentifier'=>$CardId])->one();
				if($paymentCard){
				$payment_method=new PaymentMethod();
                     //  $payment_methodColumn=$payment_method->attributes();
                     //  array_shift($payment_methodColumn);
               //    $payment_method->load($response->paymentMethod->attributes());
                  $payment_method->user_id=$paymentCard->user_id;
                  $payment_method->order_id=$order->id;
                  $payment_method->bin=$paymentCard->bin;
                   $payment_method->last4=$paymentCard->last4;
                   $payment_method->cardType=$paymentCard->cardType;
                   $payment_method->expirationMonth=$paymentCard->expirationMonth;
                   $payment_method->expirationYear=$paymentCard->expirationYear;
                   $payment_method->cardholderName=$paymentCard->cardholderName;
                   $payment_method->customerLocation=$paymentCard->customerLocation;
                   $payment_method->imageUrl=$paymentCard->imageUrl;
                   $payment_method->prepaid=$paymentCard->prepaid;
                   $payment_method->healthcare=$paymentCard->healthcare;
                   $payment_method->debit=$paymentCard->debit;
                   $payment_method->durbinRegulated=$paymentCard->durbinRegulated;
                   $payment_method->commercial=$paymentCard->commercial;
                   $payment_method->payroll=$paymentCard->payroll;
                   $payment_method->issuingBank=$paymentCard->issuingBank;
                   $payment_method->countryOfIssuance=$paymentCard->countryOfIssuance;
               //    $payment_method->productId=$response->paymentMethod->productId;
                   $payment_method->token=$paymentCard->systemToken;
                   $payment_method->uniqueNumberIdentifier=$paymentCard->uniqueNumberIdentifier;
                   $payment_method->maskedNumber=$paymentCard->maskedNumber;
                   $payment_method->venmoSdk=$paymentCard->venmoSdk;
                   $payment_method->expirationDate=$paymentCard->expirationDate;
                   $payment_method->save(false);
			   }
			}
			/* save order loyalt */
			$this->saveLoyalty($model,$order);
			/* save food order  */
			if($checktype==1){
				$this->saveFoodOrder($order);
			}else{
				/* save non food order  */
				$this->saveWorkOrder($model,$order);
			}
			 /*Send confirmation email */
		    $subject="Alpha Order Confirmation";
			$message=sprintf("Dear %s,<br/> <br/>Thanks for your purchase. You should receive your email confirmation soon.Your order details are as follows: <br/><br/>Order ID: %s<br />Transaction ID: %s<br /><br/>If you have further questions or enquiries please <u>CONTACT US</u>. In the meantime, sit back and enjoy your day.<br /><br/>Thanks<br /><br/>The Alpha Team",Yii::$app->user->identity->fullName,$order->id,$payment['id']);
	  		$emailsend =Yii::$app->mail->compose()
				->setTo(Yii::$app->user->identity->email)
				->setFrom(Yii::$app->params["salesEmail"],'No Reply')
				->setSubject($subject)
				->setHtmlBody($message)
				->send();
			 /* empty the temp. cart */
			   if($data['nonce']){
                   $tmodel=Cart::deleteAll('session_key = :key ', [':key' =>$data['nonce']]);
			   }
			// update work order
			if(count($model->dealsID)) {
					foreach($model->dealsID as $dealsIDInfo){
					$pass_count_new =Pass::find()
					->select([
					Pass::tableName().'.id,user_id,workorder_id,store_id,isUsed,count_passes'
					,Workorders::tableName().'.id,type,advertiserID,name,dateTo,noPasses,usedPasses,type,allowpasstouser'])
					->leftJoin(Workorders::tableName(), Pass::tableName().'.workorder_id = '.Workorders::tableName(). '.id')
					->where(["user_id"=>Yii::$app->user->id,"workorder_id"=>$dealsIDInfo['workorder_id']])
					->asArray()
					->all();

					/************** update code on workorder table **************/
					if($pass_count_new){
						if($pass_count_new[0]['isUsed']==0){

							$count_passes_usedPasses =  $dealsIDInfo['count_passes'] - 1;
							$usedPasses = $pass_count_new[0]['usedPasses'] + $count_passes_usedPasses;
							$count_passes =$dealsIDInfo['count_passes'];
						}else{

							$usedPasses = $pass_count_new[0]['usedPasses'] + $dealsIDInfo['count_passes'];
							$count_passes =$pass_count_new[0]['count_passes'] + $dealsIDInfo['count_passes'];
						}

						if($pass_count_new[0]['type']==1 || $pass_count_new[0]['type']==2){
							Yii::$app->db->createCommand()->update(Workorders::tableName(),
							['usedPasses' => $usedPasses],
							['id' => $pass_count_new[0]['workorder_id']])
							->execute();
							/**************** end here **********************************/
							/************** update code on pass table **************/
							$condition = 'workorder_id="'.$pass_count_new[0]['workorder_id'].'" AND user_id="'.Yii::$app->user->id.'"';

							$isused = $pass_count_new[0]['isUsed'] +  1;
							//$count_check = $pass_count_new[0]['check_count'] + 1;
							Yii::$app->db->createCommand()->update(Pass::tableName(),
							['isUsed' => $isused,'count_passes' => $count_passes], $condition)
							->execute();
						}
					/**************** end here **********************************/
					} else {
						$Passes=new Pass();
						$Passes->workorder_id = $dealsIDInfo['workorder_id'];
						$Passes->user_id = Yii::$app->user->id;
						$Passes->store_id = $model['data'][0][0]->storeID;
						$Passes->isUsed = 1;
						$Passes->count_passes = 1;
						$Passes->passesTrack = 1;
						$Passes->save(false);

					}
				}

			}
			// update event ticket

			$ticketinfo = '';
			if(count($model->eventTickets)){
				$ticketinfo = true;
					foreach($model->eventTickets as $eventTicketsInfo){
					$pass_count_new =Pass::find()
					->select([
					Pass::tableName().'.id,user_id,workorder_id,store_id,isUsed,count_passes'
					,Workorders::tableName().'.id,type,advertiserID,name,dateTo,noPasses,usedPasses,type,allowpasstouser'])
					->leftJoin(Workorders::tableName(), Pass::tableName().'.workorder_id = '.Workorders::tableName(). '.id')
					->where(["user_id"=>Yii::$app->user->id,"workorder_id"=>$eventTicketsInfo['workorder_id']])
					->asArray()
					->all();

					if($pass_count_new[0]['isUsed']==0){

						$count_passes_usedPasses =  $eventTicketsInfo['count_passes'] - 1;
						$usedPasses = $pass_count_new[0]['usedPasses'] + $count_passes_usedPasses;
						$count_passes =$eventTicketsInfo['count_passes'];
					}else{

						$usedPasses = $pass_count_new[0]['usedPasses'] + $eventTicketsInfo['count_passes'];
						$count_passes =$pass_count_new[0]['count_passes'] + $eventTicketsInfo['count_passes'];
					}
						Yii::$app->db->createCommand()->update(Workorders::tableName(),
						['usedPasses' => $usedPasses],
						['id' => $pass_count_new[0]['workorder_id']])
						->execute();

						$condition = 'workorder_id="'.$pass_count_new[0]['workorder_id'].'" AND user_id="'.Yii::$app->user->id.'"';

						$isused = $pass_count_new[0]['isUsed'] +  1;
						Yii::$app->db->createCommand()->update(Pass::tableName(),
						['isUsed' => $isused,'count_passes' => $count_passes], $condition)
						->execute();

				}

			}else{
				$ticketinfo = false;
			}
			/* show user loyality values */
			$totalpoints = '';
			$payablelpoint = '';
			$points_obtain = '';
			$points ='';
			$loyalityrate = Loyalty::find()->select('*')
				->where(['user_id' => $model['data'][0][0]->storeID])
				->orderBy('id desc')->one();

			$useLoyalitydetails = UserLoyalty::findOne(['user_id'=>Yii::$app->user->id,'store_id'=> $model['data'][0][0]->storeID]);

			$checkUserbday = User::findOne(['id'=>Yii::$app->user->id]);
			$checkStorebday = User::findOne(['id'=>$model['data'][0][0]->storeID]);
			if($model->useloyalty==1){
				if($data['nonce']!=''){
					if($model->loyalty[0]['points']>0) {
						$pointval1 = $model->loyalty[0]['points']/$model->loyalty[0]['value'];
					}else{
						$pointval1 = $loyalityrate->redeem_loyalty/$loyalityrate->redeem_dolor;

					}
					$payablelpoint = $pointval1*$model['data'][0][0]->price;

					$useLoyalityval = UserLoyalty::findOne(['user_id'=>Yii::$app->user->id,'store_id'=> $model['data'][0][0]->storeID]);
					$totalpoints = $useLoyalityval->points;

				}else{
					$payablelpoint = ($loyalityrate->redeem_loyalty/$loyalityrate->redeem_dolor)*$model['data'][0][0]->price;
					$totalpoints = $useLoyalitydetails->points;
				}
			} else {
				if(($checkUserbday->bdayOffer == 1) && ($checkStorebday->bdayOffer == 1)){
					$points_obtain = $loyalityrate->pointvalue*$model['data'][0][0]->price*2;
				} else {
					$points_obtain = $loyalityrate->pointvalue*$model['data'][0][0]->price;
				}

				if( $useLoyalitydetails)
				{
					$totalpoints =  $useLoyalitydetails->points;
				}
			}

			/* end user loyality values */

			return [
			"message"=> "Order successfully placed.",
			'orderID'=>$order->id,
			//'payment'=>$payment,
			'transactionID'=>$payment['id'],
			'isTicket' => $ticketinfo,
			'points_usedO' => $payablelpoint,
			'points_used' => $model->loyaltyPointsUsed,
			'points_obtain' =>    $points_obtain,
			'points_available' => $totalpoints,
			"statusCode"=> 200
			];
		} else {
			 Yii::$app->db->createCommand()->batchInsert(Cart::tableName(), $model->attributes(), $model->rows)->execute();
                  return [
                   "message"=> "",
                   'session_key'=>$model->session_key,
                   "statusCode"=> 200
                   ];

		}
	}
	/* save work order order  */
	public function saveWorkOrder($model,$order){
		$createdAt = $order->created_at;
		$userId = Yii::$app->user->identity->id;
		$orderItems=array();
		$orderSubItems=array();
		foreach($model->data as $key=>$value) {
			foreach($value as $item)	{
				$transFees = $this->actionTransactionFees($item->total-$item->discount,$item->storeID,$item->workOrderID,1,$order->id);

				array_push($orderItems,[
						'product_id'=>$item->productID,
						'deal_id'=>$item->workOrderID,
						'deal_count'=>$item->dealCount,
						'isCombo'=>$item->isCombo,
						'qty'=>$item->quantity,
						'order_id'=>$order->id,
						'user_id'=>Yii::$app->user->id,
						'store_id'=>$item->storeID,
						'product_price'=>$item->productPrice,
						'discount'=>$item->discount,
						'total'=>$item->total,
						'payable'=>$item->total-$item->discount,
						'status'=>2,
						'transactionFees'=>$transFees,
						'created_at'=>$createdAt,
						'updated_at'=>$createdAt,
						'created_by'=>$userId,
						'updated_by'=>$userId,
					]);



				if($item->isCombo)
				{

					array_push($orderSubItems,[
					'product_id'=>$item->comboData->productID,
					'deal_id'=>$item->comboData->workOrderID,
					'qty'=>$item->comboData->quantity,
					'order_id'=>$order->id,
					'user_id'=>Yii::$app->user->id,
					'store_id'=>$item->comboData->storeID,
					'product_price'=>$item->comboData->productPrice,
					'discount'=>$item->comboData->discount,
					'total'=>$item->comboData->total,
					'status'=>2,
					'created_at'=>$createdAt,
					'updated_at'=>$createdAt,
					'created_by'=>$userId,
					'updated_by'=>$userId,
					]);

				}

			}

		}
		if(count($orderItems))	{
			$OrderItems=new OrderItems();
			$column=array_keys($orderItems[0]);
			Yii::$app->db->createCommand()->batchInsert(OrderItems::tableName(),$column, $orderItems)->execute();
		}
		if(count($orderSubItems)) {
			$OrderSubItems=new OrderSubItems();
			$column=array_keys($orderSubItems[0]);
			Yii::$app->db->createCommand()->batchInsert(OrderSubItems::tableName(), $column,$orderSubItems)->execute();
		}
	}
	/* save food order  */
	public function saveFoodOrder($order){
		$orderItems=array();
		$orderSubItems=array();
		$createdAt = $order->created_at;
		$userId = Yii::$app->user->identity->id;
		$orderFood = Foodordersapi::findOne($foodorderID);
		if($orderFood) {
			$orderFood->status = 2;
			$orderFood->save(false);
			/* send notification to kitchen/resturent */
			if(!empty($data['lat']) && !empty($data['lang'])){
				$this->sendnotification($orderFood->store_id,$data['lat'],$data['lang'],$orderFood->id,$order->id);
			}
			/* get food order item from order id */
			$storeId = $orderFood->store_id;
			$forderItem = FoodordersItem::find()->where('order_id='.$foodorderID)->asArray()->all();

				foreach($forderItem as $_forderItem) {

				$transFees = $this->actionTransactionFees($_forderItem['grand_total'],$storeId,$_forderItem['product_id'],0,$order->id);

				array_push($orderItems,[
					'product_id'=>$_forderItem['product_id'],
					'qty'=>$_forderItem['qty'],
					'order_id'=>$order->id,
					'user_id'=>Yii::$app->user->id,
					'store_id'=>$storeId,
					'product_price'=>$_forderItem['price'],
					'total'=>$_forderItem['grand_total'],
					'payable'=>$_forderItem['grand_total'],
					'status'=>2,
					'transactionFees'=>$transFees,
					'created_at'=>$createdAt,
					'updated_at'=>$createdAt,
					'created_by'=>$userId,
					'updated_by'=>$userId,
				]);
				/* save order sub item */
				if($_forderItem['have_child'] == 1){
					$subItem = FoodordersSubItem::find()->where('order_id='.$foodorderID)->asArray()->all();
					foreach($subItem as $_subItem){
						array_push($orderSubItems,[
								'product_id'=>$_subItem['product_id'],
								'qty'=>$_subItem['qty'],
								'order_id'=>$order->id,
								'user_id'=>$userId,
								'store_id'=>$storeId,
								'product_price'=>$_subItem['price'],
								'total'=>$_subItem['qty']*$_subItem['price'],
								'status'=>2,
								'created_at'=>$createdAt,
								'updated_at'=>$createdAt,
								'created_by'=>$userId,
								'updated_by'=>$userId,
							]);
					} //end foreach sub item
				}
			} //foreach
		}
		if(count($orderItems))	{
			$OrderItems=new OrderItems();
			$column=array_keys($orderItems[0]);
			Yii::$app->db->createCommand()->batchInsert(OrderItems::tableName(), $column, $orderItems)->execute();
		}
		if(count($orderSubItems)) {
			$OrderSubItems=new OrderSubItems();
			$column=array_keys($orderSubItems[0]);
			Yii::$app->db->createCommand()->batchInsert(OrderSubItems::tableName(),$column,$orderSubItems)->execute();
		}


	}
	//save order loyalt
	public function saveLoyalty($model,$order){
		$createdAt = $order->created_at;
		$userId = Yii::$app->user->identity->id;
		$orderLoyalty = [];
		foreach($model->storeTotal as $key=>$value)
		{
			$total_price_value = round(($value["total"]-$value["discount"] - $value["loyalty"]),2);$total_price_value= ($total_price_value <= 0) ? 0: $total_price_value;
			array_push($model->loyaltyOrderData,[
				"order_id"=>$order->id,
				"user_id"=>$userId,
				"store_id"=>$key,
				"payable"=>$total_price_value,
				"value"=>$value["loyalty"],
				"created_at"=>$createdAt,
			]);

		$result['user_id'] = $userId;
		$result['order_id'] = $order->id;
		$result['store_id'] = $key;
		$result['status'] = 2;
		$result['created_at'] = $createdAt;
		$result['updated_at'] = $createdAt;
		$result['created_by'] = $userId;
		$result['updated_by'] = $userId;


		$loyalty = new Loyalty();
		$loyaltyid=Loyalty::find()->where(['user_id'=>$key])
			->select('*')
			->orderBy(['id' => SORT_DESC,])
			->asArray()->one();

		if(count($loyaltyid) && $total_price_value && $model->useloyalty==0)
		{
			$checkUserbday = User::findOne(['id'=>$userId]);
			$checkStorebday = User::findOne(['id'=>$key]);
			/* check minimum spend */
			$loyaltypoints = 0;
			$method = 1;
			$totalSpend = $this->calculateSpend($key);
			if($totalSpend['message']=='ok') 	{
				if(($checkUserbday->bdayOffer==1) && ($checkStorebday->bdayOffer==1) ){
					$loyaltypoints = $total_price_value * $loyaltyid['pointvalue']*2;
					$method = 3;
				}	else {
					$loyaltypoints = $total_price_value * $loyaltyid['pointvalue'];
				}
			}
			array_push($orderLoyalty,[
				'loyaltypoints'=>$loyaltypoints,
				'valueOnDay'=>$loyaltyid['dolor']/$loyaltyid['loyalty'],
				'rValueOnDay'=>$loyaltyid['redeem_pointvalue'],
				'order_id'=>$order->id,
				'user_id'=>$userId,
				'store_id'=>$key,
				'method'=>$method,
				'status'=>$totalSpend['status'],
				'transaction_id'=>1234,
				'created_at'=>$createdAt,
				'updated_at'=>$createdAt,
				'created_by'=>$userId,
				'updated_by'=>$userId,
			]);
		}

		}

		if(count($model->loyaltyOrderData))
		{
			$loyaltyOrderData=new OrderLoyalty();
			$loyaltyOrderDataColumn=$loyaltyOrderData->attributes();
			array_shift($loyaltyOrderDataColumn);

			Yii::$app->db->createCommand()->batchInsert(OrderLoyalty::tableName(), $loyaltyOrderDataColumn, $model->loyaltyOrderData)->execute();
		}
		if(count($orderLoyalty)){
			$Loyaltypoints=new Loyaltypoint();
			Yii::$app->db->createCommand()->batchInsert(Loyaltypoint::tableName(),array_keys($orderLoyalty[0]), $orderLoyalty)->execute();
		}
	}

	// save card on promise pay
	public function saveCard($data,$promiseUid){
					$CardData = ['number'=>$data->card_number,'expiry_month'=>$data->expiry_month,'expiry_year'=>$data->expiry_year,'cvv'=>$data->cvv,'full_name'=>$data->full_name,'user_id'=>$promiseUid];
					$CardData = (object)$CardData;
					$cards = Yii::$app->promisePay->createCardAccount($CardData);
					if(isset($response['id'])){
						return $response['id'];
					}
	}




    /* To validate cart and payment */
    public function actionUpdate($id)
    {

       if($id)
       {
           $testPaymentMode=true;

           try{
                $data=Yii::$app->getRequest()->getBodyParams();
                $cart=json_decode($data['cart']);
                $model=new Cart();
				$foodOrder = $data['foodorder'];
				$foodorderID = $data['foodorderID'];
				$checktype = 0;
				$tempcard=$data['tempcard'];
				if(isset($data['foodorderID']) && $data['foodorderID']!=0){
					$checktype =1;
				}

				$model->foodorder=$foodOrder;
				$model->foodorder_id=$foodorderID;
				$model->data=$cart;
                $model->useloyalty=isset($data['useloyalty'])?$data['useloyalty']:0;
                $model->session_key=$id;
				$totalSpend	= 'no';
                /* Create instance of payment gateway */
                $braintree = Yii::$app->braintree;
                /* validate the data/cart */
                if($model->validate())
                {

                   $payment_method=new PaymentMethod();
                   $model->payable=round($model->payable,2);
                   if($model->payable >0)
                   {
                    /* find the account of customer for braintree */
                   $paymentAccount=PaymentAccount::find()->where(['user_id'=>Yii::$app->user->id,'status'=>1])->one();
                   $paymentCard=null;
                   $response =null;
				if($tempcard==0){
                   if(!$paymentAccount)
                   {
                       /* register customer to braintree */
                       $user=Customer::findOne(Yii::$app->user->id);
                       $response = $braintree->call('Customer', 'create', [
                           'firstName' => $user->firstName,
                               'lastName' => $user->lastName,
                               //'company' => 'graycell',
                               'email' => $user->email,
                               'phone' => $user->phone,
                               //'website' => ''
                           ]);

                           /* Register braintree account detail to database */
                           $paymentAccount=new PaymentAccount();
                           $paymentAccount->user_id=Yii::$app->user->id;
                           $paymentAccount->bt_id=$response->customer->id;
                           $paymentAccount->status=1;
                           $paymentAccount->save();

                     /* generate the payment method */
                   $response = $braintree->call('PaymentMethod','create',[
                       'customerId' => $paymentAccount->bt_id,
                       'paymentMethodNonce' => $data['nonce'],
                   ]);
				  if(!$response->success)
                   {

                       return [
                           "message"=> $response->message,
                           'nonce'=>$data['nonce'],
                           'cart'=>$data['cart'],
                           'session_key'=>$id,
                           "statusCode"=> 404
                           ];
                   }

				   $paymentCard->user_id=Yii::$app->user->id;
                   $paymentCard->bin=$response->paymentMethod->bin;
                   $paymentCard->last4=$response->paymentMethod->last4;
                   $paymentCard->cardType=$response->paymentMethod->cardType;
                   $paymentCard->expirationMonth=$response->paymentMethod->expirationMonth;
                   $paymentCard->expirationYear=$response->paymentMethod->expirationYear;
                   if($response->paymentMethod->cardholderName!="" && $response->paymentMethod->cardholderName!=null)
                   $paymentCard->cardholderName=$response->paymentMethod->cardholderName;
                   $paymentCard->customerLocation=$response->paymentMethod->customerLocation;
                   $paymentCard->imageUrl=$response->paymentMethod->imageUrl;
                   $paymentCard->prepaid=$response->paymentMethod->prepaid;
                   $paymentCard->healthcare=$response->paymentMethod->healthcare;
                   $paymentCard->debit=$response->paymentMethod->debit;
                   $paymentCard->durbinRegulated=$response->paymentMethod->durbinRegulated;
                   $paymentCard->commercial=$response->paymentMethod->commercial;
                   $paymentCard->payroll=$response->paymentMethod->payroll;
                   $paymentCard->issuingBank=$response->paymentMethod->issuingBank;
                   $paymentCard->countryOfIssuance=$response->paymentMethod->countryOfIssuance;
               //    $payment_method->productId=$response->paymentMethod->productId;
                   $paymentCard->token=$response->paymentMethod->token;
                   $paymentCard->uniqueNumberIdentifier=$response->paymentMethod->uniqueNumberIdentifier;
                   $paymentCard->maskedNumber=$response->paymentMethod->maskedNumber;
                   $paymentCard->venmoSdk=$response->paymentMethod->venmoSdk;
                   $paymentCard->expirationDate=$response->paymentMethod->expirationDate;
                   $paymentCard->is_delete=0;
                   $paymentCard->save(false);


                   } // if(!$paymentAccount)
                   else
                   {
					   $paymentCard=Paymentcard::find()
					->where(['is_delete'=>0,'systemToken'=>$data['nonce'],'user_id'=>Yii::$app->user->id])
					->one();

					}





                   /* save the payment method detail and token */
                   $payment_method=new PaymentMethod();
               //    $payment_method->load($response->paymentMethod->attributes());
                   $payment_method->bin=$paymentCard->bin;
                   $payment_method->last4=$paymentCard->last4;
                   $payment_method->cardType=$paymentCard->cardType;
                   $payment_method->expirationMonth=$paymentCard->expirationMonth;
                   $payment_method->expirationYear=$paymentCard->expirationYear;
                   $payment_method->cardholderName=$paymentCard->cardholderName;
                   $payment_method->customerLocation=$paymentCard->customerLocation;
                   $payment_method->imageUrl=$paymentCard->imageUrl;
                   $payment_method->prepaid=$paymentCard->prepaid;
                   $payment_method->healthcare=$paymentCard->healthcare;
                   $payment_method->debit=$paymentCard->debit;
                   $payment_method->durbinRegulated=$paymentCard->durbinRegulated;
                   $payment_method->commercial=$paymentCard->commercial;
                   $payment_method->payroll=$paymentCard->payroll;
                   $payment_method->issuingBank=$paymentCard->issuingBank;
                   $payment_method->countryOfIssuance=$paymentCard->countryOfIssuance;
               //    $payment_method->productId=$response->paymentMethod->productId;
                   $payment_method->token=$paymentCard->systemToken;
                   $payment_method->uniqueNumberIdentifier=$paymentCard->uniqueNumberIdentifier;
                   $payment_method->maskedNumber=$paymentCard->maskedNumber;
                   $payment_method->venmoSdk=$paymentCard->venmoSdk;
                   $payment_method->expirationDate=$paymentCard->expirationDate;
                   $payment_method->save(false);

                   /* Add/Edit card details in  payment cards */
                    /* Request to prcess the transaction */
                      $response = $braintree->call('Transaction', 'sale' ,[
                       'amount' => round($model->payable,2),
                           'customerId' =>  $paymentAccount->bt_id,
                           'paymentMethodToken' => $paymentCard->token,
                            'options' => [
                               'submitForSettlement' => True
                             ]

                       ]);

                }  // temp card
                else{
					  $user=Customer::findOne(Yii::$app->user->id);
                       $response = $braintree->call('Customer', 'create', [
                           'firstName' => $user->firstName,
                               'lastName' => $user->lastName,
                               //'company' => 'graycell',
                               'email' => $user->email,
                               'phone' => $user->phone,
                               //'website' => ''
                           ]);
                        $customerId =    $response->customer->id;
                             /* generate the payment method */
                   $response = $braintree->call('PaymentMethod','create',[
                       'customerId' => $customerId ,
                       'paymentMethodNonce' => $data['nonce'],
                   ]);
					  if(!$response->success)
					   {

						   return [
							   "message"=> $response->message,
							   'nonce'=>$data['nonce'],
							   'cart'=>$data['cart'],
							   'session_key'=>$id,
							   "statusCode"=> 404
							   ];
					   }
                         /* Request to prcess the transaction */
                      $response = $braintree->call('Transaction', 'sale' ,[
                       'amount' => round($model->payable,2),
                           'customerId' =>  $customerId ,
                           'paymentMethodToken' => $response->paymentMethod->token,
                            'options' => [
                               'submitForSettlement' => True
                             ]

                       ]);
					} // else{


                       if(!$response->success)
                       {

                           return [
                               "message"=> $response->message,
                               'nonce'=>$data['nonce'],
                               'cart'=>$data['cart'],
                               'session_key'=>$id,
                               "statusCode"=> 404
                               ];
                       }
                   }
                   /* Save the order */
                   $order =new Order();
                   //$order->shipping_description=
                   //$order->shipper_id=
                   $order->is_offline=0;
                   $order->user_id=Yii::$app->user->id;
                   $order->order_type=$checktype;
                   $order->discount_amount=$model->discount;
                   $order->grand_total=$model->total;
                   $order->shipping_amount=0;
                   $order->shipping_tax_amount=0;
                   $order->subtotal=$model->total - $model->discount;
                   $order->tax_amount=0;
                   $order->total_offline_refunded=0;
                   $order->total_online_refunded=0;
                   $order->total_paid=round($model->payable,2);
                   $order->qty_ordered=$model->qty;
                   $order->total_refunded=0;
                   $order->can_ship_partially=0;
                   $order->customer_note_notify=0;
                   $order->billing_address_id=0;
                   $order->billing_address="";
                   $order->shipping_address_id=0;
                   $order->shipping_address="";
                   $order->forced_shipment_with_invoice=0;
                   $order->weight=0;
                   $order->email_sent=0;
                   $order->edit_increment=0;
                   $order->status=2;
                   $order->food_oid=$foodorderID;
                   $order->save(false);

                   $created_at=time();
                   $orderLoyalty=[];
                   foreach($model->storeTotal as $key=>$value)
                   {


                        $total_price_value = round(($value["total"]-$value["discount"] - $value["loyalty"]),2);

                       $total_price_value= ($total_price_value <= 0) ? 0: $total_price_value;
                           array_push($model->loyaltyOrderData,[
                           "order_id"=>$order->id,
                           "user_id"=>Yii::$app->user->id,
                           "store_id"=>$key,
                           "payable"=>$total_price_value,
                           "value"=>$value["loyalty"],
                           "created_at"=>time(),
                           ]);

                               $result['user_id'] = Yii::$app->user->id;
                               $result['order_id'] = $order->id;
                               $result['store_id'] = $key;
                               $result['status'] = 2;
                               $result['created_at'] = $created_at;
                               $result['updated_at'] = $created_at;
                               $result['created_by'] = Yii::$app->user->id;
                               $result['updated_by'] = Yii::$app->user->id;


                               $loyalty = new Loyalty();
                               $loyaltyid=Loyalty::find()->where(['user_id'=>$key])
                                                   ->select('*')
                                                   ->orderBy(['id' => SORT_DESC,])
                                                   ->asArray()->one();

                              if(count($loyaltyid) && $total_price_value && $model->useloyalty==0)
							  {
                              $checkUserbday = User::findOne(['id'=>Yii::$app->user->id]);
                              $checkStorebday = User::findOne(['id'=>$key]);
							 /* check minimum spend */
							 $loyaltypoints = 0;
							 $method = 1;
							 $totalSpend = $this->calculateSpend($key);
							 if($totalSpend['message']=='ok') 	{
                               if(($checkUserbday->bdayOffer==1) && ($checkStorebday->bdayOffer==1) ){
                                 $loyaltypoints = $total_price_value * $loyaltyid['pointvalue']*2;
                                 $method = 3;
                               }else{
                                   $loyaltypoints = $total_price_value * $loyaltyid['pointvalue'];
                               }
							}   //      if($totalSpend=='ok')

                               array_push($orderLoyalty,[
                                           'loyaltypoints'=>$loyaltypoints,
                                           'valueOnDay'=>$loyaltyid['dolor']/$loyaltyid['loyalty'],
                                           'rValueOnDay'=>$loyaltyid['redeem_pointvalue'],
                                           'order_id'=>$order->id,
                                           'user_id'=>Yii::$app->user->id,
                                           'store_id'=>$key,
                                           'method'=>$method,
                                           'status'=>$totalSpend['status'],
                                           'transaction_id'=>1234,
                                           //'workorderid'=>0,
                                           'created_at'=>$created_at,
                                           'updated_at'=>$created_at,
                                           'created_by'=>Yii::$app->user->id,
                                           'updated_by'=>Yii::$app->user->id,
                                   ]);
                               }

                   }

                   if(count($model->loyaltyOrderData))
                   {
                       $loyaltyOrderData=new OrderLoyalty();
                       $loyaltyOrderDataColumn=$loyaltyOrderData->attributes();
                       array_shift($loyaltyOrderDataColumn);

                       Yii::$app->db->createCommand()->batchInsert(OrderLoyalty::tableName(), $loyaltyOrderDataColumn, $model->loyaltyOrderData)->execute();
                   }

                   if($model->payable>0)
                   {
                       $payment_method->order_id=$order->id;
                       $payment_method->save(false);
                   }
                   /* Get the orderitems for batch insert */
                   $orderItems=array();
                   $orderSubItems=array();
                   $pass=array();
                   //$orderLoyalty=array();
                   //$orderLoyalty=array();
                   $result=array();

				/* Prepare data if order type is food Order */
                   	if($foodOrder == 1 && $foodorderID!=''){
					$orderFood = Foodordersapi::findOne($foodorderID);
					if($orderFood){
					$orderFood->status = 2;
					$orderFood->save(false);
					/* send notification to kitchen/resturent */
					if(!empty($data['lat']) && !empty($data['lang'])){
					 $this->sendnotification($orderFood->store_id,$data['lat'],$data['lang'],$orderFood->id,$order->id);
					}
					/* get food order item from order id */
					$storeId = $orderFood->store_id;
					$forderItem = FoodordersItem::find()->where('order_id='.$foodorderID)->asArray()->all();
					foreach($forderItem as $_forderItem){
						 $transFees = $this->actionTransactionFees($_forderItem['grand_total'],$storeId,$_forderItem['product_id'],0,$order->id);
						array_push($orderItems,[
						   'product_id'=>$_forderItem['product_id'],
						   'deal_id'=>0,
						   'dealCount'=>0,
						   'isCombo'=>0,
						   'qty'=>$_forderItem['qty'],
						   'order_id'=>$order->id,
						   'user_id'=>Yii::$app->user->id,
						   'store_id'=>$storeId,
						   'store_location_id'=>0,
						   'product_price'=>$_forderItem['price'],
						   'product_currency_code'=>0,
						   'product_currency_id'=>0,
						   'payment_currency_code'=>0,
						   'payment_currency_id'=>0,
						   'exchange_rate'=>0,
						   'product_exchange_price'=>0,
						   'tax'=>0,
						   'shipping_charges'=>0,
						   'discount'=>0,
						   'total'=>$_forderItem['grand_total'],
						   'payable'=>$_forderItem['grand_total'],
						   'status'=>2,
						   'transactionFees'=>$transFees,
						   'created_at'=>$created_at,
						   'updated_at'=>$created_at,
						   'created_by'=>Yii::$app->user->identity->id,
						   'updated_by'=>Yii::$app->user->identity->id,
						   ]);
						   /* save order sub item */
						   if($_forderItem['have_child'] == 1){
							 $subItem = FoodordersSubItem::find()->where('order_id='.$foodorderID)->asArray()->all();
							 foreach($subItem as $_subItem){
									array_push($orderSubItems,[
									'product_id'=>$_subItem['product_id'],
									'deal_id'=>0,
									'qty'=>$_subItem['qty'],
									'order_id'=>$order->id,
									'user_id'=>Yii::$app->user->id,
									'store_id'=>$storeId,
									'store_location_id'=>0,
									'product_price'=>$_subItem['price'],
									'product_currency_code'=>0,
									'product_currency_id'=>0,
									'payment_currency_code'=>0,
									'payment_currency_id'=>0,
									'exchange_rate'=>0,
									'product_exchange_price'=>0,
									'tax'=>0,
									'shipping_charges'=>0,
									'discount'=>0,
									'total'=>$_subItem['qty']*$_subItem['price'],
									'status'=>2,
									'created_at'=>$created_at,
									'updated_at'=>$created_at,
									'created_by'=>Yii::$app->user->identity->id,
									'updated_by'=>Yii::$app->user->identity->id,
									]);
							} //end foreach sub item
						   }
						} //foreach
					}

					}else {
                   foreach($model->data as $key=>$value)
                   {
                       foreach($value as $item)
                       {

								  $transFees = $this->actionTransactionFees($item->total-$item->discount,$item->storeID,$item->workOrderID,1,$order->id);

                                   array_push($orderItems,[
                                   'product_id'=>$item->productID,
                                   'deal_id'=>$item->workOrderID,
                                   'dealCount'=>$item->dealCount,
                                   'isCombo'=>$item->isCombo,
                                   'qty'=>$item->quantity,
                                   'order_id'=>$order->id,
                                   'user_id'=>Yii::$app->user->id,
                                   'store_id'=>$item->storeID,
                                   'store_location_id'=>0,
                                   'product_price'=>$item->productPrice,
                                   'product_currency_code'=>0,
                                   'product_currency_id'=>0,
                                   'payment_currency_code'=>0,
                                   'payment_currency_id'=>0,
                                   'exchange_rate'=>0,
                                   'product_exchange_price'=>0,
                                   'tax'=>0,
                                   'shipping_charges'=>0,
                                   'discount'=>$item->discount,
                                   'total'=>$item->total,
                                   'payable'=>$item->total-$item->discount,
                                   'status'=>2,
                                   'transactionFees'=>$transFees,
                                   'created_at'=>$created_at,
                                   'updated_at'=>$created_at,
                                   'created_by'=>Yii::$app->user->id,
                                   'updated_by'=>Yii::$app->user->id,
                                   ]);



                                   if($item->isCombo)
                                   {

                                       array_push($orderSubItems,[
                                           'product_id'=>$item->comboData->productID,
                                           'deal_id'=>$item->comboData->workOrderID,
                                           'qty'=>$item->comboData->quantity,
                                           'order_id'=>$order->id,
                                           'user_id'=>Yii::$app->user->id,
                                           'store_id'=>$item->comboData->storeID,
                                           'store_location_id'=>0,
                                           'product_price'=>$item->comboData->productPrice,
                                           'product_currency_code'=>0,
                                           'product_currency_id'=>0,
                                           'payment_currency_code'=>0,
                                           'payment_currency_id'=>0,
                                           'exchange_rate'=>0,
                                           'product_exchange_price'=>0,
                                           'tax'=>0,
                                           'shipping_charges'=>0,
                                           'discount'=>$item->comboData->discount,
                                           'total'=>$item->comboData->total,
                                           'status'=>2,
                                           'created_at'=>$created_at,
                                           'updated_at'=>$created_at,
                                           'created_by'=>Yii::$app->user->id,
                                           'updated_by'=>Yii::$app->user->id,
                                   ]);

                                   }


                       }

                   }

				}

                   if(count($orderItems))
                   {
                       $OrderItems=new OrderItems();
                       $orderItemColumn=$OrderItems->attributes();
                       array_shift($orderItemColumn);

                       Yii::$app->db->createCommand()->batchInsert(OrderItems::tableName(), $orderItemColumn, $orderItems)->execute();

                   if(count($orderSubItems))
                   {

                       $OrderSubItems=new OrderSubItems();
                       $orderSubItemColumn=$OrderSubItems->attributes();
                       array_shift($orderSubItemColumn);
                       Yii::$app->db->createCommand()->batchInsert(OrderSubItems::tableName(), $orderSubItemColumn, $orderSubItems)->execute();
                   }
                       if(count($orderLoyalty)){
                       $Loyaltypoints=new Loyaltypoint();
                       Yii::$app->db->createCommand()->batchInsert(Loyaltypoint::tableName(),array_keys($orderLoyalty[0]), $orderLoyalty)->execute();
                   }

                   }


                   /* empty the temp. cart */
                   if($id)
                   $tmodel=Cart::deleteAll('session_key = :key ', [':key' => $id]);

                   $transactionID=0;
                   if($model->payable>0)
                   {
                   /* save the transaction details */
                   $transaction=new Transaction();
                   $transaction->order_id=$order->id;
                   $transaction->tid = $response->transaction->id;
                   $transaction->status = $response->transaction->status;
                   $transaction->type = $response->transaction->type;
                   $transaction->currencyIsoCode = $response->transaction->currencyIsoCode;
                   $transaction->amount = $response->transaction->amount;
                   $transaction->merchantAccountId = $response->transaction->merchantAccountId;
                   $transaction->save(false);
                   $transactionID=$transaction->tid;
                   }

                   /*Send confirmation email */
                   $subject="Alpha Order Confirmation";
                   $user=Customer::findOne(['id' => Yii::$app->user->id]);

                  $message=sprintf("Dear %s,<br/> <br/>Thanks for your purchase. You should receive your email confirmation soon.Your order details are as follows: <br/><br/>Order ID: %s<br />Transaction ID: %s<br /><br/>If you have further questions or enquiries please <u>CONTACT US</u>. In the meantime, sit back and enjoy your day.<br /><br/>Thanks<br/><br />The Alpha Team",$user->fullName,$order->id,$transactionID);
                    $emailsend =Yii::$app->mail->compose()
                            ->setTo($user->email)
                            ->setFrom(Yii::$app->params["salesEmail"],'No Reply')
                            ->setSubject($subject)
                            ->setHtmlBody($message)
                           ->send();

       if(count($model->dealsID))
               {
               foreach($model->dealsID as $dealsIDInfo){
                   $pass_count_new =Pass::find()
                                   ->select([
                                               Pass::tableName().'.id,user_id,workorder_id,store_id,isUsed,count_passes'
                                               ,Workorders::tableName().'.id,type,advertiserID,name,dateTo,noPasses,usedPasses,type,allowpasstouser'])
                               ->leftJoin(Workorders::tableName(), Pass::tableName().'.workorder_id = '.Workorders::tableName(). '.id')
                                   ->where(["user_id"=>Yii::$app->user->id,"workorder_id"=>$dealsIDInfo['workorder_id']])
                                   ->asArray()
                                   ->all();

               /************** update code on workorder table **************/
               if($pass_count_new){
                   if($pass_count_new[0]['isUsed']==0){

                           $count_passes_usedPasses =  $dealsIDInfo['count_passes'] - 1;
                           $usedPasses = $pass_count_new[0]['usedPasses'] + $count_passes_usedPasses;
                           $count_passes =$dealsIDInfo['count_passes'];
                       }else{

                           $usedPasses = $pass_count_new[0]['usedPasses'] + $dealsIDInfo['count_passes'];
                           $count_passes =$pass_count_new[0]['count_passes'] + $dealsIDInfo['count_passes'];
                       }

                   if($pass_count_new[0]['type']==1 || $pass_count_new[0]['type']==2){
                   Yii::$app->db->createCommand()->update(Workorders::tableName(),
                                       ['usedPasses' => $usedPasses],
                                       ['id' => $pass_count_new[0]['workorder_id']])
                                       ->execute();
                   /**************** end here **********************************/
                   /************** update code on pass table **************/
                       $condition = 'workorder_id="'.$pass_count_new[0]['workorder_id'].'" AND user_id="'.Yii::$app->user->id.'"';

                       $isused = $pass_count_new[0]['isUsed'] +  1;
                       //$count_check = $pass_count_new[0]['check_count'] + 1;
                           Yii::$app->db->createCommand()->update(Pass::tableName(),
                                           ['isUsed' => $isused,'count_passes' => $count_passes], $condition)
                                       ->execute();
                       }
                   /**************** end here **********************************/
                   }else{

                       $Passes=new Pass();
                       $Passes->workorder_id = $dealsIDInfo['workorder_id'];
                       $Passes->user_id = Yii::$app->user->id;
                       $Passes->store_id = $model['data'][0][0]->storeID;
                       $Passes->isUsed = 1;
                       $Passes->count_passes = 1;
                       $Passes->passesTrack = 1;
                       $Passes->save(false);

                    }
               }

           }

       /******************** end here ************************/
           $ticketinfo = '';
           if(count($model->eventTickets))
            {
                $ticketinfo = true;
               foreach($model->eventTickets as $eventTicketsInfo){
                   $pass_count_new =Pass::find()
                                   ->select([
                                               Pass::tableName().'.id,user_id,workorder_id,store_id,isUsed,count_passes'
                                               ,Workorders::tableName().'.id,type,advertiserID,name,dateTo,noPasses,usedPasses,type,allowpasstouser'])
                               ->leftJoin(Workorders::tableName(), Pass::tableName().'.workorder_id = '.Workorders::tableName(). '.id')
                                   ->where(["user_id"=>Yii::$app->user->id,"workorder_id"=>$eventTicketsInfo['workorder_id']])
                                   ->asArray()
                                   ->all();

               if($pass_count_new[0]['isUsed']==0){

                       $count_passes_usedPasses =  $eventTicketsInfo['count_passes'] - 1;
                       $usedPasses = $pass_count_new[0]['usedPasses'] + $count_passes_usedPasses;
                       $count_passes =$eventTicketsInfo['count_passes'];
                   }else{

                       $usedPasses = $pass_count_new[0]['usedPasses'] + $eventTicketsInfo['count_passes'];
                       $count_passes =$pass_count_new[0]['count_passes'] + $eventTicketsInfo['count_passes'];
                   }
               Yii::$app->db->createCommand()->update(Workorders::tableName(),
                                   ['usedPasses' => $usedPasses],
                                   ['id' => $pass_count_new[0]['workorder_id']])
                                   ->execute();

                   $condition = 'workorder_id="'.$pass_count_new[0]['workorder_id'].'" AND user_id="'.Yii::$app->user->id.'"';

                   $isused = $pass_count_new[0]['isUsed'] +  1;
                   //$count_check = $pass_count_new[0]['check_count'] + 1;
                       Yii::$app->db->createCommand()->update(Pass::tableName(),
                                       ['isUsed' => $isused,'count_passes' => $count_passes], $condition)
                                   ->execute();

               }

           }else{
             $ticketinfo = false;
           }

           $totalpoints = '';
           $payablelpoint = '';
           $points_obtain = '';
           $points ='';

           /* show user loyality values */
           $loyalityrate = Loyalty::find()->select('*')
								->where(['user_id' => $model['data'][0][0]->storeID])
								->orderBy('id desc')->one();

           $useLoyalitydetails = UserLoyalty::findOne(['user_id'=>Yii::$app->user->id,'store_id'=> $model['data'][0][0]->storeID]);

           $checkUserbday = User::findOne(['id'=>Yii::$app->user->id]);
           $checkStorebday = User::findOne(['id'=>$model['data'][0][0]->storeID]);
           if($model->useloyalty==1){

           if($data['nonce']!=''){
               if($model->loyalty[0]['points']>0) {
                   $pointval1 = $model->loyalty[0]['points']/$model->loyalty[0]['value'];
               }else{
                   //$pointval1 = $loyalityrate->pointvalue;
                   $pointval1 = $loyalityrate->redeem_loyalty/$loyalityrate->redeem_dolor;

               }
                   $payablelpoint = $pointval1*$model['data'][0][0]->price;

                   $useLoyalityval = UserLoyalty::findOne(['user_id'=>Yii::$app->user->id,'store_id'=> $model['data'][0][0]->storeID]);
                   $totalpoints = $useLoyalityval->points;


               }else{

                   //$payablelpoint = $loyalityrate->pointvalue*$model['data'][0][0]->price;
                   $payablelpoint = ($loyalityrate->redeem_loyalty/$loyalityrate->redeem_dolor)*$model['data'][0][0]->price;
                   $totalpoints = $useLoyalitydetails->points;
               }


           }
           else{
               /* if loyality 0 */
               /* check user bday */
		//	if($totalSpend=='ok') 	{
               if(($checkUserbday->bdayOffer == 1) && ($checkStorebday->bdayOffer == 1)){
                   $points_obtain = $loyalityrate->pointvalue*$model['data'][0][0]->price*2;
               }else{
                   $points_obtain = $loyalityrate->pointvalue*$model['data'][0][0]->price;
               }
		//	}
                 if( $useLoyalitydetails)
				 {
					$totalpoints =  $useLoyalitydetails->points;
				 }
           }

           /* end user loyality values */


                   return [
                   "message"=> "Order successfully placed.",
                   'orderID'=>$order->id,
                   'transactionID'=>$transactionID,
                   'isTicket' => $ticketinfo,
                   'points_usedO' => $payablelpoint,
                   'points_used' => $model->loyaltyPointsUsed,
                   'points_obtain' =>    $points_obtain,
                   'points_available' => $totalpoints,
                   "statusCode"=> 200
                   ];
               }
                else
                {
                    return $model;
                   Yii::$app->db->createCommand()->batchInsert(Cart::tableName(), $model->attributes(), $model->rows)->execute();
                   $braintree = Yii::$app->braintree;
                   $response = $braintree->call('ClientToken', 'generate' ,[]);
                   return [
                   "message"=> "",
                   'session_key'=>$model->session_key,
                   'clientToken'=>$response,
                   "statusCode"=> 200
                   ];
                }
               return $model;

           }
           catch(Exception $e)
           {      return [
                   "message"=> $e->message,
                   'nonce'=>$data['nonce'],
                   'cart'=>$data['cart'],
                   'session_key'=>$id,
                   "statusCode"=> 404
                   ];
               }
       }

   }
  // $this->sendnotification($orderFood->store_id,$postData['lat'],$postData['lang']);
public function sendnotification($store_id,$lat,$lang,$foid,$oid){

	$store = Store::find()->select('storename,latitude,longitude,geo_radius,id')
							->where('owner='.$store_id)->asArray()->all();
	 $storeDis = array();
	foreach($store as $_store)  {
		 $theta = $_store['longitude'] - $lang;
		 $dist = sin(deg2rad($_store['latitude'])) * sin(deg2rad($lat)) +  cos(deg2rad($_store['latitude'])) * cos(deg2rad($lat)) * cos(deg2rad($theta));
		 $dist = acos($dist);
		 $dist = rad2deg($dist);
		 $miles = $dist * 60 * 1.1515;
		// $unit = strtoupper($unit);
		 $storeData = array();
		 $storeData['distancekm']= ($miles * 1.609344);
		 $storeData['storename']= $_store['storename'];
		 $storeData['storeID']= $_store['id'];
		 $storeData['geo_radius']= $_store['geo_radius'];
		 $storeDis[] = $storeData;
	}
	usort($storeDis, function($a, $b) {
		return $a['distancekm'] - $b['distancekm'];
	});
	$res = isset($storeDis[0]) ? $storeDis[0]:'';
	$radious = isset($res['geo_radius']) ? $res['geo_radius'] : $this->partnerradious($res['storeID']);

	$res['geo_radius'] = $radious ;
	$res['distancekm'] = !empty($res['distancekm']) ? $res['distancekm']:1;
	if($res['distancekm'] <= $res['geo_radius'])
	{
		$user = User::find()->where('storeid='.$res['storeID'])
							->one();
			if(isset($user->device_token)) {
			$userDevice[$user->device_token] = $user->device;
			return yii::$app->pushNotification->send($userDevice,["msg"=>"New order #$oid has been paid for. Please open the order and make it active. Please tell the kitchen to begin cooking.+$foid-$oid"],0);
		}
	}

}
public function partnerradious($id)
{
		$user = Profile::find()->select('geo_radius')->where('user_id='.$id)->asArray()->one();
		return isset($user['geo_radius']) ? $user['geo_radius'] : 1;
}
   public function actionDelete($id)
   {
       if($id)
       $model=Cart::deleteAll('session_key = :key ', [':key' => $id]);
       return [
           "message"=> "",
           "statusCode"=> 204
           ];


   }

	// check minimum spend by user on this store for get points
	public function calculateSpend($storeId){
			$user_id=Yii::$app->user->id;
				$lo = Loyaltypoint::tableName();
				$pro = Profile::tableName();
				$ord = OrderItems::tableName();
				$offline = Loyaltypoint::find()
									->select(["sum($lo.amount) as totalSpend"])
									->where("$lo.store_id=".$storeId)
									->andWhere("$lo.user_id=".$user_id)
									->asArray()
									->one();
				$online = OrderItems::find()
									->select(["sum($ord.payable) as total"])
									->where("$ord.store_id=".$storeId)
									->andWhere("$ord.user_id=".$user_id)
									->asArray()
									->one();
				$minimum = Profile::find()
									->select(["$pro.minimumSpend"])
									->where("$pro.user_id=".$storeId)
									->asArray()
									->one();
				$offline1 = $offline['totalSpend']!=''?$offline['totalSpend']:0;
				$online1  = $online['total']!=''?$online['total']:0;
				$totalSpend = $offline1+$online1;
				$status = $minimum['minimumSpend']>0?2:1;
				if($totalSpend >= $minimum['minimumSpend']){
					return [
					  'message'=>'ok',
					  'status'=>1
						];
				}else{
					return [
					  'message'=>'no',
					  'status'=>2
					];
				}

	}

 public function actionTransactionFees($price,$StoreID,$woId,$isWorkOrder,$orderId) {
		// required params
		/*$price = Yii::$app->request->post('price');
		$StoreID = Yii::$app->request->post('StoreID');;
		$woId = Yii::$app->request->post('workorderId');
		$isWorkOrder = Yii::$app->request->post('isWorkOrder');
		$orderId = Yii::$app->request->post('orderId');    */
		$feeamount = 0;
		$charge_type = '';
		$transFees = \common\models\PartnerFees::findOne(['partner_id'=>$StoreID]);
		if($isWorkOrder==0){
			if(is_object($transFees) &&  isset($transFees->amount_1)){
				$amount = 	 $transFees->amount_1;
				$charge_type = 1;
				$feeamount = $transFees->charge_type_1 == 1 ? $amount : ($amount/100)*$price;
			}

		}
		else {
			$workorder = \common\models\Workorderpopup::findOne(['workorder_id'=>$woId]);
			if(is_object($transFees) && is_object($workorder)){

				$offertype = $workorder->offertype;
				$offertext1 = $workorder->offertext1;

				if($offertype==1 && isset($transFees->amount_7)){
					// Offer Was/Now
					$charge_type = 7;
					$amount = $transFees->amount_7;
					$feeamount = $transFees->charge_type_7 == 1 ? $amount : ($amount/100)*$price;
				}else if($offertype==3 && $offertext1==1 && isset($transFees->amount_2)){
					// 	Buy xx get xx
					$amount = $transFees->amount_2;
					$charge_type =2;
					$feeamount = $transFees->charge_type_2 == 1 ? $amount : ($amount/100)*$price;
				}
				else if($offertype==3 && $offertext1==2 && isset($transFees->amount_3)){
						// 	Get xx %off
					$amount = $transFees->amount_3;
					$charge_type = 3;
					$feeamount = $transFees->charge_type_3 == 1 ? $amount : ($amount/100)*$price;
				}
				else if($offertype==3 && $offertext1==3 && isset($transFees->amount_4)){
				// 	Enjoy xx% Off on xx
					$amount = $transFees->amount_4;
					$charge_type = 4;
					$feeamount = $transFees->charge_type_4 == 1 ? $amount : ($amount/100)*$price;
				}
				else if($offertype==3 && $offertext1==4 && isset($transFees->amount_5)){
						// Buy xx get xx % off
					$amount = $transFees->amount_5;
					$charge_type = 5;
					$feeamount = $transFees->charge_type_5 == 1 ? $amount : ($amount/100)*$price;
				}
				else if($offertype==3 && $offertext1==5 && isset($transFees->amount_6)){
					//	Buy xx for $ xx
					$amount = $transFees->amount_6;
					$charge_type = 6;
					$feeamount = $transFees->charge_type_6 == 1 ? $amount : ($amount/100)*$price;
				}
				else if(isset($transFees->amount_1)){
					//	Per transactions
					$amount = 	 $transFees->amount_1;
					$charge_type = 1;
					$feeamount = $transFees->charge_type_1 == 1 ? $amount : ($amount/100)*$price;
				}

			}

		}
		/* save the transaction fee detail */
		if($feeamount>0){
			// get near by store
			$lat = Yii::$app->user->identity->latitude;
			$lng = Yii::$app->user->identity->longitude;
			$model = new PartnerTransactionsFees();
			$model->partner_id = $StoreID;
			$model->store_id = $this->getNearByStore($StoreID,$lat,$lng);
			$model->order_id = $orderId;
			$model->offer_type = $charge_type;
			$model->orderAmount = $price;
			$model->transactionFees = $feeamount;
			$model->workorder_id = $woId;
			$model->save(false);
		}

		return $feeamount;
	}

	public function getNearByStore($StoreID,$userLat=null,$userLng=null) {
		$store = \common\models\Store::find()->select('storename,latitude,longitude,id')
		->where('owner='.$StoreID)
		->asArray()
		->all();
		if(count($store)==0){
			return 0;
		}
		$storeDis = array();
		foreach($store as $_store)  {
			$theta = $_store['longitude'] - $userLng;
			$dist = sin(deg2rad($_store['latitude'])) * sin(deg2rad($userLat)) +  cos(deg2rad($_store['latitude'])) * cos(deg2rad($userLat)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$storeData = array();
			$storeData['distancekm']= ($miles * 1.609344);
			$storeData['storename']= $_store['storename'];
			$storeData['id']= $_store['id'];
			$storeDis[] = $storeData;
		}
		usort($storeDis, function($a, $b) {
				return $a['distancekm'] - $b['distancekm'];
			});
		$data = count($storeDis)==0 ? 0:$storeDis[0]['id'];
		return  $data;
	}

}
