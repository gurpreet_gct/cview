<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use api\components\Controller;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use common\models\Offertrack;
use common\models\Workorders;
use common\models\Workorderpopup;
use common\models\Loyalty;
use common\models\User;
use common\models\Pass;

class OffertrackController extends Controller
{    
        public $modelClass = 'common\models\Offertrack';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
    public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['delete'],$actions['view'],$actions['index']);                    

		return $actions;
	}

	/* order */
	public function actionIndex()
	{		
		   return new ActiveDataProvider([ 
            'query' => Offertrack::find()->orderBy("id desc")
       			
        ]);
		 
	}

  	/* create offer track */
   public function actionCreate()
   {
        $user_id	=Yii::$app->user->id; 
         		
		$model		= new Offertrack();			
		// $PassModel 	= new Pass();			
		$model->load(Yii::$app->getRequest()->getBodyParams(), ''); 
		
		$model->created_by	=	$user_id;
		$model->updated_by	=	$user_id;
		$model->user_id		=	$user_id;
		
		if($model->validate()){								
				/* check pin is validate form Loyalty model by workorder id  */
				$workorder 	= new Workorders();
				$loyalty	= new Loyalty();
				
				$checkpin	= Workorders:: find()->select(
										[Workorders::tableName().'.workorderpartner',
										Workorders::tableName().'.dateTo',
										User::tableName().'.id',
										User::tableName().'.loyaltyPin as loyalty_pin'])
										->leftJoin(User::tableName(), User::tableName().'.advertister_id = '.Workorders::tableName(). '.workorderpartner') //and user_type=9
										->where([Workorders::tableName().'.id' => $model->workorder_id ,
										User::tableName().'.loyaltyPin' => $model->pin, 
										Workorders::tableName().'.status' =>1])
										->orderBy([Workorders::tableName().'.id'=>'desc'])
										->asArray()
										->one();
				
					/* is pin is valid */
					
				if($checkpin){
					
					/* check total number from workorder popup model */						
					$checktotalNo = Workorderpopup::find()
						->select(['customOfferSelect1','customOfferSelect2'])
						->where(['workorder_id' => $model->workorder_id])
						->one();
					$paidoffer = $checktotalNo->customOfferSelect1; 
					$freeoffer = $checktotalNo->customOfferSelect2;
					$total = $paidoffer+$freeoffer;  
					
					/* used number from Offertrack model */
					$usedtotal = Offertrack::find()
									->where(['workorder_id' => $model->workorder_id])->count();			
					/* if free some numbers */
					
					$passObj = Pass::find()->Where(['user_id'=>$user_id,'store_id'=>$checkpin['workorderpartner'],'workorder_id'=>$model->workorder_id,'isUsed'=>0])->one();
					
					$offerTrack = '{{%offerTrack}}'	;
					
					if($total>=$model->usedNo) {
						
						$checkusedNo = Offertrack::find()->select('*')
										->leftJoin(Pass::tableName(),Pass::tableName().'.user_id = '.$offerTrack.'.user_id AND '.Pass::tableName().'.workorder_id = '.$offerTrack.'.workorder_id')
										->where([$offerTrack.'.workorder_id' => $model->workorder_id,$offerTrack.'.user_id' => $user_id,'isUsed'=>0])
										->andWhere($offerTrack.'.usedNo >='.$model->usedNo)
										->orderBy(Pass::tableName().'.id desc')->one();
						
						
						$getlast 	 = Offertrack::find()->where(['workorder_id' => $model->workorder_id, 'user_id' => $user_id])->orderBy('id desc')->one();
						
						if($checkusedNo){
								
								///return 'you have already used this number, try another number';
								
								return ["message"=>"You have already stamped this. Please stamp a new stamp.","statusCode"=>200];
								
								
						}else{
							
							if($getlast){
									
									$lastqty  	= $getlast->usedNo;
									$model->qty = $model->usedNo-$lastqty ;									  
									$model->save();
									
									// check is total used and return message
									
									if($total == $model->usedNo){
										
										// update field isUsed = 1 
										
										if($passObj->isUsed==0) $passObj->isUsed=1;$passObj->save(false);
										
										Offertrack::deleteAll(['workorder_id'=>$model->workorder_id,'user_id'=>$user_id]);
										
										return ["message"=>"Your stamp card has now expired. Please download a new card from the loyalty section of this store.","statusCode"=>200];
										
									}else{
										
										return $model;
										
									}										
								
							}else{	
									
									$model->qty = $model->usedNo;	
									
									$model->save();
									
									// check is total used and return message
									
									if($total == $model->usedNo){
										
										// update field isUsed = 1 
										
									   if($passObj->isUsed==0) $passObj->isUsed=1;$passObj->save(false);
										
										Offertrack::deleteAll(['workorder_id'=>$model->workorder_id,'user_id'=>$user_id]);
										return ["message"=>"Your stamp card has now expired. Please download a new card from the loyalty section of this store.","statusCode"=>200];
										
									}else{
										
										return $model;
										
									}
									
							}
						}
						
					}else{
						//return 'you have used your all paid and free offer';
						 return ["message"=>"you have used your all paid and free offer","statusCode"=>422];
					}
						
				}else{
				  //return 'invalid pin or deal expire';
				  
				  
				  return ["message"=>"That PIN does not match your registered 6-digit PIN. Please try again.","statusCode"=>422];
				 
				}
					
		}else{
			
			return $model;
		
		}
         
            
   }
  
   
}
