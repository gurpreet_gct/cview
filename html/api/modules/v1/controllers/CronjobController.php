<?php

namespace api\modules\v1\controllers;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\components\Controller;
use backend\models\Notification;
use api\modules\v1\models\Customer;
use yii\helpers\ArrayHelper;
use yii;
//use api\components\Controller;
/**
 * Cron Job  Controller API
 * 
 */
class CronjobController extends Controller
{
    public $modelClass = 'api\modules\v1\models\Country';    

  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);                    

		return $actions;
	}


	public function actionIndex()
	{
		$result=Customer::find()
		->select(['device','device_token','id','bdayOffer'])
		->where([">","dob",0])
		->andWhere("dayofmonth(from_unixtime(dob))=dayofmonth(CONVERT_TZ(now(), 'SYSTEM', timezone_offset))")
		->andWhere("month(from_unixtime(dob)) = month(CONVERT_TZ(now(), 'SYSTEM', timezone_offset))")
		->andWhere("hour(CONVERT_TZ(now(), 'SYSTEM', timezone_offset))=10")
		->andWhere("device_token is not null")
		->andWhere("device_token != ''")
		->asArray()
		->all();
		
		$uniqueUserNeedToUpdate=[];
		$uniqueUserNeedToSendPush=[];
		$uniqueUserAlreadyUpdated=[];
		$rows=array();	
		foreach($result as $user){
			if($user["bdayOffer"]==0)
			{
				array_push($rows,["user_id"=>$user["id"],
				"msg"=>yii::$app->params["bdayMsg"],
				"type"=>1,
				"is_read"=>0,
				"is_delete"=>0,
				"created_at"=>time(),
				"updated_at"=>time()
				
				]);
			$uniqueUserNeedToSendPush[$user["device_token"]]=$user["device"];
			array_push($uniqueUserNeedToUpdate,$user["id"]);
				
			}
			else
			array_push($uniqueUserAlreadyUpdated,$user["id"]);
			
			}	
        
		$allIds=array_column($result,"id");
		
			if(count($uniqueUserNeedToUpdate))
		yii::$app->db->createCommand(sprintf("update %s set bdayOffer=1 where user_type=3 and id in (%s);",		
		Customer::tableName(),implode($uniqueUserNeedToUpdate,',')
		))->execute();
		else
		array_push($allIds,0);
		
		yii::$app->db->createCommand(sprintf("update %s set bdayOffer=0 where user_type=3 and bdayOffer=1 and id not in(%s);",
		Customer::tableName(),implode($allIds,',')
		))->execute();
		
		
		if(count($rows))
		Yii::$app->db->createCommand()->batchInsert(Notification::tableName(), array_keys($rows[0]), $rows)->execute(); 
		return yii::$app->pushNotification->send($uniqueUserNeedToSendPush,["msg"=>yii::$app->params["bdayMsg"]]);
	   
	   
	   
	   
	   
	}
	public function actionSubfees(){
		$month = date('n');
		$lastDay = date('t');
		$currentDayOfMonth = date('j');
		$condition =  'subscriptionDueDate='.$currentDayOfMonth;
		if($month==2 && $lastDay==$currentDayOfMonth){
			$condition = 'subscriptionDueDate>='.$currentDayOfMonth;
		}
		$fullDate =  date('m-Y');
		$existing = \common\models\SubscriptionCharges::find()
					->select(['partner_id','from_unixtime(created_at,"%m-%Y") as created'])
					->having("created='$fullDate'")
					->asArray()->all();

		$getPartNers =  \common\models\Profile::find()
						->select(['user_id','subscriptionFees','subscriptionDueDate'])
						->innerJoinWith('user',false)
						->where('user_type=7');
		if(count($existing)>0){
			$pIds = array_column($existing,'partner_id');
			$getPartNers->andWhere(['NOT IN','user_id',$pIds]);
		}
		
		$getPartNers =  $getPartNers->groupBy('user_id')->all();		
		foreach($getPartNers as $_data){
			$subCharges =  new \common\models\SubscriptionCharges();
			
			if($_data->subscriptionFees!='' && $_data->subscriptionDueDate!=''){
				$subCharges->amount = $_data->subscriptionFees;
				$dueDate = strtotime($_data->subscriptionDueDate.'-'.$fullDate);			
				$subCharges->due_date = $dueDate;		
			} else {
				$setting = \common\models\Setting::getSubFeeDate();	
				$subCharges->amount = $setting['subscription_fees'];
				$dueDate = strtotime($setting['subscription_day'].'-'.$fullDate);	$subCharges->due_date = $dueDate;					
			}
			$subCharges->partner_id = $_data->user_id;			
			$subCharges->payment_status = 0;				
			$subCharges->save();
			
		}
		
		
	}
	
	public function actionMonthlysubfee(){
		$time = new \DateTime('now');
		$today = $time->format('Y-m-d');
		//print_r($today);die;
		$_amounts = \common\models\SubscriptionCharges::find()->joinWith('partner')->where(['payment_status'=>0])->andWhere(['=', 'FROM_UNIXTIME(due_date,"%Y-%m-%d")',$today])->all();
		
		foreach($_amounts as $amount){
			//print_r($amount);die;
			 $partnerID = $amount->partner_id;
			 $accDetails = $this->accdetail($partnerID,$amount->partner->subscriptionPay);
		
			$getAmount = $amount->amount !='' ? $amount->amount : 0;
			$attributes=['amount','account','acc_type'];
			  $model=new \yii\base\DynamicModel($attributes);
			  $model->addRule(['amount','account'],'required');
			  $model->addRule(['acc_type'],'safe');
			   $model->amount=$getAmount;
			   $model->account=$accDetails['0'];
			   $model->acc_type = $amount->partner->subscriptionPay;
			   if($model->validate()){
				  
			   $date = date('m-Y');
				$userName = $amount->partner->user->username;
				$name = "Subscription charges- $userName";
			  
			  	if($model->acc_type==1){
				$res = Yii::$app->promisePay->createAuthority($model->account,$model->amount);
			 
					}
					$payment=Yii::$app->promisePay->createCharges($name,$model->account,$model->amount,$amount->partner->user->email);
					
        if(is_string($payment)){
          $break = explode(':',$payment);
          \Yii::$app->getSession()->setFlash('error',end($break));
         // return $this->render('paynow',['model'=>$model,'acc'=>$accDetails]);
        }
        $amount->payment_status = 1;
        $amount->account_id = $model->account;
        $amount->detail = json_encode($payment);
       // $amount->updated_by = Yii::$app->user->identity->id;
		if(!empty($payment['id']))
        $amount->transaction_id = $payment['id'];
        $amount->save(false);
      }else{
	  }
		}
      

      die("ok");
	}
	
	public function accdetail($id,$type){
		$bankAcc = [];
	$creditCards = [];
	if($type==1){
     $bankAcc = \common\models\Bankdetail::find()->select(['concat("Bank account -  ",account_name) as account_name','promise_acid'])
                  ->where(['partner_id'=>$id])
                  ->andWhere('promise_acid is not null')
                  ->asArray()->all();
                  //print_r($id);die;
			  }else{
    $creditCards=   \common\models\Paymentcard::find()->select(['concat("Credit card - ",cardholderName) as account_name','uniqueNumberIdentifier as promise_acid'])
        ->where(['user_id'=>$id])
        ->andWhere('uniqueNumberIdentifier is not null')
        ->andWhere("issuingBank='promisePay'")
        ->asArray()->all();
	}
     $allAcs = array_merge($bankAcc,$creditCards);
     
      if(count($allAcs)>0){
          $key = array_column($allAcs,'promise_acid');
          $val = array_column($allAcs,'account_name');
          return array_merge($key,$val);
      }
      return false;
    }
}


