<?php

namespace api\modules\v1\controllers;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\components\Controller;
use common\models\States;
use common\models\City;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use api\modules\v1\models\CviewState;
/**
 * Country Controller API
 *
 *
 */
class StateController extends Controller
{
    public $modelClass = 'common\models\States';    


   public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['view'],$actions['index'],$actions['delete']);                    

		return $actions;
	}

	/**
	* Get all states of a particular country
	* - URI: *api/web/v1/state/
	*
	* @api {get} api/web/v1/state?id=13 Show All States of a particular country
	*
	* @apiName Show All States of a particular country
	* @apiVersion 0.1.1
	* @apiGroup Geo
	* @apiDescription To show all states of a particular country.
	*
	* @apiSuccessExample Success-Response:
	*    HTTP/1.1 200 OK
	*    {
	*        "message": "",
	*        "data": [
	*            {
	*                "id": 246,
	*                "name": "Australian Capital Territory",
	*                "country_id": 13
	*            },
	*            {
	*                "id": 266,
	*                "name": "New South Wales",
	*                "country_id": 13
	*            },
	*            {
	*                "id": 267,
	*                "name": "Northern Territory",
	*                "country_id": 13
	*            },
	*            {
	*                "id": 269,
	*                "name": "Queensland",
	*                "country_id": 13
	*            },
	*            {
	*                "id": 273,
	*                "name": "Victoria",
	*                "country_id": 13
	*            },
	*            {
	*                "id": 275,
	*                "name": "Western Australia",
	*                "country_id": 13
	*            }
	*        ],
	*        "status": 200
	*    }
	*/  	
    public function actionIndex($id)
	{
		
		/* get state according to country id */
		return new ActiveDataProvider([ 
            'query' => States::find()->where(['country_id' => $id]),
            'pagination' => [
			    'pageSize' => -1,
		    ], 					 
       			
        ]);
		 
	}

	/** 
	* 
	* Get all states
	* - URI: *api/web/v1/state/all-state
	*
	* @api {get} api/web/v1/state/all-state Show All States
	*
	* @apiName Show All States
	* @apiVersion 0.1.1
	* @apiGroup Geo
	* @apiDescription To show all states.
	*
	* @apiSuccessExample Success-Response:
	*    HTTP/1.1 200 OK
	*    {
	*        "message": "",
	*        "data": [
	*            {
	*                "id": 1,
	*                "name": "Victoria"
	*            },
	*            {
	*                "id": 2,
	*                "name": "New South Wales"
	*            },
	*            {
	*                "id": 3,
	*                "name": "Queensland"
	*            },
	*            {
	*                "id": 4,
	*                "name": "South Australia"
	*            },
	*            {
	*                "id": 5,
	*                "name": "Tasmania"
	*            },
	*            {
	*                "id": 6,
	*                "name": "Western Australia"
	*            },
	*            {
	*                "id": 7,
	*                "name": "Australian Capital Territory"
	*            },
	*            {
	*                "id": 8,
	*                "name": "Northern Territory"
	*            }
	*        ],
	*        "status": 200
	*    }
	*/
	public function actionAllState()
	{	
        return new ActiveDataProvider([ 
            'query' => CviewState::find(),					 
       			
        ]);
		 
	}

	/** 
	* 
	* Get all states
	* - URI: *api/web/v1/state/get-state?id=246
	*
	* @api {get} api/web/v1/state/get-state?id=246 Get State details by id
	*
	* @apiName Get State by id
	* @apiVersion 0.1.1
	* @apiGroup Geo
	* @apiDescription To get a state by id.
	*
	* @apiSuccessExample Success-Response:
	*    HTTP/1.1 200 OK
	*    {
	*        "message": "",
	*        "data": [
	*            {
	*                "id": 1,
	*                "name": "Australian Capital Territory",
	*                "country_id": 13,
	*            },
	*        ],
	*        "status": 200
	*    }
	*/
    public function actionGetState($id)
    {
        return new ActiveDataProvider([
            'query' => States::find()->where(['id' => $id])
        ]);
    }
	
    public function actionAllCity()
	{
		/* get state according to country id */
		   return new ActiveDataProvider([ 
            'query' => City::find(),		
        ]);	 
	}
    
}
