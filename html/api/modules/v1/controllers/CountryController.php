<?php
/**
 * @link https://somelink.com
 * @copyright Copyright (c) 2017 Some amazing copyright co.
 * @license https://github.com/newscloud/mp/blob/master/LICENSE
 */
namespace api\modules\v1\controllers;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\components\Controller;
use common\models\Country;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * CountryController is the base class that provides the features related to countries.
 *
 *
 * @author GraycellTech <info@graycelltech.com>
 * @since 1.0
 */
class CountryController extends Controller
{
    public $modelClass = 'common\models\Country';

    /**
     * List of supported actions associated with this controller.
     *
     * @return json
     */
    public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['view'],$actions['index'],$actions['delete']);

		return $actions;
	}

	/**
	* Returns all of the countries available in the DB.
	*
	* @api {get} api/web/v1/country/index GeoCountry
	* @apiName GetCountries
	* @apiVersion 0.1.1
	* @apiGroup Geo
	* @apiDescription Returns all of the countries available in the DB.
	*
	* @apiSuccessExample Success-Response:
	*    HTTP/1.1 201 Created
	*    {
	*        "message": "",
	*        "data": [
	*            {
	*                "id_countries": 1,
	*                "sortname": "AF",
	*                "name": "Afghanistan",
	*                "iso_code": "AFG"
	*            },
	*            {
	*                "id_countries": 2,
	*                "sortname": "AL",
	*                "name": "Albania",
	*                "iso_code": "ALB"
	*            },
	*        ],
	*        "status": 201
	*    }
	*
	* @return Object(common\models\Country)
	*/
    public function actionIndex()
    {
      return new ActiveDataProvider([
        'query' => Country::find(),
        'pagination' => [
          'pageSize' => -1,
        ],
      ]);
    }

}
