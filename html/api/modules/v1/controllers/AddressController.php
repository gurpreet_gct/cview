<?php
namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use common\models\Address;
use common\models\Cardtype;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


				 

class AddressController extends Controller
{    
        public $modelClass = 'common\models\Address';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
	public function actions()
	{
		$actions = parent::actions();
		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index']);    

		return $actions;
	}
 
  	
   /* create new address */    
    public function actionCreate(){
		$user_id=Yii::$app->user->id; 		
		$model= new Address();			
		$model->load(Yii::$app->getRequest()->getBodyParams(), ''); 
		
		$model->created_by=$user_id;
		$model->updated_by=$user_id;
		$model->user_id=$user_id;
			if($model->validate()){
				$Oldshipping= Address::find()->where(['user_id' => $user_id,'is_shipping'=>1])->orderBy("id desc")->one();				
				$Oldbilling= Address::find()->where(['user_id' => $user_id,'is_billing'=>1])->orderBy("id desc")->one();
				
				/* check if old shipping is 1*/
			    if(!empty($Oldshipping)){
					if($model->is_shipping==1){						
						$Oldshipping->is_shipping=0;
						$Oldshipping->save(false);
					}					
				}
				/* check if old billing is 1*/
				if(!empty($Oldbilling)){
					if($model->is_billing==1){	
						$Oldbilling->is_billing=0;
						$Oldbilling->save(false);
					}
				}
				/* save data */
				$model->save();
			}
         
		return $model;
   }
    
    /* update address by address id */
    public function actionUpdate($id){		
        $user_id=Yii::$app->user->id;
        $connection = Yii::$app->db;		
		$model= new Address();
		$model= Address::findOne(['id' => $id]);
				
		$model->load(Yii::$app->getRequest()->getBodyParams(), '');  
		 if($model->validate()){
			
					if($model->is_shipping==1){									  
				    $connection->createCommand()
					->update(Address::tableName(),['is_shipping' => 0],['user_id' => $user_id, 'is_shipping' => 1])->execute();
				    }			
				
					if($model->is_billing==1){						
					 $connection->createCommand()
					->update(Address::tableName(),['is_billing' => 0],['user_id' => $user_id, 'is_billing' => 1])->execute();
					}
			
			   
				$model->updated_by=$user_id;
				$model->save();		
		}
		return $model;       
    }    
      	  
   
    
   /*get all address detail by login user id */
   public function actionIndex(){
	   	 
         return new ActiveDataProvider([ 
            'query' => Address::find()	
            ->joinWith('addresstype')				 
            ->joinWith('country')				 
            ->where(['user_id'=>Yii::$app->user->id])       			
        ]);            
   }
   
   /*get all shipping address detail by login user id */
   public function actionShippingaddress(){
	   
	 return new ActiveDataProvider([ 
            'query' => Address::find()	
            ->joinWith('addresstype')				 
            ->joinWith('country')				 
            ->where(['user_id'=>Yii::$app->user->id,'is_shipping'=>1])       			
        ]);         
   }
   
   /*get all billing address detail by login user id */
   public function actionBillingaddress(){
	   
	 return new ActiveDataProvider([ 
            'query' => Address::find()	
            ->joinWith('addresstype')				 
            ->joinWith('country')				 
            ->where(['user_id'=>Yii::$app->user->id,'is_billing'=>1])       			
        ]);         
   }
   
   /* get shipping and billing api combined */
   
     public function actionShippingbilling(){
	   
	 $shipping = new ActiveDataProvider([ 
            'query' => Address::find()	
            ->joinWith('addresstype')				 
            ->joinWith('country')				 
            ->where(['user_id'=>Yii::$app->user->id])  
            ->andWhere(['or',['is_billing'=>1],['is_shipping'=>1]])       			
        ]); 
              
        return $shipping;
         
   }
   
   
   /* delete */
  /* public function actionDelete($id)
    {
        $data = Address::findOne(['id' => $id])->delete();

        return $data;
    }
    */ 
    
   
   
   
   
   
	
}
