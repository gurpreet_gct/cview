<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;
use api\modules\v1\models\Foodproductapi;
use yii\db\Query;
use yii\db\Expression;
use api\modules\v1\models\Productapi;
use common\models\Profile;
use common\models\FoodcategoryProducts;
use common\models\Userfoodcategory;
use common\models\ProductOption;
/**
 * FoodproductController Controller API
 *
 */
class FoodproductController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Foodproductapi';    

	
		
	     public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}	
	public function actions()
	{
		$actions = parent::actions();
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);
		return $actions;
	}
	public function actionIndex(){
		$userId = Yii::$app->user->identity->id;
		$brand = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/profile').'/';		
		$image = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/uploads/catalog').'/';
		$thumb = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/uploads/catalog/thumb').'/';
		$storeId = !empty($_GET['storeid'])? $_GET['storeid']:'NULL';
		if(!empty($_GET['pid']))
		{
			return $this->getProductChild($_GET['pid']);
		}
		else{
			$query = new Query;
			/* get child's category */
			$query	->select([Foodproductapi::tableName().'.id as id','name','icon','imageurl', 'lvl','root','lft'])  
				->from(Foodproductapi::tableName())
				->rightJoin(Userfoodcategory::tableName(),Userfoodcategory::tableName().'.category_id ='.Foodproductapi::tableName().'.id and '.Userfoodcategory::tableName().'.store_id='.$storeId)
				->where(['lvl'=>1])
				->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC]); 	
			$command2 = $query->createCommand();
			$data2 = $command2->queryAll();
			$pdata = array();
			foreach($data2 as $p_cat_data2){
				$levelzero2 = array();
				$levelzero2['cat_id'] = $p_cat_data2['id'];
				$levelzero2['name'] = $p_cat_data2['name'];
				$levelzero2['icon'] = $p_cat_data2['icon'];					
				/* get category's products */
				$sqlProduct=sprintf("SELECT p.id as product_id,p.p_type,p.name,p.sku,p.store_id,p.description,p.urlkey,p.price,p.status,p.is_online,image,CONCAT('%s/',image) as image,CONCAT('%s/',image) as thumb, qty,is_added,b.companyName as brand, CONCAT('%s/',b.brandLogo) as logo FROM %s p INNER JOIN %s c on p.id = c.product_id AND c.category_id =%s INNER JOIN  %s b on p.store_id=b.user_id where p.p_type IN(2,3) AND p.store_id=%s;",$image,$thumb,$brand,Productapi::tableName(),FoodcategoryProducts::tableName(),$p_cat_data2['id'],Profile::tableName(),$storeId);
					$product=Yii::$app->db->createCommand($sqlProduct)->queryAll();
					$pdata2 = array();
					$levelzero2['product'] = $product;	
					$pdata[] = $levelzero2;	
			}

			return array('items'=>$pdata);
		}
	}
public function getProductChild($id){
	$brand = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/profile').'/';		
	$image = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/uploads/catalog').'/';
	$thumb = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/uploads/catalog/thumb').'/';
	$p = Productapi::tableName();
	$b = Profile::tableName(); 
		$_product = Productapi::find()->select(["$p.id as product_id","$p.p_type","$p.name","$p.sku","$p.store_id","$p.description","$p.urlkey","$p.price","$p.status","$p.is_online","CONCAT('$image',$p.image) as image","CONCAT('$thumb',$p.image) as thumb","$p.has_option","$p.related_product",new Expression('0 as is_added'),"$p.qty","$b.companyName as brand","CONCAT('$brand',$b.brandLogo) as logo"])	
			->joinWith('profile',false,'INNER JOIN')
			->where("$p.id=".$id)
			->asArray()
			->one();	
		$subProduct = array();
		if($_product['related_product']){
		$ids = explode(',',$_product['related_product']);
		$subProduct = Productapi::find()->select(["$p.id as product_id","$p.p_type","$p.name","$p.sku","$p.store_id","$p.description","$p.urlkey","$p.price","$p.status","$p.is_online","CONCAT('$image',$p.image) as image","CONCAT('$thumb',$p.image) as thumb","$p.related_product",new Expression('0 as is_added'),"$p.qty"])
			->where(['IN', 'id',$ids])
			->asArray()
			->all();				
		}
		$_product['child']=$subProduct;
		$pOptions = array();
		if($_product['has_option']==1){
			$pOptions = ProductOption::find()
				->select(['*',ProductOption::tableName().'.is_included as is_added',new Expression('0 as qty')])
				->where('product_id='.$_product['product_id'])
				->orderBy('is_included DESC')
				->asArray()
				->all();			
		}
		$_product['options']=$pOptions;
		// return array('item'=>array($_product));				
		return $_product;				
	}

}


