<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\RegisterBid;
use api\modules\v1\models\Profiles;
use api\modules\v1\models\AccountVerification;
use api\modules\v1\models\ApplyBid;
use common\models\User;
use common\models\Property;
use common\models\Auction;
use common\models\Bidding;
use common\models\CviewConfig;
use api\components\Controller;
use common\models\CommonModel;
use common\models\Currencies;

/**
 * BiddingController
 *
 * Save|Update Register Bid management methods for APIs are defined here.
 */
class BiddingController extends Controller
{
    public $modelClass = 'api\modules\v1\models\Bidding';
    
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    { 
        $behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
            'except' => ['subfees'],
            'only'=>[
                'get-current-bid',
                'auction-details',
                'submitbid',
                'place-bid',
              //  'update-bid-status',
            ],            
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "update" actions
        unset($actions['delete'], $actions['index'], $actions['update'],$actions['create']);

        return $actions;
    }

    /** 
	 * Listing Bidders for an Auction
	 * @api {post} api/web/v1/bidding/list-bidders List Bidders for an Auction
	 * @apiName List Bidders for an Auction
	 * @apiVersion 0.1.1
	 * @apiGroup Bidding
	 * @apiDescription List Bidders for an Auction
	 *
	 * @apiParam {Number} auction_id 'auction id' to get the information about the respective bidders.
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 
     *    {
     *      "message": "",
     *      "data": {
     *        "items": [
     *          {
     *            "id": 9,
     *            "user_id": 401,
     *            "auction_id": 3,
     *            "mobile": "9569635775",
     *            "otp": "12323",
     *            "solicitor_firstname": "solicitor_firstname",
     *            "solicitor_lastname": "solicitor_lastname",
     *            "solicitor_email": "solicitor_email",
     *            "solicitor_mobile": "7018029728",
     *            "bidding_panel_number": "232332",
     *            "bidding_colour": 0,
     *            "guarantor_first_name": null,
     *            "guarantor_lastname": null,
     *            "guarantor_email": null,
     *            "bidderDetail": {
     *              "app_id": "2",
     *              "id": 401,
     *              "username": "bidder_user13",
     *              "email": "bidder_user13@yopmail.com",
     *              "auth_key": "2l1FEGn3g7_yz59VTuGWzZ_ysmKe7qO0",
     *              "firstName": "Bidder",
     *              "lastName": "User",
     *              "fullName": "Bidder User",
     *              "fbidentifier": null,
     *              "linkedin_identifier": null,
     *              "google_identifier": null,
     *              "phone": null,
     *              "image": null,
     *              "sex": null,
     *              "dob": "1999-05-25",
     *              "ageGroup_id": 1,
     *              "bdayOffer": 0,
     *              "walletPinCode": null,
     *              "loyaltyPin": null,
     *              "advertister_id": null,
     *              "device": "android",
     *              "device_token": "",
     *              "latitude": null,
     *              "longitude": null,
     *              "timezone": null,
     *              "timezone_offset": null,
     *              "storeid": null,
     *              "promise_uid": "4015afe96577208d",
     *              "promise_acid": null,
     *              "user_code": "AW-bidder_user13",
     *              "referralCode": null,
     *              "referral_percentage": null,
     *              "agency_id": null
     *            }
     *          },
     *          {
     *            "id": 33,
     *            "user_id": 383,
     *            "auction_id": 3,
     *            "mobile": "9569635775",
     *            "otp": "12323",
     *            "solicitor_firstname": "solicitor_firstname",
     *            "solicitor_lastname": "solicitor_lastname",
     *            "solicitor_email": "solicitor_email",
     *            "solicitor_mobile": "7018029728",
     *            "bidding_panel_number": "232332",
     *            "bidding_colour": 12,
     *            "guarantor_first_name": "guarantor_first_name",
     *            "guarantor_lastname": "guarantor_last_name",
     *            "guarantor_email": "guarantor_email",
     *            "bidderDetail": {
     *              "app_id": "2",
     *              "id": 383,
     *              "username": "soodvivek",
     *              "email": "viveks@graycelltech.com",
     *              "auth_key": "bzeIhiWdQ6ctsvRFODNZBwSuJWCjEiHL",
     *              "firstName": "Vivek",
     *              "lastName": "Sood",
     *              "fullName": "Vivek Sood",
     *              "fbidentifier": null,
     *              "linkedin_identifier": null,
     *              "google_identifier": null,
     *              "phone": null,
     *              "image": null,
     *              "sex": null,
     *              "dob": "1992-05-09",
     *              "ageGroup_id": 1,
     *              "bdayOffer": 0,
     *              "walletPinCode": null,
     *              "loyaltyPin": null,
     *              "advertister_id": null,
     *              "device": "android",
     *              "device_token": "",
     *              "latitude": null,
     *              "longitude": null,
     *              "timezone": null,
     *              "timezone_offset": null,
     *              "storeid": null,
     *              "promise_uid": "3835afd5d9d0e593",
     *              "promise_acid": null,
     *              "user_code": "AW-soodvivek",
     *              "referralCode": null,
     *              "referral_percentage": null,
     *              "agency_id": null
     *            }
     *          },
     *          {
     *            "id": 34,
     *            "user_id": 351,
     *            "auction_id": 3,
     *            "mobile": null,
     *            "otp": null,
     *            "solicitor_firstname": null,
     *            "solicitor_lastname": null,
     *            "solicitor_email": null,
     *            "solicitor_mobile": null,
     *            "bidding_panel_number": "ad123",
     *            "bidding_colour": 0,
     *            "guarantor_first_name": null,
     *            "guarantor_lastname": null,
     *            "guarantor_email": null,
     *            "bidderDetail": {
     *              "app_id": "2",
     *              "id": 351,
     *              "username": "gurpreet.gct",
     *              "email": "gurpreet@graycelltech.com",
     *              "auth_key": "1XQDPa2FFphutfDKT7RHLFvHwU7bVIVP",
     *              "firstName": "GPreet",
     *              "lastName": "Singh",
     *              "fullName": "GPreet Singh",
     *              "fbidentifier": null,
     *              "linkedin_identifier": "123qweasdcxz",
     *              "google_identifier": null,
     *              "phone": null,
     *              "image": null,
     *              "sex": null,
     *              "dob": "1970-01-01",
     *              "ageGroup_id": 1,
     *              "bdayOffer": 0,
     *              "walletPinCode": null,
     *              "loyaltyPin": null,
     *              "advertister_id": null,
     *              "device": "android",
     *              "device_token": "",
     *              "latitude": null,
     *              "longitude": null,
     *              "timezone": null,
     *              "timezone_offset": null,
     *              "storeid": null,
     *              "promise_uid": "3085abb78c79ebb4",
     *              "promise_acid": null,
     *              "user_code": "AW-gurpreet.gct",
     *              "referralCode": null,
     *              "referral_percentage": null,
     *              "agency_id": null
     *            }
     *          }
     *        ],
     *        "_links": {
     *          "self": {
     *            "href": "http://localhost/AlphaWallet/html/api/web/v1/bidding/list-bidders?page=1"
     *          }
     *        },
     *        "_meta": {
     *          "totalCount": 3,
     *          "pageCount": 1,
     *          "currentPage": 1,
     *          "perPage": 20
     *        }
     *      },
     *      "status": 200
     *    }
	 *
	 * @apiSuccessExample Error No Record Found
	 *    HTTP/1.1 422 Unprocessable entity	  	 
     *    {
	 *      "message": "No Record Found",
	 *      "data": {
	 *        "status": "error"
	 *      },
	 *      "status": 422
	 *    }
	 *    
	 * @return Object(common\models\Bidding)
	 */
    public function actionListBidders() {
        if(empty(Yii::$app->request->post()['auction_id'])) {
            return ["Required 'auction_id' can't be blank"];
        } 
        $id = Yii::$app->request->post()['auction_id'];
        $userIds = $this->getVerifyBidder($id);
        $data = RegisterBid::find()->where(['user_id' => $userIds, 'auction_id' => $id]);
       // echo '<pre>'; print_r($data); die('her');
        if(!empty($data->where['user_id'])){
            return new ActiveDataProvider([
                'query' => $data,
            ]);
        }else{
            $msg = CommonModel::textGlobalization('app', 'no_record');
            return ['status' => 'error', 'message' => $msg, 'statusCode' => 422];            
        }
    }
    
    /** 
	 * List bids excluding with status rejected
	 * - URI: *api/web/v1/bidding/auction-bids
	 *
	 * @api {get} api/web/v1/bidding/auction-bids Bids Listing
	 * @apiName List bids for an auction
	 * @apiVersion 0.1.1
	 * @apiGroup Bidding
	 * @apiDescription List bids for an auction excluding bids with status rejected
	 *
	 * @apiParam {Number} auction_id listing id to get the information about the respective bidders.
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 
     *    {
     *      "message": "",
     *      "data": [
     *         {
     *           "id": 14,
     *           "auction_id": 3,
     *           "bidder_id": 401,
     *           "is_vendor": 0,
     *           "amount": "1000.00",
     *           "status": 1,
     *           "bid_won": 0,
     *           "complete_status": 0,
     *           "turn_on_off": 0,
     *           "play_pause": 0,
     *           "play_status": 0,
     *           "lastbid": "0.00",
     *           "cancel": 0,
     *           "property_in_market": 0,
     *           "created_at": "2018-05-28 09:58:07",
     *           "updated_at": "2018-05-28 09:58:07",
     *           "created_by": 401,
     *           "updated_by": 401,
     *           "statusText": "Accepted",
     *           "bidderDetail": {
     *             "id": 401,
     *             "username": "bidder_user13",
     *             "email": "bidder_user13@yopmail.com",
     *             "user_type": 3,
     *             "app_id": "2",
     *             "auth_key": "D7O68qobgVgkMsHMZ2fjZqizBE7SemaG",
     *             "firstName": "Bidder",
     *             "lastName": "User",
     *             "fullName": "Bidder User",
     *             "orgName": null,
     *             "fbidentifier": null,
     *             "linkedin_identifier": null,
     *             "google_identifier": null,
     *             "role_id": null,
     *             "password_hash": "$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC",
     *             "password_reset_token": null,
     *             "phone": null,
     *             "image": null,
     *             "sex": null,
     *             "dob": "1999-05-25",
     *             "ageGroup_id": 1,
     *             "bdayOffer": 0,
     *             "walletPinCode": null,
     *             "loyaltyPin": null,
     *             "advertister_id": null,
     *             "activationKey": null,
     *             "confirmationKey": null,
     *             "access_token": null,
     *             "hashKey": null,
     *             "status": 10,
     *             "device": "android",
     *             "device_token": "",
     *             "lastLogin": 1527762172,
     *             "latitude": null,
     *             "longitude": null,
     *             "timezone": null,
     *             "timezone_offset": null,
     *             "created_at": 1526634067,
     *             "updated_at": 1527762172,
     *             "storeid": null,
     *             "promise_uid": "4015afe96577208d",
     *             "promise_acid": null,
     *             "user_code": "AW-bidder_user13",
     *             "referralCode": null,
     *             "referral_percentage": null,
     *             "agency_id": null
     *           }
     *         },
     *         {
     *           "id": 15,
     *           "auction_id": 3,
     *           "bidder_id": 401,
     *           "is_vendor": 0,
     *           "amount": "1800.00",
     *           "status": 0,
     *           "bid_won": 0,
     *           "complete_status": 0,
     *           "turn_on_off": 0,
     *           "play_pause": 0,
     *           "play_status": 0,
     *           "lastbid": "0.00",
     *           "cancel": 0,
     *           "property_in_market": 0,
     *           "created_at": "2018-05-28 09:58:07",
     *           "updated_at": "2018-05-28 09:58:07",
     *           "created_by": 401,
     *           "updated_by": 401,
     *           "statusText": "Pending",
     *           "bidderDetail": {
     *             "id": 401,
     *             "username": "bidder_user13",
     *             "email": "bidder_user13@yopmail.com",
     *             "user_type": 3,
     *             "app_id": "2",
     *             "auth_key": "D7O68qobgVgkMsHMZ2fjZqizBE7SemaG",
     *             "firstName": "Bidder",
     *             "lastName": "User",
     *             "fullName": "Bidder User",
     *             "orgName": null,
     *             "fbidentifier": null,
     *             "linkedin_identifier": null,
     *             "google_identifier": null,
     *             "role_id": null,
     *             "password_hash": "$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC",
     *             "password_reset_token": null,
     *             "phone": null,
     *             "image": null,
     *             "sex": null,
     *             "dob": "1999-05-25",
     *             "ageGroup_id": 1,
     *             "bdayOffer": 0,
     *             "walletPinCode": null,
     *             "loyaltyPin": null,
     *             "advertister_id": null,
     *             "activationKey": null,
     *             "confirmationKey": null,
     *             "access_token": null,
     *             "hashKey": null,
     *             "status": 10,
     *             "device": "android",
     *             "device_token": "",
     *             "lastLogin": 1527762172,
     *             "latitude": null,
     *             "longitude": null,
     *             "timezone": null,
     *             "timezone_offset": null,
     *             "created_at": 1526634067,
     *             "updated_at": 1527762172,
     *             "storeid": null,
     *             "promise_uid": "4015afe96577208d",
     *             "promise_acid": null,
     *             "user_code": "AW-bidder_user13",
     *             "referralCode": null,
     *             "referral_percentage": null,
     *             "agency_id": null
     *           }
     *         }
     *      ],
     *      "status": 200
     *    }
	 *
	 * @apiErrorExample Error-Response:
	 *    HTTP/1.1 401 Unauthorized
	 *    {
     *      "name": "Unauthorized",
     *    	"message": "Your request was made with invalid credentials.",
     *    	"code": 0,
     *    	"status": 401,
     *    	"type": "yii\\web\\UnauthorizedHttpException"
     *    }
	 *
	 * @apiSuccessExample Error No Record Found
	 *    HTTP/1.1 422 Unprocessable entity	  	 
     *    {
	 *      "message": "No Record Found",
	 *      "data": {
	 *        "status": "error"
	 *      },
	 *      "status": 422
	 *    }
	 *    
	 * @return Object(common\models\Bidding)
	 */    
    public function actionAuctionBids(){
        $posted = Yii::$app->request->post();
        if (empty($posted['auction_id'])) {
            return ["Required 'auction_id' can't be blank"];
        }
        $bidding = new Bidding();
        $auction = $bidding->getAll($posted['auction_id']);
        if(empty($auction)){
            $msg = CommonModel::textGlobalization('app', 'no_record');
            return ['status' => 'error', 'message' => $msg, 'statusCode' => 422];            
        }
        return $auction;
    }

    public function actionListBidders_old()
    {
        if (empty(Yii::$app->request->post()['listing_id'])) {
            return ["Required listing_id can't be blank"];
        } 

        $listing_id = Yii::$app->request->post()['listing_id'];
        $bidding = new Bidding();
        $auction = $bidding->getAll($listing_id);
        
        return $auction;
    }

	public function actionGetCurrentBid()
    {
        if (empty(Yii::$app->request->post()['listing_id'])) {
            return ["Required listing_id can't be blank"];
        }

        $listing_id = Yii::$app->request->post()['listing_id'];
        $bidding = new Bidding();
        $auction = $bidding->getCurrentBid($listing_id);
        
        return $auction;
    }

    public function getVerifyBidder($auctionId) {
        $query = new Query;
        $query->select('GROUP_CONCAT(tbl_register_bid.user_id) as userIds')
            ->from('tbl_register_bid')
            ->leftJoin('tbl_account_verification av', 'av.register_bid_id=tbl_register_bid.id')
            ->where(['tbl_register_bid.auction_id' => $auctionId,'av.complete' => 1])
            ->groupBy('tbl_register_bid.auction_id');
        $command = $query->createCommand();
        $data = $command->queryOne();
        return (isset($data['userIds']) ? explode(",", $data['userIds']) : '');
    }

    public function convertCurrency($currency_from,$currency_to,$convert_amount) {
        $currency_from      = urlencode($currency_from);
        $currency_to        = urlencode($currency_to); 
        $convert_details    = file_get_contents("https://finance.google.com/bctzjpnsun/converter?a=1&from=$currency_from&to=$currency_to");

        $convert_details    = explode("<span class=bld>",$convert_details);
        $convert_details    = explode("</span>",$convert_details[1]);
        $conversion_rate    = preg_replace("/[^0-9\.]/", null, $convert_details[0]);
        $total_converted_currency_amount = $convert_amount*$conversion_rate;
        return $total_converted_currency_amount;
    }
/*
    public function actionDemo() {
        $amount         = 100.00;
        $from_currency  = "USD";
        $to_currency    = "INR";
        return $this->convertCurrency($from_currency, $to_currency, $amount);    
    }

    public funtion actionClosedAuction() {
        ##completed/cancelled/pause/streaming
        if (empty(Yii::$app->request->post()['auction_id'])) {
            return ["Required auction_id can't be blank"];
        }

        $auction_id = Yii::$app->request->post()['auction_id'];
        $data    = Auction::find()->where(['id' =>$auction_id]);
        return new ActiveDataProvider([
            'query' => $data,
        ]);
    }
*/

    /**
    * To Submit bid
    * - URI: *api/web/v1/bidding/submitbid
    * @api {post} /api/web/v1/bidding/submitbid Submit bid
    * @apiName Submitbid
    * @apiVersion 0.1.1
    * @apiGroup Bidding
    * @apiDescription To Submit bid
    * 
    * @apiParam {Number} amount (bid amount).
    * @apiParam {Number} auction_id Auction Id
    * 
    * @apiSuccessExample Success-Response:
    *  HTTP/1.1 200 
    *   {
    *        "message": "",
    *        "data": {
    *            "amount": "1000",
    *            "auction_id": "3",
    *            "status": "1",
    *            "bidder_id": 401,
    *            "updated_at": 1527500831,
    *            "updated_by": 401,
    *            "created_at": 1527500831,
    *            "created_by": 401,
    *            "id": 13,
    *            "bidder": {
    *                "id": 401,
    *                "username": "bidder_user13",
    *                "email": "bidder_user13@yopmail.com",
    *                "user_type": 3,
    *                "app_id": "2",
    *                "auth_key": "4EdhQ2YlhGwgbrPMzqTNfdgQupPgY8OP",
    *                "firstName": "Bidder",
    *                "lastName": "User",
    *                "fullName": "Bidder User",
    *                "orgName": null,
    *                "fbidentifier": null,
    *                "linkedin_identifier": null,
    *                "google_identifier": null,
    *                "role_id": null,
    *                "password_hash": "$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC",
    *                "password_reset_token": null,
    *                "phone": null,
    *                "image": null,
    *                "sex": null,
    *                "dob": "1999-05-25",
    *                "ageGroup_id": 1,
    *                "bdayOffer": 0,
    *                "walletPinCode": null,
    *                "loyaltyPin": null,
    *                "advertister_id": null,
    *                "activationKey": null,
    *                "confirmationKey": null,
    *                "access_token": null,
    *                "hashKey": null,
    *                "status": 10,
    *                "device": "android",
    *                "device_token": "",
    *                "lastLogin": 1527485100,
    *                "latitude": null,
    *                "longitude": null,
    *                "timezone": null,
    *                "timezone_offset": null,
    *                "created_at": 1526634067,
    *                "updated_at": 1527485100,
    *                "storeid": null,
    *                "promise_uid": "4015afe96577208d",
    *                "promise_acid": null,
    *                "user_code": "AW-bidder_user13",
    *                "referralCode": null,
    *                "referral_percentage": null,
    *                "agency_id": null
    *            }
    *        },
    *        "status": 200
    *    }
    *
    * @apiErrorExample Error-Response:
    *    HTTP/1.1 401 Unauthorized
    *    {
    *        "name": "Unauthorized",
    *        "message": "Your request was made with invalid credentials.",
    *        "code": 0,
    *        "status": 401,
    *        "type": "yii\\web\\UnauthorizedHttpException"
    *    }
    *
    * @return Object(common\models\Bidding)
    */      
    public function actionSubmitbid() {
        $authToken =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        $user = User::findIdentityByAccessToken($authToken);
        $formData = Yii::$app->getRequest()->getBodyParams();
        //check for bid registration
        $verifyBidder = $this->getVerifyBidder($formData['auction_id']);
        if(empty($verifyBidder)) {
            return ['You are not verified for bidding.'];
        } elseif(!in_array($user->id, $verifyBidder)) {
            return ['You are not verified for bidding.'];
        }
        //$bidder_amount = $this->convertBidderAmount($formData);
        $formData['user_id'] = $user->id;
        $formData['bidder_id'] = $user->id;
        //$formData['bidder_amount'] = $bidder_amount;
       //$formData['bidder_currency_id'] = $formData['currency_id'];
        $model = new Bidding(['scenario' => 'Savebid']);

        $model->load($formData, '');
        if ($model->validate()) {
            $model->save();
        }
        return $model;
    }

    public function convertBidderAmount($data) {
        $currency     = new Currencies();
        $currencyInfo = $currency->getCurrencyData($data['currency_id']);
        $auction_currency = $this->getAuctionCurrencyData($data['auction_id']);
        $currency_from  = str_replace(' ', '', $currencyInfo['code']);
        $currency_to    = str_replace(' ', '', $auction_currency);
        return $this->convertCurrency($currency_from,$currency_to,$data['amount']);
    }

    public function getAuctionCurrencyData($id) {
        $query = new Query;
        $query->select('tbl_auction_event.auction_currency_id,c.code')
            ->from('tbl_auction_event')
            ->leftJoin('tbl_currencies c', 'c.id=tbl_auction_event.auction_currency_id')
            ->where(['tbl_auction_event.id' => $id]);
        $command = $query->createCommand();
        $data = $command->queryOne();
        return (isset($data['code']) ? $data['code'] : '');
    }

    /**
     * To Place bid
     * - URI: *api/web/v1/bidding/place-bid
     * @api {post} /api/web/v1/bidding/place-bid Place a Bid
     * @apiName To Place Bid
     * @apiVersion 0.1.1
     * @apiGroup Bidding
     * @apiDescription To Place bid
     * 
     * @apiParam {Number} amount (bid amount).
     * @apiParam {Number} auction_id Auction Id
     * @apiParam {Number} bidder_id Bidder Id
     * 
     * @apiSuccessExample Success-Response:
     *  HTTP/1.1 200 
     *  {
     *    "message": "",
     *    "data": {
     *      "auction_id": "3",
     *      "amount": "2202.5",
     *      "bidder_id": 351,
     *      "updated_at": 1527844460,
     *      "updated_by": 351,
     *      "created_at": 1527844460,
     *      "created_by": 351,
     *      "id": 26,
     *      "statusText": "Pending",
     *      "bidderDetail": {
     *        "id": 351,
     *        "username": "gurpreet.gct",
     *        "email": "gurpreet@graycelltech.com",
     *        "user_type": 3,
     *        "app_id": "2",
     *        "auth_key": "kyzgIhegtl5ZoO83tFGmjjWTxElDcBgA",
     *        "firstName": "GPreet",
     *        "lastName": "Singh",
     *        "fullName": "GPreet Singh",
     *        "orgName": null,
     *        "fbidentifier": null,
     *        "linkedin_identifier": "123qweasdcxz",
     *        "google_identifier": null,
     *        "role_id": null,
     *        "password_hash": "$2y$13$nHZiuXex00yoh9dFlJQg3.vfX7UpAP3.pU3UmN8dl0G0kkRt6oDKm",
     *        "password_reset_token": "ejGpGRiP1S-i1RfMjNa4iXIZpIlo4rWu_1526643425",
     *        "phone": null,
     *        "image": null,
     *        "sex": null,
     *        "dob": "1970-01-01",
     *        "ageGroup_id": 1,
     *        "bdayOffer": 0,
     *        "walletPinCode": null,
     *        "loyaltyPin": null,
     *        "advertister_id": null,
     *        "activationKey": null,
     *        "confirmationKey": null,
     *        "access_token": null,
     *        "hashKey": null,
     *        "status": 10,
     *        "device": "android",
     *        "device_token": "",
     *        "lastLogin": 1527841527,
     *        "latitude": null,
     *        "longitude": null,
     *        "timezone": null,
     *        "timezone_offset": null,
     *        "created_at": 1524141463,
     *        "updated_at": 1527841527,
     *        "storeid": null,
     *        "promise_uid": "3085abb78c79ebb4",
     *        "promise_acid": null,
     *        "user_code": "AW-gurpreet.gct",
     *        "referralCode": null,
     *        "referral_percentage": null,
     *        "agency_id": null
     *      }
     *    },
     *    "status": 200
     *  }
     *    
	 * @apiSuccessExample Error-Response:
	 *    HTTP/1.1 422 Unprocessable entity
     *    {
     *      "message": "",
     *      "data": [
     *        {
     *          "field": "amount",
     *          "message": "The minimum bid should be greater than $2,202.00"
     *        }
     *      ],
     *      "status": 422
     *    }  
     *
     * @apiErrorExample Error-Response:
     *    HTTP/1.1 401 Unauthorized
     *    {
     *      "name": "Unauthorized",
     *      "message": "Your request was made with invalid credentials.",
     *      "code": 0,
     *      "status": 401,
     *      "type": "yii\\web\\UnauthorizedHttpException"
     *    }
     *    
     *    @apiSuccessExample Error-Response:
     *    HTTP/1.1 401 Unauthorized
     *    {
     *      "message": "",
     *      "data": [
     *        "You are not verified for this bidding."
     *      ],
     *      "status": 200
     *    }
     *
     * @return Object(common\models\Bidding)
     */    
    public function actionPlaceBid(){
        $authToken =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        $user_id = Yii::$app->MyConstant->UserID($authToken);
        $formData = Yii::$app->getRequest()->getBodyParams();
        
        //check for bid registration
        $verifyBidder = $this->getVerifyBidder($formData['auction_id']);
        //echo '<pre>'; print_r($verifyBidder); die('verifyBidder');
        if(empty($verifyBidder)) {
            return ['You are not verified for this bidding.'];
        } elseif(!in_array($user_id, $verifyBidder)) {
            return ['You are not verified for this bidding.'];
        }
        $formData['user_id'] = $user_id;
        $formData['bidder_id'] = $user_id;
        $model = new Bidding(['scenario' => 'Savebid']);
        $model->load($formData, '');
        if ($model->validate()) {
            $model->save();
        }
        return $model;
    }
    
    /**
    * To Auction details
    * 
    * @api {post} /api/web/v1/bidding/auction-details Auction details
    * @apiName AuctionDetails
    * @apiVersion 0.1.1
    * @apiGroup Bidding
    * @apiDescription To Auction details
    * 
    * @apiParam {Number} auction_id Auction Id
    * 
    * @apiSuccessExample Success-Response:
    *  HTTP/1.1 200 
    *    {
    *        "message": "",
    *        "data": {
    *            "items": [
    *                {
    *                    "id": 2,
    *                    "title": "MULTI-TENANCY INVESTMENT WITH UPSIDE",
    *                    "description": "For Sale by Expressions of Interest closing Wednesday 16 May 2018 at 3pm.\n\nSubstantial retail/office opportunity with upside.\n\n- Land Area: 532m2 approx.\n- Build Area: 1,190m2 approx.\n- Combined Income: $351,390 pa plus GST\n- 4 tenancies\n- Significant development potential (14 levels - STCA)\n- Rear access",
    *                    "status": 1,
    *                    "propertyType": 4,
    *                    "price": 234.34,
    *                    "landArea": null,
    *                    "land_area_unit": null,
    *                    "floorArea": 1190,
    *                    "floor_area_unit": null,
    *                    "conditions": 0,
    *                    "address": "37-41 Hall Street, MOONEE PONDS VIC, 3039 ",
    *                    "latitude": "37.9810",
    *                    "longitude": "145.2150",
    *                    "unitNumber": 0,
    *                    "streetNumber": 0,
    *                    "streetName": null,
    *                    "suburb": null,
    *                    "city": "Richmond",
    *                    "state": "1",
    *                    "postcode": null,
    *                    "country": "13",
    *                    "parking": 1,
    *                    "parkingList": null,
    *                    "parkingOther": null,
    *                    "floorplan": null,
    *                    "media": null,
    *                    "video": "https://youtu.be/YXSXe0LVFV8",
    *                    "videoImage": "http://cview.civitastech.com/frontend/web/images/property/Property_image626.jpg",
    *                   "document": null,
    *                    "school": 1,
    *                    "communityCentre": 0,
    *                    "parkLand": 0,
    *                    "publicTransport": 0,
    *                    "shopping": 1,
    *                    "bedrooms": 8,
    *                    "bathrooms": 6,
    *                    "features": null,
    *                    "tags": null,
    *                    "created_by": null,
    *                    "propertyStatus": 0,
    *                    "updated_by": null,
    *                    "created_at": "2018-04-18 14:06:29",
    *                    "updated_at": "2018-04-18 14:06:29",
    *                    "is_featured": 0,
    *                    "cview_listing_id": "",
    *                   "is_deleted": 0,
    *                    "business_email": null,
    *                    "website": null,
    *                    "valuation": null,
    *                    "exclusivity": null,
    *                    "tenancy": null,
    *                    "zoning": null,
    *                    "authority": null,
    *                    "sale_tax": null,
    *                    "tender_closing_date": null,
    *                    "current_lease_expiry": null,
    *                    "outgoings": null,
    *                    "lease_price": null,
    *                    "lease_price_show": 0,
    *                    "lease_period": null,
    *                    "lease_tax": null,
    *                    "external_id": null,
    *                    "video_url": null,
    *                    "slug": null,
    *                    "distance": "",
    *                    "propertyTypename": "Industrial/Warehouse",
    *                    "auctionDetails": null,
    *                    "gallery": [],
    *                    "auction": [
    *                        {
    *                            "id": 2,
    *                            "listing_id": 2,
    *                            "auction_type": null,
    *                            "estate_agent_id": 0,
    *                            "agency_id": 0,
    *                            "date": null,
    *                            "time": null,
    *                            "datetime_start": "2018-05-21 06:55:57",
    *                            "datetime_expire": 1526972180,
    *                            "starting_price": null,
    *                            "home_phone_number": null,
    *                            "estate_agent": null,
    *                            "picture_of_listing": null,
    *                            "duration": null,
    *                            "real_estate_active_himself": null,
    *                            "real_estate_active_bidders": null,
    *                            "reference_relevant_electronic": null,
    *                            "reference_relevant_bidder": null,
    *                            "created_at": 0,
    *                            "auction_status": 2,
    *                            "streaming_url": null,
    *                            "completed_streaming_url": "",
    *                            "streaming_status": null,
    *                            "templateId": "",
    *                            "envelopeId": "",
    *                            "documentId": "",
    *                            "document": "",
    *                            "recipientId": "",
    *                            "bidding_status": 2,
    *                            "updated_at": "2018-04-27",
    *                            "created_by": 0,
    *                            "updated_by": 0,
    *                            "agent": null,
    *                            "agency": null
    *                        }
    *                    ],
    *                    "saveauction": null,
    *                    "sold": "0",
    *                    "calendar": 0
    *                }
    *            ],
    *            "_links": {
    *                "self": {
    *                    "href": "http://localhost/AlphaWallet/html/api/web/v1/bidding/auction-details?page=1"
    *                }
    *            },
    *            "_meta": {
    *                "totalCount": 1,
    *                "pageCount": 1,
    *                "currentPage": 1,
    *                "perPage": 20
    *            }
    *        },
    *        "status": 200
    *    }
    *
    * @apiErrorExample Error-Response:
    *    HTTP/1.1 401 Unauthorized
    *    {
    *        "name": "Unauthorized",
    *        "message": "Your request was made with invalid credentials.",
    *        "code": 0,
    *        "status": 401,
    *        "type": "yii\\web\\UnauthorizedHttpException"
    *    }
    *
    * @return Object(common\models\Bidding)
    */
    public function actionAuctionDetails() {
        $formData = Yii::$app->getRequest()->getBodyParams();
        $auctionId = (isset($formData['auction_id']) ? $formData['auction_id'] : 0);
        $userId = (isset($formData['user_id']) ? $formData['user_id'] : 0);
        $query = Property::find()->select(Property::tableName().'.*')->joinWith('auction')->joinWith('city')->joinWith('state')->where([Auction::tableName().'.id'=>$auctionId]);
        /*echo '<pre>'; print_r($query); die('here');
        if(empty($query)){
            $msg = CommonModel::textGlobalization('app', 'no_record');
            return ['status' => 'error', 'message' => $msg, 'statusCode' => 422];            
        } */
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        return $dataProvider;   
    }    
    
    /**
     * To Update Bid Status
     * - URI: *api/web/v1/bidding/update-bid-status
     * @api {post} /api/web/v1/bidding/update-bid-status Update Bid Status
     * @apiName Update Bid Status
     * @apiVersion 0.1.1
     * @apiGroup Bidding
     * @apiDescription Update Bid Status, Form-Data must be x-www-form-urlencoded
     * 
     * @apiParam {Number} bid_id ().
     * @apiParam {Number} bid_status (1 = 'Accepted', 2 => 'Rejected', 3 => 'Current Bid', 3 => 'Awarded')
     * 
     * @apiSuccessExample Success-Response:
     *    HTTP/1.1 200 
     *    {
     *      "message": "",
     *      "data": {
     *        "id": 2,
     *        "auction_id": 2,
     *        "bidder_id": 383,
     *        "is_vendor": 0,
     *        "amount": "1200.00",
     *        "status": "1",
     *        "bid_won": 0,
     *        "complete_status": 0,
     *        "turn_on_off": 0,
     *        "play_pause": 0,
     *        "play_status": 0,
     *        "lastbid": "0.00",
     *        "cancel": 0,
     *        "property_in_market": 0,
     *        "created_at": "1970-01-01 00:33:38",
     *        "updated_at": 1527845138,
     *        "created_by": 383,
     *        "updated_by": 383,
     *        "statusText": "Accepted",
     *        "bidderDetail": {
     *          "id": 383,
     *          "username": "soodvivek",
     *          "email": "viveks@graycelltech.com",
     *          "user_type": 3,
     *          "app_id": "2",
     *          "auth_key": "bzeIhiWdQ6ctsvRFODNZBwSuJWCjEiHL",
     *          "firstName": "Vivek",
     *          "lastName": "Sood",
     *          "fullName": "Vivek Sood",
     *          "orgName": null,
     *          "fbidentifier": null,
     *          "linkedin_identifier": null,
     *          "google_identifier": null,
     *          "role_id": null,
     *          "password_hash": "$2y$13$Dqt1d.ORiLY/LwAJxfZSX.TaKleAZZhORd5dl95N1IBjLOELy6.aC",
     *          "password_reset_token": null,
     *          "phone": null,
     *          "image": null,
     *          "sex": null,
     *          "dob": "1992-05-09",
     *          "ageGroup_id": 1,
     *          "bdayOffer": 0,
     *          "walletPinCode": null,
     *          "loyaltyPin": null,
     *          "advertister_id": null,
     *          "activationKey": null,
     *          "confirmationKey": null,
     *          "access_token": null,
     *          "hashKey": null,
     *          "status": 10,
     *          "device": "android",
     *          "device_token": "",
     *          "lastLogin": 1527841441,
     *          "latitude": null,
     *          "longitude": null,
     *          "timezone": null,
     *          "timezone_offset": null,
     *          "created_at": 1526554007,
     *          "updated_at": 1527841441,
     *          "storeid": null,
     *          "promise_uid": "3835afd5d9d0e593",
     *          "promise_acid": null,
     *          "user_code": "AW-soodvivek",
     *          "referralCode": null,
     *          "referral_percentage": null,
     *          "agency_id": null
     *        }
     *      },
     *      "status": 200
     *    }
     *    
     * @apiErrorExample Error-Response:
     *    HTTP/1.1 200
     *    {
     *      "message": "",
     *      "data": [
     *        "Your request was made with invalid credentials."
     *      ],
     *      "status": 200
     *    }
     *    
     * @apiErrorExample Error-Response:
     *    HTTP/1.1 401 Unauthorized
     *    {
     *      "message": "",
     *      "data": [
     *        "You don't have an authority for this action"
     *      ],
     *      "status": 200
     *    }
     *    
	 * @apiSuccessExample Error No Record Found
	 *    HTTP/1.1 422 Unprocessable entity	  	  
	 *    {
	 *      "message": "No Record Found",
	 *      "data": {
	 *        "status": "error"
	 *      },
	 *      "status": 422
	 *    }     
     * @return Object(common\models\Bidding)
     */    
    // Required params are bid_id and status
    public function actionUpdateBidStatus(){
        $posted = Yii::$app->request->post();
        $authToken =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        $user_id = Yii::$app->MyConstant->AgentID($authToken);
        if($user_id){
            if (empty($posted['bid_id'])) {
                return ["Required 'bid_id' can't be blank"];
            }
            if (empty($posted['bid_status'])) {
                return ["Required 'bid_status' can't be blank"];
            }
            $bidding = new Bidding();
            $data = $bidding->validateBid($posted['bid_id']);
            if(!empty($data)){
                $auction = new Auction();
                $auctionData = $auction->getAgentId($data['auction_id']);
                if($user_id != $auctionData['estate_agent_id']){
                    return ['You don\'t have an authority for this action'];
                }else{
                    if($posted['bid_status'] == 3){ // to set as current bid
                        $query = Bidding::updateAll(['status' => 1], ['AND', ['auction_id' => $data['auction_id'], 'status' => [1,3] ] ]);
                    }
                    $data->status = $posted['bid_status'];
                    $data->save(false);
                    return $data;
                }
            }else{
                $msg = CommonModel::textGlobalization('app', 'no_record');
                return ['status' => 'error', 'message' => $msg, 'statusCode' => 422];                          
            }
        }else{
            return ['Your request was made with invalid credentials.'];
        }   
    }
    
    /** 
	 * Get Current bid for an auction
	 * @api {get} api/web/v1/bidding/auction-current-bid Current Bid for an Auction
	 * @apiName Current Bid for an Auction
	 * @apiVersion 0.1.1
	 * @apiGroup Bidding
	 * @apiDescription Current Bid for an Auction
	 *
	 * @apiParam {Number} auction_id listing id to get the detail of current bid.
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 
     *    {
     *      "message": "",
     *      "data": {
     *        "id": 26,
     *        "auction_id": 3,
     *        "bidder_id": 351,
     *        "is_vendor": 0,
     *        "amount": "2202.50",
     *        "bidder_amount": "0.00",
     *        "bidder_currency_id": 0,
     *        "status": 3,
     *        "bid_won": 0,
     *        "complete_status": 0,
     *        "turn_on_off": 0,
     *        "play_pause": 0,
     *        "play_status": 0,
     *        "lastbid": "0.00",
     *        "cancel": 0,
     *        "property_in_market": 0,
     *        "created_at": "1970-01-01 00:33:38",
     *        "updated_at": "2018-06-04 08:34:54",
     *        "created_by": 351,
     *        "updated_by": 351,
     *        "statusText": "Current Bid",
     *        "bidderDetail": {
     *          "id": 351,
     *          "username": "gurpreet.gct",
     *          "email": "gurpreet@graycelltech.com",
     *          "user_type": 3,
     *          "app_id": "2",
     *          "auth_key": "oNoGgQNodoIRbAZlZqZgTTWDt5zb-ApK",
     *          "firstName": "GPreet",
     *          "lastName": "Singh",
     *          "fullName": "GPreet Singh",
     *          "orgName": null,
     *          "fbidentifier": null,
     *          "linkedin_identifier": "123qweasdcxz",
     *          "google_identifier": null,
     *          "role_id": null,
     *          "password_hash": "$2y$13$nHZiuXex00yoh9dFlJQg3.vfX7UpAP3.pU3UmN8dl0G0kkRt6oDKm",
     *          "password_reset_token": "ejGpGRiP1S-i1RfMjNa4iXIZpIlo4rWu_1526643425",
     *          "phone": null,
     *          "image": null,
     *          "sex": null,
     *          "dob": "1970-01-01",
     *          "ageGroup_id": 1,
     *          "bdayOffer": 0,
     *          "walletPinCode": null,
     *          "loyaltyPin": null,
     *          "advertister_id": null,
     *          "activationKey": null,
     *          "confirmationKey": null,
     *          "access_token": null,
     *          "hashKey": null,
     *          "status": 10,
     *          "device": "android",
     *          "device_token": "",
     *          "lastLogin": 1528089507,
     *          "latitude": null,
     *          "longitude": null,
     *          "timezone": null,
     *          "timezone_offset": null,
     *          "created_at": 1524141463,
     *          "updated_at": 1528089507,
     *          "storeid": null,
     *          "promise_uid": "3085abb78c79ebb4",
     *          "promise_acid": null,
     *          "user_code": "AW-gurpreet.gct",
     *          "referralCode": null,
     *          "referral_percentage": null,
     *          "agency_id": null
     *        }
     *      },
     *      "status": 200
     *    }
     *    
	 * @apiErrorExample Error No Record Found
	 *    HTTP/1.1 422 Unprocessable entity	  
	 *    {
	 *      "message": "No Record Found",
	 *      "data": {
	 *        "status": "error"
	 *      },
	 *      "status": 422
	 *    }
	 *    
	 * @return Object(common\models\Bidding)
	 */    
    public function actionAuctionCurrentBid(){
        $posted = Yii::$app->request->post();
        if (empty($posted['auction_id'])) {
            return ["Required 'auction_id' can't be blank"];
        }
        $bidding = new Bidding();
        $auction = $bidding->getCurrentBid($posted['auction_id']);
        if(empty($auction)){
            $msg = CommonModel::textGlobalization('app', 'no_record');
            return ['status' => 'error', 'message' => $msg, 'statusCode' => 422];            
        }
        return $auction;
    }    
    
}