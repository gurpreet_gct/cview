<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\components\Controller;
use common\models\Currencies;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\CviewConfig;
use api\modules\v1\models\Bidder;
use common\models\User;
/**
* Currency Controller API
*
*/
class CurrencyController extends Controller
{
    public $modelClass = 'common\models\Currencies';    
	const CONFIG_STATUS = 1;
	const CONFIG_CURRENCY_KEY = 'allowed_currencies_for_bids';

    public function behaviors()
    { 
        $behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
            'except' => ['subfees'],
            'only'=>[
            	'all-currency',
            	'get-currency',
                'currency-exchange',
                'allowed-currency',
                'add-update-currency'
            ],            
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
    }

    public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['view'],$actions['index'],$actions['delete']);                    

		return $actions;
	}

   /** 
	* 
	* Get all currencies
	* - URI: *api/web/v1/currency/all-currency
	*
	* @api {get} api/web/v1/currency/all-currency Show All Currencies
	*
	* @apiName Show All Currencies
	* @apiVersion 0.1.1
	* @apiGroup Currencies
	* @apiDescription To show all currencies.
	*
	* @apiSuccessExample Success-Response:
	*    HTTP/1.1 200 OK
	*    {
	*        "message": "",
	*        "data": [
	*	        {
	*	            "id": 2,
	*	            "country": "America",
	*	            "currency": "Dollars",
	*	            "code": "USD",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 3,
	*	            "country": "Afghanistan",
	*	            "currency": "Afghanis",
	*	            "code": "AFN",
	*	            "symbol": "؋",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 4,
	*	            "country": "Argentina",
	*	            "currency": "Pesos",
	*	            "code": "ARS",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 5,
	*	            "country": "Aruba",
	*	            "currency": "Guilders",
	*	            "code": "AWG",
	*	            "symbol": "ƒ",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 6,
	*	            "country": "Australia",
	*	            "currency": "Dollars",
	*	            "code": "AUD",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 7,
	*	            "country": "Azerbaijan",
	*	            "currency": "New Manats",
	*	            "code": "AZN",
	*	            "symbol": "ман",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 8,
	*	            "country": "Bahamas",
	*	            "currency": "Dollars",
	*	            "code": "BSD",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 9,
	*	            "country": "Barbados",
	*	            "currency": "Dollars",
	*	            "code": "BBD",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 10,
	*	            "country": "Belarus",
	*	            "currency": "Rubles",
	*	            "code": "BYR",
	*	            "symbol": "p.",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 11,
	*	            "country": "Belgium",
	*	            "currency": "Euro",
	*	            "code": "EUR",
	*	            "symbol": "€",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 12,
	*	            "country": "Beliz",
	*	            "currency": "Dollars",
	*	            "code": "BZD",
	*	            "symbol": "BZ$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 13,
	*	            "country": "Bermuda",
	*	            "currency": "Dollars",
	*	            "code": "BMD",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 14,
	*	            "country": "Bolivia",
	*	            "currency": "Bolivianos",
	*	            "code": "BOB",
	*	            "symbol": "$b",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 15,
	*	            "country": "Bosnia and Herzegovina",
	*	            "currency": "Convertible Marka",
	*	            "code": "BAM",
	*	            "symbol": "KM",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 16,
	*	            "country": "Botswana",
	*	            "currency": "Pula",
	*	            "code": "BWP",
	*	            "symbol": "P",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 17,
	*	            "country": "Bulgaria",
	*	            "currency": "Leva",
	*	            "code": "BGN",
	*	            "symbol": "лв",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 18,
	*	            "country": "Brazil",
	*	            "currency": "Reais",
	*	            "code": "BRL",
	*	            "symbol": "R$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 19,
	*	            "country": "Britain (United Kingdom)",
	*	            "currency": "Pounds",
	*	            "code": "GBP",
	*	            "symbol": "£",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 20,
	*	            "country": "Brunei Darussalam",
	*	            "currency": "Dollars",
	*	            "code": "BND",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 21,
	*	            "country": "Cambodia",
	*	            "currency": "Riels",
	*	            "code": "KHR",
	*	            "symbol": "៛",
	*	            "status": 1
	*	        }
	*        ],
	*        "status": 200
	*    }
	*/
	public function actionAllCurrency()
	{
        return new ActiveDataProvider([ 
            'query' => Currencies::find()->where(['status' => 1]),					 
        ]);
	}

   /** 
	* 
	* Get get-currency
	* - URI: *api/web/v1/currency/get-currency?id=2
	*
	* @api {get} api/web/v1/currency/get-currency?id=246 Get Currency details by id
	*
	* @apiName Get Currency by id
	* @apiVersion 0.1.1
	* @apiGroup Currencies
	* @apiDescription To get a currency by id.
	*
	* @apiParam {String} id id of the currency.
	*
	* @apiSuccessExample Success-Response:
	*    HTTP/1.1 200 OK
	*    {
	*        "message": "",
	*        "data": [
	*            {
	*                "id": 2,
	*                "country": "America",
	*                "currency": "Dollars",
	*            	 "code": "USD",
	*        		 "symbol": "$",
	*        		 "status": 1
	*            },
	*        ],
	*        "status": 200
	*    }
	*/
    public function actionGetCurrency($id)
    {
        return new ActiveDataProvider([
            'query' => Currencies::find()->where(['id' => $id,'status' => 1])
        ]);
    }

   /** 
	* 
	* Get allowed currencies
	* - URI: *api/web/v1/currency/allowed-currency
	*
	* @api {get} api/web/v1/currency/allowed-currency Show Allowed Currencies
	*
	* @apiName Show Allowed Currencies
	* @apiVersion 0.1.1
	* @apiGroup Currencies
	* @apiDescription To show allowed currencies.
	*
	* @apiSuccessExample Success-Response:
	*    HTTP/1.1 200 OK
	*    {
	*        "message": "",
	*        "data": [
	*	        {
	*	            "id": 2,
	*	            "country": "America",
	*	            "currency": "Dollars",
	*	            "code": "USD",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 3,
	*	            "country": "Afghanistan",
	*	            "currency": "Afghanis",
	*	            "code": "AFN",
	*	            "symbol": "؋",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 4,
	*	            "country": "Argentina",
	*	            "currency": "Pesos",
	*	            "code": "ARS",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 5,
	*	            "country": "Aruba",
	*	            "currency": "Guilders",
	*	            "code": "AWG",
	*	            "symbol": "ƒ",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 6,
	*	            "country": "Australia",
	*	            "currency": "Dollars",
	*	            "code": "AUD",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 7,
	*	            "country": "Azerbaijan",
	*	            "currency": "New Manats",
	*	            "code": "AZN",
	*	            "symbol": "ман",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 8,
	*	            "country": "Bahamas",
	*	            "currency": "Dollars",
	*	            "code": "BSD",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 9,
	*	            "country": "Barbados",
	*	            "currency": "Dollars",
	*	            "code": "BBD",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 10,
	*	            "country": "Belarus",
	*	            "currency": "Rubles",
	*	            "code": "BYR",
	*	            "symbol": "p.",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 11,
	*	            "country": "Belgium",
	*	            "currency": "Euro",
	*	            "code": "EUR",
	*	            "symbol": "€",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 12,
	*	            "country": "Beliz",
	*	            "currency": "Dollars",
	*	            "code": "BZD",
	*	            "symbol": "BZ$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 13,
	*	            "country": "Bermuda",
	*	            "currency": "Dollars",
	*	            "code": "BMD",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 14,
	*	            "country": "Bolivia",
	*	            "currency": "Bolivianos",
	*	            "code": "BOB",
	*	            "symbol": "$b",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 15,
	*	            "country": "Bosnia and Herzegovina",
	*	            "currency": "Convertible Marka",
	*	            "code": "BAM",
	*	            "symbol": "KM",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 16,
	*	            "country": "Botswana",
	*	            "currency": "Pula",
	*	            "code": "BWP",
	*	            "symbol": "P",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 17,
	*	            "country": "Bulgaria",
	*	            "currency": "Leva",
	*	            "code": "BGN",
	*	            "symbol": "лв",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 18,
	*	            "country": "Brazil",
	*	            "currency": "Reais",
	*	            "code": "BRL",
	*	            "symbol": "R$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 19,
	*	            "country": "Britain (United Kingdom)",
	*	            "currency": "Pounds",
	*	            "code": "GBP",
	*	            "symbol": "£",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 20,
	*	            "country": "Brunei Darussalam",
	*	            "currency": "Dollars",
	*	            "code": "BND",
	*	            "symbol": "$",
	*	            "status": 1
	*	        },
	*	        {
	*	            "id": 21,
	*	            "country": "Cambodia",
	*	            "currency": "Riels",
	*	            "code": "KHR",
	*	            "symbol": "៛",
	*	            "status": 1
	*	        }
	*        ],
	*        "status": 200
	*    }
	*/
	public function actionAllowedCurrency()
	{
		$allowedCurrencyList = CviewConfig::find()->where(['key' => self::CONFIG_CURRENCY_KEY, 'status' => self::CONFIG_STATUS])->one();

		$currencyIds = (isset($allowedCurrencyList->value) && !empty($allowedCurrencyList->value) ? json_decode($allowedCurrencyList->value) : '');

        return new ActiveDataProvider([ 
            'query' => Currencies::find()->where(['id' => $currencyIds, 'status' => 1]),					 
        ]);
	}

   /**
    * To CurrencyExchange
    * - URI: *api/web/v1/currency/currency-exchange
    * @api {post} /api/web/v1/currency/currency-exchange CurrencyExchange
    * @apiName CurrencyExchange
    * @apiVersion 0.1.1
    * @apiGroup Currencies
    * @apiDescription To Currency Exchange
    * 
    * @apiParam {Number} amount (amount).
    * @apiParam {Number} from_currency (converting from currency)
    * @apiParam {Number} to_currency (converting to currency)
    *
    * @apiSuccessExample Success-Response:
    *  HTTP/1.1 200 
	*	{
	*	    "message": "",
	*	    "data": {
	*	        "currency": "INR",
	*	        "amount": 6742
	*	    },
	*	    "status": 200
	*	}
    *
    *
    *
    * @apiErrorExample Error-Response:
    *    HTTP/1.1 401 Unauthorized
    *    {
    *        "name": "Unauthorized",
    *        "message": "Your request was made with invalid credentials.",
    *        "code": 0,
    *        "status": 401,
    *        "type": "yii\\web\\UnauthorizedHttpException"
    *    }
    *
    * @return Object(common\models\currencies)
    */
    public function actionCurrencyExchange() {
        $formData 		= Yii::$app->getRequest()->getBodyParams();
        $amount         = (isset($formData['amount']) ? $formData['amount'] : 0);
        $from_currency  = (isset($formData['from_currency']) ? $formData['from_currency'] : '');
        $to_currency    = (isset($formData['to_currency']) ? $formData['to_currency'] : '');

        $formData['amount'] 		= $amount;
        $formData['from_currency'] 	= $from_currency;
        $formData['to_currency'] 	= $to_currency;
        $model = new Currencies(['scenario' => 'currency-converter']);
        $model->load($formData, '');
        if ($model->validate()) {
        	$data = $this->convertCurrency($from_currency, $to_currency, $amount);
       		if(!empty($data))
        		return array("currency" => $to_currency, "amount" => $data);
        }
        return $model;
    }

    public function convertCurrency($currency_from,$currency_to,$convert_amount) {
        $currency_from      = urlencode($currency_from);
        $currency_to        = urlencode($currency_to); 
        $convert_details    = file_get_contents("https://finance.google.com/bctzjpnsun/converter?a=1&from=$currency_from&to=$currency_to");

        $convert_details    = explode("<span class=bld>",$convert_details);
        $convert_details    = explode("</span>",$convert_details[1]);
        $conversion_rate    = preg_replace("/[^0-9\.]/", null, $convert_details[0]);
        $total_converted_currency_amount = $convert_amount*$conversion_rate;
        return $total_converted_currency_amount;
    }

   /**
    * To AddUpdateCurrency
    * - URI: *api/web/v1/currency/add-update-currency
    * @api {post} /api/web/v1/currency/add-update-currency AddUpdateCurrency
    * @apiName AddUpdateCurrency
    * @apiVersion 0.1.1
    * @apiGroup Currencies
    * @apiDescription To Add Update Currency
    * 
    * @apiParam {String} key (For an example: allowed_currencies_for_bids).
    * @apiParam {Array} value (Currency ids in json format, for an example: ["6","22","25","38","85","99","101","123"])
    *
    * @apiSuccessExample Success-Response:
    *  HTTP/1.1 200 
	*	{
	*	    "message": "",
	*	    "data": {
	*	        "id": 2,
	*	        "key": "allowed_currencies_for_bids",
	*	        "value": "[\"6\",\"22\",\"25\",\"38\",\"85\",\"99\",\"101\",\"123\"]",
	*	        "status": 1
	*	    },
	*	    "status": 200
	*	}
    *
    * @apiErrorExample Error-Response:
    *    HTTP/1.1 401 Unauthorized
    *    {
    *        "name": "Unauthorized",
    *        "message": "Your request was made with invalid credentials.",
    *        "code": 0,
    *        "status": 401,
    *        "type": "yii\\web\\UnauthorizedHttpException"
    *    }
    * @return Object(common\models\Currencies)
    */
	public function actionAddUpdateCurrency() {
        $authToken  =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        $user 		= User::findIdentityByAccessToken($authToken);
        $formData 	= Yii::$app->getRequest()->getBodyParams();

        $formData['key'] 	= self::CONFIG_CURRENCY_KEY;
        $formData['value'] 	= $formData['value'];

        $model 			= \common\models\CviewConfig::findOne(['key' => self::CONFIG_CURRENCY_KEY]);
        if(empty($model)) {
        	$model 		= new \common\models\CviewConfig();
        }
        $model->scenario= "validating-currency"; 
        $model->load($formData, '');
        if ($model->validate()) {
            $model->save();
        }
        return $model;
	}

   /**
	* To RemoveCurrency
	* - URI: *api/web/v1/currency/remove-currency
	* @api {post} /api/web/v1/currency/remove-currency RemoveCurrency
	* @apiName RemoveCurrency
	* @apiVersion 0.1.1
	* @apiGroup Currencies
	* @apiDescription To Remove Currency
	* 
	* @apiParam {String} key (For an example: allowed_currencies_for_bids).
	* @apiParam {Array} value (Currency ids in json format, for an example: ["22","25"])
	*
	* @apiSuccessExample Success-Response:
	*  HTTP/1.1 200 
	*	{
	*	    "message": "",
	*	    "data": {
	*	        "id": 2,
	*	        "key": "allowed_currencies_for_bids",
	*	        "value": "[\"6\",\"38\",\"85\",\"99\",\"101\",\"123\"]",
	*	        "status": 1
	*	    },
	*	    "status": 200
	*	}
	*
	* @apiErrorExample Error-Response:
	*    HTTP/1.1 401 Unauthorized
	*    {
	*        "name": "Unauthorized",
	*        "message": "Your request was made with invalid credentials.",
	*        "code": 0,
	*        "status": 401,
	*        "type": "yii\\web\\UnauthorizedHttpException"
	*    }
	* @return Object(common\models\Currencies)
	*/
	public function actionRemoveCurrency() {
        $authToken  =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        $user 		= User::findIdentityByAccessToken($authToken);
        $formData 	= Yii::$app->getRequest()->getBodyParams();
        $model 		= \common\models\CviewConfig::findOne(['key' => self::CONFIG_CURRENCY_KEY]);
        $arrange_currencies = "";
        $all_currencies 	= json_decode($model['value']);
		$remove_currencies	= json_decode($formData['value']);
		if(!empty($all_currencies) && !empty($remove_currencies)) {
			$allowed_currencies = array_diff($all_currencies,$remove_currencies);
			$arrange_currencies = array_values($allowed_currencies);
		}

        $formData['key'] 		= self::CONFIG_CURRENCY_KEY;
        $formData['value'] 		= json_encode($arrange_currencies);
        $model->scenario		= "validating-currency";
        $model->load($formData, '');
        if ($model->validate()) {
            $model->save();
        }
        return $model;
	}
}
