<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\AuctionEvent;
use common\models\User;
use common\models\CviewConfig;
use api\modules\v1\models\Customer;
use api\modules\v1\models\ApplyBid;
use api\modules\v1\models\ApplyBidSearch;
use api\components\Controller;

/**
 * AuctionEventController
 *
 * Save|Update AuctionEvent management methods for APIs are defined here.
 */

class AuctionEventController extends Controller
{
    public $modelClass = 'api\modules\v1\models\AuctionEvent';
    

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
/*		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'except' => ['subfees'],
            'only'=>[
                'saveauction',
                'search',
                'unfavourite',
                'rating',
                'add-to-calendar'
            ],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];*/
		return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "update" actions
        unset($actions['delete'], $actions['index'], $actions['update'],$actions['create']);

        return $actions;
    }
	
    /**
     *
     * Get list of auction status, to be used for storing the auction related status
     * Sample response: `
     *  [
     *      ['pause' => [
     *          [1 => 'Seeking vendor advice'],
     *          [2 => 'Weather conditions'],
     *          [3 => 'Other']
     *      ]
     *  ]`
     */
    public function actionGetAuctionConfig()
    {
        $model = new CviewConfig;
        return $model->getAuctionStatusConfig();
    }
    
    /**
     * Start a streaming-auction
     *
     * @api {post} api/web/v1/auction-event/start-streaming-auction Start a Streaming Auction
     * @apiName Start Streaming Auction
     * @apiVersion 0.1.1
     * @apiGroup AuctionEvent
     * @apiDescription To start a streaming auction. Is called prior to starting the actual stremaing process Form-Data must be x-www-form-urlencoded
     *
     * @apiParam {Number} id Auction id.
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *     "message": "Time to start the auction",
     *     "data": {
     *         "status": true,
     *         "auction": {
     *             "id": 24,
     *             "listing_id": 6,
     *             "auction_type": null,
     *             "estate_agent_id": 0,
     *             "agency_id": 0,
     *             "date": null,
     *             "time": null,
     *             "datetime_start": 1212633531,
     *             "datetime_expire": null,
     *             "starting_price": null,
     *             "home_phone_number": null,
     *             "estate_agent": null,
     *             "picture_of_listing": null,
     *             "duration": null,
     *             "real_estate_active_himself": null,
     *             "real_estate_active_bidders": null,
     *             "reference_relevant_electronic": null,
     *             "reference_relevant_bidder": null,
     *             "created_at": 0,
     *             "auction_status": 2,
     *             "streaming_url": null,
     *             "completed_streaming_url": "",
     *             "streaming_status": null,
     *             "templateId": "",
     *             "envelopeId": "",
     *             "documentId": "",
     *             "document": "",
     *             "recipientId": "",
     *             "bidding_status": 2,
     *             "updated_at": 1524806263,
     *             "created_by": 0,
     *             "updated_by": 0,
     *             "is_favourite": "",
     *             "facebook": "https://www.facebook.com/",
     *             "twitter": "https://twitter.com/",
     *             "instagram": "https://www.instagram.com/",
     *             "agency_agents": []
     *         }
     *     },
     *     "status": 200
     * }
     *
     * @apiSuccessExample Invalid Streaming Response:
     * HTTP/1.1 200 OK
     * {
     *     "message": "This auction is not a streaming auction",
     *     "data": {
     *         "status": false
     *     },
     *     "status": 200
     * }  
     *
     * @apiSuccessExample Missing id:
     * HTTP/1.1 200 OK
     * {
     *     "message": "Missing id param",
     *     "data": {
     *         "status": false
     *     },
     *     "status": 200
     * }
     *
     *
     *  @return Object(api\modules\v1\models\Customer)
     */
    public function actionStartStreamingAuction(){
		
        $data = Yii::$app->request->post();
        $auction = new AuctionEvent;
        if (empty($data['id'])) {
            return [
                'status' => false,
                'message' => 'Missing id param'
            ];
        }
        
        // Check if auction if valid
        $auction_model = $auction->getAuctionById($data['id']);
        
        if (empty($auction_model)) {
            return [
                'status' => false,
                'message' => 'Invalid auction requested'
            ];
        }

        // Check if auction is actually a streaming auction
        if (!$auction_model->checkIfStreamingAuction()) {
            return [
                'status' => false,
                'message' => 'This auction is not a streaming auction'
            ];
        }

        // Check if auction is qualified to start based upon current time and auction time
        $auction_time_to_start = $auction_model->getAuctionTimeToStart();
        if ($auction_time_to_start > 0) {
            $error_message = sprintf('Still %d seconds left for the auction to start!', $auction_time_to_start);
            return [
                'status' => false,
                'message' => $error_message
            ];
        }

        return [
            'status'  => true,
            'message' => 'Time to start the auction',
            'auction'    => $auction_model
        ];

	}

    /**
     * Start a streaming-auction
     *
     * @api {post} api/web/v1/auction-event/update-bidding-pause-status Change Auction Bidding Pause status
     * @apiName Update auction bidding pause status
     * @apiVersion 0.1.1
     * @apiGroup AuctionEvent
     * @apiDescription To udpate bidding status of an auction. Form-Data must be x-www-form-urlencoded.
     *
     * @apiParam {Number} id Auction id.
     * @apiParam {Number} status Auction bidding pause status.
     *
     * @apiSuccessExample Success-Response:
     * {
     *     "message": "Auction pause status updated.",
     *     "data": {
     *         "status": true,
     *         "auction": {
     *             "id": 2,
     *             "listing_id": 3,
     *             "auction_type": null,
     *             "estate_agent_id": 381,
     *             "agency_id": 0,
     *             "date": 1529469351,
     *             "time": null,
     *             "datetime_start": 1529469351,
     *             "datetime_expire": 1527539432,
     *             "starting_price": null,
     *             "auction_currency_id": 3,
     *             "home_phone_number": null,
     *             "estate_agent": null,
     *             "picture_of_listing": null,
     *             "duration": null,
     *             "real_estate_active_himself": null,
     *             "real_estate_active_bidders": null,
     *             "reference_relevant_electronic": null,
     *             "reference_relevant_bidder": null,
     *             "created_at": 0,
     *             "auction_status": 3,
     *             "streaming_url": null,
     *             "completed_streaming_url": "",
     *             "streaming_status": null,
     *             "templateId": "",
     *             "envelopeId": "",
     *             "documentId": "",
     *             "document": "",
     *             "recipientId": "",
     *             "bidding_status": 2,
     *             "bidding_pause_status": "3",
     *             "updated_at": 1528111144,
     *             "created_by": 0,
     *             "updated_by": 0,
     *             "is_favourite": "",
     *             "facebook": "https://www.facebook.com/",
     *             "twitter": "https://twitter.com/",
     *             "instagram": "https://www.instagram.com/",
     *             "agency_agents": []
     *         }
     *     },
     *     "status": 200
     * }
     *
     * @apiSuccessExample Invalid Auction Response:
     * HTTP/1.1 200 OK
     * {
     *     "message": "Invalid auction requested",
     *     "data": {
     *         "status": false
     *     },
     *     "status": 200
     * }  
     *
     * @apiSuccessExample Missing id:
     * HTTP/1.1 200 OK
     * {
     *     "message": "Missing id param",
     *     "data": {
     *         "status": false
     *     },
     *     "status": 200
     * }
     *
     *
     *  @return Object(api\modules\v1\models\AuctionEvent)
     */
    public function actionUpdateBiddingPauseStatus(){
        
        $data = Yii::$app->request->post();
        $auction = new AuctionEvent;
        if (empty($data['id'])) {
            return [
                'status' => false,
                'message' => 'Missing id param'
            ];
        }
        
        // Check if auction if valid
        $auction_model = $auction->getAuctionById($data['id']);
        
        if (empty($auction_model)) {
            return [
                'status' => false,
                'message' => 'Invalid auction requested'
            ];
        }

        $auction_model = $auction_model->updateBiddingPauseStatus(Yii::$app->request->post()['status']);

        $status  = false;
        $message = 'Something went wrong trying to update auction bidding status.';
        
        if ($auction_model) {
            $status  = true;
            $message = 'Auction pause status updated.';
        }
        return [
            'status'  => $status,
            'message' => $message,
            'auction'    => $auction_model
        ];

    }
    
}
