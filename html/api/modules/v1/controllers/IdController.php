<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use common\models\Id;
use common\models\Cardtype;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
class IdController extends ActiveController
{    
        public $modelClass = 'common\models\Id';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['searchkeyword']);                    

		return $actions;
	}

  	
   /* save new Id information */    
    public function actionCreate()
    {
        $user_id=Yii::$app->user->id;   
        $model= new Id();
        $model->scenario = 'create';
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');  
        
       $model->user_id=$user_id;
        if($model->validate())
            {
                $find=Id::findOne(['cardType'=>$model->cardType,'user_id'=>$user_id]);
                if($find)
                {
                   $model=$find; 
                    $model->load(Yii::$app->getRequest()->getBodyParams(), '');  
                }
                if($model->front!="")
                {
				$frontimage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $model->front));
				$imageName=md5($model->nameOnCard.rand(10,1000).'front'). ".jpg";
                
                $imagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['idsimagepath']."/".$imageName);             
                file_put_contents($imagePath, $frontimage);
                /* save Id model in image name */
                $model->front=$imageName; 
				}
                
                if($model->back!="")
                {
				$backimage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $model->back));
				$backimagename=md5($model->nameOnCard.rand(10,1000).'back'). ".jpg";
				$backimagePath=str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['idsimagepath']."/".$backimagename);             
                file_put_contents($backimagePath, $backimage);
                /* save Id model in image name */
                
                $model->back=$backimagename;
                
               }
                 
                $model->save();
                  //$this->setHeader(201);
            }
			
           
        
         return $model;
    }
    
    public function actionUpdate($id)
    {
        $user_id=Yii::$app->user->id;
		$model= Id::findOne($id);      
		$oldfrontimage=$model->front;		
        $oldbackimage=$model->back;	
        	 
        $model->load(Yii::$app->getRequest()->getBodyParams(), ''); 
        $model->user_id=$user_id;
          
       if($model->validate())
        {
           

          /* for update front image */ 
            if($model->front!="" && (!strstr($model->front,".jpg")) )
            {				
                $frontimage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $model->front));                  
				preg_match('/.*\/(.*?)\./',$oldfrontimage,$match);				
				if(count($match)) 
                    $imageName=$match[1]. ".jpg";                
                else
                    $imageName= md5($model->nameOnCard.rand(10,1000).'front'). ".jpg";				
                $imagePath =str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['idsimagepath']."/".$imageName);				               
                file_put_contents($imagePath, $frontimage);     
                $model->front=$imageName;	 
							
            }
            
            /* for update back image */
            
             if($model->back!="" && (!strstr($model->back,".jpg")))
            {
						
                $backimage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $model->back));                  
				preg_match('/.*\/(.*?)\./',$oldbackimage,$match);				
				if(count($match)) 
                    $imageNameback=$match[1]. ".jpg";
                   
                else
                    $imageNameback= md5($model->nameOnCard.rand(10,1000).'back'). ".jpg";
				
				
              $imagePath =str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/../" .Yii::$app->params['idsimagepath']."/".$imageNameback);
				
                
                file_put_contents($imagePath, $backimage);     
				 $model->back = $imageNameback;	 
							
				}
				
				if (($model->save() === false) && (!$model->hasErrors())) {
					throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
					}
				else
					{
						return $model;	
					}
       				
			}
		
        
        return $model;	
    }
    
    /* To get all cards*/
   public function actionIndex()
   {
        $user_id=Yii::$app->user->id;
       return new ActiveDataProvider([
                            'query' => Id::find()
                                        ->with(['cardtype'=>function ($query) {
                                                    //$query->select('catIDs,id');
                                        },
                       ])
                       ->where(['user_id'=>$user_id]),
                       'pagination' => [
						'pageSize' => 50
						],
                        
                            
                       
                        ]);
            
   }
   
    public function actionSearchkeyword(){
	  $user_id=Yii::$app->user->id;
	  if($_POST['keyword']) {
		$keyword = $_POST['keyword'];
	     
	     $nameOnCard = Id::find()->select('id,nameOnCard,user_id')->where(['LIKE', 'nameOnCard', $keyword])->andWhere(['user_id'=>$user_id])->all();
      
        if(!empty($nameOnCard)) {
        $query =implode(",",ArrayHelper::map($nameOnCard, 'id', 'id'));    
           
        }else {              
        $cardNumber = Id::find()->select('id,cardNumber,user_id')->where(['LIKE', 'cardNumber', $keyword])->andWhere(['user_id'=>$user_id])->all();
        if(!empty($cardNumber)) {
        $query =implode(",",ArrayHelper::map($cardNumber, 'id', 'id'));       
        }else {
			 if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])$/",$keyword)){
				 $keyword = strtotime($keyword);
			}else{
				$keyword =$keyword;
			}
		
        $expiry = Id::find()->select('id,expiry,user_id')->where(['LIKE', 'expiry', $keyword])->andWhere(['user_id'=>$user_id])->all();
        if(!empty($expiry)) {
			
		$query =implode(",",ArrayHelper::map($expiry, 'id', 'id'));	
		}else{
			
			$cardtype = Cardtype::find()->select('id,name')->where(['LIKE', 'name', $keyword])->all();
			if(!empty($cardtype)) {
				$cardtypeid =implode(",",ArrayHelper::map($cardtype, 'id', 'id'));	
		   }
			
		}
		
	 }
	}
    
     	if(!empty($cardtypeid)) {
			
			return new ActiveDataProvider([
              'query' => Id::find()->where('cardType in ('.$cardtypeid.')')->andWhere(['user_id'=>$user_id])   
              ->groupBy('id')        
				->orderBy('id desc'),
				'pagination' => [
                'pageSize' => 50
            ],
			]);
		
		}
		if(!empty($query)){	  
        return new ActiveDataProvider([
              'query' => Id::find()->where('id in ('.$query.')')   
              ->groupBy('id')        
				->orderBy('id desc'),
				'pagination' => [
                'pageSize' => 50
            ],
			
			]);
		
		}else{		
		    return new ActiveDataProvider([
              'query' => Id::find()->where('id in (null)')   
              ->groupBy('id')        
				->orderBy('id desc'),
				'pagination' => [
                'pageSize' => 50
            ],
			
			]);
				
		}
	
	  
	}
	
    
 }
	
}
