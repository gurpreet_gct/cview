<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use api\modules\v1\models\WalletPin;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
class WalletController extends ActiveController
{    
        public $modelClass = 'api\modules\v1\models\WalletPin';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view']);                    

		return $actions;
	}

  	
  
    
    
    
    public function actionValidatepin()
    {
        $user_id=Yii::$app->user->id;   
        $model= new WalletPin();
        $model->userid=$user_id;
    //    $model->walletpin='adfadsfdasf';
 //   print_r(Yii::$app->request->post());die;
        $model->load(Yii::$app->request->getBodyParams(),'');       
        if($model->validate())
        {   
            return true;
        }
        else
        {
            
                    $model->validate(null,false);
				   return $model;
        }
        
        
        
    }
	
}