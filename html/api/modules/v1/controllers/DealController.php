<?php
namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Store;
use common\models\Workorders;
use common\models\Deal;
use common\models\Workorderpopup;
use api\modules\v1\models\UserCategory;
use api\modules\v1\models\Category;
use api\modules\v1\models\Manufacturer;
use api\modules\v1\models\Storelocator;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
class DealController extends ActiveController
{    
        public $modelClass = 'common\models\Deal';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['delete'],$actions['view'],$actions['search'],$actions['searchstore'],$actions['searchkeyword'],$actions['searchstorekeyword']);                    

		return $actions;
	}

  	
  
    public function actionIndex()
    {   
		 $user_id=Yii::$app->user->id;	
		   
       $category_ids=implode(",",ArrayHelper::map( UserCategory::find()->select('category_id')->where(['user_id'=>$user_id,'status'=>1])->all(), 'category_id', 'category_id'));		      
      
	   return new ActiveDataProvider([ 
            'query' => Deal::find()
                  
       ->joinWith('workorderpopup')
       
                     
       ->joinWith(['workordercategory'=> function ($q) use ($category_ids) {
            if($category_ids=="")
                $category_ids='0';
        $q->where('category_id in ( '.$category_ids.')');
           }
        ],false,'INNER JOIN')        
       
        ->joinWith(['workorders'=> function ($q) {  
			
			$q->where(sprintf('dateTo >= %s and noPasses > usedPasses' ,strtotime(date('Y-m-d 23:59:59')))); 
			} 

        ],false,'INNER JOIN')
                
        ->where(Workorders::tableName().'.status = 1')		
		->andWhere([Workorders::tableName().'.workorderType' =>1])
		->andWhere(['IN', Workorders::tableName().'.type', [1,4]])		
						
		->andWhere("(CASE rulecondition WHEN 1 THEN IF(Loyalty1 & Loyalty2,(CASE Loyalty3     
		WHEN 1 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 day) AND user_id = $user_id ) WHEN 2 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 week) AND user_id = $user_id) WHEN 3 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 month) AND user_id = $user_id) WHEN 4 THEN (select count(*)>=Loyalty1 as count from `tbl_order_loyalty` where `created_at` >= unix_timestamp(curdate() - INTERVAL Loyalty2 year) AND user_id = $user_id) END),false) ELSE rulecondition = 0 END)")
		
		->andWhere("(CASE rulecondition WHEN 1 THEN IF(behaviourDwell & behaviourDwellMinutes,(CASE behaviourDwellMinutes WHEN 1 THEN (SELECT (max(unix_timestamp(detect_at))-min(unix_timestamp(detect_at)))/60 >= behaviourDwell FROM tbl_beacon_detect_logs WHERE (unix_timestamp(detect_at) BETWEEN dateFrom AND dateTo) order by unix_timestamp(detect_at) desc) WHEN 2 THEN (SELECT (max(unix_timestamp(detect_at))-min(unix_timestamp(detect_at))) >= behaviourDwell FROM tbl_beacon_detect_logs WHERE (unix_timestamp(detect_at) BETWEEN dateFrom AND dateTo) order by unix_timestamp(detect_at) desc)  
		END),false) ELSE rulecondition = 0 END)")			
		
		->andWhere("(CASE rulecondition WHEN 1 THEN IF(behaviourvisit1 & behaviourvisit2 & behaviourvisit3, (CASE behaviourvisit3 WHEN 1 THEN (SELECT count(*) >= behaviourvisit1 FROM `tbl_userPsa` WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 DAY ) AND user_id = $user_id AND status= 0 ) WHEN 2 THEN (SELECT count(*) >= behaviourvisit1 FROM `tbl_userPsa` WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 WEEK) AND user_id = $user_id AND status= 0) WHEN 3 THEN (SELECT count(*) >= behaviourvisit1 FROM `tbl_userPsa` WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL behaviourvisit2 MONTH) AND user_id = $user_id AND status= 0) END) ,false) ELSE rulecondition = 0 END)")		
		
		->andWhere("(CASE rulecondition WHEN 1 THEN IF(purchaseHistory1 & purchaseHistory2 & purchaseHistory3, (CASE purchaseHistory3 WHEN 1 THEN (SELECT SUM(subtotal) >= purchaseHistory1 FROM `tbl_orders` WHERE created_at >= UNIX_TIMESTAMP(curdate() - INTERVAL purchaseHistory2 DAY) AND user_id = $user_id) WHEN 2 THEN (SELECT SUM(subtotal) >= purchaseHistory1 FROM `tbl_orders` WHERE created_at >= unix_timestamp(curdate() - INTERVAL purchaseHistory2 WEEK) AND user_id = $user_id) WHEN 3 THEN (SELECT SUM(subtotal) >= purchaseHistory1 FROM `tbl_orders` WHERE created_at >= unix_timestamp(curdate() - INTERVAL purchaseHistory2 month)AND user_id = $user_id) WHEN 4 THEN (SELECT SUM(subtotal) >= purchaseHistory1 as count from `tbl_orders` WHERE created_at >= unix_timestamp(curdate() - INTERVAL purchaseHistory2 year) AND user_id = $user_id) END) ,false) ELSE rulecondition = 0 END)")
			
				
		->groupBy('id')     
		->orderBy("id desc")   
       		
        ]); 
      
       
    }   
    
    
    
  
	
    public function actionSearch()
    {
		
		 $user_id=Yii::$app->user->id; 		 
			
        
		if(isset(Yii::$app->request->post()['categories']))
        {
		
        $rs=json_decode(Yii::$app->request->post()['categories'],true);          
       
       
        $category=(array_filter($rs['categories'], function($var){ return ($var['status']==1);})); 
       

		$category_ids=implode(",",ArrayHelper::map($category, 'category_id', 'category_id'));         
        
      
	   return new ActiveDataProvider([
            'query' => Deal::find()
                  
       ->joinWith('workorderpopup')        
        
       ->joinWith(['workordercategory'=> function ($q) use ($category_ids) {
            if($category_ids=="")
                $category_ids='0';
        $q->where('category_id in ( '. $category_ids.')');
           }
        ],false,'INNER JOIN')        
        
        ->joinWith(['workorders'=> function ($q)  {
            
        $q->where(sprintf('dateFrom <= %s and (dateTo + 86400) > %s',time(date("Y-m-d")),time(date("Y-m-d"))));}
        ],false,'INNER JOIN')
       ->where(Workorders::tableName().'.status = 1')
		->andWhere([Workorders::tableName().'.workorderType' =>1])
		->andWhere(['IN', Workorders::tableName().'.type', [1,4]])
        ->groupBy('id')        
       ->orderBy("id desc")
        ]);
        
       
        }
      
	}
	
	public function actionSearchkeyword()
	{
	  
			if(isset(Yii::$app->request->post()['keyword']))
			{
				
			$keyword = trim($_POST['keyword']);				
			
			$categories = Category::find()->select('id')->where(['name'=> $keyword])->one();
			
			$user = User::find()->select('id')->where(['LIKE', 'orgName', $keyword])->all();			
			
			if(isset($categories->id) && $categories->id!=='') {
			
			$category_ids = $categories->id; 		
						
			
			return new ActiveDataProvider([
             'query' => Deal::find()
            
            ->joinWith('workorderpopup') 
                     
            ->joinWith('workordercategory',false,'INNER JOIN')->where(['category_id' => $category_ids])  
		                     
			->joinWith(['workorders'=> function ($q)  {            
				$q->where(sprintf('dateFrom <= %s and dateTo > %s',time(date("Y-m-d")),time(date("Y-m-d"))+86400))->andWhere(Workorders::tableName().'.status = 1')->andWhere([Workorders::tableName().'.workorderType' =>1])->andWhere(['IN', Workorders::tableName().'.type', [1,4]]);}   
		
				],false,'INNER JOIN')
						
				->groupBy('id')       
			   ->orderBy("id desc")						
				
				]);			
			
			
		     }elseif(!empty($user[0]->id)){
										
			 $user_ids =implode(",",ArrayHelper::map($user, 'id', 'id'));
			
			 return new ActiveDataProvider([
			 
             'query' => Deal::find()
			 
			 ->orWhere(['NOT LIKE','bulletPoints', $keyword])
			 ->orWhere(['LIKE','dealTitle', $keyword])
			 ->orWhere(['LIKE', 'text', $keyword])
             ->orWhere("1=1")
			 
            ->joinWith('workorderpopup') 
                             
			->joinWith('dealstore')->orWhere(['LIKE', 'storename', $keyword])      
        
			->joinWith(['workorders'=> function ($q) use ($user_ids)  {				
				$q->andWhere(sprintf('dateFrom <= %s and (dateTo + 86400) > %s',time(date("Y-m-d")),time(date("Y-m-d"))))
				->andWhere('workorderpartner in ('.$user_ids.')')
				->andWhere(['status' => 1])
				->andWhere([Workorders::tableName().'.workorderType' =>1])
				->andWhere(['IN', Workorders::tableName().'.type', [1,4]])
				;
								
			}
			],false,'INNER JOIN')			
			->groupBy('id')        
		   ->orderBy("id desc")
			]);
			
		    }else{
				
			  return new ActiveDataProvider([
			 
              'query' =>  Deal::find()->where(['LIKE', 'dealTitle', $keyword])->orWhere(['LIKE', 'text', $keyword])                        
				->joinWith('workorderpopup')                        
		
			 ->joinWith(['workorders'=> function ($q)  {            
				$q->where(sprintf('dateFrom <= %s and (dateTo + 86400) > %s',time(date("Y-m-d")),time(date("Y-m-d"))))->andWhere(['status' => 1])->andWhere([Workorders::tableName().'.workorderType' =>1])
				->andWhere(['IN', Workorders::tableName().'.type', [1,4]])
				;}
				],false,'INNER JOIN')								
				->groupBy('id')       
			   ->orderBy("id desc")						
				
				]);
			
				
				
				}						
			
			
        }

}
	
	
      
    /* current deals */
    public function actionView($id)
    {        
	   $user_id=Yii::$app->user->id;   
        $store_id=$id;
	
 
	 return new ActiveDataProvider([
            'query' => Deal::find()
                  
       ->joinWith('workorderpopup')
              
       /*->joinWith(['dealstore'=> function ($q) use ($store_id) {          
        //$q->where('store_id in ( '. $store_id.')');
           }
        ],false,'INNER JOIN')
        */
        
        ->joinWith(['workorders'=> function ($q) use($store_id)  {
            
        $q->where(sprintf('dateFrom <= %s and (dateTo + 86400) > %s and advertiserID=%s ',time(date("Y-m-d")),time(date("Y-m-d")),$store_id));}
        ],false,'INNER JOIN')
		->where(Workorders::tableName().'.status = 1')
		->andWhere([Workorders::tableName().'.workorderType' =>1])
		->andWhere(['IN', Workorders::tableName().'.type', [1,4]])
        ->groupBy('id')        
       ->orderBy("id desc")
        ]);
      
		
    }
    
    
    /* deals pay via points start */
    
    public function actionPayviapoint($id)
    {   
		     
	   $user_id=Yii::$app->user->id;   
        $store_id=$id;
	
		$model=new Deal();
		$model->paywithpoint=1;
	 return new ActiveDataProvider([
            'query' => $model::find()
            ->select([Deal::tableName().".*", new Expression("1 as 'paywithpoint'")])
                  
       ->joinWith('workorderpopup')              
     
        ->joinWith(['workorders'=> function ($q) use($store_id)  {
            
        $q->where(sprintf('dateFrom <= %s and (dateTo + 86400) > %s and advertiserID=%s ',time(date("Y-m-d")),time(date("Y-m-d")),$store_id));}
        ],false,'INNER JOIN')
		->where(Workorders::tableName().'.status = 1')
		->andWhere([Workorders::tableName().'.workorderType' =>1])
		->andWhere([Workorders::tableName().'.buyNow' =>1])
		->andWhere(['IN', Workorders::tableName().'.type', [1,4]])
        ->groupBy('id')        
       ->orderBy("id desc"),
      
       
        ]);
      
		
    }
    
    public function actionSearchstore()
    {
		 $user_id=Yii::$app->user->id; 		 
			
  
		if(isset(Yii::$app->request->post()['categories']) && isset(Yii::$app->request->post()['store_id']) && !empty(Yii::$app->request->post()['store_id']) )
        {
				
		$store_id= $_POST['store_id'];	
		
        $rs=json_decode(Yii::$app->request->post()['categories'],true);  
       
        $category=(array_filter($rs['categories'], function($var){ return ($var['status']==1);}));

		$category_ids=implode(",",ArrayHelper::map($category, 'category_id', 'category_id'));
        
      
	   return new ActiveDataProvider([
            'query' => Deal::find()
                  
       ->joinWith('workorderpopup')        
        
       ->joinWith(['workordercategory'=> function ($q) use ($category_ids) {
            if($category_ids=="")
                $category_ids='0';
        $q->where('category_id in ( '. $category_ids.')');
           }
        ],false,'INNER JOIN') 
        
        /* ->joinWith(['dealstore'=> function ($q) use ($store_id) {          
        $q->where('store_id in ( '. $store_id.')');
           }
        ],false,'INNER JOIN')     */   
        
        ->joinWith(['workorders'=> function ($q)  {
            
        $q->where(sprintf('dateFrom <= %s and (dateTo + 86400) > %s',time(date("Y-m-d")),time(date("Y-m-d"))));}
        ],false,'INNER JOIN')
        ->where(Workorders::tableName().'.status = 1')->andWhere(Workorders::tableName().'.workorderpartner =' .$store_id)->andWhere([Workorders::tableName().'.workorderType' =>1])
        ->andWhere(['IN', Workorders::tableName().'.type', [1,4]])
        ->groupBy('id')        
       ->orderBy("id desc")
        ]);
        
       
        }
        
     }
     
      public function actionSearchstorekeyword()
	{	  
			if(isset(Yii::$app->request->post()['keyword']) && isset(Yii::$app->request->post()['store_id']) && !empty(Yii::$app->request->post()['store_id']))
			{
						
			$keyword = trim($_POST['keyword']);
			
			$store_id= $_POST['store_id'];	
			
			$categories = Category::find()->select('id')->where(['name'=> $keyword])->one();
			
			$user = User::find()->select('id')->where(['LIKE', 'orgName', $keyword])->all();
						
			if(isset($categories->id) && $categories->id!=='') {
				 
				 
			$category_ids = $categories->id; 
			
			return new ActiveDataProvider([
               'query' => Deal::find()
			   //->where('storeIDs in ('.$store_id.')')            
                    
				->joinWith('workorderpopup') 
                     
				->joinWith('workordercategory')->andWhere(['category_id' => $category_ids])         
        			

				->joinWith(['workorders'=> function ($q)  {            
				$q->where(sprintf('dateFrom <= %s and (dateTo + 86400) > %s',time(date("Y-m-d")),time(date("Y-m-d"))))->where(Workorders::tableName().'.status = 1')->andWhere([Workorders::tableName().'.workorderType' =>1])
				->andWhere(['IN', Workorders::tableName().'.type', [1,4]]);
			
				}

				],false,'INNER JOIN')
				#->where(Workorders::tableName().'.workorderpartner ='.$store_id)								
				->groupBy('id')       
			   ->orderBy("id desc")						
				
				]);			
			
			
		     }elseif(!empty($user[0]->id)){
			 
			 $user_ids =implode(",",ArrayHelper::map($user, 'id', 'id')); 
			
			 return new ActiveDataProvider([
              'query' => Deal::find()
			  //-> where('storeIDs in ('.$store_id.')')
                         
            ->joinWith('workorderpopup') 
                    
			->joinWith(['workorders'=> function ($q) use ($user_ids,$store_id)  {				
				 
               $q->where(sprintf('dateFrom <= %s and (dateTo + 86400) > %s and advertiserID=%s ',time(date("Y-m-d")),time(date("Y-m-d")),$store_id))
				
				//$q->andWhere(sprintf('dateFrom <= %s and (dateTo + 86400) > %s',time(date("Y-m-d")),time(date("Y-m-d"))))
				->andWhere('workorderpartner in ('.$user_ids.')')->andWhere(['status' => 1])->andWhere(['workorderType' => 1])
				->andWhere(['IN', Workorders::tableName().'.type', [1,4]])
				;	
						
			}
			],false,'INNER JOIN')			
			->groupBy('id')        
		   ->orderBy("id desc")
			]);
			
			
		    }else{
				
			return new ActiveDataProvider([
             'query' => Deal::find()->where(['LIKE','dealTitle', $keyword])->orWhere(['LIKE', 'text', $keyword])
                         
            ->joinWith('workorderpopup') 
                    
			->joinWith(['workorders'=> function ($q) use($store_id)  {
               $q->where(sprintf('dateFrom <= %s and (dateTo + 86400) > %s and advertiserID=%s ',time(date("Y-m-d")),time(date("Y-m-d")),$store_id))
				->andWhere(['status' => 1])->andWhere(['workorderType' => 1])
				->andWhere(['IN', Workorders::tableName().'.type', [1,4]]);	 }
				],false,'INNER JOIN')
				//->andWhere('storeIDs in ('.$store_id.')')
				
				->groupBy('id')        
			   ->orderBy("id desc")
			   
			   ]); 
        
        
        
					}						
			
        
		} 
	
	}
 
	
}
