<?php
namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use common\models\Loyaltypoint;
use common\models\Loyalty;
use common\models\UserLoyalty;
use common\models\Profile;
use common\models\Workorders;
use common\models\User;
use common\models\Orders;
use common\models\Transaction;
//use common\models\Cardtype;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class UserloyaltypointController extends Controller
{    
        public $modelClass = 'common\models\UserLoyalty';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
  public function actions()
	{
		$actions = parent::actions();

		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index'],$actions['view']);                    

		return $actions;
	}

	
	public function actionCreate(){
	$user_id=Yii::$app->user->id; 
	if(isset(Yii::$app->request->post()['store_id']) && Yii::$app->request->post()['store_id']!='' && isset(Yii::$app->request->post()['dollar']) && Yii::$app->request->post()['dollar']!=''){
	$model= new Loyalty(); 
	$loyalty= new Loyaltypoint();
	$model= Loyalty:: find()->select(
								[Loyalty::tableName().'.user_id',Loyalty::tableName().'.pointvalue',Loyalty::tableName().'.loyalty'])
								->where([Loyalty::tableName().'.user_id' => Yii::$app->request->post()['store_id']])
								->orderBy([Loyalty::tableName().'.id' => SORT_DESC])->asArray()->one();
	
			if(count($model) > 0){
								$loyalty->updated_at= time();
								$loyalty->created_at= time();
								$loyalty->valueOnDay = $model['loyalty'];
								$loyalty->loyaltypoints = $model['pointvalue'] + Yii::$app->request->post()['dollar'];
								$loyalty->user_id = $user_id;
								$loyalty->store_id = Yii::$app->request->post()['store_id'];
								$loyalty->order_id = 0;
								$loyalty->method = 1;
								$loyalty->status = 1;
								$loyalty->created_by = $user_id;
								$loyalty->updated_by = $user_id;
								$loyalty->save();
								return ["message"=>"success"];
			}
		}else{
			  return ["message"=>"please enter dollar point"];
			}
	}
	
	
	public function actionView($id){
		
	 $user_id=Yii::$app->user->id;  
	 if(isset(Yii::$app->request->get()['id'])){
     $model= new UserLoyalty();
	$model= UserLoyalty:: find()->select(
								[UserLoyalty::tableName().'.user_id',UserLoyalty::tableName().'.store_id','sum('.UserLoyalty::tableName().'.value) as Totalloyaltypoints',Profile::tableName().'.brandName',Profile::tableName().'.brandLogo',Profile::tableName().'.backgroundHexColour',Profile::tableName().'.foregroundHexColour',Profile::tableName().'.foregroundHexColour'])
								->leftJoin(Profile::tableName(), UserLoyalty::tableName().'.store_id = '.Profile::tableName(). '.user_id')
								->where([UserLoyalty::tableName().'.user_id' => $user_id])
								->groupBy([UserLoyalty::tableName().'.store_id'])
								->orderBy([UserLoyalty::tableName().'.id' => SORT_DESC,])->asArray()->all();
	
	$arr_result = array();
	foreach($model as $modelinfo){
		$result = array();
		$result['user_id'] = $modelinfo['user_id'];
		$result['store_id'] = $modelinfo['store_id'];
		$result['loyaltypoints'] = $modelinfo['Totalloyaltypoints'];
		$result['brandName'] = $modelinfo['brandName'];
		$result['backgroundHexColour'] = $modelinfo['backgroundHexColour'];
		$result['foregroundHexColour'] = $modelinfo['foregroundHexColour'];
		$result['brandLogo'] = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$modelinfo['brandLogo']);
		$arr_result[] = $result;
		}
	return $arr_result;
	 }
	}

	


	
}
