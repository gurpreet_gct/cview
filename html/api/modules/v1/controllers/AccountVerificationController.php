<?php

namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use Yii;
use yii\db\Query;
use yii\filters\auth\CompositeAuth;
use api\components\Controller;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\User;
use api\modules\v1\models\AccountVerification;

class AccountVerificationController extends Controller
{    
        public $modelClass = 'api\modules\v1\models\AccountVerification';   
    
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}
	   
    
	public function actions()
	{
		$actions = parent::actions();
		// disable the "delete" and "update" actions
		unset($actions['create'], $actions['update'],$actions['index']);    

		return $actions;
	}
    
    /**
	 * To Verify Account for bid
	 *
	 * @api {post} /api/web/v1/account-verification/verify-bid Verify account
	 * @apiName Verify Account
	 * @apiVersion 0.1.1
	 * @apiGroup AccountVerification
	 * @apiDescription To save bid, Form-Data must be x-www-form-urlencoded
	 *
	 * @apiParam {Number} listing_id Listing id of the property bid by user
	 * @apiParam {Number} user_id User id of the bidder
	 * @apiParam {Number} type 1|manual, 2|api
	 *
	 * @apiSuccessExample Success-Response:
	 *    HTTP/1.1 200 OK
	 *    {
	 *        "message": "",
	 *		  "data": {
	 *		      "id": 1,
	 *			  "register_bid_id": 2,
	 *			  "verified_by": "351",
	 *			  "type": 1,
	 *			  "data": "{\"passport_number\":0,\"driver_license_number\":0,\"mediacare_number\":0,\"recent_utility_bill\":0}",
	 *			  "complete": 1,
	 *			  "created_at": null,
	 *			  "created_by": null,
	 *			  "updated_at": null,
	 *			  "updated_by": null
	 *		  },
	 *		  "status": 200
	 *    }
	 * @return Object|AccountVerification object
	 */
    public function actionVerifyBid()
    {	
        /*$authToken =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));

        $user = User::findIdentityByAccessToken($authToken);
        if (!$user || empty($authToken)) {
            return ['Invalid request, Login first'];
        }*/

        $data = Yii::$app->getRequest()->getBodyParams();
		$model = new AccountVerification();

		$model->load(Yii::$app->getRequest()->getBodyParams(), '');
		$data['verified_by'] = null;  
		if ($model->validate()) {
			$model = $model->verifyAccount($data['verified_by'], $data['register_bid_id']);
		}
		return $model;
    }
	
}
