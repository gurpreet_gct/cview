<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;
use common\models\User;
use Yii;

/**
 * User Store Model
 *
 * 
 */
class UserStore extends ActiveRecord 
{
	
	 const STATUS_ACTIVE=1;
	  /**
     * @inheritdoc
     */
	//const ROLE_LOCATION_OWNER=7;
      
    public static function tableName()
    {
         return '{{%userstore}}';
    }
    
        public function status($state = true)
    {
        return $this->andWhere(['status' => $state]);
    }
	public function init()
	{
		
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
        ];     
    
    }
    
     public static function primaryKey(){
       return array('user_id', 'store_id');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [            
            'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First_Name'),
			'lastName' => Yii::t('app','Last_Name'),
			'fullName' => Yii::t('app','Full_Name'),
			'orgName' => Yii::t('app','Org_Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb_Identifier'),
			'role_id' => Yii::t('app','Role_Id'),
			'user_type' => Yii::t('app','User_Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New_Password'),
			'password_reset_token' => Yii::t('app','Password_Reset_Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date_Of_Birth'),
			'ageGroup_id' => Yii::t('app','Age_Group_Id'),
			'walletPinCode' => Yii::t('app','Wallet_Pin_Code'),
			'loyaltyPin' => Yii::t('app','Loyalty_Pin'),
			'auth_key' => Yii::t('app','Auth_Key'),
			'activationKey' => Yii::t('app','Activation_Key'),
			'confirmationKey' => Yii::t('app','Confirmation_Key'),
			'access_token' => Yii::t('app','Access_Token'),
			'hashKey' => Yii::t('app','Hash_Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device_Token'),
			'lastLogin' => Yii::t('app','Last_Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created At'),
            'updated_at'=>Yii::t('app','Updated_At'),
            
        ];
    }

	
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			
            if ($this->isNewRecord) {
               $this->created_at=time();
            }
			     $this->updated_at=time();							
            return true;
        }
        return false;
    }
 
   
}
