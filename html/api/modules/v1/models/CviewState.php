<?php 

namespace api\modules\v1\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
 
class CviewState extends ActiveRecord
{
	const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cview_states}}';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    
     /**
     * @inheritdoc
     */
    public function rules()
    {
        
    }
    
    
   public function attributeLabels()
		{
			return [
				'id' => Yii::t('app','ID'),
				'name' => Yii::t('app','Name'),
				
			];
		}
	
	
	
}

