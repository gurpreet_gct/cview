<?php

namespace api\modules\v1\models;

use Yii;
use common\models\Profile;
#use yii\web\UploadedFile;
/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property integer $status
 * @property string $email
 * @property integer $contact_no
 * @property string $role
 * @property integer $active
 * @property string $tokenhash
 * @property integer $tokenexpired
 * @property string $created
 * @property string $modified
 */
class Profiles extends Profile
{
    /**
     * @inheritdoc
     */
    public $usertype_id=0;
    //public $driver_license_number;
    /**
     *
    /*
    public static function tableName()
    {
        return 'tbl_profile';
    }
    */
    /* define model validation */
    public function rules(){
        return [
            [['user_id'],'required'],
//            [['passport_number', 'driver_license_number', 'mediacare_number', 'recent_utility_bill'],'required','on'=>'savebid'],
            [['passport_number', 'driver_license_number'],'required','on'=>'savebid'],
            [['id', 'user_id', 'companyName', 'companyNumber', 'ssdRegion', 'houseNumber', 'street', 'city', 'state', 'country', 'postcode', 'officePhone', 'officeFax', 'passport_number', 'driver_license_number', 'preferred_contact_number', 'enquiry_email', 'about', 'licence_number', 'awards', 'specialties', 'members', 'accreditation', 'languages', 'socialmediaFacebook', 'socialmediaTwitter', 'socialmediaLinkedin', 'socialmediaGoogle', 'socialmediaPinterest', 'socialmediaInstagram', 'latitude', 'longitude', 'alternativeContactperson', 'brandName', 'brandLogo', 'brandAddress', 'brandDescription', 'addLoyalty', 'backgroundHexColour', 'foregroundHexColour', 'subscriptionPay'],'safe'],
        ];
    }

	public function attributeLabels()
    {
        return [        
            'id' =>Yii::t('app','Id') ,
            'user_id' =>Yii::t('app','User_Id') ,
            'companyName' =>Yii::t('app','Company Name') ,
            'companyNumber' => Yii::t('app','Company Registration Number'),
            'ssdRegion' =>Yii::t('app','SweetSpot Region'),
            'houseNumber' => Yii::t('app','Street Number'),
            'street' => Yii::t('app','Street'),
            'city' =>Yii::t('app','City') ,
            'state' => Yii::t('app','State'),
            'country' => Yii::t('app','Country'),
            'postcode' =>Yii::t('app','Postcode') ,
            'officePhone' =>Yii::t('app','Office Phone') ,
            'officeFax' => Yii::t('app','Office Fax'),
			'alternativeContactperson' => Yii::t('app','Alternative Contact Person'),           
            'commericalStatus' => Yii::t('app','Commerical_Status'),
            'latitude' => Yii::t('app','Latitude'),
            'longitude' => Yii::t('app','Longitude'),
            'brandName' => Yii::t('app','Store/Brand Name'),
            'brandLogo' => Yii::t('app','Brand_Logo'),
            'brandimgLogo' => Yii::t('app','Store/Brand Logo (132px x 132px)'),
            'backgroundImage' => Yii::t('app','Background Image (640px x 360px)'),
            'brandAddress' => Yii::t('app','Store/Brand Address(head office)'),
            'brandDescription' => Yii::t('app','Store/Brand Description'),
            'socialmediaFacebook' => Yii::t('app','Facebook Url'),     
            'socialmediaTwitter' => Yii::t('app','Twitter Url'),     
            'socialmediaLinkedin' => Yii::t('app','Linkedin Url'),     
            'socialmediaGoogle' => Yii::t('app','Google Url'),  
            'socialmediaPinterest' =>Yii::t('app','Pinterest Url') ,  
            'socialmediaInstagram' => Yii::t('app','Instagram Url'),  
            'backgroundHexColour' => Yii::t('app','Background Hex Colour'),  
            'foregroundHexColour' =>Yii::t('app','Label Text Colour (Hex Colour)') ,  
            'addLoyalty' => Yii::t('app','Add "Loyalty"'),  
            'shopNow' => Yii::t('app','Add "Shop Now"'), 
            'socialcheck' => Yii::t('app','Add "Social media buttons and URLs"'), 
            'addcolors' => Yii::t('app','Add "Background and foreground hex colour"'),
            'passport_number' => Yii::t('app','Passport Number'),
            'driver_license_number' => Yii::t('app','Drivers license number'),
            'mediacare_number' => Yii::t('app','Upload "Mediacare Number"'),
            'recent_utility_bill' => Yii::t('app','Upload "Recent Utility Bill"'),
            'preferred_contact_number' => Yii::t('app',"Preferred Contact Number"),
            'about' => Yii::t('app', 'About'),
            'licence_number' => Yii::t('app', 'Licence Number'),
            'awards' => Yii::t('app', 'Awards'),
            'specialties' => Yii::t('app', 'Specialties'),
            'members' => Yii::t('app', 'Members'),
            'accreditation' => Yii::t('app', 'Accreditation'),
            'languages' => Yii::t('app', 'Languages'),                                   
        ];
    }

    public function init(){
		$this->usertype_id=3;
		$this->commercialStatus=3;        
	}
    
	public function fields()
	{
		$fields = parent::fields();
		unset($fields['RelatedBeaconLocationBrokerID']);
		unset($fields['RelatedBeaconLocationBroker']);
		unset($fields['NumberOfBeaconLocations']);
		unset($fields['contractDuration']);
		unset($fields['alternativeContactperson']);
		unset($fields['longitude']);
		unset($fields['latitude']);
		unset($fields['officeFax']);
		unset($fields['DID']);
		unset($fields['officePhone']);
		unset($fields['ssdRegion']);
		unset($fields['companyNumber']);
		// unset($fields['companyName']);
		unset($fields['user_id']);
		unset($fields['commercialStatus']);
		return $fields;
	}
	
	public function afterFind()
    {
        parent::afterFind();
		  
		if(($this->brandLogo!="")&&(!strstr($this->brandLogo,'http')))
		{			
			$this->brandLogo=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$this->brandLogo); 
        }		
		if(($this->backgroundHexColour!="")&&(!strstr($this->backgroundHexColour,'http'))&&(strstr($this->backgroundHexColour,'.')))
		{
			$this->backgroundHexColour=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$this->backgroundHexColour); 
		}
		if($this->mediacare_number){
			$this->mediacare_number=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/docs/".$this->mediacare_number);
		}
		if($this->recent_utility_bill){
			$this->recent_utility_bill=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/docs/".$this->recent_utility_bill);
		}
        return true;
    }
    
}