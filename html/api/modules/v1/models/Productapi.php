<?php
namespace api\modules\v1\models;

use Yii;
use common\models\Product;
use common\models\Profile;
use common\models\FoodcategoryProducts;

class Productapi extends Product
{
	
	public function getProfile()
	{
		return $this->hasOne(Profile::className(), ['user_id' => 'store_id']);
	}
	public function getFoodcatproducts()
	{
		return $this->hasOne(FoodcategoryProducts::className(), ['product_id' => 'id']);
	}

}