<?php

namespace api\modules\v1\models;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * This is the model class for ForgotPassword.
 *
 * @property integer $email
 * @property string $user
 
 */
class ForgotPassword extends Model
{

    const SCENARIO_ALPHA_USER = 'alpha_user';
    const SCENARIO_CVIEW_USER = 'cview_user';

    /**
     * @inheritdoc
     */
    private $_user=false;
    public $email="";
     
    
    public function rules()
    {
        return [
            ['email','required'],
            ['email', 'validateEmail', 'on' => self::SCENARIO_ALPHA_USER],
            ['email', 'validateEmailCviewUser', 'on' => self::SCENARIO_CVIEW_USER],
        ];
    }

    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First_Name'),
			'lastName' => Yii::t('app','Last_Name'),
			'fullName' => Yii::t('app','Full_Name'),
			'orgName' => Yii::t('app','Org_Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb_Identifier'),
			'role_id' => Yii::t('app','Role_Id'),
			'user_type' => Yii::t('app','User_Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New_Password'),
			'password_reset_token' => Yii::t('app','Password_Reset_Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date_Of_Birth'),
			'ageGroup_id' => Yii::t('app','Age_Group_Id'),
			'walletPinCode' => Yii::t('app','Wallet_Pin_Code'),
			'loyaltyPin' => Yii::t('app','Loyalty_Pin'),
			'auth_key' => Yii::t('app','Auth_Key'),
			'activationKey' => Yii::t('app','Activation_Key'),
			'confirmationKey' => Yii::t('app','Confirmation_Key'),
			'access_token' => Yii::t('app','Access_Token'),
			'hashKey' => Yii::t('app','Hash_Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device_Token'),
			'lastLogin' => Yii::t('app','Last_Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
        ];
    }
    
    
		
    
    /**
     * Validates the email.
     * This method serves as the inline validation for email.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateEmail($attribute, $params)
    {
		
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, 'Email is not register with us.');
            }
        }
    }

    /**
     * Validates the email.
     * This method serves as the inline validation for email.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateEmailCviewUser($attribute, $params)
    {  
        if (!$this->hasErrors()) {
            $user = $this->getUser('cview');
            if (!$user) {
                $this->addError($attribute, 'Email is not register with us.');
            }
        }
    }
    
    /**
     * Finds user by [[emali]]
     *
     * @return User|null
     */
    public function getUser($type=null)
    {
        if ($this->_user === false) {
            
            if ($type == 'cview') {
                // $this->_user = User::findByEmail($this->email);
               $this->_user =  User::find()->where(['email' => $this->email, 'status' => User::STATUS_ACTIVE, 'app_id' => User::USER_TYPE_BIDDER])->one();
            } else {
                // $this->_user = User::findByEmail($this->email);
                $this->_user = User::find()->where(['email' => $this->email, 'status' => User::STATUS_ACTIVE])->andWhere(['<>', 'app_id', User::USER_TYPE_BIDDER])->one();
            }
        }
        return $this->_user;
    
    }
    
    public function getUserObject()
    {
          return $this->_user;
    }
}
