<?php
namespace api\modules\v1\models;
use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use common\models\User;
use common\models\Auction;
use common\models\Property;
use api\modules\v1\models\AccountVerification;
/**
 * RegisterBid Model
 *
 */
class RegisterBid extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    
    const APP_ID = 2;

    public static function tableName()
    {
        return '{{%register_bid}}';
    }

	public function init()
	{
        //$this->app_id = self::APP_ID;
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'auction_id', 'bidding_panel_number'], 'required'],
			[['mobile'], 'integer'],
			[['auction_id', 'user_id', 'otp', 'solicitor_firstname', 'solicitor_lastname', 'solicitor_email', 'solicitor_mobile', 'guarantor_first_name', 'guarantor_lastname', 'guarantor_email', 'bidding_colour', 'bidding_panel_number', 'turn_on_off_notification'], 'safe'],
            [['user_id'], 'validateExistingUser'],
			[['auction_id'], 'validateAuction'],
            [['auction_id'], 'validateAuctionStatus', 'on' => 'Savebid'],
			[['auction_id', 'user_id'], 'validateExistingBid', 'on' => 'Savebid']
        ];

    }
	
	/**
     * Validates the user_id.
     * This method serves as the inline validation for user_id.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
	 */
    public function validateExistingUser($attribute, $params){
		if(!empty($this->user_id)){
        if (!$this->hasErrors() && (User::findIdentity(['id'=>$this->user_id], ['<>', 'app_id', self::APP_ID]))) {
			return true;
		}
			$this->addError($attribute, "The user with user_id \"{$this->user_id}\" does not exist");
		}
    }
    
    public function validateAuctionStatus($attribute, $params){
        $auction = Auction::find()->where(['id'=>$this->auction_id])->one();
        if (isset($auction->auction_bidding_status) && $auction->auction_bidding_status == 1) {
            $this->addError($attribute, "The bidding is restricted.");
        }
    }    
	
	/**
     * Validates the auction_id
     * This method serves as the inline validation for auction_id.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
	 */
    public function validateAuction($attribute, $params){
        $auction = Auction::find()->where(['id'=>$this->auction_id])->one();
		if ($auction) {
			return true;
		}
        $this->addError($attribute, "The Auction with auction_id \"{$this->auction_id}\" does not exist");
        
    }	
   
   
    /**
     * Validates the existing Bid for same user and property.
     * This method serves as the inline validation for Bid.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
	 */
    public function validateExistingBid($attribute, $params)
    {

	    $bid = RegisterBid::find()->where(['user_id'=>$this->user_id, 'auction_id'=>$this->auction_id])->one();
		
        if (!$bid) {
			return true;
		}

        $this->addError($attribute, "The bid with auction_id \"{$this->auction_id}\" and user_id \"{$this->user_id}\" already saved");
         
    }
	
    /* Relation with Profile table*/
	public function getProfile(){
        return $this->hasOne(Profiles::className(),['user_id'=>'user_id']);
    }

    /* Relation with Bidder*/
    public function getBidder(){
        return $this->hasOne(Bidder::className(),['id'=>'user_id']);
    }

    /* Relation with Profile table*/
    public function getAccountverify(){
        return $this->hasOne(AccountVerification::className(),['register_bid_id' => 'id']);
    }
	
    public function fields(){
        $fields = parent::fields();
        //$fields['profile'] = function($model){
        //    return !empty($model->profile)?$model->profile:'';
        //};
        $fields['bidderDetail'] = function($model){
            return !empty($model->bidder)?$model->bidder:'';
        };        
        return $fields;
    }
}