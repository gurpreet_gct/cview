<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;
use Yii;

/**
 * Agency Model
 * To handle data of agents.
 * 
 */
class AgencyOffices extends ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
         return '{{%agencies_offices}}';
    }    
    
    public function init()
    {
     //$this->status=10;   
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			['name','unique'],
			[['mobile'], 'integer'],
			[['name','email','mobile','address','latitude','longitude','agency_id'],'required'],
			['email','unique'],
			['email','email'],
			[['address','latitude','longitude'],'safe'],
        ];     
    
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','Id'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
    
     /**
     * @inheritdoc
     */
   
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
		//	$this->updated_by= Yii::$app->user->id;
			$this->updated_at= time();
			if($insert){			    		
				//$this->created_by= Yii::$app->user->id;
				$this->created_at= time();
			}
		
			return true;
		} else {
			
		 return false; 
		}
	}
	
	public function getAgency(){
		return $this->hasOne(Agency::className(),['id'=>'agency_id']);
	}
	
	public function afterFind()
	{
		parent::afterFind();
		//~ if($this->mobile){
			//~ $this->mobile = '+'.$this->mobile;
		//~ }
	}
	
	
}
