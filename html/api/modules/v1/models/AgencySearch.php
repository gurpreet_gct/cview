<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
/**
 * Agency Model
 * To handle data of agents.
 * 
 */
class AgencySearch extends Agency 
{
   public $sort;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [['id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['name','sort'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','Id'),
            'name' => Yii::t('app','Name'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
    
     /**
     * @inheritdoc
     */
   
	public function search($params)
    {   
        $expression = new Expression('latitude');
		if(!empty($params['AgencySearch']['lat']) && !empty($params['AgencySearch']['long'])){
			$lat = $params['AgencySearch']['lat'];
			$long = $params['AgencySearch']['long'];
			$exp=sprintf('(6371 * acos (cos ( radians(%s) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(%s) ) + sin ( radians(%s) ) * sin( radians( latitude ) ) )) as distance',$lat,$long,$lat);
			$expression = new Expression($exp);
		}
        $query = Agency::find()->select(Agency::tableName().'.*,'.$expression)->where(['status'=>10])
        //->all()
        ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>  [
                'pageSize' => 10,
            ],
        ]);
        
        if (!empty(Yii::$app->getRequest()->getHeaders()->get('Authorization'))) {
            $authToken =  str_replace("Bearer ", "", Yii::$app->getRequest()->getHeaders()->get('Authorization'));
            $user_id = Yii::$app->MyConstant->UserID($authToken);

            if (!empty($params['AgencySearch']['favourites']) && $params['AgencySearch']['favourites'] == 1) {
                $query->joinWith('agencyFavourite')->where([AgencyFavourite::tableName().'.is_favourite' => '1'])->andFilterWhere([AgencyFavourite::tableName().'.user_id' => $user_id]);
            }
        }

        
        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params) 
         * statement below
         */

        //~ $dataProvider->setSort([
            //~ 'attributes' => [
                //~ 'name' => [
                    //~ 'default' => SORT_DESC
                //~ ],
            //~ ]
        //~ ]);
        
		$this->load($params);
		if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
       if($this->name)
        $query->andFilterWhere(['LIKE', 'name', $this->name]);       
        if (!empty($this->sort)) {
			$sort_requested = explode(',', $this->sort);
			foreach ($sort_requested as $value) {
				if (strpos($value, 'name') !== false) {
					 if ($value[0] == '-')
					 $query->orderBy(['name' => SORT_DESC]);
					 else
					 $query->orderBy(['name' => SORT_ASC]);
				}
				$user_id = '';
				
				if (strpos($value, 'favourite') !== false && !empty($user_id)) {
					 if ($value[0] == '-')
					 $query->joinWith('agencyFavourite')->orderBy([AgencyFavourite::tableName().'.is_favourite' => SORT_ASC]);
					 else
					 $query->joinWith('agencyFavourite')->orderBy([AgencyFavourite::tableName().'.is_favourite' => SORT_DESC]);
				}
			}
            //print_r($sort_requested);die;
		}else{
			 $query->orderBy(['name' => SORT_ASC]);
		}
        return $dataProvider;
    }
	
	
}
