<?php
namespace api\modules\v1\models;
use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use common\models\User;
use common\models\Property;
use common\models\Auction;

/**
 * ApplyBid Model
 *
 */
class ApplyBid extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
   

    const APP_ID = 2;

    public static function tableName()
    {
        return '{{%apply_bid}}';
    }

	public function init()
	{
        //$this->app_id = self::APP_ID;
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','auction_id'],'required'],
            // [['user_id','auction_id'],'required'],
            ['rating','required','on'=>'ratings,update-ratings'],
            ['add_to_calendar','required','on'=>'calendar'],
            [['rating',],'required','on'=>'ratings'],
            [['rating'],'compare', 'compareValue' => 1, 'operator' => '>=','message'=>'Must be one star','on'=>'ratings'],
            [['rating',],'required','on'=>'update-ratings'],
            [['rating'],'compare', 'compareValue' => 1, 'operator' => '>=','message'=>'Must be one star','on'=>'update-ratings'],
			[['user_id','auction_id','is_favourite'], 'integer'],
			[['user_id'], 'validateExistingUser','on'=>'create'],
			[['auction_id'], 'validateExistingListing','on'=>'create,ratings'],
			[['auction_id','user_id'], 'validateExistingAuction','on'=>'create'],
			[['auction_id'], 'validateExistingListing','on'=>'ratings'],
			//[['user_id'], 'validateExistingUser','on'=>'ratings'],
			[['auction_id','user_id'], 'validateExistingAuction','on'=>'ratings'],
        ];
    }
	
	
	
	/**
     * Validates the user_id.
     * This method serves as the inline validation for user_id.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
	*/
    public function validateExistingUser($attribute, $params){
        if (!$this->hasErrors() && (User::findIdentity(['id'=>$this->user_id], ['<>', 'app_id', self::APP_ID])))
        {
			return true;
		}
        else
         {
           $this->addError($attribute, "The user with user_id \"{$this->user_id}\" does not exist");
         }
    }
		
		
	/**
     * Validates the auction_id.
     * This method serves as the inline validation for listing_id.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
	 */
    public function validateExistingListing($attribute, $params){
        $auction = Auction::find()->where(['id'=>$this->auction_id])->one();
		
        if ($auction) {
			return true;
		} else {
		   $this->addError($attribute, "The listing with auction_id \"{$this->auction_id}\" does not exist");
        }
    }	
   
   
    /**
     * Validates the existing auction for same user and listing.
     * This method serves as the inline validation for auction.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
	*/
    public function validateExistingAuction($attribute, $params) 
    {
	   $auction = ApplyBid::find()->where(['user_id'=>$this->user_id,'auction_id'=>$this->auction_id])->one();
		if (!$auction) {
			return true;
		} else {
		    $this->addError($attribute, "The auction with auction_id \"{$this->auction_id}\" and user_id \"{$this->user_id}\" already saved");
        }
    }	

	public static function findExistingAuction($params)
    {
        
        $auction = ApplyBid::find()->where(['user_id' => $params['user_id'], 'auction_id' => $params['auction_id']])->count();
        return ($auction != 0 ? true : false);

    }

    public static function findByUserAuction($params)
    {
        $auction = ApplyBid::find()->where(['user_id' => $params['user_id'], 'auction_id' => $params['auction_id']])->one();
        return $auction;

    }

    public function getUser()
    {
		return $this->hasOne(User::className(),['id'=>'user_id']);
	}
	
	public function getProperty()
	{	
		// return $this->auction->hasOne(Property::className(), ['id' => 'listing_id']);
        return $this->hasOne(Property::className(), ['id' => 'listing_id']);
	}

    public function getAuction()
    {   
        return $this->hasOne(Auction::className(), ['id' => 'auction_id']);
    }
	
	public function fields(){
		$fields=parent::fields();
		$fields['user_name']= function ($model) {
			return !empty($model->user->username)?$model->user->username:'';
		};
		return $fields;
	}
    

}
