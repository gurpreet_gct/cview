<?php
namespace api\modules\v1\models;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use common\models\User;
use yii\db\ActiveRecord;
/**
 * WalletPin 
 */
class WalletPin extends Model
{
    
    public $walletpin="",$userid;
    
       
   public function rules()
    {
        return [
        ['walletpin','required'],
        ['walletpin', 'validatePin'],
        [['walletpin'],'safe']
        ];
    }
    
    
    
    public function attributeLabels()
		{
			return [
				'id' => Yii::t('app','ID'),
				'cardType' => Yii::t('app','Card type'),
				'user_id' => Yii::t('app','User_Id'),
				'nameOnCard' => Yii::t('app','Name on Card'),
				'cardNumber' => Yii::t('app','Card Number'), 
				'expiry' =>Yii::t('app','Expiry') ,
				'front' => Yii::t('app','Front Photo'),
				'back' => Yii::t('app','Back Photo'),				
				'created_at' => Yii::t('app','Created At'),
				'updated_at' =>Yii::t('app','Updated At') ,
			];
		}
    
    
    
          /**
     * Validates the validatePin.
     * This method serves as the inline validation for email.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePin($attribute, $params)
    {
		
        if (!$this->hasErrors()) {
            
            $user = Customer::findOne($this->userid);            
            
            if (!$user->validatePassword($this->walletpin)) {
                $this->addError($attribute, 'Invalid pin. Please try again.');
            }
        }
    }
 
   
    
	
	

}
