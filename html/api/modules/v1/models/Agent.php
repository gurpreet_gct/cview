<?php
namespace api\modules\v1\models;
use Yii;
use common\models\User;
use api\modules\v1\models\Profiles;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
/**
 * Bidder Model
 *
 *
 */
class Agent extends User
{
	/**
     * @inheritdoc
     */
    public $imageFile;
    public $when;
    public $number_max;
    public $suggestedUsername;
    public $userNames;
    public $usernameChanged=true;

    const APP_ID = 2;

    public static function tableName()
    {
        return '{{%users}}';
    }

	public function init()
	{
		$this->status=10;
		$this->user_type=10;
        $this->app_id = self::APP_ID;
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status','phone'], 'integer'],
           // [['firstName', 'lastName','agency_id','dob'], 'required' , 'on' => 'update'],
            //[['id'], 'required' ,'on'=>'updateAgent'],
            ['username','required','when'=>function($model){
			   return ($model->fbidentifier=="") && ($model->google_identifier == "") && ($model->linkedin_identifier == "");
            }],
            [['username'], 'validateRequiredUnique', 'on'=>'create'],
            [['email'], 'required', 'on' => 'create'],
				//['password_hash','required','on'=>'create'],
            ['email', 'validateUniqueEmail', 'message' => 'That email address has already been registered. Please enter a valid email address.', 'on' => 'create'],
            ['email', 'email'],
           // [['dob', 'sex'], 'required','on' => 'create,update'],
           // [[ 'user_type', 'status'], 'required',  'on' => 'create,update'],
            ['sex', 'number'],
            ['dob', 'date', 'format' => 'yyyy-M-d'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            // ['phone', 'string', 'min' =>1, 'max' => 15],
            [['username'], 'string', 'max' => 50],
            [['referralCode', 'user_code', 'latitude', 'longitude', 'device_token', 'device', 'firstName', 'lastName', 'username', 'auth_key', 'fullName', 'user_type', 'app_id', 'email', 'orgName', 'fbidentifier', 'linkedin_identifier', 'google_identifier', 'role_id', 'password_hash', 'password_reset_token', 'phone', 'image', 'sex', 'dob', 'ageGroup_id', 'bdayOffer', 'walletPinCode', 'loyaltyPin', 'advertister_id', 'activationKey', 'confirmationKey', 'access_token', 'hashKey', 'status',  'lastLogin', 'timezone', 'timezone_offset', 'created_at', 'updated_at', 'storeid', 'promise_uid', 'promise_acid', 'referral_percentage', 'agency_id'], 'safe', ],
            ['referralCode','referralCodeValidate']
        ];

    }

	public function referralCodeValidate($attribute, $params){

		if ($this->$attribute != '') {

            if(self::findOne(['user_code'=>$this->referralCode]))
            {
				return true;
			}else{
				 $this->addError($attribute, 'Referral code not found');
			}
        }

        return $this->referralCode;
	}
	
    /**
     * Validates the email.
     * This method serves as the inline validation for email.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
    */
    public function validateUniqueEmail($attribute, $params){
        $customerdata  = User::findOne(['email'=>$this->email, 'app_id' => self::APP_ID]);
        if(!empty($customerdata)){
			$this->addError($attribute, 'Email '.$this->email.' has already been registered. Please enter a valid email address.');
            return false;
        }
        return true;

    }

    /**
     * Validates the username.
     * This method serves as the inline validation for username.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
	*/
    public function validateRequiredUnique($attribute, $params){
        $customerdata  = Bidder::findOne(['username'=>$this->username, 'app_id' => self::APP_ID]);

        if($customerdata!=''){
			$this->userNames =  $customerdata->username;
			$this->firstName = $this->firstName !=''?$this->firstName:$customerdata->username;
			$this->lastName = $this->lastName !=''?$this->lastName:$customerdata->username;
			//$this->email = $customerdata->email;
	    }

        if (!$this->hasErrors() && (self::findOne(['username'=>$this->username, 'app_id' => self::APP_ID])))
            {

				if(!$this->usernameChanged)
				{
					return true;
				}

                if($this->fbidentifier!="")
                {
                    $this->getSuggestedUser(true);
                    $this->username=$this->suggestedUsername;
                }
                if($this->google_identifier!="")
                {
                    $this->getSuggestedUser(true);
                    $this->username=$this->suggestedUsername;
                }
                if($this->linkedin_identifier!="")
                {
                    $this->getSuggestedUser(true);
                    $this->username=$this->suggestedUsername;
                }
                else
                {
                    $this->getSuggestedUser();
                    $usernames = implode(', ',$this->suggestedUsername);
                    $this->addError($attribute, "The username \"{$customerdata->username}\" has already been taken. The following usernames are available: \"{$usernames}\". ");
                   // $this->addError('suggestedusername', $this->suggestedUsername);
                }


        }
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
           'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last Name'),
			'fullName' => Yii::t('app','Full Name'),
			'orgName' => Yii::t('app','Org Name'),
			'email' => Yii::t('app','Email'),
            'app_id' => Yii::t('app','App Id'),
			'fbidentifier' => Yii::t('app','fb Identifier'),
			'role_id' => Yii::t('app','Role'),
			'user_type' => Yii::t('app','User Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','Password'),
			'password_reset_token' => Yii::t('app','Password Reset Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date Of Birth'),
			'ageGroup_id' => Yii::t('app','Age Group Id'),
			'walletPinCode' => Yii::t('app','Wallet Pin Code'),
			'loyaltyPin' => Yii::t('app','Loyalty_Pin'),
			'auth_key' => Yii::t('app','Auth Key'),
			'activationKey' => Yii::t('app','Activation Key'),
			'confirmationKey' => Yii::t('app','Confirmation Key'),
			'access_token' => Yii::t('app','Access Token'),
			'hashKey' => Yii::t('app','Hash Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device_Token'),
			'lastLogin' => Yii::t('app','Last Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
        ];
    }

    public static function findByUsernameOrEmail($email)
	{

        if (strstr($email,'@')) {
            return static::find()
                        ->where('email = :email', [':email' => $email])
                        ->andWhere('status =:status', [':status'=> self::STATUS_ACTIVE])
                        ->andWhere(['user_type'=>[10]])
                        ->one();
        }
        return static::find()
                         ->where('username = :username', [':username' => $email])
                         ->andWhere('status =:status', [':status'=> self::STATUS_ACTIVE])
                         ->andWhere(['user_type'=>[10]])
                         ->one();

	}

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        //throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
            
          return static::findOne(['auth_key' => $token,'user_type'=>[10]]);
    }
    
	public function fields(){
		$fields=parent::fields();
    	$fields['logo']= function ($model) {
			return !empty($model->agency->logo)?$model->agency->logo:'';
		};
		return $fields;
	}

    public function extraFields()
    {
        return ['profile'];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			$this->fullName= trim($this->firstName . ' ' . $this->lastName);

               if(strstr($this->dob,"-"))	{
				$this->dob = strtotime($this->dob);
				}


             if($this->fbidentifier=="" &&  $this->image!="")
             {
                   preg_match('/.*\/(.*?)\./',$this->image,$match);
                    if(count($match))
                        $this->image=$match[1]. ".jpg";

             }
            if ($this->isNewRecord) {
				// referral code
				$this->user_code = Yii::t('app', 'AW-').$this->username;
              //  $this->password_hash=Yii::$app->security->generatePasswordHash($this->password_hash);
                if($this->username=="")
                {
                    $this->getSuggestedUser(true);
                    $this->username=$this->suggestedUsername;
                }
            }
            return true;
        }
        return false;
    }

    public function afterFind()
    {
        parent::afterFind();
        if(!strstr($this->dob,"-"))
        {
            $this->dob=date("Y-m-d",$this->dob);
        }
        if(($this->image!="")&&(!strstr($this->image,'http')))
		{
            $this->image=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$this->image);
		}
       return true;
    }

    public function getProfile()
    {
       return $this->hasOne(Profiles::className(), ['user_id' => 'id']);
    }

	public function getImageUrl()
	{
        if(($this->image!="")&&(!strstr($this->image,'http')))
		{
            $this->image=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$this->image);
		}
    }

    public function getSuggestedUser($single=false)
    {
		$isUsername = $this->username = $this->email ? true:($this->username!=""?true:false);
        if($isUsername)
            $this->fullName =   $this->firstName.' '. $this->lastName;
            $this->username = $this->email!=""?$this->email:$this->fullName;
		    $username=preg_replace('/([^@]*).*/', '$1',$this->username);
			//$username=preg_replace('/[^a-zA-Z0-9.-_\']/', '_', $username);
            $dob=strstr($this->dob,"-")?$this->dob: date("Y-m-d",$this->dob);
            list($month, $day, $year) = explode('-', $dob);
            $usernameSuggestion[]=$username;
            if($isUsername)
            {
			/*array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.$this->lastName)); */
            // array_push($usernameSuggestion,$username.rand(1,99));
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.'_'.rand(0,999)));
			array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->lastName.'.'.rand(0,999)));
			array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '',$this->userNames.'_'.rand(100,1000)));
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.".".$this->lastName));
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."-".$this->lastName));
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."_".$this->lastName));
			array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.".".$day));
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.$month));
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.".".$month));
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.$dob));
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.$this->fullName));
            array_push($usernameSuggestion,$username. rand(100,1000));
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username."-".$day));
            array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username."-".$month));
            $username = array($username);
            }
            else
            {
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.$this->fullName));
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.$this->lastName));
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.".".$this->lastName));
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."-".$this->lastName));
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."_".$this->lastName));

                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.$day));

                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.".".$day));
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.$month));
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName.".".$month));
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $username.$dob));
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."-".$day));
                array_push($usernameSuggestion,preg_replace('/[^a-zA-Z0-9.-_\']/', '', $this->firstName."-".$month));
                array_push($usernameSuggestion,$username. rand(1,99));
                array_push($usernameSuggestion,$username. rand(100,1000));
            }

            $result=ArrayHelper::map(Bidder::find()->where(['username'=>$usernameSuggestion])->select('username')->asArray()->all(),'username', 'username');


            $result=array_unique(array_diff($usernameSuggestion,$result));

          //  $newusername =array('username'=>$username);
           //$result=array_unique(array_diff($result,$username));

           $result=array_unique(array_diff($result,$username));
			foreach($result as $key => $value){
				if(is_numeric($value)) unset($result[$key]);
			}
            $result = array_map('strtolower', $result);
            if(!$result)
                array_push($result,$username. time());

            $result=array_values($result);
            if($single)
            {
              $this->suggestedUsername=$result[0]  ;
            }
                else
            $this->suggestedUsername=array_slice($result, 0, 5, true);

    }

    public function saveProfileData($profileModel, $data) {
        // $objModelData = (object) $data;
        foreach ($data as $key => $value) {
            $profileModel->{$key} = $value;
        }
        $profileModel->save(false);
        return $profileModel;
    }
    
    public function getAgency(){
		return $this->hasOne(Agency::className(),['id'=>'agency_id']);
	}

}
