<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;
use common\models\User;
use api\modules\v1\models\Profiles;
use Yii;

/**
 * User Store Model
 *
 * 
 */
class Manufacturer extends User 
{
	  /**
     * @inheritdoc
     */
	public $params=[]; 
	public $isSelected; 
    
	public function init()
	{
		 $this->params['user_id']=0;
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
        ];     
    
    }
    
    
    
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First_Name'),
			'lastName' => Yii::t('app','Last_Name'),
			'fullName' => Yii::t('app','Full_Name'),
			'orgName' => Yii::t('app','Org_Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb_Identifier'),
			'role_id' => Yii::t('app','Role_Id'),
			'user_type' => Yii::t('app','User_Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New_Password'),
			'password_reset_token' => Yii::t('app','Password_Reset_Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date_Of_Birth'),
			'ageGroup_id' => Yii::t('app','Age_Group_Id'),
			'walletPinCode' => Yii::t('app','Wallet_Pin_Code'),
			'loyaltyPin' => Yii::t('app','Loyalty_Pin'),
			'auth_key' => Yii::t('app','Auth_Key'),
			'activationKey' => Yii::t('app','Activation_Key'),
			'confirmationKey' => Yii::t('app','Confirmation_Key'),
			'access_token' => Yii::t('app','Access_Token'),
			'hashKey' => Yii::t('app','Hash_Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device_Token'),
			'lastLogin' => Yii::t('app','Last_Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
        ];
    }
    
    
    
    
    
  public function fields()
	{
		$fields=parent::fields();
		unset($fields['password_hash']);
		unset($fields['password_reset_token']);
		unset($fields['created_at']);
		unset($fields['updated_at']);
		unset($fields['user_type']);
		unset($fields['activationKey']);
		unset($fields['access_token']);
		unset($fields['hashKey']);
		unset($fields['lastLogin']);
		unset($fields['status']);
		unset($fields['role_id']);
		unset($fields['confirmationKey']);
		unset($fields['device']);
		unset($fields['fbidentifier']);
		unset($fields['username']);
		unset($fields['image']);
		unset($fields['sex']);
		unset($fields['walletPinCode']);
		unset($fields['auth_key']);
		$fields=["id","firstName","lastName","fullName","orgName","email","phone","dob","latitude","longitude","isSelected"];
		return $fields;
	}
    
    public function extraFields()
    {
		$fields=['address'];
		unset($fields['id']);
        return $fields;
    }
	

    
     public function getAddress()
    {
       return $this->hasOne(Profiles::className(), ['user_id' => 'id']);       
    }
	
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			
            if ($this->isNewRecord) {
               $this->created_at=time();
            }
			     $this->updated_at=time();							
            return true;
        }
        return false;
    }
    
     public function getProfile()
    {
       return $this->hasOne(Profiles::className(), ['user_id' => 'id']);       
    }
 
    public function getUserStore()
    {
		
        return $this->hasOne(UserStore::className(), ['store_id' => 'id'])
		->andOnCondition(UserStore::tableName().'.`status` = :status', [':status'=>1]);
		//->andOnCondition('`user_id` = :user_id', [':user_id'=>$this->params['user_id']]);
    }
    
  /* To get all Store */
     public function getManufacturer($user_id)
     {
		 $this->params['user_id']=$user_id;
           $result = self::find()
					->select(["tbl_profile.brandLogo","tbl_profile.foregroundHexColour","tbl_profile.backgroundHexColour","tbl_users.id","tbl_users.orgName",'ifnull(if(store_id!=0,1,0),0) as isSelected'])
					->joinWith([
				'userStore'=>function($query) use ($user_id) {
																$query->andOnCondition(['=', 'user_id', $user_id]);
															},
				],false,"LEFT JOIN")
				 ->innerJoinWith('profile', false)
			
					//->leftJoin('userStore', 'store_id = `id` and status=1 and user_id='.$user_id)
					->where(['user_type'=> self::ROLE_Advertiser ])
					->andWhere([self::tableName().'.status'=> 10 ])
					->orderBy(['orgName'=>SORT_ASC])
					->asArray()
					->All(); 	
					
			
					$datam = array();
					foreach($result as $data){
						$val = array();
						if($data['brandLogo']){
							$val['brandLogo'] = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$data['brandLogo']);
						}
						else{
						$val['brandLogo'] = $data['brandLogo'];						
						}
						if(strstr($data['backgroundHexColour'],'.')){
						$val['backgroundHexColour'] = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$data['backgroundHexColour']);
						}else{
						$val['backgroundHexColour'] = $data['backgroundHexColour'];
						}
						$val['foregroundHexColour'] = $data['foregroundHexColour'];
						
						$val['id']					 = $data['id'];
						$val['orgName'] 			= $data['orgName'];
						$val['isSelected']			 = $data['isSelected'];
						$datam [] = $val;
					}
						
						return($datam);
					
					
     } 
     
     
     
	 
	 public function getManufacturerInfo(){
		 return $model=self::find()    
		->select(["tbl_profile.*","tbl_users.*"])
		 ->where(['tbl_users.id'=>$this->id,'user_type'=>Store::ROLE_Advertiser])
		 //->joinWith('profile')
		 ->innerJoinWith('profile', false)
		 ->one();
	 }
}
