<?php
namespace api\modules\v1\models;
use Yii;

/**
 * Preference Model
 *
 * 
 */
class Preference extends ActiveRecord 
{
	  /**
     * @inheritdoc
     */
  
      
    public static function tableName()
    {
         return '{{%category}}';
    }
    
    
	public function init()
	{
		
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
        ];     
    
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
           'id_countries' => Yii::t('app','Id_Countries'),
			'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last Name'),
            'name' => Yii::t('app','Name'),
			'sortname' => Yii::t('app','Sort_Name'),
			'created_at' => Yii::t('app','Created At'),
			'updated_at' => Yii::t('app','Updated At'),
        ];
    }

	
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			$this->fullName= trim($this->firstName . ' ' . $this->lastName);
            if ($this->isNewRecord) {
               
            }
            return true;
        }
        return false;
    }
    
}
