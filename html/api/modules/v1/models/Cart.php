<?php
namespace api\modules\v1\models;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use common\models\User;
use common\models\Workorders;
use common\models\OrderItem;
use common\models\UserLoyalty;
use common\models\Loyalty;
use common\models\Pass;
use common\models\Workorderpopup;
use yii\db\Expression;
use yii\db\ActiveRecord;
 
/**
 * Cart 
 */
class Cart extends \yii\db\ActiveRecord
{
    
    public $data=[],$invalidDeals=[],$rows=[],$session_key,$total=0,$qty=0,$discount=0,$loyalty,$useloyalty=0,$storeTotal=[],$payable,$loyaltyOrderData=[],$dealsID=[];
    public $eventTickets=[],$loyaltyPointsUsed=0;
    
   public $foodorder;
   public $foodorder_id;
   public $creditCardId,$creditCardNumber,$creditCardMonth,$creditCardYear,$creditCardCVV,$creditCardName;
    
    public static function tableName()
    {
        return '{{%cart}}';
    }
       
   public function rules()
    {
        return [
     //   ['session_key','deal_id','required'],
        [['data'], 'validateCart'],
        [['foodorder_id','foodorder','session_key','deal_id'], 'safe'],
        [['creditCardId','creditCardNumber','creditCardMonth','creditCardYear','creditCardCVV','creditCardName'], 'safe'],
        ];
    }
     
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app','User_ID'),
            'deal_id' => Yii::t('app','Deal_ID'),
            'dealCount' => Yii::t('app','Deal_Count'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created_At'),
           
        ];
    }
 
    
 
          /**
     * Validates the validateCart.
     * This method serves as the inline validation for cart items, deals.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateCart($attribute, $params)
    {
         if (!$this->hasErrors()) {            
           $invalid=false;
           
           if($this->session_key=="")
           $this->session_key = md5(time().'swpa');
           $created_at=time();
           $store_ids=[];
            foreach($this->data as $key=>$value)
           {               
			   foreach($value as $item)
               {
                   if($this->foodorder==1){
                       
                           array_push($store_ids,$item->storeID);
                           array_push($this->rows,['session_key'=>$this->session_key,
                           'user_id'=>Yii::$app->user->id,
                           'deal_id'=>$item->workOrderID,
                           'dealCount'=>$item->dealCount,
                           'status'=>1,
                           'created_at'=>$created_at
                           ]);                           
                           $this->total+=$item->total;
                           $this->discount+=$item->discount;
                           $this->qty+=$item->quantity;
                           if(isset($this->storeTotal[$item->storeID]))
                           {
                               $this->storeTotal[$item->storeID]["total"]+=$item->total;
                               $this->storeTotal[$item->storeID]["discount"]+=$item->discount;
                               $this->storeTotal[$item->storeID]["loyalty"]=0;
                           }
                           else
                           {
                               $this->storeTotal[$item->storeID]["total"]=$item->total;
                               $this->storeTotal[$item->storeID]["discount"]=$item->discount;
                               $this->storeTotal[$item->storeID]["loyalty"]=0;
                           }
                           
                   } 
                   
                   else{                    
                   $workorder=Workorders::find()
                           ->select([new \yii\db\Expression('sum(ifnull(dealcount,0))+usedPasses as used'),'noPasses',
                           new \yii\db\Expression('if(noPasses>0,noPasses-sum(ifnull(dealcount,0))+usedPasses,0) as valid')
                           ,'type',Workorders::tableName().'.id',new Expression(Workorders::tableName().".id as deal_id"),Workorderpopup::tableName().'.offerTitle','offerText','offerwas','offernow','readTerms','sspMoreinfo','passUrl','logo','photo'])
                           ->joinWith(['cartcount'=> function ($q) {           
                               $q->andOnCondition([Cart::tableName().'.status'=>1])
                               ->andOnCondition(Cart::tableName().'.created_at+720 < unix_timestamp()');
                                       }
                                       ],false,'LEFT JOIN')  
                                       ->joinWith("workorderpopup")
                                       ->where([Workorders::tableName().'.id'=>$item->workOrderID])
                                       ->groupBy(Workorders::tableName().".id")
                           ->asArray()            
                           ->one();
                       
                           array_push($this->dealsID,[
                                   'workorder_id'=>$item->workOrderID,
                                   'count_passes'=>$item->dealCount
                                   ]);
                           if(!$workorder['valid'])
                           {
                               $invalid=true;
                               array_push($this->invalidDeals,['workOrderID'=>$workorder['deal_id'],
                               'storeID'=>$item->storeID,
                               'msg'=> "This deal is not availble for the movement."
                               ]);
                           }else if($workorder['valid'] < $item->dealCount)
                           {
                               $invalid=true;
                               array_push($this->invalidDeals,['workOrderID'=>$workorder['deal_id'],
                               'storeID'=>$item->storeID,
                               'msg'=> "Only ".$workorder['valid']. " coupon are allowed. Please change qty of deal."
                               ]);
                           }
                           
                           
                           
                           if($workorder['type']==4)
                           {
                               array_push($this->eventTickets,[                                
                               'user_id'=>Yii::$app->user->id,
                               'store_id'=>$item->storeID,
                               'workorder_id'=>$item->workOrderID,
                               'offerTitle'=>$workorder['offerTitle'],
                               'offerText'=>$workorder['offerText'],
                               'offerwas'=>$workorder['offerwas'],
                               'offernow'=>$workorder['offernow'],
                               'readTerms'=>$workorder['readTerms'],
                               'sspMoreinfo'=>$workorder['sspMoreinfo'],
                               'created_at'=>$created_at,
                               'updated_at'=>$created_at,
                               'isUsed'=>0,
                               'count_passes'=>$item->dealCount
                               ]);
                           }
                           
                           
                           
                           array_push($store_ids,$item->storeID);
                           array_push($this->rows,['session_key'=>$this->session_key,
                           'user_id'=>Yii::$app->user->id,
                           'deal_id'=>$item->workOrderID,
                           'dealCount'=>$item->dealCount,
                           'status'=>1,
                           'created_at'=>$created_at
                           ]);
                           
                           $this->total+=$item->total;
                           $this->discount+=$item->discount;
                           $this->qty+=$item->quantity;
                           if(isset($this->storeTotal[$item->storeID]))
                           {
                               $this->storeTotal[$item->storeID]["total"]+=$item->total;
                               $this->storeTotal[$item->storeID]["discount"]+=$item->discount;
                               $this->storeTotal[$item->storeID]["loyalty"]=0;
                           }
                           else
                           {
                               $this->storeTotal[$item->storeID]["total"]=$item->total;
                               $this->storeTotal[$item->storeID]["discount"]=$item->discount;
                               $this->storeTotal[$item->storeID]["loyalty"]=0;
                           }
                           
 
                   } //else
           }
       
           }         
           
           $this->payable=$this->total-$this->discount;
           $storeids=implode(",",$store_ids);
            if ($invalid) {
               if(count($this->invalidDeals)>1)
                $this->addError($attribute, 'Few deals are not available for the movement.');
                else
                $this->addError($attribute, 'Deal is not available for the movement.');
                $this->addError("invalidDeals",$this->invalidDeals);
           return false;
            }
            else if(($storeids))
            {
               
               $this->loyalty=UserLoyalty::find()
               ->select(["store_id","value","points"])
               ->where(["user_id"=>Yii::$app->user->id])
               ->andWhere("store_id in (". $storeids .")")
           //    ->andWhere(["status"=>1])
               //->groupBy("store_id")
               //->orderBy("id")
               ->asArray()
               ->all();
               if($this->useloyalty)
               foreach($this->loyalty as $key=>$value)
               {
                   if(($this->storeTotal[$value["store_id"]]["total"] - $this->storeTotal[$value["store_id"]]["discount"]) >= $value["value"])
                   {
                       $this->storeTotal[$value["store_id"]]["loyalty"]=$value["value"];
                       $this->loyaltyPointsUsed+=$value["points"];
                       
                   }
                   else
                   {
                       
                       $this->storeTotal[$value["store_id"]]["loyalty"]=$this->storeTotal[$value["store_id"]]["total"]- $this->storeTotal[$value["store_id"]]["discount"];
                       $this->loyaltyPointsUsed+=$value["points"]/$value["value"]*$this->storeTotal[$value["store_id"]]["loyalty"];
                   }
                   $this->payable-=$this->storeTotal[$value["store_id"]]["loyalty"];
                   
               
               }
           
           }
        }
    }
 
   
     public function getWorkorderpopup()
    {
         return $this->hasOne(Workorderpopup::className(), ['id' => 'workorder_id']);
    }
   
   
 
}

