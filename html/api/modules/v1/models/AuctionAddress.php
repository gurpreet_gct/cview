<?php
namespace api\modules\v1\models;
use Yii;
use common\models\Auction;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
/**
 * AuctionEvent Model
 * To handle data of an auction event.
 * 
 */
class AuctionAddress extends ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName(){
         return '{{%auction_address}}';
    }    
    
    public function init(){
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['auction_event_id'], 'integer'], 
            // [['cv_agency_id'], 'required' ,'on'=>'updateAgency'],
            // [['name','email','mobile','cv_agency_id'],'required', 'on'=>'create'],
            ['email','email'],
            [['auction_event_id', 'address_line_1', 'address_line_2', 'city', 'state', 'pincode', 'country', 'mobile', 'email', 'auction_start_time'],'safe'],
            ['mobile', 'string', 'min' =>1, 'max' => 15], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => Yii::t('app','Id'),
            'auction_event_id' => Yii::t('app','Auction Event Id'),
        ];
    }
    
    /**
     * @inheritdoc
     */
   	public function beforeSave($insert){
		if (parent::beforeSave($insert)) {
            /*if(strstr($this->auction_start_time,"-"))	{
                $this->auction_start_time = strtotime($this->auction_start_time);
            } */       
			$this->updated_at = time();
            if ($this->isNewRecord) {
				$this->created_at = time();
			}
			return true;
		} else {
            return false; 
		}
	}

	public function getAuctionById($id){
		return $this->findOne(['id' => $id]);
	}

		
	public function afterFind(){
		parent::afterFind();
        if( !strstr($this->auction_start_time, "-") ){
            $this->auction_start_time = date("Y-m-d H:i:s", $this->auction_start_time);
        }
        if( !strstr($this->created_at, "-") ){
            $this->created_at = date("Y-m-d H:i:s", $this->created_at);
        }
        if( !strstr($this->updated_at, "-") ){
            $this->updated_at = date("Y-m-d H:i:s", $this->updated_at);
        }
		return true;		
	}
    
	
	public function fields(){
		$fields = parent::fields();
        $fields['country_name'] = function(){
			return $this->getCountryName();
		};     
		$fields['auction_detail'] = function($model){
			return $model->auction;
		};
		return $fields;
	}

    public function getAuction(){
       return $this->hasOne(Auction::className(), ['id' => 'auction_event_id']);
    }
    
    // To Get the Country Name by Id
	public function getCountryName(){
        $countryModel = new Country();
        $data = $countryModel->findOne( ['id_countries' => $this->country] );
        return ($data['name']) ? $data['name'] : 'N-A';
	}    

}