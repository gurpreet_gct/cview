<?php
namespace api\modules\v1\models;
use common\models\UserAgentLoginForm;
use api\modules\v1\models\Agent;
use Yii;
use yii\base\Model;

/**
 * UserAgent
 */
class UserAgentLogin extends UserAgentLoginForm
{
    
    private $_user=false;
   
   
	public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First_Name'),
			'lastName' => Yii::t('app','Last_Name'),
			'fullName' => Yii::t('app','Full_Name'),
			'orgName' => Yii::t('app','Org_Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb_Identifier'),
			'role_id' => Yii::t('app','Role_Id'),
			'user_type' => Yii::t('app','User_Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New_Password'),
			'password_reset_token' => Yii::t('app','Password_Reset_Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date_Of_Birth'),
			'ageGroup_id' => Yii::t('app','Age_Group_Id'),
			'walletPinCode' => Yii::t('app','Wallet_Pin_Code'),
			'loyaltyPin' => Yii::t('app','Loyalty_Pin'),
			'auth_key' => Yii::t('app','Auth_Key'),
			'activationKey' => Yii::t('app','Activation_Key'),
			'confirmationKey' => Yii::t('app','Confirmation_Key'),
			'access_token' => Yii::t('app','Access_Token'),
			'hashKey' => Yii::t('app','Hash_Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device_Token'),
			'lastLogin' => Yii::t('app','Last_Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
        ];
    }
   
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {	
		if (!$this->hasErrors()) {
            $user = $this->getUser();
            $pasMessage = 'password';
            if (isset($user->user_type) && ($user->user_type == 10) ) {
				 $pasMessage ='Pin';
			}
            
            if ($user && !$user->validatePassword($this->password))
            {
				$this->addError($attribute, "That  $pasMessage does not match the registered email address. Please try again or click on Forgot  $pasMessage? to reset it.");
			}
			if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, "Please enter a registered email address and  $pasMessage.");
            }
			
        }
    }
   
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
    	if ($this->_user === false) {
            $this->_user = Agent::findByUsernameOrEmail($this->username);
        }

        return $this->_user;
    }
	
	

}
