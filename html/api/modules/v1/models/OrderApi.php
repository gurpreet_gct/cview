<?php
namespace api\modules\v1\models;

use Yii;
use common\models\Order;
use common\models\User;

class OrderApi extends Order
{
  public $store_id;
  public $pin;
   public function rules()
    {
        return [
		
			[['id','store_id'], 'required','on'=>'finalize'],
			[['id'], 'required','on'=>'active-order'],
			[['pin',],'validatepin','on'=>'finalize','skipOnEmpty' => false],
        ];
    }
	public function attributeLabels(){
		return[
			'id'=> Yii::t('app','Order id'),
			'store_id'=> Yii::t('app','Store id'),
			'pin'=> Yii::t('app','Employee Pin'),
		]; 
	}
	public function validatepin($attribute, $params){
		if($this->pin!=''){
		
			$checkPin = User::findOne(['advertister_id' =>
										$this->store_id, 
										'loyaltyPin' => 
										$this->pin]
									);	
			if($checkPin !=''){
				
				}else{
				$this->addError('pin', 'That PIN does not match your registered 6-digit PIN. Please try again.');
				}
		}else{
			$this->addError('pin', 'Employee Pin is required');
		}
		
		
	
	}
}
