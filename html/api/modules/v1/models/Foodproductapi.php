<?php
namespace api\modules\v1\models;

use Yii;
use common\models\FoodCategories;

class Foodproductapi extends FoodCategories
{
	public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'root' => Yii::t('app','Root'),
			'lft' => Yii::t('app','Lft'),
			'rgt' => Yii::t('app','Rgt'),
			'lvl' => Yii::t('app','Lvl'),
			'name' => Yii::t('app','Name'),
			'parentKey' => Yii::t('app','Parent_Key'),
			'icon' => Yii::t('app','Icon'),
			'imageurl' => Yii::t('app','Image_Url'),
			'icon_type' => Yii::t('app','Icon_Type'),
			'active' => Yii::t('app','Active'),
			'selected' => Yii::t('app','Selected'),
			'disabled' => Yii::t('app','Disabled'),
			'readonly' => Yii::t('app','Readonly'),
			'visible' => Yii::t('app','Visible'),
			'collapsed' => Yii::t('app','Collapsed'),
			'moveable_u' => Yii::t('app','Moveable_U'),
			'moveable_d' => Yii::t('app','Moveable_D'),
			'moveable_r' => Yii::t('app','Moveable_R'),
			'moveable_l' => Yii::t('app','Moveable_L'),
			'removeable' => Yii::t('app','Removeable'),
			'removeable_all' => Yii::t('app','Removeable_All'),
			'path' => Yii::t('app','Path'),
			
			
           ];
    }

}
