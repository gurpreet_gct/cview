<?php

namespace api\modules\v1\models;
use Yii;
use yii\base\Model;
use common\models\User;
use api\modules\v1\models\Customer;
use linslin\yii2\curl;
/**
 * This is the model class for FacebookLogin.
 *
 * @property integer $email
 * @property string $user
 
 */
class FacebookLogin extends Model
{
    /**
     * @inheritdoc
     */
    private $_user=false;
    public $token="";
    public $_tokenError=false;
    public $apiData=false;
    public $device;    
    public function rules()
    {
        return [
        [['token','device'],'required'],
        ['token', 'validateToken'],
        ['device', 'in','range'=>['android','ios'],'message'=>'Device can be ios or android.'],
        ];
    }


	public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last_Name'),
			'fullName' => Yii::t('app','Full_Name'),
			'orgName' => Yii::t('app','Org_Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb_Identifier'),
			'role_id' => Yii::t('app','Role_Id'),
      'app_id' => Yii::t('app', 'App id'),
			'user_type' => Yii::t('app','User_Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New_Password'),
			'password_reset_token' => Yii::t('app','Password_Reset_Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date_Of_Birth'),
			'ageGroup_id' => Yii::t('app','Age_Group_Id'),
			'walletPinCode' => Yii::t('app','Wallet_Pin_Code'),
			'loyaltyPin' => Yii::t('app','Loyalty_Pin'),
			'auth_key' => Yii::t('app','Auth_Key'),
			'activationKey' => Yii::t('app','Activation_Key'),
			'confirmationKey' => Yii::t('app','Confirmation_Key'),
			'access_token' => Yii::t('app','Access_Token'),
			'hashKey' => Yii::t('app','Hash_Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device_Token'),
			'lastLogin' => Yii::t('app','Last_Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
        ];
    }
	
    /**
     * Validates the token.
     * This method serves as the inline validation for token.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateToken($attribute, $params)
    {
		
        if (!$this->hasErrors()) {
            $user = $this->getUser();            
            if (!$user) {
                if($this->_tokenError) {
                    $this->addError($attribute, 'Invalid token');
                }
                else {
                    $this->addError("apiData", $this->apiData); 
                }
            }
        }
    }
    
    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
           
          //Init curl
          $curl = new curl\Curl();
          $curl->setOption(CURLOPT_SSL_VERIFYPEER,false);    
          $url=sprintf("https://graph.facebook.com/me?access_token=%s&fields=id,name,gender,email,first_name,last_name",$this->token);
          $response = $curl->get($url);  
          if($response)
          {
            
              $ob=json_decode($response);
              $this->apiData=$ob;
              $app_id = false;
              if (isset($_REQUEST['app_id'])) {
                  $app_id = $_REQUEST['app_id'];
              }
              $this->_user = Customer::findByIdentifier($ob->id, 'fb', $app_id);
              if($this->_user)
              {
                  $this->_user->firstName=$ob->first_name;
                  $this->_user->lastName=$ob->last_name;
                  $this->_user->sex=$ob->gender=="male"?1:0;
                  $this->_user->fullName=$ob->name;
                  $this->_user->generateAuthKey();
                  $this->_user->device=$this->device;
                  $this->_user->save(false);
              } 
          
          }
          else {
              $this->_tokenError= true;
          }
 
        }

        return $this->_user;
    }
    
    public function getUserObject()
    {
         $this->_user->dob=date("Y-m-d", $this->_user->dob);
          return $this->_user;
    }
    
    
}
