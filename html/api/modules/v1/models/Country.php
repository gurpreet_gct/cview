<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;
/**
 * Country Model
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class Country extends ActiveRecord 
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tbl_countries';
	}

    /**
     * @inheritdoc
     */
    

    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['code', 'name', 'population'], 'required'],
            [['code', ], 'unique'],
            [['population', ], 'number']
        ];
    }
    
    
    public function attributeLabels()
		{
			return [
				'id_countries' => Yii::t('app','Id_Countries'),
				'sortname' => Yii::t('app','Sortname'),
				'name' => Yii::t('app','Name'),
				
			];
		}
    
    

    /* public function afterFind()
    {
       // parent::afterFind();
               $this->name=date("Y-m-d",strtotime("2015-10-10"));//date("Y-m-d",$this->dob);
       if (parent::afterFind()) {
        // ...custom code here...
         $this->name=date("Y-m-d",strtotime("2015-10-10"));//date("Y-m-d",$this->dob);
          

        return true;
    } else {
        return false;
    }
    }*/
   
}
