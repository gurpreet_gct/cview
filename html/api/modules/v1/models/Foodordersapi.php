<?php
namespace api\modules\v1\models;

use Yii;
#use common\models\FoodordersItem;
#use common\models\FoodProduct;
use common\models\Foodorders;
use common\models\Profile;

class Foodordersapi extends Foodorders
{
		public $item;
		public $lat;
		public $lang;
		public $order_id;
	 public function rules()
    {
        return [
            [['item', 'grand_total', 'tax_amount', 'qty'], 'required','on'=>['create','update'],],
            [['item_total_price','sub_item_total_price','store_id'], 'safe'],
			[['order_id','lat','lang','user_id',],'myvalidation', 'on' => 'finalize' ,'skipOnEmpty' => false],
		];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'user_id' => Yii::t('app','User'),
            'is_offline' => Yii::t('app','Is Offline'),
            'is_saved' => Yii::t('app','Is Saved'),
            'status' => Yii::t('app','Status'),
            'qty' => Yii::t('app','Qty'),
            'grand_total' => Yii::t('app','Grand Total'),
            'store_id' => Yii::t('app','Store'),
            'tax_amount' => Yii::t('app','Tax Amount'),
            'discount_amount' => Yii::t('app','Discount Amount'),
            'coupon_code' => Yii::t('app','Coupon Code'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
    
    
    
	public function myvalidation($attribute, $params){
		if($this->order_id==''){
			$this->addError('order_id', 'Order is required');
		}
		if($this->lat==''){
			$this->addError('lat', 'lat is required');
		}
		if($this->lang==''){
			$this->addError('lang', 'lang is required');
		}
		if($this->user_id==''){
			$this->addError('user_id', 'User id is required');
		}
	}
	public function getProfile()
	{
		return $this->hasOne(Profile::className(), ['user_id' => 'store_id']);
	}

}
