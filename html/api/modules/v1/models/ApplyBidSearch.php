<?php

namespace api\modules\v1\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\base\Model; 
use yii\data\ActiveDataProvider;
use common\models\Property;
use common\models\SqlDataProvider;
use yii\db\Expression;
use api\modules\v1\models\ApplyBid;
/**
 * ApplyBidSearch represents the model behind the search form about `api\modules\v1\models\ApplyBid`.
 */
class ApplyBidSearch extends ApplyBid
{
    const AUCTION_STATUS = ['inactive' => 0, 'auction' => 1, 'streaming' => 2, 'offline' => 3];
	public $title,$suburb,$state,$price,$floorArea,$landArea,$property_type,$from_date,$to_date,$sale_from,$sale_to,$min_land,$max_land,$min_floor,$max_floor,$auction_only,$auction_all,$list,$sort,$lat,$long;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [['id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
           [['title','suburb', 'state', 'price','floorArea','landArea','status','property_type','from_date','to_date','sale_from','sale_to','min_land','max_land','min_floor','max_floor','auction_only','auction_all','list','property_type','sort','lat','long'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		$expression = new Expression('latitude'); 
		if(!empty($params['lat']) && !empty($params['long'])){
			$lat = $params['lat'];
			$long = $params['long'];
			$exp=sprintf('(6371 * acos (cos ( radians(%s) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(%s) ) + sin ( radians(%s) ) * sin( radians( latitude ) ) )) as distance',$lat,$long,$lat);
			$expression = new Expression($exp); 
		}
        $query = Property::find()->select(Property::tableName().'.*,'.$expression)->joinWith('auction')->joinWith('city')->joinWith('state')->joinWith('applyBid')->where([ApplyBid::tableName().'.user_id'=>Yii::$app->user->id])->andWhere([ApplyBid::tableName().'.is_favourite'=>1])
        //->all()
        ;
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        
        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params) 
         * statement below
         */

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'default' => SORT_DESC
                ],
                //~ 'distance' => [
                    //~ 'default' => SORT_DESC
                //~ ],
                'title' => [
                    'default' => SORT_ASC
                ],
                'description' => [
                    'default' => SORT_ASC
                ],
                'landArea' => [
                    'default' => SORT_ASC
                ],
                'floorArea' => [
                    'default' => SORT_ASC
                ],
                'streetName' => [
                    'default' => SORT_ASC
                ],
                'suburb' => [
                    'default' => SORT_ASC
                ],
                'city' => [
                    'default' => SORT_ASC
                ],
                'state' => [
                    'default' => SORT_ASC
                ],
                'bedrooms' => [
                    'default' => SORT_ASC
                ],
                'bathrooms' => [
                    'default' => SORT_ASC
                ],
                'price' => [
                    'default' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);
		 if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
         // filter by property type
        if (!empty ($this->property_type) ) {
            $paramPropertyType = explode(",",$this->property_type);
            if(count($paramPropertyType)>1){
				foreach($paramPropertyType as $type)
				$query->joinWith('propertyTypename')->orFilterWhere(['like','tbl_property_types.name' ,  $type]);
            }else{
				$query->joinWith('propertyTypename')->andFilterWhere(['like','tbl_property_types.name' ,  $this->property_type]);
			}
        }
        
        // filter by sale price
        if (!empty($this->sale_from)) {
            $query->andFilterWhere(['>=', 'price', $this->sale_from]);
        }

        if (!empty($this->sale_to)) {
            $query->andFilterWhere(['<=', 'price', $this->sale_to]);
        }

        // filter by start date
        if (!empty($this->from_date)) {
            // Validate date format befoer processing
            if ($this->validate_date_format($this->from_date)) {
                // $from_date = date_create_from_format("Y-m-d", $params['from_date']);
                $from_date = strtotime($this->from_date);
                $query->andFilterWhere(['>=', 'tbl_auction_event.datetime_start', $from_date]);
            }
        }

        if (!empty($this->to_date)) {
            // Validate date format befoer processing
            if ($this->validate_date_format($this->to_date)) {
                // $from_date = date_create_from_format("Y-m-d", $params['from_date']);
                $to_date = strtotime($this->to_date);
                $query->andFilterWhere(['<=', 'tbl_auction_event.datetime_expire', $to_date]);
            }
        }
        // Min and max land
		 if (!empty($this->min_land)) {
            $query->andFilterWhere(['>=', 'landArea', $this->min_land]);
        }

        if (!empty($this->max_land)) {
            $query->andFilterWhere(['<=', 'landArea', $this->max_land]);
        }
		if (!empty($this->auction_only)) {
            $all = false;
            if (!empty($this->auction_all) && intval($this->auction_all) == 1) {
                $all = true;
            }
            if (intval($this->auction_only) == 1 && !$all) {
                $query->andFilterWhere(['tbl_auction_event.auction_status' => self::AUCTION_STATUS['auction']]);
            }
        }
		
        if(!empty($this->list)){
            $currentDateTime = strtotime(date('Y-m-d H:i:s',time()));
            if (intval($this->list) == 1) {  // past properties
                // If auction_only requested
                $query->andFilterWhere(['<','tbl_auction_event.datetime_start',($currentDateTime-86400)]);
                if ( !empty($this->auction_only) && intval($this->auction_only) == 1)
                {
                    $query->andFilterWhere(['tbl_auction_event.auction_status' => self::AUCTION_STATUS['auction']]);
                }
                $query->orderBy(Property::tableName().'.id desc');
            } elseif (intval($this->list) == 2){ // upcoming property
                $query->andFilterWhere(['>','tbl_auction_event.datetime_start',$currentDateTime]);
                if (!empty($this->auction_only) && intval($this->auction_only) == 1)
                {
                    $query->andFilterWhere(['tbl_auction_event.auction_status' => self::AUCTION_STATUS['auction']]);
                }
                $query->orderBy(Property::tableName().'.id desc');
            }
        }
        //print_r($params);die;
		
		if(!empty($this->status)){
			$query->andFilterWhere(['=', Property::tableName().'.status', $this->status]);
		}
		
        if (!empty($this->suburb)) {
            $query->orWhere(['LIKE', 'suburb', $this->suburb]);
            $query->orWhere(['LIKE', 'tbl_cview_states.name', $this->suburb]);
            $query->orWhere(['LIKE', 'tbl_city.name', $this->suburb]);
            $query->orWhere(['LIKE', 'postcode', $this->suburb]);
        }
        if(!empty($this->state))
         $query->andFilterWhere(['=', 'state', $this->state]);
        /*if (!empty($params['to_date'])) {
            $query->andFilterWhere(['<=', 'price', $params['to_date']]);
        }*/
        
        // Sort incase of datetime_start, stored in relation table
        if (!empty($this->sort)) {
            $sort_requested = explode(',', $this->sort);
            //print_r($sort_requested);die;
            foreach ($sort_requested as $value) {
                if (strpos($value, 'date') !== false) {
                    if ($value[0] == '-') {
                        $query->orderBy(['tbl_auction_event.datetime_start' => SORT_DESC]);
                    } else {
                        $query->orderBy(['tbl_auction_event.datetime_start' => SORT_ASC]);
                    }
                    break;
                }
                if(isset($this->lat) && isset($this->long))
                if(strpos($value, 'distance')  !== false){
					if($value[0] == '-')
					$query->orderBy(['distance'=>SORT_DESC]);
					else
					$query->orderBy(['distance'=>SORT_ASC]);
				}
            }
        }

        return $dataProvider;
    }

  
}
