<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;
use Yii;
use api\modules\v1\models\Agent;
/**
 * Agency Model
 * To handle data of agents.
 * 
 */
class Agency extends ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
         return '{{%agencies}}';
    }    
    
    public function init()
    {
     $this->status=10;   
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name','unique'],
            [['status','mobile'], 'integer'], 
           // [['name','logo','address','latitude','longitude','email','mobile','about_us'],'required', 'on'=>'create'],
            [['name','email','mobile','cv_agency_id'],'required', 'on'=>'create'],
            ['email','unique'],
            ['email','email'],
            [['logo', 'cv_agency_id', 'abn', 'slug', 'primary_color', 'secondary_color', 'text_color', 'latitude', 'longitude', 'name', 'mobile', 'email', 'fax', 'backgroungImage', 'address', 'street_address', 'full_address', 'suburb_id', 'about_us', 'status', 'web', 'twitter', 'facebook'], 'safe'],
            ['mobile', 'string', 'min' =>1, 'max' => 15], 
            [['logo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }  

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','Id'),
            'cv_agency_id' => Yii::t('app','cv_agency_id'),
            'name' => Yii::t('app','Name'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
    
    /**
     * @inheritdoc
     */
   	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
		//	$this->updated_by= Yii::$app->user->id;
			$this->updated_at= time();
            if ($this->isNewRecord) {
				//$this->created_by= Yii::$app->user->id;
				$this->created_at= time();
			}
			return true;
		} else {
            return false; 
		}
	}
	
	public function getLatLong($address){
		$geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCJAWURcRMIgLBQ6Wt0X7WHrUWk68E5N4g&address='.urlencode($address).'&sensor=false');
		$lat = '';
		$long = '';
		$geo = json_decode($geo, true);
		if ($geo['status'] == 'OK') {
			$lat = $geo['results'][0]['geometry']['location']['lat'];
			$long = $geo['results'][0]['geometry']['location']['lng'];
		}
		return array('lat'=>$lat,'long'=>$long);
	}
	
	public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

		  $theta = $lon1 - $lon2;
		  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		  $dist = acos($dist);
		  $dist = rad2deg($dist);
		  $miles = $dist * 60 * 1.1515;
		  $unit = strtoupper($unit);

		  if ($unit == "K") {
			return ($miles * 1.609344);
		  } else if ($unit == "N") {
			  return ($miles * 0.8684);
			} else {
				return $miles;
			  }
		}
		
	public function afterFind()
	{
		parent::afterFind();
        // $link = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('agencylogo');
		// if(!empty($this->logo)){
		// 	$this->logo = $link.'/'.$this->logo;
		// }
		// if(!empty($this->backgroungImage)){
		// 	$this->backgroungImage = $link.'/'.$this->backgroungImage;
		// }
		return true;		
	}
	
	public function fields(){
		$fields = parent::fields();
		$fields['is_favourite'] = function($model){
				return !empty($model->agencyFavourite)?$model->agencyFavourite->is_favourite:'';
			};
		$fields['facebook'] = function($model){
		return "https://www.facebook.com/";
		};
		$fields['twitter'] = function($model){
			return "https://twitter.com/";
		};
		$fields['instagram'] = function($model){
			return "https://www.instagram.com/";
		};
		$fields['agency_agents'] = function($model){
			return $model->agents;
		};		

		return $fields;
	}

    public function getAgents()
    {
       return $this->hasMany(Agent::className(),['agency_id'=>'id']);
    }

	public function getAgencyFavourite(){
		$result= null;
		if(!empty(Yii::$app->getRequest()->getHeaders()->get('Authorization')))
			$user_id = Yii::$app->MyConstant->UserID(Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        if(isset($user_id))
        $result = $this->hasOne(AgencyFavourite::className(),['agency_id'=>'id'])->andOnCondition(['user_id' => $user_id]);
        return $result;
	}
}