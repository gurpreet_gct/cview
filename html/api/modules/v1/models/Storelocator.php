<?php

namespace api\modules\v1\models;
use Yii;
use yii\base\Model;


/**
 * This is the model class for ForgotPassword.
 *
 * @property integer $email
 * @property string $user
 
 */
class Storelocator extends Model
{
    /**
     * @inheritdoc
     */
    public $lat,$lng;
     
    
   public function rules()
    {
        return [
        [['lat','lng'],'required'],        
        [['lat','lng'],'safe'],        
        ];
    }
    
         /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'lat' => Yii::t('app','Latitude'),
            'lng' => Yii::t('app','Longitude'),
           
        ];
    }  
    
 
  
}
