<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;
use Yii;

/**
 * AgencyFavourite Model
 * To handle data of agents.
 * 
 */
class AgencyFavourite extends ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
         return '{{%agency_favourite}}';
    }    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['user_id','agency_id','is_favourite'],'required'],
        ];     
    
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','Id'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
    
     /**
     * @inheritdoc
     */
   
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			$this->updated_by= Yii::$app->user->id;
			$this->updated_at= time();
			if($insert){			    		
				$this->created_by= Yii::$app->user->id;
				$this->created_at= time();
			}
		
			return true;
		} else {
			
		 return false; 
		}
	}
	
	public function validateExistingAgency($attribute, $params) 
    {
	   $auction = self::find()->where(['user_id'=>$this->user_id,'agency_id'=>$this->agency_id])->one();
		if (!$auction) {
			return true;
		} else {
		    $this->addError($attribute, "This agency is already in your favourite list.");
        }
    }
		
	
	
	
}
