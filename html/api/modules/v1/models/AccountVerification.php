<?php
namespace api\modules\v1\models;
use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use common\models\User;
use api\modules\v1\models\RegisterBid;

/**
 * ApplyBid Model
 *
 *
 */
class AccountVerification extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    const APP_ID = 2;

    public static function tableName()
    {
        return '{{%account_verification}}';
    }

	public function init()
	{
        //$this->app_id = self::APP_ID;
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['register_bid_id','type'],'required'],
            [['register_bid_id','verified_by','type','data','complete'],'safe'],
        ];

    }

    public function getUser()
    {
		return $this->hasOne(User::className(),['id'=>'user_id']);
	}

    public function getRegisterBid()
    {
        return $this->hasOne(RegisterBid::className(),['id'=>'register_bid_id']);
    }

    /**
     * Verify bid account
     * User & Bid
     */
    public function verifyAccount($verified_by, $register_bid_id)
    {
        $account = $this->find()->where(['register_bid_id' => $register_bid_id])->one();
        /*if (isset($account->data)) {
            $docs_data = json_decode($account->data, true);
            $updated_docs_data = array();
            
        }*/
        $data_verify = array('passport_number'=>1,'driver_license_number'=>1,'mediacare_number'=>1,'recent_utility_bill'=>1);
        $account->data = json_encode($data_verify);
        $account->complete = 1;
        $account->verified_by = $verified_by;
        if ($account->save(false)) {
            return $account;
        }
        return false;
    }

    /**
     * Check if account verified
     * User & Bid
     */
    public function isAccountVerified($user_id, $listing_id)
    {
        $account = $this->find()->where(['user_id' => $user_id, 'listing_id' => $listing_id])->one();
        return $account->complete == 1 ? true : false;
    }

    /**
     * Continue later if required.
     * Calculate the account verification percentage.
     */
    public function getVerificationPercentage($user_id, $listing_id)
    {
        $account = $this->find()->where(['user_id' => $user_id, 'listing_id' => $listing_id])->one();
        $docs_data = json_decode($account->data, true);
        $updated_docs_data = array();
        foreach ($docs_data as $key => $value) {
            // if (intval($value) == 1)
        }
        // $data_verify = array('passport_number'=>1,'driver_license_number'=>1,'mediacare_number'=>1,'recent_utility_bill'=>1);
        $account->data = json_encode($data_verify);
        $account->complete = 1;
        $account->verified_by = $verified_by;
        if ($account->save(false)) {
            return $account;
        }
        return false;
    }

    public function beforeSave($insert)
    {
        $this->updated_at = time();
        if ($insert) {
            $this->created_at = time();
        }
        return true;
    }
}
