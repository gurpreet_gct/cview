<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
return [
    'name'=>'SweetSpot Api',
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'rbac' =>  [
            'class' => 'johnitvn\rbacplus\Module'
        ],
        'gii' => 'yii\gii\Module',
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'   // here is our v1 modules
        ]
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'enableSession' => false,
            'loginUrl' => null,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /* Custom response format for rest api  */
        'response' => [
            'format' => 'json',
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if ($response->data !== null && $response->statusCode != 401 && $response->statusCode != 404 && $response->statusCode != 405   ) {
    				$message= isset($response->data['message'])? $response->data['message']:'';
                    $response->statusCode=  $statusCode=isset($response->data['statusCode'])?$response->data['statusCode']:$response->statusCode;
    				
                    if (isset($response->data['message'])) {
                        unset($response->data['message']);
                    }

                	if (isset($response->data['statusCode'])) {
                        unset($response->data['statusCode']);
                    }

                    $response->data = [
                        'message' =>$message,
                        'data' => $response->data,
                        'status'=>$statusCode
                    ];
                }
                /*
                Add response in case of no content(Delete action)
                else if($response->statusCode==204)
                {
                    $response->data = [
                        'message' =>'',
                        'data' => '',
                        'status'=>$response->statusCode
                    ];
                }*/
            },
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/user'],
                    'extraPatterns' => [
                        'POST login' => 'login',
                        'POST logout' => 'logout',
                        'POST checkemail' => 'checkemail',
                        'POST checkusername' => 'checkusername',
                        'POST changepassword' => 'changepassword',
                        'POST forgot' => 'forgot',
                        'POST validateuser' => 'validateuser',
                        'POST sendnote' =>'sendnote',
                        'POST employeee-login' =>'employeee-login',
                        'POST facebooklogin' => 'facebooklogin',
                        'POST linkedinlogin' => 'linkedinlogin',
                        'POST googlelogin' => 'googlelogin',
                        'POST create-agent' => 'create-agent',
                        'POST agent-list' => 'agent-list',
                        'POST email-agent' => 'email-agent',
                        /*'POST delete-agency' => 'delete-agency',
                        'POST add-agent' => 'add-agent',
                        'POST update-agent' => 'update-agent',
                        'POST delete-agent' => 'delete-agent',
                        'POST add-bidder' => 'add-bidder',
                        'POST update-bidder' => 'update-bidder',*/
                    ],

                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/agency'],
                    'extraPatterns' => [
                        'POST create' => 'create',                        
                        'GET view' => 'view',                        
                        'GET list-agencies' => 'list-agencies',                        
                        'GET view-detail' => 'view-detail',                        
                    ],

                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                    ]
                ],
				[
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/store'],
					'extraPatterns' => [
                        'GET storelocator/<id:\w+>' => 'storelocator',
                         'POST searchcategories' => 'searchcategories',
                         'POST searchkeyword' => 'searchkeyword',
                        ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                ],
				[
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/wallet'],
					'extraPatterns' => [
                        'POST validatepin' => 'validatepin',
                        ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                ],
				[
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/loyaltypoint'],
					'extraPatterns' => [
                        'POST userloyaltypoint' => 'userloyaltypoint',
                        'GET storelist' => 'storelist',
                        ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/category'],
                    'extraPatterns' => [
                        'POST updatebatch' => 'updatebatch',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/barcode'],
                    'tokens' => [
                         '{id}' => '<id:\\w+>'
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/deal'],
					'extraPatterns' => [
                        'POST search' => 'search',
                        'POST searchkeyword' => 'searchkeyword',
                        'POST searchstore' => 'searchstore',
                        'POST searchstorekeyword' => 'searchstorekeyword',
                        'GET payviapoint' => 'payviapoint',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/foodproduct'],
					'extraPatterns' => [
                        //   'GET categorypro' =>'categoryproduct',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ]
                ],
		        [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/foodorder'],
					'extraPatterns' => [
						'POST finalize' =>'finalize',
						'GET allorder' =>'allorder',
						'GET foodwallet' =>'foodwallet',
						'GET resturent-order' =>'resturent-order',
                        ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ]
                ],
				[
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/qr'],
					'extraPatterns' => [
						'POST order' =>'order',
                        ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ]
                ],
				[
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/cart'],
					'extraPatterns' => [
						//'POST finalize' =>'finalize',
						'POST payinapp' =>'payinapp',
						'POST pay' =>'pay',
						'POST transaction-fees' =>'transaction-fees',
						'POST create-item' =>'create-item',
						'POST pay-now' =>'pay-now',
                        ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/bookmarkdeal'],
					'extraPatterns' => [
                        'POST search' => 'search',
                        'POST searchkeyword' => 'searchkeyword',
                        'POST searchstore' => 'searchstore',
                        'POST searchstorekeyword' => 'searchstorekeyword',
                        //'GET storelist' => 'storelist',
                        ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/id'],
					'extraPatterns' => [
                        'POST searchkeyword' => 'searchkeyword',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                ],
				[
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/passes'],
					'extraPatterns' => [
            			'GET digitalstampcard' => 'digitalstampcard',
            			'GET eventticket' => 'eventticket',
            			'GET redeem' => 'redeem',
            			'GET reedeemedstampcard' => 'reedeemedstampcard',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/address'],
					'extraPatterns' => [
                        'GET shippingaddress' => 'shippingaddress',
                        'GET billingaddress' => 'billingaddress',
                        'GET shippingbilling' => 'shippingbilling',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/paymentcard'],
					'extraPatterns' => [
                        'POST token' => 'token',
                        'POST remove-card' => 'remove-card',
                        'POST add-credit-card' => 'add-credit-card',
                        'GET credit-card-list' => 'credit-card-list',
                        'GET generate-token' => 'generate-token',
                        'POST remove-credit-card' => 'remove-credit-card',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                ],
				[
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/order'],
					'extraPatterns' => [
						'POST finalize' =>'finalize',
						'POST active-order' =>'active-order',
				     ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/country','v1/shopdeal','v1/product','v1/paymentcardtype','v1/cardtype','v1/beacondetect','v1/qr','v1/cart','v1/order','v1/addresstype','v1/store-order','v1/offline','v1/loyaltycard','v1/state','v1/offertrack','v1/geolocation','v1/notificationsetting','v1/dealnotification','v1/payloadmessage','v1/country'],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/notification'],
					'extraPatterns' => [
                        'PUT read' => 'read',
                        'PUT isdelete' => 'isdelete',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/property'],
                    'extraPatterns' => [
                        'POST create' => 'create',
                        'GET all' => 'all',
                        'GET search' => 'search',
                        'GET view' => 'view',
                        'POST delete-property' => 'delete-property',
                        'POST update-property' => 'update-property',
                        'POST save-list-properties' => 'save-list-properties',
                        'GET get-agent-properties' => 'get-agent-properties',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/user-agent'],
                    'extraPatterns' => [
                        'POST login' => 'login',
                        'POST logout' => 'logout',
                        'POST forgot' => 'forgot',
                    ],

                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/webhooks/user'],
                    'extraPatterns' => [
                        'POST get-auth-key' => 'get-auth-key',
                        'POST add-agent' => 'add-agent',
                        'POST update-agent' => 'update-agent',
                        'POST delete-agent' => 'delete-agent',
                        'POST delete-bidder' => 'delete-bidder',
                        'POST add-bidder' => 'add-bidder',
                        'POST update-bidder' => 'update-bidder',
                        'GET sync-agent-data' => 'sync-agent-data',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/webhooks/bidder'],
                    'extraPatterns' => [
                        'POST index' => 'index',
                        'DELETE index' => 'delete',
                        'PUT index' => 'update',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/webhooks/bidder'],
                    'extraPatterns' => [
                        'POST index' => 'index',
                        'DELETE index' => 'delete',
                        'PUT index' => 'update',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/webhooks/agent'],
                    'extraPatterns' => [
                        'POST index' => 'index',
                        'DELETE index' => 'delete',
                        'PUT index' => 'update',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/webhooks/agency'],
                    'extraPatterns' => [
                        'POST add-agency' => 'add-agency',
                        'POST delete-agency' => 'delete-agency',
                        'DELETE' => 'delete',
                        'PUT' => 'update',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/webhooks/property'],
                    'extraPatterns' => [
                        'POST index' => 'index',
                        'DELETE index' => 'delete',
                        'PUT index' => 'update',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                    ]
                ], 
               // ['class' => 'api\components\UrlRuleCustom', 'path' => 'test'],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/streaming'],
                    'extraPatterns' => [
                        'GET server-info' => 'server-info',
                    ],
                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/auction-event'],
                    'extraPatterns' => [
                        'POST start-streaming-auction' => 'start-streaming-auction',
                        'GET get-auction-config' => 'get-auction-config',
                        'POST update-bidding-pause-status' => 'update-bidding-pause-status',
                    ],
                    'tokens' => [
                        // '{id}' => '<id:\\w+>',
                    ]
                ],
            ],
        ],
        /* Auth Client */
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                    'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                    'clientId' => '1472499159722404',
                    'clientSecret' => '30104eac82383eda0bde6e3662d49de6',
                ],
            ],
        ],
    ],
    'params' => $params,
];
