<?php 
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
namespace api\components;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;
use linslin\yii2\curl\Curl;

class Controller extends ActiveController {
    public function beforeAction($event)
    {   
		
        return parent::beforeAction($event);
    }

    /**
     * Processing callback urls
     * Call this method to execute the request on the client server.
     * @param callback_url {string} The callback url to the http link that listens for a POST request
     * @param post_callback_url {string} The post callback url, to be called post successful execution of the calback_url on the server.
     */
    public function processCallback($data_to_post)
    {
    	if (empty($data_to_post['callback_url'])) {
    		return false;
    	}

    	$curl = new Curl();
		$response = $curl->setPostParams($data_to_post)
		     ->post($data_to_post['callback_url']);

		return true;
    }

    public function validateWebhooksInput($data) {
        $returnData = [
            'status' => true,
            'message' => ''
        ];
        if (empty($data)) {
            $returnData = [
                'status' => false,
                'message' => 'Empty payload data',
            ];
        }
        return $returnData;
    }
}
