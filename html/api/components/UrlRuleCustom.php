<?php
namespace api\components;
use Yii;
use yii\rest\UrlRule;

class UrlRuleCustom extends UrlRule {
  public $path = '@app/components';
  public function init()
  {   
    
     $d = dir(Yii::getAlias('@app/modules/v1/controllers'));
    $arr = [];
    
    while (false !== ($entry = $d->read())) {
       if (strpos($entry, 'Controller.php') !== false) {
           $con=lcfirst(str_replace(['Controller.php'], '', $entry));
          $arr[$con] = $con;
       }
    }

    $this->controller = $arr;    
    //print_r($this->controller);
    //die("test");

   //  parent::init();
  }
}