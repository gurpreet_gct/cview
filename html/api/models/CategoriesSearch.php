<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Categories;

/**
 * CatagoriesSearch represents the model behind the search form about `app\models\Categories`.
 */
class CategoriesSearch extends Categories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catID', 'catParent', 'level', 'sortOrder'], 'integer'],
            [['catName', 'catImage', 'catIconImage', 'pathText', 'Path'], 'safe'],
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'root' => Yii::t('app','Root'),
			'lft' => Yii::t('app','Lft'),
			'rgt' => Yii::t('app','Rgt'),
			'lvl' => Yii::t('app','Lvl'),
			'name' => Yii::t('app','Name'),
			'parentKey' => Yii::t('app','Parent_Key'),
			'icon' => Yii::t('app','Icon'),
			'imageurl' => Yii::t('app','Image_Url'),
			'icon_type' => Yii::t('app','Icon_Type'),
			'active' => Yii::t('app','Active'),
			'selected' => Yii::t('app','Selected'),
			'disabled' => Yii::t('app','Disabled'),
			'readonly' => Yii::t('app','Readonly'),
			'visible' => Yii::t('app','Visible'),
			'collapsed' => Yii::t('app','Collapsed'),
			'moveable_u' => Yii::t('app','Moveable_U'),
			'moveable_d' => Yii::t('app','Moveable_D'),
			'moveable_r' => Yii::t('app','Moveable_R'),
			'moveable_l' => Yii::t('app','Moveable_L'),
			'removeable' => Yii::t('app','Removeable'),
			'removeable_all' => Yii::t('app','Removeable_All'),
			'path' => Yii::t('app','Path'),
			'catID' => Yii::t('app','Cat ID'),
			'catName' => Yii::t('app','Cat Name'),
			'catParent' => Yii::t('app','Cat Parent'),
			'catImage' => Yii::t('app','Cat Image'),
			'catIconImage' => Yii::t('app','Cat Icon Image'),
			'level' => Yii::t('app','Level'),
			'pathText' => Yii::t('app','Path Text'),
			'sortOrder' => Yii::t('app','Sort Order'),
        ];
    }
    
    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Categories::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'catID' => $this->catID,
            'catParent' => $this->catParent,
            'level' => $this->level,
            'sortOrder' => $this->sortOrder,
        ]);

        $query->andFilterWhere(['like', 'catName', $this->catName])
            ->andFilterWhere(['like', 'catImage', $this->catImage])
            ->andFilterWhere(['like', 'catIconImage', $this->catIconImage])
            ->andFilterWhere(['like', 'pathText', $this->pathText])
            ->andFilterWhere(['like', 'Path', $this->Path]);

        return $dataProvider;
    }
}
