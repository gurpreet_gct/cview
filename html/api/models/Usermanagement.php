<?php
	
namespace app\models;
use Yii;
use common\models\Usertype;
use yii\web\UploadedFile;
/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property integer $status
 * @property string $email
 * @property integer $contact_no
 * @property string $role
 * @property integer $active
 * @property string $tokenhash
 * @property integer $tokenexpired
 * @property string $created
 * @property string $modified
 */
class Usermanagement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
   public $imageFile;
   public $when;
   public $number_max;

      
    public static function tableName()
    {
         return '{{%users}}';
    }
    
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status','phone'], 'integer'],                           
            [['email', 'sex','password_hash', 'dob', 'phone', 'fullName', 'user_type', 'firstName', 'lastName','username', 'status'], 'required'],
				//['password_hash','required','on'=>'create'],           
            ['email','unique'],['username','unique'],
            ['email', 'email'],            
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            ['phone', 'string', 'min' =>1, 'max' => 10],           
            [['username'], 'string', 'max' => 50],
          
           
        ]; 
        
       
    
    
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First Name'),
			'lastName' => Yii::t('app','Last Name'),
			'fullName' => Yii::t('app','Full Name'),
			'orgName' => Yii::t('app','Org_Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb_Identifier'),
			'role_id' => Yii::t('app','Role'),
			'user_type' => Yii::t('app','User_Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','Password'),
			'password_reset_token' => Yii::t('app','Password Reset Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date of Birth'),
			'ageGroup_id' => Yii::t('app','Age_Group_Id'),
			'walletPinCode' => Yii::t('app','Wallet Pin Code'),
			'loyaltyPin' => Yii::t('app','Loyalty_Pin'),
			'auth_key' => Yii::t('app','Auth Key'),
			'activationKey' => Yii::t('app','Activation Key'),
			'confirmationKey' => Yii::t('app','Confirmation Key'),
			'access_token' => Yii::t('app','Access Token'),
			'hashKey' => Yii::t('app','Hash Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device_Token'),
			'lastLogin' => Yii::t('app','Last Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created At'),
            'updated_at'=>Yii::t('app','Updated At'),
        ];
    }
    
    
    
    
    public function getUsertype()
    {
         return $this->hasOne(Usertype::className(), ['id' => 'user_type']);
    }
    
    public function getsextype()
    {
         $sex = $this->sex;
        		 if($sex==1){         	
        				return 'Male';
        		 }else {
        			  	return 'Female';
        		 }
    }  
     
    
		
			/**
		 * @inheritdoc
		 */
	/*	public function updatepassword($insert)
		{
			
		    if (parent::beforeSave($insert)) {
		        // Place your custom code here
		        	$this->password_hash=Yii::$app->security->generatePasswordHash($this->password_hash);
					
		        return true;
		    } else {
		        return false;
		    }
		}
		*/
		
	
		
		

}
