<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_categories".
 *
 * @property integer $catID
 * @property string $catName
 * @property integer $catParent
 * @property string $catImage
 * @property string $catIconImage
 * @property integer $level
 * @property string $pathText
 * @property string $Path
 * @property integer $sortOrder
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catID'], 'required'],
            [['catID', 'catParent', 'level', 'sortOrder'], 'integer'],
            [['catName'], 'string', 'max' => 27],
            [['catImage'], 'string', 'max' => 99],
            [['catIconImage'], 'string', 'max' => 86],
            [['pathText'], 'string', 'max' => 10],
            [['Path'], 'string', 'max' => 56]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
			'root' => Yii::t('app','Root'),
			'lft' => Yii::t('app','Lft'),
			'rgt' => Yii::t('app','Rgt'),
			'lvl' => Yii::t('app','Lvl'),
			'name' => Yii::t('app','Name'),
			'parentKey' => Yii::t('app','Parent_Key'),
			'icon' => Yii::t('app','Icon'),
			'imageurl' => Yii::t('app','Image_Url'),
			'icon_type' => Yii::t('app','Icon_Type'),
			'active' => Yii::t('app','Active'),
			'selected' => Yii::t('app','Selected'),
			'disabled' => Yii::t('app','Disabled'),
			'readonly' => Yii::t('app','Readonly'),
			'visible' => Yii::t('app','Visible'),
			'collapsed' => Yii::t('app','Collapsed'),
			'moveable_u' => Yii::t('app','Moveable_U'),
			'moveable_d' => Yii::t('app','Moveable_D'),
			'moveable_r' => Yii::t('app','Moveable_R'),
			'moveable_l' => Yii::t('app','Moveable_L'),
			'removeable' => Yii::t('app','Removeable'),
			'removeable_all' => Yii::t('app','Removeable_All'),
			'path' => Yii::t('app','Path'),
			'catID' => Yii::t('app','Cat ID'),
			'catName' => Yii::t('app','Cat Name'),
			'catParent' => Yii::t('app','Cat Parent'),
			'catImage' => Yii::t('app','Cat Image'),
			'catIconImage' => Yii::t('app','Cat Icon Image'),
			'level' => Yii::t('app','Level'),
			'pathText' => Yii::t('app','Path Text'),
			'sortOrder' => Yii::t('app','Sort Order'),
        ];
    }
}
