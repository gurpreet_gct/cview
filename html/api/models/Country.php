<?php
namespace api\models;
use \yii\db\ActiveRecord;
/**
 * Country Model
 * Country Model
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class Country extends ActiveRecord 
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tbl_countries';
	}

    /**
     * @inheritdoc
     */
  /*  public static function primaryKey()
    {
        return ['code'];
    }*/

    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['code', 'name', 'population'], 'required']
        ];
    } 
    
    
    public function attributeLabels()
		{
			return [
				'id_countries' => Yii::t('app','Id_Countries'),
				'sortname' => Yii::t('app','Sortname'),
				'name' => Yii::t('app','Name'),
				
			];
		}
		  
}
