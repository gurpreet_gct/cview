<?php

#namespace backend\models;
namespace app\models;

use Yii;
#use common\models\Usertype;
#use yii\web\UploadedFile;
/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property integer $status
 * @property string $email
 * @property integer $contact_no
 * @property string $role
 * @property integer $active
 * @property string $tokenhash
 * @property integer $tokenexpired
 * @property string $created
 * @property string $modified
 */
class Userextrafield extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $usertype_id=0; 
      
    public static function tableName()
    {
        return 'tbl_profile';
    }
    
    

    /**
     * @inheritdoc
     */
   public function rules()
    {
        return [
            [['companyNumber','officePhone'], 'integer'],
            [['country','postcode','officePhone','alternativeContactperson', 'ssdRegion', 'houseNumber', 'street', 'city','state'], 'required'],
     
     
            [['latitude', 'longitude', 'alternativeContactperson', 'commercialStatus', 'contractDuration', 'NumberOfBeaconLocations', 'RelatedBeaconLocationBroker', 'RelatedBeaconLocationBrokerID'], 'required', 'when' => function($model) {
        return in_array($model->usertype_id,array(7,8));
    },    
'whenClient' => "function (attribute, value) {
        var rs=$.inArray($('#usermanagement-user_type').val(), ['7','8']);
       
        if(rs>=0)        
        return true;
        else 
        return false;
    }"    
    ], 
    
   
    [['companyNumber','companyName'], 'required', 'when' => function ($model) {
        return $model->commercialStatus != 3;
    }, 'whenClient' => "function (attribute, value) {
        return $('#userextrafield-commercialstatus').val() != '3';
    }"],
        
        [['officeFax'], 'string', 'max' => 10],
         ['officePhone', 'string', 'min' => 1, 'max' => 10],
         [['DID'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
   public function attributeLabels()
    {
        return [
            'id' =>Yii::t('app','Id') ,
            'user_id' =>Yii::t('app','User_Id') ,
            'companyName' =>Yii::t('app','Company Name') ,
            'companyNumber' => Yii::t('app','Company Number'),
            'ssdRegion' =>Yii::t('app','SSD Region'),
            'houseNumber' => Yii::t('app','House Number'),
            'street' => Yii::t('app','Street'),
            'city' =>Yii::t('app','City') ,
            'state' => Yii::t('app','State'),
            'country' => Yii::t('app','Country'),
            'postcode' =>Yii::t('app','Postcode') ,
            'officePhone' =>Yii::t('app','Office Phone') ,
            'officeFax' => Yii::t('app','OfficeFax'),
			'alternativeContactperson' => Yii::t('app','Alternative Contact Person'),           
            'commericalStatus' => Yii::t('app','Commercial Status'),
            'latitude' => Yii::t('app','Latitude'),
            'longitude' => Yii::t('app','Longitude'),
            'brandName' => Yii::t('app','Brand_Name'),
            'brandLogo' => Yii::t('app','Brand_Logo'),
            'brandimgLogo' => Yii::t('app','Brand_Image_Logo'),
            'backgroundImage' => Yii::t('app','Background_Image'),
            'brandAddress' => Yii::t('app','Brand_Address'),
            'brandDescription' => Yii::t('app','Brand_Description'),
            'socialmediaFacebook' => Yii::t('app','Social_Media_Facebook'),     
            'socialmediaTwitter' => Yii::t('app','Social_media_Twitter'),     
            'socialmediaGoogle' => Yii::t('app','Social_Media_Google'),  
            'socialmediaPinterest' =>Yii::t('app','Social_Media_Pinterest') ,  
            'socialmediaInstagram' => Yii::t('app','Social_media_Instagram'),  
            'backgroundHexColour' => Yii::t('app','Background Hex Colour'),  
            'foregroundHexColour' =>Yii::t('app','Label Text Colour (Hex Colour)') ,  
            'addLoyalty' => Yii::t('app','Add "Loyalty"'),  
            'shopNow' => Yii::t('app','Add "Shop Now"'), 
            'socialcheck' => Yii::t('app','Add "Social media buttons and URLs"'), 
            'addcolors' => Yii::t('app','Add "Background and foreground hex colour"'), 
            'DID' => Yii::t('app','DID'), 
            'contractDuration' => Yii::t('app','Contract Duration'), 
            'NumberOfBeaconLocations' => Yii::t('app','Number Of Beacon Locations'), 
            'RelatedBeaconLocationBroker' => Yii::t('app','Related Beacon Location Broker'), 
            'RelatedBeaconLocationBrokerID' => Yii::t('app','Related Beacon Location Broker ID'), 
        ];
    }
    
   public function getUser_type()
    {
         return $this->hasOne(User::className(), ['id' => 'user_id']);
    } 
  

}
