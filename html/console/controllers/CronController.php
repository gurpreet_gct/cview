<?php
//namespace app\commands;

namespace console\controllers;

use yii\console\Controller;
use Yii;

class CronController extends Controller
{

	public function actionIndex()
	{
		$month = date('n');
		$lastDay = date('t');
		$currentDayOfMonth = date('j');
		$condition =  'due_date='.$currentDayOfMonth;
		if($month==2 && $lastDay==$currentDayOfMonth){
		$condition = 'due_date>='.$currentDayOfMonth;
		}
		$fullDate =  date('m-Y');
		$existing = \common\models\SubscriptionCharges::find()
		->select(['partner_id','from_unixtime(created_at,"%m-%Y") as created'])
		->having("created='$fullDate'")
		->asArray()->all();

		$getPartNers =  \common\models\SubscriptionFees::find()
		->where($condition);
		if(count($existing)>0){
		$pIds = array_column($existing,'partner_id');
		$getPartNers->andWhere(['NOT IN','partner_id',$pIds]);
		}
		$getPartNers =  $getPartNers->all();
		foreach($getPartNers as $_data){
		$subCharges =  new \common\models\SubscriptionCharges();
		$subCharges->partner_id = $_data->partner_id;
		$subCharges->amount = $_data->amount;
		$subCharges->payment_status = 0;
		$subCharges->save();
		}
		
	}

}
