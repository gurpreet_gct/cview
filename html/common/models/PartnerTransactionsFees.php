<?php

namespace common\models;
use common\models\User;
use Yii;

/**
 * This is the model class for table "{{%partner_transactions_fees}}".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property integer $order_id
 * @property string $orderAmount
 * @property string $transactionFees
 * @property integer $workorder_id
 * @property integer $is_deducted (this means is this amount is paid to admin or not)
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $offer_type ( workorder offer type )
 */
class PartnerTransactionsFees extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_transactions_fees}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id'], 'required'],
            [['offer_type','store_id','partner_id', 'order_id', 'workorder_id', 'is_deducted', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['orderAmount', 'transactionFees'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partner_id' => 'Partner',
            'order_id' => 'Order ID',
            'store_id' => 'Store',
            'orderAmount' => 'Order Amount',
            'transactionFees' => 'Fees',
            'workorder_id' => 'Deal',
            'is_deducted' => 'status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
     	public function beforeSave($insert)
	{
		
		if (parent::beforeSave($insert)) {
				$this->updated_at= time();
				if ($this->isNewRecord) {			
					$this->created_at = time();
					$this->created_by= Yii::$app->user->id;
				}
			
			return true;
		} else {
			return false;
		}
	}
	public function getPartnernameval(){
		
		if(!empty($this->partner_id)){
		
			$partnerName =  User::find()->select('orgName')
					->where(['id' =>$this->partner_id])->one();
			if($partnerName){
				return $partnerName->orgName; 
				
			}	
		}	
		return 'N/A';		
	}
	public function getStorename(){
		
		if(!empty($this->store_id)){		
			$partnerName =  \common\models\Store::find()->select('storename')
						->where(['id' =>$this->store_id])->one();
			if($partnerName){
				return $partnerName->storename; 				
			}					
		}
		return 'N/A';	
	}
	public function afterFind()
	{
		parent::afterFind();
		
		/* Date From */
		if(!strstr($this->created_at,"-"))
		{               
			$this->created_at=date("Y-m-d",$this->created_at);
		}	
			return true;		
	}
	public function getWorkordername(){
		if($this->workorder_id){
			$model = \common\models\Workorders::find()->select('name')
						->where('id='.$this->workorder_id)->one();
			if(isset($model->name)){
				return $model->name;
			}
			$product = \common\models\Product::find()->select('name')
						->where('id='.$this->workorder_id)->one();
			if(isset($product->name)){
				return $product->name;
			}
		}
		return 'N/A';
	}
	public function feeAmount($unixDateStart,$unixDateEnd,$filterType,$userField){
		
		$query = Yii::$app->getDb();		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND partner_id='.$userId;
			
		}else{			
			$custQuery		= '';			
		}			
		
		$tableName = self::tableName();		
		$command 	= $query->createCommand("SELECT IFNULL(sum(transactionFees),0) as paid FROM $tableName where is_deducted=1 and created_at BETWEEN $unixDateStart AND $unixDateEnd $custQuery union all SELECT IFNULL(sum(transactionFees),0) as paid FROM $tableName where is_deducted=0 and created_at BETWEEN $unixDateStart AND $unixDateEnd $custQuery union all SELECT IFNULL(sum(transactionFees),0) as paid FROM $tableName where is_deducted!=100 and created_at BETWEEN $unixDateStart AND $unixDateEnd $custQuery ;");		
		$result 	= $command->queryAll();
		return array_column($result,'paid');
		
	}
	
	public static function getOfferTypes(){
		
		return [
			1=>'Per transactions',
			2=>'Buy xx get xx',
			3=>'Get xx %off',
			4=>'Enjoy xx% Off on xx',
			5=>'Buy xx get xx % off',
			6=>'Buy xx for $ xx',
			7=>'Offer Was/Now',
		];
	}
	public function afterSave($insert, $changedAttributes){
		
		if($this->orderAmount>0){
			$this->saveReferral($this);
		}
		return true;
	}
	
	public function saveReferral($model){		
		  $percentage = new \common\models\Setting();
          $amt = $percentage->getPercentage();
          if(is_array($amt)){			
			 $referalModel = new \common\models\ReferralEarnings(); 			
			 $referalModel->user_id = Yii::$app->user->id;
			 $referalModel->refer_by = $amt['referBy'];
			 $referalModel->commission_amount = ($amt['amount']/100)*$model->transactionFees;
			 $referalModel->commission_percentage = $amt['amount'];
			 $referalModel->order_id = $model->order_id;
			 $referalModel->order_total = $model->transactionFees;
			 $referalModel->status = 0;
			 $referalModel->save(false);
		  }
	}
}
