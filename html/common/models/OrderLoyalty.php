<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_order_loyalty".
 *
 */
class OrderLoyalty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_loyalty}}';
    }


    public function rules()
    {
        return [

            [['user_id','created_at'], 'safe'],
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'user_id'=>Yii::t('app','User name'),
			'order_id' => Yii::t('app','Order Id'),
			'store_id' => Yii::t('app','Store Id'),
			'payable' => Yii::t('app','Payable'),
			'value' => Yii::t('app','Value'),
			'status' =>Yii::t('app','Status'),
			'created_at' => Yii::t('app','Created At'),
			'updated_at' => Yii::t('app','Updated At'),
			'created_by' => Yii::t('app','Created By'),
			'updated_by' => Yii::t('app','Updated By'),
			'pointvalue' => Yii::t('app','Loyalty Dollars Value'),
			'valueOnDay' =>  Yii::t('app','Value On Day'),
			'loyaltypoints' =>  Yii::t('app','Loyalty Dollars'),
           ];
    }

	/*
	*@pointLiability
	* return data of point liability
	*/

	public function pointLiability($startDate,$endDate,$userField){

		// select sum(value) from tbl_order_loyalty;

		/* $query  = new OrderLoyalty();

		$reslt 	= $query->find()
						->select("SUM(value) AS liabilityPoint")
						->asArray()
						->one();

		return 	$reslt;

		*/

		$query 		= Yii::$app->getDb();
		if(!empty($userField)){
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND ol.store_id='.$userId;

		}else{
			$custQuery		= '';
		}

		$command 	= $query->createCommand("SELECT sum(value) AS pointLiability,str.orgName AS storename FROM {{%order_loyalty}} AS ol JOIN {{%users}} AS usr ON usr.id=ol.user_id JOIN {{%users}} str ON str.id=ol.store_id JOIN {{%orders}} ord ON ol.order_id=ord.id AND (ord.created_at BETWEEN $startDate AND $endDate) $custQuery GROUP BY ol.store_id");
		$result 	= $command->queryAll();
		return 	$result;


	}

	/*
	*@avgLoyalTransaction
	* return data of avg Loyal Transaction
	*/

	public function avgLoyalTransaction($userField){

		// select sum(value)/count(Distinct user_id) from tbl_order_loyalty;

		if(!empty($userField)){

			$userType 		= $userField['userType'];
			$userId 		= $userField['userId'];
			$custQuery 		= User::tableName().'.user_type='.$userType.' AND '.OrderLoyalty::tableName().'.store_id='.$userId;

		}else{

			$custQuery		= '';

		}

		$query  = new OrderLoyalty();

		$reslt 	= $query->find()
						->select("sum(value)/count(Distinct user_id) AS avg_transc")
						->leftjoin(User::tableName(),User::tableName().'.id='.OrderLoyalty::tableName().'.store_id')
						->where($custQuery)
						->asArray()
						->one();

		return 	$reslt;

	}


}
