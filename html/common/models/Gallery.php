<?php
namespace common\models;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tbl_gallery".
 *
 * @property integer $id
 * @property integer $tbl_pk
 * @property string $detail
 * @property string $photo
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return '{{%gallery}}';
    }
	
    /**
     * @inheritdoc
     */
    public $imageFiles;
    
    public function rules()
    {
        return [
            [['tbl_pk'], 'integer'],
            [['url', 'type', 'file_name'], 'string', 'max' => 255],
            //[['type'], 'string', 'max' => 255],
            [['tbl_name', 'thumbnail', 'file_name'],'safe'],
            //['docFiles', 'url', 'skipOnEmpty' => true, 'extensions' => 'pdf', 'docx', 'maxFiles' => 20],
            //['imageFiles', 'url', 'minWidth' => 450, 'maxWidth' => 450,'minHeight' => 450, 'maxHeight' => 450, 'extensions' => 'jpg, png', 'maxSize' => 1024 * 1024 * 2, 'maxFiles' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tbl_pk' => 'Table Primary Key',
            'url' => 'Url of the file',
            'type' => 'The type of the file, eg. image/',
            'photo' => 'Photo',
            'file_name' => 'Name of the File',
            'imageFiles' => 'Photo (Should be 450 x 450px)',
        ];
    }

    //~ public function imagepath($photo)
    //~ {
		//~ if ($photo) {
			//~ return  Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('images/property/'.$photo); 
        //~ }
		//~ return false;	
	//~ }
	
	public function afterFind()
	{
		parent::afterFind();
		/*if (!empty($this->url)) {
			$this->url = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('images/property/'.$this->url);
		}*/
		return true;		
	}  
   
}