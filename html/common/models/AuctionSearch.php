<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Auction;

/**
 * AuctionSeach represents the model behind the search form about `common\models\Auction`.
 */
class AuctionSearch extends Auction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['listing_id', 'auction_type', 'date', 'time', 'datetime_start', 'datetime_expire', 'starting_price', 'home_phone_number', 'estate_agent', 'picture_of_listing', 'duration', 'real_estate_active_himself', 'real_estate_active_bidders', 'reference_relevant_electronic', 'reference_relevant_bidder', 'created_at', 'auction_status', 'streaming_url', 'streaming_status', 'templateId', 'envelopeId', 'documentId', 'document', 'recipientId', 'bidding_status', 'updated_at', 'created_by', 'updated_by', 'estate_agent_id', 'agency_id'],'safe'],
        	//[['price','user_id', 'propertyType'],'number'],     
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'id' =>  Yii::t('app','ID'),
            'title' => Yii::t('app','Property title'),
            /*'icon' => Yii::t('app','Icon'),
            'status' =>Yii::t('app','Status') ,
            'created_at' =>Yii::t('app','Created At') ,
            'updated_at' =>Yii::t('app','Updated At') ,*/
        ];
    }    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auction::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'title' => $this->title,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        //$query->andFilterWhere(['like', 'title', $this->name]);
            //->andFilterWhere(['like', 'icon', $this->icon]);

        return $dataProvider;
    }
}
