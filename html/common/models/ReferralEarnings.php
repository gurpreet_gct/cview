<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%referral_earnings}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $refer_by
 * @property string $commission_amount
 * @property integer $commission_percentage
 * @property integer $order_id
 * @property string $order_total
 * @property integer $status
 * @property integer $created_at
 * @property integer $created_by
 */
class ReferralEarnings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%referral_earnings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'refer_by', 'commission_percentage', 'order_id', 'status', 'created_at', 'created_by'], 'integer'],
            [['commission_amount', 'order_total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'refer_by' => Yii::t('app', 'Refer By'),
            'commission_amount' => Yii::t('app', 'Commission Amount'),
            'commission_percentage' => Yii::t('app', 'Commission %'),
            'order_id' => Yii::t('app', 'Order ID'),
            'order_total' => Yii::t('app', 'Order Total'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    public function beforeSave($insert)
	{
		   if (parent::beforeSave($insert)) {
			   if($insert){
				   $this->created_at= time();
				   $this->created_by = Yii::$app->user->id;
			   }

			   return true;
			 } else {

			  return false;
		   }
	}
	
	public function refEarning($unixDateStart,$unixDateEnd,$userField){
		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND refer_by='.$userId;

		}else{

			$custQuery		= '';

		}
		$table = self::tableName();
		$db = Yii::$app->getDb();
		$query = $db->createCommand("SELECT IFNULL(sum(commission_amount),0) as total FROM $table where created_at BETWEEN $unixDateStart AND $unixDateEnd $custQuery;");
		return $query->queryScalar();
	}
	
	
	public function getUsers($field){
		$conn = Yii::$app->getDb();
		$usrTbl = \common\models\User::tableName();
		$rfTbl = self::tableName();
		$sql = "select a.id,a.username from $usrTbl as a inner join $rfTbl as b on a.id=b.$field ;";
		$query = $conn->createCommand($sql)->queryAll();
		return array_column($query,'username','id');
	}
}
