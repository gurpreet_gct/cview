<?php
 
namespace common\models;
 
 
use Yii;
 
/**
 * This is the model class for table "tbl_userPsa".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $status
 * @property int $created_at
 */
class ZoneNotificationStats extends \yii\db\ActiveRecord
{
    
     /**
     * @inheritdoc
     */
     
    
    
    public static function tableName()
    {
        return '{{%zone_notification_stats}}';
    }
 
    /**
     * @inheritdoc
	*/
	 
    public function rules()
    {
        return [
            [['user_id', 'deal_id','notification_limit','remaining_limit','created_at','updated_at'], 'safe'],
           
        ];
    }
    
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','Id'),
            'user_id' => Yii::t('app','User Id'),
            'deal_id' => Yii::t('app','Deal Id'),
            'detect_type' => Yii::t('app','Detect Type'),
            'notification_limit' => Yii::t('app','Notification Limit'),
            'used_limit' => Yii::t('app','used Limit'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
            ];
    }
	
   
   
}
