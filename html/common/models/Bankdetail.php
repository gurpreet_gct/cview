<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%bank_accounts}}".
 *
 * @property integer $id
 * @property string $bank_name
 * @property string $account_name
 * @property string $holder_type
 * @property string $account_type
 * @property integer $prefrence
 * @property integer $partner_id
 * @property integer $store_id
 * @property integer $account_number
 * @property integer $routing_number
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Bankdetail extends \yii\db\ActiveRecord
{
	public $user_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bank_accounts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			 [['disbursement','status','country','holder_type','account_type','prefrence','partner_id','bank_name', 'account_name', 'account_number', 'routing_number'], 'required'],

            [['prefrence', 'partner_id', 'store_id','routing_number', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
						['account_number', 'string', 'length' => [8, 16],'message' => "Please enter  valid account number"],
						['account_number', 'match', 'pattern' => '/^[0-9]*$/i','message' => "Please enter valid account number"],

						['routing_number', 'string', 'length' => [5, 10],'message' => "Please enter  valid routing number"],
						['routing_number', 'match', 'pattern' => '/^[0-9]*$/i','message' => "Please enter valid routing number"],
            [['promise_acid','bank_name', 'account_name'], 'string', 'max' => 50],

            [['partner_id', 'store_id'],'unique','targetAttribute' => ['partner_id','store_id'],'message' => 'This store have already bank account.'],

            [['store_id'], 'required', 'when' => function ($model) {
				return $model->prefrence ==2;
			}, 'whenClient' => "function (attribute, value) {
				return $('#bankdetail-prefrence').val()==2;
			}"],

            [['holder_type', 'account_type'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bank_name' => 'Bank Name',
            'disbursement'=>Yii::t('app',"Is disbursement account ?"),
            'account_name' => 'Account Name',
            'holder_type' => 'Holder Type',
            'account_type' => 'Account Type',
            'prefrence' => 'Account belong to:',
            'partner_id' => 'Partner',
            'store_id' => 'Store',
            'account_number' => 'Account Number',
            'routing_number' => 'BSB',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function beforeSave($insert)
		{
			    if (parent::beforeSave($insert)) {

			    	$this->updated_at= time();
			    	if($insert){

			    		$this->created_at= time();
			    		$this->created_by= Yii::$app->user->identity->id;
			     	}

			    	return true;
			    } else {

			     return false;
			    }
		}
		/* get the partner name name from  profile model */
	 public function getPartnername() {
		if($this->partner_id!='') {
				$lowner = \common\models\User::find()->select('orgName')->where(['id' =>$this->partner_id])->one();
					if($lowner){
							 return $lowner->orgName;
					}
					return 'N/A';
		}
	}
	
	/* get store name*/
	public function getStorename(){
		if($this->store_id != ''){
			$lowner = \common\models\Store::find()->select('storename')->where(['id' =>$this->store_id,'status'=>1])->one();
			if($lowner){
							 return $lowner->storename;
					}
					return 'N/A';
		}
	}

}
