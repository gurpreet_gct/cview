<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\User;
use common\models\Profile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_user_loyalty".
 *
 */
class UserLoyalty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
      public $nameOnCard;
      public $cardNumber;
      public $data;
      public $image;
      public $expiry;
      public $type;
      public $loyaltystatus;
      public $loyaltycardbrand;
      public $brandName;
    public static function tableName()
    {
        return '{{%user_loyalty}}';
    }


    public function rules()
    {
        return [

            [['user_id','loyaltyID'], 'safe'],
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'user_id' => Yii::t('app','User name'),
			'order_id' => Yii::t('app','Order ID'),
			'store_id' => Yii::t('app','Store ID'),
			'loyaltyID' => Yii::t('app','loyalty ID'),
			'status' => Yii::t('app','Status'),
			'created_at' => Yii::t('app','created at'),
			'updated_at' => Yii::t('app','updated at'),
			'created_by' => Yii::t('app','created by'),
			'updated_by' => Yii::t('app','updated by'),
			'pointvalue' => Yii::t('app','Loyalty dollors value'),
			'valueOnDay' => Yii::t('app','value On Day'),
			'loyaltypoints' => Yii::t('app','Loyalty dollors'),
			'value' => Yii::t('app','Value'),
			'points' => Yii::t('app','Dollors'),
			'accuredpoints' => Yii::t('app','Accured dollors'),
           ];
    }
    public function getCustomername(){
      $newvalue =  User::findOne(['id' => $this->user_id]);
		return $newvalue;
      }


 	public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	//$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
			    	if($insert){
			    		//$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			     	}

			    	return true;
			    } else {

			     return false;
			    }
			}



	public function fields()
	{
		//$fields=parent::fields();
			$fields=["user_id","store_id",
			"loyaltypoints"=>function($model)
			{return $model->points;},
			"loyaltyvalue"=>function($model)
			{return $model->value;},
			"brandName"=>function($model){
				return $model->profile->brandName;
			},
			"loyaltycardbrand"=>function($model){
				return $model->brandName;
			},
			"brandLogo"=>function ($model){
				if($model->profile->brandLogo!="")
			return Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$model->profile->brandLogo);
		else
			return "";
			},

			"loyaltyID"=>function ($model){
				if(strstr($model->loyaltyID,"-"))
			return Yii::$app->urlManagerApi->createAbsoluteUrl("/v1/barcodes?id=".$model->loyaltyID);
		else
			return $model->loyaltyID;
			},

			"id"=>function ($model){
				if(strstr($model->loyaltyID,"-"))
			return "";
		else
			return $model->loyaltyID;
			},

		    "backgroundHexColour"=>function($model){
				//return $model->profile->backgroundHexColour;
				return Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$model->profile->backgroundHexColour);
			},
            "foregroundHexColour"=>function($model){
				return $model->profile->foregroundHexColour;
			},

  	   'nameOnCard',
	   'cardNumber',
	   "image"=>function ($model){
				return Yii::$app->urlManagerLoyaltyCardFrontEnd->createAbsoluteUrl("loyaltycardImage/".$model->image);
			},
	   'expiry',

			"data"=>function ($model){
				return Yii::$app->urlManagerLoyaltyKit->createAbsoluteUrl($model->data);
			},

	   'type',
	   'loyaltystatus',

	  ];

		return $fields;
	}

	public function getProfile()
	{
		       return $this->hasOne(Profile::className(), ['user_id' => 'store_id']);
	//->leftJoin(Profile::tableName(), UserLoyalty::tableName().'.store_id = '.Profile::tableName(). '.user_id')
	}


	public function extraFields()
	{
		return ['profile'];
	}

	/*
	*@activeLoyalCust
	* return data of avg Loyal Transaction
	*/

	public function activeLoyalCust($startDate,$endDate,$userField){

		$query 		= Yii::$app->getDb();

		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND ul.store_id='.$userId;

		}else{

			$custQuery		= '';

		}

		$command 	= $query->createCommand("SELECT count(Distinct user_id) AS active_cust,str.orgName as storename FROM {{%user_loyalty}} AS ul JOIN {{%users}} str ON str.id=ul.store_id AND (ul.created_at BETWEEN $startDate AND $endDate) $custQuery GROUP BY ul.store_id");

		$result 	= $command->queryAll();

		return 	$result;

	}

}
