<?php 
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class Currencies extends ActiveRecord
{
	const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    public $amount,$from_currency,$to_currency;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currencies}}';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    
     /**
     * @inheritdoc
     */
    public function rules()
    {
        return [                    
            [['amount','from_currency','to_currency'], 'required','on'=>'currency-converter'],
            [['amount'], 'number']
        ];
    }
    
    public function attributeLabels()
    {
    	return [
    		'id' => Yii::t('app','ID'),
    		'country' => Yii::t('app','Country'),
    		'currency' => Yii::t('app','Currency name'),
            'code' => Yii::t('app','Currency code'),
            'symbol' => Yii::t('app','Currency symbol'),
    	];
    }

    public function getCurrencyData($id) {
        return Currencies::findOne(['id' => $id]);  
    }
}