<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bankdetail;

/**
 * BankdetailSearch represents the model behind the search form about `common\models\Bankdetail`.
 */
class BankdetailSearch extends Bankdetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'prefrence', 'partner_id', 'store_id', 'account_number', 'routing_number', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['bank_name', 'account_name', 'holder_type', 'account_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		 $usertype = Yii::$app->user->identity->user_type;
		 $userId = $usertype == 7 ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
		if($usertype == 1){
			 $query = Bankdetail::find();
		}else {
			 $query = Bankdetail::find()->where('partner_id='.$userId);
		}
       

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'prefrence' => $this->prefrence,
            'partner_id' => $this->partner_id,
            'store_id' => $this->store_id,
            'account_number' => $this->account_number,
            'routing_number' => $this->routing_number,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'account_name', $this->account_name])
            ->andFilterWhere(['like', 'holder_type', $this->holder_type])
            ->andFilterWhere(['like', 'account_type', $this->account_type]);

        return $dataProvider;
    }
}
