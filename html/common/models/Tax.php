<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\Country;
use common\models\States;

/**
 * Tax model
 *
 * @property integer $country_id
 * @property integer $state_id 
 * @property integer $postcoderange
 * @property integer $postcode
 
 */
 
class Tax extends ActiveRecord
{
   
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tax}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            [['id','country_id','postcode','rate'],'required'],
            ['id','unique'],
            [['state_id','postcoderange','created_at','updated_at','updated_by','created_by'],'safe'],          
            [['postcodeTo'], 'required', 'when' => function($model) {
               return '';
           },    
           'whenClient' => "function (attribute, value) {               
               
                  var  postcode = $('#tax-postcoderange').is(':checked');                         
						if(postcode ==1) {                           
							return true; }
						else {
						return false; }               
					}"    
   
				], 
				
		 [['postcode','postcodeTo'], 'integer', 'when' => function($model) {
               return '';
           },    
           'whenClient' => "function (attribute, value) {               
               
                  var  postcode = $('#tax-postcoderange').is(':checked');                         
						if(postcode ==1) {                           
							return true; }
						else {
						return false; }               
					}"    
   
				], 
				
		 ['postcodeTo','compare','compareAttribute'=>'postcode','operator'=>'>','message'=>'postcodeTo must be greater than postcode','when' => function($model) {
               return '';
           },    
           'whenClient' => "function (attribute, value) {               
               
                  var  postcode = $('#tax-postcoderange').is(':checked');                         
						if(postcode ==1) {                           
							return true; }
						else {
						return false; }               
					}"    
   
				], 
				
				
           
			];
		}
		
		
		public function getCountry(){
		
		return $this->hasOne(Country::className(), ['id_countries' => 'country_id']);	
		
		}
		
		public function getStates(){
		
		return $this->hasOne(States::className(), ['id' => 'state_id']);	
		
		}
		
		
		
		/*Get Post Code value */
		public function getPostcoderangeval(){
		
			if($this->postcoderange==1) {  return 'true';} else{ return 'false'; }
          
		
		}
		
		/*Get Country Name By country_id */
		public function getCountryNameval(){
		
			if(isset($this->country_id)){
			$result = Country::findOne(['id_countries' =>$this->country_id ]);	
				return $result->name;
			}else{
				return '';
			}
		
		}
		
		/*Get State Name By state_id */
		public function getStateNameval(){
			
		
		
			if(($this->state_id !='') && ($this->country_id !='')){
					
			$result = States::find()->where(['id' =>$this->state_id])->andWhere(['country_id' => $this->country_id])->one();
				return $result->name;	
			}else{
				return 'not set';
			}
		
		}


	    public function beforeSave($insert)
		{
				
			if (parent::beforeSave($insert)) {
			    	
			    	$this->updated_by	=Yii::$app->user->id;			    					    		
			    	$this->updated_at= time();	
			    		
			    	if($insert){			    		
			    		$this->created_by=Yii::$app->user->id;			    					    		
			    		$this->created_at= time();			    		
			    		
			     	}
			     	
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
		}
		
		public function afterFind()
		{
			parent::afterFind();
		
         
         
        return true;
		}
		
		public function attributeLabels()
    {
        return [
             'id' => Yii::t('app','Tax Identifier'),
            'country_id' => Yii::t('app','Country Name'),
            'state_id' => Yii::t('app','State Name'),
            'postcoderange'=>Yii::t('app','Postcode Range'),
            'postcode'=>Yii::t('app','Postcode'),
            'postcodeTo'=>Yii::t('app','Postcode To'),
            'rate'=>Yii::t('app','Rate'),
            'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
            'created_by'=>Yii::t('app','Created_By'),
            'updated_by'=>Yii::t('app','Updated_By'),
            
        ];
    }
		
		
		
		
   
}
