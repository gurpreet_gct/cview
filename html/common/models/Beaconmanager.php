<?php

namespace common\models;


use Yii;
use common\models\Profile;
use backend\models\Beaconlocation;
use backend\models\City;
use backend\models\Regions;
use backend\models\Beacongroup;
use backend\models\Storebeacons;
use common\models\Country;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tbl_beacon_manager".
 
 */
 
class BeaconManager extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
      public $imageFile;
    
      
    public static function tableName()
    {
       return '{{%beacon_manager}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'name', 'sn', 'groups', 'streetAddress', 'countryID', 'manufacturer', 'major', 'minor','notesBox'], 'required'],
            ['groups', 'groupsvalidation'],  
			[['minor','major'], 'checkuniqueminor'],
			[['rSSIValue', 'interval', 'locationOwnerID'], 'integer'],

			#[['latitude', 'longitude'],'string'],
			[['major', 'minor'],'integer'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
            'sn' => Yii::t('app','SN'),
            'notesBox' => Yii::t('app','Notes Box'),
            'linksUrl' => Yii::t('app','Links Url'),
            'majorValue' => Yii::t('app','Major Value'),
            'minorValue' =>Yii::t('app','Minor Value'),
            'powerValue' =>Yii::t('app','Power Value'),
            'rSSIValue' =>  Yii::t('app','RSSI value'),
            'soundFileUrl' => Yii::t('app','Sound File Url'),
            'timeStamp' =>Yii::t('app','Time Stamp') ,
            'interval' => Yii::t('app','Interval'),
            'category' => Yii::t('app','Category'),
            'groups' => Yii::t('app','Beacon Groups'),           
            'businessGalleryPictures' => Yii::t('app','Business Gallery Pictures'),
            'businessLocation' =>Yii::t('app','Business Location') ,
            'beaconCountry' => Yii::t('app','Beacon Country'),
            'businessName' => Yii::t('app','Business Name'),
            'businessPictureUrl' =>Yii::t('app','Business Picture Url') ,
            'businessSubscribe' => Yii::t('app','Business Subscribe'),
            'businessType' => Yii::t('app','Business Type'),
            'businessWebAddress' => Yii::t('app','Business Web Address'),
            'beaconSoundTitle' => Yii::t('app','Beacon Sound Title'),           
            'passUrl' => Yii::t('app','Pass Url'),
            'locationOwnerID' =>Yii::t('app','Relevant Partner') ,
            'locationBrockerID' => Yii::t('app','Relevant Location Broker'),
            'locationID' => Yii::t('app','Location'),
            'streetNumber' => Yii::t('app','Street Number'),
            'streetAddress' => Yii::t('app','Street Address'),
            'countryID' => Yii::t('app','Country'),
            'cityID' => Yii::t('app','City'),
            'suburbID' =>Yii::t('app','Region/Suburb') ,
            'postCode' => Yii::t('app','Postcode'),
            'latitude' => Yii::t('app','Latitude'),
            'longitude' =>Yii::t('app','Longitude'),           
            'manufacturer' => Yii::t('app','Manufacturer (beacon uuid)'),
            'manufacturerSerial' =>Yii::t('app','Manufacturer Serial Number') ,
            'uuid' => Yii::t('app','UUID'),
            'major' => Yii::t('app','Major'),
            'minor' => Yii::t('app','Minor'),
            'rssiID' => Yii::t('app','RSSI'),
            'intervalID' =>Yii::t('app','Ad Interval') ,
            'status' => Yii::t('app','Status'),
          ];
    }
    
    public function groupsvalidation($attribute,$param){
			 
			
			if(is_array($this->groups)){
					
			}else{
				$this->addError($attribute,'Beacon group cannot be blank.');
			 }
		}
		
		 public function checkuniqueminor($attribute,$param){
			 
			if($this->major && $this->minor && $this->id != ''){
							
			$beaconmodel = BeaconManager::find()->select('id')->where(['major' => $this->major])->andWhere(['minor' => $this->minor])->andWhere(['id' => $this->id])->one();			
	
				if(isset($beaconmodel->id)){				
					return true;			
				}
			$beaconmodels = BeaconManager::find()->select('id')->where(['major' => $this->major])->andWhere(['minor' => $this->minor])->one();			
				if(isset($beaconmodels->id)){				
					$this->addError($attribute,'Major and Minor values should be unique.');
				}else{
					return true;		  	
			
				}		
			
			}elseif($this->major && $this->minor != ''){			
			$beaconmodel = BeaconManager::find()->select('id')->where(['major' => $this->major])->andWhere(['minor' => $this->minor])->one();			
			
				if(isset($beaconmodel->id)){			  
			    $this->addError($attribute,'Major and Minor values should be unique.');
				} 		
			
			}
			
		}
		
		
		
    
		
    /* get the location owner name name from  profile model */	
     public function getlocationownername() {
		if($this->locationOwnerID!=''){		
		$lowner = User::find()->select('orgName')->where(['id' =>$this->locationOwnerID])->one();
		if($lowner){
			return $lowner->orgName;
		}
		}
		}
		
	/* get the location name from  Beaconlocation model */	
	public function getLocationbrokername()
		{
			
		if($this->locationBrockerID!=''){		  
		$lbroker = Profile::find()->select('companyName')->where(['id' =>$this->locationBrockerID])->one();	
		 return $lbroker->companyName;	
		}else{
			return false;
			}
			
			//return $this->hasOne(Beaconlocation::className(), ['id' => 'locationID']);
			
			
		}	
	/* get the city name from  City model */			
	public function getCityname()
		{
			return $this->hasOne(City::className(), ['id' => 'cityID']);
			
			
		}
	/* get the region name from  Regions model */		
	public function getRegionname()
		{
			return $this->hasOne(Regions::className(), ['id' => 'suburbID']);
			
			
		}
	/* get the country name from  Country model */		
	public function getCountryname()
		{
			return $this->hasOne(Country::className(), ['id_countries' => 'countryID']);
			
			
		}
		
	/* create relation with store table */
	public function getStorebeacons()
		{
			return $this->hasOne(Storebeacons::className(), ['beacon_id' => 'id']);
			
			
		}	
		
		
	/* get the categories name from  categories model */	
	public function getCategoryname()
		{
			$scat = $this->hasOne(Categories::className(), ['id' => 'category']);
			return $scat;
			
		}
		
	/* status type */
	public function getStatustype()
    {
    	  if($this->status==1){
         return 'Enable';    	  
    	  }else {
    	  return 'Disable';
       }
    }
    
    public function getStorename(){
		
	$storebeacon =  Store::find()->joinWith('storebeacons', true, 'INNER JOIN')->select('tbl_store_beacons.beacon_id ,tbl_store_beacons.store_id ,tbl_store.id,tbl_store.storename')->Where('store_id = tbl_store.id' )->andWhere('beacon_id ='.$this->id)->all()	; 
	$becondevices = array();
	foreach ($storebeacon as $beacondevice) {
		
				
		$becondevices[] =  $beacondevice['storename']; 
		
		}
		$comma_separated = implode(", ", $becondevices);
		return ($comma_separated);
	
	}
	
    
    /* get beacon group name by ids */
	public function getBeacongroupname(){
		
	$id = $this->groups;	
		
		if(isset($id)) {
				
		$model1 = Beacongroup::find()->select('group_name')->andWhere('id IN('.$id.')')->all(); 
		
		$GroupN = array();
		foreach ($model1 as $groupName) {
			$GroupN[] = $groupName['group_name'];
		 	 
		}
			$model = implode(', ', $GroupN);
			return $model; 
		}else{
			return 'No any Group Selected';
		}
	
	}
 
    
    
    
    
    
    public function beforeSave($insert)
			{
				
			    if (parent::beforeSave($insert)) {
			    	
			    	if($insert){			    		
			    		$this->uuid= Yii::$app->user->id;			    		
			    		$this->timeStamp= time();
			    		
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}
    
}
