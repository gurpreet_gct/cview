<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\TaxruleItem;


/**
 * Taxrule model
 *
 * @property integer $productclass
 * @property integer $country_id 
 * @property integer $priority_id
 * @property integer $sort_id
 
 */
 
class Taxrule extends ActiveRecord
{
   
    public $taxrate; 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxrule}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
                 [['productclass','name','taxrate','country_id','priority_id','sort_id'],'required'],                
                 ['name','unique'],            
			   ];
		}
		
		
		

	    public function beforeSave($insert)
		{
				
			if (parent::beforeSave($insert)) {
			    	
			    	$this->updated_by	=Yii::$app->user->id;			    					    		
			    	$this->updated_at= time();	
			    		
			    	if($insert){			    		
			    		$this->created_by=Yii::$app->user->id;			    					    		
			    		$this->created_at= time();			    		
			    		
			     	}
			     	
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
		}
		
		/*Get Country Name By country_id */
		public function getCountryNameval(){
		
			if(isset($this->country_id)){
			$result = Country::findOne(['id_countries' =>$this->country_id ]);	
				return $result->name;
			}else{
				return '';
			}
		
		}
		
		/*Get Tax Rate Value  */
		public function getTaxrateval(){
		
			if(isset($this->id)){
			    $taxrulItemModel =  TaxruleItem::find()->select('tax_id')->where(['rule_id' => $this->id])->all();
			   if(is_array($taxrulItemModel)){ 
				   $val = array();
				foreach($taxrulItemModel as $key=>$value) {				
					$val[$value->tax_id] = $value->tax_id;
				}
				return (implode(', ',$val));
			
			}else{
				return '';
			}
		
		 }else {
			 return '';
		}
	}
		
		
		public function getCountry(){
		
		return $this->hasOne(Country::className(), ['id_countries' => 'country_id']);	
		
		}
		
		public function afterFind()
		{
			parent::afterFind();
		
         
         
        return true;
		}
		
		public function attributeLabels()
		{
			return [
			'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
            'productclass' => Yii::t('app','Product Taxrule Class'),
            'country_id' => Yii::t('app','Taxrule Country'),
            'priority_id' => Yii::t('app','Priority'),
            'sort_id' => Yii::t('app','Sort Order'),
            'taxrate' => Yii::t('app','Tax Rate'),
            'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
            'created_by'=>Yii::t('app','Created_By'),
            'updated_by'=>Yii::t('app','Updated_By'),
           
				
			];
		}
		
		
		
		
   
}
