<?php

namespace common\models;

use Yii;
use common\models\FoodCategories;
use common\models\Product;
use common\models\Store;
class FoodProduct extends Product
{
    /**
     * @inheritdoc
     */
	  public $imageFile;
	 public $thumbimage;
	public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'p_type' => Yii::t('app','P Type'),
            'name' => Yii::t('app','Name'),
            'sku' => Yii::t('app','SKU'),
            'category' => Yii::t('app','Category'),
            'description' => Yii::t('app','Description'),
            'short_description'=> Yii::t('app','Short Description'),
            'urlkey' => Yii::t('app','Url Key'),
            'price' => Yii::t('app','Price'),
            'qty' => Yii::t('app','Quantity'),
            'status' => Yii::t('app','Status'),
            'is_online' => Yii::t('app','Is Online'),
            'image' => Yii::t('app','Image'),
            'store_id' => Yii::t('app','Store Id'),
            'related_product' => Yii::t('app','Related Product'),
            'is_added' => Yii::t('app','Is Added'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_by' => Yii::t('app','Updated By'),
        ];
    }

		public function beforeSave($insert)
	{
		
		if (parent::beforeSave($insert)) {
			date_default_timezone_set("UTC");

			if($this->related_product && is_array($this->related_product)){				
				$this->related_product = implode(',',$this->related_product);	
			}			
			$this->updated_at= time();
			$this->updated_by = Yii::$app->user->identity->id;
			if ($this->isNewRecord) {			
				$this->created_at = time();
				$this->created_by = Yii::$app->user->identity->id;
			}
			
			return true;
		} else {
			return false;
		}
	}
	public function afterFind()
    {
        parent::afterFind();            
           
        {
       
           if($this->category){
			   $this->category = explode(',',$this->category);
		   }    
				
        }
            
        return true;
    }
	public function getUsername(){
		if($this->created_by){
			$userModel = User::find()->select('username')->where(['id'=>$this->created_by])->one();
			if($userModel->username){
			return $userModel->username;
			}else{

			return 'N\A';
			}
		}
	}
/*	 public function getImagename(){
		  if(($this->image!="")&&(!strstr($this->image,'http'))) {
		
		   return \Yii::$app->request->BaseUrl.'/uploads/foodProductImages/'.$this->image;; 
		 }else {
			  return \Yii::$app->request->BaseUrl.'/uploads/foodProductImages/'.'no-image.jpg'; 
		 }
    } */
	public function getCatname(){
		if($this->category){
			$catName = FoodCategories::find()->select('name')
						->where(['id'=>$this->category])->all();
			$catArray = array();
			foreach($catName as $_catName){
				$catArray[]= $_catName->name; 
			}			
			return implode(', ',$catArray);
		}else{
			return 'N/A';
		}
	}
	public function getProducttype(){
		if($this->p_type==2)
		{
			return 'Simple';
		}else if($this->p_type==3){
			return 'Group';
		}
		else{
			return 'Simple';
		}
	}
	public function getAssociatedpro(){
		if($this->related_product){
			$ids= strlen($this->related_product)>1?explode(',',$this->related_product):$this->related_product;
			$pName = $this->find()->select('name')->where(['id'=>$ids])->all();
			$productName = array();
			if($pName){
				foreach($pName as $_product){
						$productName[] = $_product->name;
				}
				return implode(', ',$productName);
			}
		}
	}
	public function getStorename(){
		if($this->store_id)
		{
			$storeName = Store::find()->select('storename')
								->where(['id'=>$this->store_id])->one();								
			if(isset($storeName->storename)){
				return $storeName->storename;
			}else{
				return 'N\A';
			}
		}
	}
}
