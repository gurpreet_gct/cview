<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Login Logs model
 *
 * @property integer $id
 * @property integer $user_id 
 * @property string $device
 * @property string $ip
 * @property integer $login_at
 * @property integer $logout_at
 * @property string $sessionid
 
 */
class LoginLogs extends ActiveRecord
{
     public function rules()
    {
        return [
           
            [['resolution','my_prefrences','my_deals','my_loyalty','my_wallet','my_store','help'], 'safe'],

        ];
    }
    
    
        public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'user_id' => Yii::t('app','User_Id'),
			'sessionid' => Yii::t('app','Session_Id'),
			'ip' => Yii::t('app','Ip'),
			'device' => Yii::t('app','Device'),
			'login_at' => Yii::t('app','Login_At'),
			'logout_at' => Yii::t('app','Logout_At'),
			'address' => Yii::t('app','Address'),
			'resolution' => Yii::t('app','Resolution'),
			'my_prefrences' => Yii::t('app','My_Prefrences'),
			'my_deals' => Yii::t('app','My_Deals'),
			'my_loyalty' => Yii::t('app','My_Loyalty'),
			'my_wallet' => Yii::t('app','My_Wallet'),
			'my_store' => Yii::t('app','My_Store'),
			'help' => Yii::t('app','Help'),
			
           ];
    }
    
    
    
    

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%loginLogs}}';
    }
	  public function getUser()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id']);
    }
   
   
    /**
     * @countUserSession
	 * return users session array 
     */
	 
    public function countUserSession($startDate,$endDate,$userField){
		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= '{{%users}}.id='.$userId;
			
		}else{
			
			$custQuery		= '';
			
		}
		
		$query 	= LoginLogs::find();
		
		$report = $query->select(["count(tbl_loginLogs.id) as totalsession","FROM_UNIXTIME(login_at,'%b %d,%Y %h:%i:%s') AS loginat"])
						->joinWith('user',false,'INNER JOIN')
						->where(['between','login_at',$startDate,$endDate])
						->andWhere(['user_type'=>3])
						->andWhere($custQuery)
						->groupBy('user_id')
						->asArray()
						->all();
		
		return 	$report;		
		
    }
	
    /**
     * @countUserSession
	 * return users session array 
     */
	 
    public function activeAppUser(){
		
		$date 			= new \DateTime(date('m/d/Y',time()));
		
		$endDate 		= $date->getTimeStamp(); // max
		
		$date->sub(new \DateInterval('PT300S'));
		
		$startDate 		= $date->getTimeStamp(); // min
		
		$query 	= LoginLogs::find();
		
		$report = $query->select('count(tbl_loginLogs.id) as activesession')
						->joinWith('user',false,'INNER JOIN')
						->where(['between','tbl_loginLogs.login_at',$startDate,$endDate])
						->andWhere(['user_type'=>3])
						->asArray()
						->one();
		
		return 	$report;		
		
    }
	
	
}
