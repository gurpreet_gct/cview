<?php

namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;

// This model is only used for api data placement. 

class StoreOrders extends OrderItems
{


	public function attributeLabels()
    {
        return [
            
            'id' => Yii::t('app','ID'),
            'product_id' => Yii::t('app','Product Id'),
            'deal_id' => Yii::t('app','Deal ID'),
            'deal_Count' => Yii::t('app','Deal Count'),
            'isCombo' => Yii::t('app','Is Combo'),
            'qty' => Yii::t('app','Quantity'),
            'user_id' => Yii::t('app','User Id'),
            'order_id' => Yii::t('app','order Id'),
            'store_id' => Yii::t('app','Store Id'),
            'store_location_id' => Yii::t('app','Store Location Id'),
            'product_price' => Yii::t('app','Product Price'),
            'product_currency_code' => Yii::t('app','Product Currency Code'),
            'product_currency_id' => Yii::t('app','Product Currency Id'),
            'payment_currency_code' => Yii::t('app','Payment Purrency Code'),
            'payment_currency_id' => Yii::t('app','Payment Currency Id'),
            'exchange_rate' => Yii::t('app','Exchange Rate'),
            'product_exchange_price' => Yii::t('app','product exchange price'),
            'tax' => Yii::t('app','Tax'),
            'shipping_charges' => Yii::t('app','Shipping Charges'),
             'discount' => Yii::t('app','discount'),
             'total' => Yii::t('app','Total'),
             'payable' => Yii::t('app','Payable'),
             'status' => Yii::t('app','Status'),
            'created_at'=>Yii::t('app','Created At'),
            'Updated_at'=>Yii::t('app','Updated At'),
            'created_by'=>Yii::t('app','Created By'),
            'updated_by'=>Yii::t('app','Updated By'),
               
         
            ];
    }


    
    public function getOrdersubitems()
		{
			   return $this->hasMany(OrderSubItems::className(), ['order_id' =>'order_id','deal_id'=>'deal_id']); 
		}
		
		public function getStore()
		{
			   return $this->hasOne(Profile::className(), ['user_id' =>'store_id']); 
		}
		
		public function getPayment()
		{
			   return $this->hasOne(PaymentMethod::className(), ['order_id' =>'order_id']); 
		}
		
	  public function fields()
		{
			
			$fields=['brand' => function ($model) {
			$brann = isset($model->store->brandName)?$model->store->brandName:'';
            return $brann; // Return related model property, correct according to your structure
			},
			'brandlogo' => function ($model) {
			$branl = isset($model->store->brandLogo)?Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('profile/'.$model->store->brandLogo):'';		
            return $branl; // Return related model property, correct according to your structure
			},'payable','store_id','order_id',
			'placedOn'=>function($model){return date("Y-m-d",$model->created_at);},
			
			/*'payment'=>function($model){				
				return $model->payment;
				},*/
		
			];
			return $fields;
		}
		
		public function extraFields()
		{
			return [
			'ordersubitems',
			'payment',
			//'store',//=>['id','passUrl']
			

			];
		}
   
}
