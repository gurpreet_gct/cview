<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%foodorders_sub_item}}".
 *
 * @property string $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $parent_id
 * @property string $price
 * @property integer $qty
 * @property integer $created_at
 * @property integer $updated_at
 */
class FoodordersSubItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%foodorders_sub_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'parent_id', 'qty', 'created_at', 'updated_at'], 'integer'],
			[['child_pname'],'safe'],
            [['price'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'order_id' => Yii::t('app','Order ID'),
            'product_id' => Yii::t('app','Product ID'),
            'parent_id' => Yii::t('app','Parent ID'),
            'price' => Yii::t('app','Price'),
            'qty' => Yii::t('app','Qty'),
            'child_pname'=> Yii::t('app','Child Pname'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
		public function beforeSave($insert)
	{
		
		if (parent::beforeSave($insert)) {
			date_default_timezone_set("UTC");
						
			$this->updated_at= time();
				if ($this->isNewRecord) {			
				$this->created_at = time();
				}
			
			return true;
		} else {
			return false;
		}
	}
}
