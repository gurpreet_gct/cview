<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Usertype model
 *
 * @property integer $id
 * @property string $usertype 
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Usertype extends ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%usertype}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }
    
    
    
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','Id'),
            'usertype' => Yii::t('app','User Type'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
            ];
    }

    public static function getUsertypeName($id)
    {
        $usertype = Usertype::findOne(['id'=>$id]);
        return $usertype->usertype;
    }

    public static function getBidderType()
    {
        return Usertype::findOne(['usertype' => 'Bidder']);
    }
   
}
