<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\OrderItems;
use common\models\User;
use common\models\Workorders;

/**
 * Payment Method model
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $bt_id 
 * @property integer $status
 * @property integer $created_at
 
 */
 
class PaymentMethod extends ActiveRecord
{
   
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%paymentMethod}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            [['order_id','user_id'],'required'],
        ];
    }
    
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'order_id' => Yii::t('app','Order_Id'),
            'user_id' => Yii::t('app','User_Id'),
            'bin' => Yii::t('app','Bin'),
            'last4' => Yii::t('app','Last_4'),
            'cardType' => Yii::t('app','Card_Type'),
            'expirationMonth' => Yii::t('app','Expiration_Month'),
            'expirationYear' => Yii::t('app','Expiration_Year'),
            'customerLocation' => Yii::t('app','Customer_Location'),
            'cardholderName' => Yii::t('app','Card_Holder_Name'),
            'imageUrl' => Yii::t('app','Image_Url'),
            'prepaid' => Yii::t('app','Prepaid'),
            'healthcare' => Yii::t('app','Health_Care'),
            'debit' => Yii::t('app','Debit'),
            'durbinRegulated' => Yii::t('app','Durbin_Regulated'),
            'commercial' => Yii::t('app','Commercial'),
            'payroll' => Yii::t('app','Payroll'),
            'issuingBank' => Yii::t('app','issuingBank'),
            'countryOfIssuance' => Yii::t('app','Country_Of_Issuance'),
            'productId' => Yii::t('app','Product_Id'),
            'uniqueNumberIdentifier' => Yii::t('app','Unique_Number_Identifier'),
            'venmoSdk' => Yii::t('app','Venmo_Sdk'),
            'expirationDate' => Yii::t('app','Expiration_Date'),
            'maskedNumber' => Yii::t('app','Masked_Number'),
            'token' => Yii::t('app','Token'),
            'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
        ];
    }




	public function getTransaction()
	{
		   return $this->hasOne(Transaction::className(), ['order_id' =>'order_id','user_id'=>'user_id']); 
	}
	    public function beforeSave($insert)
		{
				
			if (parent::beforeSave($insert)) {			    	
			    	if($insert){			    		
			    		$this->user_id=Yii::$app->user->id;			    					    		
			    		$this->created_at= time();			    		
			    		
			     	}
			     	
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
		}
		
		 public function fields()
		{		
			
			
			return ['cardType','imageUrl','maskedNumber',
			'transactionID'=>function($model){return isset($model->transaction->tid)?$model->transaction->tid:0;}
			];
		}
		
		
	
	/*
	*@creditCardUsed
	* return data of credit card used
	*/			
		
	public function creditCardUsed($unixDateStart,$unixDateEnd,$userField){
		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= Workorders::tableName().'.workorderpartner='.$userId;
			
		}else{
			
			$custQuery		= '';
			
		}
		
		$query  = new PaymentMethod();
		
		$reslt 	= $query->find()
						->select("count(".PaymentMethod::tableName().".id) AS totalusedcard,".PaymentMethod::tableName().".cardType")
						->leftjoin(OrderItems::tableName(),OrderItems::tableName().'.order_id='.PaymentMethod::tableName().'.order_id')
						->leftjoin(Workorders::tableName(),Workorders::tableName().'.id='.OrderItems::tableName().'.deal_id')
						->leftjoin(User::tableName(),User::tableName().'.id='.Workorders::tableName().'.workorderpartner')
						->Where(['between',PaymentMethod::tableName().'.created_at', $unixDateStart,$unixDateEnd])
						->andWhere(PaymentMethod::tableName().'.order_id is not null')
						->andWhere($custQuery)
						->asArray()
						->groupBy(PaymentMethod::tableName().'.cardType')
						->all();
		
		return 	$reslt;
		
	}
		
		
   
}
