<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "tbl_polygons_region".
 *	
 */
class PolygonsRegion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	 
    public static function tableName()
    {
        return '{{%polygons_region}}';
    }
    
 
    public function rules()
    {
        return [
            [['id', 'deal_id', 'created_at', 'updated_at'], 'integer'],
          	[['polygon_shape'], 'required']
        ];
    }
    
    public function attributeLabels()
    {
        return [
           'id' => Yii::t('app','ID'),
            'deal_id' => Yii::t('app','Deal Id'),
            'polygon_shape' => Yii::t('app','Polygon Shape'),
            'message_limit' => Yii::t('app','Message Limit'),
            'created_at'=>Yii::t('app','Created At'),
			'updated_at'=>Yii::t('app','Updated At'),
        ];
    }
    
}
