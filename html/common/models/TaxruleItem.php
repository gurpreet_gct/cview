<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;


/**
 * TaxruleItem model
 *
 * @property integer $tax_id
 * @property integer $rule_id 
 * @property integer $is_delete

 
 */
 
class TaxruleItem extends ActiveRecord
{
   
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxrule_item}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
                 [['tax_id','rule_id','is_delete'],'required'],
                 ['id','unique'],
            
            ];
		}
		
		
		

	    public function beforeSave($insert)
		{
				
			if (parent::beforeSave($insert)) {
			    	
			    	$this->updated_by =Yii::$app->user->id;			    					    		
			    	$this->updated_at= time();	
			    		
			    	if($insert){			    		
			    		$this->created_by=Yii::$app->user->id;			    					    		
			    		$this->created_at= time();			    		
			    		
			     	}
			     	
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
		}
		
		public function afterFind()
		{
			parent::afterFind();
		
         
         
        return true;
		}
		
		public function attributeLabels()
		{
			return [
			'id' => Yii::t('app','ID'),
            'tax_id' => Yii::t('app','Tax Id'),
            'rule_id' => Yii::t('app','Rule Id'),
            'is_delete' => Yii::t('app','Is Delete'),
            'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
            'created_by'=>Yii::t('app','Created_By'),
            'updated_by'=>Yii::t('app','Updated_By'),
			];
		}
		
		
		
		
   
}
