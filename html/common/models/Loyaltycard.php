<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\User;
use common\models\Profile;
use common\models\UserLoyalty;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_loyaltypoints".
 *	
 */
class Loyaltycard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     //public $pin;
     //public $amount;
     public $store_id;
     public $loyaltypoints;
     public $loyaltyvalue;
     public $brandName;
     public $brandLogo;
     public $loyaltyID;
     public $backgroundHexColour;
     public $foregroundHexColour;
    public static function tableName()
    {
        return '{{%loyaltyCards}}';
    }
    
 
    public function rules()
    {
        return [
			[['nameOnCard','cardNumber'], 'required'],
			[['image'], 'safe', 'on' => 'create'],
			['type', 'typevalue'],
			[['nameOnCard','expiry'], 'string', 'max' => 200],
            [['cardNumber'], 'string', 'max' => 100],
            [['user_id','image','expiry','data','type','actualData'], 'safe'], 
			];
    }
    
    
    
     public function attributeLabels()
    {
        return [ 
              'id'=>Yii::t('app','ID'),
            'brand' => Yii::t('app','brand'),
            'user_id' => Yii::t('app','User_Id'),
            'nameOnCard' => Yii::t('app','Name_On_Card'),
            'cardNumber' => Yii::t('app','Card_Number'),
            'expiry' => Yii::t('app','Expiry'),
            'image' => Yii::t('app','Image'),
            'data' => Yii::t('app','Data'),
            'actualData' => Yii::t('app','Actual_Data'),
            'type' => Yii::t('app','Type'),
            'loyaltystatus' => Yii::t('app','Loyalty_Status'),
            'created_at' => Yii::t('app','Created_At '),
            'updated_at' => Yii::t('app','Updated_At'),
            'created_by' => Yii::t('app','Created_By'),
            'updated_by' => Yii::t('app','Updated_By'),
            
           ];
    }
    
    
   

public function typevalue($attribute,$params)
   {
	  $type_value = array("0","1");

	if (in_array($this->$attribute, $type_value))
	{
	return true;
	}
	else
	{
	  $this->addError($attribute, 'Please enter 0 or 1 value');
	}
        

 }
 
 
 	public function fields()
	{
		//$fields=parent::fields();
			$fields=["id","nameOnCard","cardNumber","expiry","type","image",
			"loyaltystatus","user_id",
			
			"data"=>function ($model){
				return Yii::$app->urlManagerLoyaltyKit->createAbsoluteUrl($model->data);			
			},
			"image"=>function ($model){
				return Yii::$app->urlManagerLoyaltyCardFrontEnd->createAbsoluteUrl("loyaltycardImage/".$model->image);			
			},
		];
		return $fields;
	}
 


    /**1456120518
     * @inheritdoc
     */
 public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
						
					if($this->expiry!="")
					$this->expiry=strtotime($this->expiry);
			    	$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
			    	if($insert){							    		
			    		$this->created_by= Yii::$app->user->id;			    		
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}
			
			
			public function afterFind()
			{
				parent::afterFind();
				
			/* expiry */
			 if(!strstr($this->expiry,"-"))
            {          
				if($this->expiry==0){
					$this->expiry=null;
				}else{
				$this->expiry=date("Y-m",$this->expiry);
				}  
                
                #$this->created_at=date("Y-m-d",$this->created_at);
                $this->updated_at=date("Y-m-d",$this->updated_at);
            }
				
			/* For front image */		
			if(($this->image!="")&&(!strstr($this->image,'http')))
			{
				
					#$this->image = Yii::$app->urlManagerLoyaltyCardFrontEnd->createAbsoluteUrl("loyaltycardImage/".$this->image); 
					
			}
			if(($this->data!="")&&(!strstr($this->data,'http')))
			{
				
					#$this->data =Yii::$app->urlManagerLoyaltyCardBarimage->createAbsoluteUrl("loyaltycard/".$this->data); 
					
			}
				
				   return true;
			  
				
			}
		  
    
}
