<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\User;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "tbl_ordercomment".
 *	
 */
class OrderComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_ordercomment';
    }
    
 
    public function rules()
    {
        return [
           [['comment', 'comment'], 'required'],
             [['status', 'status'], 'required'],
             [['email_to', 'email_to'], 'required'],
             [['order_id', 'order_id'], 'required'],
             [['author_id', 'author_id'], 'required']
            
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
             'comment' => Yii::t('app','Comment'),
            'status' => Yii::t('app','Status'),
            'onWeb' => Yii::t('app','OnWeb'),
            'id' => Yii::t('app','ID'),
            'auther_id'=>Yii::t('app','Auther_Id'),
            'order_id'=>Yii::t('app','Order_Id'),
            'email_to'=>Yii::t('app','Email_To'),
            'created_at'=>Yii::t('app','Created_At'),
            'Updated_at'=>Yii::t('app','Updated_At'),
            'created_by'=>Yii::t('app','Created_By'),
            'updated_by'=>Yii::t('app','Updated_By'),
            ];
    }
    
    public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
			    	if($insert){			    		
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}

   
  
    
}
