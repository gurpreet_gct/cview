<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_walletids".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $deal_id 
 * @property integer $created_at
 
 */
class DealBookmark extends \yii\db\ActiveRecord
{
    
     /**
     * @inheritdoc
     */
     

    
    public static function tableName()
    {
        return '{{%deal_bookmark}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['deal_id', 'user_id', ], 'required'],						
			['deal_id', 'validateBookmarkExists'],						
        ];
    }
    
        /**
     * Validates if the Bookmark Exists.
     * This method serves as the inline validation for Bookmark Exists.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateBookmarkExists($attribute, $params)
    {
	
        if (!$this->hasErrors()) {
            $bookmark = self::find()->where(['user_id'=>$this->user_id,'deal_id'=>$this->deal_id])->one();
           
			if ($bookmark) {
                $this->addError($attribute, 'This deal is already added in bookmark list.');
            }
        }
		
    }

    /**
     * @inheritdoc
     */
		public function attributeLabels()
		{
			return [
				'id' => Yii::t('app','ID'),
				'deal_id' =>Yii::t('app','Deal Id') ,
				'user_id' => Yii::t('app','User Id'),
				'created_at' =>Yii::t('app','Created At') ,
				
			];
		}
		
	public function fields()
	{
		$fields=parent::fields();
		unset($fields['created_at']);	
	
		return $fields;
	}
    
        public function extraFields()
        {
        return [
        'workorderpopup',//=>['id','passUrl']
        'workorders',

        ];
        }
        
         public function getDealstore()
		{
         return $this->hasMany(Dealstore::className(), ['deal_id' => 'deal_id']);
		} 
    
    	
			public function beforeSave($insert)
				{ 
					if (parent::beforeSave($insert)) {                        
                        
						
						if($insert){			    		
							
							$this->created_at= time();
						}
    				
						return true;
					} else {
						
					 return false; 
					}
				}
	
			public function afterFind()
			{
				parent::afterFind();				
				
				return true;		  
				
			}
			
}
