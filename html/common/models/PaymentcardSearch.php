<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Paymentcard;

/**
 * PaymentcardSearch represents the model behind the search form about `common\models\Paymentcard`.
 */
class PaymentcardSearch extends Paymentcard
{
   /**
     * @inheritdoc
     */
    public function rules()
    {
        return [


            [['created_at', 'updated_at'], 'integer', 'on' => 'create'],
            [['cardholderName'], 'string', 'max' => 200],
            [['uniqueNumberIdentifier'], 'string', 'max' => 100],
            [['user_id','cardType','maskedNumber', 'expirationDate','cardholderName','cardType'], 'safe'],
        ];
    }


    public function attributeLabels()
		{
			return [
				'id' => Yii::t('app','ID'),
				'cardType' => Yii::t('app','Card type'),
				'user_id' => Yii::t('app','User_Id'),
				'cardholderName' => Yii::t('app','Name on Card'),
				'maskedNumber' => Yii::t('app','Card Number'),
				'expirationDate' => Yii::t('app','Expiry'),
				'cvv' => Yii::t('app','CVV'),
				'created_at'=>Yii::t('app','Created_At'),
				'updated_at'=>Yii::t('app','Updated_At'),
			];
		}


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
      $usertype = Yii::$app->user->identity->user_type;
      $userId = $usertype==\common\models\User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
      $query = Paymentcard::find()
      ->joinWith('user', false, 'INNER JOIN');
      if($usertype!=\common\models\User::ROLE_ADMIN){
          $query->where(['user_id'=>$userId,'user_type'=>7]);
      }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

         if(!empty($this->expirationDate)){
		       $Expiry = strtotime($this->expirationDate);
		 }else{
			 $Expiry = $this->expirationDate;
		 }
        $query->andFilterWhere([
            'id' => $this->id,
            'expirationDate' => $Expiry,
            'maskedNumber' => $this->maskedNumber,
            'user_id' => $this->user_id,


        ]);

        $query->andFilterWhere(['like', 'cardholderName', $this->cardholderName]);
        $query->andFilterWhere(['like', 'cardType', $this->cardType]);


        return $dataProvider;
    }
}
