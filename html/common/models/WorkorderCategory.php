<?php
namespace common\models;
use \yii\db\ActiveRecord;
use Yii;

/**
 * Workorder Category Model
 *
 * 
 */
class WorkorderCategory extends ActiveRecord 
{
	  /**
     * @inheritdoc
     */
  
      
    public static function tableName()
    {
         return '{{%workorderCategory}}';
    }
    
    
	public function init()
	{
		
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
        ];     
    
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
             'id' => Yii::t('app','Id'),
            'workorder_id' => Yii::t('app','Workorder_Id'),
            'category_id' => Yii::t('app','Category_Id'),
            'status' => Yii::t('app','Status'),
            'firstName' => Yii::t('app','First Name'),
            'lastName' => Yii::t('app','Last Name'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }

	public function getCategories()
	{
		return $this->hasOne(Categories::className(), ['id' => 'category_id']);
	}
	
		
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			
            if ($this->isNewRecord) {
               $this->created_at=time();
            }
			   $this->updated_at=time();
				
            return true;
        }
        return false;
    }
    
}
