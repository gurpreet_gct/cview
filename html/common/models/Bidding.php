<?php

namespace common\models;
use common\models\Property;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

/**
 * This is the model class for table "{{%bidding}}".
 *
 * @property integer $id
 * @property integer $auction_id
 * @property integer $bidder_id
 * @property double $amount
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Bidding extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public $minimumBid=0;
    public $maxBid=0;
    public $statusText = 'Pending';
    
    public static function tableName()
    {
        return '{{%bidding}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			['amount', 'compareIntegerAmount'],
			//['amount','biddingAllowed'],
			[['amount','auction_id', 'bidder_id'],'required'],
            [['auction_id', 'bidder_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'bid_won','complete_status','turn_on_off','property_in_market','lastbid','cancel','is_vendor'], 'safe'],
            [['auction_id', 'bidder_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['amount','maxBid','minimumBid'], 'number'],                     
			[['auction_id','amount'], 'required','on'=>'Savebid'],
        ];
    }

	public function compareIntegerAmount_previous($attribute, $params)
	{
		$bidder=self::find()->where(['play_pause'=>0])->orderBy(['id' => SORT_DESC])->one();
		$lastbid=self::find()->where(['bidder_id'=>$this->bidder_id,'auction_id'=>$this->auction_id,'cancel'=>0])->orderBy(['id' => SORT_DESC])->one();
		$bidding=self::find()->where(['auction_id'=>$this->auction_id,'cancel'=>0])->orderBy(['id' => SORT_DESC])->one();

		if(!empty($lastbid)) {
			$this->addError($attribute, strtr(\Yii::t('app', "You are allowed to bid only once."),
			[" "]));
			if($lastbid->amount == $this->amount){
				$this->addError($attribute, strtr(\Yii::t('app', "The minimum bid should be greater than {minimumBid}"),
				['{minimumBid}'=>"$".number_format($this->amount,2)]));
			}
		} if(!empty($bidding)) {
			if($bidding->amount == $this->amount){
				$this->addError($attribute, strtr(\Yii::t('app', "The minimum bid should be greater than {minimumBid}"),
				['{minimumBid}'=>"$".number_format($this->amount,2)]));
			}else if($this->amount < $bidding->minimumBid) {	
				$this->addError($attribute, strtr(\Yii::t('app', "The minimum bid should be greater than {minimumBid}"),
				['{minimumBid}'=>"$".number_format($this->minimumBid,2)]));	
			}
		}
		if($this->amount < $this->minimumBid)
	    {	
			$this->addError($attribute, strtr(\Yii::t('app', "The minimum bid should be greater than {minimumBid}"),
			['{minimumBid}'=>"$".number_format($this->minimumBid,2)]));
		}
		else if($this->amount == $this->minimumBid)
		{
		    $biddingModel=self::find()->where(["auction_id"=>$this->auction_id])->one();
			if($biddingModel)
			{
				$this->addError($attribute, strtr(\Yii::t('app', "The minimum bid should be greater than {minimumBid}"),
				['{minimumBid}'=>"$".number_format($this->minimumBid,2)]));
			}	
		}
		 /*elseif($this->amount > $this->maxBid){
			
			$this->addError($attribute, strtr(\Yii::t('app', "The amount should be less than {maxBid} of total price"),
			['{maxBid}'=>$this->maxBid]));
			
		} */
	}
    
	public function compareIntegerAmount($attribute, $params)
	{
		$lastbid=self::find()->where(['auction_id'=>$this->auction_id, 'cancel'=>0])->orderBy(['id' => SORT_DESC])->one();
		//$bidding=self::find()->where(['auction_id'=>$this->auction_id,'cancel'=>0])->orderBy(['id' => SORT_DESC])->one();

		if(!empty($lastbid)) {
			//$this->addError($attribute, strtr(\Yii::t('app', "You are allowed to bid only once."),[" "]));
			if($lastbid->amount >= $this->amount){
				$this->addError($attribute, strtr(\Yii::t('app', "The minimum bid should be greater than {minimumBid}"),
				['{minimumBid}'=>"$".number_format($lastbid->amount,2)]));
			}/*else if($this->amount < $bidding->minimumBid) {	
				$this->addError($attribute, strtr(\Yii::t('app', "The minimum bid should be greater than {minimumBid}"),
				['{minimumBid}'=>"$".number_format($this->minimumBid,2)]));	
			}*/
        }
		/*if($this->amount < $this->minimumBid)
	    {	
			$this->addError($attribute, strtr(\Yii::t('app', "The minimum bid should be greater than {minimumBid}"),
			['{minimumBid}'=>"$".number_format($this->minimumBid,2)]));
		}
		else if($this->amount == $this->minimumBid)
		{
		    $biddingModel=self::find()->where(["auction_id"=>$this->auction_id])->one();
			if($biddingModel)
			{
				$this->addError($attribute, strtr(\Yii::t('app', "The minimum bid should be greater than {minimumBid}"),
				['{minimumBid}'=>"$".number_format($this->minimumBid,2)]));
			}	
		}*/
		 /*elseif($this->amount > $this->maxBid){
			
			$this->addError($attribute, strtr(\Yii::t('app', "The amount should be less than {maxBid} of total price"),
			['{maxBid}'=>$this->maxBid]));
			
		} */
	}    

	public function biddingAllowed($attribute, $params)
	{
		if($this->amount > $this->minimumBid)
	    {
			$auction=Auction::find()->where(["id"=>$this->auction_id])->one();
			/* allow prior bid */
			$startBefore = 0;
			$user_type   = 3;
			$biArr 		 = explode(',',$auction->reference_relevant_bidder);
			if($user_type==2 && Yii::$app->User->identity->id==$auction->estate_agent){
				$startBefore = $auction->real_estate_active_himself*60;
			}
			else if($user_type==7 && in_array($this->bidder_id,$biArr))
			{
				$startBefore = $auction->real_estate_active_bidders*60;
			}

			$datetimeStart = $auction->datetime_start;
			$allow = 0;
			$currentDateTime = strtotime(date('Y-m-d H:i:s',time()));
			if($datetimeStart<=$currentDateTime){
				$allow = 1;
			}

			/* allow prior bid */
			if($auction->bidding_status==0)
				$this->addError($attribute, (\Yii::t('app', "Bidding on this property has temporarily paused.")
			));
			else if($auction->bidding_status==2 && $allow != 1)
				$this->addError($attribute, (\Yii::t('app', "Auction has not yet started.")
			));
			else if($auction->bidding_status==3)
				$this->addError($attribute, (\Yii::t('app', "This auction has been closed.")
			));
		}
		/*elseif($this->amount > $this->maxBid){
			
			$this->addError($attribute, strtr(\Yii::t('app', "The amount should be less than {maxBid} of total price"),
			['{maxBid}'=>$this->maxBid]));
			
		}*/
	}

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auction_id' => 'Listing ID',
            'bidder_id' => 'Bidder ID',
            'amount' => 'Amount',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'statusText' => 'statusText',
        ];
    }
    
    
	public function getProperty()
    {
         return $this->hasOne(Property::className(), ['id' => 'auction_id']);
    }

	public function GetUser()
    {
         return $this->hasOne(User::className(), ['id' => 'bidder_id']);
    }
    
    /* bidder name */
	public function GetBidderName(){
		if($this->bidder_id){
			$Bidder = User::find()->select('username')->where(['id' => $this->bidder_id, 'status' => 1, 'user_type' => 7])->one();
			if($Bidder) { return $Bidder->username;  }elseif(!empty($this->is_vendor)){ return 'Vendor Bid'; }else{ return 'Onsite Agent';}
			 
		}
	
	} 
	public function GetStreamingStatus(){
		if($this->auction_id){
			$query=Auction::find()->select('streaming_status')->where(['auction_id'=>$this->auction_id])->one();
			return $query->streaming_status;
		}
	
	} 
	
	public function GetCancelAmount(){
		if($this->auction_id){
			$query=Bidding::find()->where(["auction_id"=>$this->auction_id])
								//->where(['bidder_id'=>Yii::$app->User->identity->id])
								->orderBy(['id'=>SORT_DESC])
								->limit(2)->all();
			return $query[0]['lastbid'];
		}
	
	}
	
	public function fields()
	{
		$fields = parent::fields();
		$fields['statusText'] = function ($model) {
			return $this->getStatusText($model->status);
		};        
		$fields['bidderDetail']= function ($model) {
			return  $model->user;
		};
		return $fields;		
	}

    /* Before Save */
	public function beforeSave($insert) {
	    if (parent::beforeSave($insert)) {
            //$token = Yii::$app->getRequest()->getHeaders()->get('Authorization');
            //$user_id = Yii::$app->MyConstant->UserID($token);

				#$this->status = 1;
				$this->updated_at= time();
				$this->updated_by= $this->bidder_id;
				if($insert){
					$this->created_at= time();
					$this->created_by= $this->bidder_id;
				}
				return true;
		    } else {
				return false;
			}
	}

	/**
	* Find list of all bids of the given property with status Pending, Accepted & Awarded
	*
	*/
	public function getAll($auction_id)
	{
		return $this->find()->where([ 'auction_id' => $auction_id, 'status' => [0,1,3] ])->orderBy('created_at ASC')->all();
	}

	/**
	* Find list of all bids of the given property with status Pending, Accepted & Awarded
	*
	*/
	public function getCurrentBid($auction_id)
	{
		return $this->find()->where(['auction_id' => $auction_id, 'status' => 3])->one();
	}

	/**
	* Get the detail of bid
	*/
	public function validateBid($id)
	{
		//return $res = Bidding::find()->alias('bidding')->where(['bidding.id' => $id])->joinWith('auction')->one();
        $res = Bidding::find()->alias('bidding')->where(['bidding.id' => $id])->one();
        if(empty($res)) return false;
        else return $res;
	}
    
	/* Make relation Auction Event and Bidding */
	public function getAuction()
	{	
		return $this->hasOne(Auction::className(), ['id' => 'auction_id']);
	}     

    /**
	 * Find list of all bidders of the given property
	 *
	 */
	public function actionGetCurrentBid($auction_id)
	{
		return $this->find()
					->where(['auction_id' => $auction_id, 'status' => 0, 'cancel' => 0])
					->limit(1)
					->orderBy(['created_at' => SORT_DESC])
					->get();
	}
    
    /*send email to all bidder */
    
    public function sendemailtobidder($listingID,$bidderId){
        $user=ArrayHelper::map(Bidding::find()
        ->join("INNER JOIN",User::tableName(),User::tableName().".id=bidder_id")
        //->select([Bidding::tableName().".id","bidder_id"])
        ->where(['auction_id'=>$listingID])->all(),'id','bidder_id');	
        if(!empty($user)){
            if(is_array($user)){ 
                $value = implode(',',$user); 
                $AllBidder = ArrayHelper::map(User::find()->where(User::tableName().'.id IN ('.$value.')')->andWhere(['user_type'=>7])->all(),'username','email');
            }
        }
        $winnerBidder = ArrayHelper::map(User::find()->where(['id'=>$bidderId,'user_type'=>7])->all(),'username','email');
        $LoosingBidder =array();
        $LoosingBidder = array_diff($AllBidder,$winnerBidder);
        $subject	= 'GoBiddo: $1000 refundable security deposit';
        $property=Yii::$app->urlManager->createAbsoluteUrl(['property/view', 'id' => $listingID ]);
        $propertyname=Property::find()->select('title')->where(['id'=>$listingID])->one();
        if(!empty($winnerBidder)){
            $email=array_values($winnerBidder);
            foreach($email as $mail){	
            }
            $emailData = [
                'Name' => key($winnerBidder),
                'customeText' => 'Congratulations on winning the <a href="'.$property.'">'.$propertyname->title.'</a> auction. Please note your credit card will be refunded the $1000 security deposit within 2 business days. '
            ];
            Yii::$app->mail->compose(['html' => 'generic-mail-template'],$emailData)
            ->setTo($mail)
            ->setFrom(Yii::$app->params["noReplyEmail"],'No Reply')
            ->setSubject($subject)
            ->send();
        }
	
        if(!empty($LoosingBidder)){
            foreach($LoosingBidder as $key => $loosing){
                $mail=$loosing;
                $emailData = [
                    'Name' => $key,
                    'customeText' => 'Following your unsuccessful bid on the GoBiddo website, your credit card will be refunded the $1000 security deposit within 2 business days. We look forward to having you participate in future auctions. '
                ];
                Yii::$app->mail->compose(['html' => 'generic-mail-template'],$emailData)
                ->setTo($mail)
                ->setFrom(Yii::$app->params["noReplyEmail"],'No Reply')
                ->setSubject($subject)
                ->send();
            }
        }
	}
	
	public function afterFind(){
		parent::afterFind();
		if(!strstr($this->updated_at,"-") && !empty($this->updated_at)){             
			$this->updated_at=date("Y-m-d H:i:s",$this->updated_at);
		}						
		if(!strstr($this->created_at,"-") && !empty($this->created_at)){             
			$this->created_at=date("Y-m-d H:i:s",$this->created_at);
		}
		return true;		
	}
    
    public function getStatusText($status){
        $statusText = 'Pending';
        if($this->status == 1) $statusText = 'Accepted';
        if($this->status == 2) $statusText = 'Rejected';
        if($this->status == 3) $statusText = 'Current Bid';
        if($this->status == 4) $statusText = 'Awarded';
        return $statusText;
    }
}