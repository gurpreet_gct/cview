<?php

namespace common\models;
use common\models\Cardtype;

use Yii;

/**
 * This is the model class for table "tbl_walletids".
 *
 * @property integer $id
 * @property integer $cardType
 * @property string $nameOnCard
 * @property string $cardNumber
 * @property string $back
 * @property string $front
 * @property integer $expiry
 * @property integer $created_at
 * @property integer $updated_at
 */
class Id extends \yii\db\ActiveRecord
{
    
     /**
     * @inheritdoc
     */
    public $imageFile1;
    public $imageFile2;
     

    
    public static function tableName()
    {
        return '{{%walletids}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['cardType'], 'required'],			
		//	[['front','back'],'required',  'on' => 'create'], 
			[['imageFile1','imageFile2'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
			[['cardType'],'number'], 
            [['created_at', 'updated_at'], 'integer', 'on' => 'create'],
            [['nameOnCard','expiry'], 'string', 'max' => 200],
            [['cardNumber'], 'string', 'max' => 100],
             [['front','back','nameOnCard',], 'safe'], 
        ];
    }

    /**
     * @inheritdoc
     */
		public function attributeLabels()
		{
			return [
				'id' => Yii::t('app','ID'),
				'cardType' => Yii::t('app','Card type'),
				'user_id' => Yii::t('app','User_Id'),
				'nameOnCard' => Yii::t('app','Name on Card'),
				'cardNumber' => Yii::t('app','Card Number'), 
				'expiry' =>Yii::t('app','Expiry') ,
				'front' => Yii::t('app','Front Photo'),
				'back' => Yii::t('app','Back Photo'),				
				'created_at' => Yii::t('app','Created At'),
				'updated_at' =>Yii::t('app','Updated At') ,
			];
		}
		
	public function fields()
	{
		$fields=parent::fields();
		unset($fields['created_at']);
		unset($fields['updated_at']);
	//	unset($fields['cardType']);
		return $fields;
	}
    public function extraFields()
	{
		 return ['cardtype'];
	}
	
    
    public function getCardtype()
    {
          return $this->hasOne(Cardtype::className(), ['id' => 'cardType']);
    }
    
		  public function getImageUrl()
			{
				   if(($this->front!="")&&(!strstr($this->front,'http')))
					{
						
							return $this->front=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("Id/".$this->front); 
							
					}
					 if(($this->back!="")&&(!strstr($this->back,'http')))
					{
						
							$this->back=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("Id/".$this->back); 
							
					}
			}
			
		public function getFrontimage(){
			if($this->front!='') {
				return $this->front; 
			}else {
				return Yii::$app->params['userUploadPath']."/".'noimage.png'; 
			}
		}
		
		public function getBackimage(){
			if($this->back!='') {
				return $this->back; 
			}else {
				return Yii::$app->params['userUploadPath']."/".'noimage.png'; 
			}
		}
		
		
			public function beforeSave($insert)
				{
					if (parent::beforeSave($insert)) {                        
                        
						if($this->expiry!="")
								
						$this->expiry=strtotime($this->expiry);
						
						$this->updated_at= time();
						
						if($insert){			    		
							
							$this->created_at= time();
						}
                        else
                        {
                                preg_match('/.*\/(.*?)\./',$this->front,$match);				
                                if(count($match)) 
                                    $this->front=$match[1]. ".jpg";                
                                preg_match('/.*\/(.*?)\./',$this->back,$match2);				
                                if(count($match2)) 
                                    $this->back=$match2[1]. ".jpg";                
                        }
					
						return true;
					} else {
						
					 return false; 
					}
				}
	
			public function afterFind()
			{
				parent::afterFind();
				
			/* expiry */
			 if(!strstr($this->expiry,"-"))
            {          
				if($this->expiry==0){
					$this->expiry=null;
				}else{
				$this->expiry=date("Y-m",$this->expiry);
				}  
                
                $this->created_at=date("Y-m-d",$this->created_at);
                $this->updated_at=date("Y-m-d",$this->updated_at);
            }
				
			/* For front image */		
			if(($this->front!="")&&(!strstr($this->front,'http')))
			{
				
					$this->front =Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("Idimages/".$this->front); 
					
			}
			/* For back image */
		   if(($this->back!="")&&(!strstr($this->back,'http')))
			{
				
					$this->back =Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("Idimages/".$this->back); 
					
			}
				
				   return true;
			  
				
			}
			
}
