<?php
 
namespace common\models;
 
 
use Yii;
 
/**
 * This is the model class for table "tbl_userPsa".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $workorder_id
 * @property string $status
 * @property int $created_at
 */
class UserPsa extends \yii\db\ActiveRecord
{
    
     /**
     * @inheritdoc
     */
     
    
    
    public static function tableName()
    {
        return '{{%userPsa}}';
    }
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'workorder_id','status','created_at'], 'required'],
            [['geo_status'],'safe'],
           
        ];
    }
 
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
           'id' => Yii::t('app','Id'),
            'user_id' => Yii::t('app','User Name'),
            'workorder_id' => Yii::t('app','Workorder Id'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
    
  
    
    public function fields()
   {
       $fields=parent::fields();
       unset($fields['created_at']);
   
   
       return $fields;
   }
    
    
        public function afterFind()
        {
            parent::afterFind();
            
           return true;
               
        }
    
	
	// used to get NOTIFICATIONS (SENT VS. OPENED)
	
	public function notifications($unixDateStart,$unixDateEnd,$filterType,$userField){
		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND w.workorderpartner='.$userId;
			
		}else{
			
			$custQuery		= '';
			
		}
		
		
		$query = Yii::$app->getDb();
		
		if($filterType=='D'){
			
			$group = 'HOUR(FROM_UNIXTIME(UP.created_at))';
			
			$specifier = '%b %d,%Y %h:%i:%s';
			
		}elseif($filterType=='W' || $filterType=='M'){
			
			$group = 'DAY(FROM_UNIXTIME(UP.created_at))';
			$specifier = '%b %d,%Y';
			
		}elseif($filterType=='Y'){
			
			$group 		= 'MONTH(FROM_UNIXTIME(UP.created_at))';
			$specifier 	= '%b,%Y';
			
		}elseif($filterType=='O'){
			
			$group = 'DAY(FROM_UNIXTIME(UP.created_at))';
			
			$specifier = '%b %d,%Y %h:%i:%s';
			
		}
		
		$command 	= $query->createCommand("SELECT count(if(UP.status=0,1,null)) AS notOpened,count(if(UP.status=1,1,null)) AS opended,FROM_UNIXTIME(UP.created_at,'".$specifier."') AS datetime FROM {{%userPsa}} UP LEFT JOIN {{%users}} t ON UP.user_id=t.id LEFT JOIN {{%workorders}} w ON w.id=UP.workorder_id WHERE (UP.created_at BETWEEN $unixDateStart AND $unixDateEnd) $custQuery GROUP BY $group ORDER BY UP.created_at ASC");
		
		$result 	= $command->queryAll();
		
		return 	$result;
		
	}	
   
   
}
