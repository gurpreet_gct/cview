<?php

namespace common\models;
use common\models\FoodCategories;
use Yii;

/**
 * This is the model class for table "{{%foodcategory_products}}".
 *
 * @property string $id
 * @property integer $product_id
 * @property integer $category_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class FoodcategoryProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%foodcategory_products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'category_id', 'status', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'product_id' => Yii::t('app','Product ID'),
            'category_id' => Yii::t('app','Category ID'),
            'firstName' => Yii::t('app','First Name'),
            'lastName' => Yii::t('app','Last Name'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
    
    public function getFoodcategories()
	{
		return $this->hasOne(Foodcategories::className(), ['id' => 'category_id']);
	}
}
