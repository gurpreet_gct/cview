<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\Address;
use frontend\models\UserStore;
/**
 * Order model
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property integer $created_at

 */

class Order extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'],'required'],
            ['transaction_id','safe'],
        ];
    }



	 public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'status' => Yii::t('app','Status'),
			'coupon_code' => Yii::t('app','Coupon_Code'),
			'shipping_description' => Yii::t('app','Shipping_Description'),
			'shipper_id' => Yii::t('app','Shipper_Id'),
			'is_offline' => Yii::t('app','Is_Offline'),
			'user_id' => Yii::t('app','User_Id'),
			'discount_amount' => Yii::t('app','Discount_Amount'),
			'grand_total' => Yii::t('app','Grand_Total'),
			'shipping_amount' => Yii::t('app','Shipping_Amount'),
			'shipping_tax_amount' => Yii::t('app','Shipping_Tax_Amount'),
			'subtotal' => Yii::t('app','Subtotal'),
			'tax_amount' => Yii::t('app','Tax_Amount'),
			'total_offline_refunded' => Yii::t('app','Total_Offline_Refunded'),
			'total_online_refunded' => Yii::t('app','Total_Online_Refunded'),
			'total_paid' => Yii::t('app','Total_Paid'),
			'qty_ordered' => Yii::t('app','Qty_Ordered'),
			'total_refunded' => Yii::t('app','Total_Refunded'),
			'can_ship_partially' => Yii::t('app','Can_Ship_Partially'),
			'customer_note_notify' => Yii::t('app','Customer_Note_Notify'),
			'billing_address_id' => Yii::t('app','Billing_Address_Id'),
			'billing_address' => Yii::t('app','Billing_Address'),
			'shipping_address_id' => Yii::t('app','Shipping_Address_Id'),
			'shipping_address' => Yii::t('app','Shipping_Address'),
			'forced_shipment_with_invoice' => Yii::t('app','Forced_Shipment_With_Invoice'),
			'weight' => Yii::t('app','Weight'),
			'email_sent' => Yii::t('app','Email_Sent'),
			'edit_increment' => Yii::t('app','Edit_Increment'),
			'created_at' => Yii::t('app','created_at'),
			'updated_at' => Yii::t('app','updated_at'),
			'created_by' => Yii::t('app','created_by'),
			'food_oid' => Yii::t('app','food order id'),
			'updated_by' => Yii::t('app','updated_by'),

           ];
    }






	    public function beforeSave($insert)
		{

			if (parent::beforeSave($insert)) {

			    	if($insert){
			    		$this->user_id=Yii::$app->user->id;
			    		$this->created_at= time();

			     	}


			    	return true;
			    } else {

			     return false;
			    }
		}

		public function getOrderitems()
		{
			   return $this->hasMany(OrderItems::className(), ['order_id' =>'id']);
		}

		public function getShippingaddress()
		{
			   return $this->hasMany(Address::className(), ['id' =>'shipping_address_id']);
		}

		public function getBillingaddress()
		{
			   return $this->hasMany(Address::className(), ['id' =>'billing_address_id']);
		}



		public function getStatustype()
    {
    	  if($this->status==1){
         return 'Confirmed';
    	  }else {
    	  return 'Pending';
       }
    }

		 public function fields()
		{

			$fields=parent::fields();
			unset($fields['created_at']);
			unset($fields['updated_at']);
			unset($fields['created_by']);
			unset($fields['updated_by']);

			return $fields;
		}


		public function extraFields()
		{
			return [
			'orderitems',
			'shippingaddress',
			'billingaddress',

			];
		}
		public function getBrandname(){
			if(isset($this->orderitems[0]->store_id)){
				$profile=Profile::find()->select('brandName')
				->where(['user_id'=>$this->orderitems[0]->store_id])->one();
				if($profile->brandName !=''){
					return $profile->brandName;
				}else{
					return '';
				}
			}
		}

		public function getThumbimage(){
		/* Find Brand images */
		if(isset($this->orderitems[0]->store_id)) {
			 $profile = Profile::find()->select('brandLogo')->where(['user_id'=>$this->orderitems[0]->store_id])->one();
			 if($profile->brandLogo !=''){
				 return Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$profile->brandLogo);

			}else{
			 return 'no image found';
			}
		}

	 }
	public function storefavourite($id)
	{
		$model = UserStore::find()
		->where(['store_id' => $id])
		->Andwhere(['user_id' => Yii::$app->user->id])
		->one();
	     if(isset($model->user_id)) {
           if($model->status==1){

					return true;
			    }else {
					 return 0;
			  }
        }
    }
    /*public function afterSave($insert, $changedAttributes){
		
		if($this->subtotal>0 && ($this->status==2 || $this->is_offline==1)){
			$this->saveReferral($this);
		}
		return true;
	}
	
	public function saveReferral($model){		
		  $percentage = new \common\models\Setting();
          $amt = $percentage->getPercentage();
          if(is_array($amt)){			
			 $referalModel = new \common\models\ReferralEarnings(); 			
			 $referalModel->user_id = Yii::$app->user->id;
			 $referalModel->refer_by = $amt['referBy'];
			 $referalModel->commission_amount = ($amt['amount']/100)*$model->subtotal;
			 $referalModel->commission_percentage = $amt['amount'];
			 $referalModel->order_id = $model->id;
			 $referalModel->order_total = $model->subtotal;
			 $referalModel->status = 0;
			 $referalModel->save(false);
		  }
	}*/

}
