<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "tbl_addresstype".
 *	
 */
class Addresstypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%addresstype}}';
    }
    
 
    public function rules()
    {
        return [
        [['type'],'required']
        ];
    }
    
    
    
    public function attributeLabels()
    {
        return [
              'id' => Yii::t('app','Id'),
              'type' => Yii::t('app','Type'),
              'is_delete' => Yii::t('app','Is_Delete'),
             'created_at' => Yii::t('app','Created_At'),
             'updated_at' => Yii::t('app','Updated_At'),
             'created_by' => Yii::t('app','Created_By'),
             'updated_by' => Yii::t('app','Updated_By') ,
            
        ];
    }
    
    
    

    /**
     * @inheritdoc
     */
   
   public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
			    	if($insert){			    		
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}
			
			
	  public function fields()
		{
			
			$fields=parent::fields();		  
			unset($fields['created_at']);
			unset($fields['updated_at']);
			unset($fields['created_by']);
			unset($fields['updated_by']);
			
			return $fields;
		}
				
			
   }
