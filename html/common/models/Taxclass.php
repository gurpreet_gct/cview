<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\Country;
use common\models\States;

/**
 * Tax model
 *
 * @property integer $country_id
 * @property integer $state_id 
 * @property integer $postcoderange
 * @property integer $postcode
 
 */
 
class Taxclass extends ActiveRecord
{
   
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxclass}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
                ['taxClass','required'],
                 ['taxClass','string'],
            
            ];
		}
		
		
		

	    public function beforeSave($insert)
		{
				
			if (parent::beforeSave($insert)) {
			    	
			    	$this->updated_by	=Yii::$app->user->id;			    					    		
			    	$this->updated_at= time();	
			    		
			    	if($insert){			    		
			    		$this->created_by=Yii::$app->user->id;			    					    		
			    		$this->created_at= time();			    		
			    		
			     	}
			     	
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
		}
		
		public function afterFind()
		{
			parent::afterFind();
		
         
         
        return true;
		}
		
		public function attributeLabels()
		{
			return [
            'id' => Yii::t('app','ID'),
            'taxClass' => Yii::t('app','Tax Class'),
            'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
            'created_by'=>Yii::t('app','Created_By'),
            'updated_by'=>Yii::t('app','Updated_By'),
				
			];
		}
		
		
		
		
   
}
