<?php
namespace common\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Pass;
use common\models\Workorderpopup;
use common\models\Workorders;

/**
 * IdSearch represents the model behind the search form about `common\models\Id`.
 */
class PassSearch extends Pass
{
   /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
		
			
             [['user_id', 'workorder_id','offerTitle','offerwas','offernow','readTerms','sspMoreinfo','isUsed','count_passes'], 'safe'], 
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','Id'),
            'user_id' => Yii::t('app','User_Id'),
            'store_id' => Yii::t('app','Store_Id'),
            'workorder_id' => Yii::t('app','Workorder_Id'),
            'offerTitle' => Yii::t('app','Offer_Title'),
            'offerText' => Yii::t('app','Offer_Text'),
            'offerwas' => Yii::t('app','Offer_Was'),
            'offernow' => Yii::t('app','Offer_Now'),
            'readTerms' => Yii::t('app','Read_Terms'),
            'sspMoreinfo' => Yii::t('app','SSP_Moreinfo'),
            'created_at' => Yii::t('app','created at'),
			'updated_at' => Yii::t('app','updated at'),
			'isUsed' => Yii::t('app','Is_Used'),
			'count_passes' => Yii::t('app','Count_Passes'),
			'cardTrack' => Yii::t('app','Card_Track'),
        ];
    }
    
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		/* $user_id = Yii::$app->user->id;	
		$Workorders= new Workorders();
		$Workorderpopup= new Workorderpopup();
        return $dataProvider = new ActiveDataProvider([
				'query' => Pass:: find()
							->joinWith("workorders")
							->joinWith("workorderpopup")
							->joinWith("profile")
							->where([Pass::tableName().'.user_id' => $user_id,Workorders::tableName().'.workorderType' => 1,Workorders::tableName().'.status' =>1])							

							->andWhere(['or',Pass::tableName().'.count_passes >=0 and'.Workorders::tableName().'.type=4',Pass::tableName().'.count_passes >'.Pass::tableName().'.isUsed and'.Workorders::tableName().'.type=1 and '.Workorders::tableName().'.allowpasstouser >'.Pass::tableName().'.isUsed'])							
							->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d")))),
							//->orderBy("id desc")
				]);*/
				
				
				
				$user_id = Yii::$app->user->id;  	
		$pass_count= new Pass();
		$Workorders= new Workorders();
		$Workorderpopup= new Workorderpopup();
		return $dataProvider = new ActiveDataProvider([
				'query' => Pass:: find()
							->joinWith("workorders")
							->joinWith("workorderpopup")
							->joinWith("profile")
							->where([Pass::tableName().'.user_id' => $user_id,Workorders::tableName().'.workorderType' => 1,Workorders::tableName().'.status' =>1])							
							
							#->andWhere(Pass::tableName().'.isUsed <'.Workorders::tableName().'.allowpasstouser')	
							->andWhere(Pass::tableName().'.isUsed !='.Pass::tableName().'.passesTrack' )
							
							->andWhere(Workorders::tableName().'.type!=2')					

							->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d")))),
				]);
    }
    /**************** ticket listing *************************/
    public function ticketsearch($params)
    {
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$Workorderpopup= new Workorderpopup();
	return $dataProvider = new ActiveDataProvider([ 
					 'query' =>	Pass:: find()
						->joinWith("workorders")
						->joinWith("workorderpopup")
						->joinWith("profile")
						->where([Pass::tableName().'.user_id' => $user_id,Workorders::tableName().'.workorderType' => 1,Workorders::tableName().'.type' => 4,Workorders::tableName().'.status' =>1])
						->andWhere(Pass::tableName().'.isUsed > 0')
						->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d")))),
					]);
    }
    /**************** end here *************************/
    /********************** get all redeemed pass ***************/
    public function redeemedsearch(){
	$user_id = Yii::$app->user->id;  	
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$Workorderpopup= new Workorderpopup();
	return $dataProvider = new ActiveDataProvider([
            'query' => Pass:: find()
						->joinWith("workorders")
						->joinWith("workorderpopup")
						->joinWith("profile")
						->where([Pass::tableName().'.user_id' => $user_id,Workorders::tableName().'.workorderType' => 1,Workorders::tableName().'.status' =>1,Workorders::tableName().'.type' => 1])
						->andWhere(Pass::tableName().'.isUsed > 0')
						->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d")))),
            ]);
            
	
	}
    /********************** end here **************************/
    
    /********************** get all stamp cards  ***************/
    public function stampcardsearch(){
	$user_id = Yii::$app->user->id; 
	$pass_count= new Pass();
	$Workorders= new Workorders();
	$Workorderpopup= new Workorderpopup();
	return $dataProvider = new ActiveDataProvider([
            'query' => Pass:: find()
						->joinWith("workorders")
						->joinWith("workorderpopup")
						->joinWith("profile")
						->where([Pass::tableName().'.user_id' => $user_id,Workorders::tableName().'.workorderType' => 1,Workorders::tableName().'.type' => 2,Workorders::tableName().'.status' =>1])
						->andWhere(sprintf('dateTo >= UNIX_TIMESTAMP("%s")' ,(date("Y-m-d")))),
						]);
	
	}
    /********************** end here **************************/
    
    
}
