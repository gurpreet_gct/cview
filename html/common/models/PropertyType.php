<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "tbl_property_types".
 *
 *
 */
class PropertyType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%property_types}}';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['name'],'safe'],
        	[['name','status'],'required'],
        	['name','unique'],
        	[['status','created_at', 'updated_at'],'number'],     
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'name' => Yii::t('app','Name'),
			'status' => Yii::t('app','Status'),
			'created_at' => Yii::t('app','Created_At'),
			'updated_at' => Yii::t('app','Updated_At'),
        ];
    }
	
	public function getParent()
	{
		return $this->hasOne($this->className(), ['id' =>'parent_id']);
	}

    public function getIdByName($name)
    {   
        return $this->findOne(['name' => $name]);
    }

	public function fields()
	{		
		$fields=parent::fields();		  
		unset($fields['created_at']);
		unset($fields['updated_at']);

		return $fields;
	}
    
	/**
	* Find list of all active property types
	*
	*/
	public function getAll($auction_id)
	{
		return $this->find()->where([ 'status' => 1 ])->all();
	}    

}
