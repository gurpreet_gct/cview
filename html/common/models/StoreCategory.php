<?php
namespace common\models;
use \yii\db\ActiveRecord;
use Yii;

/**
 * Store Category Model
 *
 * 
 */
class StoreCategory extends ActiveRecord 
{
	  /**
     * @inheritdoc
     */
  
      
    public static function tableName()
    {
         return '{{%storeCategory}}';
    }
    
    
	public function init()
	{
		
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
        ];     
    
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
             'id' => Yii::t('app','ID'),
            'firstName' => Yii::t('app','First Name'),
            'lastName' => Yii::t('app','Last Name'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
            'store_id' => Yii::t('app','Store Id'),
            'category_id' => Yii::t('app','Category Id'),
            'status' => Yii::t('app','Status'),
        ];
    }

	public function getCategories()
	{
		return $this->hasOne(Categories::className(), ['id' => 'category_id']);
	}	
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			
            if ($this->isNewRecord) {
               $this->created_at=time();
            }
			   $this->updated_at=time();
				
            return true;
        }
        return false;
    }
    
}
