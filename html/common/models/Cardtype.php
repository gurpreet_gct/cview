<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_cardtype".
 *
 * @property integer $id
 * @property string $name
 * @property string $icon
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Cardtype extends \yii\db\ActiveRecord
{
    
     /**
     * @inheritdoc
     */
     
    public  $imageFile;
    
    public static function tableName()
    {
        return '{{%cardtype}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			 [['status', 'name'], 'required'],
			 ['imageFile', 'required', 'on' => 'create'],   
			[['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['icon'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' =>  Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
            'icon' => Yii::t('app','Icon'),
            'status' =>Yii::t('app','Status') ,
            'created_at' =>Yii::t('app','Created At') ,
            'updated_at' =>Yii::t('app','Updated At') ,
        ];
    }
    
      public function getStatustype()
    {
    	  if($this->status==1){
         return 'Enable';    	  
    	  }else {
    	  return 'Disable';
       }
    }
    
    public function fields()
	{
		$fields=parent::fields();
		unset($fields['created_at']);
		unset($fields['updated_at']);
		unset($fields['status']);
		return $fields;
	}
    
    
    	public function afterFind()
        {
            parent::afterFind();
            
           
            /* For back image */
           if(($this->icon!="")&&(!strstr($this->icon,'http')))
            {
                
                    $this->icon =Yii::$app->urlManagerBackEnd->createAbsoluteUrl("uploads/icons/".$this->icon); 
                    
            }
            
           return true;
			  
				
        }
     	public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	
			    	$this->updated_at= time();
			    	if($insert){			    		
			    		
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}
      public function getMyids()
        {
              return $this->hasMany(Id::className(), ['cardType' => 'id']);
        }
    	
}
