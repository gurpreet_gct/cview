<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_partner_fees".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $trans_rule_id
 * @property integer $charge_type
 * @property string $amount
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class PartnerFees extends \yii\db\ActiveRecord
{

		
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_partner_fees';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['general_1','general_2','general_3','general_4','general_5','general_6','general_7','partner_id','trans_id_1','charge_type_1','trans_id_2','charge_type_2','trans_id_3','charge_type_3','trans_id_4','charge_type_4','trans_id_5','charge_type_5','charge_type_6','trans_id_7','charge_type_7','trans_id_6'],'number'],
			[['amount_1','amount_2','amount_3','amount_4','amount_5','amount_6','amount_7'],'number','min'=>0.01],  
			/*
			 [['trans_id_1','trans_id_2','trans_id_3','trans_id_4','trans_id_5','trans_id_6','trans_id_7'], 'validaterule','skipOnEmpty' => false],*/        
            [['charge_type_1','amount_1','paid_day_1'],'required','when' => function ($model) {
				return $model->trans_id_1 ==1;
			}, 'whenClient' => "function (attribute, value) {
				return $('#PartnerFees-trans_id_1 option').filter(':selected').val()==1;
			}"],
			
			[['charge_type_2','amount_2','paid_day_2'], 'required', 'when' => function ($model) {
				return $model->trans_id_2 ==1;
			}, 'whenClient' => "function (attribute, value) {
				return $('#PartnerFees-trans_id_2 option').filter(':selected').val()==1;
			}"],
			[['charge_type_3','amount_3','paid_day_3'], 'required', 'when' => function ($model) {				
				return $model->trans_id_3 ==1;
			}, 'whenClient' => "function (attribute, value) {
				return $('#PartnerFees-trans_id_3 option').filter(':selected').val()==1;
			}"],
			[['charge_type_4','amount_4','paid_day_4'], 'required', 'when' => function ($model) {
				return $model->trans_id_4 ==1;
			}, 'whenClient' => "function (attribute, value) {
				return $('#PartnerFees-trans_id_4 option').filter(':selected').val()==1;
			}"],
			[['charge_type_5','amount_5','paid_day_5'], 'required', 'when' => function ($model) {
				return $model->trans_id_5 ==1;
			}, 'whenClient' => "function (attribute, value) {
				return $('#PartnerFees-trans_id_5 option').filter(':selected').val()==1;
			}"],
			[['charge_type_6','amount_6','paid_day_6'], 'required', 'when' => function ($model) {
				return $model->trans_id_6 ==1;
			}, 'whenClient' => "function (attribute, value) {
				return $('#partnerfees-trans_id_6 option').filter(':selected').val()==1;
			}"],
			[['charge_type_7','amount_7','paid_day_7'], 'required', 'when' => function ($model) {
				return $model->trans_id_7 ==1;
			}, 'whenClient' => "function (attribute, value) {
				return $('#PartnerFees-trans_id_7 option').filter(':selected').val()==1;
			}"],
			
           
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'amount_1' => 'Amount',
            'amount_2' => 'Amount',
            'amount_3' => 'Amount',
            'amount_4' => 'Amount',
            'amount_5' => 'Amount',
            'amount_6' => 'Amount',
            'amount_7' => 'Amount',
            'charge_type_1' => 'Charge type',
            'charge_type_2' => 'Charge type',
            'charge_type_3' => 'Charge type',
            'charge_type_4' => 'Charge type',
            'charge_type_5' => 'Charge type',
            'charge_type_6' => 'Charge type',
            'charge_type_7' => 'Charge type',
            'trans_id_1' => 'Trans Rule',
            'trans_id_2' => 'Trans Rule',
            'trans_id_3' => 'Trans Rule',
            'trans_id_4' => 'Trans Rule',
            'trans_id_5' => 'Trans Rule',
            'trans_id_6' => 'Trans Rule',
            'trans_id_7' => 'Trans Rule',            
            'paid_day_1' => 'Paid within',
            'paid_day_2' => 'Paid within',
            'paid_day_3' => 'Paid within',
            'paid_day_4' => 'Paid within',
            'paid_day_5' => 'Paid within',
            'paid_day_6' => 'Paid within',
            'paid_day_7' => 'Paid within',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'general_1' => 'General rule',
            'general_2' => 'General rule',
            'general_3' => 'General rule',
            'general_4' => 'General rule',
            'general_5' => 'General rule',
            'general_6' => 'General rule',
            'general_7' => 'General rule',
        ];
    }
    
   	public function beforeSave($insert)
	{
		
		if (parent::beforeSave($insert)) {
				$this->updated_at= time();
				if ($this->isNewRecord) {			
					$this->created_at = time();
					$this->created_by= Yii::$app->user->id;
				}
			if($this->trans_id_1!=1){
				$this->trans_id_1='';
				$this->general_1='';
				$this->amount_1='';
				$this->paid_day_1='';
				$this->charge_type_1='';
			}
			if($this->trans_id_2!=1){
				$this->trans_id_2='';
				$this->general_2='';
				$this->amount_2='';
				$this->paid_day_2='';
				$this->charge_type_2='';
			}
			if($this->trans_id_3!=1){
				$this->trans_id_3='';
				$this->general_3='';
				$this->amount_3='';
				$this->paid_day_3='';
				$this->charge_type_3='';
			}
			if($this->trans_id_4!=1){
				$this->trans_id_4='';
				$this->general_4='';
				$this->amount_4='';
				$this->paid_day_4='';
				$this->charge_type_4='';
			}
			if($this->trans_id_5!=1){
				$this->trans_id_5='';
				$this->general_5='';
				$this->amount_5='';
				$this->paid_day_5='';
				$this->charge_type_5='';
			}
			if($this->trans_id_6!=1){
				$this->trans_id_6='';
				$this->general_6='';
				$this->amount_6='';
				$this->paid_day_6='';
				$this->charge_type_6='';
			}
			if($this->trans_id_7!=1){
				$this->trans_id_7='';
				$this->amount_7='';
				$this->general_7='';
				$this->paid_day_7='';
				$this->charge_type_7='';
			}
			return true;
		} else {
			return false;
		}
	}
	public function validaterule($attribute, $params)
	{
	   $status = false;
	   if ($this->trans_id_1==1 || $this->trans_id_2==1 || $this->trans_id_3==1 || $this->trans_id_4==1 || $this->trans_id_5==1 || $this->trans_id_6==1 || $this->trans_id_7==1) {	
		   $status = true;	  
			  
		}
		if($status == false){
			 $this->addError($attribute, 'Please select atleast 1 transaction rule');
		}
	}
}
