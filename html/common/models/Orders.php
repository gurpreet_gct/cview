<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\User;
use common\models\OrderItem;
use common\models\PaymentMethod;
use common\models\Workorders;
use backend\models\OrderStatus;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "tbl_orders".
 *	
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders}}';
    }
    
 
    public function rules()
    {
        return [
            [['user_id'], 'string'],
          [['coupon_code', 'coupon_code'], 'required'],
          	];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
           'id' => Yii::t('app','#Order ID'),
        	'user_id' => Yii::t('app','User Name'),
        	'created_at' => Yii::t('app','Date'),
        	'grand_total' => Yii::t('app','Grand Total'),
        	'status' => Yii::t('app','Status'),
        	'shipping_description' => Yii::t('app','Shipping_Description'),
        	'shipper_id' => Yii::t('app','Shipper_Id'),
        	'is_offline' => Yii::t('app','Is_Offline'),
        	'discount_amount' => Yii::t('app','Discount_Amount'),
        	'grand_total' => Yii::t('app','Grand_Total'),
        	'shipping_amount' => Yii::t('app','Shipping_Amount'),
        	'shipping_tax_amount' => Yii::t('app','Shipping_Tax_Amount'),
        	'subtotal' => Yii::t('app','Subtotal'),
        	'tax_amount' => Yii::t('app','Tax_Amount'),
        	'total_offline_refunded' => Yii::t('app','Total_Offline_Refunded'),
        	'total_online_refunded' => Yii::t('app','Total_Online_Refunded'),
        	'total_paid' => Yii::t('app','Total_Paid'),
        	'qty_ordered' => Yii::t('app','Quantity_Ordered'),
        	'total_refunded' => Yii::t('app','Total_Refunded'),
        	'can_ship_partially' => Yii::t('app','Can_Ship_Partially'),
        	'customer_note_notify' => Yii::t('app','Customer_Note_Notify'),
        	'billing_address_id' => Yii::t('app','Billing_Address_Id'),
        	'billing_address' => Yii::t('app','Billing_Address'),
        	'shipping_address_id' => Yii::t('app','Shipping_Address_Id'),
        	'shipping_address' => Yii::t('app','Shipping_Address'),
        	'forced_shipment_with_invoice' => Yii::t('app','Forced_Shipment_With_Invoice'),
        	'weight' => Yii::t('app','Weight'),
        	'email_sent' => Yii::t('app','Email_Sent'),
        	'edit_increment' => Yii::t('app','Edit_Increment'),
        	'created_at' => Yii::t('app','created at'),
			'updated_at' => Yii::t('app','updated at'),
			'created_by' => Yii::t('app','created by'),
			'updated_by' => Yii::t('app','updated by'),
        ];
    }
    
    
    
    /* get user status type */
   /* public function getStatustype()
    {
    	  if($this->status==1){
         return 'Enable';    	  
    	  }else {
    	  return 'Disable';
       }
    }*/
    
    
    /* get created user name from User model */
    /*public function getCreateduser()
    {
    	  return $this->hasOne(User::className(), ['id' => 'created_by']);
       
    }*/
     public function getCustomername(){
      $newvalue =  User::findOne(['id' => $this->user_id]);
		return $newvalue;
      }
      public function getPaymentstatus(){
      $payment =  PaymentMethod::findOne(['order_id' => $this->id]);
		return $payment;
      }
      public function getOrderStatus(){
      $status =  OrderStatus::findOne(['id' => $this->status]);
		return $status;
        
    }
    
    /* get the updated name form the User model */
     /*public function getUpdateduser()
    {
    	  return $this->hasOne(User::className(), ['id' => 'updated_by']);
       
    }*/
        
 	/*public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
			    	if($insert){			    		
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}*/
			
			
	// used to generate CAMPAIGNS REDEMPTION
	
	public function campaignRedemption($start,$end,$filterType,$userField){
				
		$query = Yii::$app->getDb();
		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND u.id='.$userId;
			
		}else{
			
			$custQuery		= '';
			
		}
		
			
		if($filterType=='D'){
			
			$group = 'HOUR(FROM_UNIXTIME(t.created_at))';
			
			$specifier = '%b %d,%Y %h:%i:%s';
			
		}elseif($filterType=='W' || $filterType=='M'){
			
			$group = 'DAY(FROM_UNIXTIME(t.created_at))';
			$specifier = '%b %d,%Y';
			
		}elseif($filterType=='Y'){
			
			$group 		= 'MONTH(FROM_UNIXTIME(t.created_at))';
			$specifier 	= '%b,%Y';
			
		}elseif($filterType=='O'){
			
			$group = 'DAY(FROM_UNIXTIME(t.created_at))';
			
			$specifier = '%b %d,%Y %h:%i:%s';
			
		}
		
		$command 	= $query->createCommand("SELECT SUM(if(t.is_offline=0,OI.deal_count,0)) AS inApp, SUM(if(t.is_offline=1,OI.deal_count,0)) AS inStore,FROM_UNIXTIME(t.created_at,'".$specifier."') AS datetime FROM {{%orders}} t JOIN {{%order_item}} OI ON OI.order_id=t.id JOIN {{%workorders}} w ON w.id=OI.deal_id JOIN {{%users}} u ON u.id=w.workorderpartner WHERE (t.created_at BETWEEN $start AND $end) $custQuery GROUP BY $group ORDER BY t.created_at ASC");
		
		$result 	= $command->queryAll();
		
		return 	$result;
		
	}	
	
	// used to generate PAYMENT VIA
	
	public function payViaTransc($unixDateStart,$unixDateEnd,$filterType,$userField){
		
		$query = Yii::$app->getDb();
		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND toi.store_id='.$userId;
			
		}else{
			
			$custQuery		= '';
			
		}
		
		
		if($filterType=='D'){
			
			$group = 'HOUR(FROM_UNIXTIME(tos.created_at))';
			
			$specifier = '%b %d,%Y %h:%i:%s';
			
		}elseif($filterType=='W' || $filterType=='M'){
			
			$group = 'DAY(FROM_UNIXTIME(tos.created_at))';
			$specifier = '%b %d,%Y';
			
		}elseif($filterType=='Y'){
			
			$group 		= 'MONTH(FROM_UNIXTIME(tos.created_at))';
			$specifier 	= '%b,%Y';
			
		}elseif($filterType=='O'){
			
			$group = 'DAY(FROM_UNIXTIME(tos.created_at))';
			
			$specifier = '%b %d,%Y %h:%i:%s';
			
		}
		
		
		$command 	= $query->createCommand("select count(if(tos.grand_total-tos.discount_amount>tos.total_paid,1,null)) as loyaltyandpay, count(if(tos.grand_total-tos.discount_amount=tos.total_paid,1,null)) as onlypay,count(if(tos.total_paid=0,1,null)) as loyalty,FROM_UNIXTIME(tos.created_at,'".$specifier."') AS datetime from {{%orders}} tos LEFT JOIN {{%order_item}} toi on toi.order_id=tos.id LEFT JOIN {{%users}} t on t.id=toi.store_id WHERE tos.is_offline=0 AND (tos.created_at BETWEEN $unixDateStart AND $unixDateEnd) $custQuery GROUP BY $group ORDER BY tos.created_at ASC");
		
		$result 	= $command->queryAll();
		
		return 	$result;
		
	}	
	
	// used to generate CART ABANDONMENT
	
	public function cartAbdonment($unixDateStart,$unixDateEnd,$filterType,$userField){
		
		$query 		= Yii::$app->getDb();
		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND wo.workorderpartner='.$userId;
			
		}else{
			
			$custQuery		= '';
			
		}
		
		if($filterType=='D'){
			
			$group = 'HOUR(FROM_UNIXTIME(ca.created_at))';
			
			$specifier = '%b %d,%Y %h:%i:%s';
			
		}elseif($filterType=='W' || $filterType=='M'){
			
			$group = 'DAY(FROM_UNIXTIME(ca.created_at))';
			$specifier = '%b %d,%Y';
			
		}elseif($filterType=='Y'){
			
			$group 		= 'MONTH(FROM_UNIXTIME(ca.created_at))';
			$specifier 	= '%b,%Y';
			
		}elseif($filterType=='O'){
			
			$group = 'DAY(FROM_UNIXTIME(ca.created_at))';
			
			$specifier = '%b %d,%Y %h:%i:%s';
			
		}
		
		$command 	= $query->createCommand("SELECT count(ca.session_key) AS cart_abdonment,FROM_UNIXTIME(ca.created_at,'".$specifier."') AS datetime FROM {{%cart}} ca LEFT JOIN ".Workorders::tableName()." wo on wo.id=ca.deal_id LEFT JOIN {{%users}} t on t.id=wo.workorderpartner WHERE (ca.created_at BETWEEN $unixDateStart AND $unixDateEnd) $custQuery GROUP BY $group ORDER BY ca.created_at ASC");
		
		$result 	= $command->queryAll();
		
		return 	$result;
		
	}
	public function getOrderitemp(){	  
	   return $this->hasOne(OrderItem::className(), ['order_id' => 'id']);
	 }
	
    
}
