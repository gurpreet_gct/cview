<?php

namespace common\models;

use Yii;

use yii\db\ActiveRecord;
use common\models\User;
use common\models\Workorderpopup;
use common\models\Workorders;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "tbl_product".
 *
 * @property integer $id
 * @property string $name
 * @property string $sku
 * @property string $image
 * @property text $description
 * @property string $urlkey
 * @property integer $price
 * @property integer $is_online
 * @property integer $store_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
 
 class Product extends ActiveRecord implements CartPositionInterface
{
    use CartPositionTrait;
	public $workorder=0;
	public $category;
	public $pimage;
	public $thumbimage;
	public $oprice;
	public $title;
	public $is_included;
	 public function getPrice()
    {
        return $this->price;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sku', 'description', 'urlkey', 'price','status','is_online','store_id'], 'required'],
            [['pimage','p_type'],'required', 'on' => 'create'],             
            [['sku','urlkey'],'unique'],             
            //[['pimage'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
			[['is_included','has_option','pimage','qty','related_product','is_added','category'],'safe'],
			['title', 'required', 'when' => function ($model) {
				return '';
			}, 'whenClient' => "function (attribute, value) {
				var optval = $('#product-has_option:checked').val();
				if (optval ==1) {  return true; }
						   else {
				return false; }
			}"],
			['oprice', 'required', 'when' => function($model) {
				return '';
			}, 'whenClient' => "function (attribute, value) {
				var wt = $('#product-has_option:checked').val();
				if (wt ==1) {  return true; }
                   else {
                   return false; }                                     
			}"]
        ];
 

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Product Name'),
            'sku' => Yii::t('app','SKU'),
            'image' => Yii::t('app','Product Image'),
            'pimage' => Yii::t('app','Product Image'),
            'description' => Yii::t('app','Description'),
            'urlkey' => Yii::t('app',' URL Key'),
            'price' => Yii::t('app','Price'),
            'is_online' => Yii::t('app','Is online'),
            'store_id' => Yii::t('app','Partner'),
            'status' => Yii::t('app','Status'),
            'p_type' => Yii::t('app','Product Type'),
            'has_option' => Yii::t('app','Add Ingredient/Options'),
            'oprice' => Yii::t('app','Price'),
            'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
            'created_by'=>Yii::t('app','Created_By'),
            'updated_by'=>Yii::t('app','Updated_By'), 
            
            
        ];
    }
    
	public function fields()
	{
		$fields=parent::fields();
		unset($fields['store_id']);
		unset($fields['status']);
		unset($fields['created_at']);
		unset($fields['updated_at']);
		unset($fields['created_by']);
		unset($fields['updated_by']);
		
		return $fields;
	}
	
	
	
	
	public function getworkorderpopup(){	
		$id=$this->workorder;

	   return $this->hasOne(Workorderpopup::className(), ['product_id' => 'id'])
	   
	  ->joinWith(['workorders'=> function ($q)  {            

        $q->where(sprintf('dateFrom <= %s and dateTo > %s',time(date("Y-m-d")),time(date("Y-m-d"))+86400));}     

        ],false,'INNER JOIN');		 
	
	
	}

	
	
	
	public function getOnlinestatus(){
	
		if($this->is_online==1){
			return 'Yes';
		}else {
			return 'No';
		}	
	
	}
	
	public function getStatustype(){
	
		if($this->status==1){
			return 'Enable';
		}else {
			return 'Disable';
		}	
	
	}
	public function getPtype(){
		if($this->p_type==3){
			return 'FoodOrder-Group';			
		}else if($this->p_type==2){
			return 'FoodOrder-Simple';
		}
		else{
			return 'Simple';
		}
	}
	public function getPriceval() {
	
	if(isset($this->price)){
	
	return '$'.$this->price;	
	}	
	
	} 
	
	
    public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
					if($this->related_product && is_array($this->related_product)){				
				$this->related_product = implode(',',$this->related_product);	
			}	
			    	if($insert){			    		
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}
	
	public function afterFind()
    {
        parent::afterFind();            
                   
        {
			
            if($this->image!="")
            $this->image=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('uploads/catalog/'.$this->image); 
            $lastImage=explode("/",$this->image	);
            $image=end($lastImage);
            //print_r($image);die;
            $this->thumbimage=Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('uploads/catalog/thumb/'.$image);
          if($this->category){
			   $this->category = explode(',',$this->category);
		   }  
				
        }
            
        return true;
    }
    
    public function extraFields()
	{
		return [
		'workorderpopup',		

		];
	}
	public function getPartnernameval(){
		
		if(!empty($this->store_id)){
		
			$partnerName =  User::findOne(['id' =>$this->store_id]);
			if($partnerName){
				return $partnerName->orgName; 
				
				}
			else{
				return 'N/A';
			}
 		
		}
	}		
			
}
