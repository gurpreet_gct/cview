<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * ChangePassword
 */
class ChangePassword extends Model
{
	public $id=0;
    public $newPassword;
    public $password;
	public $auth_key="";
    private $_user = false;    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['newPassword', 'password'], 'required'],
            // rememberMe must be a boolean value
         
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
    
    
    
     public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First_Name'),
			'lastName' => Yii::t('app','Last_Name'),
			'fullName' => Yii::t('app','Full_Name'),
			'orgName' => Yii::t('app','Org_Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb_Identifier'),
			'role_id' => Yii::t('app','Role_Id'),
			'user_type' => Yii::t('app','User_Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New_Password'),
			'password_reset_token' => Yii::t('app','Password_Reset_Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date_Of_Birth'),
			'ageGroup_id' => Yii::t('app','Age_Group_Id'),
			'walletPinCode' => Yii::t('app','Wallet_Pin_Code'),
			'loyaltyPin' => Yii::t('app','Loyalty_Pin'),
			'auth_key' => Yii::t('app','Auth_Key'),
			'activationKey' => Yii::t('app','Activation_Key'),
			'confirmationKey' => Yii::t('app','Confirmation_Key'),
			'access_token' => Yii::t('app','Access_Token'),
			'hashKey' => Yii::t('app','Hash_Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device_Token'),
			'lastLogin' => Yii::t('app','Last_Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
        ];
    }
    
    
    
    
    

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
	
        if (!$this->hasErrors()) {
            $user = $this->getUser();
			if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Current password is not matched.');
            }
			else if ((!$user || $user->validatePassword($this->password)) && ($this->newPassword==$this->password) ){
			 $this->addError($attribute, 'New password can not be the current password.');
			}
        }
		
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function changePassword()
    {     
        if ($this->validate()) {
            $user=$this->getUser();   
            $user->generateAuthKey();
			$user->setPassword($this->newPassword);
            $user->save();
			$this->auth_key=$user->getAuthKey();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findOne($this->id);
        }

        return $this->_user;
    }
	
	

}
