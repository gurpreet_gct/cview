<?php

namespace common\models;

use Yii;
use common\models\UserPsa;

/**
 * This is the model class for table "tbl_foodorders".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $is_offline
 * @property integer $is_saved
 * @property integer $status
 * @property integer $qty
 * @property string $grand_total
 * @property string $tax_amount
 * @property string $discount_amount
 * @property string $coupon_code
 * @property integer $created_at
 * @property integer $updated_at
 */
class Foodorders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
      		return '{{%foodorders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','store_id','user_id', 'is_offline','status', 'qty', 'created_at', 'updated_at'], 'integer'],
			[['user_id', 'is_offline','status', 'qty',],'required'],
            [['grand_total', 'tax_amount', 'discount_amount'], 'number']           
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'user_id' => Yii::t('app','User'),
            'is_offline' => Yii::t('app','Is Offline'),
            'is_saved' => Yii::t('app','Is Saved'),
            'status' => Yii::t('app','Status'),
            'qty' => Yii::t('app','Qty'),
            'grand_total' => Yii::t('app','Grand Total'),
            'store_id' => Yii::t('app','Store'),
            'tax_amount' => Yii::t('app','Tax Amount'),
            'discount_amount' => Yii::t('app','Discount Amount'),
            'coupon_code' => Yii::t('app','Coupon Code'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
		public function beforeSave($insert)
	{
		
		if (parent::beforeSave($insert)) {
			date_default_timezone_set("UTC");
						
			$this->updated_at= time();
				if ($this->isNewRecord) {			
				$this->created_at = time();
				}
			
			return true;
		} else {
			return false;
		}
	}public function getstatustype()
    {
		if($this->status==1)
		{
			return "enable";
			}
			else{
				return "disable";
				}
		}
	 public function getofflinetype()
	 {
		 if($this->is_offline==1)
		 {
			 return "Yes";
			 }
			 else{
				 return "No";
				 }
		 }
     public function getusername()
     {
	      if($this->user_id)
	      {
		    $model = User::find()->select('firstName,lastName')->where('id='.$this->user_id)->one();
		    if($model){
					  return   $model->firstName.' '.$model->lastName;
					}else{
						return 'not set';
					}
		 }
	 }
	 
	 public function getSname()
     {   
			if($this->store_id)
			{
				$model = User::find()->select('username')->where('id='.$this->store_id)->one();
				  if($model){
					   return   $model->username;
					}else{
						return 'not set';
					}			  
			}
	}
	public function getUserpsa(){    
           return $this->hasOne(UserPsa::className(), ['workorder_id' =>'id']);              
	}
	
}
