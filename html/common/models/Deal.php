<?php

namespace common\models;
use yii;
use yii\base\Model;
use yii\web\UploadedFile;
use common\models\Store;
use common\models\Pass;
use common\models\User;
use backend\models\Dealstore;
use common\models\UserLoyalty;
use common\models\Product;
use common\models\Loyalty;
use common\models\PolygonsRegion;
use yii\helpers\ImageHelper;

/**
 * This is the model class for table "tbl_Deal".
 *
*/

class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */


  public $imageFile1;
  public $paywithpoint=0;
  public $imageFile2;
  public $imageFile3;
  public $dealCount="0";

  public $dealvideoWorkorderName;
  public $dealvideoWorkorderUrl;
  public $dealvideoWorkorderImage;


  public static function tableName()
    {
        return '{{%deal}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['dealuploadTypes'], 'required'],
				['imageFile3', 'dimensionValidationmain'],

             [['offerwas','offernow'], 'string'],
             ['imageFile3', 'required', 'on' => 'create'],
           // [['imageFile3','mainPhotoUpload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
		        [['dealTitle'], 'string', 'max' => 50],            
            [['text'], 'string', 'max' => 120],
            [['frontPhotoUpload','logoUpload'], 'string'],
            //[['dealvideoWorkorderName'], 'file' ,'extensions' => 'mp4'],
            [['dealuploadTypes','dealvideoTypes','dealvideoWorkorderUrl','dealvideoWorkorderImage','dealbrowsercheck','dealvideokey'], 'safe'],
            ['dealvideokey', 'validateVideotype'],
            ['dealvideoWorkorderImage', 'dimensionValidationmainvideo'],
            // [['dealvideoWorkorderImage'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],






            [['imageFile3'], 'required', 'when' => function($model) {
               return '';
           },
           'whenClient' => "function (attribute, value) {

                  var  wtval1 = $('input:radio[name=\"Deal[dealuploadTypes]\"]:checked').val();
                   var imagedealmainPhotoUpload   = $('#imagedealmainPhotoUpload').attr('src');

                 if (wtval1==1 && imagedealmainPhotoUpload=='') {
                   return true; }
                    else if(wtval1 ==1 && imagedealmainPhotoUpload== undefined) {

                    return true;
                   }
                   else {
                   return false; }

               }"

           ],




           [['dealvideoTypes'], 'required', 'when' => function($model) {
               return '';
           },
           'whenClient' => "function (attribute, value) {

                  var  wtval = $('input:radio[name=\"Deal[dealuploadTypes]\"]:checked').val();

                 if (wtval==2) {
                   return true; }
                   else {
                   return false; }

               }"

           ],

            [['dealvideokey'], 'required','message' =>'Deal Video Url cannot be blank','when' => function($model) {
               return '';
           },
           'whenClient' => "function (attribute, value) {


                  var  wtval = $('input:radio[name=\"Deal[dealvideoTypes]\"]:checked').val();
                   var  wtval2 = $('input:radio[name=\"Deal[dealuploadTypes]\"]:checked').val();


                 if (wtval ==2 && wtval2 == 2) {

					 return true;

                   }
                   else {
                   return false;


                   }

               }"

           ],

           [['dealvideokey'], 'url', 'defaultScheme' => 'http','when' => function($model) {
               return '';
           },
           'whenClient' => "function (attribute, value) {


                  var  wtval = $('input:radio[name=\"Deal[dealvideoTypes]\"]:checked').val();
                   var  wtval2 = $('input:radio[name=\"Deal[dealuploadTypes]\"]:checked').val();


                 if (wtval ==2 && wtval2 == 2) {

					 return true;

                   }
                   else {
                   return false;


                   }

               }"

           ],



            [['dealvideoWorkorderName','dealvideoWorkorderUrl'], 'required','when' => function($model) {
               return '';
           },
           'whenClient' => "function (attribute, value) {
                var  wtval = $('input:radio[name=\"Deal[dealvideoTypes]\"]:checked').val();
                   var  wtval2 = $('input:radio[name=\"Deal[dealuploadTypes]\"]:checked').val();
                   var  dealvideourl = $('#deal-dealvideoworkorderurl').val();
                    var  dealvideoname = $('#deal-dealvideoworkordername').val();


                   if (wtval ==1 && wtval2==2) {
                 if (dealvideourl == '' &&  dealvideoname== '') {
					 return true;
                 }else{
					 return false;
                 }
			 }

               }"

           ],


             [['dealbrowsercheck'], 'required', 'message' =>'Open video in browser cannot be blank', 'when' => function($model) {
               return '';
           },
           'whenClient' => "function (attribute, value) {


                  var  WorkUploadTypes = $('input:radio[name=\"Deal[dealuploadTypes]\"]:checked').val();
                  var  workVideoTypes = $('input:radio[name=\"Deal[dealvideoTypes]\"]:checked').val();
                  var  browsercheck = $('input:radio[name=\"Deal[dealbrowsercheck]]\"]:checked').val();


                         if(WorkUploadTypes == 2){
							return true;
						}

						else {
							return false;
						 }


                   }"

           ],



           [['dealvideoWorkorderImage'],  'required','message' =>'Deal Video Image  cannot be blank','when' => function($model) {
               return '';
           },
           'whenClient' => "function (attribute, value) {
                var  dealuploadTypes = $('input:radio[name=\"Deal[dealuploadTypes]\"]:checked').val();
                  var  dealvideoTypes = $('input:radio[name=\"Deal[dealvideoTypes]\"]:checked').val();
                     var dealvideoworkorderimage1   = $('#imagedealvideoImage').attr('src');


                 if ( dealuploadTypes == 2 && dealvideoTypes ==1 && dealvideoworkorderimage1=='' ) {

                   return true; }
                   else if(dealuploadTypes == 2 &&  dealvideoTypes ==1 && dealvideoworkorderimage1== undefined) {

                    return true;
                   }
                   else {
                   return false; }

               }"

           ],


            ];
    }


		/* Deal Image validation */

		 public function dimensionValidationmain($attribute,$param){
			if(is_object($this->imageFile3)){

 				list($width, $height) = getimagesize($this->imageFile3->tempName);

				if($width!=750 || $height!=360){

					$this->addError($attribute,'Main image size should be 750px x 360px dimension');
			    }
			}
		}
		 public function dimensionValidationmainvideo($attribute,$param){
			if(is_object($this->dealvideoWorkorderImage)){

 				list($width, $height) = getimagesize($this->dealvideoWorkorderImage->tempName);

				if($width!=640 || $height!=300){

					$this->addError($attribute,' video image size should be 640px x 300px dimension');
			    }
			}
		}


    public function attributeLabels()
    {
        return [
				'id' =>  Yii::t('app','ID'),
				'workorder_id' => Yii::t('app','Workorder ID'),
				'frontPhotoUpload' => Yii::t('app','Front Photo Upload'),
				'mainPhotoUpload' => Yii::t('app','Main Photo Upload'),
				'additionalPhotoUpload' =>Yii::t('app','Additional Photo Upload') ,
				'logoUpload' =>Yii::t('app','Logo Upload') ,
				'colorOptions' => Yii::t('app','Color Options'),
				'size' => Yii::t('app','Size'),
				'offerwas' => Yii::t('app','Offer was'),
				'offernow' =>Yii::t('app','Offer now') ,
				'dealTitle' => Yii::t('app','Deal Title (50 characters max)'),
				'imageFile1' => Yii::t('app','Offer Front Photo ( Image size should be 640px x 300px dimension )'),
				'imageFile2' => Yii::t('app','Logo Photo ( Logo size should be 132px x 132px dimension )'),
				'imageFile3' =>Yii::t('app','My Deal image (Size: 750pxW x 360pxH Resolution 72DPI)') ,
				'storeIDs' =>Yii::t('app','Select Store') ,
				'text' => Yii::t('app','Text (Note : 120 characters max)'),
				'bulletPoints'=> Yii::t('app','Bullet Points (Up to 5 option max 15 characters each)'),
				'dealuploadTypes' => Yii::t('app','DEAL Image / Video'),
				'dealvideoTypes' =>Yii::t('app','Video Type') ,
				'dealvideoKey' => Yii::t('app','Deal Video Key'),
				'dealvideoImage' => Yii::t('app','Deal Video Image'),
				'dealbrowsercheck' =>Yii::t('app','Deal Crowser Check') ,
				'dealvideoWorkorderName'=>Yii::t('app','Video'),
				'dealvideoWorkorderUrl'=>Yii::t('app','Video url'),
        ];
    }

    public function getStorename(){


	$dealstore =  User::find()->joinWith('dealstore', true, 'INNER JOIN')->select('tbl_deal_store.deal_id ,tbl_deal_store.store_id ,tbl_users.id,tbl_users.orgName')->Where('store_id = tbl_users.id' )->andWhere('deal_id ='.$this->id)->all();



	$storename = array();
	foreach ($dealstore as $storeName) {


		$storename[] =  $storeName->orgName;


		}

		$comma_separated = implode(", ", $storename);
		return ($comma_separated);

	}



    public function getWorkorders()
    {
         return $this->hasOne(Workorders::className(), ['id' => 'workorder_id']);
    }


    public function getWorkorderpopup()
    {
         return $this->hasOne(Workorderpopup::className(), ['workorder_id' => 'workorder_id']);
    }

    public function getworkordercategory()
    {
         return $this->hasMany(WorkorderCategory::className(), ['workorder_id' => 'workorder_id']);
    }

     public function getDealstore()
    {
         return $this->hasMany(Dealstore::className(), ['deal_id' => 'id']);
    }



    public function getDealbookmark()
    {
         return $this->hasOne(DealBookmark::className(), ['deal_id' => 'id'])
         ->onCondition(["user_id"=>Yii::$app->user->id]);
    }



    public function getUserpsa(){
           return $this->hasOne(UserPsa::className(), ['workorder_id' =>'workorder_id']);
	}
   public function getStorebday()
   {

	   $partnerbday = User::findOne(['id' => $this->workorders->workorderpartner]);
			if(isset($partnerbday->bdayOffer)){
				return $partnerbday->bdayOffer;
			}else{
				return '';
			}


   }

   public function getUserbday()
   {

	   $user = User::findOne(['id' => Yii::$app->user->id]);

			if(isset($user->bdayOffer)){
				return $user->bdayOffer;
			}else{
				return '';
			}


   }





    public function getPass()
    {
         return $this->hasOne(Pass::className(), ['workorder_id' => 'workorder_id'])
         ->onCondition(["user_id"=>Yii::$app->user->id]);
    }


     public function bookmarkstatus($id)
    {

		$model = DealBookmark::find()
		->where(['deal_id' => $id])
		->Andwhere(['user_id' => Yii::$app->user->id])
		->one();

           if(isset($model->id)==1){
					return true;
			    }else {
					 return 0;
			  }

    }


	public function getPolygonregion()
    {
         return $this->hasOne(PolygonsRegion::className(),['deal_id' => 'workorder_id']);
    }



   public function afterFind()
    {
        parent::afterFind();

        //if(property_exists(Yii::$app,'urlManagerBackEnd'))
        {
            if($this->frontPhotoUpload!="")
              $this->frontPhotoUpload=Yii::$app->urlManagerBackEnd->createAbsoluteUrl(Yii::$app->params['workorderimage'] .$this->frontPhotoUpload);
			if($this->logoUpload!="")
           $this->logoUpload=Yii::$app->urlManagerBackEnd->createAbsoluteUrl(Yii::$app->params['workorderimage'] .$this->logoUpload);
           if($this->dealuploadTypes==1) {
			if($this->mainPhotoUpload!="")
              $this->mainPhotoUpload=Yii::$app->urlManagerBackEnd->createAbsoluteUrl(Yii::$app->params['workorderimage'] .$this->mainPhotoUpload);
		   }
               if($this->dealvideoTypes==1) {
				$basepath = Yii::$app->basePath;


				   if($this->dealvideokey!=""){
				 $match_value=strrchr($this->dealvideokey,'backend/web/uploads/');
				 if($match_value){
					 $this->dealvideokey = $this->dealvideokey;

				 } else {

				 $this->dealvideokey=Yii::$app->urlManagerBackEnd->createAbsoluteUrl(Yii::$app->params['workorderimage'] .$this->dealvideokey);
			 }

			 }
		     }
		      if($this->dealvideoTypes==1) {
				   if($this->dealvideoImage!="")
				  $this->dealvideoImage=Yii::$app->urlManagerBackEnd->createAbsoluteUrl(Yii::$app->params['workorderimage'] .$this->dealvideoImage);
		     }
			if($this->additionalPhotoUpload!="")
              $this->additionalPhotoUpload=Yii::$app->urlManagerBackEnd->createAbsoluteUrl(Yii::$app->params['workorderimage'] .$this->additionalPhotoUpload);
        }

        return true;
    }


     public function fields()
    {

        $fields=parent::fields();

		$fields['storebdayOffer']= function ($model) {
           return $model->storebday;
        };
        $fields['userbdayOffer']= function ($model) {
           return $model->userbday;
        };


        $fields['store_id']= function ($model) {
            if($model->workorders->workorderpartner!="")
            return $model->workorders->workorderpartner;

        };
        $fields['advertiserName']= function ($model) {
            if($model->workorders->advertiser["orgName"]!="")
            return $model->workorders->advertiser["orgName"];

        };
        $fields['advertiserAddress']= function ($model) {
            if($model->workorders->advaddress["brandAddress"]!="")
            return $model->workorders->advaddress["brandAddress"];

        };
        $fields['workorderType']= function ($model) {
            if($model->workorders['workorderType'] != '') {
				return $model->workorders['workorderType'];
			 }
		};
		$fields['type']= function ($model) {
            if($model->workorders['type'] != '')
               return $model->workorders['type'];
		};
        $fields['is_bookmark']= function ($model) {

            return isset($model->dealbookmark['id'])?true:false;
        };

        $fields['isSelected']= function ($model) {
            return isset($model->workorders->storefavorites['user_id'])?true:false;
        };
        $fields['count_passes']= function ($model) {
               return $model->pass['count_passes'];
		};
        $fields['is_used']= function ($model) {
               return $model->pass['isUsed'];
		};
		$fields['rulecondition']= function ($model) {
               return $model->workorders['rulecondition'];
		};
		$fields['daypartingcustom1']= function ($model) {
               return $model->workorders->daypartingcustom1value;
        };
		$fields['daypartingcustom1end']= function ($model) {
               return $model->workorders->daypartingcustom1endvalue;
		};
		$fields['daypartingcustom2']= function ($model) {
               return $model->workorders['daypartingcustom2'];
        };
		$fields['beaconGroup']= function ($model) {
               return $model->workorders['beaconGroup'];
        };
		$fields['daypartingcustom2end']= function ($model) {
               return $model->workorders['daypartingcustom2end'];
		};
		$fields['daypartingcustom3']= function ($model) {
              return $model->workorders['daypartingcustom3'];

		};
		$fields['daypartingcustom3end']= function ($model) {
               return $model->workorders['daypartingcustom3end'];
		};
		$fields['userenter']= function ($model) {
               return $model->workorders['userenter'];
		};
		$fields['usereturned']= function ($model) {
               return $model->workorders['usereturned'];
		};
		$fields['purchaseHistory1']= function ($model) {
               return $model->workorders['purchaseHistory1'];
		};
		$fields['purchaseHistory2']= function ($model) {
               return $model->workorders['purchaseHistory2'];
		};
		$fields['geoProximity']= function ($model) {
               return $model->workorders['geoProximity'];
		};
		$fields['purchaseHistory3']= function ($model) {
               return $model->workorders['purchaseHistory3'];
		};
		$fields['dealCount']= function ($model) { return 1; };
			$fields['paywithpoint']= function ($model) {
				return  $model->paywithpoint;
		};


        return $fields;
    }

      public function validateVideotype($attribute,$param){

           if(isset($this->dealvideokey)){
			 if($this->dealuploadTypes==2 && $this->dealvideoTypes==2){

                if ((strpos($this->dealvideokey, 'youtube') > 0) ||  (strpos($this->dealvideokey, 'youtu.be') > 0) || (strpos($this->dealvideokey, 'vimeo') > 0)) {

				}else{
					 $this->addError($attribute,'Please insert youtube and vimeo video url');
				}
			}

       }

   }

     public function getVideothumbnail()
    {

		if(!empty($this->dealvideokey)){
			if (strpos($this->dealvideokey, 'youtube') > 0) {
						$video_id = explode("?v=", $this->dealvideokey);
					if(isset($video_id[1])){
							return 'http://img.youtube.com/vi/'."$video_id[1]".'/hqdefault.jpg';
						}

			}
			elseif(strpos($this->dealvideokey, 'youtu.be') > 0) {
						$video_id = explode("be/", $this->dealvideokey);
					if(isset($video_id[1])){
							return 'http://img.youtube.com/vi/'."$video_id[1]".'/hqdefault.jpg';
						}

			}
			elseif(strpos($this->dealvideokey, 'vimeo') > 0) {

						$videiID = (int) substr(parse_url($this->dealvideokey, PHP_URL_PATH), 1);

						$hash = unserialize(file_get_contents('http://vimeo.com/api/v2/video/'.$videiID.'.php'));

						if($hash){
							return $hash[0]['thumbnail_medium'];
						}
				}

			}else{
				return '';
		}

    }


    public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {

					if($this->dealuploadTypes==1){
					if(($this->mainPhotoUpload!="")&&(strstr($this->mainPhotoUpload,'http'))) {
					$mainPhotoUpload = explode("workorder/",$this->mainPhotoUpload);
					$this->mainPhotoUpload = isset($mainPhotoUpload[1])?$mainPhotoUpload[1]:'';
				  }
				 }
				 if($this->dealvideoTypes==1){

					if(($this->dealvideoImage!="")&&(strstr($this->dealvideoImage,'http'))) {
					$dealvideoImage = explode("workorder/",$this->dealvideoImage);
					$this->dealvideoImage = $dealvideoImage[1];
				  }

				 }


			    	return true;
			    } else {

			     return false;
			    }
			}

	public function extraFields()
	{
		return [
		'workorderpopup',//=>['id','passUrl']
		'workorders',

		];
	}



}
