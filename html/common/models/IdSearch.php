<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Id;

/**
 * IdSearch represents the model behind the search form about `common\models\Id`.
 */
class IdSearch extends Id
{
   /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			//[['cardType',  'cardNumber', 'expiry'], 'required'],			
		//	[['front','back'],'required',  'on' => 'create'], 
			[['cardType'],'number', ], 
            [['created_at', 'updated_at'], 'integer', 'on' => 'create'],
            [['nameOnCard'], 'string', 'max' => 200],
            [['cardNumber'], 'string', 'max' => 100],
             [['cardType',  'cardNumber', 'expiry','front','back','nameOnCard',], 'safe'], 
        ];
    }
    
    
    
    public function attributeLabels()
		{
			return [
				'id' => Yii::t('app','ID'),
				'cardType' => Yii::t('app','Card type'),
				'user_id' => Yii::t('app','User_Id'),
				'nameOnCard' => Yii::t('app','Name on Card'),
				'cardNumber' => Yii::t('app','Card Number'), 
				'expiry' =>Yii::t('app','Expiry') ,
				'front' => Yii::t('app','Front Photo'),
				'back' => Yii::t('app','Back Photo'),				
				'created_at' => Yii::t('app','Created At'),
				'updated_at' =>Yii::t('app','Updated At') ,
			];
		}
    
    
    
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Id::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
         if(!empty($this->expiry)){
		       $Expiry = strtotime($this->expiry);
		 }else{
			 $Expiry = $this->expiry;
		 }
        $query->andFilterWhere([
            'id' => $this->id,
            'expiry' => $Expiry,
            'cardNumber' => $this->cardNumber,
            'cardType' => $this->cardType,
            
        ]);

        $query->andFilterWhere(['like', 'nameOnCard', $this->nameOnCard]);
            

        return $dataProvider;
    }
}
