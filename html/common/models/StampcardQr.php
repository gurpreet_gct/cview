<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "tbl_stampcardqr".
 *	
 */
class StampcardQr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stampcardqr}}';
    }
    
 
    public function rules()
    {
        return [            
			[['workorder_id','qr_code','created_at','updated_at','created_by','updated_by'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
   
	public function beforeSave($insert)
	{
	    if (parent::beforeSave($insert)) {
			   	$this->updated_by= Yii::$app->user->id;
			   	$this->updated_at= time();
			   	if($insert){			    		
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			   	}
			    
					return true;
			    } else {
			    	
					return false; 
			    }
		}
		
		
		public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','Id'),
            'workorder_id' => Yii::t('app','Workorder_Id'), 
            'qr_code' => Yii::t('app','Qr_Code'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'), 
            'created_by' => Yii::t('app','Created By'), 
            'updated_by' => Yii::t('app','Updated By'), 
			];
    }
			
	  
   }
   
   
