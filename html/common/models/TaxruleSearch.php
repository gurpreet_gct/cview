<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Taxrule;

/**
 * TaxruleSearch represents the model behind the search form about `common\models\TaxruleSearch`.
 */
class TaxruleSearch extends Taxrule
{
   /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
		
			[['productclass','name'],'safe' ], 
			[['country_id','priority_id','sort_id'],'integer', ], 
            [['created_at', 'updated_at','updated_by','created_by'], 'integer'],
            
            
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
            'productclass' => Yii::t('app','Product_Class'),
            'country_id' => Yii::t('app','Country_Id'),
            'priority_id' => Yii::t('app','Priority_Id'),
            'sort_id' => Yii::t('app','Sort_Id'),
            'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
            'created_by'=>Yii::t('app','Created_By'),
            'updated_by'=>Yii::t('app','Updated_By'),
        ];
    }
    
    
    
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Taxrule::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        
        $query->andFilterWhere([
          
            'priority_id' => $this->priority_id,
            'sort_id' => $this->sort_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'created_by' => $this->created_by,
            'country_id' => $this->country_id,
            
        ]);

        $query->andFilterWhere(['like', 'productclass', $this->productclass]);
        $query->andFilterWhere(['like', 'name', $this->name]);
      
       
            

        return $dataProvider;
    }
}
