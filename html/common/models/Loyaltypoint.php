<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_loyaltypoints".
 *
 */

 /* Method : Method = 1 For loyalty online, Method = 2 For loyalty offline, Method = 3 For loyalty bdayOffer, */

class Loyaltypoint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     public $pin;
     public $foodorder;
     public $workorderid=0;
	 public $foodorder_id;
    public static function tableName()
    {
        return '{{%loyaltypoints}}';
    }


    public function rules()
    {
        return [
			['amount', 'greaterThanZero'],
			 [['foodorder','store_id','pin','transaction_id','amount'], 'required'],
			['transaction_id', 'unique','message'=>'This transaction ID has already accrued loyalty Dollars.'],
          	[['foodorder_id','valueOnDay','order_id','user_id','store_id','method','status','workorderid'], 'safe'],
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'user_id' =>Yii::t('app','User Name'),
			'order_id' =>Yii::t('app','Order ID') ,
			'store_id' =>Yii::t('app','Store ID') ,
			'status' => Yii::t('app','Status'),
			'method' => Yii::t('app','Method'),
			'transaction_id' => Yii::t('app','Transaction Id'),
			'created_at' => Yii::t('app','created_at'),
			'updated_at' => Yii::t('app','updated_at'),
			'created_by' => Yii::t('app','created_by'),
			'updated_by' => Yii::t('app','updated_by'),
			'pointvalue' =>Yii::t('app','Loyalty Dollars Value') ,
			'valueOnDay' => Yii::t('app','value_On_Day'),
			'rValueOnDay' =>Yii::t('app','R_Value_On_Day') ,
			'loyaltypoints' =>Yii::t('app','Loyalty_Dollars') ,
           ];
    }
    public function getCustomername(){
      $newvalue =  User::findOne(['id' => $this->user_id]);
		return $newvalue;
      }

public function greaterThanZero($attribute,$params)
   {

      if ($this->$attribute<=0)
         $this->addError($attribute, 'amount has to be greater than zero');

 }




 	public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
			    	if($insert){
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			     	}

			    	return true;
			    } else {

			     return false;
			    }
			}

	/*
	*@offlineTransaction
	* return data of offline transaction
	*/

	public function offlineTransaction($unixDateStart,$unixDateEnd,$userField){

		//select count(id),store_id from tbl_loyaltypoints where order_id=0 group by store_id;

		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= Loyaltypoint::tableName().'.store_id='.$userId;

		}else{

			$custQuery		= '';

		}

		$query  = new Loyaltypoint();

		$reslt 	= $query->find()
						->select(['count('.Loyaltypoint::tableName().'.id) AS offlineTransac',Loyaltypoint::tableName().'.store_id',Loyaltypoint::tableName().'.created_at as dateCreate','FROM_UNIXTIME('.Loyaltypoint::tableName().'.created_at,"%b %d,%Y %h:%i:%s") AS created_at'])
						->leftjoin(User::tableName(),User::tableName().'.id='.Loyaltypoint::tableName().'.store_id')
						->Where(['between',Loyaltypoint::tableName().'.created_at', $unixDateStart,$unixDateEnd])
						->andWhere([Loyaltypoint::tableName().'.order_id'=>0])
						->andWhere($custQuery)
						->asArray()
						->groupBy(Loyaltypoint::tableName().'.store_id')
						->orderBy([Loyaltypoint::tableName().'.created_at'=>SORT_ASC])
						->all();

		return 	$reslt;

	}





}
