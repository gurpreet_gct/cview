<?php
namespace common\models;
use Yii;
use yii\web\UploadedFile;
use common\models\Product;
use common\models\Workorders;
use common\models\Qr;
use yii\helpers\ImageHelper;
/**
* This is the model class for table "tbl_workorderspopup".
*
*/
class Workorderpopup extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public $imageFile;
  public $imageFilelogo;
  public $varImage;
  public $sspBarcode;
  public $partnerAddress;
  public $wtype_pop;
  public $videoWorkorderName;
  public $videoWorkorderImage;
  public $videoWorkorderUrl;




  public static function tableName()
  {
  return '{{%workorder_popup}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {

  return [
  [['videoName'], 'safe'],
  [['iphoneNotificationText'], 'required','on'=>'stepone'],
  [['offerTitle'], 'required','on'=>'create'],
  ['imageFilelogo', 'dimensionValidationlogo'],
  [['imageFile'], 'required','on'=>'create','when' => function($model) {
  return ($model->uploadTypes==1 && $model->imageFile!='');
  },'whenClient' => "function (attribute, value) {

  var wtval = $('input:radio[name=\"Workorderpopup[uploadTypes]\"]:checked').val();

  if(wtval==1){

  return true;

  }else {

  return false;
  }
  }"],
  ['imageFile', 'dimensionValidation','when'=>function($model){ return $model->uploadTypes==1; }],
  ['videokey', 'validateVideotype'],
  [['videokey'],'safe'],
  ['passUrl', 'unique'],
  [['offerTitle'], 'string', 'max' => 50],
  ['readTerms', 'checkreadterms'],
  [['iphoneNotificationText'], 'string', 'max' => 110],
  [['customOfferSelect1','customOfferSelect2','offertext1','customOfferSelect3','customOfferSelect4'], 'string', 'max' => 110],
  [['product_id','product_id_combo','comboProductType'],'integer'],
  [['sspBarcode','ticketinfo','sspMoreinfo','eventDate','uploadTypes','eventVenue'],'safe'],

  [['videoWorkorderImage'], 'required','on'=>'create','when' => function($model) {
  return '';
  },'whenClient' => "function (attribute, value) {

  var wtval = $('input:radio[name=\"Workorderpopup[videoTypes]\"]:checked').val();
  var wtval2 = $('input:radio[name=\"Workorderpopup[uploadTypes]\"]:checked').val();
  if(wtval==1 && wtval2==2 && wtval2!=1){

  return true;

  }else {

  return false;
  }
  }"],

  ['videoWorkorderImage', 'dimensionValidationvideologo','when'=> function($model) {
  return ($model->uploadTypes==2 && $model->videoTypes==1);
  }],
  [['videoWorkorderName'], 'file' ,'extensions' => 'mp4'],
  ['videoWorkorderUrl', 'checkvideoWorkorderUrl'],
  [['videoTypes','browsercheck'], 'safe'],
  /* required field for event tickets ( Event date ) */

  [['eventDate'], 'required','on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var wtval= $('#workorderpopup-eventDate').val();

  if (wtval) {
  return false; }
  else {
  return true; }

  }"

  ],

  /* required field for event Venue ( Event date ) */
  [['eventVenue'], 'required','on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var workorders_type= $('#workorderpopup-eventvenue').val();

  if(workorders_type==1 || workorders_type==4){
  return true; }
  else {
  return false; }

  }"

  ],

  /* required field for customOfferSelect1 */
  [['customOfferSelect1'], 'required','on'=>'create','when' => function($model) {

  if($model->offertype==3 && $model->customOfferSelect1==''){

  return true;

  }else{

  return false;

  }

  },
  'whenClient' => "function (attribute, value) {

  var customOfferSelect1 = $('#customOfferSelect1').val();

  if(customOfferSelect1=''){
  return true; }
  else {
  return false; }

  }"

  ,'message'=>'Please select atleast one option'],


  /* required field for event tickets ( Event ticket info  ) */
  [['ticketinfo'], 'required', 'on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var wtval= $('#workorderpopup-ticketinfo').val();

  if (wtval) {
  return false; }
  else {
  return true; }

  }"

  ],

  /* required field for event tickets ( offer was and offer now ) */
  [['offerwas','offernow'],'offerwasValidation','skipOnEmpty'=>false]  ,

  [['uploadTypes'], 'required', 'on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var  wtval = $('input:radio[name=\"Workorderpopup[uploadTypes]\"]:checked').val();
  var  workordertype = $('#workorderpopup-wtype_pop').val();

  if (workordertype==1 || workordertype==4) {
  return true; }
  else {
  return false; }

  }"

  ],

  [['browsercheck'], 'required','on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var  workordertype = $('#workorderpopup-wtype_pop').val();
  var  WorkUploadTypes = $('input:radio[name=\"Workorderpopup[uploadTypes]\"]:checked').val();
  var  workVideoTypes = $('input:radio[name=\"Workorderpopup[videoTypes]\"]:checked').val();
  var  browsercheck = $('input:radio[name=\"Workorderpopup[browsercheck]]\"]:checked').val();

  if (workordertype==1 || workordertype==4) {
  if(workVideoTypes == 2){
  return true;
  }
  }
  else {
  return false;
  }


  }"

  ],

  [['videoTypes'], 'required','on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var  wtval = $('input:radio[name=\"Workorderpopup[uploadTypes]\"]:checked').val();

  if (wtval==2) {
  return true; }
  else {
  return false; }

  }"

  ],


  [['comboProductType'], 'required', 'on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var  wtval = $('input:radio[name=\"Workorderpopup[offertext1]\"]:checked').val();

  if (wtval ==5) {

  return true; }
  else {
  return false; }

  }"

  ],

  [['product_id_combo'], 'required','on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var  wtval = $('#workorderpopup-comboproducttype').val();

  if (wtval ==2) {

  return true; }
  else {
  return false; }

  }"

  ],





  [['passUrl'], 'required', 'on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var wt= $('#workorderpopup-passurl').val();

  if (wt !='') {  return false; }
  else {
  return true; }

  }"

  ],

  [['product_id'], 'required','on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var wt= $('#workorders-buynow:checked').val();
   if (wt==1) {  return true; }
	else {
	return false																																																																																		; 
	}

  }"

  ],

  [['offertype'], 'required','on'=>'create','skipOnEmpty' => true,'when' => function($model) {
  if($model->wtype_pop==1 || $model->wtype_pop==4){
  return true;
  }
  return false;

  },
  'whenClient' => "function (attribute, value) {
  var valType = $('#workorderpopup-wtype_pop').val();
  if(valType==1 || valType==4){
  return true;
  }
  return false;
  }"

  ],

  [['videokey'], 'required','on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {


  var  wtval = $('input:radio[name=\"Workorderpopup[videoTypes]\"]:checked').val();
  var  wtval2 = $('input:radio[name=\"Workorderpopup[uploadTypes]\"]:checked').val();
  var  videokey = $('#workorderpopup-videokey').val();

  if (wtval ==2 && wtval2 == 2) {

  return true;

  }
  else {
  return false;


  }

  }"

  ],

  [['videokey'], 'url', 'defaultScheme' => 'http','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {


  var  wtval = $('input:radio[name=\"Workorderpopup[videoTypes]\"]:checked').val();
  var  wtval2 = $('input:radio[name=\"Workorderpopup[uploadTypes]\"]:checked').val();
  var  videokey = $('#workorderpopup-videokey').val();

  if (wtval ==2 && wtval2 == 2) {

  return true;

  }
  else {
  return false;


  }

  }"

  ],






  [['imageFile'], 'required','on' => 'create', 'when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var  wtval = $('input:radio[name=\"Workorderpopup[uploadTypes]\"]:checked').val();
  var  wtval2 = $('input:radio[name=\"Workorderpopup[uploadTypes]\"]:checked').val();

  if (wtval ==1) {

  return true; }
  else {
  return false; }

  }"

  ],



  [['videoWorkorderName','videoWorkorderUrl'], 'required','on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {
  var  wtval = $('input:radio[name=\"Workorderpopup[videoTypes]\"]:checked').val();
  var  wtval2 = $('input:radio[name=\"Workorderpopup[uploadTypes]\"]:checked').val();
  var  videoworkorderurl = $('#workorderpopup-videoworkorderurl').val();
  var  videoworkordername = $('#workorderpopup-videoworkordername').val();


  if (wtval ==1 && wtval2==2) {
  if (videoworkorderurl == '' &&  videoworkordername== '') {
  return true;
  }else{
  return false;
  }
  }

  }"

  ],

  // 'on' => 'create',


  [['imageFile'], 'required','on' => 'create', 'when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var  wtval = $('#workorderpopup-psaoffertype').val();

  if (wtval) {

  return true; }
  else {
  return false; }

  }"

  ],



  [['buynow'], 'required','on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var wt= $('#workorderpopup-buynow').val();

  if (wt !='') {  return false; }
  else {
  return true; }

  }"

  ],




  [['imageFilelogo'], 'required','on' => 'create', 'when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var wt= $('#workorderpopup-imagefilelogo').val();

  if (wt !='') {  return false; }
  else {
  return true; }

  }"

  ],


  [['psaoffertype'], 'required', 'on'=>'create','when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var wt= $('#workorderpopup-psaoffertype').val();

  if (wt !='') {

  return false; }
  else {
  return true; }

  }"

  ],

  [['offerText'], 'string', 'message' => 'PSA (deal) Text should contain at most 100 characters.', 'max' => 100, 'when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var wt= $('#workorderpopup-psaoffertype').val();

  if (wt==2) {

  return true; }
  else {
  return false; }

  }" ],

  [['offerText'], 'string', 'message' => 'PSA (info) Text should contain at most 220 characters.', 'max' => 220, 'when' => function($model) {
  return '';
  },
  'whenClient' => "function (attribute, value) {

  var wt= $('#workorderpopup-psaoffertype').val();


  if (wt==1) {

  return true; }
  else {
  return false; }

  }" ],

  ];
  }


  public function attributeLabels()
  {
  return [
  'id' => Yii::t('app','Id'),
  'workorder_id' => Yii::t('app','Workorder ID'),
  'psaoffertype' => Yii::t('app','PSA Type'),
  'comboProductType' => Yii::t('app','Select Product for combo offer'),
  'uploadTypes' => Yii::t('app','Upload Media Type'),
  'photo' => Yii::t('app','Photo'),
  'videokey' => Yii::t('app','Enter Youtube/Vimeo Video URL'),
  'logo' => Yii::t('app','Logo'),
  'offerTitle' => Yii::t('app','DEAL Title (50 characters max)'),
  'product_id' => Yii::t('app','Select Product'),
  'product_id_combo' => Yii::t('app','Add Product for combo offer'),
  'comboProductType' => Yii::t('app','Combo Product Type'),
  'offerText' => Yii::t('app','Select Offer Type'),
  'offertype' => Yii::t('app','DEAL Text'),
  'offerwas' => Yii::t('app','Offer Was'),
  'offernow' => Yii::t('app','Offer now'),
  'iphoneNotificationText' => Yii::t('app','Notification Text (110 characters max)'),
  'readTerms' => Yii::t('app','Read Terms'),
  'buynow' => Yii::t('app','Buy now'),
  'passUrl' => Yii::t('app','URL Key'),
  'customOfferSelect1' => Yii::t('app','Custom Offer Select1'),
  'customOfferSelect2' => Yii::t('app','Custom Offer Select2'),
  'customOfferSelect3' => Yii::t('app','Custom Offer Select3'),
  'customOfferSelect4' => Yii::t('app','Custom Offer Select4'),
  'offertext1' => Yii::t('app','Offer Text1'),
  'offertype' => Yii::t('app','DEAL Type'),
  'sspMoreinfo' => Yii::t('app','Extra Deal Info'),
  'sspBarcode' => Yii::t('app','Upload Barcode Image'),
  'sspBarcodeImage' => Yii::t('app','SSP Barcode Image'),
  'eventDate' => Yii::t('app','Select Event Date'),
  'eventVenue' => Yii::t('app','Event Venue'),
  'ticketinfo' => Yii::t('app','Ticket Info'),
  'videoTypes' => Yii::t('app','Video Types'),
  'videoName' => Yii::t('app','Video Name'),
  'videoImage' => Yii::t('app','Video Image'),
  'imageFile' => Yii::t('app','Photo ( Image size should be 640px x 300px dimension )'),
  'imageFilelogo' => Yii::t('app','Logo Photo ( Logo size should be 132px x 132px dimension )'),
  'videoWorkorderName' => Yii::t('app','Upload video'),
  'browsercheck' => Yii::t('app','Open Video In Browser'),
  'videoworkorderurl' => Yii::t('app','Video Url'),
  'videoWorkorderImage' => Yii::t('app','Uplaod Video Image ( Video Image size should be 640px x 300px dimension )'),
  ];
  }

  // validate on offer now and offer was
  public function offerwasValidation($attribute, $params)
  {
  $status = false;
  if($this->offertype ==1 && ($this->offerwas==0 || $this->offernow==0)) {
  $status = true;

  }
  if($status == true && $this->offertype ==1 && $this->offerwas==0){
  $this->addError('offerwas', 'Please enter a valid amount.');

  }
  if($status == true && $this->offertype ==1 && $this->offernow==0){
  $this->addError('offernow', 'Please enter a valid amount.');

  }
  }

  public function checkreadterms($attribute, $params)
  {

  $wordcounter = str_word_count($this->readTerms);
  if($wordcounter > 220){
  // no real check at the moment to be sure that the error is triggered
  $this->addError($attribute, 'Read Terms should contain at most 220 words.');
  }
  }


  public function dimensionValidation($attribute){

  if(is_object($this->imageFile) && $this->uploadTypes==1){


  list($width, $height) = getimagesize($this->imageFile->tempName);

  if(!($width==640 && $height==300)){

  // if($width!=640 || $height!=300){

  $this->addError($attribute,'Photo size should be 640px x 300px dimension');

  }

  }

  }

  public function validateVideotype($attribute,$param){


  if(isset($this->videokey)){
  // if($this->uploadTypes==2 && $this->videoTypes==2){

  if($this->uploadTypes==2 && $this->videoTypes==2){

  if ((strpos($this->videokey, 'youtube') > 0) ||  (strpos($this->videokey, 'youtu.be') > 0) || (strpos($this->videokey, 'vimeo') > 0)) {

  }else{
  $this->addError($attribute,'Please insert youtube and vimeo video url');
  }
  }

  }

  }
  /* check local video url */
  public function checkvideoWorkorderUrl($attribute,$param){

  if(isset($this->videoWorkorderUrl)){
  if($this->uploadTypes==2 && $this->videoTypes==1){

  $check_value =  substr($this->videoWorkorderUrl,0,4);
  $check_value1 =  substr($this->videoWorkorderUrl,0,5);

  if ($check_value=='http' || $check_value1=='https') {

  }else{
  $this->addError($attribute,'Please insert validate video url');
  }
  }

  }

  }




  public function getbuynowval() {

  if($this->buynow==1){
  return 'Yes';
  }else{
  return 'No';
  }

  }

  public function getadvname() {
  $getcmpType = workorders::find()->select(['type','workorderpartner'])->where(['id' => $this->workorder_id])->one();
  if(isset($getcmpType->workorderpartner)) {
  $partnerinfo = Profile::find()->select(['companyName','brandAddress'])->where(['user_id' => $getcmpType->workorderpartner])->one();
  /* GET partner info from profile model */
  if(isset($partnerinfo->companyName)){
  $partnerName = $partnerinfo->companyName;
  }else{
  $partnerName = 'Not Selected';
  }
  return $partnerName;

  }
  }

  public function getadvaddress() {

  $getcmpType = workorders::find()->select(['type','workorderpartner'])->where(['id' => $this->workorder_id])->one();
  if(isset($getcmpType->workorderpartner)) {
  $partnerinfo = Profile::find()->select(['companyName','brandAddress'])->where(['user_id' => $getcmpType->workorderpartner])->one();
  /* GET partner info from profile model */
  if(isset($partnerinfo->brandAddress)){
  $brandAddress = $partnerinfo->brandAddress;
  }else{
  $brandAddress = 'Not Selected';
  }
  return $brandAddress;

  }

  }
  public function getbrandnameval() {

  $getcmpType = workorders::find()->select(['type','workorderpartner'])->where(['id' => $this->workorder_id])->one();
  if(isset($getcmpType->workorderpartner)) {
  $partnerinfo = Profile::find()->select(['companyName','brandName'])->where(['user_id' => $getcmpType->workorderpartner])->one();
  /* GET partner info from profile model */
  if(isset($partnerinfo->brandName)){
  $brandName = $partnerinfo->brandName;
  }else{
  $brandName = 'Not Selected';
  }
  return $brandName;

  }

  }


  public function dimensionValidationlogo($attribute,$param){


  if(is_object($this->imageFilelogo)){

  list($width, $height) = getimagesize($this->imageFilelogo->tempName);

  if($width!=132 || $height!=132){

  $this->addError($attribute,'Logo size should be 132px x 132px dimension');

  }

  }
  }

  public function dimensionValidationvideologo($attribute,$param){

  if(is_object($this->videoWorkorderImage) && $this->uploadTypes==2){

  list($width, $height) = getimagesize($this->videoWorkorderImage->tempName);

  if($width!=640 || $height!=300){

  $this->addError($attribute,'Video Image size should be 640px x 300px dimension');

  }

  }
  }
  public function getPsatype(){

  if($this->psaoffertype!=''){

  if($this->psaoffertype==1)    {

  return 'PSA (info)';

  }elseif($this->psaoffertype==2){

  return 'PSA (deal)';

  }

  else{
  return false;
  }

  }

  }

  /*get partner for workorder*/
  public function getProductname(){

  if($this->product_id){
  $model = Product::find()->select('name')->where(['id' => $this->product_id])->one();
  return $model->name;
  }else{
  return 'Product not select';

  }

  }

  public function getWorkorders(){
  return $this->hasOne(Workorders::className(), ['id' =>'workorder_id']);
  }
  public function getUserpsa(){
  return $this->hasOne(UserPsa::className(), ['workorder_id' =>'workorder_id']);
  }



  public function getProduct(){
  return $this->hasMany(Product::className(), ['id' =>'product_id']);
  }


  /**
  * @inheritdoc
  */


  public function getworkordercategory(){

  return $this->hasMany(WorkorderCategory::className(), ['workorder_id' => 'workorder_id']);

  }

  public function getOffertrack(){

  $user_id=Yii::$app->user->id;
  $offertarck = Offertrack::find()->where(['workorder_id' => $this->workorder_id, 'user_id' => $user_id])->orderBy('id desc')->one();
  if(isset($offertarck->usedNo)){
  $usedNo = $offertarck->usedNo;
  }else{
  $usedNo = 0;
  }

  return $usedNo;

  }

  public function afterFind()
  {
  parent::afterFind();
  {
  if(($this->eventDate !='') && (!strstr($this->eventDate,"-")))
  {
  $this->eventDate=date("Y-m-d",$this->eventDate);
  }
  $img = isset(Yii::$app->params['workorderimage']) ? Yii::$app->params['workorderimage'] : '';

  if($this->photo!=""){
  $this->photo=Yii::$app->urlManagerBackEnd->createAbsoluteUrl($img .$this->photo
  );

  }
  if($this->logo!=""){
  $this->logo=Yii::$app->urlManagerBackEnd->createAbsoluteUrl($img.$this->logo);
  }

  if($this->videoImage!=""){

  $data = explode(':',$this->videoImage);
  //	print_r($this->videoImage);die();

  if(isset($data[0]) && $data[0]!='http'){
  $this->videoImage=Yii::$app->urlManagerBackEnd->createAbsoluteUrl($img.$this->videoImage);
  }



  }
  if($this->videoName!=""){
  $this->videoName=Yii::$app->urlManagerBackEnd->createAbsoluteUrl($img .'/'.$this->videoName);
  }
  if($this->passUrl)
  {
  $passtype = QR::findOne(['qrkey' => $this->passUrl ]);
  if($passtype){
  if($passtype->type==1){
  $this->passUrl=Yii::$app->urlManagerPassKit->createAbsoluteUrl($this->passUrl);
  }elseif($passtype->type==0){
  $this->passUrl=Yii::$app->urlManagerQrKit->createAbsoluteUrl($this->passUrl);
  }
  }
  }


  return true;
  }

  }

  /* total loyalty points */
  public function getDealpricepoint()
  {

  if(isset($_GET['id'])){
  $store_id =  $_GET['id'];
  $product_id = $this->product_id;
  $productQty = $this->customOfferSelect1;
  if($productQty==''){
  $productPrice = $this->offernow;
  }else{

  $proudctdata = Product::find()->select('price')->Where(['id' => $product_id])->one();
  if($proudctdata){
  $productPrice = $proudctdata->price*$productQty;
  }else{
  $productPrice = '';
  }
  }
  if($productPrice){
  $pointValue=0;
  $userLoyalty=UserLoyalty::find()
  ->select(["store_id","value","points"])
  ->where(["user_id"=>Yii::$app->user->id])
  ->andWhere("store_id in (". $store_id .")")
  ->andWhere("points !=0")
  ->one();
  if($userLoyalty)
  {
  $pointValue=$userLoyalty->points/$userLoyalty->value;
  }
  if($pointValue <=0)
  {
  $obl = Loyalty::find()->where(['user_id' =>$store_id])->orderBy('id desc')->one();

  $pointValue=$obl?$obl->redeem_loyalty/$obl->redeem_dolor:0;
  }


  if($pointValue){
  return $productPrice*$pointValue;
  }else{
  return 0;
  }
  }

  }

  }

  /* convert deal price to points */
  public function getTotalloyaltypoint()
  {
  if(isset($_GET['id'])){
  $store_id =  $_GET['id'];
  $userLoyalty = UserLoyalty::findOne(['store_id' => $store_id, 'user_id' => Yii::$app->user->id ]);
  if($userLoyalty){
  return $userLoyalty->points;
  }else{
  return '0';
  }
  }

  }

  public function getVideothumbnail()
  {

  if(!empty($this->videokey)){
  if (strpos($this->videokey, 'youtube') > 0) {
  $video_id = explode("?v=", $this->videokey);
  if(isset($video_id[1])){
  return 'http://img.youtube.com/vi/'."$video_id[1]".'/hqdefault.jpg';
  }

  }
  elseif(strpos($this->videokey, 'youtu.be') > 0) {
  $video_id = explode("be/", $this->videokey);
  if(isset($video_id[1])){
  return 'http://img.youtube.com/vi/'."$video_id[1]".'/hqdefault.jpg';
  }

  }
  elseif(strpos($this->videokey, 'vimeo') > 0) {

  $videiID = (int) substr(parse_url($this->videokey, PHP_URL_PATH), 1);

  $hash = unserialize(file_get_contents('http://vimeo.com/api/v2/video/'.$videiID.'.php'));

  if($hash){
  return $hash[0]['thumbnail_medium'];
  }
  }

  }else{
  return '';
  }

  }


  public function beforeSave($insert)
  {
  if(($this->eventDate !='') && (strstr($this->eventDate,'-'))){

  $this->eventDate = strtotime($this->eventDate);

  }

  if (parent::beforeSave($insert)) {

  if(($this->photo!="")) {
  $photo = explode("workorder/",$this->photo);
  $this->photo = isset($photo[1])? $photo[1] :  $this->photo;

  }

  if(($this->logo!="")&&(strstr($this->logo,'http')) || (strstr($this->logo,'https'))) {
  $logo = explode("workorder/",$this->logo);
  $this->logo = isset($logo[1]) ? $logo[1]: $this->logo;
  }
  if(($this->videoImage!="")&&(strstr($this->videoImage,'http')) ||(strstr($this->videoImage,'https'))) {
  $videoImage = explode("/",$this->videoWorkorderImage);
  $this->videoImage = end($videoImage);

  }


  if(($this->videoName!="")&&(strstr($this->videoName,'http')) || (strstr($this->videoName,'https'))) {
  $videoName = explode("workorder//",$this->videoName);
  $this->videoName = isset($videoName[1]) ? $videoName[1] : $this->videoName;
  }
  return true;

  } else {

  return false;

  }

  }

  public function fields(){

  $fields	=	parent::fields();

  //$fields=["user_id","store_id","workorder_id","id","count_passes",

  $fields["expire date"]=function($model){

  return $model->workorders->dateTo;

  };

  $fields["Stampcardused"]=function($model){
  return $model->offertrack;

  };

  $fields["videoThumb"]=function($model){
  return $model->videothumbnail;

  };

  $fields["count"]=function($model){
  return $this->customOfferSelect1;

  };

  $fields["freecount"]=function($model){
  return $this->customOfferSelect2;

  };
  $fields["total"]=function($model){
	  $customOfferSelect1 = $customOfferSelect2 = 0;
	  if(is_numeric($this->customOfferSelect2)){
		  $customOfferSelect2 = $this->customOfferSelect2;
	  }
	  if(is_numeric($this->customOfferSelect1)){
		   $customOfferSelect1 = $this->customOfferSelect1;
	  }
	return $customOfferSelect1+$customOfferSelect2;

  };

  $fields['totalpoints']= function ($model) {

  return $model->totalloyaltypoint;
  };

  $fields['dealpoints']= function ($model) {

  return $model->dealpricepoint;
  };

  return $fields;
  }

}
