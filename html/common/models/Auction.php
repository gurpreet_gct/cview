<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\Country;
use common\models\States;
use api\modules\v1\models\Agency;
use api\modules\v1\models\Agent;
use api\modules\v1\models\ApplyBid;

/**
 * This is the model class for table "tbl_properties".
 *
 *
 */
class Auction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auction_event}}';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['listing_id', 'auction_type', 'datetime_start', 'datetime_expire', 'starting_price', 'estate_agent_id', 'auction_currency_id', 'auction_bidding_status'], 'required' ,'on'=>'adminUpdate'],
        	[['listing_id', 'auction_type', 'date', 'time', 'datetime_start', 'datetime_expire', 'starting_price', 'home_phone_number', 'estate_agent', 'picture_of_listing', 'duration', 'real_estate_active_himself', 'real_estate_active_bidders', 'reference_relevant_electronic', 'reference_relevant_bidder', 'created_at', 'auction_status', 'streaming_url', 'streaming_status', 'templateId', 'envelopeId', 'documentId', 'document', 'recipientId', 'bidding_status', 'updated_at', 'created_by', 'updated_by', 'estate_agent_id', 'agency_id', 'auction_currency_id', 'auction_bidding_status'], 'safe'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','Id'),
			'listing_id' => Yii::t('app','Property'),
			'datetime_start' => Yii::t('app','Auction Start Time'),
			'datetime_expire' => Yii::t('app','Auction End Time'),
			'duration' => Yii::t('app','Auction Duration'),
        ];
    }

    /**
     * @inheritdoc
     */   
   	public function beforeSave($insert)
	{
        //echo '<pre>'; print_r($this); die('herse');
		if(!empty($this->auction_type)){
			if($this->auction_type == 'auction')
				$this->auction_status = 1;
			else if($this->auction_type == 'streaming')
				$this->auction_status = 2;
			else
				$this->auction_status = 3;
		}
		if(strstr($this->datetime_start,"-")){
			$this->datetime_start = strtotime($this->date.' '.$this->time);
		}
		if(strstr($this->date,"-") && !empty($this->date))
		{          
			$this->date=strtotime($this->date);
		}
		/*if(!empty($this->document))
			$this->document = json_encode($this->document);*/
	    if (! parent::beforeSave($insert)) {
	    	return false;
	    }

    //	$this->updated_by= Yii::$app->user->id;
    	$this->updated_at= time();
		if ($insert) {			    		
    //		$this->created_by= Yii::$app->user->id;
			$this->created_at= time();
     	}
    	return true;
	    
	}
	
	public function afterFind()
	{
		parent::afterFind();
		
		/* Date From */
		if(!strstr($this->date,"-") && !empty($this->date))
		{             
			$this->date=date("Y-m-d",$this->date);
		}						
		if(!strstr($this->datetime_start,"-") && !empty($this->datetime_start))
		{             
			$this->datetime_start=date("Y-m-d H:i:s",$this->datetime_start);
		}						
		if(!strstr($this->updated_at,"-") && !empty($this->updated_at))
		{             
			$this->updated_at=date("Y-m-d",$this->updated_at);
		}	
		//~ if(!empty($this->document)){
			//~ $documents = json_decode($this->document);
			//~ $details = [];
			//~ $link = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('images/property/doc');
			//~ //$object = new \stdClass();
			//~ if(count($documents)>1){
				//~ foreach($documents as $key => $document){
					//~ if(!empty($document))
					//~ $data['url'] = $document;
					//~ $details[] = $data;
					//~ }
					//~ //print_r((object)$details);die;
					//~ $this->document = $details;
			//~ }elseif(count($documents) == 1){
				//~ $data['url'] = $documents[0];
				//~ $details[] = $data;
				//~ $this->document = $details;
			//~ }
		//~ }					
		return true;		
	}
	
	public function getCountry()
	{
		return $this->hasOne(Country::className(), ['id_countries' =>'country_id']); 
	}
		
	public function getState()
	{
		return $this->hasOne(States::className(), ['id' =>'state']); 
	}
    
    public function getAgency() 
    {
    	return $this->hasOne(Agency::className(), ['id' =>'agency_id']);
    }
    
    public function getAgent() 
    {
    	return $this->hasOne(Agent::className(), ['id' =>'estate_agent_id']);
    }
    
	public function fields(){
        $fields = parent::fields();
		$fields['agent'] = function ($model) {
			return $model->agent;
		};
		$fields['agency'] = function ($model) {
			return $model->agency;
		};        
	   return $fields;
	}
		
	/*public function extraFields()
	{
		return [
		'addresstype',//=>['id','passUrl']
		

		];
	}*/
	
	public function getPropertylive()
    {
         return $this->hasOne(Property::className(), ['id' => 'listing_id']);
    }
    
    public static function checkvalidBidder($auction_id){
		/* User Type = 3 Customer/Bidder , 10 Agent */
		
		$userType = (isset(Yii::$app->User->identity->user_type)?Yii::$app->User->identity->user_type:0);
		$userID = (isset(Yii::$app->User->id)?Yii::$app->User->id:0);
		
			 
			//if(($userType>0) && ($userID>0)) {
			$checkvalidBid = ApplyBid::findOne(['user_id' => $userID,'auction_id'=>$auction_id]);
			  $checkvalidBidInEvent = self::findOne(['auction_id'=>$auction_id]);
			  if(($checkvalidBidInEvent  && in_array($userID,explode(",",$checkvalidBidInEvent->reference_relevant_bidder)))||($checkvalidBid))
			  {
				  
				  $checkvalidBid=true;
			  }
			  if($checkvalidBid)				 
					 return 1;					
					else 
					return 13;
			/* Check bidder user type */
			 if($userType==2 || $userType==6){
				    if($userType==2){
						$property=Property::find()->where(['id'=>$auction_id])->andWhere(["OR",['estate_agent_id' => $userID],['second_agent_id' => $userID]])->one();
						if(!empty($property))
						return 10;	
						else
						return 11;				
					}
					return $userType;
			  }else{
				return 0;
			  }
			
			
			//}return 0;		
	
	
	}
    
    // Function to get the detail of an Auction
    public function getAuctionDetail($id){
        return $this->findOne(['id' => $id]);
    }
    
    // To Get the Agent Id by auction_id
	public function getAgentId($auction_id){ 
        return $query = Auction::find(['id'=>$auction_id])->select('estate_agent_id')->one();
	}

}