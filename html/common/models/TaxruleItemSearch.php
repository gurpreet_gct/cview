<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaxruleItem;

/**
 * TaxruleItemSearch represents the model behind the search form about `common\models\TaxruleItem`.
 */
class TaxruleItemSearch extends TaxruleItem
{
   /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
		
			[['tax_id','rule_id','is_delete'],'safe', ], 
            [['created_at', 'updated_at','updated_by','created_by'], 'integer'],
            
            
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'tax_id' => Yii::t('app','Tax_Id'),
            'rule_id' => Yii::t('app','Rule_Id'),
            'is_delete' => Yii::t('app','Is_Delete'),
            'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
            'created_by'=>Yii::t('app','Created_By'),
            'updated_by'=>Yii::t('app','Updated_By'),
        ];
    }
    
    
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaxruleItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        
        $query->andFilterWhere([
          
            'tax_id' => $this->tax_id,
            'rule_id' => $this->rule_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'created_by' => $this->created_by,
            
        ]);

      //  $query->andFilterWhere(['like', 'TaxruleItem', $this->TaxruleItem]);
      
       
            

        return $dataProvider;
    }
}
