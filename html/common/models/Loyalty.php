<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_loyalty".
 *
 */
class Loyalty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%loyalty}}';
    }


    public function rules()
    {
        return [
			 [['loyalty','dolor','redeem_loyalty','redeem_dolor'], 'required', 'when' => function($model) {
               return '';
           },
           'whenClient' => "function (attribute, value) {

                  var  wtval = $('input:checkbox[name=\"Profile[addLoyalty]\"]:checked').val();

                    if (wtval==1) {

                   return true; }
                   else {

                   return false; }

               }"

           ],
            [['loyalty'], 'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
            [['dolor'], 'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
             [['redeem_loyalty'], 'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
            [['redeem_dolor'], 'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
			[['pointvalue'], 'safe'],
			[['redeem_pointvalue'], 'safe'],
           # ['loyalty_pin','string','length' => [6, 6] ],
            [['user_id'], 'safe'],
         ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'=>Yii::t('app','ID'),
            'loyalty' => Yii::t('app','Loyalty'),
            'dolor' => Yii::t('app','Dollar'),
            'redeem_dolor' =>Yii::t('app','Redeem Dollar') ,
            'user_id' => Yii::t('app','User name'),
            'pointvalue' => Yii::t('app','Dollors'),
            'redeem_pointvalue' => Yii::t('app','Redeem Dollors'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At '),
            'updated_at' => Yii::t('app','Updated At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_by' => Yii::t('app','Updated By'),
            'loyalty_pin' => Yii::t('app','Loyalty Pin'),
            'redeem_loyalty' =>Yii::t('app','Redeem Loyalty') ,
           ];
    }
    public function getCustomername(){
      $newvalue =  User::findOne(['id' => $this->user_id]);
		return $newvalue;
      }






 	public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
			    	if($insert){
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			     	}

			    	return true;
			    } else {

			     return false;
			    }
			}


}
