<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_transactions_fees".
 *
 * @property integer $id
 * @property integer $charge_type
 * @property string $amount
 */
class TransactionsFees extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transactions_fees}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['charge_type'], 'integer'],
            [['title'], 'string'],
            [['amount','charge_type','created_at','updated_at','created_by','updated_by'], 'number'],
             [['amount'],'number','min'=>0,'max'=>50],
            [['paid_within','charge_type','amount','title'],'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'charge_type' => 'Charge type',
            'amount' => 'Amount',
            'partner_id' => 'Partner',
            'paid_within' => 'Paid within',
        ];
    }
    public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			$this->updated_by= Yii::$app->user->id;
			$this->updated_at= time();
			if($insert){			    		
				$this->created_by= Yii::$app->user->id;
				$this->created_at= time();
			}
		
			return true;
		} else {
			
		 return false; 
		}
	}
	static public function chargeTypes(){
		 $chargeTypes = [
							'1'=>'Charge amount for each transactions',
							'2'=>'Buy xx get xx',
							'3'=>'xx %off',
							'4'=>'Enjoy xx% Off on xx',
							'5'=>'Buy xx get xx',
							'6'=>'Buy xx for $ xx',
							'7'=>'Buy xx for $ xx',
							'8'=>'Offer Was/Now (Numeric Only in $)',
						]; 	
		return $chargeTypes;
	}
	static public function getRules(){
		return self::find()->asArray()->all();
	}
}
