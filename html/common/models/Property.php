<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\Country;
use common\models\States;
use yii\helpers\ArrayHelper;
use api\modules\v1\models\Agency;
use api\modules\v1\models\Agent;
use api\modules\v1\models\ApplyBid;
use api\modules\v1\models\CviewState;

/**
 * This is the model class for table "tbl_properties".
 *
 *
 */
class Property extends \yii\db\ActiveRecord
{
	public $distance;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%properties}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required' ,'on'=>'webhook_create'],
        	[['title', 'description', 'city', 'state', 'status', 'address', 'latitude', 'longitude', 'distance','video', 'videoImage', 'created_at', 'updated_at', 'propertyStatus', 'created_by', 'updated_by', 'cview_listing_id', 'is_deleted', 'business_email', 'website', 'valuation', 'exclusivity', 'land_area_unit', 'floor_area_unit', 'tenancy', 'zoning', 'authority', 'sale_tax', 'tender_closing_date', 'current_lease_expiry', 'outgoings', 'lease_price', 'lease_price_show', 'lease_period', 'lease_tax', 'external_id', 'video_url', 'slug'], 'safe'],
        	[['price', 'propertyType', 'landArea','floorArea', 'country', 'parking', 'school', 'communityCentre', 'parkLand', 'shopping', 'bedrooms', 'bathrooms'], 'number'],     
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','Id'),
			'title' => Yii::t('app','Title'),
			'description' => Yii::t('app','Description'),
			'status' => Yii::t('app','Status'),
			'propertyType' => Yii::t('app', 'Property Type'),
			'price' => Yii::t('app', 'Price'),
			'landArea' => Yii::t('app', 'Land_Area'),
			'floorArea' => Yii::t('app', 'Floor_Area'),
			'address' => Yii::t('app','Address') ,
			'latitude' => Yii::t('app', 'Latitude'),
			'longitude' => Yii::t('app', 'Longitude'),
			'streetName' => Yii::t('app', 'Street_Name'),
			'suburb' => Yii::t('app', 'Suburb'),
			'city' => Yii::t('app','City'),
			'state' => Yii::t('app','State'),
			'pincode' => Yii::t('app','Postal_Code'),
			'country_id' => Yii::t('app','Country'),
			'parking' => Yii::t('app','Parking'),
			'parkingList' => Yii::t('app','Parking_List'),
			'parkingOther' => Yii::t('app','Parking_Other'),
			'floorplan' => Yii::t('app','Floor_Plan'),
			'media' => Yii::t('app', 'Media'),
			'video' => Yii::t('app', 'Video'),
			'videoImage' => Yii::t('app', 'VideoImage'),
			'document' => Yii::t('app', 'Document'),
			'school' => Yii::t('app','School'),
			'communityCentre' => Yii::t('app','Community_Centre'),
			'parkingLand' => Yii::t('app','Parking_Land'),
			'publicTransport' => Yii::t('app','Public_Transport'),
			'shopping' => Yii::t('app','Shopping'),
			'bedrooms' => Yii::t('app','Bedrooms'),
			'bathrooms' => Yii::t('app','Bathrooms'),
			'features' => Yii::t('app','Features'),
			'tags' => Yii::t('app','Tags'),
			'user_id' => Yii::t('app', 'User_Id'),
			'created_by' => Yii::t('app','Created_By'),
			'propertyStatus' => Yii::t('app', 'Property_Status'),
			'updated_by' => Yii::t('app','Updated_By'),
			'created_at' => Yii::t('app','Created_At'),
			'updated_at' => Yii::t('app','Updated_At'),
			'is_featured' => Yii::t('app', 'Is_Featured'),
			'cview_listing_id' => Yii::t('app', 'cview_listing_id'),
			'is_deleted' => Yii::t('app', 'is_deleted'),
        ];
    }
	
	
	public function afterFind()
	{
		parent::afterFind();
		
		/* Date From */
		if(!strstr($this->created_at,"-") && !empty($this->created_at))
		{             
			$this->created_at=date("Y-m-d H:i:s",$this->created_at);
		}						
		if(!strstr($this->updated_at,"-") && !empty($this->updated_at))
		{             
			$this->updated_at=date("Y-m-d H:i:s",$this->updated_at);
		}
		// if($this->videoImage){
			// $videoImageSet = json_decode($this->videoImage, true);
			// if (count($videoImageSet) == 0 ) {
			// 	return true;
			// }
			// // $link = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('video_thumbnail');
			// for($i=0; $i<count($videoImageSet); $i++) {
			// 	$videoImageSet[$i] =  $link.'/'.$videoImageSet[$i];	
			// }
			// $this->videoImage =  json_encode($videoImageSet);
		// }
		return true;		
	}
	
    /**
     * @inheritdoc
     */   
   	public function beforeSave($insert)
	{
	    if (! parent::beforeSave($insert)) {
	    	return false;
	    }

    	$this->updated_by= Yii::$app->user->id;
    	$this->updated_at= time();
    	if ($insert) {			    		
    		$this->created_by= Yii::$app->user->id;
    		$this->created_at= time();
     	}
    	return true;
	    
	}
	
	public function getCountry()
	{
		return $this->hasOne(Country::className(), ['id_countries' =>'country_id']); 
	}
		
	public function getState()
	{
		return $this->hasOne(CviewState::className(), ['id' =>'state']); 
	}
	 
	/* Make relation Auction Event and Property */
	public function getAuction()
	{	
		return $this->hasOne(Auction::className(), ['listing_id' => 'id']);
//		 return $this->hasMany(Auction::className(), ['listing_id' => 'id']);
	}
	public function getCalendar()
	{	
		$result= 0;
		if(!empty(Yii::$app->getRequest()->getHeaders()->get('Authorization')))
			$user_id = Yii::$app->MyConstant->UserID(Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        if(isset($user_id))
        $result = $this->hasOne(ApplyBid::className(), ['auction_id' => 'id'])->andOnCondition(['user_id' => $user_id]);
        return $result;
	}
	
	public function getGallery()
    {
        return $this->hasMany(Gallery::className(), ['tbl_pk' => 'id'])->andOnCondition(['tbl_name' => 'tbl_properties']);
    }
	public function getApplyBid()
    {
		$result= null;
		if(!empty(Yii::$app->getRequest()->getHeaders()->get('Authorization')))
			$user_id = Yii::$app->MyConstant->UserID(Yii::$app->getRequest()->getHeaders()->get('Authorization'));
        if(isset($user_id))
        $result = $this->hasOne(ApplyBid::className(), ['auction_id' => 'id'])->andOnCondition(['user_id' => $user_id]);
    	// $result = $this->auction->hasOne(ApplyBid::className(), ['auction_id' => 'id'])->andOnCondition(['user_id' => $user_id]);
        return $result;
    }

    /*public function getAgency() 
    {
    	return $this->hasOne(Agency::className(), ['id' =>'agency_id']);
    }
    public function getAgent() 
    {
    	return $this->hasOne(Agent::className(), ['id' =>'estate_agent_id']);
    }*/

    public function getPropertyTypename() 
    {
    	return $this->hasOne(PropertyType::className(), ['id' =>'propertyType']);
    }
   
    public function getCity() 
    {
    	return $this->hasOne(City::className(), ['id' =>'state']);
    }
	/* Bidding now */
	public function getbidding()
    {
         return $this->hasOne(Bidding::className(), ['auction_id' => 'id']);
    }
    public function fields()
	{
        //echo '<pre>'; print_r($this->auction); //die('ger');
		$fields=parent::fields();
		$fields['distance']= function ($model) {
			return  !empty($model->distance)?$model->distance:'';
		};
		$fields['propertyTypename'] = function ($model) {
			return !empty($model->propertyTypename)?$model->propertyTypename->name:'';
		};
		$fields['auctionDetails'] = function($model){
			if(!empty($model->auction->datetime_start)) {
				return $this->getAllDetails($model->id,$model->auction->datetime_start);
			}
		};
		$fields['gallery']= function ($model) {
			return  $model->gallery;
		};
		$fields['auction']= function ($model) {
			return  $model->auction;
		};
		/*$fields['agent'] = function ($model) {
			return $model->agent;
		};*/
		/*$fields['agency'] = function ($model) {
			return $model->agency;
		};*/
		$fields['saveauction']= function ($model) {
			return  $model->applyBid;
		};
		$fields['sold']= function ($model) {
			return  !empty($model->bidding)?$model->bidding->bid_won:'0';
		};
		$fields['calendar'] = function($model){
			return !empty($model->calendar)?$model->calendar->add_to_calendar:0;
		};
		return $fields;
		
	}
	
	public function getAllDetails($id = null,$start_time){
        $live = $this->getCheckActiveAuction($id);
		$past = $this->getCheckActiveAuctionpast($id);
		$seconds = 0;
		$upcomming = $this->getCheckActiveAuctionupcoming($id);
		if(!empty($start_time)){
			if($this->getCheckActiveAuctionupcoming($id) == true || $this->getCheckActiveAuction($id) == true){
				$cal = strtotime($start_time) - time();
				if ($cal > 0) {
					$seconds = $cal;
				}
			}
		}
		return ['live'=>$live,'past'=>$past,'upcoming'=>$upcomming,'seconds'=>$seconds];
	}
	
	public function getCheckActiveAuction($id=null) {
		if(!empty($id)){
			$currentDateTime = strtotime(date('Y-m-d H:i:s',time()));
			$ActiveBid = Auction::find()->select([Auction::tableName().'.id',Auction::tableName().'.listing_id','datetime_start'])->joinWith(['propertylive'])->where(sprintf('datetime_start <= %s' ,strtotime(date('Y-m-d H:i:s'))))
			//->andWhere([Property::tableName().'.propertyStatus'=>1])
			->andWhere(['>=',Auction::tableName().'.created_at',$currentDateTime])
			->andWhere([Auction::tableName().'.listing_id' => $id])->one(); 
			
			if($ActiveBid)
				return true;
			else
				return false;
		}
	
	}
	
	public function getCheckActiveAuctionpast($id=null) {
		if(!empty($id)){
			$currentDateTime = strtotime(date('Y-m-d H:i:s',time()))-86400;
			$ActiveBid = Auction::find()->select([Auction::tableName().'.id',Auction::tableName().'.listing_id','datetime_start'])->joinWith(['propertylive'])
			//->where(['>',Property::tableName().'.propertyStatus',1]) 
			->where([Auction::tableName().'.listing_id' => $id])
			->andWhere(['<',Auction::tableName().'.created_at',$currentDateTime])
			->andWhere(['<','datetime_start',$currentDateTime])
			->one();
            if($ActiveBid)
				return true;
			else
				return false;
		}
	}

    public function getCheckActiveAuctionupcoming($id=null) {
		if(!empty($id)){
			$currentDateTime = strtotime(date('Y-m-d H:i:s',time()));
			$ActiveBid = Auction::find()->select([Auction::tableName().'.id',Auction::tableName().'.listing_id',Auction::tableName().'.datetime_start'])->joinWith(['propertylive'])->where(['>', Auction::tableName().'.datetime_start',$currentDateTime])//->andWhere([Property::tableName().'.propertyStatus'=>1])
			//->andWhere(['>',Auction::tableName().'.created_at',$currentDateTime])
			->andWhere([Auction::tableName().'.listing_id' => $id])->one(); 
			//print_r($ActiveBid);die;
			if($ActiveBid)
				return true;
			else
				return false;
		}
	}
    
    public function getActiveProperties(){
        $res = self::find()->select(['id','title'])->where(['is_deleted' => 0, ])->all();
        list($id,$name) = ['id','title'];
        $res = array_map(function($res) use($id,$name) {return [$id=>(string)$res->$id, $name=>$res->$name];}, $res);
        return ArrayHelper::map($res, $id, $name);        
    }

}