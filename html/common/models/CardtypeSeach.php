<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Cardtype;

/**
 * CardtypeSeach represents the model behind the search form about `common\models\Cardtype`.
 */
class CardtypeSeach extends Cardtype
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'icon'], 'safe'],
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'id' =>  Yii::t('app','ID'),
            'name' => Yii::t('app','Name'),
            'icon' => Yii::t('app','Icon'),
            'status' =>Yii::t('app','Status') ,
            'created_at' =>Yii::t('app','Created At') ,
            'updated_at' =>Yii::t('app','Updated At') ,
        ];
    }
    
    
    
    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cardtype::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'icon', $this->icon]);

        return $dataProvider;
    }
}
