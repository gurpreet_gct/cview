<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bidding;
use common\models\Property;

/**
 * BiddingSearch represents the model behind the search form about `common\models\Bidding`.
 */
class BiddingSearch extends Bidding
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'listing_id', 'bidder_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['amount'], 'number'],
            [['bid_won'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$PropertyID=null)
    {
		
        $query = Bidding::find()->orderBy(Bidding::tableName().'.id desc')
        ->select([Bidding::tableName().".*","propertyStatus"]);
		$query->joinWith('property');
		$query->joinWith('user');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
				'pageSize' => 10,
			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'listing_id' => $PropertyID,
            'bidder_id' => $this->bidder_id,
            'bid_won' => $this->bid_won,
            'amount' => $this->amount,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
