<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%product_option}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $title
 * @property string $price
 */
class ProductOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['price'], 'number'],
            [['title'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'product_id' => Yii::t('app','Product ID'),
            'title' => Yii::t('app','Title'),
            'price' => Yii::t('app','Price'),
        ];
    }
}
