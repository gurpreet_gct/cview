<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%setting}}".
 *
 * @property integer $id
 * @property string $partner_referral
 * @property string $customer_referral
 * @property string $subscription_fees
 * @property integer $subscription_day
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
   
    public static function tableName()
    {
        return '{{%setting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['minimum_cashout','partner_referral', 'customer_referral', 'subscription_fees'], 'number','min'=>0.1],
            [['partner_referral','customer_referral'],'number','max'=>50],
            [['subscription_day', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
             [['minimum_cashout','partner_referral', 'customer_referral', 'subscription_fees','cashout'],'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'partner_referral' => Yii::t('app', 'Partner Referral %'),
            'customer_referral' => Yii::t('app', 'Customer Referral %'),
            'subscription_fees' => Yii::t('app', 'Default Subscription Fees'),
            'subscription_day' => Yii::t('app', 'Default Subscription Day'),
            'minimum_cashout' => Yii::t('app', 'Minimum referral amount for Cashout'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
    public function beforeSave($insert)
	{
		   if (parent::beforeSave($insert)) {
			   $this->updated_at= time();
			   $this->updated_by = Yii::$app->user->id;
			   if($insert){
				   $this->created_at= time();
				   $this->created_by = Yii::$app->user->id;
			   }

			   return true;
			 } else {

			  return false;
		   }
	}
	
	public function getPercentage(){
		$user = Yii::$app->user->identity;		
		if(!empty($user->referralCode)){
			$refferedUsr = \common\models\User::find()
							->select(['id','referral_percentage as p','user_type'])
							->where("user_code='$user->referralCode'")->asArray()->one();
			if(isset($refferedUsr['p'])){
				return ['amount'=>$refferedUsr['p'],'referBy'=>$refferedUsr['id']];
			}else {
				$data =  self::find()
						->select(['partner_referral as 7','customer_referral as 3'])
						->asArray()->one();
				return ['amount'=>$data[$refferedUsr['user_type']],'referBy'=>$refferedUsr['id']];	
			}
		}
		return '';
		
	}
	
	public static function getCashOutLimit(){
		
		$data =  self::find()->select('minimum_cashout')->asArray()->one();
		return $data['minimum_cashout'];
	}
	
	public static function getSubFeeDate(){
		
		$data =  self::find()->select(['subscription_fees','subscription_day'])->asArray()->one();
		return $data;
	}
}
