<?php

namespace common\models;

use Yii;
use common\models\FoodCategories;
/**
 * This is the model class for table "{{%userfoodcategory}}".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $category_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class Userfoodcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%userfoodcategory}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'category_id', 'status', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'user_id' => Yii::t('app','User ID'),
            'category_id' => Yii::t('app','Category ID'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
	  public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
			
            if ($this->isNewRecord) {
               $this->created_at=time();
            }
			   $this->updated_at=time();
				
            return true;
        }
        return false;
    }
		public function getCategories()
	{
		return $this->hasOne(FoodCategories::className(), ['id' => 'category_id']);
	}	
}
