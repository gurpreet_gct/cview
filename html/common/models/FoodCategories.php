<?php 

namespace common\models;
use \yii\db\ActiveRecord;
use Yii;
 
class FoodCategories extends \kartik\tree\models\Tree
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%foodcategories}}';
    }  
    
    
    
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'root' => Yii::t('app','Root'),
			'lft' => Yii::t('app','Lft'),
			'rgt' => Yii::t('app','Rgt'),
			'lvl' => Yii::t('app','Lvl'),
			'name' => Yii::t('app','Name'),
			'parentKey' => Yii::t('app','Parent_Key'),
			'icon' => Yii::t('app','Icon'),
			'imageurl' => Yii::t('app','Image_Url'),
			'icon_type' => Yii::t('app','Icon_Type'),
			'active' => Yii::t('app','Active'),
			'selected' => Yii::t('app','Selected'),
			'disabled' => Yii::t('app','Disabled'),
			'readonly' => Yii::t('app','Readonly'),
			'visible' => Yii::t('app','Visible'),
			'collapsed' => Yii::t('app','Collapsed'),
			'moveable_u' => Yii::t('app','Moveable_U'),
			'moveable_d' => Yii::t('app','Moveable_D'),
			'moveable_r' => Yii::t('app','Moveable_R'),
			'moveable_l' => Yii::t('app','Moveable_L'),
			'removeable' => Yii::t('app','Removeable'),
			'removeable_all' => Yii::t('app','Removeable_All'),
			'path' => Yii::t('app','Path'),
			
			
           ];
    }
    
    
    

    public function rules()
    {
        return [
            
           // [['lvl'], 'integer','max'=>2],
          	];
    }    
      
    
    /**
     * Override isDisabled method if you need as shown in the  
     * example below. You can override similarly other methods
     * like isActive, isMovable etc.
     */
   public function isDisabled()
    {
		
        if (Yii::$app->user->id !== 'admin') {
            return false;
        }
        return parent::isDisabled();
    }

	public function updateCategoryPath()
	{
		
		$data=$this->find()->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC])->asArray()->all(); 	

		$requiredResult=array();
		$oldLevel=0;
		$levelKeys=array();
		
		$path="";
		$ids=[];
		foreach($data as $key=>$value)
		{
			$value['path']="root/";
			$key=$value['id'];
			if($value['lvl']==0)
			{
			$value['path']=$value['path'].$value['name'];
			$requiredResult[$key]=$value;
				
			}
			else
			{	
				$array_ptr=&$requiredResult;
				
				for($i=0; $i<$value['lvl']; $i++)
				{
					$value['path']= $array_ptr[ strval($levelKeys[$i])]['path'];						 
					 $array_ptr = &$array_ptr[ strval($levelKeys[$i]) ]['subCategory'];
					 
				}
				$value['path']=$value['path']."/".$value['name'];
				$array_ptr[$key]=$value;			
			}            
			$path.=sprintf(" WHEN %s THEN '%s'",$value['id'],str_replace(" ","-",$value['path']));
			array_push($ids,$value['id']);
			$levelKeys[$value['lvl']]=$value['id'];			
		}	
		
		$command="update ". $this->tableName(). " set path=(case id ".$path." else '' END) where id in (".implode(",",$ids).")";  
		
		Yii::$app->db->createCommand($command)->execute();
					
	}
	
	public function findChilds($id)
	{
		//$this->find()->
	}
	
	public function convertToTree($data)
	{
		//$data=$this->find()->orderBy(['root'=>SORT_ASC,'lft'=>SORT_ASC])->asArray()->all(); 	

		$requiredResult=array();
		$oldLevel=0;
		$levelKeys=array();
		
		$path="";
		$ids=[];
		foreach($data as $key=>$value)
		{
			
			$key=$value['id'];
			if($value['lvl']==0)
			{
			
			$requiredResult[$key]=$value;
				
			}
			else
			{	
				$array_ptr=&$requiredResult;
				
				for($i=0; $i<$value['lvl']; $i++)
				{
										 
					 $array_ptr = &$array_ptr[ strval($levelKeys[$i]) ]['subCategory'];
					 
				}
			
				$array_ptr[$key]=$value;
		
			}            
			
			array_push($ids,$value['id']);
			$levelKeys[$value['lvl']]=$value['id'];			
		}	
		
		return $requiredResult;
	}
	
	
	
}

