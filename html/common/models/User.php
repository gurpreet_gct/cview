<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\Store;
use backend\models\Dealstore;
use api\modules\v1\models\UserStore;
use yii\web\urlManager;
use common\models\Workorders;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED	= 0;
    const STATUS_ACTIVE 	= 10;
    const ROLE_ADMIN 		= 1;
    const ROLE_CUSTOMER 	= 3;
    const ROLE_LOCATION_OWNER = 6;
    const ROLE_Advertiser = 7;

    const USER_TYPE_BIDDER = '2';

    const SCENARIO_CVIEW_REQUEST = 'cview';
    const SCENARIO_WEB_REQUEST = 'web';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First_Name'),
			'lastName' => Yii::t('app','Last_Name'),
			'fullName' => Yii::t('app','Full_Name'),
			'orgName' => Yii::t('app','Org_Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb_Identifier'),
			'role_id' => Yii::t('app','Role_Id'),
			'user_type' => Yii::t('app','User_Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New Password'),
			'password_reset_token' => Yii::t('app','Password_Reset_Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date_Of_Birth'),
			'ageGroup_id' => Yii::t('app','Age_Group_Id'),
			'walletPinCode' => Yii::t('app','Wallet_Pin_Code'),
			'loyaltyPin' => Yii::t('app','Loyalty_Pin'),
			'auth_key' => Yii::t('app','Auth_Key'),
			'activationKey' => Yii::t('app','Activation_Key'),
			'confirmationKey' => Yii::t('app','Confirmation_Key'),
			'access_token' => Yii::t('app','Access_Token'),
			'hashKey' => Yii::t('app','Hash_Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device_Token'),
			'lastLogin' => Yii::t('app','Last_Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),

        ];
    }



    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['email', 'required'],
            ['password_hash', 'required', 'message' => 'New PIN cannot be blank.'],
            [['password_hash'], 'string', 'min' => 4, 'on' => self::SCENARIO_WEB_REQUEST, 'when' => function($model) {
            return in_array($model->usertype->id,array(1,2,4,5,6,7,8));
            }, ],
            [['password_hash'], 'string', 'length' => 6,'when' => function($model) {
            return in_array($model->usertype->id,array(1,2,4,5,6,7,8));
            },  'on' => self::SCENARIO_CVIEW_REQUEST],
            //['password_hash','required','on'=>'create'],
            ['email', 'email'],
            [['referralCode', 'user_code', 'latitude', 'longitude', 'device_token', 'device', 'firstName', 'lastName', 'username', 'auth_key', 'fullName', 'user_type', 'app_id', 'email', 'orgName', 'fbidentifier', 'linkedin_identifier', 'google_identifier', 'role_id', 'password_hash', 'password_reset_token', 'phone', 'image', 'sex', 'dob', 'ageGroup_id', 'bdayOffer', 'walletPinCode', 'loyaltyPin', 'advertister_id', 'activationKey', 'confirmationKey', 'access_token', 'hashKey', 'status',  'lastLogin', 'timezone', 'timezone_offset', 'created_at', 'updated_at', 'storeid', 'promise_uid', 'promise_acid', 'referral_percentage', 'agency_id'], 'safe', ],            
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        //throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
          return static::findOne(['auth_key' => $token, 'user_type'=>[3,9]]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }


    /* find email by email address */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }
    /* Check if user is customer */
    public static function isUserCustomer($username)
    {


          if (static::find()
		->andWhere('email = :username or username = :username',[':username' => $username])
		//->andwhere('user_type=:usertype',['usertype' => self::ROLE_CUSTOMER])
		->andWhere(['user_type'=>[3,9]])
		->one()){
                 return true;
          } else {

                 return false;
          }

    }

    /* Check if user is bidder */
    public static function isUserCviewUser($username)
    {
        if (static::find()
            ->andWhere('email = :username or username = :username',[':username' => $username])
            // ->andwhere('user_type=:usertype',['usertype' => self::ROLE_CUSTOMER])
            ->andWhere(['app_id'=>self::USER_TYPE_BIDDER])
            ->andWhere(['user_type'=>[3]])
            ->one()) {
                return true;
            } else {
                return false;
            }
    }

    /* Check if user is bidder */
    public static function isUserBidder($username)
    {
        if (static::find()
            ->andWhere('email = :username or username = :username',[':username' => $username])
            //->andwhere('user_type=:usertype',['usertype' => self::ROLE_CUSTOMER])
            ->andWhere(['user_type'=>10])
            ->one()) {
                return true;
            } else {
                return false;
            }
    }

    /* Check if user is agent */
    public static function isUserAgent($username)
    {
        if (static::find()
        ->andWhere('email = :username or username = :username',[':username' => $username])
        //->andwhere('user_type=:usertype',['usertype' => self::ROLE_CUSTOMER])
        ->andWhere(['user_type'=>[10]])
        ->one()){
                 return true;
          } else {

                 return false;
          }
    }

    public static function findByUsernameOrEmail($email)
	{
		$userDetails = static::find()->where('email = :email',[':email' => $email])
		->orWhere('username = :username',[':username' => $email])
		->andWhere('status =:status', [':status'=> self::STATUS_ACTIVE])
		->andWhere('user_type =:user_type', [':user_type'=> self::ROLE_ADMIN])
		->one();
		if(empty($userDetails->id)) {
		$userDetails = static::find()->where('email = :email',[':email' => $email])
		->orWhere('username = :username',[':username' => $email])
		->andWhere('status =:status', [':status'=> self::STATUS_ACTIVE])
		->andWhere('user_type =:user_types', [':user_types'=> self::ROLE_Advertiser])
		->one();
			//return $userDetails;
		}
		  return $userDetails;



	}

    public static function findByIdentifier($identifier, $type='fb', $app_id=false)
	{
        if ($type == 'fb') {
            $cond = ["fbidentifier"=>$identifier,'user_type'=>3,'status'=> self::STATUS_ACTIVE];
            if (!$app_id) {
                return static::find()->where($cond)->andWhere(["<>", "app_id", self::USER_TYPE_BIDDER])->one();
            } else {
                return static::find()->where($cond)->andWhere(["=", "app_id", self::USER_TYPE_BIDDER])->one();
            }
	    }
        if ($type == 'linkedin') {
            $cond = ["linkedin_identifier"=>$identifier,'user_type'=>3,'status'=> self::STATUS_ACTIVE];
            if (!$app_id) {
                return static::find()->where($cond)->andWhere(["<>", "app_id", self::USER_TYPE_BIDDER])->one();
            } else {
                // $cond[] = ["app_id" => $app_id];  
                return static::find()->where($cond)->andWhere(["=", "app_id", self::USER_TYPE_BIDDER])->one();
            }

        }

        if ($type == 'google') {
            $cond = ["google_identifier"=>$identifier,'user_type'=>3,'status'=> self::STATUS_ACTIVE];
            if (!$app_id) {
                return static::find()->where($cond)->andWhere(["<>", "app_id", self::USER_TYPE_BIDDER])->one();
            } else {
                return static::find()->where($cond)->andWhere(["=", "app_id", self::USER_TYPE_BIDDER])->one();
            }
        }
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];


        $parts = explode('_', $token);

        $timestamp = (int) end($parts);

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {

        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);

        return $this->password_hash;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }



    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();

        return $this->password_reset_token;
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;

        return null;
    }

    public function getstore()
	{
		return $this->hasOne(Store::className(), ['owner' => 'id']);


	}

	public function getdealstore()
	{
		return $this->hasOne(Dealstore::className(), ['store_id' => 'id']);


	}





    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsertype()
    {
        return $this->hasOne(Usertype::className(), ['ID' => 'user_type']);
    }


     public function getUserextrafield()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);

    }




    public function beforeSave($insert)
    {


        if (parent::beforeSave($insert)) {

			if(strstr($this->dob,"-"))	{
				$this->dob = strtotime($this->dob);
			}

            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
            }
            else
            {
				if(($this->image!="")&&(!strstr($this->image,'http'))) {

				 preg_match('/.*\/(.*?)\.(jpg|jpeg|png)/',$this->image,$match);

                                if(count($match))
                                    $this->image=$match[1].'.'.$match[2];
				}
			}


            return true;
        }
        return false;
    }


   public function afterFind()
    {
        parent::afterFind();
        if($this->fbidentifier!="")
        {
                $this->image=sprintf("https://graph.facebook.com/%s/picture?type=large",$this->fbidentifier);
        }

        else if(($this->image!="")&&(!strstr($this->image,'http')))
        {

             //$this->image = Yii::$app->params['userUploadPath']."/".$this->image;

        }


		if(!strstr($this->dob,"-"))
            {
                $this->dob=date("Y-m-d",$this->dob);
            }



        return true;
    }
     /* public function fields()
		{

			$fields=parent::fields();
		/*	$fields=['orgName',
			'logo'=>function($model){
				return $model->userextrafield;
				}
			];
			return $fields;
		}
*/
	public function getUserStore()
	{
		 return $this->hasMany(UserStore::className(), ['store_id' => 'id']);
	}



	/*
	*@activeAppUsers
	* user that has opened up the app
	*/

	public function activeAppUsers($startDate,$endDate,$userField){

		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND t.user_type='.$userType.' AND t.id='.$userId;

		}else{

			$custQuery		= '';

		}


		$query 		= Yii::$app->getDb();


		$command 	= $query->createCommand("SELECT tag.id,name,sex,count(if(sex=1,tual.id,null)) as maleActiveUser,count(if(sex=0,tual.id,null)) as femaleActiveUser FROM {{%agegroup}} tag LEFT JOIN {{%users}} t ON tag.id=t.ageGroup_id AND user_type=3 LEFT JOIN {{%user_access_log}} tual ON tual.user_id=t.id AND unix_timestamp()-lastvisit<=3600 AND tual.id is not null GROUP BY tag.id");

		$result 	= $command->queryAll();


		// select tag.id,name,sex,count(t.id)
		// from tbl_agegroup tag
		// left join tbl_users t on tag.id=t.ageGroup_id and user_type=3 and unix_timestamp()-lastlogin<=86400
		// group by tag.id,sex;



		/*
		$genderQuery  		= User::find();
		$dateAge 			= new \DateTime(date('m/d/Y',time()));

		$startRange 		= 5;

		$ageRange 			= 0;

		$dateAge->sub(new \DateInterval('P'.$startRange.'Y'));

		for($i=1;$i<=11;$i++){

			$ageEndDate 		= $dateAge->getTimeStamp(); // max

			$ageRange 			= $startRange + 9;

			$dateAge->sub(new \DateInterval('P10Y'));

			$ageStartDate 		= $dateAge->getTimeStamp(); // min

			// calculate total genders

			$totalGender		= $genderQuery->select('count(case when sex=1 then 1 end) as male_cnt,count(case when sex=0 then 1 end) as female_cnt')
											->Where(['between', 'lastLogin', $startDate,$endDate])
											->andWhere(['user_type'=>3,'status'=>10])
											->andWhere(['between', 'dob', $ageStartDate,$ageEndDate])
											->groupBy('sex')
											->asArray();

			$male_cnt			=	(int) $totalGender->sum('male_cnt');
			$female_cnt			=	(int) $totalGender->sum('female_cnt');

			$totalMale			=	$male_cnt?$male_cnt:0;
			$totalFemale		=	$female_cnt?$female_cnt:0;

			$reportData[] 		= array(
										'maleData'=>$totalMale,
										'femaleData'=>$totalFemale,
										'agerange'=>"$startRange-$ageRange"
									);


			$startRange =  $ageRange+1;

		}  */

		return $result;


	}




	/*
	*@newUsers
	* return array of new users
	*/

	public function newUsers($startDate,$endDate,$userField){

		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND t.user_type='.$userType.' AND t.id='.$userId;

		}else{

			$custQuery		= '';

		}

		$genderQuery  		= User::find();

		$query 				= Yii::$app->getDb();

		$command 			= $query->createCommand("SELECT tag.id,name as agerange,count(if(sex=1,tag.id,null)) as maleData,count(if(sex=0,tag.id,null)) as femaleData FROM {{%agegroup}} tag LEFT JOIN {{%users}} t ON tag.id=t.ageGroup_id AND t.user_type=3 AND (t.created_at BETWEEN $startDate AND $endDate) $custQuery GROUP BY tag.id");

		$result 			= $command->queryAll();

		/*$dateAge 			= new \DateTime(date('m/d/Y',time()));

		$startRange 		= 5;

		$ageRange 			= 0;

		$dateAge->sub(new \DateInterval('P'.$startRange.'Y'));



		 for($i=1;$i<=11;$i++){

			$ageEndDate 		= $dateAge->getTimeStamp(); // max

			$ageRange 			= $startRange + 9;

			$dateAge->sub(new \DateInterval('P10Y'));

			$ageStartDate 		= $dateAge->getTimeStamp(); // min

			// calculate total genders

			$totalGender		= $genderQuery->select('count(case when sex=1 then 1 end) as male_cnt,count(case when sex=0 then 1 end) as female_cnt')
											->Where(['between', 'created_at', $startDate,$endDate])
											->andWhere(['user_type'=>3,'status'=>10])
											->andWhere(['between', 'dob', $ageStartDate,$ageEndDate])
											->groupBy('sex')
											->asArray();

			$male_cnt			=	(int) $totalGender->sum('male_cnt');
			$female_cnt			=	(int) $totalGender->sum('female_cnt');

			$totalMale			=	$male_cnt?$male_cnt:0;
			$totalFemale		=	$female_cnt?$female_cnt:0;

			$reportData[] 		= array(
										'maleData'=>$totalMale,
										'femaleData'=>$totalFemale,
										'agerange'=>"$startRange-$ageRange"
									);


			$startRange =  $ageRange+1;

		} */

		return $result;


	}

	/*
	*@activeAppUsers
	* user that has opened up the app
	*/

	public function devicesUsed($startDate,$endDate,$userField){

		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= 'id='.$userId;

		}else{

			$custQuery		= '';

		}


		$query  		= User::find();

		$reportData 	= 	$query->select('count(id) AS total,device')
						    ->where(['between','created_at',$startDate,$endDate])
							->andWhere(['user_type'=>3,'status'=>10])
							->andWhere($custQuery)
							// ->andWhere("device !='web'")
							->groupBy('device')
							->asArray()
							->all();


		return $reportData;

	}


	// used to get CATEGORIES (TOP / BOTTOM 3)

	public function getCategories($unixDateStart,$unixDateEnd,$userField){

		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND u.id='.$userId;

		}else{

			$custQuery		= '';

		}

		$query 		= Yii::$app->getDb();


		$command 	= $query->createCommand("SELECT tc.id,u.id,tc.name, count(t.id) AS counts from {{%userCategory}} t JOIN {{%categories}} tc on t.category_id=tc.id and tc.lvl=0 LEFT JOIN {{%users}} u ON t.user_id=u.id WHERE  t.status=1 AND (t.created_at BETWEEN $unixDateStart AND $unixDateEnd) $custQuery group by category_id ORDER BY counts DESC");

		$result 	= $command->queryAll();

		return 	$result;

	}


	// used to get getRightNowActiveUsers

	public function getRightNowActiveUsers($userField){


		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND u.user_type='.$userType.' AND u.id='.$userId;

		}else{

			$custQuery		= '';

		}

		$query 		= Yii::$app->getDb();

		$command 	= $query->createCommand("SELECT count(tual.id) as activeUser FROM  {{%user_access_log}} tual LEFT JOIN {{%users}} u ON u.id=tual.user_id WHERE unix_timestamp()-lastvisit<=900 $custQuery");

		$result 	= $command->queryOne();

		return 	$result;

	}


	// used to get getPointsAccrued

	public function getPointsAccrued($start,$end,$userField){

		$query 		= Yii::$app->getDb();

		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND tul.store_id='.$userId;

		}else{

			$custQuery		= '';

		}


		$command 	= $query->createCommand("select tag.id,name,ifnull(sum(if(sex=1,tul.accuredpoints,0)),0) as maleAccured,ifnull(sum(if(sex=0,tul.accuredpoints,0)),0) as femaleAccured FROM {{%agegroup}} tag LEFT JOIN {{%users}} t on tag.id=t.ageGroup_id AND user_type=3 LEFT JOIN {{%user_loyalty}} tul ON tul.user_id=t.id AND (tul.created_at BETWEEN $start AND $end) $custQuery GROUP BY tag.id");



		$result 	= $command->queryAll();

		return 	$result;

	}


	// used to get getPointsAccruedRedeemed

	public function getPointsAccruedRedeemed($start,$end,$userField){

		$query 		= Yii::$app->getDb();

		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND tul.store_id='.$userId;

		}else{

			$custQuery		= '';

		}

		$command 	= $query->createCommand("SELECT tag.id,name,sex,ifnull(sum(if(sex=1,tul.accuredpoints,0)),0) AS maleAccured,ifnull(sum(if(sex=0,tul.accuredpoints,0)),0) AS femaleAccured,ifnull(sum(if(sex=1,tul.accuredpoints,0)),0)-ifnull(sum(if(sex=1,tul.points,0)),0) AS maleRedeem,ifnull(sum(if(sex=0,tul.accuredpoints,0)),0)-ifnull(sum(if(sex=0,tul.points,0)),0) as femaleRedeem FROM {{%agegroup}} tag LEFT JOIN {{%users}} t ON tag.id=t.ageGroup_id AND user_type=3 LEFT JOIN {{%user_loyalty}} tul on tul.user_id=t.id AND (tul.created_at BETWEEN $start AND $end) $custQuery GROUP BY tag.id");

		$result 	= $command->queryAll();

		return 	$result;

	}


	// used to get getTransacByGen

	public function getTransacByGen($start,$end,$userField){

		$query 		= Yii::$app->getDb();

		if(!empty($userField)){

			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' WHERE toi.store_id='.$userId;

		}else{

			$custQuery		= '';

		}


		$command 	= $query->createCommand("SELECT tag.id,tag.name,count(if(t.sex=1,tos.id,null)) AS MaleTransaction,count(if(t.sex=0,tos.id,null)) AS femaleTransaction,ifnull(sum(if(t.sex=1,toi.deal_count,0)),0) AS maleUnit,ifnull(sum(if(t.sex=0,toi.deal_count,0)),0) AS femaleUnit FROM {{%agegroup}} tag LEFT JOIN {{%users}} t ON tag.id=t.ageGroup_id AND user_type=3 LEFT JOIN {{%orders}} tos ON t.id=tos.user_id LEFT JOIN {{%order_item}} toi on toi.order_id=tos.id AND (tos.created_at BETWEEN $start AND $end) $custQuery GROUP BY tag.id");

		$result 	= $command->queryAll();

		return 	$result;

	}

	public function afterSave($insert, $changedAttributes)
	{

		if($this->promise_uid == '' && ($this->user_type==self::ROLE_Advertiser || $this->user_type==self::ROLE_CUSTOMER))
	    {
				$userModel = $this;
				$result=Yii::$app->promisePay->createUser($userModel);
				if(isset($result->id)){
					$paccount_id=$userModel->promise_uid=$result->id;
					$userModel->save(false);
				}

		}
		return true;
	  }
	 //  this function can be used to index and view action to display name
	public static function getUserNameById($id){
		$user = User::find()->select('username')->where('id='.$id)->one();
		if(is_object($user)){
			return $user->username;
		}
		return 'N/A';
	}
}
