<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class UserAgentLoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;
    public $isMobile=false;
    public $device;
    public $resolution;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            [['resolution'], 'safe'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            ['device', 'in','range'=>['android','ios'],'message'=>'Device can be ios or android.','when'=>function($model){
                return $model->isMobile;
            }],
            ['device','required','when'=>function($model){
                return $model->isMobile;
            }],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
    
    
    
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'firstName' => Yii::t('app','First_Name'),
			'lastName' => Yii::t('app','Last_Name'),
			'fullName' => Yii::t('app','Full_Name'),
			'orgName' => Yii::t('app','Org_Name'),
			'email' => Yii::t('app','Email'),
			'fbidentifier' => Yii::t('app','fb_Identifier'),
			'role_id' => Yii::t('app','Role_Id'),
			'user_type' => Yii::t('app','User_Type'),
			'username' => Yii::t('app','Username'),
			'password_hash' => Yii::t('app','New_Password'),
			'password_reset_token' => Yii::t('app','Password_Reset_Token'),
			'phone' => Yii::t('app','Phone'),
			'image' => Yii::t('app','Image'),
			'sex' => Yii::t('app','Sex'),
			'dob' => Yii::t('app','Date_Of_Birth'),
			'ageGroup_id' => Yii::t('app','Age_Group_Id'),
			'walletPinCode' => Yii::t('app','Wallet_Pin_Code'),
			'loyaltyPin' => Yii::t('app','Loyalty_Pin'),
			'auth_key' => Yii::t('app','Auth_Key'),
			'activationKey' => Yii::t('app','Activation_Key'),
			'confirmationKey' => Yii::t('app','Confirmation_Key'),
			'access_token' => Yii::t('app','Access_Token'),
			'hashKey' => Yii::t('app','Hash_Key'),
			'status' => Yii::t('app','Status'),
			'device' => Yii::t('app','Device'),
			'device_token' => Yii::t('app','Device_Token'),
			'lastLogin' => Yii::t('app','Last_Login'),
			'latitude' => Yii::t('app','Latitude'),
			'longitude' => Yii::t('app','Longitude'),
			'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
		//$pwd = Yii::$app->security->generatePasswordHash('kumar');
		//echo $pwd; die();
		//$2y$13$pK1u5mw3KBY/BXb1fYzf.u8ltWZrCeL9EavsbLXNIvwMWVYX8OVEm (kumar)
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if (! $this->validate()) {
            return false;
        }

        if ($this->isMobile) {
            if (! User::isUserAgent($this->username)) {
                $this->addError('password', 'You are not allowed to login the app.');
                return false;
            }
            $user=$this->getUser();   
            $user->generateAuthKey();
            $user->device=$this->device;
            $user->lastLogin=time();
            $user->save(false);                     
            $loginlog=new LoginLogs();
            $loginlog->user_id=$user->id;
            $loginlog->sessionid=$user->auth_key;
            $loginlog->device=$user->device;
            $loginlog->login_at=time();
            $loginlog->ip=$_SERVER['REMOTE_ADDR'];					
            $loginlog->resolution = $this->resolution ;					
            $loginlog->save();
            return Yii::$app->user->login($this->getUser());

        } else {
            $this->_user->lastLogin=time();
            $this->_user->device='web';
            $this->_user->save(false);
            $identitiyObject=Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
            $loginlog=new LoginLogs();
            $loginlog->user_id=$this->_user->id;
            $loginlog->sessionid=Yii::$app->session->id;
            $loginlog->device=$this->_user->device;
            $loginlog->login_at=time();
            $loginlog->ip=$_SERVER['REMOTE_ADDR'];
            $loginlog->resolution = $this->resolution ;
            $loginlog->save();

            return $identitiyObject;
        }
        
    }

    /**
     * Logs in a bidder using the provided username and password.
     *
     * @return boolean whether the bidder is logged in successfully
     */
    public function loginBidder()
    {   

        if (! $this->validate()) {
            return false;
        }

        if ($this->isMobile) {                
            if (! User::isUserBidder($this->username)) {
                $this->addError('password', 'You are not allowed to login the app.');
                return false;
            }
            $user=$this->getUser();   
            $user->generateAuthKey();
            $user->device=$this->device;
            $user->lastLogin=time();
            $user->save(false);                     
            $loginlog=new LoginLogs();
            $loginlog->user_id=$user->id;
            $loginlog->sessionid=$user->auth_key;
            $loginlog->device=$user->device;
            $loginlog->login_at=time();
            $loginlog->ip=$_SERVER['REMOTE_ADDR'];                  
            $loginlog->resolution = $this->resolution ;                 
            $loginlog->save();

            return Yii::$app->user->login($this->getUser());
            
        } else {
            $this->_user->lastLogin=time();
            $this->_user->device='web';
            $this->_user->save(false);
            $identitiyObject=Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
            $loginlog=new LoginLogs();
            $loginlog->user_id=$this->_user->id;
            $loginlog->sessionid=Yii::$app->session->id;
            $loginlog->device=$this->_user->device;
            $loginlog->login_at=time();
            $loginlog->ip=$_SERVER['REMOTE_ADDR'];
            $loginlog->resolution = $this->resolution ;
            $loginlog->save();

            return $identitiyObject;
        }
        
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Agent::findByUsernameOrEmail($this->username);
        }

        return $this->_user;
    }
	

}
