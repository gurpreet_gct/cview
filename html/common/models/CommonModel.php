<?php
namespace common\models;
use Yii;
use common\models\User;
use common\models\Currencies;
use yii\helpers\ArrayHelper;
class CommonModel{
    
    // pagination controls...
    const LIMIT= 15;// pagination limit...

    const HTTP_SUCCESS= 200;
    const HTTP_NON_AUTHORATIVE =  203;
    const HTTP_NO_CONTENT =  204;
    const HTTP_UNAUTHORIZED =  401;
    const HTTP_BAD_REQUEST =  400;
    const HTTP_PAYMENT_REQUIRED =  402;
    const HTTP_NOT_ACCEPTABLE =  406;
    const HTTP_CONFLICT =  409;
    const HTTP_UNPROCESSABLE =  422;
    const HTTP_TOO_MANY_REQUEST =  429;
    const CUSTOM_MAILNOTVERIFIED =  909; // custom error for inactive,blocked user or record...
    const CUSTOM_INACTIVE =  910; // custom error for inactive,blocked user or record...
    const CUSTOM_SERVER_ERROR =  901; // custom error MAIL FAILED OR ANY OTHER SERVER ERROR...

    static public function My(){
        return new self();
    }

    /** Encode array to json object
    *  remove element key no and return Mobile
    *  Relavant Dictionary
    *  @params $array
    *  @return JSON Object Mobile Dictionary
    **/  
    public function JSONEncode($array,$array_format=true){
        if($array_format) $array = self::My()->JSONArrayFormat($array);
            return json_encode($array,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
    }

    /** Convert array to relavant JSON
    * format.remove index from array
    * @params $array array
    * @return $array formated array
    **/
    public function JSONArrayFormat($array){
        $array = array_values($array);
        return $array;
    }

    private function httpStatusCode(){
        $rs ='REQUEST_SUCCESS_';
        return $codes =[
            'HTTP_SUCCESS'=>200,
            $rs.'NO_CONTENT'=>204,
            'REQUEST_ACCEPTED'=>202,
            'NOT_FOUND'=>404,
        ];
    }

    /* send response data to api response
    *@params (int) status_code,(string|array) $message,(array) $response 
    *@returns json | throws Exception
    */
    public function send($status_code,$response=[],$message=null){ 
        $ack                = 'error';
        $success            = ['200','204'];
        if(in_array($status_code,$success)) 
        $ack                = 'success';// && $message = ['success'=>$message];
        else $message       = ['errors'=>$message];    
        $json['status_code']= $status_code;
        $json['ack']        = $ack;
        $json['message']    = $message;
        $json['response']   = $response; 
        die(self::JSONEncode($json,0)); 
    }
    
    /**
    * Test method for performing debugging and stuff
    */
    public function textGlobalization($filename, $key)
    {   
        \Yii::$app->language = 'en-US';
        return Yii::t($filename, $key);
    }
    
    /**
     * Function to get the list of active currency available in the system
     */
    public function getCurrency(){
        $res = Currencies::find()->select(['id', 'code', 'symbol'])->where(['status' => 1])->all();
      //  echo '<pre>'; print_r($res); die('here');
        list($id,$code,$symbol) = ['id','code', 'symbol'];
        $res = array_map(function($res) use($id, $code, $symbol) {return [$id => $res->$id, $code => $res->$code.' ('.$res->$symbol.') '];}, $res);
        return ArrayHelper::map($res, $id, $code);           
    }
  
    /**
     * Function to get the list of active Agents
     */
    public function getActiveAgents(){
        $res = User::find()->select(['id', 'firstName', 'lastName', 'email'])->where(['user_type' => 10, 'app_id' => '2'])->all();
        list($id, $firstName, $lastName, $email) = ['id','firstName', 'lastName', 'email'];
        $res = array_map(function($res) use($id, $firstName, $lastName, $email) {return [$id => $res->$id, $firstName => $res->$firstName.' '.$res->$lastName.' ( '.$res->$email.' )' ];}, $res);
        return ArrayHelper::map($res, $id, $firstName);           
    }
    
    public function getCurrencyData($id) {
        $data =  Currencies::find()->select(['id', 'code', 'symbol'])->where(['id' => $id])->one();
        if($data) return $data->code.' ('.$data->symbol.') ';
        echo '<pre>'; print_r($data); die('here');
    }    
  
    
    /* genrate Dropdown listData Dynamically */    
    public function getList($model, $cond, $columns){
        $res = $model::find()->select($columns)->where($cond)->all();
        list($id,$name) = $columns;
        $res = array_map(function($res) use($id,$name) {return [$id=>(string)$res->$id, $name=>$res->$name];}, $res);
        return ArrayHelper::map($res, $id, $name);
    }
    
    /* Function to get the Ownner Name*/
    public function getOwner($id){
        $result = Moderators::findOne($id);
        if($result['name']['firstname']){
            return $id = $result['name']['firstname'].' '.$result['name']['lastname'];
        }else{
            return $id = 'N-A';
        }
    }
        
    /* Function to get Bidding Status*/
    public function getBiddingStatus($type = NULL){
        $category = ['1' => 'No Online Bidding', '2' => 'Online Bidding', '3' => 'Offsite Bidding'];
        if($type) return isset($category[$type]) ? $category[$type] : 'N-A';
        return $category;
    }
    
    /* Function to get Auction Type*/
    public function getAuctionType_pre($type = NULL){
        $category = ['auction' => 'Auction', 'streaming' => 'Streaming', 'offline' => 'Offline'];
        if($type) return isset($category[$type]) ? $category[$type] : 'N-A';
        return $category;
    }
    
    /* Function to get Auction Type*/
    public function getAuctionType($type = NULL){
        $category = ['1' => 'For Sale', '2' => 'For Rent', '3' => 'For Lease'];
        if($type) return isset($category[$type]) ? $category[$type] : 'N-A';
        return $category;
    }
}