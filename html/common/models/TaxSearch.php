<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tax;

/**
 * IdSearch represents the model behind the search form about `common\models\Tax`.
 */
class TaxSearch extends Tax
{
   /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
		
			[['postcoderange'],'number', ], 
            [['created_at', 'updated_at','updated_by','created_by'], 'integer'],
            [['postcode','postcodeTo','id'], 'string'],
            [['country_id','state_id','rate'], 'safe'],
          
            
            
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'country_id' => Yii::t('app','Country_Id'),
            'state_id' => Yii::t('app','State_Id'),
            'postcoderange'=>Yii::t('app','Post_Code_Range'),
            'postcode'=>Yii::t('app','Post_Code'),
            'postcodeTo'=>Yii::t('app','Post-Code_To'),
            'rate'=>Yii::t('app','Rate'),
            'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
            'created_by'=>Yii::t('app','Created_By'),
            'updated_by'=>Yii::t('app','Updated_By'),
        ];
    }
    
    
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tax::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        
        $query->andFilterWhere([
            'country_id' => $this->country_id,           
            'state_id' => $this->state_id,           
            'postcoderange' => $this->postcoderange,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'created_by' => $this->created_by,
            
        ]);

        $query->andFilterWhere(['like', 'rate', $this->rate]);
        $query->andFilterWhere(['like', 'id', $this->id]);
        $query->andFilterWhere(['like', 'postcode', $this->postcode]);
        $query->andFilterWhere(['like', 'postcodeTo', $this->postcodeTo]);
       
            

        return $dataProvider;
    }
}
