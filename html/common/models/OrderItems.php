<?php

namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
class OrderItems extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
     return '{{%order_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id,order_id'], 'safe'],
      
        ];
    }
    
    
    
     public function attributeLabels()
    {
        return [
            
            'id' => Yii::t('app','ID'),
            'product_id' => Yii::t('app','Product Id'),
            'deal_id' => Yii::t('app','Deal ID'),
            'deal_Count' => Yii::t('app','Deal Count'),
            'isCombo' => Yii::t('app','IsCombo'),
            'qty' => Yii::t('app','Quantity'),
            'user_id' => Yii::t('app','User Id'),
            'order_id' => Yii::t('app','order Id'),
            'store_id' => Yii::t('app','Store Id'),
            'store_location_id' => Yii::t('app','Store Location Id'),
            'product_price' => Yii::t('app','Product Price'),
            'product_currency_code' => Yii::t('app','Product Currency Code'),
            'product_currency_id' => Yii::t('app','Product Currency Id'),
            'payment_currency_code' => Yii::t('app','Payment Purrency Code'),
            'payment_currency_id' => Yii::t('app','Payment Currency Id'),
            'exchange_rate' => Yii::t('app','Exchange Rate'),
            'product_exchange_price' => Yii::t('app','Product Exchange Price'),
            'tax' => Yii::t('app','Tax'),
            'shipping_charges' => Yii::t('app','Shipping Charges'),
             'discount' => Yii::t('app','Discount'),
             'total' => Yii::t('app','Total'),
             'payable' => Yii::t('app','Payable'),
             'status' => Yii::t('app','Status'),
            'created_at'=>Yii::t('app','Created At'),
            'Updated_at'=>Yii::t('app','Updated At'),
            'created_by'=>Yii::t('app','Created By'),
            'updated_by'=>Yii::t('app','Updated By'),
            'updated_by'=>Yii::t('app','Updated By'),
               
         
            ];
    }
    
    
    
    
    public function getOrdersubitems()
		{
			   return $this->hasMany(OrderSubItems::className(), ['order_id' =>'order_id','deal_id'=>'deal_id']); 
		}
		 public function getStore()
		{
			   return $this->hasMany(User::className(), ['id' =>'store_id']); 
		}
		
		 public function getProduct()
		{
			   return $this->hasOne(Product::className(), ['id' =>'product_id']); 
		}
		 public function getOrder()
		{
			   return $this->hasOne(Order::className(), ['id' =>'order_id']); 
		}
		
		  public function fields()
		{
			
			$fields=parent::fields();		  
			unset($fields['created_at']);
			unset($fields['updated_at']);
			unset($fields['created_by']);
			unset($fields['updated_by']);
			$fields['subitems'] = function ($model) {
            return $model->ordersubitems; // Return related model property, correct according to your structure
			};
			$fields['orderType'] = function ($model) {
            return $model->order->order_type; // Return related model property, correct according to your structure
			};
			$fields['product'] = function ($model) {
            return $model->product; // Return related model property, correct according to your structure
			};
			return $fields;
		}
		
		public function extraFields()
		{
			return [
			'ordersubitems',//=>['id','passUrl']
			

			];
		}
   
}
