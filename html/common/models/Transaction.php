<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\OrderItems;
use common\models\User;
use common\models\Workorders;

/**
 * Transaction model
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $bt_id 
 * @property integer $status
 * @property integer $created_at
 
 */
 
class Transaction extends ActiveRecord
{
   
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction_detail}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            [['order_id','user_id'],'required'],
        ];
    }


	public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'tid'=> Yii::t('app','T_Id'),
            'order_id'=>Yii::t('app','Order_Id'),
            'user_id'=>Yii::t('app','User_Id'),
            'status'=>Yii::t('app','Status'),
            'transaction_id'=>Yii::t('app','Transaction_Id'),
            'type'=>Yii::t('app','Type'),
            'currencyIsoCode'=>Yii::t('app','Currency_Iso_Code'),
            'amount'=>Yii::t('app','Amount'),
            'merchantAccountId'=>Yii::t('app','Merchant_Account_Id'),
            'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
        ];
    }







	    public function beforeSave($insert)
		{
				
			if (parent::beforeSave($insert)) {
			    	
			    	if($insert){			    		
			    		$this->user_id=Yii::$app->user->id;			    					    		
			    		$this->created_at= time();			    		
			    		
			     	}
			     	
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
		}
	
	/*
	*@avgTranscValue
	* return data of average transactions
	*/			
		
	public function avgTranscValue($unixDateStart,$userField){
		
	
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= Workorders::tableName().'.workorderpartner='.$userId;
			
		}else{
			
			$custQuery		= '';
			
		}
		
		$query  = new Transaction();
		
		$reslt 	= $query->find()
						->select("(sum(".Transaction::tableName().".amount)/count(".Transaction::tableName().".id)) AS avg_transc")
						->leftjoin(OrderItems::tableName(),OrderItems::tableName().'.order_id='.Transaction::tableName().'.order_id')
						->leftjoin(Workorders::tableName(),Workorders::tableName().'.id='.OrderItems::tableName().'.deal_id')
						->leftjoin(User::tableName(),User::tableName().'.id='.Workorders::tableName().'.workorderpartner')
						->Where(['>',Transaction::tableName().'.created_at', $unixDateStart])
						->andWhere($custQuery)
						->asArray()
						->one();
		
		return 	$reslt;
		
	}
   
}
