<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Qr;

/**
 * QrSearch represents the model behind the search form about `app\models\Qr`.
 */
class QrSearch extends Qr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['data', 'qrkey'], 'safe'],
        ];
    }
    
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'data' => Yii::t('app','Data'),
            'pin' => Yii::t('app','Pin for offline detail updation'),
            'qrImage' => Yii::t('app','QR code'),
            'qrkey' => Yii::t('app','Url Key'),
            'url'=> Yii::t('app','Pass Url'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_by' => Yii::t('app','Updated By'),
            'type' => Yii::t('app','Type'),
        ];
    }
    
    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Qr::find()->orderBy('id DESC');;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'qrkey', $this->qrkey]);

        return $dataProvider;
    }
}
