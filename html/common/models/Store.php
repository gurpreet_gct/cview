<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use common\models\Beaconmanager;
use backend\models\Storebeacons;
use backend\models\Dealstore;
use common\models\Country;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "tbl_store".
 *
 * @property integer $id
 * @property string $storename
 * @property string $address
 * @property string $street
 * @property string $city
 * @property string $state
 * @property integer $country
 * @property integer $owner
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Store extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%store}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'status', 'owner', 'created_at', 'updated_at', 'created_by', 'updated_by','addopeninghours'], 'integer'],
            [['storename', 'address', 'city'],'string', 'max' => 100],
            [['mondayopeninghoursAM','mondayopeninghoursPM','tuesdayopeninghoursAM','tuesdayopeninghoursPM','wednesdayopeninghoursAM','wednesdayopeninghoursPM','thursdayopeninghoursPM','thursdayopeninghoursAM','fridayopeninghoursAM','fridayopeninghoursPM','saturdayopeninghoursAM','saturdayopeninghoursPM','sundayopeninghoursAM','sundayopeninghoursPM'], 'string'],
             
             [['sundayFullDayOpen','sundayFullDayClose','mondayFullDayOpen','mondayFullDayClose','tuesdayFullDayOpen','tuesdayFullDayClose','wednesdayFullDayOpen','wednesdayFullDayClose','thursdayFullDayOpen','thursdayFullDayClose','fridayFullDayOpen','fridayFullDayClose','saturdayFullDayOpen','saturdayFullDayClose'],'safe'],
            //['storename','unique'],
             ['email','email'],
            # ['phoneNumber','required'],
             [['phoneNumber','countrycode','geo_radius'], 'safe'],
             [['storename', 'address', 'latitude','longitude','city','country', 'street','postal_code'], 'required'],
             ['latitude', 'match', 'pattern' => '/^[0-9.-]+$/', 'message' => 'Please enter valid latitude.'],
             ['longitude', 'match', 'pattern' => '/^[0-9.-]+$/', 'message' => 'Please enter valid longitude.'],
            [['street','latitude'], 'string', 'max' => 50],
            [['state','longitude'], 'string', 'max' => 60],
              [['postal_code'], 'string','max' => 100],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
            'storename' => Yii::t('app','Store Location Name'),
            'email' => Yii::t('app','Store Location Email (optional)'),
            'phoneNumber' => Yii::t('app','Store Location Phone Number (optional)'),
            'countrycode' => Yii::t('app','Country_Code'),
            'addopeninghours' => Yii::t('app','Add Store Location Opening Hours (optional)'),
            'address' => Yii::t('app','Store Location Address'),
            'street' => Yii::t('app','Street'),
            'city' => Yii::t('app','City'),            
            'state' => Yii::t('app','State'),
            'postal_code' => Yii::t('app','Postcode'),
            'latitude' => Yii::t('app','Latitude'),
            'longitude' => Yii::t('app','Longitude'),
            'country' => Yii::t('app','Country'),
            'owner' => Yii::t('app','Partner'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_by' => Yii::t('app','Updated By'),
			'mondayopeninghoursAM' =>Yii::t('app','Monday opening time') ,
			'mondayopeninghoursPM' =>Yii::t('app','Monday closing time'),
			'tuesdayopeninghoursAM' => Yii::t('app','Tuesday opening time'),
			'tuesdayopeninghoursPM' => Yii::t('app','Tuesday closing time'),
			'wednesdayopeninghoursAM' => Yii::t('app','Wednesday opening time'),
			'wednesdayopeninghoursPM' =>Yii::t('app','Wednesday closing time') ,
			'thursdayopeninghoursAM' => Yii::t('app','Thursday opening time'),
			'thursdayopeninghoursPM' => Yii::t('app','Thursday closing time'),
			'fridayopeninghoursAM' => Yii::t('app','Friday opening time'),
			'fridayopeninghoursPM' => Yii::t('app','Friday closing time'),
			'saturdayopeninghoursAM' => Yii::t('app','Saturday opening time'),
			'saturdayopeninghoursPM' => Yii::t('app','Saturday closing time'),
			'sundayopeninghoursAM' => Yii::t('app','Sunday opening time'),
			'sundayFullDayOpen' => Yii::t('app','Sunday_Full_Day_Open'),
			'sundayFullDayClose' => Yii::t('app','Sunday_Full_Day_Close'),
			'mondayFullDayOpen' => Yii::t('app','Monday_Full_Day_Open'),
			'mondayFullDayClose' => Yii::t('app','Monday_Full_Day_Close'),
			'tuesdayFullDayOpen' => Yii::t('app','Tuesday_Full_Day_Open'),
			'tuesdayFullDayClose' => Yii::t('app','Tuesday_Full_Day_Close'),
			'wednesdayFullDayOpen' =>Yii::t('app','Wednesday_Full_Day_Open'),
			'wednesdayFullDayClose' => Yii::t('app','Wednesday_Full_Day_Close'),
			'thursdayFullDayOpen' => Yii::t('app','Thursday_Full_Day_Open'),
			'thursdayFullDayClose' => Yii::t('app','Thursday_Full_Day_Close'),
			'fridayFullDayOpen' => Yii::t('app','Friday_Full_Day_Open'),
			'fridayFullDayClose' => Yii::t('app','Friday_Full_Day_Close'),
			'saturdayFullDayOpen' => Yii::t('app','Saturday_Full_Day_Open'),
			'saturdayFullDayClose' =>Yii::t('app','Saturday_Full_Day_Close') ,
			'geo_radius' => Yii::t('app','Add geolocation radius (in km) for food order'), 
        ];
    }
   
	public function fields()
	{
		$fields=parent::fields();
		unset($fields['owner']);
		unset($fields['status']);
		unset($fields['created_at']);
		unset($fields['updated_at']);
		unset($fields['created_by']);
		unset($fields['updated_by']);
		$fields['country']= function ($model) {
            return $model->countryname->name;
        };
		return $fields;
	}
	
	
	
     /* get the store status */
    public function getStatustype()
    {
    	  if($this->status==1){
         return 'Enable';    	  
    	  }else {
    	  return 'Disable';
       }
    }
    
    /* get the country name form Country model */
    public function getCountryname(){
		
		return $this->hasOne(Country::className(), ['id_countries' => 'country']);
	}
	 public function getMobileno(){
		 if(!empty($this->phoneNumber)){
			return '<div class="iti-flag '.$this->countrycode.'"></div>'.'<div class="mobile-no">'.$this->phoneNumber.'</div>';
	    }
	}
	
	/* get the owner name from the User model */
	 public function getOwnernameval(){
		
		$orgName =  Profile::find()->select('companyName')->where(['user_id' => $this->owner])->orderBy('id desc')->one();
		if(!empty($orgName->companyName)) {
				return $orgName->companyName;
			}else{
				return 'Not selected';
			}
		
	}
	public function getOpeninghours(){		
		if(isset($this->addopeninghours) && ($this->addopeninghours==1)){
			$message = '';
			if(!empty($this->mondayopeninghoursPM) && ($this->mondayopeninghoursAM)){
				$message .= 'Monday '.$this->mondayopeninghoursAM.' To '.$this->mondayopeninghoursPM.', ';
				}elseif($this->mondayFullDayOpen){
					$message .= 'Monday 24 Hours Open, ';
				}elseif($this->mondayFullDayClose){
					$message .= 'Monday Closed, ';
				}else{
					$message .= '';
			}
			if(!empty($this->tuesdayopeninghoursAM) && ($this->tuesdayopeninghoursPM)){
				$message .= 'Tuesday '.$this->tuesdayopeninghoursAM.' To '.$this->tuesdayopeninghoursPM.', ';
				}elseif($this->tuesdayFullDayOpen){
					$message .= 'Tuesday 24 Hours Open, ';
				}elseif($this->tuesdayFullDayClose){
					$message .= 'Tuesday Closed, ';
				}else{
					$message .= '';
			}
			if(!empty($this->wednesdayopeninghoursAM) && ($this->wednesdayopeninghoursPM)){
				$message .= 'Wednesday '.$this->wednesdayopeninghoursAM.' To '.$this->wednesdayopeninghoursPM.', ';
				}elseif($this->wednesdayFullDayOpen){
					$message .= 'Wednesday 24 Hours Open, ';
				}elseif($this->wednesdayFullDayClose){
					$message .= 'Wednesday Closed, ';
				}else{
					$message .= '';
			}
			if(!empty($this->thursdayopeninghoursAM) && ($this->thursdayopeninghoursPM)){
				$message .= 'Thursday '.$this->thursdayopeninghoursAM.' To '.$this->thursdayopeninghoursPM.', ';
				}elseif($this->thursdayFullDayOpen){
					$message .= 'Thursday 24 Hours Open, ';
				}elseif($this->thursdayFullDayClose){
					$message .= 'Thursday Closed, ';
				}else{
					$message .= '';
			}
			if(!empty($this->fridayopeninghoursAM) && ($this->fridayopeninghoursPM)){
				$message .= 'Friday '.$this->fridayopeninghoursAM.' To '.$this->fridayopeninghoursPM.', ';
				}elseif($this->fridayFullDayOpen){
					$message .= 'Friday 24 Hours Open, ';
				}elseif($this->fridayFullDayClose){
					$message .= 'Friday Closed, ';
				}else{
					$message .= '';
			}
			if(!empty($this->saturdayopeninghoursAM) && ($this->saturdayopeninghoursPM)){
				$message .= 'Saturday '.$this->saturdayopeninghoursAM.' To '.$this->saturdayopeninghoursPM.', ';
				}elseif($this->saturdayFullDayOpen){
					$message .= 'Saturday 24 Hours Open, ';
				}elseif($this->saturdayFullDayClose){
					$message .= 'Saturday Closed, ';
				}else{
					$message .= '';
			}
			if(!empty($this->sundayopeninghoursAM) && ($this->sundayopeninghoursPM)){
				$message .= 'Sunday '.$this->sundayopeninghoursAM.' To '.$this->sundayopeninghoursPM.'.';
				}elseif($this->sundayFullDayOpen){
					$message .= 'Sunday 24 Hours Open.';
				}elseif($this->sundayFullDayClose){
					$message .= 'Sunday Closed.';
				}else{
					$message .= '';
			}
		 
		 return $message; 
		
		}else{
				return 'Not selected';
		 }
		
		
	}
	
	
    /* get the created name from the User model */
    public function getCreateduser()
    {
    	 if(isset($this->created_by)){
			$user = User::find()->select('username')->where('id='.$this->created_by)->one();
			return isset($user->username) ? $user->username : '';
		
		}
	    
    }
    /* get the updated name from the User model */
    public function getUpdateduser()
    {
    	   if(isset($this->updated_by)){
			$user = User::find()->select('username')->where('id='.$this->updated_by)->one();
			return isset($user->username) ? $user->username : '';
		
		}
       
    }
  
	
	/* create relation with deal table table */
		public function getdealstore()
		{
			return $this->hasOne(Dealstore::className(), ['store_id' => 'id']);
			
			
		}
		
		
		/* create relation with store beacon table */
		public function getStorebeacons()
		{
			return $this->hasOne(Storebeacons::className(), ['store_id' => 'id']);
			
			
		}	
	    
    
    public function beforeSave($insert)
			{				
				
			    if (parent::beforeSave($insert)) {
			    	$this->updated_by= Yii::$app->user->id;			    	    		    
			    	$this->updated_at= strtotime($_COOKIE['username']);	
			    	if($this->addopeninghours!=1){
						$this->mondayopeninghoursAM = '';
						$this->mondayopeninghoursPM = '';
						$this->tuesdayopeninghoursAM = '';
						$this->tuesdayopeninghoursPM = '';
						$this->wednesdayopeninghoursAM = '';
						$this->wednesdayopeninghoursPM = '';
						$this->thursdayopeninghoursAM = '';
						$this->thursdayopeninghoursPM = '';
						$this->fridayopeninghoursAM = '';
						$this->fridayopeninghoursPM = '';
						$this->saturdayopeninghoursAM = '';
						$this->saturdayopeninghoursPM = '';
						$this->sundayopeninghoursAM = '';
						$this->sundayopeninghoursPM = '';
						$this->mondayFullDayOpen = 0;
						$this->mondayFullDayClose = 0;
						$this->tuesdayFullDayOpen = 0;
						$this->tuesdayFullDayClose = 0;
						$this->wednesdayFullDayOpen = 0;
						$this->wednesdayFullDayClose = 0;
						$this->thursdayFullDayOpen = 0;
						$this->thursdayFullDayClose = 0;
						$this->fridayFullDayOpen = 0;
						$this->fridayFullDayClose = 0;
						$this->saturdayFullDayOpen = 0;
						$this->saturdayFullDayClose = 0;
						$this->sundayFullDayOpen = 0;
						$this->sundayFullDayClose = 0;
					
				    }else{
							if(!empty($this->mondayFullDayOpen) || ($this->mondayFullDayClose)){
								$this->mondayopeninghoursAM = '';
								$this->mondayopeninghoursPM = '';
							}
							if(!empty($this->tuesdayFullDayOpen) || ($this->tuesdayFullDayClose)){
								$this->tuesdayopeninghoursAM = '';
								$this->tuesdayopeninghoursPM = '';
							}if(!empty($this->wednesdayFullDayOpen) || ($this->wednesdayFullDayClose)){
								$this->wednesdayopeninghoursAM = '';
								$this->wednesdayopeninghoursPM = '';
							}if(!empty($this->thursdayFullDayOpen) || ($this->thursdayFullDayClose)){
								$this->thursdayopeninghoursAM = '';
								$this->thursdayopeninghoursPM = '';
							}if(!empty($this->fridayFullDayOpen) || ($this->fridayFullDayClose)){
								$this->fridayopeninghoursAM = '';
								$this->fridayopeninghoursPM = '';
							}if(!empty($this->saturdayFullDayOpen) || ($this->saturdayFullDayClose)){
								$this->saturdayopeninghoursAM = '';
								$this->saturdayopeninghoursPM = '';
							}if(!empty($this->sundayFullDayOpen) || ($this->sundayFullDayClose)){
								$this->sundayopeninghoursAM = '';
								$this->sundayopeninghoursPM = '';
							}
					}
			    			  
			    	if($insert){
						$this->status= 1; 			    		
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= strtotime($_COOKIE['username']);
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}
}
