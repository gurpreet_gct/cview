<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\base\Model; 
use yii\data\ActiveDataProvider;
use common\models\Property;
use common\models\SqlDataProvider;
use yii\db\Expression;

/**
 * SendsmsSearch represents the model behind the search form about `common\models\Sendsms`.
 */
class PropertySearch extends Property
{
    const AUCTION_STATUS = ['inactive' => 0, 'auction' => 1, 'streaming' => 2, 'offline' => 3];
	public $property_type,$from_date,$to_date,$sale_from,$sale_to,$min_land,$max_land,$min_floor,$max_floor,$auction_only,$auction_all,$list,$sort,$lat,$long,$estate_agent_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [['id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['title', 'created_at', 'suburb', 'state', 'price', 'floorArea', 'landArea', 'status', 'property_type', 'from_date', 'to_date', 'sale_from', 'sale_to', 'min_land', 'max_land', 'min_floor', 'max_floor', 'auction_only', 'auction_all', 'list', 'property_type', 'sort', 'lat', 'long'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function actionDebug($arr){
    
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
        
    }  

    public function search_old($params)
    {
        $this->load($params);

        $currenttime=time();

        $dataProvider = (new \yii\db\Query())->from('tbl_properties');
        $lat = empty(Yii::$app->request->get('userlat')) ? '0' : Yii::$app->request->get('userlat');
        $long = empty(Yii::$app->request->get('userlong')) ? '0' : Yii::$app->request->get('userlong');
        
        // return $dataProvider->select("*, tbl_properties.title as distance")->all();
        $dataProvider->select("*, (((acos(sin((".$lat."*pi()/180)) * sin((".$lat."*pi()/180))+cos((".$lat."*pi()/180)) * cos((".$lat."*pi()/180)) * cos(((".$long."- `longitude`)* pi()/180))))*180/pi())*60*1.1515) as distance,((datetime_start-$currenttime)/3600) as within24hours");

        //
        $dataProvider->leftJoin('tbl_auction_event', 'tbl_auction_event.listing_id = tbl_properties.id')->all();
        ////

        // within 24 hours sorting 

        if(!empty(Yii::$app->request->get('nearfuture'))){
            $dataProvider = $dataProvider->having(['<','within24hours',24]); // to be continued
        }


        if(!empty(Yii::$app->request->get('status'))){
            $dataProvider = $dataProvider->andWhere(['tbl_properties.status'=>Yii::$app->request->get('status')]);
        }

        if(!empty(Yii::$app->request->get('landArea'))){
             $dataProvider = $dataProvider->andWhere(['tbl_properties.landArea'=>Yii::$app->request->get('landArea')]);
          }
        if(!empty(Yii::$app->request->get('floorArea'))){
             $dataProvider = $dataProvider->andWhere(['tbl_properties.floorArea'=>Yii::$app->request->get('floorArea')]);
          }
        if(!empty(Yii::$app->request->get('price'))){
             $dataProvider = $dataProvider->andWhere(['tbl_properties.price'=>Yii::$app->request->get('price')]);
          }
        if(!empty(Yii::$app->request->get('propertyType'))){
             $dataProvider = $dataProvider->andWhere(['tbl_properties.propertyType'=>Yii::$app->request->get('propertyType')]);
         }
        if(!empty(Yii::$app->request->get('state'))){
             $dataProvider = $dataProvider->andWhere(['tbl_properties.state'=>Yii::$app->request->get('state')]);
         }
        if(!empty(Yii::$app->request->get('suburb'))){ 
            $dataProvider = $dataProvider->andWhere(['tbl_properties.suburb'=>Yii::$app->request->get('suburb')]);
         }
        if(!empty(Yii::$app->request->get('created_at'))){ 
            $dataProvider = $dataProvider->andWhere(['tbl_properties.created_at'=>Yii::$app->request->get('created_at')]);
         }
        // price sort
        if(!empty(Yii::$app->request->get('price_sort')))
        {
            if(Yii::$app->request->get('price_sort')==0)
            {
                $dataProvider = $dataProvider->orderBy([
                'tbl_properties.price'=>SORT_ASC,
                 ]);
            }
            else
            {
                $dataProvider = $dataProvider->orderBy([
                'tbl_properties.price'=>SORT_DESC,
                ]);
            }
            
        }
        // date sort
        if(!empty(Yii::$app->request->get('date')))
        {
            if(Yii::$app->request->get('date')==0)
            {
                $dataProvider = $dataProvider->orderBy([
                'tbl_properties.created_at'=>SORT_ASC,
                 ]);
            }
            else
            {
                $dataProvider = $dataProvider->orderBy([
                'tbl_properties.created_at'=>SORT_DESC,
                ]);
            }
            
        }
        // location sort
        if(!empty(Yii::$app->request->get('userlat')) && !empty(Yii::$app->request->get('userlong')) && !empty(Yii::$app->request->get('location')))
        { 
            if(Yii::$app->request->get('location')==0)
            {
                $dataProvider = $dataProvider->orderBy([
                'distance'=>SORT_ASC,
                 ]);
            }
            else
            { 
                $dataProvider = $dataProvider->orderBy([
                'distance'=>SORT_DESC,
                ]);
            }
        }


       
       
        $dataProvider = $dataProvider->limit(10);

        
        $dataProvider=$dataProvider->all();

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
                    
        return $dataProvider;
    }

    public function search($params)
    {   
        $expression = new Expression('latitude');
		if(!empty($params['PropertySearch']['lat']) && !empty($params['PropertySearch']['long'])){
			$lat = $params['PropertySearch']['lat'];
			$long = $params['PropertySearch']['long'];
			$exp=sprintf('(6371 * acos (cos ( radians(%s) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(%s) ) + sin ( radians(%s) ) * sin( radians( latitude ) ) )) as distance',$lat,$long,$lat);
			$expression = new Expression($exp);
		}
        $query = Property::find()->select(Property::tableName().'.*,'.$expression)->joinWith('auction')->joinWith('city')->joinWith('state')->joinWith('bidding')
        //->all()
        ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        
       // echo '<pre>'; print_r($dataProvider); die('here');
        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params) 
         * statement below
         */

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'default' => SORT_DESC
                ],
                //~ 'distance' => [
                    //~ 'default' => SORT_DESC
                //~ ],
                'title' => [
                    'default' => SORT_ASC
                ],
                'description' => [
                    'default' => SORT_ASC
                ],
                'landArea' => [
                    'default' => SORT_ASC
                ],
                'floorArea' => [
                    'default' => SORT_ASC
                ],
                'streetName' => [
                    'default' => SORT_ASC
                ],
                'suburb' => [
                    'default' => SORT_ASC
                ],
                'city' => [
                    'default' => SORT_ASC
                ],
                'state' => [
                    'default' => SORT_ASC
                ],
                'bedrooms' => [
                    'default' => SORT_ASC
                ],
                'bathrooms' => [
                    'default' => SORT_ASC
                ],
                'price' => [
                    'default' => SORT_ASC
                ]
            ]
        ]);
        
		$this->load($params);
		if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // filter by property type
        if (!empty ($this->property_type) ) {
            $paramPropertyType = explode(",",$this->property_type);
            if(count($paramPropertyType)>1){
				foreach($paramPropertyType as $type)
				$query->joinWith('propertyTypename')->orFilterWhere(['like','tbl_property_types.name' ,  $type]);
            }else{
				$query->joinWith('propertyTypename')->andFilterWhere(['like','tbl_property_types.name' ,  $this->property_type]);
			}
        }
        
        // filter by sale price
        if (!empty($this->sale_from)) {
            $query->andFilterWhere(['>=', 'price', $this->sale_from]);
        }

        if (!empty($this->sale_to)) {
            $query->andFilterWhere(['<=', 'price', $this->sale_to]);
        }
        // filter by start date
        if(!empty($this->from_date) && !empty($this->to_date) && ($this->from_date == $this->to_date)){
			 if ($this->validate_date_format($this->from_date)) {
                // $from_date = date_create_from_format("Y-m-d", $params['from_date']);
                $from_date = strtotime($this->from_date);
                $query->andFilterWhere(['=', 'tbl_auction_event.date', $from_date]);
            }
		}else{
        if (!empty($this->from_date)) {
            // Validate date format befoer processing
            if ($this->validate_date_format($this->from_date)) {
                // $from_date = date_create_from_format("Y-m-d", $params['from_date']);
                $from_date = strtotime($this->from_date);
                $query->andFilterWhere(['>=', 'tbl_auction_event.datetime_start', $from_date]);
            }
        }

        if (!empty($this->to_date)) {
            // Validate date format befoer processing
            if ($this->validate_date_format($this->to_date)) {
                // $from_date = date_create_from_format("Y-m-d", $params['from_date']);
                $to_date = strtotime($this->to_date);
                $query->andFilterWhere(['<=', 'tbl_auction_event.datetime_start', $to_date]);
            }
        }
		}
        // Min and max land
		 if (!empty($this->min_land)) {
            $query->andFilterWhere(['>=', 'landArea', $this->min_land]);
        }

        if (!empty($this->max_land)) {
            $query->andFilterWhere(['<=', 'landArea', $this->max_land]);
        }
        // Min and max land
        if (!empty($this->min_floor)) {
            $query->andFilterWhere(['>=', 'floorArea', $this->min_floor]);
        }

        if (!empty($this->max_floor)) {
            $query->andFilterWhere(['<=', 'floorArea', $this->max_floor]);
        }

		if (!empty($this->auction_only)) {
            $all = false;
            if (!empty($this->auction_all) && intval($this->auction_all) == 1) {
                $all = true;
            }
            if (intval($this->auction_only) == 1 && !$all) {
                $query->andFilterWhere(['tbl_auction_event.auction_status' => self::AUCTION_STATUS['auction']]);
            }
        }
		
        if(!empty($this->list)){
            $currentDateTime = strtotime(date('Y-m-d H:i:s',time()));
            if (intval($this->list) == 1) {  // past properties
                // If auction_only requested
                $query->andFilterWhere(['<','tbl_auction_event.datetime_start',($currentDateTime-86400)]);
                if ( !empty($this->auction_only) && intval($this->auction_only) == 1)
                {
                    $query->andFilterWhere(['tbl_auction_event.auction_status' => self::AUCTION_STATUS['auction']]);
                }
                $query->orderBy(Property::tableName().'.id desc');
            } elseif (intval($this->list) == 2){ // upcoming property
                $query->andFilterWhere(['>','tbl_auction_event.datetime_start',$currentDateTime]);
                if (!empty($this->auction_only) && intval($this->auction_only) == 1)
                {
                    $query->andFilterWhere(['tbl_auction_event.auction_status' => self::AUCTION_STATUS['auction']]);
                }
                $query->orderBy(Property::tableName().'.id desc');
            }
        }
		if(!empty($this->status)){
			$query->andFilterWhere(['=', Property::tableName().'.status', $this->status]);
		}
        if (!empty($this->suburb)) {
            $query->orWhere(['LIKE', 'suburb', $this->suburb]);
            $query->orWhere(['LIKE', 'tbl_cview_states.name', $this->suburb]);
            $query->orWhere(['LIKE', 'tbl_city.name', $this->suburb]);
            $query->orWhere(['LIKE', 'postcode', $this->suburb]);
        }
        
        if (!empty($this->state) && ($this->state != 0)) {
                $query->andFilterWhere(['=', 'state', $this->state]);
        }	
        /*if(!empty($this->agency_id))
			$query->andFilterWhere(['=', 'agency_id', $this->agency_id]);

        if(!empty($this->estate_agent_id))
            $query->andFilterWhere(['=', 'estate_agent_id', $this->estate_agent_id]);*/

        // Sort incase of datetime_start, stored in relation table
        if (!empty($this->sort)) {
            $sort_requested = explode(',', $this->sort);
            //print_r($sort_requested);die;
            foreach ($sort_requested as $value) {
                if (strpos($value, 'date') !== false) {
                    if ($value[0] == '-' && $this->list == 2) {
                        $query->orderBy(['tbl_auction_event.datetime_start' => SORT_DESC]);
                    } elseif($value[0] == '-' && $this->list == 1) {
                        $query->orderBy(['tbl_auction_event.datetime_start' => SORT_ASC]);
                    } else{
						if($this->list == 2)
						$query->orderBy(['tbl_auction_event.datetime_start' => SORT_ASC]);
						else
						$query->orderBy(['tbl_auction_event.datetime_start' => SORT_DESC]);
					}
                    break;
                }
                if (isset($this->lat) && isset($this->long)) {
                    if (strpos($value, 'distance')  !== false){
    					if($value[0] == '-')
    					$query->orderBy(['distance'=>SORT_DESC]);
    					else
    					$query->orderBy(['distance'=>SORT_ASC]);
    				}
                }
                if (strpos($value, 'price')  !== false){
    					if($value[0] == '-')
    					$query->orderBy(['price'=>SORT_DESC]);
    					else
    					$query->orderBy(['price'=>SORT_ASC]);
    				}
            }
            
        }

        return $dataProvider;
    }

    public function validate_date_format($date_string="1900-01-23")
    {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date_string)) {
            return true;
        } else {
            return false;
        }
    }

    public function testSearch($params)
    {
        $query = Property::find();
        $dataProvider = new ActiveDataProvider([
           'query' => $query
        ]);

        /**
        * Setup your sorting attributes
        * Note: This is setup before the $this->load($params) 
        * statement below
        */
        /*$dataProvider->sort->attributes['title'] = [
           'default' => SORT_DESC
        ];*/

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'default' => SORT_DESC
                ],
                'title' => [
                    'default' => SORT_ASC
                ],
                'description' => [
                    'default' => SORT_ASC
                ],
                'landArea' => [
                    'default' => SORT_ASC
                ],
                'floorArea' => [
                    'default' => SORT_ASC
                ],
                'streetName' => [
                    'default' => SORT_ASC
                ],
                'suburb' => [
                    'default' => SORT_ASC
                ],
                'city' => [
                    'default' => SORT_ASC
                ],
                'state' => [
                    'default' => SORT_ASC
                ],
                'bedrooms' => [
                    'default' => SORT_ASC
                ],
                'bathrooms' => [
                    'default' => SORT_ASC
                ],
                'auction.datetime_start' => [
                    'default' => SORT_ASC
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
           return $dataProvider;
        }
        return $dataProvider;
    }
}
