<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%foodnotification}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Foodnotification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%foodnotification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function beforeSave($insert)
		{
		    if (parent::beforeSave($insert)) {			    	
		    	$this->updated_at= time();				
		    	if($insert){			    		
		    		$this->created_at= time();
		     	}
		    
		    	return true;
		    } else {
		    	
		     return false; 
		    }
		}
}
