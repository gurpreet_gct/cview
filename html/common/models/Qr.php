<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
use yii\web\urlManager;
use yii\helpers\BaseUrl;
/**
 * This is the model class for table "tbl_qrkit".
 *	
 * @property integer $id
 * @property string $data
 * @property string $qrkey
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Qr extends \yii\db\ActiveRecord
{
	public $qrImage="",$url='';
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%qrkit}}';
    }
    
 
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
          	[['data', 'status','type'], 'required'],
          	 ['qrkey','unique'],
            [['data','qrkey'], 'string', 'max' => 255],            
            [['pin'], 'string', 'max' => 14,'min'=>4],            
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'data' => Yii::t('app','Data'),
            'pin' => Yii::t('app','Pin for offline detail updation'),
            'qrImage' => Yii::t('app','Image'),
            'qrkey' => Yii::t('app','Url Key'),
            'url'=> Yii::t('app','Pass Url'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_by' => Yii::t('app','Updated By'),
            'type' => Yii::t('app','Type'),
        ];
    }
    
    
    /* get user status type */
    public function getStatustype()
    {
    	  if($this->status==1){
         return 'Enable';    	  
    	  }else {
    	  return 'Disable';
       }
    }
    
    /* get created user name from User model */
    public function getCreateduser()
    {
    	  return $this->hasOne(User::className(), ['id' => 'created_by']);
       
    }
    
    /* get the updated name form the User model */
     public function getUpdateduser()
    {
    	  return $this->hasOne(User::className(), ['id' => 'updated_by']);
       
    }
    
    
 	public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
			    	if($insert){
						$this->qrkey=md5("swp@r".time()."$#%");			    		
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}
	
	public function afterFind()
	{
		parent::afterFind();
		
		if($this->type)
		{
			if($this->type==1){				
				$this->qrImage=Yii::$app->urlManagerPassKit->createAbsoluteUrl($this->qrkey);
				$this->url=Yii::$app->urlManagerPassKit->createAbsoluteUrl($this->qrkey);
			}elseif($this->type==0){
				$this->qrImage=Yii::$app->urlManagerQrKit->createAbsoluteUrl($this->qrkey);
				$this->url=Yii::$app->urlManagerQrKit->createAbsoluteUrl($this->qrkey);
			}
	    }
	    else
	    {
		  $this->qrImage=Yii::$app->urlManagerQrKit->createAbsoluteUrl($this->qrkey);
		  $this->url=Yii::$app->urlManagerQrKit->createAbsoluteUrl($this->qrkey);
			
			}
		
	}	  
    
}
