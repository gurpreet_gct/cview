<?php

namespace common\models;
use common\models\Paymentcardtype;
use common\models\User;
use Yii;

/**
 * This is the model class for table "tbl_walletids".
 *
 * @property integer $id
 * @property integer $cardType
 * @property string $nameOnCard
 * @property string $cardNumber
 * @property integer $expiry
 * @property integer $cvv
 * @property integer $created_at
 * @property integer $updated_at
 */
class Paymentcard extends \yii\db\ActiveRecord
{

     /**
     * @inheritdoc
     */
   public $nonce='';

    public static function tableName()
    {
        return '{{%paymentMethods}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
		[['nonce','cardholderName'],'safe'],
        [['user_id','maskedNumber','expirationMonth','expirationYear','cardholderName','bin'],'required'],
        ['maskedNumber', 'string', 'length' => [14, 16],'message' => "Please enter  valid card number"],
        ['maskedNumber', 'match', 'pattern' => '/^[0-9]*$/i','message' => "Please enter valid card number"],
        ['bin', 'string', 'length' => [3, 3],'message' => "Please enter  valid cvv number"],
        ['bin', 'match', 'pattern' => '/^[0-9]*$/i','message' => "Please enter valid cvv number"],
        ['cardholderName','cardholderNameValidate'],
        ];
    }
/*	public function fields()
	{
		$fields=parent::fields();
		return [ "id",
        "user_id",
        "bin",
        "last4",
        "cardType",
        "expirationMonth",
        "expirationYear",
        "customerLocation",
        "cardholderName",
        "imageUrl",
        "venmoSdk",
        "expirationDate",
        "maskedNumber",
        "systemToken"];

	}*/
    /**
     * @inheritdoc
     */
		public function attributeLabels()
		{
			return [
				'id' => Yii::t('app','ID'),
				'cardType' => Yii::t('app','Card type'),
				'user_id' => Yii::t('app','Partner'),
				'cardholderName' => Yii::t('app','Name on Card'),
				'maskedNumber' => Yii::t('app','Card Number'),
				'expirationDate' => Yii::t('app','Expiry Date'),
        'expirationMonth' => Yii::t('app','Expiry Month'),
        'expirationYear' => Yii::t('app','Expiry Year'),
				'bin' => Yii::t('app','CVV'),
				'created_at'=>Yii::t('app','Created At'),
				'updated_at'=>Yii::t('app','Updated At'),
			];
		}


	public function cardholderNameValidate($attribute, $params){
		if(str_word_count($this->cardholderName)<2){
			$this->addError('cardholderName', 'Please enter valide card holder name ex. John Smith');
		}
	}
	public function validatecard($attribute, $params){

		if($this->maskedNumber!='' && $this->isNewRecord){
			$userId = Yii::$app->user->identity->id;
			$data = $this::find()->where(['user_id'=>$userId,'maskedNumber'=>$this->maskedNumber])->all();
			if($data){
				$this->addError('maskedNumber', 'Entered card is already exist');
			}
		}

	}
    /*
    public function getCardtype()
    {
          return $this->hasOne(Paymentcardtype::className(), ['id' => 'cardType']);
    }
    */
	public function getuser(){
		return $this->hasOne(User::className(),['id'=>'user_id']);
	}
		public function beforeSave($insert)
				{
					if (parent::beforeSave($insert)) {
						$this->updated_at= time();
						$this->systemToken=md5(time()."paras" .Yii::$app->security->generateRandomString());
							$this->expirationDate=$this->expirationMonth.'/'.$this->expirationYear;
					
						if($insert){
              $this->created_by= Yii::$app->user->identity->id;
							$this->created_at= time();
						}


						return true;
					} else {

					 return false;
					}
				}

			public function afterFind()
			{
				parent::afterFind();
				$this->created_at=date("Y-m-d",$this->created_at);
                $this->updated_at=date("Y-m-d",$this->updated_at);
				return true;
			}
  /* get the partner name name from  profile model */
     public function getUsername() {
  		if($this->user_id!='') {
  		    $lowner = \common\models\User::find()
  		    ->select("IFNULL(orgName,`fullName`)  fullName")
  		    ->where(['id' =>$this->user_id])->one();
  		      if($lowner){
  			         return $lowner->fullName;
  		      }
           
  	  }
  	   return 'N/A';
  	}
}
