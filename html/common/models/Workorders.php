<?php

namespace common\models;
use common\models\Categories;
use common\models\User;
use common\models\Profile;
use common\models\Country;
use backend\models\Beacongroup;
use api\modules\v1\models\UserStore;
use backend\models\Beaconlocation;
use common\models\Workorderpopup;
use api\modules\v1\models\Cart;
use yii\helpers\ArrayHelper;


use Yii;

/**
 * This is the model class for table "tbl_workorders".
 *
 * @property integer $id
 * @property string $campaign_name
 * @property string $campaign_type
 * @property string $country_name
 * @property string $media_contract_number
 * @property string $number_of_pass
 * @property integer $total_amount
 * @property integer $campain_duration
 * @property string $date_from
 * @property string $date_to
 * @property string $campaign_expires
 * @property string $users_type
 * @property string $aged_group
 * @property string $interested_in
 * @property string $locations
 * @property string $beacon_group
 */


class Workorders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $qrtitle;
    
   /* public $returneduser;
    public $timeofday1;
    public $userbefore;
    public $timeofday;
    public $userafter; */
    public $offerTitle; 
    public $savetype;

    public static function tableName()
    {
        return '{{%workorders}}';
    }

    /**
     * @inheritdoc
     */
   public function rules()
    {
        return [
          
            [['geoProximity', 'name','userType','agedGroup', 'countryID', 'workorderpartner','workorderType', 'noPasses',  'duration','dateFrom', 'dateTo','returnWithin'], 'required'],
            ['qrtitle','required', 'on' => 'qrdown'],
           [['temperaturestart','rulecondition','usereturned','userenter','temperaturend','dayparting','temperature','daypartingcustom1end','daypartingcustom2end','daypartingcustom3end','daypartingcustom1','daypartingcustom2','daypartingcustom3','behaviourvisit1','behaviourvisit2','behaviourvisit3','purchaseHistory1','purchaseHistory2','purchaseHistory3','behaviourDwell','Loyalty1','Loyalty2','Loyalty3','behaviourDwellMinutes','buynow'], 'safe'],
			['dateTo', 'compare','compareAttribute'=>'dateFrom','operator'=>'>=','message'=>'End date should be greater than  from start date.'],
			[['type', 'duration','workorderpartner', 'workorderType','status','noPasses'], 'integer'],
            [['name'], 'string', 'max' => 30],
            [['countryID', 'mediaContractNumber', 'noPasses'], 'string', 'max' => 100],
            [['expires'], 'string', 'max' => 50],
            [['allowpasstouser','qtyPerPass','offerTitle'], 'safe'],
            
            [['beaconGroup'], 'required','when' => function($model) {
					if($model->geoProximity==1 || $model->geoProximity==2)
						return true;
					
				},'whenClient' => "function (attribute, value) { 				
					var wtval = $('#workorders-geoproximity').val(); 
					if(wtval==2 || wtval==1){						
						return true; 						
					}else {						
						return false; 
					}
				}"],
				/*['catIDs','required','message'=>'Categories cannot be blank.','when'=>function($model){
					return $model->catIDs == ""? true:false;
				},'whenClient'=>"function(attribute,value){
				var val = $('ul.chosentree-choices').children('li').length;
				if(val>1){
					return false;
				}else{
					return true;
				}
				}"
				],*/
            [['daypartingcustom1','daypartingcustom1end'], 'required','when' => function($model) {
					if($model->dayparting==3)
						return true;
					
				},'whenClient' => "function (attribute, value) { 
				
					var wtval = $('input:radio[name=\"Workorders[dayparting]\"]:checked').val(); 
					 
					if(wtval==3){						
						return true; 						
					}else {						
						return false; 
					}
            }"],
            
            [['temperaturestart','temperaturend'], 'required','when' => function($model) {
					if($model->temperature==2)
						return true;
					
				},'whenClient' => "function (attribute, value) { 
				
					var wtval = $('input:radio[name=\"Workorders[temperature]\"]:checked').val(); 
					
					if(wtval==2){						
						return true; 						
					}else {						
						return false; 
					}
            }"],
            
            [['Loyalty1','Loyalty2','Loyalty3'], 'required','when' => function($model) {
					if(($model->Loyalty1 !='') && ($model->Loyalty2 !='') && ($model->Loyalty3 !='') )
						return true;
					
				},'whenClient' => "function (attribute, value) { 
				
				 var loyalty1= $('#workorders-loyalty1').val(); 			
				 var loyalty2= $('#workorders-loyalty2').val(); 			
				 var loyalty3= $('#workorders-loyalty3').val(); 			
										
				 if(loyalty1!=''){						
						return true; 						
					}else {						
						return false; 
					}
				if(loyalty2!=''){						
						return true; 						
					}else {						
						return false; 
					}
				if(loyalty3!=''){						
						return true; 						
					}else {						
						return false; 
					}
            }"],
            
            [['purchaseHistory1','purchaseHistory2','purchaseHistory3'], 'required','when' => function($model) {
					if(($model->purchaseHistory1 !='') && ($model->purchaseHistory2 !='') && ($model->purchaseHistory3 !='') )
						return true;
					
				},'whenClient' => "function (attribute, value) { 
				
				 var purchasehistory1= $('#workorders-purchasehistory1').val(); 			
				 var purchasehistory2= $('#workorders-purchasehistory2').val(); 			
				 var purchasehistory3= $('#workorders-purchasehistory3').val(); 			
										
				 if(purchasehistory1!=''){						
						return true; 						
					}else {						
						return false; 
					}
				if(purchasehistory2!=''){						
						return true; 						
					}else {						
						return false; 
					}
				if(purchasehistory3!=''){						
						return true; 						
					}else {						
						return false; 
					}
            }"],
            
            [['behaviourvisit1','behaviourvisit2','behaviourvisit3'], 'required','when' => function($model) {
					if(($model->behaviourvisit1 !='') && ($model->behaviourvisit2 !='') && ($model->behaviourvisit3 !='') )
						return true;
					
				},'whenClient' => "function (attribute, value) { 
				
				 var behaviourvisit1= $('#workorders-behaviourvisit1').val(); 			
				 var behaviourvisit2= $('#workorders-behaviourvisit2').val(); 			
				 var behaviourvisit3= $('#workorders-behaviourvisit3').val(); 			
										
				 if(behaviourvisit1!=''){						
						return true; 						
					}else {						
						return false; 
					}
				if(behaviourvisit2!=''){						
						return true; 						
					}else {						
						return false; 
					}
				if(behaviourvisit3!=''){						
						return true; 						
					}else {						
						return false; 
					}
            }"],
            
               [['behaviourDwell','behaviourDwellMinutes'], 'required','when' => function($model) {
					if(($model->behaviourDwell !='') && ($model->behaviourDwellMinutes !=''))
						return true;
					
				},'whenClient' => "function (attribute, value) { 
				
				 var behaviourdwell1= $('#workorders-behaviourdwell').val(); 			
				 var behaviourdwell2= $('#workorders-behaviourdwellminutes').val(); 			
						
										
				 if(behaviourdwell1!=''){						
						return true; 						
					}else {						
						return false; 
					}
				if(behaviourdwell2!=''){						
						return true; 						
					}else {						
						return false; 
					}
				
            }"],
            
           
            
            
           
         
        ];
    }	
	
    public function attributeLabels()
    {
        return [
             'id' => Yii::t('app','Id'),
            'name' => Yii::t('app','Campaign Name'),
            'type' => Yii::t('app','Campaign Type'),
            'countryID' => Yii::t('app','Select Country'),           
            'resellerID' => Yii::t('app','Reseller'),
            'mediaContractNumber' => Yii::t('app','Media Contract Number / PO Number'),
            'noPasses' => Yii::t('app','Number of Passes'),
            'allowpasstouser' => Yii::t('app','Select how many times the pass can be used'),
            'workorderpartner' => Yii::t('app','Select Partner'),
            'duration' => Yii::t('app','Duration'),
            'dateFrom' => Yii::t('app','Deal Availability Start Date'),
            'dateTo' => Yii::t('app','Deal Availability End Date'),
            'expires' => Yii::t('app','Campaign Expires'),
            'userType' => Yii::t('app','User Type'),
            'agedGroup' => Yii::t('app','Age Group'),
            'buynow' => Yii::t('app','Tick if this item will be available for purchase in-app?'),
            'beaconGroup' => Yii::t('app','Select Beacon Group'),
            'qrtitle' => Yii::t('app','Enter Text for QR image'),
            'workorderType' => Yii::t('app','Workorder Type'),
            'mediaContractNumber' => Yii::t('app','Media Contract Number (optional)'),
            'usedPasses' => Yii::t('app','Used Passes'),
            'qtyPerPass' => Yii::t('app','Select maximum of items per pass'),
            'catIDs' => Yii::t('app','Cat IDs'),
            'uniqueID' => Yii::t('app','Unique ID'),
            'status' => Yii::t('app','Status'),
            'created_at' => Yii::t('app','created at'),
			'updated_at' => Yii::t('app','updated at'),
			'created_by' => Yii::t('app','created by'),
			'updated_by' => Yii::t('app','updated by'),
			'dayparting' => Yii::t('app','Day Parting'),
			'daypartingcustom1' => Yii::t('app','Day Parting One'),
			'daypartingcustom2' => Yii::t('app','Day Parting Two'),
			'temperature' => Yii::t('app','Temperature'),
			'temperaturestart' => Yii::t('app','Temperature Start'),
			'temperaturend' => Yii::t('app','Temperature End'),
			'daypartingcustom1end' => Yii::t('app','Day Parting One End'),
			'daypartingcustom2end' => Yii::t('app','Day Parting Two End'),
			'daypartingcustom3end' => Yii::t('app','Day Parting Three End'),
			'rulecondition' => Yii::t('app','Tick to select the rules and conditions for this deal'),
			'behaviourvisit1' => Yii::t('app','Behaviour Visit One'),
			'behaviourvisit2' => Yii::t('app','Behaviour Visit Two'),
			'behaviourvisit3' => Yii::t('app','Behaviour Visit Three'),
			'behaviourDwell' => Yii::t('app','Behaviour_Dwell'),
			'purchaseHistory1' => Yii::t('app','Purchase History One'),
			'purchaseHistory3' => Yii::t('app','Purchase History Three'),
			'Loyalty1' => Yii::t('app','Loyalty value one'),
			'Loyalty2' => Yii::t('app','Loyalty value two'),
			'Loyalty3' => Yii::t('app','Loyalty value three'),
			'purchaseHistory2' => Yii::t('app',''),
			'usereturned' => Yii::t('app','Use_Returned'),
			'userenter' => Yii::t('app','User_Enter'),
			'behaviourDwellMinutes' => Yii::t('app','Behaviour_Dwell_Minutes'),
			'offerTitle' => Yii::t('app','Offer Title'),
			'geoProximity' => Yii::t('app','Set Geolocation or Proximity'),
			'returnWithin' => Yii::t('app','Return period in days from purchase day'),
        ];
    }
    /* get categories name form Categories Model */
   
	 /* get country name form Country Model */
	public function getCountryname(){
		
		return $this->hasOne(Country::className(), ['id_countries' => 'countryID']);
	}
	 /* get geo Proximity name */
	public function getGeoproximityname(){
	
		if(isset($this->geoProximity)){
			if($this->geoProximity==1){
				return 'Both';
			}elseif($this->geoProximity==2){
				return 'Proximity';
			}elseif($this->geoProximity==3){
				return 'Geolocation';
			}else{
				return 'not set';
			}
		}else{
			return 'not set';
		}
	
	}
	
	
	/*get partner for workorder*/	
	public function getPartnername(){
		
		return $this->hasOne(User::className(), ['id' => 'workorderpartner']);
	}
	
	public function getPartnernameval(){
		
		if(!empty($this->workorderpartner)){
		
			$partnerName =  User::findOne(['id' =>$this->workorderpartner]);
			if($partnerName){
				return $partnerName->orgName; 
				
				}
			else{
				return 'N/A';
			}
 		
		}
	}

	
	
	
		
	/* get offer title from beacon */
	
	public function getOffertitlename(){
		
		$offer = Workorderpopup::find()->select('offertitle')->Where('workorder_id IN('.$this->id.')')->asArray()->all();	
		
		foreach($offer as $offertitle)	{
				
		$offer_title = 	ucfirst($offertitle['offertitle']);
		
		return $offer_title ;
			
		}		
	}
	public function getDealid(){
		
		$deal = Deal::find()->select('id')->Where('workorder_id IN('.$this->id.')')->asArray()->one();	
		
		if(isset($deal['id'])){
			return true;
		}
		else {
			return false;
		}
	}
/* check if deal has value */
public function getDealuploadval(){
		
		$deal = Deal::find()->select('dealuploadTypes')->Where('workorder_id IN('.$this->id.')')->asArray()->all();	
		
		foreach($deal as $_deal)	{
				
		$offer_title = 	ucfirst($_deal['dealuploadTypes']);
		
		return $offer_title ;
			
		}		
	}
	


	
	public function getCampaigntypename(){
		
		
	
		
	/* check is workorder or PSA */
			if($this->workorderType == 1){
				if($this->type==1){
					return 'Voucher';
				}elseif($this->type==2){
					return 'Digital Stamp Card';
				}elseif($this->type==3){
					return 'Multi-media card';
				}elseif($this->type==4){
					return 'Event ticket';
				}elseif($this->type==5){
					return 'Go to store';
				}else{
				  return '';
				}
				
		    }elseif($this->workorderType == 2){
				if($this->type==1){
					return 'PSA info';
				}elseif($this->type==2){
					return 'PSA deal';
				}else{
					return '';
				}
			}else{
				return '';
			}
	       	
	
}
	
	
	
	public function getTemparturevalue(){
		
		if($this->temperature==1) {
			return 'All';
		}elseif($this->temperaturestart!='' && $this->temperaturend!='' && $this->temperature==2 ){
			
		$temprature = $this->temperaturestart.html_entity_decode("&#8451;").' to '.$this->temperaturend.html_entity_decode("&#8451;");
		return $temprature;
		}else{
			return false;
		}
	
	}
	
	
	public function getDaypartingcustom1value(){
		if($this->dayparting==1){
			return 24;
		}elseif($this->dayparting==3){
			if($this->daypartingcustom1!='' && $this->daypartingcustom1end!='')	{
				return $this->daypartingcustom1;
			}
		}else{
			return NULL;
		}
	}
	
	public function getDaypartingcustom1endvalue(){
		if($this->dayparting==1){
			return 24;
		}elseif($this->dayparting==3){
			if($this->daypartingcustom1!='' && $this->daypartingcustom1end!='')	{
				return $this->daypartingcustom1end;
			}
		}else{
			return NULL;
		}
	}
	
	
	
	
	public function getDaypartingvalue(){
	
		if($this->dayparting==1){
			return '24 Hours';
		}elseif($this->dayparting==3){
			$options = array();
			if($this->daypartingcustom1!='' && $this->daypartingcustom1end!='')	{	
			$options[] = $this->daypartingcustom1.' to '.$this->daypartingcustom1end;	
			}
			
			if($this->daypartingcustom2!='' && $this->daypartingcustom2end!='')	{	
			$options[] = $this->daypartingcustom2.' to '.$this->daypartingcustom2end;	
			}
			if($this->daypartingcustom3!='' && $this->daypartingcustom3end!='')	{	
			$options[] = $this->daypartingcustom3.' to '.$this->daypartingcustom3end;	
			}			
			return implode(", ",$options); 
			
		}else{
		   return false;	
		 }
	
	}
	/* start Rule/conditions */ 
	public function getRulesandconditions(){
		
		if($this->rulecondition == 1){			
			
			return  'Yes';			
			
		}else{
			
			return  'No';			
		}
	}
	
	public function getBehaviourvalue(){
		
		if($this->behaviourvisit1 != '' || $this->behaviourvisit2 != '' || $this->behaviourvisit3 != '' ){		
			
			if($this->behaviourvisit3==1){
				$behaviourvisit3 = 'days';
			}elseif($this->behaviourvisit3==2){
				$behaviourvisit3 = 'weeks';
			}elseif($this->behaviourvisit3==3){
				$behaviourvisit3 = 'months';
			}elseif($this->behaviourvisit3==4){
				$behaviourvisit3 = 'years';
			}	
			
			$behaviourvisit = 'User has visited beacon location minimum of '.$this->behaviourvisit1.' times in '.$this->behaviourvisit2.' '.$behaviourvisit3.'.';
			
			return $behaviourvisit; 
			
		}else{
			return '';  
		}
	}
	
	public function getBehaviourdwellvalue(){
		
		if($this->behaviourDwell != ''){
			if($this->behaviourDwellMinutes==1){
				$behaviourDwellMinutes = 'minutes';
			}elseif($this->behaviourDwellMinutes==2){
				$behaviourDwellMinutes = 'seconds';	
			}	
			
			$behaviourDwell = 'User has dwelt in beacon location minimum of '.$this->behaviourDwell.' '.$behaviourDwellMinutes.'';
			
			return $behaviourDwell; 
			 
		}else{
			return '';  
		}
	}
	
	public function getEntryvalue(){
		
		if($this->userenter == 1){	
			
			$entry = 'Yes, user has entered the store.';
			
			return $entry; 
			 
		}else{
			return '';  
		}
	}
	
	
	
	public function getExitvalue(){
		
		if($this->usereturned == 1){		
			
			$exitvalue = 'Yes, user has exited the store.';
			
			return $exitvalue; 
			 
		}else{
			return '';  
		}
	}
	
	public function getPurchasevalue(){
		
		if($this->purchaseHistory1 != '' || $this->purchaseHistory2 != '' || $this->purchaseHistory3 != ''){
			if($this->purchaseHistory3==1){
				$purchaseHistory3 = 'days';
			}elseif($this->purchaseHistory3==2){
				$purchaseHistory3 = 'weeks';
			}elseif($this->purchaseHistory3==3){
				$purchaseHistory3 = 'months';
			}elseif($this->purchaseHistory3==4){
				$purchaseHistory3 = 'years';
			}					
			$Purchasevalue = 'User has spent minimum of ' .'  $'.$this->purchaseHistory1.' in the past ' .$this->purchaseHistory2. ' ' .$purchaseHistory3. '.';
			
			return $Purchasevalue;  
			 
		}else{
			return '';  
		}
	}
	
	public function getLoyaltyvalue(){
		
		if($this->Loyalty1 != '' || $this->Loyalty2 != '' || $this->Loyalty3 != ''){
			
			if($this->Loyalty3==1){
				$Loyalty3 = 'days';
			}elseif($this->Loyalty3==2){
				$Loyalty3 = 'weeks';
			}elseif($this->Loyalty3==3){
				$Loyalty3 = 'months';
			}elseif($this->Loyalty3==4){
				$Loyalty3 = 'years';
			}	
								
			$Loyalty = 'User has scanned their loyalty ID '.$this->Loyalty1.' times in the past ' .$this->Loyalty2. ' ' .$Loyalty3. '.';
			
			return $Loyalty;  
			 
		}else{
			return '';  
		}
	}
	

	public function getusertype(){
			if(isset($this->userType)) {
				 $userName = explode(', ', $this->userType); 
				 $userdata = array(); 
				 foreach($userName as $userN) { if($userN==1) { $userdata[] = 'Men'; } if($userN==0) { $userdata[] = 'Women'; } if($userN==2) { $userdata[] = 'Kids'; } }
				$model = implode(', ', $userdata);
				return $model; 
		}
		
		
	}
	
	/* get Advertiser  form User Model */
	public function getAdvertiser(){
		
		return $this->hasOne(User::className(), ['id' => 'workorderpartner']);
	}
	
	public function getAdvaddress(){
		
		return $this->hasOne(Profile::className(), ['user_id' => 'workorderpartner']);
	}
	
	public function getstorefavorites()
    {
         return $this->hasOne(UserStore::className(), ['store_id' => 'workorderpartner'])
         ->onCondition(["user_id"=>Yii::$app->user->id]);
    }
	/* get Reseller  form User Model */
	public function getReseller(){
		
		return $this->hasOne(User::className(), ['id' => 'resellerID']);
	}
	
 
	
	/* get workoder type is 'workorder' or 'PSA' */
	 public function getWorkordertypename(){
		
		$Workodertype = $this->workorderType;
		if($Workodertype==1) {
			 return 'Work Order'; 
		}elseif($Workodertype==2) {			 
		$pastype = Workorderpopup::find()->select('psaoffertype')->where(['workorder_id' => $this->id])->one(); 
			if($pastype->psaoffertype == 1){
				return 'PSA info';
			}elseif($pastype->psaoffertype == 2){
				return 'PSA deal';
			}else{
				return '';
			}
		 }else { return ''; }
		
	}  
	

/* get woroder popup pass url */	
	public function getWorkorderpopup(){
		 return $this->hasMany(Workorderpopup::className(), ['workorder_id' => 'id']);
	}
	
	public function getDeal(){
		
		return $this->hasMany(Deal::className(), ['workorder_id' => 'id']);
	}
	
	/* Used Passes */
	public function getCartcount(){
		
		return $this->hasMany(Cart::className(), ['deal_id' => 'id']);
	}
	
	
	
	
	/* get age group name */
	
	
	public function getAgegroupname(){
		
		if(!empty($this->agedGroup)){
		 $ageGroups = explode(', ',$this->agedGroup);
		 $Findall = array();
		 foreach($ageGroups as $ageGroup){
			  if($ageGroup==1){
					$Findall[$ageGroup] = 'Between 15-18 years';
			  }elseif($ageGroup==2){
					$Findall[$ageGroup] = 'Between 19-25 years';
			  }elseif($ageGroup==3){
				    $Findall[$ageGroup] = 'Between 26-35 years';
			  }elseif($ageGroup==4){
				    $Findall[$ageGroup] = 'Between 35-45 years';
			  }elseif($ageGroup==5){
				     $Findall[$ageGroup] = 'Between 46-60 years';
			  }elseif($ageGroup==6){
				   $Findall[$ageGroup] = 'More than 61 years';
				  
			  }
		  
		 }
		 
		 $selectedagegroup = implode(', ',$Findall);
	      return  $selectedagegroup;
		
		}else{
			return 'Not selected';
		}
	}
	
	
	/* get beacon name form Beacongroup Model */
	public function getBeacongroupname(){
		
	 $groupid =  trim($this->beaconGroup,","); 
	 if(!empty($groupid)) {
		 
		$model1 = Beacongroup::find()->select('group_name')->andWhere('id IN('.$groupid.')')->all(); 
		
		$GroupN = array();
		foreach ($model1 as $groupName) {
			$GroupN[] = $groupName['group_name'];
		 	 
	   }
		$model = implode(', ', $GroupN);
		return $model; 
	}else{
		return 'No any Group Selected';
	}
	
	}
	
	
	
	public function afterFind()
			{
				parent::afterFind();
				
					/* Date From */
					if(!strstr($this->dateFrom,"-"))
					{               
						$this->dateFrom=date("Y-m-d",$this->dateFrom);
					}
					if(!strstr($this->dateTo,"-"))
					{               
						$this->dateTo=date("Y-m-d",$this->dateTo);
					}
			
			return true;		
			}

	
	 public function beforeSave($insert)
		{
									
			$dateFrom = new \DateTime($this->dateFrom.' 00:00:01');			
			$this->dateFrom = strtotime($dateFrom->format("Y-m-d H:i:s"));
			$dateTo = new \DateTime($this->dateTo.' 23:59:59');			
			$this->dateTo = strtotime($dateTo->format("Y-m-d H:i:s"));
								
	    if (parent::beforeSave($insert)) {
	    	
	    	$this->updated_at= time();
	    	$this->updated_by= Yii::$app->user->id;
	    	if($insert){			    		
	    		
	    		$this->created_at= time();
	    		$this->created_by= Yii::$app->user->id;
	     	}
			    
	    	return true;
		    } else {
	    	
		     return false; 
			    }
		}
		
	// used to generate top perform campaign graph
	
	public function topPerformCampaign($start,$end,$userField){
		
		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			
			$custQuery 		= User::tableName().'.user_type='.$userType.' AND '.User::tableName().'.id='.$userId;
			
		}else{
			
			$custQuery = '';
			
		}
		
		
		$query  = new Workorders();
		$reslt 	= $query->find()
						->select(Workorders::tableName().".id,name,(usedPasses*100/noPasses)/(unix_timestamp()-dateFrom)/86400 as topPerform, from_unixtime(dateFrom),from_unixtime(dateTo)")
						->leftJoin(User::tableName(), User::tableName().'.id='.Workorders::tableName() .'.workorderpartner')
						->Where(['between',Workorders::tableName().'.created_at', $start,$end])
						->andWhere($custQuery)
						->andWhere([Workorders::tableName().'.status'=>1])
						->andWhere('type!=5')
						->orderBy(['topPerform'=>SORT_DESC])
						->limit(5)
						->offset(0)
						->asArray()
						->all();
						
		
		
		return 	$reslt;
		
	}	
		
	// used to generate MONTHLY QUOTA - PASSES
	
	public function monthlyQuotaPass($userField){
		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			
			$custQuery 		= User::tableName().'.user_type='.$userType.' AND '.User::tableName().'.id='.$userId;
			
		}else{			
			$custQuery = '';			
		}		
		$query  = new Workorders();
		
		$date 	= new \DateTime(date('m/d/Y H:i:s',time()));
						
		$unixDateEnd 	  = $date->getTimeStamp();
		
		$date->sub(new \DateInterval('P1M'));
		
		$unixDateStart = $date->getTimeStamp();
		
		
		$reslt 	= $query->find()->select("noPasses,usedPasses,(noPasses - usedPasses) AS availPass")
					  ->leftJoin(User::tableName(), User::tableName().'.id='.Workorders::tableName() .'.workorderpartner')
					  ->Where(['between',Workorders::tableName().'.created_at', $unixDateStart,$unixDateEnd])
					  ->andWhere($custQuery)
					  ->groupBy('type');
					  
		// $totalPass 		= $reslt->sum('noPasses');
		$savedPass		= $reslt->sum('usedPasses');
		$availPass 	 	= $reslt->sum('availPass');
		return 	array((int) $savedPass,(int) $availPass);
		
	}	
	
	
	// used to generate total campaign running
	
	public function campaignRunning($userField){
		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= User::tableName().'.user_type='.$userType.' AND '.Workorders::tableName().'.workorderpartner='.$userId;
			
		}else{
			
			$custQuery		= '';
			
		}
		
		$query  = new Workorders();
		
		$reslt 	= $query->find()->select("count(".Workorders::tableName().".id) AS running_camp")
					  ->leftjoin(User::tableName(),User::tableName().'.id='.Workorders::tableName().'.workorderpartner')
					  ->andWhere([Workorders::tableName().'.status'=>1])
					  ->andWhere(Workorders::tableName().'.type!=5')
					  ->andWhere($custQuery)
					  ->asArray()
					  ->one();
		
		return 	$reslt;
		
	}	
	
}
