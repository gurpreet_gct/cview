<?php 

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
 
class City extends ActiveRecord
{
	const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city}}';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    
     /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
            
    }
    
    
   public function attributeLabels()
		{
			return [
				'id' => Yii::t('app','ID'),
				'name' => Yii::t('app','Name'),
				
			];
		}
	
	
	
}

