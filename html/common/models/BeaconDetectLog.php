<?php

namespace common\models;
use common\models\Beaconmanager;

use Yii;

/**
 * This is the model class for table "tbl_beacon_detect_log".
 *
 * @property integer $id
 * @property integer $customerId
 * @property string $minorValue
 * @property string $majorValue
 * @property datetime $detect_at
 */
class BeaconDetectLog extends \yii\db\ActiveRecord
{
    
     /**
     * @inheritdoc
     */
     
    
    
    public static function tableName()
    {
        return '{{%beacon_detect_logs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			 [['minorValue', 'majorValue','customerId','beaconUUID'], 'required'],
			
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'minorValue' => Yii::t('app','Minor Value'),
            'majorValue' => Yii::t('app','Major Value'),
            'beaconUUID' => Yii::t('app','BeaconUUID'),
            'customerId' =>Yii::t('app','Customer') ,
            'deviceId' => Yii::t('app','Device Id'),
            'detect_at' => Yii::t('app','Detect_At'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
    
  
    
    public function fields()
	{
		$fields=parent::fields();
		unset($fields['created_at']);
		unset($fields['updated_at']);
	
		return $fields;
	}
    
    
    	public function afterFind()
        {
            parent::afterFind();
            
           return true;
				
        }
      
	  public function getCustomer()
        {
              return $this->hasOne(User::className(), ['id' => 'customerId']);
        }
		
		public function getCustomerName()
		{
			 return $this->Customer->fullName;
		}
		
		
	// used to generate BEACONS ONLINE report
	
	public function beaconsOnline($unixDateStart,$unixDateEnd){
							
		$query 		= Yii::$app->getDb();
		
		$command 	= $query->createCommand("SELECT BM.name,count(distinct BDL.majorValue) AS onlinebeacon FROM `tbl_beacon_detect_logs` AS BDL JOIN {{%beacon_manager}} AS BM ON BDL.majorValue=BM.major AND BDL.minorValue=BM.minor WHERE BDL.detect_at BETWEEN FROM_UNIXTIME($unixDateStart) AND FROM_UNIXTIME($unixDateEnd) GROUP BY BDL.majorValue");
		
		$result 	= $command->queryAll();
		
		return 	$result;
		
	}
	public function getBeaconmanager(){
		
		return $this->hasOne(Beaconmanager::className(), ['uuid' => 'beaconUUID']);
	}
	
}
