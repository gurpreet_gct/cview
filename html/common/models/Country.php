<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Usertype model
 *
 * @property integer $id
 * @property string $usertype 
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 
 */
class Country extends ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%countries}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['iso_code','safe'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }
	
	
	public function attributeLabels()
		{
			return [
				'id_countries' => Yii::t('app','Id_Countries'),
				'sortname' => Yii::t('app','Sortname'),
				'name' => Yii::t('app','Name'),
				
			];
		}
	
	
	
	
   
}
