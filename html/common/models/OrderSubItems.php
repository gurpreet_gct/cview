<?php

namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
class OrderSubItems extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
     return '{{%order_sub_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id,order_id'], 'safe'],
      
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
        	'id' => Yii::t('app','ID'),
        	'product_id' => Yii::t('app','Product_Id'),
        	'deal_id' => Yii::t('app','Deal_Id'),
        	'qty' => Yii::t('app','Quantity'),
        	'order_id' => Yii::t('app','Order_Id'),
        	'user_id' => Yii::t('app','User_Id'),
        	'store_id' => Yii::t('app','Store_Id'),
        	'store_location_id' => Yii::t('app','Store_Location_Id'),
        	'product_price' => Yii::t('app','Product_Price'),
        	'product_currency_code' => Yii::t('app','Product_Currency_Code'),
        	'product_currency_id' => Yii::t('app','Product_Currency_Id'),
        	'payment_currency_code' => Yii::t('app','Payment_Currency_Code'),
        	'payment_currency_id' => Yii::t('app','Payment_Currency_Id'),
        	'exchange_rate' => Yii::t('app','Exchange_Rate'),
        	'product_exchange_price' => Yii::t('app','Product_Exchange_Price'),
        	'tax' => Yii::t('app','Tax'),
        	'shipping_charges' => Yii::t('app','Shipping_Charges'),
        	'discount' => Yii::t('app','Discount'),
        	'total' => Yii::t('app','Total'),
        	'status' => Yii::t('app','Status'),
        	'created_at' => Yii::t('app','created at'),
			'updated_at' => Yii::t('app','updated at'),
			'created_by' => Yii::t('app','created by'),
			'updated_by' => Yii::t('app','updated by'),
        ];
    }
    
    
    
    
    
    
	public function getProduct()
	{
		   return $this->hasOne(Product::className(), ['id' =>'product_id']); 
	}
   public function fields()
		{
			
			$fields=parent::fields();		  
			unset($fields['created_at']);
			unset($fields['updated_at']);
			unset($fields['created_by']);
			unset($fields['updated_by']);
			$fields['product'] = function ($model) {
            return $model->product; // Return related model property, correct according to your structure
			};
			return $fields;
		}
		
		
		
   
}
