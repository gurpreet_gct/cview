<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Account model
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $bt_id 
 * @property integer $status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 
 */
 
class PaymentAccount extends ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['user_id','bt_id'],'required'],
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'user_id' => Yii::t('app','User_Id'),
            'bt_id' => Yii::t('app','Bt_Id'),
            'status' => Yii::t('app','Status'),
            'created_at'=>Yii::t('app','Created_At'),
            'updated_at'=>Yii::t('app','Updated_At'),
            'created_by'=>Yii::t('app','Created_By'),
            'updated_by'=>Yii::t('app','Updated_By'),
        ];
    }
    
    

	    public function beforeSave($insert)
		{
				
			if (parent::beforeSave($insert)) {
			    	
			    	if($insert){			    		
			    		$this->created_by=Yii::$app->user->id;			    		
			    		
			    		$this->created_at= time();
			    		
			    		
			     	}
			     	$this->updated_by=Yii::$app->user->id;			    		
			     	$this->updated_at= time();
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
		}
   
}
