<?php

namespace common\models;

use Yii;
use common\models\User;
use backend\models\Regions;
use yii\helpers\ArrayHelper;
#use yii\web\UploadedFile;
/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property integer $status
 * @property string $email
 * @property integer $contact_no
 * @property string $role
 * @property integer $active
 * @property string $tokenhash
 * @property integer $tokenexpired
 * @property string $created
 * @property string $modified
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $usertype_id=3;
//    public $brandimgLogo;
//    public $backgroundImage;
//    public $foodcatname;
//    public $subscriptionCheck;
//    public $companyName;


    public static function tableName()
    {
        return '{{%profile}}';
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			 ['brandimgLogo', 'dimensionValidationmain'],
			 [['subscriptionCheck','houseNumber','foodcatname','minimumSpend','geo_radius','subscriptionFees','street','city','state','country','postcode'],'safe'],
			 ['backgroundImage', 'dimensionValidationback'],
			 //[['brandimgLogo','backgroundImage'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['transactionrule','brandName','brandAddress','brandDescription','country','companyNumber','companyName','postcode','street','alternativeContactperson', 'city','ssdRegion','officePhone'], 'required'],
           # ['brandName','match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9\s]/','message' => 'Please enter a valid brandname.'],
            ['companyNumber', 'match', 'pattern' => '/^[a-zA-Z0-9]+$/', 'message' => 'Please enter valid company number.'],
            [['officePhone'], 'integer','message' => 'Please enter a valid phone number.' ],
			[['companyName','brandName'],'unique'],
			[['officeFax'], 'integer', 'message' => 'Please enter valid fax number'],
			[['state','latitude','longitude','brandLogo','socialmediaFacebook','socialmediaTwitter','socialmediaGoogle','backgroundHexColour','foregroundHexColour'], 'string'],
			 [['backgroundHexColour', 'foregroundHexColour'], 'string', 'min' => 7],
			[['addLoyalty','shopNow','foodEstablishment','socialcheck','addcolors'], 'integer'],
			 [['socialmediaFacebook','socialmediaTwitter','socialmediaGoogle','socialmediaPinterest','socialmediaInstagram'], 'url', 'defaultScheme' => 'http'],
			  [['brandimgLogo'], 'required','on' => 'create', 'when' => function($model) {
               return '';
           },
           'whenClient' => "function (attribute, value) {

               var wt= $('#profile-brandimglogo').val();

                 if (wt !='') {  return false; }
                   else {
                   return true; }

               }"

           ],

            [['backgroundImage'], 'required','on' => 'create', 'when' => function($model) {
               return '';
           },
           'whenClient' => "function (attribute, value) {

               var wt= $('#profile-backgroundimage').val();

                 if (wt !='') {  return false; }
                   else {
                   return true; }

               }"

           ],
           [['subscriptionFees'], 'number','min'=>0.1],          
            [['subscriptionDueDate','subscriptionFees','subscriptionPay'], 'required', 'when' => function ($model) {
				return $model->subscriptionCheck ==0?false:true;
			}, 'whenClient' => "function (attribute, value) {
			var wt= $('input[type=checkbox][name=\"Profile[subscriptionCheck]\"]:checked').val();

                 if (wt == 1) {  return true; }
                   else {
                   return false; }
			}"],



        ];
    }

    /**
     * @inheritdoc
     */
   public function attributeLabels()
    {
        return [
             'id' =>Yii::t('app','Id') ,
            'user_id' =>Yii::t('app','User_Id') ,
            'subscriptionDueDate' =>Yii::t('app','Subscription due day') ,
            'subscriptionFees' =>Yii::t('app','Subscription amount') ,
            'companyName' =>Yii::t('app','Company Name') ,
            'companyNumber' => Yii::t('app','Company Registration Number'),
            'ssdRegion' =>Yii::t('app','Region'),
            'houseNumber' => Yii::t('app','Street Number'),
            'street' => Yii::t('app','Street Number and Street'),
            'city' =>Yii::t('app','City') ,
            'state' => Yii::t('app','State'),
            'country' => Yii::t('app','Country'),
            'postcode' =>Yii::t('app','Postcode') ,
            'officePhone' =>Yii::t('app','Office Phone') ,
            'officeFax' => Yii::t('app','Office Fax'),
			'alternativeContactperson' => Yii::t('app','Alternative Contact Person'),
            'commericalStatus' => Yii::t('app','Commerical_Status'),
            'latitude' => Yii::t('app','Latitude'),
            'longitude' => Yii::t('app','Longitude'),
            'brandName' => Yii::t('app','Store/Brand Name'),
            'brandLogo' => Yii::t('app','Brand_Logo'),
            'brandimgLogo' => Yii::t('app','Store/Brand Logo (132px x 132px)'),
            'backgroundImage' => Yii::t('app','Background Image (640px x 360px)'),
            'brandAddress' => Yii::t('app','Store/Brand Address(head office)'),
            'brandDescription' => Yii::t('app','Store/Brand Description'),
            'socialmediaFacebook' => Yii::t('app','Facebook Url'),
            'socialmediaTwitter' => Yii::t('app','Twitter Url'),
            'socialmediaGoogle' => Yii::t('app','Google Url'),
            'socialmediaPinterest' =>Yii::t('app','Pinterest Url') ,
            'socialmediaInstagram' => Yii::t('app','Instagram Url'),
            'backgroundHexColour' => Yii::t('app','Background Hex Colour'),
            'foregroundHexColour' =>Yii::t('app','Label Text Colour (Hex Colour)') ,
            'addLoyalty' => Yii::t('app','Add loyalty points / dollar values'),
            'shopNow' => Yii::t('app','Add "FOOD MENU"'),
            'foodEstablishment' => Yii::t('app','Is this is a food establishment?'),
            'socialcheck' => Yii::t('app','Add Social media buttons and URLs'),
            'addcolors' => Yii::t('app','Add "Background and foreground hex colour"'),
            'minimumSpend' => Yii::t('app','Add minimum spend a user must first spend before they get loyalty points.'),
            'geo_radius' => Yii::t('app','Add geolocation radius (in km) for food order'),
        ];
    }

    /* Brand/Store Logo image validation */
    public function dimensionValidationmain($attribute,$param){
			if(is_object($this->brandimgLogo)){

 				list($width, $height) = getimagesize($this->brandimgLogo->tempName);

				if($width!=132 || $height!=132){

					$this->addError($attribute,'Brand logo size should be 132px x 132px dimension');
			    }
			}
		}

	/* Store Background image validation  */
	public function dimensionValidationback($attribute,$param){

			if(is_object($this->backgroundImage)){


 				list($width, $height) = getimagesize($this->backgroundImage->tempName);

				if($width!=640 || $height!=360){

					$this->addError($attribute,'Background image should be 640px x 360px dimension');
			    }
			}
		}



	public function getAddLoyaltyvalue() {

		  if($this->addLoyalty==1) {
		   return 'True';
		 }else {
			  return 'false';
		 }
     }

     public function getShopNowvalue() {

		  if($this->shopNow==1) {
		   return 'True';
		 }else {
			  return 'false';
		 }
     }

     public function getimagename(){

		  if(($this->brandLogo!="")&&(!strstr($this->brandLogo,'http'))) {
		   return Yii::$app->params['userUploadPath'].$this->brandLogo;
		 }else {
			  return Yii::$app->params['userUploadPath'].'no-image.jpg';
		 }
     }

     public function getBackimage(){


		  if(($this->backgroundHexColour!="")&&(!strstr($this->backgroundHexColour,'http'))) {

		     return Yii::$app->params['userUploadPath']."/".$this->backgroundHexColour;
		 }else {
			 return Yii::$app->params['userUploadPath']."/".'no-image.jpg';
		 }
     }

   public function getUser_type()
    {
         return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);

    }

     /* get region name from Region model */
     public function getRegionname()
    {
        return $this->hasOne(Regions::className(), ['id' => 'ssdRegion']);

    }


    public static function getAdvertiser()
    {
		if(Yii::$app->user->identity->user_type==1){
return   ArrayHelper::map(Profile::find()->joinWith('user', true, 'INNER JOIN')
	     ->select(sprintf("%s.id ,%s.user_type ,%s.user_id, %s.companyName",User::tableName(),User::tableName(),Profile::tableName(),Profile::tableName()))
	     ->where(['user_type' => 7,'status' => 10])
	     ->all(), 'id', 'companyName');
	 }else{
		return '';
	 }




	}
	
	public function getCardtype(){
		if($this->subscriptionPay==1)
		return 'Bank Account';
		else if($this->subscriptionPay==2)
		return 'Credit Card';
	}

}
