<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_subscription_charges".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property string $amount
 * @property integer $payment_status
 * @property string $transaction_id
 * @property string $account_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class SubscriptionCharges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_subscription_charges';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id'], 'required'],
            [['due_date','updated_by','partner_id', 'payment_status', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
            [['transaction_id', 'account_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'partner_id' => Yii::t('app', 'Partner '),
            'amount' => Yii::t('app', 'Amount'),
            'payment_status' => Yii::t('app', 'Payment Status'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'due_date' => Yii::t('app', 'Due date'),
            'account_id' => Yii::t('app', 'Paid via'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
	public function beforeSave($insert)
	{
		   if (parent::beforeSave($insert)) {
			   $this->updated_at= time();
			   $this->updated_by = Yii::$app->user->id;
			   if($insert){
				   $this->created_at= time();
			   }

			   return true;
			 } else {

			  return false;
		   }
	}
	     /* get the partner name name from  profile model */
     public function getPartnername() {
  		if($this->partner_id!='') {
  		    $lowner = \common\models\User::find()->select('orgName')->where(['id' =>$this->partner_id])->one();
  		      if($lowner){
  			         return $lowner->orgName;
  		      }
            return 'N/A';
  	  }
  	}
  	
  	/* relation with partners*/
  	public function getPartner(){
			return $this->hasOne(Profile::className(), ['user_id' =>'partner_id']);
	}
	/* relation with bank account*/ 
  	public function getBankdetail(){
			return $this->hasOne(Bankdetail::className(), ['partner_id' =>'partner_id']);
	}
	
    public function getAccountname(){
      if($this->account_id){
        $creditCards=   \common\models\Paymentcard::findOne(['uniqueNumberIdentifier'=>$this->account_id,'issuingBank'=>'promisePay']);
        if($creditCards!==null){
          return 'Credit card '.$creditCards->maskedNumber;
        }else {
            $bankAcc = \common\models\Bankdetail::findOne(['promise_acid'=>$this->account_id]);
          if($bankAcc->bank_name){
            return 'Bank account '.$bankAcc->account_number;
          }
        }
        return 'N/A';
      }
    }
    public function chargesHistory($unixDateStart,$unixDateEnd,$filterType,$userField){

  		$query = Yii::$app->getDb();
  		if(!empty($userField)){

  			$userType 	= $userField['userType'];
  			$userId 	= $userField['userId'];
  			$custQuery 		= ' AND partner_id='.$userId;

  		}else{
  			$custQuery		= '';
  		}

  		$tableName = self::tableName();
  		$command 	= $query->createCommand("SELECT IFNULL(sum(amount),0) as paid FROM $tableName where payment_status=1 and created_at BETWEEN $unixDateStart AND $unixDateEnd $custQuery union all SELECT IFNULL(sum(amount),0) as paid FROM $tableName where payment_status=0 and created_at BETWEEN $unixDateStart AND $unixDateEnd $custQuery union all SELECT IFNULL(sum(amount),0) as paid FROM $tableName where payment_status!=100 and created_at BETWEEN $unixDateStart AND $unixDateEnd $custQuery ;");
  		$result 	= $command->queryAll();
  		return array_column($result,'paid');

  	}
}
