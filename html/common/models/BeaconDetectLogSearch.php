<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BeaconDetectLog;
use common\models\Beaconmanager;
use common\models\User;
/**
 * BeaconDetectLogSearch represents the model behind the search form about `common\models\BeaconDetectLog`.
 */
class BeaconDetectLogSearch extends BeaconDetectLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
         return [
            [['minorValue', 'majorValue', 'beaconUUID', 'deviceId', 'detect_at'], 'safe'],
            [[ 'customerId' ], 'integer'],
           
        ];
    }
    
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'minorValue' => Yii::t('app','Minor Value'),
            'majorValue' => Yii::t('app','Major Value'),
            'beaconUUID' => Yii::t('app','BeaconUUID'),
            'customerId' =>Yii::t('app','Customer') ,
            'deviceId' => Yii::t('app','Device Id'),
            'detect_at' => Yii::t('app','Detect_At'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }
    
    
    

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		
		 $usertype = Yii::$app->user->identity->user_type;
         $userId = $usertype == User::ROLE_Advertiser ? Yii::$app->user->id : Yii::$app->user->identity->advertister_id;
       
         if($usertype==User::ROLE_ADMIN){
			 
			$query = BeaconDetectLog::find();
			
		}else{
			$query = BeaconDetectLog::find()
					->joinWith('beaconmanager',false,'INNER JOIN')
					->where(Beaconmanager::tableName().'.locationOwnerID='. $userId);
		}		
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'customerId' => $this->customerId,
            
        ]);

        $query->andFilterWhere(['like', 'minorValue', $this->minorValue])
				->andFilterWhere(['like', 'majorValue', $this->majorValue])
				->andFilterWhere(['like', 'beaconUUID', $this->beaconUUID])
				->andFilterWhere(['like', 'deviceId', $this->deviceId])
				->andFilterWhere(['like', 'detect_at', $this->detect_at]);
		
        return $dataProvider;
    }
}
