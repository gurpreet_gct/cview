<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "tbl_civew_config".
 *	
 */
class CviewConfig extends \yii\db\ActiveRecord
{

	const KEY_AUCTION_STATUS = 'auction_status';
	const KEY_MIN_SECONDS_TO_BID_BEFORE_AUCTION_STARTS = 'auction_status';
    const KEYS_WITH_JSON_CONFIG = [
    	'auction_status'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cview_config}}';
    }

    public function rules()
    {
        return [
        	[['key','value'],'required'],
        	[['key','value'],'safe'],
        	['key', 'unique'],
        	['value', 'validateValueArray' , 'on' => 'validating-currency'],
        ];
    }

    public function afterFind()
	{
		parent::afterFind();
    	if (in_array($this->key, self::KEYS_WITH_JSON_CONFIG)) {
			$this->value = json_decode($this->value, true);
		}
		return true;		
	}
    
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','Id'),
			'key' => Yii::t('app','Key'),
			'value' => Yii::t('app','Value'),
			'status' => Yii::t('app','Status'),
        ];
    }
	
	public function fields()
	{
			
		$fields=parent::fields();		  
		/*unset($fields['created_at']);
		unset($fields['updated_at']);
		unset($fields['created_by']);
		unset($fields['updated_by']);
		$fields['address_typename'] = function ($model) {
			return $model->addresstype->type; // Return related model property, correct according to your structure
		};
		$fields['countryname'] = function ($model) {
			return $model->country->name; // Return related model property, correct according to your structure
		};*/
			
		return $fields;
	}

	public function validateValueArray($attribute, $params)
	{
		$arr = json_decode($this->value);
        if(!is_array($arr)) { 
			$this->addError($attribute, strtr(\Yii::t('app', "The wrong json format."),
			[" "]));		     	
        } else if(empty($arr)) {
			$this->addError($attribute, strtr(\Yii::t('app', "The wrong json format."),
			[" "]));         
        } else {
	        foreach($arr as $row) {
	        	if (!is_numeric($row)) {
					$this->addError($attribute, strtr(\Yii::t('app', "json data should be in numeric format."),
				[" "]));
	        	}
	        }
    	}
    }

	public function resetSettings()
	{
		$status = self::truncateTable(self::tableName())->execute();
		return ['status' => $status, 'message' => 'The status for this table has been updated'];
	}

	public function getConfigObj($key)
	{
		return $this->find()->where(['key' => $key, 'status' => 1])->one();
	}

	public function getMinTimeToBidBeforeAuctionConfig()
	{
		$data = $this->getConfigObj(self::KEY_AUCTION_STATUS);
		return $data;
	}

	public function getAuctionStatusConfig()
	{
		$data = $this->getConfigObj(self::KEY_AUCTION_STATUS);
		return $data;
	}

	public function getAuctionPauseConfig()
	{
		$data = $this->getAuctionStatusConfig(self::KEY_AUCTION_STATUS);
		return $data['value']['pause'];
	}

}
