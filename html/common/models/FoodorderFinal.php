<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%foodorder_final}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $user_id
 * @property string $lat
 * @property string $lang
 * @property integer $created_at
 * @property integer $created_by
 */
class FoodorderFinal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%foodorder_final}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id','lat', 'lang'], 'required'],
            [['order_id', 'user_id', 'created_at', 'created_by'], 'integer'],
            [['lat', 'lang'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'order_id' => Yii::t('app','Order ID'),
            'user_id' => Yii::t('app','User ID'),
            'lat' => Yii::t('app','Lat'),
            'lang' => Yii::t('app','Lang'),
            'created_at' => Yii::t('app','Created At'),
            'created_by' => Yii::t('app','Created By'),
        ];
    }
}
