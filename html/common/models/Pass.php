<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\User;
use common\models\Profile;
use common\models\Workorders;
use common\models\Workorderpopup;
use common\models\Qr;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "tbl_passes".
 *	
 */
class Pass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%passes}}';
    }
    
 
    public function rules()
    {
        return [
            [['user_id','workorder_id','id','passesTrack'],'safe'],
            ['passesTrack','integer'],
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','Id'),
            'user_id' => Yii::t('app','User_Id'),
            'store_id' => Yii::t('app','Store_Id'),
            'workorder_id' => Yii::t('app','Workorder_Id'),
            'offerTitle' => Yii::t('app','Offer_Title'),
            'offerText' => Yii::t('app','Offer_Text'),
            'offerwas' => Yii::t('app','Offer_Was'),
            'offernow' => Yii::t('app','Offer_Now'),
            'readTerms' => Yii::t('app','Read_Terms'),
            'sspMoreinfo' => Yii::t('app','SSP_Moreinfo'),
            'created_at' => Yii::t('app','created at'),
			'updated_at' => Yii::t('app','updated at'),
			'isUsed' => Yii::t('app','Is_Used'),
			'count_passes' => Yii::t('app','Count_Passes'),
			'cardTrack' => Yii::t('app','Card_Track'),
        ];
    }
    
    

    /**
     * @inheritdoc
     */
     
     
     public function getOffertrack(){
		  $user_id=Yii::$app->user->id; 
		  $offertarck = Offertrack::find()->where(['workorder_id' => $this->workorder_id, 'user_id' => $user_id])->orderBy('id desc')->one();
	      if(isset($offertarck->usedNo)){
			  $usedNo = $offertarck->usedNo;
		  }else{
		    $usedNo = 0;
		  }
	 return $usedNo;
	 }
     public function fields()
	{  
	
		
		
		$fields=parent::fields();
			unset($fields['offerTitle']);
			unset($fields['offerText']);
			unset($fields['offerwas']);
			unset($fields['offernow']);
			unset($fields['readTerms']);
			unset($fields['sspMoreinfo']);
			unset($fields['created_at']);
			unset($fields['updated_at']);
			
			//$fields=["user_id","store_id","workorder_id","id","count_passes",
			$fields["brandName"]=function($model){
				return $model->profile->brandName;
			};
			$fields["brandAddress"]=function($model){
				return $model->profile->brandAddress;
			};			
			$fields["brandLogo"]=function ($model){
			return Yii::$app->urlManagerFrontEnd->createAbsoluteUrl("profile/".$model->profile->brandLogo);			
			};
			
			$fields["workordername"]=function($model){
				return $model->workorders->name;
			};
			$fields["workorderType"]=function($model){
				return $model->workorders->workorderType;
			};
			$fields["type"]=function($model){
				return $model->workorders->type;
			};
            /* $fields["expire date"]=function($model){
				return $model->workorders->dateTo;
			}; */
			$fields["passUrl"]=function($model){
				return $model->workorderpopup->passUrl;
				
			}; 		
			
			/*$fields["logo"]=function($model){
				return $model->workorderpopup->logo;
				
			};
			$fields["photo"]=function($model){
				return $model->workorderpopup->photo;
				
			}; 
		   $fields["buynow"]=function($model){
				return $model->workorderpopup->buynow;
				
			}; 
			$fields["eventDate"]=function($model){
				return $model->workorderpopup->eventDate;
				
			};
			$fields["eventVenue"]=function($model){
				return $model->workorderpopup->eventVenue;
				
			}; 
			
			$fields["ticketinfo"]=function($model){
				return $model->workorderpopup->ticketinfo;
				
			}; */
			
		
		
		return $fields;
	}
	/*public function extraFields()
	{
	
			return ["workorderpopup"];
	}*/
	
 public function getProfile()
	{
		       return $this->hasOne(Profile::className(), ['user_id' => 'store_id']);       

	}
	public function getWorkorders()
    {
         return $this->hasOne(Workorders::className(), ['id' => 'workorder_id']);
    }
    public function getWorkorderpopup()
    {
         return $this->hasOne(Workorderpopup::className(), ['workorder_id' => 'workorder_id']);
    }
 
   
   public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	
			    	$this->updated_at= time();
			    	if($insert){			    		
			    	
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}
			
			
	 
	/*
   * get total record of saved and redeemed data
   */
   
    public function getSavedRedeemedPass($start,$end,$filterType,$userField){
		
		
		
		if($filterType=='D'){
			
			$group = 'HOUR(FROM_UNIXTIME(p.created_at))';
			
			$specifier = '%b %d,%Y %h:%i:%s';
			
		}elseif($filterType=='W' || $filterType=='M'){
			
			$group = 'DAY(FROM_UNIXTIME(p.created_at))';
			$specifier = '%b %d,%Y';
			
		}elseif($filterType=='Y'){
			
			$group 		= 'MONTH(FROM_UNIXTIME(p.created_at))';
			$specifier 	= '%b,%Y';
			
		}elseif($filterType=='O'){
			
			$group = 'DAY(FROM_UNIXTIME(p.created_at))';
			
			$specifier = '%b %d,%Y %h:%i:%s';
			
		}
		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND p.store_id='.$userId;
			
		}else{
			
			$custQuery		= '';
			
		}
		
		
		$query 		= Yii::$app->getDb();
		
		
		// "SELECT sum(p.isUsed) as saved,sum(p.passesTrack) as redeemed,FROM_UNIXTIME(p.created_at,'".$specifier."') AS datetime
 // FROM {{%passes}} p LEFT JOIN {{%users}} t ON p.user_id=t.id WHERE (p.created_at BETWEEN $start AND $end) $custQuery GROUP BY MONTH(FROM_UNIXTIME(p.created_at)) ORDER BY p.created_at
 // ASC"
 
		$command 	= $query->createCommand("SELECT sum(p.isUsed) as saved,sum(p.passesTrack) as redeemed,FROM_UNIXTIME(p.created_at,'".$specifier."') AS datetime
 FROM {{%passes}} p LEFT JOIN {{%users}} t ON p.user_id=t.id WHERE (p.created_at BETWEEN $start AND $end) $custQuery GROUP BY MONTH(FROM_UNIXTIME(p.created_at)) ORDER BY p.created_at
 ASC");
		
		$result 	= $command->queryAll();
		
		return 	$result;
		
		
	}		
   		
	 
	/*
	* get total record of PASSES PER CATEGORY
	*/
   
    public function passPerCategory($start,$end,$userField){
		
		$query 		= Yii::$app->getDb();
		
		if(!empty($userField)){
			
			$userType 	= $userField['userType'];
			$userId 	= $userField['userId'];
			$custQuery 		= ' AND u.id='.$userId;
			
		}else{
			
			$custQuery		= '';
			
		}
		
		
		$command 	= $query->createCommand("SELECT tc.id, tc.name, SUM(isUsed) as saved,SUM(passesTrack) as redeemed FROM {{%categories}} tc JOIN {{%workorderCategory}} twc ON twc.category_id=tc.id AND lvl=0 JOIN {{%passes}} t on t.workorder_id=twc.workorder_id AND twc.status=1 JOIN {{%users}} u ON u.id=t.user_id WHERE tc.lvl=0 AND (t.created_at BETWEEN $start AND $end) $custQuery GROUP BY tc.id");
		
		$result 	= $command->queryAll();
		
		return 	$result;
		
	}

	
   }
   
   
