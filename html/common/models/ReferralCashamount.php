<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%referral_cashamount}}".
 *
 * @property integer $user_id
 * @property string $lifetime_commission
 * @property string $unused_commission
 * @property string $used_commission
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $updated_by
 */
class ReferralCashamount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%referral_cashamount}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lifetime_commission', 'unused_commission', 'used_commission'], 'number'],
            [['created_at', 'updated_at', 'updated_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User'),
            'lifetime_commission' => Yii::t('app', 'Lifetime Commission'),
            'unused_commission' => Yii::t('app', 'Unused Commission'),
            'used_commission' => Yii::t('app', 'Used Commission'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
    
    public function getUserReferral($userId){
		$minimumAmt = \common\models\Setting::getCashOutLimit();
		$data = self::find()
					->select(['lifetime_commission','unused_commission','used_commission',"IF(unused_commission>$minimumAmt,1,0) as cashout"])
					->where('user_id='.$userId)
					->asArray()
					->one();
		if($data!==null){
			return $data;
		}
		return ['lifetime_commission'=>0.00,'unused_commission'=>0.00,'used_commission'=>0.00,'cashout'=>0];
	}
}
