<?php
namespace common\models;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\Country;
use common\models\States;

/**
 * This is the model class for table "tbl_orders".
 *	
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%address}}';
    }
    
 
    public function rules()
    {
        return [
        [['fullname','addressLine1','city','state','country_id','mobile','address_type','is_shipping','is_billing'],'required'],
        [['address_type','country_id'],'number'],
        [['landmark','addressLine2'],'safe'],
        ['pincode', 'required', 'message' => 'Postcode cannot be blank.'],
      
        ];
    }
    
    
    
    
    public function attributeLabels()
    {
        return [
              'id' => Yii::t('app','Id'),
             'fullname' => Yii::t('app','FullName'),
             'addressLine1' => Yii::t('app','AddressLine1'),
             'addressLine2' => Yii::t('app','AddressLine2'),
             'city' => Yii::t('app','City'),
             'state' => Yii::t('app','State'),
             'pincode' => Yii::t('app','Postal Code'),
             'country_id' => Yii::t('app','Country'),
             'mobile' => Yii::t('app','Phone'),
             'landmark' => Yii::t('app','Landmark') ,
             'address_type' => Yii::t('app','AddressType') ,
             'is_shipping' => Yii::t('app','Is_Shipping'),
             'is_billing' => Yii::t('app','Is_Billing'),
             'created_at' => Yii::t('app','Created_At'),
             'updated_at' => Yii::t('app','Updated_At'),
             'created_by' => Yii::t('app','Created_By'),
             'updated_by' => Yii::t('app','Updated_By') ,
             'user_id' => Yii::t('app','User_Id'),
        ];
    }
    
    
    
    
    

    /**
     * @inheritdoc
     */
   
   public function beforeSave($insert)
			{
			    if (parent::beforeSave($insert)) {
			    	$this->updated_by= Yii::$app->user->id;
			    	$this->updated_at= time();
			    	if($insert){			    		
			    		$this->created_by= Yii::$app->user->id;
			    		$this->created_at= time();
			     	}
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
			}
			public function getTypevalue()
			{
				$model = AddressType::findOne(['id' =>$this->address_type]);
				if($model) {
					return $model->type;
				}
			}
		//	public function getCountry()
		//	{
			//	$model = Country::findOne(['name' =>$this->country_id]);
			//	if($model) {
			//		return $model->name;
			//	}
			//}
			//public function getState()
		//	{
				//$model = States::findOne(['country_id' =>$this->state]);
			//	if($model) {
				//	return $model->name;
			//	}
		//	}
			
			
	 public function getAddresstype()
		{
			   return $this->hasOne(AddressType::className(), ['id' =>'address_type']); 
		}	
		
		 public function getCountry()
		{
			   return $this->hasOne(Country::className(), ['id_countries' =>'country_id']); 
		}	
		
		 public function getStates()
		{
			   return $this->hasOne(States::className(), ['id' =>'state']); 
		}	
		/*Get Country Name By country_id */
		/*public function getCountryName(){
		
			if(isset($this->country_id)){
			$result = Country::findOne(['id_countries' =>$this->country_id ]);	
				return $result->name;
			}else{
				return '';
			}
		
		}*/
		
				/*Get State Name By state_id */
	/*	public function getStateName(){
			
		if(($this->state!='') && ($this->country_id !='')){
					
			$result = States::find()->where(['id' =>$this->state])->andWhere(['country_id' => $this->country_id])->one();
				return $result->name;	
			}else{
				return 'not set';
			}
		
		}*/
			
	  
	  
	  public function fields()
		{
			
			$fields=parent::fields();		  
			unset($fields['created_at']);
			unset($fields['updated_at']);
			unset($fields['created_by']);
			unset($fields['updated_by']);
			$fields['address_typename'] = function ($model) {
            return $model->addresstype->type; // Return related model property, correct according to your structure
			};
			$fields['countryname'] = function ($model) {
            return $model->country->name; // Return related model property, correct according to your structure
			};
			
			return $fields;
		}
		
		public function extraFields()
		{
			return [
			'addresstype',//=>['id','passUrl']
			

			];
		}
   }
   
   
