<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\Address;

/**
 * Order model
 *
 * @property integer $id
 * @property integer $user_id 
 * @property integer $status
 * @property integer $created_at
 
 */
 
class Offertrack extends ActiveRecord
{
   
    /**
     * @inheritdoc
     */
    public $cnt=0;
    public static function tableName()
    {
        return '{{%offerTrack}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            [['user_id','workorder_id','usedNo','pin'],'required'],
            [['qty'],'safe'],
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app','ID'),
			'user_id' =>Yii::t('app','User_Name'),
			'workorder_id' =>Yii::t('app','Workorder_ID') ,
			'usedNo' =>Yii::t('app','Used_No') ,
			'pin' => Yii::t('app','Pin'),
			'created_at' => Yii::t('app','created_at'),
			'updated_at' => Yii::t('app','updated_at'),
			'created_by' => Yii::t('app','created_by'),
			'updated_by' => Yii::t('app','updated_by'),
			
           ];
    }
    
    
    
    

	    public function beforeSave($insert)
		{
				
			if (parent::beforeSave($insert)) {
			    $this->updated_at= time();
				$this->updated_by= Yii::$app->user->id;
			    	if($insert){			    		
			    		$this->created_by=Yii::$app->user->id;			    					    		
			    		$this->created_at= time();			    		
			    		
			     	}
			     	
			    
			    	return true;
			    } else {
			    	
			     return false; 
			    }
		}
		
		
		
		 public function fields()
		{
			
			$fields=parent::fields();		  
			unset($fields['created_at']);
			unset($fields['updated_at']);
			unset($fields['created_by']);
			unset($fields['updated_by']);
			unset($fields['usedNo']);
			
			$fields["Stampcardused"]=function($model){
				return $this->usedNo;
			};
			
			
			return $fields;
		}
   
		
		   
}
