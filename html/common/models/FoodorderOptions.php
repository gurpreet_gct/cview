<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%foodorder_options}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property string $title
 * @property double $price
 */
class FoodorderOptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%foodorder_options}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'title', 'price'], 'required'],
            [['order_id', 'product_id'], 'integer'],
            [['price'], 'number'],
            [['title'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'order_id' => Yii::t('app','Order ID'),
            'product_id' => Yii::t('app','Product ID'),
            'title' => Yii::t('app','Title'),
            'price' => Yii::t('app','Price'),
            'qty'=>Yii::t('app','Quantity'),
        ];
    }
}
