<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<html>
<head>
	<title>Email Template</title>
</head>
<body>
<table class="templete_m" style="width: 567px; font-family: Arial,Helvetica;line-height: normal;" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td>
                <table style="width: 100%;">
                    <tbody>
                        <tr class="t_top">
                            <td  align="left"> </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table style="width: 567px; background:#f5f5f5" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td style="padding: 20px 20px; font-family: Arial, Helvetica; color: #666666; font-size: 12px;" colspan="2">
                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" style="color: #666666; font-size: 12px;" align="left"><strong>Hello <?= Html::encode($Name) ?></strong>,</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" height="15"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="color: #666666; font-size: 12px;">Please follow the below mentioned url to reset your <?php echo $pasMessage; ?>.<br/>
                                            </td>
                                        </tr>
										<tr>
                                            <td colspan="2" style="color: #666666; font-size: 12px;">
												
											<?= Html::a(Html::encode("Click Here to Reset ".$pasMessage),$resetLink) ?>
											</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 0px 20px; font-family: Arial, Helvetica; color: #666666; font-size: 16px;">
                                <table style="width: 100%;">
                                    <tbody>
                                        <tr>
                                            <td style="color: #691712;"><strong>With kind regards,</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="color: #691712;">
											<?= strtolower($_SERVER['HTTP_HOST']) ?>
											</td>
                                        </tr>
                                        <tr>
                                            <td style="color: #59acff;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><br />
		<tr class="t_footr">
			<td align="center">
				<table style="width: 567px;">
					<tbody>
						<tr>
							<td  align="center">
							<?= Html::a('Need Help | ', Url::home('http')) ?> 
							<?= Html::a('Have Feedback | ', Url::home('http')) ?> 
							<?= Html::a('Feel free to contact', Url::home('http')) ?>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
</tbody>
</table>
</body>
</html>
