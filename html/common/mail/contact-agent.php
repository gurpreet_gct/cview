<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $user common\models\User */

// $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/reset', 'token' => $validtoken]);
/*$formData = [
	'property_title' => 'High Clearance warehouse',
	'property_address' => '10-12 street, city, near XYZ ltd.',
	'property' => '12',
	'enquiry' => [
		'name' => 'John Doe',
		'email' => 'mail@johndoe.com',
		'phone' => '123-987-3847',
		'i_would_like_to' => [
			'receive a price guide',
			'receive a contract',
			'arrange an inspection',
			'receive information on similar properties'
		],
		'message' => 'Some additional message goes here!',
		'contact_me' => true
	]
];*/
?>
<html>
<head>
	<title>Property enquiry</title>
</head>
<body>
<table class="templete_m" style="width: 567px; font-family: Arial,Helvetica;line-height: normal;" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td>
                <table style="width: 100%;">
                    <tbody>
                        <tr class="t_top">
                            <td  align="left"> </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table style="width: 567px; background:#f5f5f5" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td style="padding: 20px 20px; font-family: Arial, Helvetica; color: #666666; font-size: 12px;" colspan="2">
                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" style="color: #666666; font-size: 14px;" align="left"><strong>Property Enquiry for: <?= Html::encode($property_title) ?> <br> <?= Html::encode($property_address) ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" height="15"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="color: #666666; font-size: 12px;">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 0px 20px; font-family: Arial, Helvetica; color: #666666; font-size: 12px;">
                                <table style="width: 100%;">
                                    <tbody>
                                        <tr>
                                            <td style="color: #691712;"><strong>Query details:</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="color: #691712;">
											<?= ucwords($enquiry_name) ?>
											</td>
                                        </tr>
                                        <tr>
                                            <td style="color: #691712;">
											<span><?= ucwords($enquiry_email) ?>
											<?php 
												if (!empty($enquiry_phone)) { ?>
													<?= ucwords($enquiry_phone) ?>
												<?php } ?></span>
											</td>
                                        </tr>
                                        <?php if (!empty($enquiry_i_would_like_to)) { ?>
	                                        <tr>
	                                        	<td style="color: #691712">
	                                        		<?php foreach($enquiry_i_would_like_to as $key => $val) { ?>
	                                        			<small><?= $val ?></small><br/>
	                                        		<?php } ?>
	                                        	</td>
	                                        </tr>
	                                    <?php } ?>
	                                    <tr>
                                            <td style="color: #691712;">
											<?= ucwords($enquiry_message) ?>
											</td>
                                        </tr>
                                        <?php if ($enquiry_contact_me) { ?>
	                                        <tr>
	                                        	<td style="color: #691712">
	                                        		Please contact me to discuss my insurance requirements.
	                                        	</td>
	                                        </tr>
	                                    <?php } ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><br />
		<tr class="t_footr">
			<td align="center">
				<table style="width: 567px;">
					<tbody>
						<tr>
							<td  align="center">
							<?= Html::a('Need Help | ', Url::home('http')) ?> 
							<?= Html::a('Have Feedback | ', Url::home('http')) ?> 
							<?= Html::a('Feel free to contact', Url::home('http')) ?>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
</tbody>
</table>
</body>
</html>
