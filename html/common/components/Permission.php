<?php 
/**
 * Component is to set permission of users.
 *   
 */
namespace common\components;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
 
class Permission extends Component
{ 
	
	function setPermission($user=NULL,$userId=NULL)
	{
		
		if($userId){
			
			if(\Yii::$app->authManager->getAssignment($user,$userId)){
			
			// delete that and then reassign
			
				$delReslt = \Yii::$app
							->db
							->createCommand()
							->delete('{{%auth_assignment}}',['user_id' => $userId])
							->execute();
							
				if($delReslt){
					
					// assign role
					$auth = Yii::$app->authManager;			
					$authorRole = $auth->getRole($user);
					$auth->assign($authorRole, $userId);
					
				}
				
			}else{
				
				// insert
				$auth = Yii::$app->authManager;			
				$authorRole = $auth->getRole($user);
				$auth->assign($authorRole, $userId);
				
			}
			
		}
		
	}
	
}