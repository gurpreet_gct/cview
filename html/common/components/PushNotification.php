<?php 
/**
 * Controller is to generate the Push Notification on the basis of code39.
 *   
 */
namespace common\components;
set_time_limit(0);  
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
 
class PushNotification extends Component
{ 
	public $settings=["API_ACCESS_KEY"=>"","iOSPem"=>""];
	private $iOSOb=null;	
	
	/* public function init()
	{
		
		  parent::init(); // Call parent implementation;
	} */
	
	function send($devices,$message,$workorderId=NULL,$foodOrderId=null)
   {
		
		
	    $messageText="Alpha App"; //default text
		if(isset($message['msg']) && $message['msg']!="")
		{
			$messageText=$message['msg'];
		}
		$customLink=isset($message['customLink'])?$message['customLink']:false;
		$androidMsg = array
		(
			'message' 	=> $messageText,
			'title'		=> 'Alpha App',
			'subtitle'	=> 'MSG subtitle1',
			'detail'	=> $message,						
			'vibrate'	=> 1,
			'sound'		=> 1,
			'soundname'		=> "beep.wav",
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon',
			'foodOrderId'	=> $foodOrderId,
			'workorderId'	=> $workorderId
		);
		
		$iOSMsg=array(
		"alert"=>$messageText,

			'message' 	=> $messageText,
			'title'		=> 'Alpha App',
			'subtitle'	=> 'MSG subtitle1',
			'detail'	=> $messageText,			
			"sound"=>"default"
			
		);
		
		$workOrderMesage	=	array(
									'workorderId'=>$workorderId
									
								);	
		$androidResult="";
		$iOSResult="";
		$androidDevice=array_values(array_keys($devices,"android"));
		
		/* Send notification to android device */
		if(count($androidDevice))
		$androidResult=	$this->sendAndroid($androidDevice,$androidMsg);
		
		
		/* Send notification to iphone device */
		if($this->settings["iOSPem"]){
		$iosDevice=array_values(array_keys($devices,"ios"));
		if(count($iosDevice))
		$iOSResult=	$this->sendIOS($iosDevice,$iOSMsg,$workOrderMesage,$foodOrderId);		
	    }
		
		if($iOSResult || $androidResult){
			return true;
			
		}else{
			
			return false;
		}
		
		// else
		
	
	}
   
	
   /* Send notification to android device */
    function sendAndroid($registrationIds,$msg){
        $conditions = array();
    
		$deviceTokens = is_array($registrationIds) ? $registrationIds : array($registrationIds);
		$fields = array
		(
			'registration_ids' =>$deviceTokens,
			'data'=> $msg
		);
		
		$headers = array
		(
			'Authorization: key=' . $this->settings['API_ACCESS_KEY'],
			'Content-Type: application/json'
		);
		 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );



	  
	  
		return $result;
		
    }



	function connectIOS()
	{
		// Put your private key's passphrase here:
		$passphrase = '';
		$cert=__DIR__ . '/'.$this->settings["iOSPem"];
		

		$ctx = stream_context_create();

		stream_context_set_option($ctx, 'ssl', 'local_cert', $cert);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		
		// Open a connection to the APNS server
		
		$fp = stream_socket_client(
			//'ssl://gateway.push.apple.com:2195', $err,
			'ssl://gateway.sandbox.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		
		$this->iOSOb=$fp;
	
		stream_set_blocking($this->iOSOb,false);
		
	}
	
	/* Send notification to IOS */
	function sendIOS($deviceTokens,$msg,$workOrderMesage=NULL,$foodOrderId=null)
	{
		$this->connectIOS();	
	

		// Create the payload body

		$body['aps'] = $msg;
		
		if(!empty($workOrderMesage)){
			$body['workorderData'] = $workOrderMesage;
			
		}
		
		if(!empty($foodOrderId)){		
			$body['foodOrderId'] = $foodOrderId;
		}
		
		
		// Encode the payload as JSON
		$payload = json_encode($body);
		$completeResult=array();
		
				$i=0;
	//	echo time()."<br/>";
		
		foreach($deviceTokens as $deviceToken)
		{
			
			$i++;
			
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		if($i%16==0)
			$this->connectIOS();
		// Send it to the server
		 $completeResult[$deviceToken] = fwrite($this->iOSOb, $msg, strlen($msg));
		if($completeResult[$deviceToken]==0)
		{

		//	sleep(1);
			$this->connectIOS();
			$completeResult[$deviceToken] = fwrite($this->iOSOb, $msg, strlen($msg));	
		}
	//	echo "<br/>";
		}
		
		
		fclose($this->iOSOb);

		
	}
	
}
