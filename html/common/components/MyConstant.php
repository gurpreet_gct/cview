<?php
namespace common\components;
use Yii;
use yii\base\Component;
use yii\helpers\Url;
use yii\base\InvalidConfigException;
use common\models\User;
use api\modules\v1\controllers\StreamingController;
use api\modules\v1\models\Agent;
use yii\helpers\ArrayHelper;


class MyConstant extends Component
{	
	/*find user if for apis*/
	public function UserID($token)
	{
		$user_id = 0;
		$authToken = str_replace("Bearer ", "", $token);
        $user = User::findIdentityByAccessToken($authToken);
       // print_r($user);die;
			if($user)
			$user_id = $user->id;
			return $user_id;
	}
		
	/*find agent if for apis*/
	public function AgentID($token)
	{
		$user_id = 0;
		$authToken = str_replace("Bearer ", "", $token);
        $user = Agent::findIdentityByAccessToken($authToken);
       // print_r($user);die;
			if($user)
			$user_id = $user->id;
			return $user_id;
	}

	/**
	 * Get Streaming server details
	 */
	public function StreamingServerInfo()
	{	
		return Yii::$app->params['wowza'];
	}
	
}
?>


