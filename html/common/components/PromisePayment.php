<?php
/** 
 * 
 */

namespace common\components;
use Yii;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\base\Component;

use PromisePay\PromisePay;

class PromisePayment extends Component
{	
	public $environment,$login,$password,$defaultUser,$feeID,$defaultAccount;
	public $debug=false;
	public function init()
	{
		PromisePay::Configuration()->environment($this->environment); // Use 'production' for the production environment.
		PromisePay::Configuration()->login($this->login);
		PromisePay::Configuration()->password($this->password);		
	}
	

	public function createUser($data)
	{
	  
	  try{
		  $users = PromisePay::User()->getList(array(
				'limit' => 1,
				'offset' => 0,
				'search'=>$data->email
			));
		 if(count($users))
		 {
			 return (object)$users[0];
		 } 
		 $user = PromisePay::User()->create(array(
			'id'            => uniqid($data->id),
			'first_name'    => $data->firstName,
			'last_name'     => $data->lastName,
			'email'         => $data->email,
			//'mobile'        => $data->mobile,
			//'address_line1' => '#268/1',
			//'address_line2' => 'Ladwa, Kurukshetra',
			//'state'         => 'Haryana',
			//'city'          => 'Ladwa',
			//'zip'           => '136132',
			'country'       => 'AUS'
		));
		return (object)$user;
	}catch(PromisePay\Exception\Api $ob)
	{
		if($this->debug)
		{print_r($ob);die("pars");}
	}
	//catch exception
	catch(\Exception $e) {
		if($this->debug)
	  {echo 'Message: ' .$e->getMessage();}
	}

		return true;

	}
	
	public function getListOfCardAccounts($userID)
	{
		try
		{
		return $accounts = PromisePay::User()->getListOfCardAccounts($userID);	
		}
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();}
			return false;
		}
	}
	public function getListOfWalletAccounts($userID)
	{
		try
		{
		return $accounts = PromisePay::User()->getListOfWalletAccounts($userID);	
		}
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();}
			return false;
		}
	}
	
	public function removeCard($cardId)
	{
		try
		{
		// $accounts = PromisePay::User()->getListOfCardAccounts($userID);	
		 return $account = PromisePay::CardAccount()->delete($cardId);	
		}
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();}
			return false;
		}
		
	}
	
	public function createCardAccount($data)
	{
		try{
			
		$account = PromisePay::CardAccount()->create(array(
		   'user_id'      => $data->user_id,
		   'full_name'    => $data->full_name,
		   'number'       => $data->number,
		   "expiry_month" => $data->expiry_month,
		   "expiry_year"  => $data->expiry_year,
		   "cvv"          => $data->cvv,
		
		));
		
		return $account;
		}
		
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();}
			
			return $e->getMessage();
			
		}	
	}
	
	
	public function createBankAccount($data)
	{
		try{
			
		$account = PromisePay::BankAccount()->create(array(
			'user_id'      => $data->user_id,
			"active"         => 'true',
			"bank_name"      => $data->bank_name,
			"account_name"   => $data->account_name,
			"routing_number" => $data->routing_number,
			"account_number" => $data->account_number,
			"account_type"   => $data->account_type,
			"holder_type"    => $data->holder_type,
			"country"        => $data->country,
		));
		if($account)
		{
				return $account;
		}		
		return false;
		}
	
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			$message=$e->getMessage();
			return $message;
		}
	}
	public function removeBankAcc($accId){
		try{
			return  PromisePay::BankAccount()->delete($accId);
		}catch(\PromisePay\Exception\Api $e){
			$message=$e->getMessage();
			print_r($message);
		}
	}
	
	
	public function createPayPalAccount($data)
	{
		try{

			$account = PromisePay::PayPalAccount()->create(array(
				'user_id'      => $data->user_id,			
				"paypal_email"      => $data->paypal_email			
			));
			return false;
		}
	
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			$message=$e->getMessage();
			print_r($message);
			$message=str_replace("Response Code: 422 Error Message: ","",$message);
			print_r($message);die;
			return $message;
		}
	}
	
	public function getListOfPayPalAccounts($userID)
	{
		
		try{
			
		return $accounts = PromisePay::User()->getListOfPayPalAccounts($userID);	
		}
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return false;
		}
	
	}
	public function setDisbursementAccount($uid,$acc)
	{
	
		try{
			
			return $account = PromisePay::User()->setDisbursementAccount($uid,$acc);
		}
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return false;
		}
	
	
	}
	public function createAuthority($accountId, $amount) {
       try{
			return PromisePay::DirectDebitAuthority()->create(
				array(
					'account_id' => $accountId,
					'amount' => $amount*100
				)
			);
		} catch(\PromisePay\Exception\Api $e){
			$message=$e->getMessage();
			print_r($message);
		}
    }
	public function depositAmount($bankAccId,$amount){	   
		try{$deposit = PromisePay::WalletAccounts()->deposit(
			'TARGET_WALLET_ID',
			array(
				'account_id' =>$bankAccId,
				'amount'     => $amount*100	
			)
			);
		} catch(\PromisePay\Exception\Api $e)
			{
				
				if($this->debug)
				{
					echo 'Message: ' .$e->getMessage();
					return false;
				}
				return false;
			}
	}
	public function getListOfBankAccounts($userID)
	{
		try{
			
		return $accounts = PromisePay::User()->getListOfBankAccounts($userID);	
		}
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return false;
		}
	}
	
	
	public function getFee()
	{
		try{
			$fees = PromisePay::Fee()->getList(array(
				'limit' => 20,
				'offset' => 0
			));
			if($fees)
		return $fees;
		}
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return $e->getMessage();
		}
	}
	
	public function createFee($percent=1000)
	{
		
		try{
			
			$fee = PromisePay::Fee()->create(array(
				'amount'      => $percent,
				'name'        => 'Tutor Plus charges',
				'fee_type_id' => '2',
				//'cap'         => '1',
				//'max'         => '3',
				//'min'         => '2',
				'to'          => 'seller'
			));
			return $fee;
		}
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return false;
		}
		
	}
		
	public function createItem($id,$amount,$buyer,$workOrderID)
	{
		
		try{
				try{
				$item = PromisePay::Item()->get($id);
				if($item)
					return $item;
				}
				catch(\PromisePay\Exception\Api $e){
				   //  $feeID="d5b9a7ee-9aba-40b4-bcad-5c82c24c726c";			  
					 $item = PromisePay::Item()->create(array(
						"id"              => $id,
						"name"            => $workOrderID,
						"amount"          => $amount*100,
						"payment_type_id" => 1,					
						"buyer_id"        => $buyer,
						"seller_id"       => $this->defaultUser,
						"description"     => 'Description'
					));
					
					
					return $item;	
				}
		}
		
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return $e->getMessage();
		}
		
	
	}
	
	
	
	public function makePayment($id,$buyerAccountID,$type="card")	{
		
		try{
			$accountID=$buyerAccountID;
			if($type!="card"){
				$directDebitAuthority = PromisePay::DirectDebitAuthority()->create(
				array
				(
					'account_id' => $buyerAccountID,
					'amount'     => $amount*100
				)
				);
				$accountID=$directDebitAuthority["id"];
			}
			
			
			   $item = PromisePay::Item()->makePayment($id, array(
					'account_id' => $accountID,
				));
				return $item;	
		}
		
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return $e->getMessage();
		}
		
	
	}
	
	public function releasePayment($id)
	{
		try{
			$item = PromisePay::Item()->releasePayment($id);
			return $item;	
		}
		
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return $e->getMessage();
		}
		
	
	}
	
	public function createCharges($name,$buyerAccountID,$amount,$email)
	{
		try{
			$feeID="d5b9a7ee-9aba-40b4-bcad-5c82c24c726c";
			return $createCharge = PromisePay::Charges()->create(
				array
				(
					'account_id' => $buyerAccountID,//'CARD_OR_BANK_ACCOUNT_ID',
					'amount' => $amount*100,
					'name'=>$name, // workorde 15 || subscription charges
					'email' => $email,//'charged.user@email.com',
					'fee_ids'=>$feeID,
					'zip' => 90210,
					'country' => 'AUS',
					'device_id' => 'DEVICE_ID',
					'ip_address' => '49.229.186.182'
				)
			);
		}
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return $e->getMessage();
		}
		
	}
	public function refundItem($id,$msg="")
	{
		try{
			$item = PromisePay::Item()->refund($id, array(
					//'refund_amount' => 1000,
					'refund_message' => 'session is canceled by the student'
				));
			
			return $item;
		}
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return $e->getMessage();
		}
	}


/* Amount Transfer To Banks*/
	public function amountTransfer($transferAmount){
		try{
			return $transferAmount = PromisePay::DirectDebitAuthority()->create($transferAmount);
			
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}
	##Token Auth #####Generate Card Token
	public function generateToken($userID){
	return PromisePay::Token()->generateCardToken(
			array('token_type' => 'card',
				'user_id' => $userID
			)
		);
		
	}
	#####Get a card account
	public function getCardDetails($cardId){
		
		try{
			return  PromisePay::CardAccount()->get($cardId);
			
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}
	public function referralCashOut($id)	{
		
		try{		
			 
			 $account_id=$this->defaultAccount;
			 $item = PromisePay::Item()->makePayment($id, array(
					
					'account_id' => $account_id,
				));
			return $item;	
		}
		
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return $e->getMessage();
		}
		
	
	}
	
	public function createItemCashout($id,$amount,$buyer,$itemName)
	{
		
		try{
				try{
				$item = PromisePay::Item()->get($id);
				if($item)
					return $item;
				}
				catch(\PromisePay\Exception\Api $e){
						$item = PromisePay::Item()->create(array(
						"id"              => $id,
						"name"            => 'Referral cashout by '.$itemName,
						"amount"          => $amount*100,
						"payment_type" => 4,
						"buyer_id"        => $this->defaultUser,
						"seller_id"       => $buyer,
						"description"     => 'Referral cash out'
					));
					
					
					return $item;	
				}
		}
		
		catch(\PromisePay\Exception\Api $e)
		{
			
			if($this->debug)
			{echo 'Message: ' .$e->getMessage();
			return false;
			}
			return $e->getMessage();
		}
		
	
	}
}

	


