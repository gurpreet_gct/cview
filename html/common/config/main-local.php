<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
             'dsn' => 'mysql:host=192.168.100.71;dbname=alphawallet',
            'username' => 'alphawallet',
            'password' => 'alphawallet',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
              'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.sendgrid.net',
            'username' => 'sweetspot.gct@gmail.com',
            'password' => 'gtech@1517',
            'port' => '587',
            'encryption' => '',
                        ],
        ],
          'log' => [
               'traceLevel' => 0,
				'targets' => [
				'file' => [
								'class' => 'yii\log\FileTarget',
								'categories' => ['yii\web\HttpException:404'],
								'levels' => ['error', 'warning'],
								'logFile' => '@runtime/logs/404.log',
							],
				'email'=>		[
								'class' => 'yii\log\EmailTarget',
								'mailer' => 'yii\swiftmailer\Mailer',
								'levels' => ['error'],
								'except' => ['yii\web\HttpException:404'],
								'message' => [
									'from' => ['noreply@graycelltech.com'],
									'to' => ['sweetspot.gct@gmail.com'],
									'subject' => 'SweetSpot(Graycell Server): Error Logs',
								],
						],
          ],
					/*	'targets' => [
							'file' => [
								'class' => 'yii\log\FileTarget',
								'categories' => ['yii\web\HttpException:404'],
								'levels' => ['error', 'warning'],
								'logFile' => '@runtime/logs/404.log',
							],
							'email' => [
								'class' => 'yii\log\EmailTarget',
								'except' => ['yii\web\HttpException:404'],
								'levels' => ['error', 'warning'],
								'message' => ['from' => 'noreply@graycelltech.com', 'to' => 'paras@graycelltech.com'],
							],
						],*/
					],
	/* S3 bucket */
	/*'s3bucket' => [
        'class' => frostealth\yii2\aws\s3\Storage::className(),
        'region' => 'Sydney',
        'credentials' => [ // Aws\Credentials\CredentialsInterface|array|callable
            'key' => 'AKIAJNH67UPCU2DHRTUQ',
            'secret' => 'PJOTELZpGE9pIcBF9BXp+Ajzy1aSMMDMxsZPzrrT',
        ],
        'bucket' => 'ssdatatrack',
        'cdnHostname' => 'http://example.cloudfront.net',
        'defaultAcl' => frostealth\yii2\aws\s3\Storage::ACL_PUBLIC_READ,
        'debug' => true, // bool|array
    ],	*/

    ],
];
