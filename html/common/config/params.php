<?php
    
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
	'profileUploadPath' => 'frontend/web/profile/',
	'agencylogoUploadPath' => 'frontend/web/agencylogo/',
	'PropertyImagePath' => 'frontend/web/images/property/',
	'PropertyDocPath' => 'frontend/web/images/property/doc/',
	'amalUploadPath' => 'frontend/web/amal/',
	'userUploadPath' => '/AlphaWallet/html/frontend/web/profile',
	'idsimagepath' => 'frontend/web/Idimages/',
	'uploads' => 'uploads/',
	'icons' => 'uploads/icons/',
	'workorderimage' => 'uploads/workorder/',
	'workordervideo' => 'uploads/workordervideo',
	'productPath' => Yii::getAlias('@frontend').'/web/uploads/catalog/',
	'loyaltycard' => 'api/web/loyaltycard/',
	'loyaltycardImage' => 'api/web/loyaltycardImage/',
  //  'profileUploadPath' => str_replace("/",DIRECTORY_SEPARATOR,Yii::$app->basePath."/" .'backend/web/profile/'),
    'salesEmail'=>'sales@beonic.com',
    'noReplyEmail'=>'no-reply@cview.com',
    'bdayMsg'=>'Happy birthday! For your birthday week please enjoy double loyalty points on purchases with participating stores',
    'androidGCMKey'=>'AIzaSyCERm33Kv8cMhMSyRhoW5kwTjmCaW7oCDk',
    'iOSPem'=>'SweetSpotAPNSDevCertificates.pem',
    'basePath'=>'/SweetSpotV2/',
    'radian'=>'30',
	'profileUploadDocPath' => 'frontend/web/profile/docs/',

	// Wowza Server Config 
	'wowza' => [
		'host' 			=> '54.206.31.70',
		'port' 			=> '1935',
		'application'	=> 'CVIEW_alpha',
		'source'		=>	[
			'username'    =>    'graycell',
			'password'	  =>    'graycell',
		],
	],
];
