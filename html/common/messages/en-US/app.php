<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    '' => '',
    ' URL Key' => '',
    '#Order ID' => '',
    'Access_Token' => '',
    'Accured dollors' => '',
    'Activation_Key' => '',
    'Active' => '',
    'Actual_Data' => '',
    'Ad Interval' => '',
    'Add "Background and foreground hex colour"' => '',
    'Add "FOOD MENU"' => '',
    'Add Ingredient/Options' => '',
    'Add Product for combo offer' => '',
    'Add Social media buttons and URLs' => '',
    'Add Store Location Opening Hours (optional)' => '',
    'Add geolocation radius (in km) for food order' => '',
    'Add loyalty points / dollar values' => '',
    'Add minimum spend a user must first spend before they get loyalty points.' => '',
    'Additional Photo Upload' => '',
    'Address' => '',
    'AddressLine1' => '',
    'AddressLine2' => '',
    'AddressType' => '',
    'Age Group' => '',
    'Age_Group_Id' => '',
    'Alternative Contact Person' => '',
    'Amount' => '',
    'Auth_Key' => '',
    'Auther_Id' => '',
    'Back Photo' => '',
    'Background Hex Colour' => '',
    'Background Image (640px x 360px)' => '',
    'Beacon Country' => '',
    'Beacon Groups' => '',
    'Beacon Sound Title' => '',
    'BeaconUUID' => '',
    'Behaviour Visit One' => '',
    'Behaviour Visit Three' => '',
    'Behaviour Visit Two' => '',
    'Behaviour_Dwell' => '',
    'Behaviour_Dwell_Minutes' => '',
    'Billing_Address' => '',
    'Billing_Address_Id' => '',
    'Bin' => '',
    'Brand_Logo' => '',
    'Bt_Id' => '',
    'Bullet Points (Up to 5 option max 15 characters each)' => '',
    'Business Gallery Pictures' => '',
    'Business Location' => '',
    'Business Name' => '',
    'Business Picture Url' => '',
    'Business Subscribe' => '',
    'Business Type' => '',
    'Business Web Address' => '',
    'Buy now' => '',
    'CVV' => '',
    'Campaign Expires' => '',
    'Campaign Name' => '',
    'Campaign Type' => '',
    'Can_Ship_Partially' => '',
    'Card Number' => '',
    'Card type' => '',
    'Card_Holder_Name' => '',
    'Card_Number' => '',
    'Card_Track' => '',
    'Card_Type' => '',
    'Cat IDs' => '',
    'Category' => '',
    'Category ID' => '',
    'Category Id' => '',
    'Category_Id' => '',
    'Child Pname' => '',
    'City' => '',
    'Collapsed' => '',
    'Color Options' => '',
    'Combo Product Type' => '',
    'Comment' => '',
    'Commercial' => '',
    'Commerical_Status' => '',
    'Commission %' => '',
    'Commission Amount' => '',
    'Company Name' => '',
    'Company Registration Number' => '',
    'Confirmation_Key' => '',
    'Count_Passes' => '',
    'Country' => '',
    'Country Name' => '',
    'Country_Code' => '',
    'Country_Id' => '',
    'Country_Of_Issuance' => '',
    'Coupon Code' => '',
    'Coupon_Code' => '',
    'Created At' => '',
    'Created At ' => '',
    'Created By' => '',
    'Created_At' => '',
    'Created_At ' => '',
    'Created_By' => '',
    'Currency_Iso_Code' => '',
    'Custom Offer Select1' => '',
    'Custom Offer Select2' => '',
    'Custom Offer Select3' => '',
    'Custom Offer Select4' => '',
    'Customer' => '',
    'Customer Referral %' => '',
    'Customer_Location' => '',
    'Customer_Note_Notify' => '',
    'DEAL Image / Video' => '',
    'DEAL Text' => '',
    'DEAL Title (50 characters max)' => '',
    'DEAL Type' => '',
    'Data' => '',
    'Date' => '',
    'Date_Of_Birth' => '',
    'Day Parting' => '',
    'Day Parting One' => '',
    'Day Parting One End' => '',
    'Day Parting Three End' => '',
    'Day Parting Two' => '',
    'Day Parting Two End' => '',
    'Deal Availability End Date' => '',
    'Deal Availability Start Date' => '',
    'Deal Count' => '',
    'Deal Crowser Check' => '',
    'Deal ID' => '',
    'Deal Id' => '',
    'Deal Title (50 characters max)' => '',
    'Deal Video Image' => '',
    'Deal Video Key' => '',
    'Deal_Id' => '',
    'Debit' => '',
    'Default Subscription Day' => '',
    'Default Subscription Fees' => '',
    'Description' => '',
    'Detect Type' => '',
    'Detect_At' => '',
    'Device' => '',
    'Device Id' => '',
    'Device_Token' => '',
    'Disabled' => '',
    'Discount' => '',
    'Discount Amount' => '',
    'Discount_Amount' => '',
    'Dollar' => '',
    'Dollors' => '',
    'Due date' => '',
    'Duration' => '',
    'Durbin_Regulated' => '',
    'Edit_Increment' => '',
    'Email' => '',
    'Email_Sent' => '',
    'Email_To' => '',
    'Enter Text for QR image' => '',
    'Enter Youtube/Vimeo Video URL' => '',
    'Event Venue' => '',
    'Exchange Rate' => '',
    'Exchange_Rate' => '',
    'Expiration_Date' => '',
    'Expiration_Month' => '',
    'Expiration_Year' => '',
    'Expiry' => '',
    'Expiry Date' => '',
    'Expiry Month' => '',
    'Expiry Year' => '',
    'Extra Deal Info' => '',
    'Facebook Url' => '',
    'First Name' => '',
    'First_Name' => '',
    'Forced_Shipment_With_Invoice' => '',
    'Friday closing time' => '',
    'Friday opening time' => '',
    'Friday_Full_Day_Close' => '',
    'Friday_Full_Day_Open' => '',
    'Front Photo' => '',
    'Front Photo Upload' => '',
    'FullName' => '',
    'Full_Name' => '',
    'Google Url' => '',
    'Grand Total' => '',
    'Grand_Total' => '',
    'Hash_Key' => '',
    'Have Child' => '',
    'Have Option' => '',
    'Health_Care' => '',
    'Help' => '',
    'ID' => '',
    'Icon' => '',
    'Icon_Type' => '',
    'Id' => '',
    'Id_Countries' => '',
    'Image' => '',
    'Image_Url' => '',
    'Instagram Url' => '',
    'Interval' => '',
    'Ip' => '',
    'Is Added' => '',
    'Is Combo' => '',
    'Is Delete' => '',
    'Is Offline' => '',
    'Is Online' => '',
    'Is Saved' => '',
    'Is disbursement account ?' => '',
    'Is online' => '',
    'Is this is a food establishment?' => '',
    'IsCombo' => '',
    'Is_Billing' => '',
    'Is_Delete' => '',
    'Is_Offline' => '',
    'Is_Shipping' => '',
    'Is_Used' => '',
    'Item Total Price' => '',
    'Label Text Colour (Hex Colour)' => '',
    'Landmark' => '',
    'Lang' => '',
    'Last Name' => '',
    'Last_4' => '',
    'Last_Login' => '',
    'Last_Name' => '',
    'Lat' => '',
    'Latitude' => '',
    'Lft' => '',
    'Lifetime Commission' => '',
    'Links Url' => '',
    'Location' => '',
    'Login_At' => '',
    'Logo' => '',
    'Logo Photo ( Logo size should be 132px x 132px dimension )' => '',
    'Logo Upload' => '',
    'Logout_At' => '',
    'Longitude' => '',
    'Loyalty' => '',
    'Loyalty Dollars' => '',
    'Loyalty Dollars Value' => '',
    'Loyalty Pin' => '',
    'Loyalty dollors' => '',
    'Loyalty dollors value' => '',
    'Loyalty value one' => '',
    'Loyalty value three' => '',
    'Loyalty value two' => '',
    'Loyalty_Dollars' => '',
    'Loyalty_Pin' => '',
    'Loyalty_Status' => '',
    'Lvl' => '',
    'Main Photo Upload' => '',
    'Major' => '',
    'Major Value' => '',
    'Manufacturer (beacon uuid)' => '',
    'Manufacturer Serial Number' => '',
    'Masked_Number' => '',
    'Media Contract Number (optional)' => '',
    'Media Contract Number / PO Number' => '',
    'Merchant_Account_Id' => '',
    'Message Limit' => '',
    'Method' => '',
    'Minimum referral amount for Cashout' => '',
    'Minor' => '',
    'Minor Value' => '',
    'Monday closing time' => '',
    'Monday opening time' => '',
    'Monday_Full_Day_Close' => '',
    'Monday_Full_Day_Open' => '',
    'Moveable_D' => '',
    'Moveable_L' => '',
    'Moveable_R' => '',
    'Moveable_U' => '',
    'My Deal image (Size: 750pxW x 360pxH Resolution 72DPI)' => '',
    'My_Deals' => '',
    'My_Loyalty' => '',
    'My_Prefrences' => '',
    'My_Store' => '',
    'My_Wallet' => '',
    'Name' => '',
    'Name on Card' => '',
    'Name_On_Card' => '',
    'New Password' => '',
    'New_Password' => '',
    'Notes Box' => '',
    'Notification Limit' => '',
    'Notification Text (110 characters max)' => '',
    'Number of Passes' => '',
    'Offer Front Photo ( Image size should be 640px x 300px dimension )' => '',
    'Offer Text1' => '',
    'Offer Title' => '',
    'Offer Was' => '',
    'Offer now' => '',
    'Offer was' => '',
    'Offer_Now' => '',
    'Offer_Text' => '',
    'Offer_Title' => '',
    'Offer_Was' => '',
    'Office Fax' => '',
    'Office Phone' => '',
    'OnWeb' => '',
    'Open Video In Browser' => '',
    'Order ID' => '',
    'Order Id' => '',
    'Order Total' => '',
    'Order_Id' => '',
    'Org_Name' => '',
    'P Type' => '',
    'PSA Type' => '',
    'Paid via' => '',
    'Parent ID' => '',
    'Parent_Key' => '',
    'Partner' => '',
    'Partner ' => '',
    'Partner Referral %' => '',
    'Pass Url' => '',
    'Password_Reset_Token' => '',
    'Path' => '',
    'Payable' => '',
    'Payment Currency Code' => '',
    'Payment Currency ID' => '',
    'Payment Currency Id' => '',
    'Payment Purrency Code' => '',
    'Payment Status' => '',
    'Payment_Currency_Code' => '',
    'Payment_Currency_Id' => '',
    'Payroll' => '',
    'Phone' => '',
    'Photo' => '',
    'Photo ( Image size should be 640px x 300px dimension )' => '',
    'Pin' => '',
    'Pin for offline detail updation' => '',
    'Pinterest Url' => '',
    'Polygon Shape' => '',
    'Post-Code_To' => '',
    'Post_Code' => '',
    'Post_Code_Range' => '',
    'Postal Code' => '',
    'Postcode' => '',
    'Postcode Range' => '',
    'Postcode To' => '',
    'Power Value' => '',
    'Prepaid' => '',
    'Price' => '',
    'Priority' => '',
    'Priority_Id' => '',
    'Product Currency Code' => '',
    'Product Currency ID' => '',
    'Product Currency Id' => '',
    'Product Exchange Price' => '',
    'Product ID' => '',
    'Product Id' => '',
    'Product Image' => '',
    'Product Name' => '',
    'Product Price' => '',
    'Product Taxrule Class' => '',
    'Product Type' => '',
    'Product_Class' => '',
    'Product_Currency_Code' => '',
    'Product_Currency_Id' => '',
    'Product_Exchange_Price' => '',
    'Product_Id' => '',
    'Product_Price' => '',
    'Purchase History One' => '',
    'Purchase History Three' => '',
    'QR code' => '',
    'Qr_Code' => '',
    'Qty' => '',
    'Qty_Ordered' => '',
    'Quantity' => '',
    'Quantity_Ordered' => '',
    'RSSI' => '',
    'RSSI value' => '',
    'R_Value_On_Day' => '',
    'Rate' => '',
    'Read Terms' => '',
    'Read_Terms' => '',
    'Readonly' => '',
    'Redeem Dollar' => '',
    'Redeem Dollors' => '',
    'Redeem Loyalty' => '',
    'Refer By' => '',
    'Region' => '',
    'Region/Suburb' => '',
    'Related Product' => '',
    'Relevant Location Broker' => '',
    'Relevant Partner' => '',
    'Removeable' => '',
    'Removeable_All' => '',
    'Reseller' => '',
    'Resolution' => '',
    'Return period in days from purchase day' => '',
    'Rgt' => '',
    'Role_Id' => '',
    'Root' => '',
    'Rule Id' => '',
    'Rule_Id' => '',
    'SKU' => '',
    'SN' => '',
    'SSP Barcode Image' => '',
    'SSP_Moreinfo' => '',
    'Saturday closing time' => '',
    'Saturday opening time' => '',
    'Saturday_Full_Day_Close' => '',
    'Saturday_Full_Day_Open' => '',
    'Select Beacon Group' => '',
    'Select Country' => '',
    'Select Event Date' => '',
    'Select Offer Type' => '',
    'Select Partner' => '',
    'Select Product' => '',
    'Select Product for combo offer' => '',
    'Select Store' => '',
    'Select how many times the pass can be used' => '',
    'Select maximum of items per pass' => '',
    'Selected' => '',
    'Session_Id' => '',
    'Set Geolocation or Proximity' => '',
    'Sex' => '',
    'Shipper_Id' => '',
    'Shipping Charges' => '',
    'Shipping_Address' => '',
    'Shipping_Address_Id' => '',
    'Shipping_Amount' => '',
    'Shipping_Charges' => '',
    'Shipping_Description' => '',
    'Shipping_Tax_Amount' => '',
    'Short Description' => '',
    'Size' => '',
    'Sort Order' => '',
    'Sort_Id' => '',
    'Sort_Name' => '',
    'Sortname' => '',
    'Sound File Url' => '',
    'State' => '',
    'State Name' => '',
    'State_Id' => '',
    'Status' => '',
    'Store' => '',
    'Store ID' => '',
    'Store Id' => '',
    'Store Location Address' => '',
    'Store Location Email (optional)' => '',
    'Store Location Id' => '',
    'Store Location Name' => '',
    'Store Location Phone Number (optional)' => '',
    'Store/Brand Address(head office)' => '',
    'Store/Brand Description' => '',
    'Store/Brand Logo (132px x 132px)' => '',
    'Store/Brand Name' => '',
    'Store_Id' => '',
    'Store_Location_Id' => '',
    'Street' => '',
    'Street Address' => '',
    'Street Number' => '',
    'Street Number and Street' => '',
    'Sub Item Total Price' => '',
    'Subscription amount' => '',
    'Subscription due day' => '',
    'Subtotal' => '',
    'Sunday opening time' => '',
    'Sunday_Full_Day_Close' => '',
    'Sunday_Full_Day_Open' => '',
    'T_Id' => '',
    'Tax' => '',
    'Tax Amount' => '',
    'Tax Class' => '',
    'Tax Id' => '',
    'Tax Identifier' => '',
    'Tax Rate' => '',
    'Tax_Amount' => '',
    'Tax_Class' => '',
    'Tax_Id' => '',
    'Taxrule Country' => '',
    'Temperature' => '',
    'Temperature End' => '',
    'Temperature Start' => '',
    'Text (Note : 120 characters max)' => '',
    'Thursday closing time' => '',
    'Thursday opening time' => '',
    'Thursday_Full_Day_Close' => '',
    'Thursday_Full_Day_Open' => '',
    'Tick if this item will be available for purchase in-app?' => '',
    'Tick to select the rules and conditions for this deal' => '',
    'Ticket Info' => '',
    'Time Stamp' => '',
    'Title' => '',
    'Token' => '',
    'Total' => '',
    'Total_Offline_Refunded' => '',
    'Total_Online_Refunded' => '',
    'Total_Paid' => '',
    'Total_Refunded' => '',
    'Transaction ID' => '',
    'Transaction Id' => '',
    'Transaction_Id' => '',
    'Tuesday closing time' => '',
    'Tuesday opening time' => '',
    'Tuesday_Full_Day_Close' => '',
    'Tuesday_Full_Day_Open' => '',
    'Twitter Url' => '',
    'Type' => '',
    'URL Key' => '',
    'UUID' => '',
    'Unique ID' => '',
    'Unique_Number_Identifier' => '',
    'Unused Commission' => '',
    'Updated At' => '',
    'Updated By' => '',
    'Updated_At' => '',
    'Updated_By' => '',
    'Uplaod Video Image ( Video Image size should be 640px x 300px dimension )' => '',
    'Upload Barcode Image' => '',
    'Upload Media Type' => '',
    'Upload video' => '',
    'Url Key' => '',
    'Use_Returned' => '',
    'Used Commission' => '',
    'Used Passes' => '',
    'Used_No' => '',
    'User' => '',
    'User ID' => '',
    'User Id' => '',
    'User Name' => '',
    'User Type' => '',
    'User id' => '',
    'User name' => '',
    'User_Enter' => '',
    'User_Id' => '',
    'User_Name' => '',
    'User_Type' => '',
    'Username' => 'Crazyy',
    'Value' => '',
    'Value On Day' => '',
    'Venmo_Sdk' => '',
    'Video' => '',
    'Video Image' => '',
    'Video Name' => '',
    'Video Type' => '',
    'Video Types' => '',
    'Video Url' => '',
    'Video url' => '',
    'Visible' => '',
    'Wallet_Pin_Code' => '',
    'Wednesday closing time' => '',
    'Wednesday opening time' => '',
    'Wednesday_Full_Day_Close' => '',
    'Wednesday_Full_Day_Open' => '',
    'Weight' => '',
    'Workorder ID' => '',
    'Workorder Id' => '',
    'Workorder Type' => '',
    'Workorder_ID' => '',
    'Workorder_Id' => '',
    'brand' => '',
    'created at' => '',
    'created by' => '',
    'created_at' => '',
    'created_by' => '',
    'discount' => '',
    'fb_Identifier' => '',
    'food order id' => '',
    'issuingBank' => '',
    'loyalty ID' => '',
    'order Id' => '',
    'product exchange price' => '',
    'updated at' => '',
    'updated by' => '',
    'updated_at' => '',
    'updated_by' => '',
    'used Limit' => '',
    'value On Day' => '',
    'value_On_Day' => '',
];
